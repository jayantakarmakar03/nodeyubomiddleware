document.onreadystatechange = function(e){
    if ((document.readyState === 'interactive' || document.readyState === 'complete') && document.getElementById("bootm-box")==undefined){
		(function() {
			let host = location.hostname;
			let url=location.href;
			const domain = "https://yubo.yugasa.org"
			const clientId="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6InNhYWthYXJfY2hhdGJvdCIsImlhdCI6MTYwMDg1NDU4NX0.SdSSjWqVVOIzTu1DvqvEkKS2X2jkRj3tmRliJ8v-Z6U";

			let parent = document.createElement('div');
			parent.setAttribute('class', 'iframe-box');
			parent.setAttribute('id', 'iframe-box');

			let bootm = document.createElement('div');
			bootm.setAttribute('class', 'bootm-box');
			bootm.setAttribute('id', 'bootm-box');

			let chatstart = document.createElement('div');
			chatstart.setAttribute('class', 'chat-start bounce-top');

			let closeimg = document.createElement('div');
			closeimg.setAttribute('class', 'close-img');
			let imgclose  = document.createElement('img');
			imgclose.src = domain+"/img/close.svg";
			closeimg.appendChild(imgclose);

			let outercircle = document.createElement('div');
			outercircle.setAttribute('class', 'outerCircle');

			let innercircle = document.createElement('div');
			innercircle.setAttribute('class', 'innerCircle');

			let icon = document.createElement('div');
			icon.setAttribute('class', 'icon');

			let bubbleimg  = document.createElement('img');
			bubbleimg.setAttribute('class', 'bubble-img');
			bubbleimg.src = domain+"/img/animations/real-estate-agent-06.gif";

			icon.appendChild(bubbleimg);

			let yuboicontext=document.createElement('div');
			yuboicontext.setAttribute('id', 'yuboicontext');
			yuboicontext.innerHTML="Hi, this is Saakaar";

			// chatstart.appendChild(outercircle)
			// chatstart.appendChild(innercircle)
			chatstart.appendChild(yuboicontext)
			chatstart.appendChild(icon)

			bootm.appendChild(chatstart);
		    bootm.appendChild(closeimg);

		    //js for banner
		    // let marquee  = document.createElement('marquee');
		    // marquee.setAttribute('id', 'bnner');
		    // marquee.innerHTML="Price Revising after 20th Nov 2021, Hurry up to Book your Flat in Saakaar Aquacity.";
		    // bootm.appendChild(marquee);
		    //js for banner end

		    let iframe = document.createElement('iframe');
		   	iframe.setAttribute('id', 'frame-lode');
		    iframe.src = domain+"/chatWindow?clientId="+clientId+"&host="+host+"&url="+url;
		    parent.appendChild(bootm);
		    parent.appendChild(iframe);
			
			let powered = document.createElement("div");
			powered.setAttribute("class","poweredbyyugasa");
			let poweredlink = document.createElement("a");
			poweredlink.setAttribute("href","https://helloyubo.com/");
			poweredlink.setAttribute("target","_blank");
			poweredlink.setAttribute("title","Building Blocks for Building Bots");
			let poweredbytext = document.createTextNode("Powered by ");
			let hyperlinktext = document.createTextNode("Yugasa Bot");
			poweredlink.appendChild(hyperlinktext);
			powered.appendChild(poweredbytext);
			powered.appendChild(poweredlink);
			parent.appendChild(powered);

		    document.body.appendChild(parent);
			document.getElementById("bootm-box").addEventListener("click",function(){
				document.getElementById("iframe-box").classList.toggle("open-chat-window");
			});
			// if(url=='https://saakaar.com/'){
			// 	setTimeout(function(){
			// 		document.getElementById("bootm-box").dispatchEvent(new Event('click'));
			// 	},2000)	
			// }
			/*Auto Popup Disabled on 2nd Aug 2021
			launchBot();
			*/
		})();
    }
};

// window.onload = function() {
// 	document.getElementById("bootm-box").dispatchEvent(new Event('click'));	
// };
function launchBot() {
	// First check, if localStorage is supported.
	if (window.localStorage) {
		// Get the expiration date of the previous popup.
		var nextPopup = localStorage.getItem( 'nextyubo' );

		if (nextPopup > new Date()) {
			return;
		}
		// Store the expiration date of the current popup in localStorage.
		var expires = new Date();
		expires = expires.setHours(expires.getHours() + 24);

		localStorage.setItem( 'nextyubo', expires );
	}

	setTimeout(function(){document.getElementById("bootm-box").dispatchEvent(new Event('click'));},5000)
};