async function uploadFile() {
	let funcCall = await Yubo.upload(document.getElementById('clientId').value);
	Yubo.send("Attachment sent",document.getElementById("clientId").value);

	// if (funcCall == 200 || funcCall == "200") {
	//     var successDiv = document.getElementById("success")
	//     successDiv.innerHTML = "Successfully uploaded"
	//     successDiv.style.display = "block"
	//     setTimeout(function () {
	//         successDiv.style.display = "none"
	//     }, 2000);
	// } else {
	//     var errorDiv = document.getElementById("error");
	//     errorDiv.innerHTML = "File size should be less than 500kb"               
	//     errorDiv.style.display = "block"
	//     setTimeout(function () {
	//         errorDiv.style.display = "none"
	//     }, 2000);
	// }
}


var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;

if (typeof SpeechRecognition != 'undefined') {
	var recognition = new SpeechRecognition();
	recognition.lang = 'en-IN';
}

function voice() {
	document.getElementById('mic').setAttribute('src', "img/mic_off.png")
	document.getElementById('mic').setAttribute('title', "Stop Listening")
	document.getElementById('mic').setAttribute('onclick', "stop();")
	recognition.start();
	document.getElementById("msg").placeholder = "Listening...";
	recognition.onsoundstart = function () {
		recognition.onresult = function (event) {
			let current = event.resultIndex;
			let transcript = event.results[current][0].transcript;
			// console.log('silence')
			// $('#msg').empty();
			document.getElementById('msg').value = transcript
			Yubo.send(transcript, document.getElementById('clientId').value);
			stop();
		}
	}
}
function stop() {
	document.getElementById('mic').setAttribute('src', "img/micro_phone.png")
	document.getElementById('mic').setAttribute('title', "Start Listening")
	document.getElementById('mic').setAttribute('onclick', "voice();")
	recognition.stop();
	document.getElementById("msg").placeholder = "Write your message...";
}
// document.addEventListener("DOMContentLoaded", function () {
// 	if (document.getElementById('clientId').value == 'amit_chatbot') {
// 		Yubo.launch(document.getElementById('clientId').value);
// 	}
// });

function start_slider(id) {
	const ss = new Splide(id, {
		type: 'slide',
		autoplay: true,
		perPage: 1,
		focus: 'center',
		padding: {
			right: '5rem',
			left: '1rem'
		},
		lazyLoad:'nearby'
	});
	ss.mount();
}

//for language, chat restart and attachfile popup
$(document).ready(function () {
	$('#show-hidden-menu').click(function () {
		$('.hidden-menu').toggle();
		if ($('img.show-hidden-menu_img').hasClass('show-menu-icon')) {
			$('img.show-hidden-menu_img').removeClass('show-menu-icon');
			$('img.show-hidden-menu_img').attr('src', './dist/img/Menu-icon_open.png');
		} else {
			$('img.show-hidden-menu_img').addClass('show-menu-icon');
			$('img.show-hidden-menu_img').attr('src', './dist/img/Menu-icon_close.png');
		}
	});
});


//context menu js

function changeLanguage(language) {
    var element = document.getElementById("url");
    element.value = language;
    element.innerHTML = language;
    }

    function showDropdown() {
    document.getElementById("menu-dropdown").classList.toggle("show");
    }

    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
    if (!event.target.matches(".dropbtn")) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains("show")) {
            openDropdown.classList.remove("show");
        }
        }
    }
};


function Botlanguage(){
        let data = {
            clientId : $("#clientId").val()
        };
        $.ajax({
            async: false,
            cache: false,
            type: "POST",
            data: data,
            url: '/botLanguage',
            success:  function (data) {
                if (data.statusCode == 200) {
                	if(data.data.length==1){
                		$('#changelang').hide();
                		return;
                	}
                    $('#language_list').html('');
                    let cls="";
                    for (const iterator of data.data) {
                    	cls=(getLang()==iterator.languageCode)?"selected":"";
                        let html = '<li class="'+cls+'" onclick="setLang(\''+iterator.languageCode+'\',this);"><input type="radio" name="lang" id="'+iterator.languageCode+'" value="'+iterator.languageCode+'" /><label class="symbol" for="'+iterator.languageCode+'">'+iterator.languageCode+'</label><label for="'+iterator.languageCode+'">'+iterator.language+'</label><label for="'+iterator.languageCode+'">('+iterator.languageNative+')</label></li>'
                        $('#language_list').append(html);
                    }
        
                }
            }
        });
}

Botlanguage();


function onRestart() {
	Yubo.send("Restart",document.getElementById("clientId").value);

	$('.hidden-menu').toggle();
	if ($('img.show-hidden-menu_img').hasClass('show-menu-icon')) {
		$('img.show-hidden-menu_img').removeClass('show-menu-icon');
		$('img.show-hidden-menu_img').attr('src', './dist/img/Menu-icon_open.png');
	} else {
		$('img.show-hidden-menu_img').addClass('show-menu-icon');
		$('img.show-hidden-menu_img').attr('src', './dist/img/Menu-icon_close.png');
	}
}

function setLang(lang="en",obj) {
	$(obj).addClass('selected').siblings().removeClass('selected');
	let clientId=document.getElementById("clientId").value;
	let getUserSessionData = Yubo.getSession(Yubo.getCookie(clientId));
	getUserSessionData.lang=lang;
	Yubo.setSession(Yubo.getCookie(clientId), getUserSessionData);
	$('#changeLanguageModal').fadeOut(200);
}

function getLang() {
	let clientId=document.getElementById("clientId").value;
	let getUserSessionData = Yubo.getSession(Yubo.getCookie(clientId));
	return (getUserSessionData.lang!=undefined)?getUserSessionData.lang:"en";
}

function openMenu(){
	let e=window.event;
	e.stopPropagation();
}
function closePopups(){
	$('#menu-dropdown').hide();
	$('#changeLanguageModal').hide();
}