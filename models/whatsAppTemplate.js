var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
    mongoose.connect(require('./connection-string'));
}
var newSchema = new Schema({
    'clientId': { type: String },
    'name': { type: String },
    "agent_Id": {  type: Array, 'default': [] },
    'text': { type: String, default: null },
    'msg_type': { type: String, default: 'TEXT' },
    'header': { type: String, default: '' },
    'footer': { type: String, default: '' },
    'lang': { type: String, default: 'en_us' },
    'button_url': { type: Boolean, default: false },
    'button': { type: Boolean, default: false },
    'status': { type: String, default: 'APPROVED' },
    'templateId':{ type: String, default: '' },
    'components':{type:Object},
    'category':{ type: String, default: 'TRANSACTIONAL' },
    'createdAt': { type: Date, default: Date.now },
    'updatedAt': { type: Date, default: Date.now }
});
newSchema.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});
newSchema.pre('update', function () {
    this.update({}, { $set: { updatedAt: Date.now() } });
});
newSchema.pre('findOneAndUpdate', function () {
    this.update({}, { $set: { updatedAt: Date.now() } });
});
module.exports = mongoose.model('whatsAppTemplate', newSchema);
