document.onreadystatechange = function(e){
    if ((document.readyState === 'interactive' || document.readyState === 'complete') && document.getElementById("bootm-box")==undefined){
		(function() {
			const domain = "https://yubo.yugasa.org"
			const clientId="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6ImRvY2dlbmllIiwiaWF0IjoxNjM0NzA3MTUxfQ._TvlqETiL5FhEvQjmP-f5lOsx2d8tizEa_zKC3jkxB0";
			
			let host = location.hostname;
			let url=location.href;

			let parent = document.createElement('div');
			parent.setAttribute('class', 'iframe-box');
			parent.setAttribute('id', 'iframe-box');

			let bootm = document.createElement('div');
			bootm.setAttribute('class', 'bootm-box');
			bootm.setAttribute('id', 'bootm-box');

			let chatstart = document.createElement('div');
			chatstart.setAttribute('class', 'chat-start bounce-top');

			let closeimg = document.createElement('div');
			closeimg.setAttribute('class', 'close-img');
			closeimg.setAttribute('id', 'yubo-close-img');
			closeimg.innerHTML='<svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg>'

			let outercircle = document.createElement('div');
			outercircle.setAttribute('class', 'outerCircle');

			let innercircle = document.createElement('div');
			innercircle.setAttribute('class', 'innerCircle');

			let icon = document.createElement('div');
			icon.setAttribute('class', 'hy-icon');

			let bubbleimg  = document.createElement('img');
			bubbleimg.setAttribute('class', 'bubble-img');
			bubbleimg.src = domain+"/img/docgenie_widget.png";
			bubbleimg.setAttribute('alt', 'Docgenie Chat Icon');

			icon.appendChild(bubbleimg);
			
			let yuboicontext=document.createElement('div');
			yuboicontext.setAttribute('id', 'yuboicontext');
			yuboicontext.innerHTML="Click me for any help";

			chatstart.appendChild(yuboicontext)


			// chatstart.appendChild(outercircle)
			// chatstart.appendChild(innercircle)
			chatstart.appendChild(icon)

			bootm.appendChild(chatstart);
		   		    let iframe = document.createElement('iframe');
		   	iframe.setAttribute('id', 'frame-lode');
		   	iframe.setAttribute('title', 'Yugasa Bot Chat Window');
		    iframe.src = domain+"/chatWindow?clientId="+clientId+"&host="+host+"&url="+url;
		    // parent.appendChild(bootm);
		    parent.appendChild(closeimg);
		    parent.appendChild(iframe);
		    document.body.appendChild(bootm)
		    document.body.appendChild(parent);
			document.getElementById("bootm-box").addEventListener("click",function(){
				document.getElementById("iframe-box").classList.add("open-chat-window");
				document.getElementById("bootm-box").style.display = 'none';
			});
			document.getElementById("yubo-close-img").addEventListener("click",function(){
				document.getElementById("iframe-box").classList.remove("open-chat-window");
				document.getElementById("bootm-box").style.display = 'block';
			});
			//launchBot();
		})();
    }
    function start_slider(id) {
		const ss = new Splide(id, {
			type: 'slide',
			autoplay: true,
			perPage: 1,
			focus: 'center',
			padding: {
				right: '5rem',
				left: '5rem'
			}
		});
		ss.mount();
	}
};

function launchBot() {
	// First check, if localStorage is supported.
	if (window.localStorage) {
		// Get the expiration date of the previous popup.
		var nextPopup = localStorage.getItem( 'nextyubo' );

		if (nextPopup > new Date()) {
			return;
		}
		// Store the expiration date of the current popup in localStorage.
		var expires = new Date();
		expires = expires.setHours(expires.getHours() + 24);

		localStorage.setItem( 'nextyubo', expires );
	}

	setTimeout(function(){
		if(document.getElementById("iframe-box").classList.contains("open-chat-window")){
			return;
		}
		document.getElementById("bootm-box").dispatchEvent(new Event('click'));
	},5000);
};