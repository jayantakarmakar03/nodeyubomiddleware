const db = require("../models/all-models"),
    Hubspot = require("./hubspot"),
    moment = require("moment"),
    axios = require("axios");
languageConvert = require("../controllers/languageConvert");
const dbController = require("../controllers/dbintegration");
const genAi= require("../controllers/genAi.js");

module.exports = {
    sendMessage: sendMessage,
    followUp: followUp,
    pythonApi: pythonApi,
    botLanguage: botLanguage,
    botGenAi: botGenAi
};

let currentDate = moment(new Date()).format("DD/MM/YYYY"),
    now = moment(Date.now()).tz('Asia/Kolkata'),
    currentTime = now.hour() + ':' + now.minutes(),
    errorMessage = "Please come back and chat with me after some time. Right now I am taking some red and blue pills for maintenance."

async function sendMessage(req, res) {
try{
    let date = new Date(),
        client = await db.Client.findOne({
            clientId: req.body.data.client_id,
        });


    if(req.body.data.text==undefined){
        return res.json({
            success: false,
            message: "Could not find user input",
            type_option: true,
        });
    }
    
    req.body.data.text = req.body.data.text.trim();

    //Client not exist message
    if (!client) {
        return res.json({
            success: false,
            message: "Client does not exists",
            type_option: true,
        });
    }

    //UserId is invalid
    if (!validateUserid(req.body.data.userId)) {
        console.log("rejected ", req.body.data.userId)
        return res.json({
            success: false,
            message: "UserId is invalid",
            type_option: true,
        });
    }

    //set defaults for isHinglish

    let isHinglish=false;
    let api_key=false;
    let hinglish_text=false;

    //check if isHinglish
    api_key=await genAi.makeGenAiConfig(req.body.data.client_id);
    if(api_key){
        let resp=await genAi.detectLang(req.body.data.text,req.body.data.client_id);
        if(resp.isHinglish){
            isHinglish=true;
            hinglish_text=resp.text;
        }
    }

    //Socket to send msgs to agent panel
    let socketData = {
        clientId: req.body.data.client_id,
        userId: req.body.data.userId,
        message: req.body.data.text,
        messageType: "usermessage",
        name: req.body.data.session ? (req.body.data.session.name ? req.body.data.session.name : "Unknown") : "Unknown",
        date: currentDate,
        time: currentTime
    }
    io.sockets.emit('message', socketData);

    //Checking the user associated with client Id
    const user = await db.Userchat.findOne({
        userId: req.body.data.userId,
        clientId: client._id,
    });

    if (!user) {
        //If user doesn't exist, create a new user
        let saveData = await new db.Userchat({
            userId: req.body.data.userId,
            clientId: client._id,
            lastActiveTime: new Date(),
            userState: "OPEN",
            chats: [{
                text: req.body.data.text,
                messageType: "incoming",
                replies: [],
                typing: true,
                session: req.body.data.session,
                node: req.body.data.node,
                createdAt: date,
                date: date,
                node: "root",
            }, ],
        });
        await saveData.save();
        checkNewUserNotificationFlag(client._id, req.body.data.userId)
    } else {
        //If user exist, update the user details
        await db.Userchat.updateOne({ userId: req.body.data.userId, clientId: client._id }, {
            lastActiveTime: new Date(),
            userState: "OPEN",
            $addToSet: {
                chats: {
                    text: req.body.data.text,
                    messageType: "incoming",
                    replies: [],
                    node: req.body.data.node,
                    type_option: true,
                    createdAt: date,
                },
            },
        });
    };

    //Return if chat control is enabled
    if (user && user.chatControl == 1) {
        return res.json({ status: "success", message: "Agent has taken control", control: true })
    }

    const usr = await db.Userchat.findOne({
        userId: req.body.data.userId,
        clientId: client._id,
    });

    if(isHinglish && hinglish_text){
        req.body.data.text=hinglish_text;
    }

    await sendMessagePython(req, res, usr, client, isHinglish);
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}
async function followUp(req, res) {
try{
    let date = new Date(),
        client = await db.Client.findOne({
            clientId: req.body.data.client_id,
        });

    req.body.data.text = req.body.data.text.trim();

    //Client not exist message
    if (!client) {
        return res.json({
            success: false,
            message: "Client does not exists",
            type_option: true,
        });
    }

    //check if node exists
    let trigger=await nodeExists(req.body.data.node,client._id);
    if(!trigger){
        return res.json({
            success: false,
            message: "Node does not exists",
            type_option: true,
        });
    }

    //UserId is invalid
    if (!validateUserid(req.body.data.userId)) {
        console.log("rejected ", req.body.data.userId)
        return res.json({
            success: false,
            message: "UserId is invalid",
            type_option: true,
        });
    }

    const usr = await db.Userchat.findOne({
        userId: req.body.data.userId,
        clientId: client._id,
    });

    await sendMessagePython(req, res, usr, client);
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

async function sendMessagePython(req, res, userData, client, isHinglish=false) {
    //Sending request to python
    let data;
    if (client.middleware != undefined && client.middleware != '') {
        try {
            let pythondata = await axios({
                method: "post",
                url: process.env.pythonUrl,
                data: { data: req.body.data },
            });
            pythondata = pythondata.data

            let slotdata = await axios({
                method: "post",
                url: process.env.findSlotsPythonUrl,
                data: { client_id: req.body.data.client_id, query: req.body.data.text },
            });

            slotdata = slotdata.data
            data = await axios({
                method: "post",
                url: client.middleware,
                data: { data: req.body.data, response: pythondata, slotdata: slotdata },
            });
        } catch (err) {
            console.log("error " + err);
            return res.json({
                success: false,
                message: errorMessage,
                type_option: true,
            });
        }
    } else if (client.isActionEnabled) {
        try {
            let chatId = await db.Userchat.findOne({ userId: req.body.data.userId, clientId: client._id }, { chats: 0 });
            let contextObj = chatId.context ? chatId.context : {},
                session = chatId.session ? chatId.session : { email: "", name: "", phone: "", source: "" };
            let requestData = {
                userId: req.body.data.userId,
                client_id: req.body.data.client_id,
                // category: req.body.category,
                category: client.type ? client.type : "basic",
                node: req.body.data.node,
                story: client.story ? client.story : "True",
                open: client.open ? client.open : "False",
                session: req.body.data.session,
                train: "False",
                unsubsribe: "False",
                update_tree: "False",
                update_story: "False",
                text: req.body.data.text
            };
            requestData = {...requestData, context: contextObj, clientId: chatId.clientId }
            console.log("requestData", requestData)
            data = await dbController.botrequest({ data: requestData });
            console.log('dbController data', data)
            if (data.status == "failed") {
                errorMessage["text"] = data.message;
                return res.json({
                    success: false,
                    message: errorMessage,
                    type_option: true,
                });
            }
        } catch (err) {
            return res.json({
                success: false,
                message: errorMessage,
                type_option: true,
            });
        }
    } else {
        try {
            data = await axios({
                method: "post",
                url: process.env.pythonUrl,
                data: { data: req.body.data },
            });
        } catch (err) {
            console.log("error " + err);
            return res.json({
                success: false,
                message: errorMessage,
                type_option: true,
            });
        }
    }

    data.data = await languageConvert.changeLanguage(data.data);

    if(isHinglish){
        data.data.text=await genAi.convertResponse(data.data.text,data.data.client_id);
    }

    await updateBotResponseIndb(req, res, data.data, userData);
}

//Function to update the bot messages
async function updateBotResponseIndb(req, res, pythonData, userData) {
try{
    
    let dateTime = new Date(),
        date = dateTime;
    date.setSeconds(date.getSeconds() + 2);
    let response_source =
        pythonData.tree !== "false" ?
        "tree" :
        pythonData.intent == "basic_yubo" ?
        "story" :
        "intent";

    if (userData) {
        if (userData.session && pythonData.session) {
            if (
                JSON.stringify(userData.session.email) ===
                JSON.stringify(pythonData.session.email) &&
                JSON.stringify(userData.session.phone) ===
                JSON.stringify(pythonData.session.phone)
            ) {
                pythonData.session.createdDate = pythonData.session.createdDate;
            } else {
                pythonData.session.createdDate = new Date();
            }
        }
        if (pythonData.tree !== "false" && pythonData.tree!=undefined && pythonData.tree.attachment != false && pythonData.tree.attachment != undefined) {
            try {
                let ext = pythonData.tree.attachment.split('.').pop();
                let attachment = { "type": ext, "link": pythonData.tree.attachment }
                let attchat = {
                    text: "Attachment sent",
                    ignoreMsg: false,
                    messageType: "outgoing",
                    attachment: true,
                    attachmentType: attachment.type,
                    attachmentLink: attachment.link,
                    replies: [],
                    type_option: pythonData.type_option,
                    possible_conflict: pythonData.possible_conflict,
                    user_query: pythonData.user_query,
                    intent: pythonData.intent,
                    response_source: response_source,
                    score: pythonData.score,
                    node: pythonData.default_opt,
                    createdAt: date,
                    session: pythonData.session,
                    client_id: pythonData.client_id
                }
                await db.Userchat.updateOne({ userId: req.body.data.userId, clientId: userData.clientId }, {
                    $addToSet: {
                        chats: attchat,
                    },
                });
            } catch (e) {
                console.log("attachment save error", e);
            }
        }
        await db.Userchat.updateOne({ userId: req.body.data.userId, clientId: userData.clientId }, {
            $addToSet: {
                chats: {
                    text: pythonData.text,
                    ignoreMsg: false,
                    messageType: "outgoing",
                    replies: pythonData.replies,
                    products: (pythonData.tree !== "false" && pythonData.tree !== undefined) ? pythonData.tree.products : [],
                    type_option: pythonData.type_option,
                    possible_conflict: pythonData.possible_conflict,
                    intent: pythonData.intent,
                    response_source: response_source,
                    score: pythonData.score,
                    // node: req.body.data.node,
                    node: pythonData.default_opt,
                    createdAt: date,
                },
            },
            $set: {
                session: pythonData.session,
                node: pythonData.default_opt,
                date: dateTime,
            },
        });
    }

    //checking for the webhook and the action key to hit the hubspot API
    let resp = ("webhook" in req.body && (req.body.webhook.action == "true" || req.body.webhook.action)) ? await checkForWebhook(req.body) : false;
    if(pythonData.session!=undefined){
        pythonData.session["visitor_id"] = resp;
    }
    
    //Emit the bot messages
    let pythonDataObj = {
        clientId: pythonData.clientId,
        userId: pythonData.userId,
        botmessage: pythonData.text,
        messagetype: "botmessage",
        name: pythonData.session ? (pythonData.session.name ? pythonData.session.name : "Unknown") : "Unknown",
        date: currentDate,
        time: currentTime
    }
    io.sockets.emit("botmessage", pythonDataObj);
    // pythonData = await languageConvert.changeLanguage(pythonData);

    return res.json(pythonData);
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

//webhook checking for the respective client and calling the create and update API based on Visitor's id
async function checkForWebhook(data) {
try{
    let webhookDetails = await db.Webhook.findOne({
        clientId: data.data.client_id,
    });

    if (webhookDetails) {
        //Call the update API, if visitor's id is present in the request.body webhook
        if (data.webhook.data.visitor_id && (data.webhook.data.visitor_id != false)) {
            console.log('vid ', data.webhook.data.visitor_id);
            await Hubspot.updateCntctOfflineByVid(webhookDetails.webhookApiKey, data.webhook.data);
            return data.webhook.data.visitor_id;
        } else { //create new entry in hubspot and return visitor id
            console.log('novid');
            let resp = await Hubspot.createNewCntctOffline(webhookDetails.webhookApiKey, data.webhook.data);
            return (resp && resp.vid) ? resp.vid : false;
        }
    }
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

async function pythonApi(req, res, reqData) {
    let data;
    try {
        data = await axios({
            method: "post",
            url: process.env.pythonUrl,
            data: { data: reqData },
        });
        // console.log("data", data.data)
        return {
            success: true,
            message: "Data received",
            data: data.data
        };
    } catch (err) {
        console.log("error " + err);
        return {
            success: false,
            message: errorMessage,
            type_option: true,
        };
    }
}

async function botLanguage(req, res) {
    try {
        const clientData = await db.Client.findOne({ clientId: req.body.clientId }, { locales: 1 })
        let clientsLanguages = clientData.locales;
        let allLanguage = await db.Botlanguage.find({});
        let filtereddata = await allLanguage.filter(ele => {
            return clientsLanguages.includes(ele.languageCode);
        })
        res.status(200).json({
            statusCode: 200,
            status: "success",
            message: "Data found successfully.",
            data: filtereddata,
        });
    } catch (error) {
        res.status(500).json({
            status: "failed",
            message: "Data not found",
            data: error,
        });
    }
}


async function botGenAi(req, res) {
    try {
        const clientData = await db.Client.findOne({ clientId: req.body.clientId }, { genAiConfig: 1 , isGenAiEnabled: 1})
        let genAiConfig = clientData.genAiConfig;
        if(genAiConfig!=undefined || isGenAiEnabled==true){
            res.status(200).json({
                statusCode: 200,
                status: "success",
                message: "Data found successfully.",
                data: genAiConfig,
            });
        } else {
            res.status(200).json({
                statusCode: 200,
                status: "failed",
                message: "Generative AI not enabled for this bot."
            });
        }
    } catch (error) {
        res.status(500).json({
            status: "failed",
            message: "Data not found",
            data: error,
        });
    }
}


function validateUserid(userId) {
    try {
        if (userId.indexOf('.') !== -1) {
            return false;
        } else if (userId.split('_')[2].indexOf('16') == 0 && userId.split('_')[2].length == 13) {
            return false;
        } else {
            return true;
        }
    } catch (err) {
        console.log("validateUserid error", err);
        return true;
    }

}

//to send otification on whatsApp whenever new user interacts on bot. It checks whatsappNotification flag in client collection
async function checkNewUserNotificationFlag(clientId, userId) {
    try {
        console.log("checkNewUserNotificationFlag", clientId)
        let clientData = await db.Client.findOne({ "clientId": clientId })
        if (clientData) {
            if (clientData.emailNotification == 1) {
                console.log("client email", clientData.email)
                email = clientData.email
                let html = '<div style="width:400px;border:thin solid #dadce0;border-radius:10px;padding:5px 0px" align="center" ><h2 style="color:blue">Hello  ' + clientData.name + '</h2><h2 style="color:blue">New user is interacting on chabot</h2></div>';;
                sendMail(email, html, "New client");
            } else if (clientData.whatsappNotification == true) {
                phone = clientData.phone
                let data = {
                    clientId: clientId,
                    nodeName: "yubo_new_user_start"
                }
                io.sockets.to(userId).emit('yubo_new_user_start', data);
            } else {
                return
            }
        } else {
            console.log("No client available having clientId = ", clientId)
        }
    } catch (err) {
        console.log("error " + err);
        return res.json({
            success: false,
            message: errorMessage,
            type_option: true,
        });
    }
}


//check if the nodeExists before calling followup
async function nodeExists(node, cid){
    let nodelist=[];
    let nodes;
    let status=false;
    try{
        let tree = await db.Tree.findOne({ client: cid });
        if (tree) {
            nodes = !tree.jsonOfTree.DTree
              ? tree.jsonOfTree
                ? JSON.parse(tree.jsonOfTree).DTree
                : ""
              : tree.jsonOfTree.DTree;
        }
        console.log(nodes)
        nodes.forEach(function (element) {
            nodelist.push(element.node_name);
            nodelist.push(element.node_id)
        });
        if(nodelist.includes(node)){
            status=true;
        }
    } catch(err) {
        console.log("nodeExists error ",err);
    }
    return status;
}
