const db = require("../models/all-models");
const fs = require('fs');

module.exports = {
    saveContactNode: saveContactNode,
    saveTreeQuery: saveTreeQuery,
    uploadConversationalMedia: uploadConversationalMedia
};

async function saveContactNode(clientId) {
   let client = await db.Client.findOne({ clientId: clientId });
  let treeData = {
      clientId: client._id,
      tree: '{\n' + ' "operators": {\n' + ' "operator6": {\n' + ' "top": 40,\n' + ' "left": 40,\n' + ' "node_type": "contact",\n' + ' "properties": {\n' + ' "title": "start",\n' + ' "question": "Please fill out the form below. Our experts will get back to you, in case we loose connection on this chat here.",\n' + ' "options": [],\n' + ' "form": "[{\\"type\\":\\"text\\",\\"required\\":true,\\"label\\":\\"Name\\",\\"placeholder\\":\\"Your name\\",\\"className\\":\\"form-control\\",\\"name\\":\\"fname\\",\\"access\\":false,\\"subtype\\":\\"text\\"},{\\"type\\":\\"text\\",\\"subtype\\":\\"email\\",\\"required\\":true,\\"label\\":\\"Email\\",\\"placeholder\\":\\"Your Email Id\\",\\"className\\":\\"form-control\\",\\"name\\":\\"email\\",\\"access\\":false},{\\"type\\":\\"number\\",\\"required\\":true,\\"label\\":\\"Phone\\",\\"placeholder\\":\\"Your Phone Number\\",\\"className\\":\\"form-control\\",\\"name\\":\\"phone\\",\\"access\\":false},{\\"type\\":\\"button\\",\\"subtype\\":\\"submit\\",\\"label\\":\\"Submit\\",\\"className\\":\\"btn-success btn\\",\\"name\\":\\"button-1611153690819\\",\\"value\\":\\"Submit\\",\\"access\\":false,\\"style\\":\\"success\\"}]",\n' + ' "webhook": "",\n' + ' "followup": {\n' + ' "nodeName": "",\n' + ' "delayInMs": "0"\n' + ' },\n' + ' "inputs": {\n' + ' "input_1": {\n' + ' "label": "In1"\n' + ' }\n' + ' },\n' + ' "outputs": {\n' + ' "output_1": {\n' + ' "label": "O1",\n' + ' "answer": ""\n' + ' }\n' + ' },\n' + ' "type_opt": true,\n' + ' "products": []\n' + ' }\n' + ' }\n' + ' },\n' + ' "links": {},\n' + ' "settings": {\n' + ' "maxFields": 20\n' + ' },\n' + ' "operatorTypes": {}\n' + '}',
      btree: '{"DTree":[{"node_id":"n6","node_name":"start","text":"Please fill out the form below. Our experts will get back to you, in case we loose connection on this chat here.","options":[],"type_of_node":"contact","type_opt":true,"default":null,"form":[{\"type\":\"text\",\"required\":true,\"label\":\"Name\",\"placeholder\":\"Your name\",\"className\":\"form-control\",\"name\":\"fname\",\"access\":false,\"subtype\":\"text\"},{\"type\":\"text\",\"subtype\":\"email\",\"required\":true,\"label\":\"Email\",\"placeholder\":\"Your Email Id\",\"className\":\"form-control\",\"name\":\"email\",\"access\":false},{\"type\":\"number\",\"required\":true,\"label\":\"Phone\",\"placeholder\":\"Your Phone Number\",\"className\":\"form-control\",\"name\":\"phone\",\"access\":false},{\"type\":\"button\",\"subtype\":\"submit\",\"label\":\"Submit\",\"className\":\"btn-success btn\",\"name\":\"button-1611153690819\",\"value\":\"Submit\",\"access\":false,\"style\":\"success\"}],"webhook":"","followup":{"nodeName":"","delayInMs":"0"},"products":[]}]}'
  }
    await saveTreeQuery(treeData)
}

async function saveTreeQuery(treeData){
   let saveData = new db.Tree({
        client: treeData.clientId,
        tree: treeData.tree,
        jsonOfTree: treeData.btree,
      });
    await saveData.save();
}

async function uploadConversationalMedia(req,res) {  

    
  await AWS.config.update({
      accessKeyId: config.s3Linode.accessKeyId,
      secretAccessKey: config.s3Linode.secretAccessKey,
      region: config.s3Linode.region,
      s3BucketEndpoint: config.s3Linode.s3BucketEndpoint,
  });
  var s3 = await new AWS.S3({ endpoint: config.s3Linode.url });

  const clientId = req.user.clientId
  const operatorId = req.body.operatorId
  try{
      const content =await fs.readFileSync(req.file.path);
      var filetype = req.file.mimetype
      var filesize = req.file.size
       fileType = filetype.split("/");
      let fileExt = req.file.originalname.split('.').pop(); 

      //var filename= `${clientId}_${operatorId}.${fileExt}`;
      var filename= `${operatorId}.${fileExt}`;

      if(filesize<=16777216) {
              var params = {
              Bucket: `yugasabot/${clientId}/Media/Admin_files/conv_flow`,
              Key: filename,
              Body: content,
              ContentType: req.file.mimetype,
              ACL:'public-read'
          }
          } else {
              return res.json({
                  message: 'Please select file of size less than 16 MB',
                  statusCode: 500,
                  status: false,
                  error  : err
              });
        }

        await s3.upload(params, (err, result) => {
      
          if (err) {
              return res.json({
                  message: 'Error occured during file upload on s3 server',
                  statusCode: 500,
                  status: false,
                  error  : err
              });
          } else {

              fs.unlink(req.file.path,function(errDel){
                  if(errDel) console.log('Error during unlink file ')
                  console.log('file deleted successfully')
              });

              return res.json({
                  message: 'File upload successfully',
                  statusCode: 200,
                  status: true,
                  s3_url : result.Location
              })
          }            
        }) 

  }catch(error){
      console.log('error in try catch block  ',error);
      return res.json ({
          success: false,
          statusCode : 500,
          message: 'file format and size is not supporting in s3',
          error: error
      });
  }
}
