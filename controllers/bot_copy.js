const express = require("express"),
  yubo = require("../controllers/yubo.js"),
  db = require("../models/all-models"),
  jwt = require("jsonwebtoken"),
  bodyParser = require("body-parser"),
  axios = require("axios"),
  Fcm = require("../middleware/fcm"),
  bot = express.Router(),
  multer = require("multer"),
  emailController = require("../controllers/emailverification"),
  sendMail = require("../middleware/sendChat.js"),
  botApi = require("./botAPI_copy");

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/uploads");
  },
  filename: function (req, file, cb) {
    let extArray = file.mimetype.split("/");
    let extension = extArray[extArray.length - 1];
    cb(null, file.fieldname + "-" + Date.now() + "." + extension);
  },
});

var upload = multer({ storage: storage });

/* GET users listing. */
bot.get("/", function (req, res, next) {
  res.redirect("http://yugasa.org/");
});

//caal connect event
bot.post("/connect", function (req, res, next) {
  global.io.emit("user", req.body);
  return res.json(req.body);
});

let errorMessage = {
  default_opt: "False",
  replies: [],
  text:
    "Please come back and chat with me after some time. Right now I am taking some red and blue pills for maintenance.",
  type_option: true,
};

bot.post(
  "/fileUpload",
  upload.single("fileList"),
  async function (req, res, next) {
    req.body.clientId = req.body.fileList[1];
    req.body.userId = req.body.fileList[0];
    let client = await db.Client.findOne({ clientId: req.body.clientId });
    if (!client) {
      return res.json({
        success: false,
        message: errorMessage,
        type_option: true,
      });
    }
    let user = await db.Userchat.findOne(
      { userId: req.body.userId, clientId: client._id },
      { chats: 0 }
    );
    let userCount = await db.Userchat.find({ clientId: client._id }).count();
    var msg = "";

    if (!user) {
      let userUpload = new db.Userchat({
        userId: req.body.userId,
        clientId: client._id,
        name: "Visitor" + userCount(req.body.clientId),
        fileUploaded: req.file,
      });
      await userUpload.save();
    } else {
      let updateUserUpload = await db.Userchat.updateOne(
        { userId: req.body.userId },
        { $push: { fileUploaded: req.file } }
      );
    }
    return res.send("success");
  }
);

/** This module used for user chat conversation*/
bot.post("/sendMessage", async function (req, res, next) {
  let client = await botApi.checkForClient(req.body.clientId),
      user = await botApi.checkForUser(req.body.userId, req.body.clientId);

  //start of packaging baba
  if (req.body.clientId == "packagingBaba_chatbot") {
    await packagingBaba(req, res, user, client);
  }
  //end of packaging baba
  if (!req.body.node) {
    req.body.node = user.node || "False";
  }
  let requestData = {
    userId: req.body.userId,
    client_id: req.body.clientId,
    // category: req.body.category,
    category: client.type ? client.type : "basic",
    node: req.body.node,
    basic: "True",
    session: req.body.session,
    train: "False",
    unsubsribe: "False",
    update_tree: "False",
    update_story: "False",
  };

  if (!user) {
    requestData.text = "restart yubo";
    requestData.node = req.body.node ? req.body.node : "False";
    let userMsg = new db.Userchat({
      userId: req.body.userId,
      clientId: client._id,
      name: "Visitor" + userCount(req.body.clientId),
      location: req.body.location,
      chats: [
        {
          text: req.body.text,
          messageType: "incoming",
          replies: [],
          typing: true,
          ignoreMsg: false,
          session: req.body.session,
          node: req.body.node,
          url: req.body.url,
          createdAt: date,
        },
      ],
    });
    await userMsg.save();
    axios({
      method: "POST",
      url:
        req.body.clientId == "farmitra_chatbot_hi"
          ? process.env.hindiYubo
          : process.env.pythonUrl,
      data: { data: requestData },
    }).then(async function (response) { });
  } else {
    let updateChat = await db.Userchat.updateOne(
      { userId: req.body.userId, clientId: client._id },
      {
        $addToSet: {
          chats: {
            text: req.body.text,
            messageType: "incoming",
            ignoreMsg: false,
            replies: [],
            node: req.body.node,
            url: req.body.url,
            type_option: true,
            createdAt: date,
          },
        },
      }
    );
  }
  let chatId = await db.Userchat.findOne(
    { userId: req.body.userId, clientId: client._id },
    { chats: 0 }
  );
  requestData.session = chatId.session;
  requestData.text = req.body.text;

  global.io.emit(client._id, {
    _id: chatId._id,
    name: chatId.name,
    userId: req.body.userId,
    text: req.body.text,
    node: req.body.node,
    url: req.body.url,
    messageType: "incoming",
    replies: [],
    typing: true,
    createdAt: date,
  });
  Fcm.sendMessage(client.deviceId, {
    _id: chatId._id,
    name: chatId.name,
    userId: req.body.userId,
    text: req.body.text,
    node: req.body.node,
    url: req.body.url,
    messageType: "incoming",
    replies: [],
    typing: true,
    createdAt: date,
  });

  if (chatId.chatControl == 1) {
    return res.json({ success: false, chatControl: 1 });
  }
  try {
    var data = await axios({
      method: "post",
      url:
        req.body.clientId == "farmitra_chatbot_hi"
          ? process.env.hindiYubo
          : process.env.pythonUrl,
      data: { data: requestData },
    });
  } catch (err) {
    console.log("error " + err);
    return res.json({
      success: false,
      message: errorMessage,
      type_option: true,
    });
  }

  date.setSeconds(date.getSeconds() + 2);

  if (chatId.session) {
    if (
      JSON.stringify(chatId.session.email) ===
      JSON.stringify(data.data.session.email) &&
      JSON.stringify(chatId.session.phone) ===
      JSON.stringify(data.data.session.phone)
    ) {
      data.data.session.createdDate = data.data.session.createdDate;
    } else {
      data.data.session.createdDate = new Date();
    }
  }

  let dateTime = new Date();
  if (user) {
    let updateChat = await db.Userchat.updateOne(
      { _id: user._id },
      {
        $set: {
          session: data.data.session,
          node: data.data.default_opt,
          date: dateTime,
        },
      }
    );
  }

  let response_source =
    data.data.tree !== "false"
      ? "tree"
      : data.data.intent == "basic_yubo"
        ? "story"
        : "intent";

  let updateOpt = await db.Userchat.updateOne(
    { userId: req.body.userId, clientId: client._id },
    {
      $addToSet: {
        chats: {
          text: data.data.text,
          ignoreMsg: false,
          messageType: "outgoing",
          replies: data.data.replies,
          type_option: data.data.type_option,
          possible_conflict: data.data.possible_conflict,
          user_query: data.data.user_query,
          intent: data.data.intent,
          response_source: response_source,
          score: data.data.score,
          node: req.body.node,
          createdAt: date,
        },
      },
    }
  );

  //Hubspot
  let updateHubProp = false;
  if (user) {
    if (
      req.body.hubprop &&
      req.body.hubprop != "" &&
      req.body.hubprop != null &&
      user.session &&
      user.session.vid
    ) {
      updateHubProp = await updateHubProperties(
        req.body.hubprop,
        req.body.text,
        data.data.client_id,
        user.session.vid
      );
    }
  }

  //Flag to indicate the form has submitted or not before by the user
  let formAlreadyFilled = false;
  if (
    data.data.tree &&
    data.data.tree.form &&
    user && user.formdata &&
    user.formdata.hasOwnProperty(data.data.tree.node_name)
  ) {
    data.data["formValues"] = await formDataLabeling(
      client,
      user.formdata[data.data.tree.node_name],
      data.data.tree.node_name
    ).then(function (value) {
      return objToString(value); //To convert object in string formate
    });
    formAlreadyFilled = true;
  }
  data.data["updateHubProp"] = updateHubProp;
  return res.json({
    success: true,
    formAlreadyFilled: formAlreadyFilled,
    message: data.data,
  });
});

bot.post("/followUp", async function (req, res, next) {
  var date = new Date();
  let client = await db.Client.findOne({ clientId: req.body.clientId });
  if (!client) {
    return res.json({
      success: false,
      message: errorMessage,
      type_option: true,
    });
  }

  const user = await db.Userchat.findOne(
    { userId: req.body.userId, clientId: client._id },
    { chats: 0 }
  );

  let userCount = await db.Userchat.find({ clientId: client._id }).count();

  if (!req.body.node) {
    req.body.node = user.node || "False";
  }

  let requestData = {
    userId: req.body.userId,
    client_id: req.body.clientId,
    // category: req.body.category,
    category: client.type ? client.type : "basic",
    node: req.body.node,
    basic: "True",
    session: req.body.session,
    train: "False",
    unsubsribe: "False",
    update_tree: "False",
    update_story: "False",
  };

  // if (!user) {
  //   requestData.text = "restart yubo";
  //   requestData.node = 'False';
  //   let userMsg = new db.Userchat({
  //     'userId': req.body.userId,
  //     'clientId': client._id,
  //     'name': "Visitor" + userCount,
  //     'chats': [{ text: req.body.text, messageType: "incoming", replies: [], typing: true, ignoreMsg: false, session: req.body.session, node: req.body.node, createdAt: date }]
  //   }); await userMsg.save();
  //   axios({
  //     method: 'POST',
  //     url: process.env.pythonUrl,
  //     data: { data: requestData }
  //   }).then(async function (response) {

  //   });

  // } else {
  //   let updateChat = await db.Userchat.updateOne({ userId: req.body.userId, clientId: client._id }, {
  //     $addToSet: {
  //       chats: {
  //         text: req.body.text, messageType: "incoming", ignoreMsg: false, replies: [], node: req.body.node, type_option: true, createdAt: date
  //       }
  //     }
  //   });
  // }

  let chatId = await db.Userchat.findOne(
    { userId: req.body.userId, clientId: client._id },
    { chats: 0 }
  );
  requestData.session = chatId.session;
  requestData.text = req.body.text;

  global.io.emit(client._id, {
    _id: chatId._id,
    name: chatId.name,
    userId: req.body.userId,
    text: req.body.text,
    node: req.body.node,
    messageType: "incoming",
    replies: [],
    typing: true,
    createdAt: date,
  });
  Fcm.sendMessage(client.deviceId, {
    _id: chatId._id,
    name: chatId.name,
    userId: req.body.userId,
    text: req.body.text,
    node: req.body.node,
    messageType: "incoming",
    replies: [],
    typing: true,
    createdAt: date,
  });

  if (chatId.chatControl == 1) {
    return res.json({ success: false, chatControl: 1 });
  }

  try {
    var data = await axios({
      method: "post",
      url:
        req.body.clientId == "farmitra_chatbot_hi"
          ? process.env.hindiYubo
          : process.env.pythonUrl,
      data: { data: requestData },
    });
  } catch (err) {
    return res.json({
      success: false,
      message: errorMessage,
      type_option: true,
    });
  }

  date.setSeconds(date.getSeconds() + 2);

  if (chatId.session) {
    if (
      JSON.stringify(chatId.session.email) ===
      JSON.stringify(data.data.session.email) &&
      JSON.stringify(chatId.session.phone) ===
      JSON.stringify(data.data.session.phone)
    ) {
      data.data.session.createdDate = data.data.session.createdDate;
    } else {
      data.data.session.createdDate = new Date();
    }
  }

  let response_source =
    data.data.tree !== "false"
      ? "tree"
      : data.data.intent == "basic_yubo"
        ? "story"
        : "intent";

  let dateTime = new Date();
  if (user) {
    let updateChat = await db.Userchat.updateOne(
      { _id: user._id },
      {
        $set: {
          session: data.data.session,
          node: data.data.default_opt,
          date: dateTime,
        },
      }
    );
  }

  let updateOpt = await db.Userchat.updateOne(
    { userId: req.body.userId, clientId: client._id },
    {
      $addToSet: {
        chats: {
          text: data.data.text,
          ignoreMsg: false,
          messageType: "outgoing",
          replies: data.data.replies,
          type_option: data.data.type_option,
          possible_conflict: data.data.possible_conflict,
          intent: data.data.intent,
          response_source: response_source,
          score: data.data.score,
          node: req.body.node,
          createdAt: date,
        },
      },
    }
  );

  //Hubspot
  let updateHubProp = false;
  if (user) {
    if (
      req.body.hubprop &&
      req.body.hubprop != "" &&
      req.body.hubprop != null &&
      user.session &&
      user.session.vid
    ) {
      updateHubProp = await updateHubProperties(
        req.body.hubprop,
        req.body.text,
        data.data.client_id,
        user.session.vid
      );
    }
  }

  //Flag to indicate the form has submitted or not before by the user
  let formAlreadyFilled = false;
  if (
    data.data.tree.form &&
    user.formdata.hasOwnProperty(data.data.tree.node_name)
  ) {
    formAlreadyFilled = true;
  }

  data.data["updateHubProp"] = updateHubProp;
  return res.json({
    success: true,
    formAlreadyFilled: formAlreadyFilled,
    message: data.data,
  });
});

/** This module used for user chat conversation*/
bot.post("/launch", async function (req, res, next) {
  let client = await db.Client.findOne({ clientId: req.body.clientId });
  let requestData = {
    userId: "bf235b58-128d-0b62-a40f-bbd0952109b6",
    client_id: req.body.clientId,
    category: client.type ? client.type : "basic",
    node: "launch",
    basic: "True",
    session: { email: "", name: "", phone: "", source: "" },
    train: "False",
    unsubsribe: "False",
    update_tree: "False",
    update_story: "False",
    text: "",
  };
  // pythonUrl: 'http://172.105.42.134:5000/yugasa'
  try {
    var data = await axios({
      method: "post",
      url:
        req.body.clientId == "farmitra_chatbot_hi"
          ? process.env.hindiYubo
          : process.env.pythonUrl,
      data: { data: requestData },
    });
  } catch (err) {
    return res.json({
      success: false,
      message: errorMessage,
      type_option: true,
    });
  }

  return res.json({ success: true, message: data.data });
});

//When contact form gets submitted from chatbot
bot.post("/formSubmit", async function (req, res) {
  let client = await db.Client.findOne({ clientId: req.body.clientId }),
    webhookResponse = req.body.formData.hutk ? req.body.formData.hutk : "",
    webhookDetails = "";

  if (!client) {
    return res.json({
      success: false,
      message: errorMessage,
      type_option: true,
    });
  }
  //To integrate with any webhook
  if (req.body.webhook) {
    webhookDetails = await db.Webhook.findOne({
      clientId: req.body.clientId,
      webhookName: req.body.webhook,
    });
    // if (webhookDetails) {
    //   webhookResponse = await callToWebhook(webhookDetails, req.body.formData);
    // }
  }

  let key = Object.keys(req.body.formobj)[0];
  // let formdata1 = formDataLabeling(client, req.body.formobj[key], key);
  let user = await db.Userchat.findOne(
    { userId: req.body.userId, clientId: client._id },
    { chats: 0 }
  );
  //To replace the form name key with label, formDataLabeling function returns the promise
  let formdata = await formDataLabeling(
    client,
    req.body.formobj[key],
    key
  ).then(function (value) {
    return objToString(value); //To convert object in string formate
  });

  if (user) {
    if (user.formdata.hasOwnProperty(key)) {
      for (key1 in req.body.formobj[key]) {
        if (user.formdata[key][key1]) {
          user.formdata[key][key1] = req.body.formobj[key][key1];
        } else {
          Object.assign(user.formdata[key], req.body.formobj[key]);
        }
      }
      // user.formdata[key] = req.body.formobj[key];
    } else {
      Object.assign(user.formdata, req.body.formobj);
    }
  }

  if (!user) {
    let formData = new db.Userchat({
      userId: req.body.userId,
      clientId: client._id,
      location: req.body.location,
      session: {
        email: req.body.formData.email,
        name: req.body.formData.fname,
        phone: req.body.formData.phone,
        createdDate: new Date(),
        vid: webhookResponse ? webhookResponse : "",
      },
      formdata: req.body.formobj,
      chats: [
        {
          text: formdata,
          messageType: "outgoing",
          createdAt: new Date(),
        },
      ],
      //  (webhookResponse && webhookResponse.vid) ? webhookResponse.vid : null
    });

    await formData.save();
  } else {
    await db.Userchat.updateOne(
      { userId: req.body.userId, clientId: client._id },
      {
        $addToSet: {
          chats: {
            text: formdata,
            messageType: "outgoing",
            createdAt: new Date(),
          },
        },
        $set: {
          session: {
            email: req.body.formData.email,
            name: req.body.formData.fname,
            phone: req.body.formData.phone,
            createdDate: new Date(),
            vid: webhookResponse ? webhookResponse : "",
          },
          formdata: user.formdata,
        },
      }
    );
  }

  key == "start" ? await emailController.leadNotificationMail(client.email, formdata, req.body.formData.fname) : "";
  return res.json({
    success: true,
    data: webhookDetails ? webhookDetails : "",
    message:
      "Thanks for sharing the details.",
  });
});

/** This module used for user chat history*/
bot.post("/chatHistory", async function (req, res, next) {
  let client = await db.Client.findOne({ clientId: req.body.clientId });
  let userChat = await db.Userchat.findOne({
    userId: req.body.userId,
    clientId: client._id,
  });
  if (userChat) {
    return res.json({ success: true, message: userChat.chats });
  } else {
    let treeData = await db.Tree.findOne({ client: client._id });
    // To show the contact form for new user on bot
    if (treeData) {
      treeData = JSON.parse(treeData.jsonOfTree).DTree;
      treeData.forEach(function (item) {
        if (item.node_name == "start") {
          return res.json({
            success: true,
            formData: item,
          });
        }
      });
    } else {
      return res.json({ success: true, message: false });
    }
  }
});

function getClient(req) {
  var data = req.headers["host"].split(";");
  var client = "";
  if (data[0] == "yugasa.com") {
    client = "yugasa_chatbot";
  } else {
    client = "amit_chatbot";
  }
  return client;
}

async function updateHubProperties(propertyName, value, clientId, vid) {
  const getQuery = await db.Integrations.findOne({ clientId: clientId });
  if (getQuery) {
    if (getQuery.name == "Hubspot") {
      let data = {
        fields: [
          {
            name: propertyName,
            value: value,
          },
        ],
        context: {
          hutk: vid,
          // pageUri: "www.example.com/page",
          // pageName: "Example page",
        },
      };
      return {
        authKey: getQuery.apiKey,
        data: data
      }
    }
  }
}

bot.post("/yuboFirstInstance", yubo.yuboFirstInstance);

async function formDataLabeling(client, formdata1, key) {
  if (!client) return;
  let treeData = await db.Tree.findOne({ client: client._id });
  let newobj = JSON.parse(treeData.jsonOfTree),
    customformdata;
  for (i = 0; i < newobj.DTree.length; i++) {
    if (newobj.DTree[i].node_name == key) {
      customformdata = newobj.DTree[i].form;
      break;
    }
  }

  let customdata =
    typeof customformdata == "string"
      ? JSON.parse(customformdata)
      : customformdata,
    newdataobj = {};
  if (customdata) {
    for (const key in formdata1) {
      for (const iterator of customdata) {
        if (key === iterator.name) {
          if (iterator.label == "Submit" || iterator.label == "Button") {
            continue;
          } else {
            newdataobj[iterator.label] = formdata1[key];
          }
        }
      }
    }
  }
  return newdataobj;
  /************************label code******************************************************* */
}

function objToString(obj) {
  var str = "";
  for (var label in obj) {
    if (obj.hasOwnProperty(label)) {
      str += label + " : " + obj[label] + "<br>";
    }
  }
  return str;
}

/***** packaging baba *****/
async function packagingBaba(req, res, user, client) {
    let date = new Date();
  if (!user) {
    let userMsg = new db.Userchat({
      userId: req.body.userId,
      clientId: client._id,
      name: "Visitor" + userCount(req.body.clientId),
      chats: [
        {
          text: req.body.text,
          messageType: "incoming",
          replies: [],
          typing: true,
          session: req.body.session,
          node: req.body.node,
          url: req.body.url,
          createdAt: date,
          date: date,
        },
      ],
    });
    await userMsg.save();
    let updateChat = await db.Userchat.updateOne(
      { userId: req.body.userId, clientId: client._id },
      { $set: { node: "root" } }
    );
  } else {
    let updateChat = await db.Userchat.updateOne(
      { userId: req.body.userId, clientId: client._id },
      {
        $addToSet: {
          chats: {
            text: req.body.text,
            messageType: "incoming",
            replies: [],
            node: req.body.node,
            url: req.body.url,
            type_option: true,
            createdAt: date,
          },
        },
      }
    );
  }
  let userDetails = await db.Userchat.findOne(
    { userId: req.body.userId, clientId: client._id },
    { chats: 0 }
  );
  var data1 = client.btree.DTree.filter(
    (x) => x.node_id === userDetails.node
  )[0];
  let updateChat = await db.Userchat.findOneAndUpdate(
    { _id: userDetails._id },
    { $set: { node: data1.default } }
  );

  let data = {
    client_id: req.body.clientId,
    default_opt: "False",
    node: req.body.node,
    replies: data1.options,
    session: { email: "", name: "", phone: "", source: "" },
    text: data1.text,
    type_option: data1.type_opt,
    userId: req.body.userId,
  };
  if (data1.node_id == "contact_number") {
    data.typeNumber = "number";
  }

  let updateOpt = await db.Userchat.updateOne(
    { userId: req.body.userId, clientId: client._id },
    {
      $addToSet: {
        chats: {
          text: data1.text,
          messageType: "outgoing",
          replies: data1.options,
          type_option: data1.type_opt,
          node: req.body.node,
          createdAt: date,
          date: date,
        },
      },
    }
  );

  return res.json({ success: true, message: data });
}

async function userCount(clientId){
    return await db.Userchat.find({ clientId: clientId }).count();
}

module.exports = bot;
