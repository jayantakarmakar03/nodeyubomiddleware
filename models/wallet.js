const mongoose = require("mongoose");
Schema = mongoose.Schema 

const WalletSchema = new Schema({
    clientId:{
        type:String,
    },
    balance: {
            type: mongoose.Decimal128,
            required: true,
            default: 0.00
        },
    // orderId:{
    //     type:String
    // },
    // customer_id:{
    //     type:String
    // },
    // customer_name:{
    //     type:String
    // },
    // customer_email:{
    //     type:String
    // },
    // customer_phone:{
    //     type:String
    // },
    // payment_session_id:{
    //     type:String
    // },
    'createdAt': { type: Date, default: Date.now },
    'updatedAt': { type: Date, default: Date.now }
});

const Wallet = mongoose.model("Wallet",WalletSchema);
module.exports = Wallet