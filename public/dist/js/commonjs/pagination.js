
  var current_page = 1, records_per_page = 10, data = document.getElementsByClassName("tr2");

  function firstPage() {
    changePage(1);
  }
  
  function lastPage() {
    changePage(numPages())
  }
  
  function prevPage() {
    if (current_page > 1) {
      current_page--;
      changePage(current_page);
    }
  }
  
  function nextPage() {
    if (current_page < numPages()) {
      current_page++;
      changePage(current_page);
    }
  }
  
  
  function changePage(page) {
    current_page = page;
    var btn_next = document.getElementById("btn_next");
    var btn_prev = document.getElementById("btn_prev");
  
  
    var totalPagenum = $("li.page").length;
    if (totalPagenum > 5) {
      $("li.page").hide();
      for (var n = page; n <= page + 4; n++) {
        $("li.page:nth-child(" + n + ")").show();
      }
    }
  
    if (page == 1) {
      $("#btn_first").hide();
      $("#btn_prev").addClass("active")
    } else {
      $("#btn_first").show();
      $("#btn_prev").removeClass("active")
    }
  
    if (page == numPages()) {
      $("#btn_last").hide();
      $("#btn_next").addClass("active");
    } else {
      $("#btn_last").show();
      $("#btn_next").removeClass("active");
    }
  
    $(".page").removeClass("active")
    $("#p" + page).addClass("active")
    // Validate page
    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();
  
  
    $('.tr2').each(function () {
      var indx = $(this).attr('data-index');
  
      if (indx > ((page - 1) * records_per_page) && indx <= (page * records_per_page)) {
        $(this).show();
      } else {
        $(this).hide();
      }
  
    });
  
  }
  
  function numPages() {
    return Math.ceil(data.length / records_per_page);
  }
  
  window.onload = function () {
  
    $(".pagination").append(' <li id="btn_prev"><a href="javascript:prevPage()"><span class="glyphicon glyphicon-chevron-left"></span></a></li>');
    $(".pagination").append(' <li id="btn_first"><a href="javascript:firstPage()">First</a></li>');
    for (var i = 1; i <= numPages(); i++) {
      $(".pagination").append(' <li class="page" id="p' + i + '" onclick="changePage(' + i + ')"><a href="#">' + i + '</a></li>');
    }
    $(".pagination").append(' <li id="btn_last"><a href="javascript:lastPage()">Last</a></li>');
    $(".pagination").append('<li id="btn_next"><a href="javascript:nextPage()"><span class="glyphicon glyphicon-chevron-right"></span></a></li>');
  
    changePage(1);
  };
  