var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
    mongoose.connect(require('./connection-string'));
}

var newSchema = new Schema({
    'clientId': { type: String },
    'name': { type: String },
    'host': { type: String },
    'email': { type: String },
    'deleteRequest': { type: Boolean, default: false },
    'phone': { type: Number },
    'accepted_trmscndtn': { type: String, default: 'no' },
    'email_verified': { type: Boolean, default: false }, //To verify email while registeration true means verified
    'email_token': { type: String }, //Token to validate link sent for email verification
    'category': { type: String },
    'WAcampaignSentStatus': { type: Boolean, default: false },
    'status': { type: String },
    'type': { type: String },
    'story': { type: String, default: "True" },
    'open': { type: String, default: "False" },
    'userToken': { type: String },
    'deviceId': { type: String },
    'devType': { type: String },
    'password': { type: String },
    'queueSize': { type: Number, default: 45 },
    'codeNodeStatus': { type: Object },
    'delayTime': { type: Number, default: 1000 }, //1000ms = 1sec
    "api_secret": { type: String },
    'blockStatus': { type: String, default: 0 },
    'otp': { type: Number },
    'profile_img': { type: String },
    'timezone': { type: String, default: "Asia/Kolkata" },
    'location': { type: Object },
    //0 for website and 1 for Yubo App 
    'token': { type: String },
    'campaignconfig': { type: Object }, //for campaign feature
    'gupShupConfig': { type: Object }, //for whatsapp campaign feature
    'genAiConfig':{ type: Object }, 
    'agentConfig': { type: Object }, //for human agent feature
    "api_keys": [{
        token: "string",
        timestamp: Date,
        channel: "string"
    }],
    'reset_token': { type: String }, //for forgot password
    "tree": { type: Object },
    "btree": { type: Object }, //Branching tree
    "renderData": { type: Object },
    "isHumanAgentEnabled": { type: Boolean, default: false },
    "isCampaigningEnabled": { type: Boolean, default: false },
    "isGenAiEnabled": { type: Boolean, default: true },
    "userchatsToUsersMerged": { type: Boolean, default: false },
    "locales": { type: Array, 'default': ['en'] },
    '_2FA': { type: Boolean, default: false }, //two factor authentication true/false
    "custom_design": { type: Boolean, default: false },
    "isActionEnabled": { type: Boolean, default: false },
    'middleware': { type: String, default: 'https://helloyubo.com:2000/' },
    'createdAt': { type: Date, default: Date.now },
    'updatedAt': { type: Date, default: Date.now },
    'sessionToken': { type: String },
    'emailNotification': { type: Boolean, default: false },
    'whatsappNotification': { type: Boolean, default: false },
    'filterDays': { type: Number, default: 7 },
    'agentTemplate': { type: String, default: "" },
    'storageUsed': { type: String, default: "S3_YUBO" },
    'storageConfigKeys': { type: Object },
    'codeNodeMonitorFlag': { type: Boolean, default: false },
    'spreadsheetURL': { type: String },
    'sheetName': { type: String },
    'failedLoginCount': { type: Number, default: 0 },
    'restartMessageStatus' : { type: Boolean, default: true },
    'subscription': { type: Object },
    'notifyMailCount': { type: Number ,default : 0 },// to send email when user's subscription expired [count to send mail for 3 times only]
});

newSchema.pre('save', function(next) {
    this.updatedAt = Date.now();
    next();
});

newSchema.pre('update', function() {
    this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
    this.update({}, { $set: { updatedAt: Date.now() } });
});



module.exports = mongoose.model('Client', newSchema);
