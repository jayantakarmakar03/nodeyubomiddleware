//***********************************************************************************/
//Ace code setup here
//***********************************************************************************/

// Retrieve Elements
const consoleLogList = document.querySelector('.editor__console-logs');
//const executeCodeBtn = document.querySelector('.editor__run');
//const resetCodeBtn = document.querySelector('.editor__reset');

// Setup Ace code here
let codeEditor = ace.edit("editorCode");
let defaultCode = "console.log(\"Hello World!\")";
let consoleMessages = [];

let editorLib = {
    clearConsoleScreen() {
        consoleMessages.length = 0;

        // Remove all elements in the log list
        while (consoleLogList.firstChild) {
            consoleLogList.removeChild(consoleLogList.firstChild);
        }
    },
    printToConsole() {
        consoleMessages.forEach(log => {
            const newLogItem = document.createElement('li');
            const newLogText = document.createElement('pre');

            newLogText.className = log.class;
            newLogText.textContent = `> ${log.message}`;

            newLogItem.appendChild(newLogText);

            consoleLogList.appendChild(newLogItem);
        })
    },
    init() {
        // Configure Ace

        // Theme
        codeEditor.setTheme("ace/theme/cobalt"); //dreamweaver, github

        // Set language
        codeEditor.session.setMode("ace/mode/javascript");
        //set identation
        codeEditor.session.setOptions({ tabSize: 2, useSoftTabs: true })

        // Set Options
        codeEditor.setOptions({
            fontFamily: 'Inconsolata',
            fontSize: '12pt',
            enableBasicAutocompletion: true,
            enableLiveAutocompletion: true,
            tabSize: 2,
            useSoftTabs: true
        });

        // Set Default Code
        codeEditor.setValue(defaultCode);
    }
}

// Events
// executeCodeBtn.addEventListener('click', () => {
//     // Clear console messages
//     editorLib.clearConsoleScreen();

//     // Get input from the code editor
//     const userCode = codeEditor.getValue();

//     // Run the user code
//     try {
//         new Function(userCode)();
//     } catch (err) {
//         console.error(err);
//     }

//     // Print to the console
//     editorLib.printToConsole();
// });

function runBtnClick() {
    // yuboVariables.class = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    const userCode = codeEditor.getValue();

    console.log('userCode \n', userCode)
}
// resetCodeBtn.addEventListener('click', () => {
//     // Clear ace editor
//     codeEditor.setValue(defaultCode);

//     // Clear console messages
//     editorLib.clearConsoleScreen();
// })

editorLib.init();

//***********************************************************************************/

//****************AJAX CALLS HERE*************************/
$(".code_save").click(function(i) {
    const userCode = codeEditor.getValue();
    if(userCode.length == 0 || !userCode){
        alert('Codenode can not be empty, please add some code here')
    }else{

    $.ajax({
        type: "POST",
        url: "/saveCodes",
        data: { code: userCode },
        success: function(data) {
            if (data) {
                if (data.statusCode == 200) {
                    swal({
                        title: 'Success!',
                        icon: "success",
                        text: `${data.data.listOfOptinNumbers.length} phone numbers have been opted out while ${data.data.listOfNotOptinNumbers.length} phone numbers were invalid`,
                        type: 'success',
                        confirmButtonText: 'OK'
                    }).then(() => {
                        console.log('multi user optin  done ');
                        location.reload();
                    });
                } else {
                    swal({
                        title: data.message,
                        icon: "error",
                        type: 'info',
                        confirmButtonText: 'OK'
                    }).then(() => {
                        location.reload();
                    });
                }
            }
        },
        error: function(jqXHR, textStatus, err) {
            if (jqXHR.responseJSON.statusCode == 403) {
                window.location.replace("https://admin.helloyubo.com/login");
            } else {
                alert("Something went wrong, please try again")
            }
        }
    });
    }
});

async function preDefined() {
    const userCode = codeEditor.getValue();
    console.log(userCode)
    let returnObj = [{ result: q }]
    return returnObj
}