const db = require('../models/all-models');
const mongo = require('mongodb');
const axios = require('axios');
const moment = require('moment');
const ObjectID = mongo.ObjectID;
const escapeHtml = require('escape-html');
const emailverifycontroller = require("../controllers/emailverification.js");

module.exports={
    addCoupon            : addCoupon,
    updateCoupon         : updateCoupon,
    deleteCoupon         : deleteCoupon,
    getCouponList        : getCouponList,
    applyCoupon          : applyCoupon,
    getAllcouponList     : getAllcouponList

}

async function getCouponList(req,res){
    try{
        let couponData = await db.Coupon.find({ visibility : true });
        res.json({
                statusCode : 200,
                status     : true,
                message    : "Coupon list found",
                data       : couponData
        });
    } catch (error) {
        res.status(201).json({ error :  error});
    }
}
async function addCoupon(req,res){
    try {
        // console.log('body:', req.body)
        var { code, discount, validUntil, validFrom, isActive, isReusable, duration, durationType,visibility } = req.body;
        if(validFrom && validUntil){
            validFrom = new Date(validFrom);
            validUntil =new Date(validUntil);
        }

        let saveData = new db.Coupon({
            code         :  code,
            discount     :  discount,
            validFrom    :  validFrom,
            validUntil   :  validUntil,
            isActive     :  isActive,
            isReusable   :  isReusable,
            duration     :  duration,
            durationType :  durationType,
            visibility   :  visibility
        })

        let result =  await saveData.save();
        // console.log('result:', result)
        res.json({
                statusCode : 200,
                status     : true,
                message    : "Coupon added successfully",
                data       : result
        });
   } catch (error) {
        console.log('error in catch block',error);
        res.status(201).json({ error :  error});
   }
}
async function updateCoupon(req ,res ){
    try {
        var {_id, code, discount, validUntil, validFrom, isActive, isReusable, duration, durationType,visibility } = req.body;

        if(validFrom && validUntil){
            validFrom = new Date(validFrom);
            validUntil =new Date(validUntil);
        }
       let updateData =  await db.Coupon.updateOne({_id : _id },{
            $set : {
                code         :  code,
                discount     :  discount,
                validFrom    :  validFrom,
                validUntil   :  validUntil,
                isActive     :  isActive,
                isReusable   :  isReusable,
                duration     :  duration,
                durationType :  durationType,
                visibility   :  visibility
            }
        });
        res.json({
                statusCode : 200,
                status     : true,
                message    : "Coupon updated successfully",
                data       : updateData
        });

      } catch (error) {
        console.log('error in catch block',error);
        res.status(500).json({ error: 'An error occurred' });
      }
    

}
async function deleteCoupon(req ,res ){
    try {
            const {_id } = req.body;
            let deleteRes =  await db.Coupon.deleteOne({_id : _id });
            res.json({
                    statusCode : 200,
                    status     : true,
                    message    : "Coupon deleted successfully",
                    data       : deleteRes
            });
      } catch (error) {
        res.status(500).json({ error: 'An error occurred' });
      }
    

}

async function applyCoupon(req, res) {
    try{
            let { couponCode,clientId } = req.body;
            let couponData =  await db.Coupon.findOne({ code : couponCode });
            // console.log('couponData:11111111', couponData)
            if(!couponData){
                    return res.status(201).json({
                        statusCode : 201,
                        status     : false,
                        message    : "This coupon is not valid!",
                        data       : []
                    });
            }
            let isCouponValidCheck = await isCouponValid(couponData.validFrom, couponData.validUntil) ;
            // console.log('isCouponValidCheck:222222222222', isCouponValidCheck)
            if (isCouponValidCheck) {
                // console.log('This coupon is not valid!');
                
                const calculatedDuration = await calculateDurationDays(couponData.duration, couponData.durationType );
                // console.log('Calculated Duration:33333333333333', calculatedDuration);
                let newEndDate = getNextPaymentDate(calculatedDuration) ;
                // console.log('newEndDate4444444444:', newEndDate)
                newEndDate= new Date(newEndDate);
                // console.log('newEndDate:444444444444444', newEndDate)
                const newStartDate = new Date();
                // const newEndDate = moment(currentDate).add(30, 'days').toDate();
            
                let updateClientData =await updateSubscription(newStartDate, newEndDate , clientId);
                // console.log('updateClientData:5555555555555', updateClientData)
                let clientData = await db.Client.findOne({clientId: clientId},{clientId:1,email:1}) 
                await emailverifycontroller.couponAppliedSuccessfully(clientData);

                return res.status(200).json({
                    statusCode : 200,
                    status     : true,
                    message    : "Coupon applied successfully",
                    data       : []
                });

            } else {
                return res.status(201).json({
                    statusCode : 201,
                    status     : false,
                    message    : "This coupon is not valid!",
                    data       : []
                });
            }

    }catch(err){
        console.log('error in catch block',err);
    }
}
function getNextPaymentDate(numberOfDays) {
    try{
            const today = new Date();
            const nextRenewalDate = new Date(today);
            nextRenewalDate.setDate(today.getDate() + numberOfDays);
            const year = nextRenewalDate.getFullYear();
            const month = String(nextRenewalDate.getMonth() + 1).padStart(2, '0');
            const day = String(nextRenewalDate.getDate()).padStart(2, '0');
            return `${year}-${month}-${day}`;
    }catch(err){
        console.log('error in catch block',err);
    }  
}
function calculateDurationDays(duration, durationType) {
    try{
        if (durationType === 'day') {
            return duration * 1;
        } else if (durationType === 'month') {
            return duration * 30;
        } else if (durationType === 'year') {
            return duration * 365;
        } else {
            // console.log('Invalid durationType');
            return null; // or an appropriate default value
        }
    }catch(err){
        console.log('error in catch block',err);
    }  
}

function isCouponValid(validFrom, validUntil) {
    try{

        const currentDate = new Date();
        const startDate = new Date(validFrom);
        const endDate = new Date(validUntil);
        if (currentDate >= startDate && currentDate <= endDate) {
            return true; // Coupon is valid
        } else {
            return false; // Coupon is not valid
        }
    }catch(err){
        console.log('error in catch block',err);
    }  
}

async function updateSubscription(startDate, endDate, clientId) {
    try{
        // console.log('Updating subscription in the database...------------------');
        let subscriptionObj = {
            startDate : startDate,
            endDate   : endDate,
            type      : "COUPON"
        }
        // console.log('clientId: 99999999999999999999-------------', clientId)
        let updateTransactionStatus = await db.Client.updateOne(
                                    { clientId: clientId }, 
                                    {
                                        $set: {
                                            subscription : subscriptionObj
                                        }
                                    });
          return updateTransactionStatus;
        // console.log('updateTransactionStatus: 100000000000000000000000 ------------------', updateTransactionStatus)
    }catch(err){
        console.log('error in catch block',err);
    }  
}

//all or coupon list having visibility true or false both
async function getAllcouponList(req,res){
    try{
        let couponData = await db.Coupon.find();
        res.json({
                statusCode : 200,
                status     : true,
                message    : "All Coupon list found",
                data       : couponData
        });
    } catch (error) {
        res.status(201).json({ error :  error});
    }
}