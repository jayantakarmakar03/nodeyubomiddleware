const db = require("../models/all-models")
var AWS = require('aws-sdk')
var fs = require('fs')
var moment = require('moment');
var axios = require('axios')
 
module.exports = {
    createCampaign: createCampaign,
    scheduledCampaignList: scheduledCampaignList,
    getScheduledCampaign: getScheduledCampaign,
    updateScheduledCampaign: updateScheduledCampaign,
    deleteScheduledCampaign: deleteScheduledCampaign,
    sendWhatsupCampaignInGroup: sendWhatsupCampaignInGroup

}


async function createCampaign(req,res) {

    const clientId = req.user.clientId
    var campaignData = req.body
    campaignData.clientId = clientId
    let file_name = 'file';
    var campaignData;
   
    var time = moment().format("DDMMYYYYHHMMSS");
    filename = `${time}_${file_name}`;
    var imageContentType, buffer;

    await AWS.config.update({
        accessKeyId: config.s3Linode.accessKeyId,
        secretAccessKey: config.s3Linode.secretAccessKey,
        region: config.s3Linode.region,
        s3BucketEndpoint: config.s3Linode.s3BucketEndpoint,
    });
    var s3 = await new AWS.S3({ endpoint: config.s3Linode.url });
    var newMediaUrl = req.body.media_url.split('/')

    req.body.scheduledAt=new Date(req.body.scheduledAt)
    if(req.body.savedMsgType=="TEXT") {

        const data = await db.ScheduledCampaign.updateOne({ scheduledAt: req.body.scheduledAt, clientId:req.body.clientId},   
            {  $setOnInsert: campaignData},
            { upsert: true })

        return res.json({
            message: "Campaign Scheduled Successfully!",
            data:data,
            statusCode:200,
            status: true,
        }) 
    }

    if(newMediaUrl.indexOf('yugasabot.ap-south-1.linodeobjects.com')== -1) {
         if(newMediaUrl.indexOf('http:')== -1 && newMediaUrl.indexOf('https:')== -1) {
            return res.json({
                message: "Please enter the valid media URL and/or variables' values.",
                statusCode:201,
                status: false,
            })

         } else {
            
            const data = await db.ScheduledCampaign.updateOne({ scheduledAt: req.body.scheduledAt, clientId:req.body.clientId},   
                {  $setOnInsert: campaignData},
                { upsert: true })
            return res.json({
                message: "Campaign Scheduled Successfully!",
                data:data,
                statusCode:200,
                status: true,
            })
        }

    } else {
        await axios.get(campaignData.media_url, { responseType: 'arraybuffer'})
        .then(function (response) {
            imageContentType = Object.values(response.headers)[1]
            buffer = response.data
           // buffer = Buffer.from(response.data, 'base64');
        }).catch(function (error) {
            console.log(error);
        })

        var params = {
            Bucket: `yugasabot/${clientId}/Media/Admin_files/WhatsApp_campaign/scheduledCampaign`,
            Key: `${filename}`,
            Body: buffer,
            ContentType: imageContentType,
            ACL:'public-read'
        }

        await s3.upload(params, async(err, result) => {
        
            if (err) {
                   res.json({
                    message: 'Error occured during file upload on s3 server',
                    statusCode: 500,
                    status: false,
                    error  : err
                });
            } else {
               campaignData.media_url=result.Location
              const data = await db.ScheduledCampaign.updateOne({ scheduledAt: req.body.scheduledAt, clientId:req.body.clientId},   
                {  $setOnInsert: campaignData},
                { upsert: true })
                
                return res.json({
                    message: "Campaign Scheduled Successfully!",
                    data:data,
                    status: true,
                    statusCode:200,
                })
            }            
        })

    }

}

async function scheduledCampaignList(req,res) {
     const clientId = req.user.clientId
    try {
        const campaignList = await db.ScheduledCampaign.find({clientId:clientId}).sort({createdAt:-1})
        res.json({
            message: "Scheduled campaigns find successfully",
            data:campaignList,
            statusCode:200,
        })
        
    } catch (error) {
        return res.json({
            message:'Scheduled campaigns is not find',
            statusCode:500,
            error:error
        })
    }

}

async function getScheduledCampaign(req,res) {
    const id = req.body.id
    const clientId = req.user.clientId

   try {
    const getCampaign = await db.ScheduledCampaign.find({clientId:clientId,_id:id})
       res.json({
           message: "Scheduled campaigns find successfully",
           data:getCampaign,
           statusCode:200,
       })
       
   } catch (error) {
       return res.json({
           message:'Scheduled campaigns is not find',
           statusCode:500,
           error:error
       })
   }

}

async function updateScheduledCampaign(req,res) {
   const clientId = req.user.clientId

   try {
    const data = await db.ScheduledCampaign.findOne({scheduledAt: req.body.scheduledAt,
        clientId:clientId})
        if(data) {
        var updatedCampaign = await db.ScheduledCampaign.findOneAndUpdate({
                _id:req.body.id,clientId:clientId},
                {
                    templateName: req.body.templateName,
                    groups: req.body.groups,
                    buttonUrlParam: req.body.buttonUrlParam,
                    headerParam: req.body.headerParam,
                    media_url: req.body.media_url,
                    parameters: req.body.parameters,
                    // scheduledAt: new Date(req.body.scheduledAt),
                    frequency:req.body.frequency
                },
                {returnOriginal: false})
        } else {
                var updatedCampaign = await db.ScheduledCampaign.findOneAndUpdate({
                _id:req.body.id,clientId:clientId},
                {
                    templateName: req.body.templateName,
                    groups: req.body.groups,
                    buttonUrlParam: req.body.buttonUrlParam,
                    headerParam: req.body.headerParam,
                    media_url: req.body.media_url,
                    parameters: req.body.parameters,
                    scheduledAt: new Date(req.body.scheduledAt),
                    frequency:req.body.frequency
                },
                {returnOriginal: false})
        }                         

       res.json({
           message: "Scheduled campaigns is update successfully",
           data:updatedCampaign,
           statusCode:200,
       })
       
   } catch (error) {
       return res.json({
           message:'Scheduled campaigns is not update',
           statusCode:500,
           error:error
       })
   }

}


async function deleteScheduledCampaign(req,res) {
    const id = req.body.id
    const clientId = req.user.clientId

    try {
       const deleteCampaign = await db.ScheduledCampaign.findOneAndDelete({_id:id,clientId:clientId})
       res.json({
           message: "Scheduled campaigns is delete successfully",
           data:deleteCampaign,
           statusCode:200,
       })
       
    } catch (error) {
       return res.json({
           message:'Scheduled campaigns is not delete',
           statusCode:500,
           error:error
       })
   }

}

async function sendWhatsupCampaignInGroup(req,res) {

    const currentDateAndTime = new Date()
    const campaignData = await db.ScheduledCampaign.find({status:"Scheduled", scheduledAt:{ "$lte": new Date()}})

            if(campaignData.length<=0) {
               // console.log('There are no campaigns scheduled at this time');
               return
            }

        let promiseArrayObj = []
        await campaignData.forEach(function(obj) {

            if(obj.scheduledAt <= currentDateAndTime) {
                const newObj = {
                    "_id": obj._id,
                    "clientId" : obj.clientId,
                    "groupName" : obj.groups,
                    "buttonUrlParam": obj.buttonUrlParam,
                    "headerParam": obj.headerParam,
                    "media_url": obj.media_url,
                    "templateName": obj.templateName,
                    "parameters": obj.parameters,
                    "lang":obj.lang
                }

                var scheduledAtDate = obj.scheduledAt 
                promiseArrayObj.push(sendCampainAllGroup(newObj,scheduledAtDate))
            }
        })


        await Promise.allSettled(promiseArrayObj).then((promiseReturnArray) => {
            updateCampaignSendingStatus(promiseReturnArray)

        }).catch((error) => {
            console.log("error occured in promise all catch block", error);
        });
           
}

 function sendCampainAllGroup(data) {
    const newObj = data

       return new Promise((resolve, reject) => {
        axios.post('https://campaign.helloyubo.com/sendWaCampaignToGroups',newObj)
        .then(function(response) {
            response.scheduleId = newObj._id
           if(response.data.statusCode == 200){
              resolve(response);
           }else{
              reject(response);
           }
       }).catch(function (error) {
           console.log('Error in catch block1  \n',error.Error)
       })
    })

}

async function updateCampaignSendingStatus(dataObj) {
    const campaignData = dataObj
    var date = new Date()

    try {
        await campaignData.forEach(async function(obj) {

            if(obj.status == 'fulfilled') {
                
                const data = await db.ScheduledCampaign.find({_id:obj.value.scheduleId})
                if(data[0].frequency=="Does not repeat") {
                    const data = await db.ScheduledCampaign.findOneAndUpdate({_id:obj.value.scheduleId},
                    {status:"Success",errorReason:""},{returnOriginal: false})

                } else if(data[0].frequency=="Daily") {
                    const newDate = new Date(date.setDate(date.getDate() + 1))
                    const data = await db.ScheduledCampaign.findOneAndUpdate({_id:obj.value.scheduleId},
                    {scheduledAt:newDate,errorReason:""},{returnOriginal: false})

                }else if(data[0].frequency=="Weekly") {
                    const newDate = new Date(date.setDate(date.getDate() + 7))
                    const data = await db.ScheduledCampaign.findOneAndUpdate({_id:obj.value.scheduleId},
                        {scheduledAt:newDate,errorReason:""},{returnOriginal: false})    
                    
                }else if(data[0].frequency=="Monthly") {
                    const newDate = new Date(date.setMonth(date.getMonth()+1))
                    const data = await db.ScheduledCampaign.findOneAndUpdate({_id:obj.value.scheduleId},
                        {scheduledAt:newDate,errorReason:""},{returnOriginal: false})

                }else if(data[0].frequency=="Yearly") {
                    const newDate = new Date(date.setFullYear(date.getFullYear()+1))
                    const data = await db.ScheduledCampaign.findOneAndUpdate({_id:obj.value.scheduleId},
                        {scheduledAt:newDate,errorReason:""},{returnOriginal: false})
                    
                }
    
                } else if(obj.status == 'rejected') {
                    const data = await db.ScheduledCampaign.findOneAndUpdate({_id:obj.reason.scheduleId},
                        {status:"Failed",errorReason:obj.reason.data.message},{returnOriginal: false})   
            }
        }) 
    } catch (error) {
        console.log('Scheduled campaigns is not sent');
        
    }
}
