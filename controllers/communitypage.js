const db = require("../models/all-models"),
jwt = require("jsonwebtoken"),
bcrypt = require('bcrypt');

module.exports={
    communitylogin,
    communitydata
}

async function communitylogin(req,res,next){
try{
    const querystatus = await db.Client.find(
      {$or: [{ clientId: req.body.username }, { email: req.body.username }],},
      { password: 1 }
    );
      if(querystatus.length<1){
        return res.json({message:'Incorrect Username'})
      } else {
        var  passwordIsValid = await bcrypt.compareSync(
          req.body.password,
          querystatus[0].password
        );
      }
      if(!passwordIsValid){
        return res.json({message:'Incorrect Password'})
      }else{
        var status = await db.Client.find(
          { clientId: req.body.username},
          { name: 1 ,profile_img:1,email:1}
        );
      }
      let authToken = await jwt.sign(
        { email: status[0].username, email: status[0].email },
        "YuboSecretKey"
      );
    res.json({data:authToken,message:'Login Successfully'})
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function communitydata (req,res,next){
try{
    let decodedToken = jwt.verify(req.query.token, "YuboSecretKey",async function(err){
      if(err){
        return res.send('Invalid token')
      }
    });
    var status = await db.Client.find(
      { email: decodedToken.username},
      { name: 1 ,profile_img:1,email:1}
    );
    return res.json({message:'token verifed',data:status})
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
  }