const nodemailer = require('nodemailer');
const email = process.env.YUBO_EMAIL_ADDRESS
const password = process.env.YUBO_EMAIL_PASSWORD
 //sending mail using nodemailer. 
module.exports=function(email, pwd, name, html, attachments=""){
  let transporter = nodemailer.createTransport({
  host:'smtp.gmail.com',
  port:465,
  auth: {
    user:email,
    pass: password
   }
  });
  let mailOptions = {
    from:"Yugasa Bot <contact@helloyubo.com>",
    to:email,
    subject: 'Yugasa Bot',
    html:html,
    attachments: attachments
  }
 
  transporter.sendMail(mailOptions, async function(error, info){
    if(error){
      console.log("error",error);
    }
  });
}

