var limit = 50;
var skip = 0;
var docCount = 0; 

function getSubscriptionList() {
    $(".subscriptionListDiv").html("");
    let searchData = $("#searchBoxId").val();
    // showLoader();
    let clientId = document.getElementById("clientId").value;
    let data = {
        clientId   : clientId,
        searchItem : searchData,
        skip       : skip,
        limit      : limit
    };
    let html = `<h4 style="text-align:center"> Data not available</h4>`
    $(".subscriptionListDiv").append(html);

    // $.ajax({
    //     data: data,
    //     async: false,
    //     cache: false,
    //     method: "POST",
    //     url: "/getAllSubscriptionData",
    //     success: function(data) {
    //         if (data.statusCode == 200) {
    //               let subscribersList = data.data;
    //               skip = data.skip;
    //               limit =data.limit;
    //               docCount = data.docCount;
    //               if(subscribersList.length>0){
    //                 for (const iterator of subscribersList) {
    //                 let cf_eventTime =new Date(iterator.cf_eventTime).toDateString();
                    
    //                     let html = `
    //                                     <div class="col-md-2 width tablerow">${iterator.amount}</div>
    //                                     <div class="col-md-2 yugasa tablerow">${iterator.cf_subReferenceId}</div>
    //                                     <div class="col-md-2 yugasa tablerow">${cf_eventTime}</div>
    //                                     <div class="col-md-2 yugasa tablerow">${iterator.cf_status}</div>
    //                                     <div class="col-md-2 yugasa tablerow" >
    //                                         <i class="fa fa-file-pdf-o redColorPdf" aria-hidden="true" 
    //                                         data-cf_subReferenceId = ${iterator.cf_subReferenceId}
    //                                         onclick = "getOrderDetailsPDF(this)"
    //                                         id="generatePdf"
    //                                         ></i>
    //                                         <i  class="fa fa-eye eyeColor" aria-hidden="true" 
    //                                             data-cf_subReferenceId = ${iterator.cf_subReferenceId}
    //                                             onclick = "viewOrderDetails(this)"
    //                                         ></i>

    //                                     </div>
    //                                     ` 
    //                     $(".subscriptionListDiv").append(html);
    //                 }
    //               }else{
    //                     let html = `<h4 style="text-align:center"> Data not available</h4>`
    //                     $(".subscriptionListDiv").append(html);
    //               }
    //         } else if (data.statusCode == 201) {
    //             alert(data.message);
    //         }
    //     },error: function (jqXHR, textStatus, err) {  
    //         if(jqXHR.responseJSON.statusCode == 403) {
    //             window.location.replace("https://admin.helloyubo.com/login");
    //         } else {
    //             alert("Something went wrong, please try again")
    //         }
    //     }
    // });
}
$(document).ready(function(){
   getSubscriptionList();
   getClientSubscriptionDetails();
   listAllCoupon();

});
function searchFunction(e){
    skip = 0;
    getSubscriptionList();
    return;
}
function getClientSubscriptionDetails() {

    let clientId = document.getElementById("clientId").value;
    let data = {
        clientId   : clientId,
    };
    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: "POST",
        url: "/getClientSubscriptionDetails",
        success: function(data) {
            
            if (data.statusCode == 200) {
                if(data.data.subscription && data.data.subscription.endDate){

                    let subscribersList = data.data.subscription;
                    let endDate = new Date(subscribersList.endDate).toDateString();
                    $("#nextPaymentDate").html(endDate);
                    $("#amount").html(subscribersList.amount);
                      
                    html = `<button class="modify-subscription" 
                                    id = ${subscribersList.cf_subReferenceId} 
                                    onclick="cancelSubscription(id)">
                                    Cancel Subscription</button>`
                     $("#subCancelDiv").append(html);
                     if(subscribersList?.intervalType){
                        let duration =  subscribersList.intervalType.toLowerCase()
                        $("#planType").html(duration);
                     }
                     if(subscribersList?.type && subscribersList.type == 'COUPON' ){
                        $(".autoRenewalMsgDiv").addClass("displayNone");
                     }
                    let daysLeft = daysLeftFromEndDate(subscribersList.endDate);
                
                    $("#endDate").html(daysLeft);
            }
                
            } else if (data.statusCode == 203) {
                alert(data.message);
            }
        },error: function (jqXHR, textStatus, err) {  
            if(jqXHR.responseJSON.statusCode == 403) {
                window.location.replace("https://admin.helloyubo.com/login");
            } else {
                alert("Something went wrong, please try again")
            }
        }
    });
}
function daysLeftFromEndDate(endDate) {
    const oneDayInMillis = 24 * 60 * 60 * 1000; // Number of milliseconds in one day
    // Get the current date and time
    const currentDate = new Date();
    // Parse the end date string into a Date object
    const parsedEndDate = new Date(endDate);
    // Calculate the time difference in milliseconds
    const timeDifference = parsedEndDate - currentDate;
    // Calculate the number of days left
    const daysLeft = Math.ceil(timeDifference / oneDayInMillis);
    return daysLeft;
  }
function onTypingEvent(e){
    let ascii, key = e.key;
        ascii = key.charCodeAt(0);
    let searchData = $("#searchBoxId").val();
    if(ascii != 66){
        if (searchData.length > 3){
            skip = 0;
            getSubscriptionList();
            return;
        }
    }
    if (searchData.length == 0){
        // $( "#loadMoreButtonId" ).show();
        // $(".ExcelData").html("");
        skip = 0;
        getSubscriptionList();
    }
}

function viewOrderDetails(el) {
    $("#tablebody").html('');

    subReferenceId = $(el).attr("data-cf_subReferenceId");
    var data = {
        clientId          : document.getElementById("clientId").value,
        subReferenceId    : subReferenceId
    };

    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: "POST",
        url: "/getOrderDetailsBySubRefId",
        success: function(data) {
            if (data.statusCode == 200) {
                let tData = data.data.subscription ;
                $('#add_agents').modal('show');
                
                $("#userName").html(tData.customerName)
                $("#userEmail").html(tData.customerEmail)
                $("#userPhone").html(tData.customerPhone)
                $("#planName").html(tData.planName)
                $("#PlanId").html(tData.planId)
                $("#subStatus").html(tData.status)


                html = `
                <tr>
                   <th>customerPhone </th>
                   <td> ${tData.customerPhone}</td>
                </tr>
                <tr>
                    <th>customerName </th>
                    <td> ${tData.customerName}</td>
                </tr>
                <tr>
                    <th>currency </th>
                    <td> ${tData.currency}</td>
                </tr>
                <tr>
                    <th>customerEmail </th>
                    <td> ${tData.customerEmail}</td>
                </tr>
                <tr>
                    <th>mode </th>
                    <td> ${tData.mode}</td>
                </tr>
                <tr>
                    <th>planId </th>
                    <td> ${tData.planId}</td>
                </tr>
                <tr>
                    <th>planName </th>
                    <td> ${tData.planName}</td>
                </tr>
                <tr>
                    <th>recurringAmount </th>
                    <td> ${tData.recurringAmount}</td>
                </tr>
                <tr>
                    <th>scheduledOn </th>
                    <td> ${tData.scheduledOn}</td>
                </tr>
                <tr>
                    <th>status </th>
                    <td> ${tData.status}</td>
                </tr>
                <tr>
                    <th>subReferenceId </th>
                    <td> ${tData.subReferenceId}</td>
                </tr>
                <tr>
                    <th>subscriptionId </th>
                    <td> ${tData.subscriptionId}</td>
                </tr>
                <tr>
                    <th>upiId </th>
                    <td> ${tData.upiId}</td>
                </tr>
                <tr>
                    <th>intervals </th>
                    <td> ${tData.intervals}</td>
                </tr>
                <tr>
                    <th>intervalType </th>
                    <td> ${tData.intervalType}</td>
                </tr>
                <tr>
                    <th>firstChargeDate </th>
                    <td> ${tData.firstChargeDate}</td>
                </tr>
                <tr>
                    <th>cardNumber </th>
                    <td> ${tData.cardNumber}</td>
                </tr>
                <tr>
                    <th>authLink </th>
                    <td> ${tData.authLink}</td>
                </tr>
                <tr>
                    <th>authFlow </th>
                    <td> ${tData.authFlow}</td>
                </tr>

                `;
                  $("#tablebody").append(html);

                // swal({
                //     title: data.message,
                //     icon: "success",
                //     timer: 3000,
                // });
                // location.reload();
            } else {
                swal({
                    title: data.message,
                    icon: "success",
                    timer: 3000,
                });
            }
        },error: function (jqXHR, textStatus, err) {  
            location.reload();
            if(jqXHR.responseJSON.statusCode == 403) {
            window.location.replace("https://admin.helloyubo.com/login");
            } else {
            alert("Something went wrong, please try again")
            }
        }  
     })
}

function cancelSubscription(cf_subReferenceId){
    swal({
        title: "There is no active subscription right now. The free version of the bot is active.",
        icon: "info",
        timer: 10000,
    });
    // var data = {
    //     clientId             : document.getElementById("clientId").value,
    //     cf_subReferenceId    : cf_subReferenceId
    // };
    // $.ajax({
    //     data: data,
    //     async: false,
    //     cache: false,
    //     method: "POST",
    //     url: "/cancelSubscriptionPlan",
    //     success: function(data) {
    //         if (data.statusCode == 200) {

    //             // swal({
    //             //     title: data.message,
    //             //     icon: "success",
    //             //     timer: 3000,
    //             // });
    //             // location.reload();
    //         } else {
    //             swal({
    //                 title: data.message,
    //                 icon: "success",
    //                 timer: 3000,
    //             });
    //         }
    //     },error: function (jqXHR, textStatus, err) {  
    //         location.reload();
    //         if(jqXHR.responseJSON.statusCode == 403) {
    //         window.location.replace("https://admin.helloyubo.com/login");
    //         } else {
    //         alert("Something went wrong, please try again")
    //         }
    //     }  
    //  })
}

async function downloadInvoicePdf(el) {
    subReferenceId = $(el).attr("data-cf_subReferenceId");
    var data = {
        clientId          : document.getElementById("clientId").value,
        subReferenceId    : subReferenceId
    };

    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: "POST",
        url: "/getOrderDetailsBySubRefId",
        success: function(data) {
            if (data.statusCode == 200) {
                let tData = data.data.subscription ;
                
                 // Get the current date and format it
                let formattedDate = formatDate(new Date());
                // Get the current date and format it
                let startDateSpan = formatDate2(new Date(tData.addedOn));
                let endDateSpan = formatDate2(new Date(tData.scheduledOn));
                
                
                
                $("#todaysDate").html(formattedDate)
                $("#subrefid").html(tData.subReferenceId)
                $(".amountSpan").html(tData.recurringAmount)

                $("#startDateSpan").html(startDateSpan)
                $("#endDateSpan").html(endDateSpan)


                // $("#userEmail").html(tData.customerEmail)
                // $("#userPhone").html(tData.customerPhone)
                // $("#planName").html(tData.planName)
                // $("#PlanId").html(tData.planId)
                // $("#subStatus").html(tData.status)
                swal({
                    title: "Invoice downloaded successfully",
                    icon: "success",
                    timer: 3000,
                });
                return;
            } else {
                swal({
                    title: data.message,
                    icon: "success",
                    timer: 3000,
                });
            }
        },error: function ( err) {  
             console.log('error',err )
        }  
     })
}

function formatDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1; // Months are zero-based
    var year = date.getFullYear();
    
    // Add leading zeros if needed
    if (day < 10) day = '0' + day;
    if (month < 10) month = '0' + month;
    
    return day + '/' + month + '/' + year;
 }

 function formatDate2(date) {
    var day = date.getDate();
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var month = monthNames[date.getMonth()];
    var year = date.getFullYear();
    
    return day + "-" + month + "-" + year;
 }
 
// ****** coupon ajax calls*******//

function applyCoupon() {
    let couponCode = $(".couponinput").val().trim();
    console.log('couponCode:', couponCode)
    if(couponCode ==""){
        swal({
            title: "Please enter a coupon code",
            icon: "error",
            timer: 9000,
          });
          return;
    }
    let clientId = document.getElementById("clientId").value;
    let data = {
        clientId   : clientId,
        couponCode : couponCode,
    };
    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: "POST",
        url: "/applyCoupon",
        success: function(data) {
            if (data.statusCode == 200) {

                  swal({
                    title: data.message,
                    icon: "success",
                    timer: 9000,
                  });
            
                setTimeout(() => {
                        location.reload();
                }, 2000);

            } else if (data.statusCode == 201) {
                swal({
                    title: data.message,
                    icon: "error",
                    timer: 9000,
                  });
                // alert(data.message);
            }
        },error: function (jqXHR, textStatus, err) {  
            if(jqXHR.responseJSON.statusCode == 403) {
                window.location.replace("https://admin.helloyubo.com/login");
            } else {
                alert("Something went wrong, please try again")
            }
        }
    });
}
function listAllCoupon() {
    $("#couponListDiv").html("");
    // showLoader();
    let data = {
        
    };
    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: "POST",
        url: "/getCouponList",
        success: function(data) {
            if (data.statusCode == 200) {

                  let couponList = data.data;
                  if(couponList.length>0){
                    for (const iterator of couponList) {
                        if(iterator.duration>1){
                            iterator.durationType =iterator.durationType+'s'
                        }
                    
                        let html = `<div class="mb-8">
                                        <div class="row">
                                        <div class="yubofree"> ${iterator.code}</div>
                                        </div>
                                        <div class="row row-margin-top">
                                        <div class="col-md-8">
                                            <div class="free-sunscription"><span > <span> ${iterator.duration} </span> <span> ${iterator.durationType} </span>  of free subscription  </span></div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <button class="coupon-apply-button apply-padding"
                                                        data-couponcode = ${iterator.code}
                                                        onclick="applyCouponFromList(this)"
                                                        >Apply</button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>`

                        $("#couponListDiv").append(html);
                    }
                  }else{
                          let html = `<h5 style="text-align:center"> Coupons not available</h5>`
                          $("#couponListDiv").append(html);
                  }

            } else if (data.statusCode == 201) {
                alert(data.message);
            }
        },error: function (jqXHR, textStatus, err) {  
            if(jqXHR.responseJSON.statusCode == 403) {
                window.location.replace("https://admin.helloyubo.com/login");
            } else {
                alert("Something went wrong, please try again")
            }
        }
    });
}

function applyCouponFromList(el){
    let couponCode = $(el).attr("data-couponcode");
    $(".couponinput").val(couponCode);
    applyCoupon();
    // let couponCode = $(".couponinput").val().trim();

}
