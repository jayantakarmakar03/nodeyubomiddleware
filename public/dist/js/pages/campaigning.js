// to Delete the image
document.querySelectorAll(".emailer_dlt_icon").forEach((item) => {
    item.addEventListener("click", function() {
    //function remove_emailerImgBtn() {
    let input = this;
    var remove_emailerImg = document.getElementById(
    this.getAttribute("parentDivId")
    );
    remove_emailerImg.remove();
    });
   });
   //to upload benner images
   
   document.querySelectorAll(".email_image_uploaded").forEach((item) => {
    item.addEventListener("change", function() {
    let input = this;
    if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
    $("#" + input.nextElementSibling.id).attr("src", e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
    }
    });
   });
   
   // for link prompt
   
   function link_prompt(type) {
    var link_btn = prompt("Please enter button url");
    if (link_btn != null) {
    $("#" + type).val(link_btn);
    }
   }
   //***********template 1 bottom color picker change function ******//
   var emailerBtncolor1;
   var defaultemailerBtncolor1 = "#0000ff";
   window.addEventListener("load", emailerBtncolor1_bg_fnctn, false);
   
   function emailerBtncolor1_bg_fnctn() {
    emailerBtncolor1 = document.getElementById("emailerBtncolor1");
    emailerBtncolor1.value = defaultemailerBtncolor1;
    emailerBtncolor1.addEventListener(
    "input",
    emailerBtncolor1bg_color_fnctn,
    false
    );
   }
   
   function emailerBtncolor1bg_color_fnctn(event) {
    var emailerBtncolor1_bg = document.getElementById("emailerBtncolor1_bg");
    if (emailerBtncolor1_bg) {
    emailerBtncolor1_bg.style.background = event.target.value;
    $(".demo1buttoncolor").val(event.target.value);
    }
   }
   //***************************************//
   
   //btn 2
   var emailerBtncolor2;
   var defaultemailerBtncolor2 = "#0000ff";
   
   window.addEventListener("load", emailerBtncolor2_bg_fnctn, false);
   
   function emailerBtncolor2_bg_fnctn() {
    emailerBtncolor2 = document.getElementById("emailerBtncolor2");
    emailerBtncolor2.value = defaultemailerBtncolor2;
    emailerBtncolor2.addEventListener(
    "input",
    emailerBtncolor2bg_color_fnctn,
    false
    );
   }
   
   function emailerBtncolor2bg_color_fnctn(event) {
    var emailerBtncolor2_bg = document.getElementById("emailerBtncolor2_bg");
   
    if (emailerBtncolor2_bg) {
    emailerBtncolor2_bg.style.background = event.target.value;
    $(".demo2_button1color").val(event.target.value);
    }
   }
   //btn 3
   // var emailerBtncolor3;
   // var defaultemailerBtncolor3 = "#0000ff";
   
   // window.addEventListener("load", emailerBtncolor3_bg_fnctn, false);
   
   // function emailerBtncolor3_bg_fnctn() {
   // emailerBtncolor3 = document.getElementById("emailerBtncolor3");
    
   // emailerBtncolor3.value = defaultemailerBtncolor3;
   // emailerBtncolor3.addEventListener(
   // "input",
   // emailerBtncolor3bg_color_fnctn,
   // false
   // );
   // }
   
   // function emailerBtncolor3bg_color_fnctn(event) {
   // var emailerBtncolor3_bg = document.getElementById("emailerBtncolor3_bg");
   
   // if (emailerBtncolor3_bg) {
   // emailerBtncolor3_bg.style.background = event.target.value;
   // $(".demo2_button2color").val(event.target.value);
   // }
   // }
   //******template 1 top color picker fumction*******//
   var emailerBtncolor4;
   var defaultemailerBtncolor4 = "#0000ff";
   
   window.addEventListener("load", emailerBtncolor4_bg_fnctn, false);
   
   function emailerBtncolor4_bg_fnctn() {
    emailerBtncolor4 = document.getElementById("emailerBtncolor4");
    emailerBtncolor4.value = defaultemailerBtncolor4;
    emailerBtncolor4.addEventListener(
    "input",
    emailerBtncolor4bg_color_fnctn,
    false
    );
   }
   
   function emailerBtncolor4bg_color_fnctn(event) {
    var emailerBtncolor4_bg = document.getElementById("emailerBtncolor4_bg");
    if (emailerBtncolor4_bg) {
    emailerBtncolor4_bg.style.background = event.target.value;
    $(".demo1headercolor").val(event.target.value);
    }
   }
   //******************************************** */
   //btn 5+
   var emailerBtncolor5;
   var defaultemailerBtncolor5 = "#0000ff";
   
   window.addEventListener("load", emailerBtncolor5_bg_fnctn, false);
   
   function emailerBtncolor5_bg_fnctn() {
    emailerBtncolor5 = document.getElementById("emailerBtncolor5");
    emailerBtncolor5.value = defaultemailerBtncolor5;
    emailerBtncolor5.addEventListener(
    "input",
    emailerBtncolor5bg_color_fnctn,
    false
    );
   }
   
   function emailerBtncolor5bg_color_fnctn(event) {
    var emailerBtncolor5_bg = document.getElementById("emailerBtncolor5_bg");
    if (emailerBtncolor5_bg) {
    emailerBtncolor5_bg.style.background = event.target.value;
    $(".demo2headercolor").val(event.target.value);
    }
   }
   //for calander popup
   $(".schedule_btn_open").click(function(e) {
    $(".schedule-popUp-body").fadeIn();
    e.stopPropagation();
   });
   $(".schedule_btn_close").click(function() {
    $(".schedule-popUp-body").fadeOut();
   });
   $(".schedule-popUp-body-Inner").click(function(e) {
    e.stopPropagation();
   });
   $(".schedule-popUp-close").click(function() {
    $(".schedule-popUp-body").fadeOut();
   });
   //code for calander
   // $('#demo').dcalendarpicker();
   // $('#calendar-demo').dcalendar(); //creates the calendar
   $(document).on("click", ".campaign_selected_option", function(e) {
    $.ajax({
    type: "POST",
    url: "/getuserName",
    data: {},
    success: function(data) {
    if (data) {
    $(".userlist").html("");
   
    if (data.groupcreated.length < 1) {
    return;
    }
    $(".userlist").append('<li class="groupheading">Groups</li>');
    let tempLocalStoreData = getMailLocalstorageUserData();
    if(tempLocalStoreData && tempLocalStoreData.dropdownEmailData.length>0){

    let groupkeys;
    if(tempLocalStoreData && tempLocalStoreData.dropdownEmailData && tempLocalStoreData.dropdownEmailData.length>0){
        groupkeys = tempLocalStoreData.dropdownEmailData.map((ele)=>{
            if(ele.type == 'group'){
                return ele.name
            }
        })
    }
  
    for (j = 0; j < data.groupcreated.length; j++) {
        let objkey = Object.keys(data.groupcreated[j].createdgroup);
        if(groupkeys.includes(objkey[0])){
            $(".userlist").append(
                `<li><input type="checkbox" checked class="userdata ckbox" data-type="group" id =${
                data.groupcreated[j]._id } data-id=${ data.groupcreated[j]._id } name="groupmail[]" data-mail='${JSON.stringify(
                data.groupcreated[j].createdgroup[objkey[0]]
                )}' value="${objkey[0]}"> <span> ${objkey[0]} </span></li>`);
        }else{
            $(".userlist").append(
                `<li><input type="checkbox"  class="userdata ckbox" data-type="group" id =${
                data.groupcreated[j]._id } data-id=${ data.groupcreated[j]._id } name="groupmail[]" data-mail='${JSON.stringify(
                data.groupcreated[j].createdgroup[objkey[0]]
                )}' value="${objkey[0]}"> <span> ${objkey[0]} </span></li>`);
        }
      
    }
   
    // email list
    if(tempLocalStoreData && tempLocalStoreData.dropdownEmailData && tempLocalStoreData.dropdownEmailData.length>0){
        groupkeys = tempLocalStoreData.dropdownEmailData.map((ele)=>{
            if(ele.type != 'group'){
                return ele.name
            }
        })
    }
    $(".userlist").append('<li class="groupheading">Email list</li>');
    for (i = 0; i < data.data.length; i++) {
        if(groupkeys.includes(data.data[i].name)){
            $(".userlist").append(
                '<li><input type="checkbox" checked class="userdata ckbox" data-type="user" id="' +data.data[i].userId +'" data-id ="' +data.data[i].userId +'" data-mail="' +data.data[i].mail +'" value="' +data.data[i].name +'">' +
                "<span>" + data.data[i].name + "</span>"+
                "</li>"
                );
        }else{
            $(".userlist").append(
                '<li><input type="checkbox" class="userdata ckbox" data-type="user" id="' +data.data[i].userId +'" data-id ="' +data.data[i].userId +'" data-mail="' +data.data[i].mail +'" value="' +data.data[i].name +'">' +
                "<span>" + data.data[i].name + "</span>"+
                "</li>"
                );
        }
       
    }
   }else{
       //**in case local storage  is empty to show a fresh list of user  */
        for (j = 0; j < data.groupcreated.length; j++) {
            let objkey = Object.keys(data.groupcreated[j].createdgroup);
                $(".userlist").append(
                    `<li><input type="checkbox"  class="userdata ckbox" data-type="group" id =${
                    data.groupcreated[j]._id } data-id=${ data.groupcreated[j]._id } name="groupmail[]" data-mail='${JSON.stringify(
                    data.groupcreated[j].createdgroup[objkey[0]]
                    )}' value="${objkey[0]}"> <span> ${objkey[0]} </span> </li>`);
        }

        $(".userlist").append('<li class="groupheading">Email list</li>');
        for (i = 0; i < data.data.length; i++) {
                $(".userlist").append(
                    '<li><input type="checkbox" class="userdata ckbox" data-type="user" id="' +data.data[i].userId +'" data-id ="' +data.data[i].userId +'" data-mail="' +data.data[i].mail +'" value="' +data.data[i].name +'">' +
                    "<span>" + data.data[i].name + "</span>"+
                    "</li>"
                    );
        }
   }
    }
    },
    error: function(jqXHR, textStatus, err) {
    alert("Please refresh !");
    },
    });
   });
   
   $(document).on("click", ".savecreatedraft", async function(e) {
    let data = await templateJson();
   
    $.ajax({
    type: "POST",
    url: "/saveDataOnS3",
    async: false,
    cache: false,
    data: data,
    // timeout: 300000,
    beforeSend: function() {
    swal({
    title: "Mail sending...",
    button: false,
    closeOnClickOutside: true,
    closeOnEsc: false,
    timer: 3500,
    });
    },
    success: async function(data) {
    await swal({
    title: "Draft Saved Successfully",
    icon: "success",
    timer: 3500,
    });
    },
    error: function(jqXHR, textStatus, err) {
    alert("Please refresh !");
    },
    });
   });
   
   // function loader (){
   
   // <div class="spinner-border text-primary"></div>
   // }
   
   // code for configure
   // code for sandgrid
   $(".sandgridPopUp-open").click(function(e) {
    $(".sandgridPopUp-body").fadeIn();
    e.stopPropagation();
   });
   $(".sandgridPopUp-body-Inner").click(function(e) {
    e.stopPropagation();
   });
   $(".sandgridPopUp-close").click(function() {
    $(".sandgridPopUp-body").fadeOut();
   });
   $(document).click(function() {
    $(".sandgridPopUp-body").fadeOut();
   });
   
   $(".sandgrid_btn").on("click", function() {
    if ($(this).val() == "yes") {
    $(".sandgrid_mail").addClass("hide");
    $(".sandgrid_api").removeClass("hide");
    } else {
    $(".sandgrid_api").addClass("hide");
    $(".sandgrid_mail").removeClass("hide");
    }
   });
   $.ajax({
    type: "POST",
    url: "/campaigntempdata",
    data: {},
    success: function(data) {
    if (data.data) {
    let maildraft = data.data.maildraft;
    //console.log("svhiammmm", maildraft);
    for (elem in maildraft) {
    if (maildraft[elem].includes("https")) {
    $("." + elem).attr("src", `${maildraft[elem]}`);
    } else if (maildraft[elem][0] == "#") {
    //console.log(" `${maildraft[elem]}`", `${maildraft[elem]}`);
    //console.log(" `${[elem]}`", `${elem}`);
    if (`${elem}` == "demo1headercolor") {
    $("#emailerBtncolor4_bg").css(
    "background-color",
    `${maildraft[elem]}`
    );
    }
    if (`${elem}` == "demo1buttoncolor") {
    $("#emailerBtncolor1_bg").css(
    "background-color",
    `${maildraft[elem]}`
    );
    }
    if (`${elem}` == "demo2headercolor") {
    $("#emailerBtncolor5_bg").css(
    "background-color",
    `${maildraft[elem]}`
    );
    }
    if (`${elem}` == "demo2_button1color") {
    $("#emailerBtncolor2_bg").css(
    "background-color",
    `${maildraft[elem]}`
    );
    }
    if (`${elem}` == "demo2_button2color") {
    $("#emailerBtncolor3_bg").css(
    "background-color",
    `${maildraft[elem]}`
    );
    }
    } else {
    $("." + elem).val(maildraft[elem]);
    }
    }
    // if(maildraft.templateid == 'demo1'){
    // $('.tempseldemo1').addClass('in active');
    // $('.tempseldemo2').removeClass('in active');
   
    // }else{
    // $('.tempseldemo2').addClass(' in active');
    // $('.tempseldemo1').removeClass(' in active');
    // }
    }
    },
    error: function(jqXHR, textStatus, err) {
    alert("Please refresh !");
    },
   });
   
   // code for group dropdown
   
   var checkList = document.getElementById("campaign_list");
   var campaign_select_items = document.getElementById("campaign_select_items");
   checkList.getElementsByClassName("campaign_selected_option")[0].onclick =
    function(evt) {
    if (campaign_select_items.classList.contains("visible")) {
    campaign_select_items.classList.remove("visible");
    campaign_select_items.style.display = "none";
    } else {
    campaign_select_items.classList.add("visible");
    campaign_select_items.style.display = "block";
    }
    };
   
   // campaign_select_items.onblur = function(evt) {
   // campaign_select_items.classList.remove('visible');
   // }
   
   // for whatsapp campaigning 
   var existing = localStorage.getItem('savedTemplateData');
       existing = JSON.parse(existing);
       var singleUserArray ;
       if(existing === null){
           singleUserArray = [];
           let obj = {
                dropdownData :[],
                templateData : "",
                headerData : "",
                footerData : "",
                msgtypeData:"TEXT",
                buttonurlData : ""
            }
         localStorage.setItem('savedTemplateData', JSON.stringify(obj));
       }else{
           singleUserArray = existing.dropdownData;
       }
    //***code for whatsapp dropdown checkbox */
   $(document).on("click", ".ckbox1", function() {
    if (this.checked) {
    let phone = $(this).attr("data-phone");
    let obj ={
    id :$(this).attr("data-id"),
    name: $(this).val(),
    phone: JSON.parse(phone)
    }
    if(typeof(JSON.parse(phone)) == 'object'){
    obj.type = 'group';
    }
    singleUserArray.push(obj);
    
    let id = $(this).attr("data-id") ? $(this).attr("data-id") : 2;
    let campaign_tag1 = `<span class="campaign_tags1" tag-id="${id}"><span class="campaign_entitiesTag rec_phone" usertype='${$(
    this
    ).attr("data-type")}' data-phone='${$(this).attr("data-phone")}'>${ this.value
    }</span><img class="campaign_entity-close" entity_id="${id}" src="dist/img/close-button.png" alt="close-button"></span>`;
   
    $(".created_entity1").append(campaign_tag1);
    $(".campaign_list_entity1").show();
    $(".campaign_list_entity1").css("display", "block", "width", "500px");
    } else {

    $('span[tag-id ="' + $(this).attr("id") + '"]').remove();
    if ($(".campaign_tags1").length == 0) {
    $(".campaign_list_entity1").css("display", "none");
    }
   
    var index = singleUserArray.findIndex((el)=>{
    return el.name == $(this).val();
    })
    
    if (index !== -1) {
    singleUserArray.splice(index, 1);
    }
    }
    
    var existing = localStorage.getItem('savedTemplateData');
    if(existing === null){
    let obj = {
    dropdownData :singleUserArray,
    templateData : "",
    headerData : "",
    footerData : "",
    msgtypeData:"TEXT",
    buttonurlData : ""
    }
    localStorage.setItem('savedTemplateData', JSON.stringify(obj));
    } else {

    existing = JSON.parse(existing);
    existing.dropdownData = singleUserArray;
    localStorage.setItem('savedTemplateData', JSON.stringify(existing));
    }
   });
   
   $(document).on("click", ".campaign_tags1 .campaign_entity-close", function() {

    var existing = localStorage.getItem('savedTemplateData');
    existing = JSON.parse(existing);
    
    let selected_id = `${$(this).attr("entity_id")}`;
    // var index = existing.dropdownData.findIndex((el)=>{
    //     return el.id == selected_id ;
    // })
   
    // const index = existing.dropdownData.findIndex(obj => obj.id == selected_id);
    // const index = existing.dropdownData.indexOf(5);
    // console.log('index num=======',index);
    
    // if (index !== -1) {
    //     existing = existing.dropdownData.splice(index, 1);
    // }
    let data = existing.dropdownData.filter(ele=> {
        return ele.id != selected_id
    })

    let obj = {
        dropdownData: data ,
        templateData : '',
        headerData : "",
        footerData : "",
        msgtypeData:"TEXT",
        buttonurlData : ""
    }
    localStorage.setItem('savedTemplateData', JSON.stringify(obj));
    singleUserArray = data ;

    $(this).parent("span").remove();
    $(`#${$(this).attr("entity_id")}`).prop("checked", false);
    if ($(".campaign_tags1").length == 0) {
    $(".campaign_list_entity1").css("display", "none");
    }
   });
   
   //for email campaigning 
   var existing = localStorage.getItem('savedEmailData');
   existing = JSON.parse(existing);
   var singleEmailUserArray =[] ;
   if(existing === null){
    singleEmailUserArray = [];
       let obj = {
          dropdownEmailData :[],
        }
     localStorage.setItem('savedEmailData', JSON.stringify(obj));
   }else{
    singleEmailUserArray = existing.dropdownEmailData;
   }
   // email config class is used for saving sendgrid crenditials

   $(document).on("click", ".ckbox", function() {
    if (this.checked) {
        let mail = $(this).attr("data-mail");
        // console.log('typeof(JSON.parse(mail))',typeof(JSON.parse(mail)))
        //console.log(' typem  of  (mail))',((mail)))
        //console.log('vvvvvv',mail.includes('['));
                                               
        let obj ={
            id :$(this).attr("data-id"),
            name: $(this).val(),
        }
        if(mail.includes('[')){
            obj.type = 'group';
            obj.mail = JSON.parse(mail);
        }else{
            obj.type = '';
            obj.mail = mail;
        }
        singleEmailUserArray.push(obj);

    let id = $(this).attr("data-id") ? $(this).attr("data-id") : 2;
    let campaign_tag = `<span class="campaign_tags" tag-id="${id}" ><span class="campaign_entitiesTag rec_mail" usertype='${$(
    this
    ).attr("data-type")}' data-mail='${$(this).attr("data-mail")}'>${
    this.value
    }</span><img class="campaign_entity-close" entity_id="${id}" src="dist/img/close-button.png" alt="close-button"></li>`;
    $(".created_entity").append(campaign_tag);
    $(".campaign_list_entity").show();
    $(".campaign_list_entity").css("display", "block", "width", "500px");
    } else {
    $('span[tag-id ="' + $(this).attr("id") + '"]').remove();
    if ($(".campaign_tags").length == 0) {
    $(".campaign_list_entity").css("display", "none");
    }
    
        var index = singleEmailUserArray.findIndex((el)=>{
          return el.name == $(this).val();
        })
        
        if (index !== -1) {
            singleEmailUserArray.splice(index, 1);
        }
    }

    var existing = localStorage.getItem('savedEmailData');
    if(existing === null){
        let obj = {
           dropdownEmailData :[],
        }
        localStorage.setItem('savedEmailData', JSON.stringify(obj));
    } else {
        existing = JSON.parse(existing);
        existing.dropdownEmailData = singleEmailUserArray;
        localStorage.setItem('savedEmailData', JSON.stringify(existing));
    }
    
   });
   
   $(document).on("click", ".campaign_tags .campaign_entity-close", function() {
    var existing = localStorage.getItem('savedEmailData');
    existing = JSON.parse(existing);

    let selected_id = `${$(this).attr("entity_id")}`;
    
    let data = existing.dropdownEmailData.filter(ele=> {
        return ele.id != selected_id
    })

    
    let obj = {
        dropdownEmailData: data 
    }

    localStorage.setItem('savedEmailData', JSON.stringify(obj));
    singleEmailUserArray = data ;

    $(this).parent("span").remove();
    $(`#${$(this).attr("entity_id")}`).prop("checked", false);
    if ($(".campaign_tags").length == 0) {
    $(".campaign_list_entity").css("display", "none");
    }
   });
   
   /** api for scheduling mail */
   
   $(".send-schedule").click(function() {
    let selecteddate =
    $(".selected").find("a").text() + " " + $(".calendar-curr-month").html();
    let mailarray = getmail();
   
    if ($(".Emailtitle").val() == "") {
    alert("Please Enter title for the Draft");
    return;
    } else if (mailarray.length == 0) {
    alert("Please Select at least one recipient");
    return;
    }
    let id;
    if ($(".active").attr("temp-id") == "demo1") {
    id = "#demo1";
    } else {
    id = "#demo2";
    }
    let scheduleobj = {
    emailtitle: $(".Emailtitle").val(),
    templateId: $(".active").attr("temp-id"),
    scheduledate: selecteddate,
    mailrecipent: mailarray,
    };
    var formData = new FormData($(id)[0]);
    $.each(scheduleobj, function(key, value) {
    formData.append(key, value);
    });
    $.ajax({
    type: "POST",
    url: "/schedulemail",
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    data: formData,
    // timeout: 300000,
    beforeSend: function() {
    swal({
    title: "Mail sending...",
    button: false,
    closeOnClickOutside: true,
    closeOnEsc: false,
    timer: 3500,
    });
    },
    success: async function(data) {
    if (data.status == "success") {
    await swal({
    title: "Campaign Scheduled Successfully",
    text: `Campaign will be schedule for the date ${selecteddate}`,
    icon: "success",
    buttons: false,
    dangerMode: true,
    });
    } else {
    alert(data.msg);
    return;
    }
    },
    error: function(jqXHR, textStatus, err) {
    alert("Please refresh !");
    },
    });
   });
   
   // for getting receiver mail //
   mailArray = [];
   function getmail() {
       var data = localStorage.getItem('savedEmailData');
       var dataitem = JSON.parse(data)

       dataitem.dropdownEmailData.forEach(ele=>{
        if(ele.type == 'group'){
            ele.mail.forEach(element => {
                mailArray.push(element.mail)
            });
        }else{
            mailArray.push(ele.mail)
        }
       })

        /**for uique emails from array */
        let unique = [...new Set(mailArray)];
        return unique;
    // let emailarray = [];
    // $(".rec_mail").each(function() {
    // if ($(this).attr("usertype") == "group") {
    // let maildata = JSON.parse($(this).attr("data-mail"));
    // for (i = 0; i < maildata.length; i++) {
    // emailarray.push(maildata[i].mail);
    // }
    // } else {
    // emailarray.push($(this).attr("data-mail"));
    // }
    // });
    // /**for uique emails from array */
    // let unique = [...new Set(emailarray)];
    // return unique;
   }
   
   //for sending test mail
   $(document).on("click", ".email_test", async function() {
   
    const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   
    if ($(".Emailtitle").val() == "") {
    swal({
    title: "Email title cannot be blank",
    icon: "warning",
    timer: 3500,
    });
   
    return;
    }
    if ($(this).attr("mailtype") == "maildata") {
    // for send immediatly mail to leads
    var reciptormailTemp = getmail();
    var reciptormail = reciptormailTemp.filter(function (el) {
    return el != 'null' && el != "" && el != null && el != undefined && el != 'undefined';
    });
    if (reciptormail.length < 1) {
    swal({
    title: "Please select reciptor mailId from dropdown",
    icon: "warning",
    timer: 3500,
    });
   
    return;
    }
    } else {
    var reciptormail = $(".recievermail").val(); // for send test mail
    if (reciptormail == "") {
    swal({
    title: "Please enter reciptor mailId",
    icon: "warning",
    timer: 3500,
    });
   
    return;
    } else if (!re.test(String(reciptormail).toLowerCase())) {
    swal({
    title: "Please provide valid email id",
    icon: "warning",
    timer: 3500,
    });
   
    return;
    }
    }
   
    let data = await templateJson();
    data["reciptormail"] = reciptormail;
    $.ajax({
    type: "POST",
    url: "/sendtestmail",
    data: data,
    // timeout: 300000,
    beforeSend: function() {
    swal({
    title: "Mail sending...",
    button: false,
    closeOnClickOutside: true,
    closeOnEsc: false,
    timer: 3500,
    });
    },
    success: function(data) {
    if (data.statusCode == 200) {
    swal({
    title: data.message,
    icon: "success",
    timer: 3500,
    });
    } else {
    swal({
    title: data.message,
    icon: "error",
    timer: 3500,
    });
    }
    },
    error: function(jqXHR, textStatus, err) {
    alert("Please refresh !");
    },
    });
   });
   
   async function templateJson() {
    let templatejson = {};
    
    if (activeTemplate == "demo1") {
    templatejson = {
    templateid: "#demo1",
    Emailtitle: $(".Emailtitle").val(),
    demo1_headericon: $(".demo1_headericon").attr("src") ?
    $(".demo1_headericon").attr("src") :
    "",
    demo1banner1: imagedatabyClass("demo1banner1"),
    demo1leftimage: imagedatabyClass("demo1leftimage"),
    demo1rightimage: imagedatabyClass("demo1rightimage"),
   
    // demo1btn1url: $(".demo1btn1url").val(),
    // demo1_button1text: $(".demo1_button1text").val(),
    // demo1buttoncolor: $(".demo1buttoncolor").val(),
   
    demo1headercolor: $(".demo1headercolor").val(),
    demo1_imagesubtitleTitle1: $(".demo1_imagesubtitleTitle1").val(),
    demo1_emailsubtitle2: $(".demo1_emailsubtitle2").val(),
    demo1_bodytext: $(".demo1_bodytext").val(),
    demo1_maintext: $(".demo1_maintext").val(),
    demo1_imagetitle2: $(".demo1_imagetitle2").val(),
    demo1_imageTitle1: $(".demo1_imageTitle1").val(),
   
    demo2_buttontext1: $(".demo2_buttontext1").val(),
    demo2_buttontext2: $(".demo2_buttontext2").val(),
    demo2_button2color: $(".demo2_button2color").val(),
    demo2_button1color: $(".demo2_button1color").val(),
    demo2btn1url1: $(".demo2btn1url1").val(),
    demo2btn2url2: $(".demo2btn2url2").val(),
    facebookLinkTemp1: $(".facebookLinkTemp1").val(),
    instaLinkTemp1: $(".instaLinkTemp1").val(),
    linkedinLinkTemp1: $(".linkedinLinkTemp1").val(),
    emailTemp1: $(".emailTemp1").val(),
    mobileTemp1: $(".mobileTemp1").val(),
    };
    //console.log("templatejson111",templatejson);
   
    return templatejson;
    } else {
    
    id = "#demo2";
    templatejson = {
    templateid: "#demo2",
    Emailtitle: $(".Emailtitle").val(),
    demo2_headicon: $(".demo2_headicon").attr("src") ?
    $(".demo2_headicon").attr("src") :
    "",
    demo2bannerimage1: imagedatabyClass("demo2bannerimage1"),
    demo2bannerimage2: imagedatabyClass("demo2bannerimage2"),
    demo2bannertext: $(".demo2bannertext").val(),
    demo2bannersubtitle: $(".demo2bannersubtitle").val(),
    demo2_footertext: $(".demo2_footertext").val(),
    demo2headercolor: $(".demo2headercolor").val(),
   
    demo1btn1url: $(".demo1btn1url").val(),
    demo1_button1text: $(".demo1_button1text").val(),
    demo1buttoncolor: $(".demo1buttoncolor").val(),
    facebookLinkTemp2: $(".facebookLinkTemp2").val(),
    instaLinkTemp2: $(".instaLinkTemp2").val(),
    linkedinLinkTemp2: $(".linkedinLinkTemp2").val(),
    emailTemp2: $(".emailTemp2").val(),
    mobileTemp2: $(".mobileTemp2").val(),
    // demo2_buttontext1: $(".demo2_buttontext1").val(),
    // demo2_buttontext2: $(".demo2_buttontext2").val(),
    // demo2_button2color: $(".demo2_button2color").val(),
    // demo2_button1color: $(".demo2_button1color").val(),
    // demo2btn1url1: $(".demo2btn1url1").val(),
    // demo2btn2url2: $(".demo2btn2url2").val(),
    };
    //console.log("templatejson 222",templatejson);
    return templatejson;
    }
   }
   function imagedatabyClass(className) {
    let data = document.getElementsByClassName(className);
    if (data[1] && data[1].src.includes("data:image")) {
    return data[1].src;
    } else if (data[0] && data[0].src.includes("https")) {
    return data[0].src;
    } else {
    return data[0].src;
    }
   }
   let activeTemplate = 'demo1';
   function setActiveTab(tempName){
    activeTemplate = tempName ;
   }
   
   //***************WHATSAPP CAMPAIGNING JS CODE IS HERE************************************/
   //***************************************************************************************/
   
    $(document).on("click", "#onWhatsappSelect", function(e) {
   
    $.ajax({
    type: "POST",
    url: "/getWhatsappUserList",
    data: {},
    success: function(data) {
   data.groupcreated.reverse();
    if (data) {
    $("#userlistWhatsapp").html("");
    if (data.groupcreated.length < 1) {
    return;
    }
    $("#userlistWhatsapp").append('<li class="grup_sec">Groups </li>');
    let tempLocalStoreData = getTempDataFromLocalStor();
    if(tempLocalStoreData && tempLocalStoreData.dropdownData.length>0){
    let drpdowndata;
     let groupkeys = tempLocalStoreData.dropdownData.map((ele)=>{
         if(ele.type == 'group'){
            return ele.name
         }
    })
    for (j = 0; j < data.groupcreated.length; j++) {
    let objkey = Object.keys(data.groupcreated[j].createdgroup);
    // for(const iterator of tempLocalStoreData.dropdownData){
    // if(iterator.name == objkey[0]){
    if(groupkeys.includes(objkey[0])){
        drpdowndata = `<li> 
        <input type="checkbox" checked class="userdata1 ckbox1" data-type="group2" id =${data.groupcreated[j]._id} data-id=${
        data.groupcreated[j]._id} name="groupmail[]" data-phone='${JSON.stringify(data.groupcreated[j].createdgroup[objkey[0]])}' value="${objkey[0]}"><span> ${objkey[0]} </span>
        </li>`;
    }else{
        drpdowndata = `<li> 
        <input type="checkbox" class="userdata1 ckbox1" data-type="group2" id =${data.groupcreated[j]._id} data-id=${
        data.groupcreated[j]._id} name="groupmail[]" data-phone='${JSON.stringify(data.groupcreated[j].createdgroup[objkey[0]])}' value="${objkey[0]}"><span> ${objkey[0]} </span>
        </li>`;
    }
   
    $("#userlistWhatsapp").append(
    drpdowndata);
    // }
    }

    // user list with having phone
    let singleUserkeys = tempLocalStoreData.dropdownData.map((ele)=>{
        if(ele.type != 'group'){
           return ele.name
        }
   })

    // $("#userlistWhatsapp").append('<li class="groupheading">User list</li>');
    for (i = 0; i < data.data.length; i++) {
    let userdata;
    let id = data.data[i].userId ? data.data[i].userId : data.data[i]._id;
    // for (const iterator of tempLocalStoreData.dropdownData) {
    // if(iterator.name == data.data[i].name){
    if(singleUserkeys.includes(data.data[i].name)){

    userdata = '<li><input type="checkbox" checked class="ckbox1" data-type="user2" id="' +id +'" data-id ="' +id +'" data-mail="' +data.data[i].mail +'" data-phone="' +data.data[i].phone +'" value="' +data.data[i].name +'">' +"<span>"+ data.data[i].name +"</span>"+"</li>";
    } else {
    userdata = '<li><input type="checkbox" class="ckbox1" data-type="user2" id="' +id +'" data-id ="' +id +'" data-mail="' +data.data[i].mail +'" data-phone="' +data.data[i].phone +'" value="' +data.data[i].name +'">' +"<span>"+ data.data[i].name +"</span>"+"</li>";
    }
    // }
    $("#userlistWhatsapp").append(
    userdata
    )
    }
    } else {
    /** for new case without any data in localstorage */
    let drpdowndata;
    for (j = 0; j < data.groupcreated.length; j++) {
    let objkey = Object.keys(data.groupcreated[j].createdgroup);
    drpdowndata = `<li> 
    <input type="checkbox" class="userdata1 ckbox1" data-type="group2" id =${data.groupcreated[j]._id} data-id=${
    data.groupcreated[j]._id} name="groupmail[]" data-phone='${JSON.stringify(data.groupcreated[j].createdgroup[objkey[0]])}' value="${objkey[0]}"><span> ${objkey[0]} </span>
    </li>`;
    $("#userlistWhatsapp").append(
    drpdowndata);
    }
    
    // user list with having phone
    
    //$("#userlistWhatsapp").append('<li class="groupheading">User list</li>');
    // for (i = 0; i < data.data.length; i++) {
    // let userdata="";
    // let id = data.data[i].userId ? data.data[i].userId : data.data[i]._id
    // userdata = '<li><input type="checkbox" class="ckbox1" data-type="user2" id="' +id +'" data-id ="' +id +'" data-mail="' +data.data[i].mail +'" data-phone="' +data.data[i].phone +'" value="' +data.data[i].name +'">' +"<span>"+ data.data[i].name +"</span>"+"</li>";
    // $("#userlistWhatsapp").append(
    // userdata
    // )
    // }
    }
    }
   
    }, error: function (jqXHR, textStatus, err) {  
        if(jqXHR.responseJSON.statusCode == 403) {
        window.location.replace("https://admin.helloyubo.com/login");
        } else {
        alert("Something went wrong, please try again")
        }
      }
    });
   
   });
   
   
   // add approved template 
   selectedAgentIdArray =[];
   
   $(".createTemplate").click(function() {
    $('.js-example-basic-multiple').select2();
    var selectArr = $(".js-example-basic-multiple").select2().val();

    var data = {
    clientId: $(".clientId").val(),
    name: $("#tempName").val(),
    text: $("#tempText").val(),
    msg_type: $("#type_dropdown").val(),
    lang: $("#lang_dropdown").val(),
    header: ($("#type_dropdown").val()!='TEXT')?'':$(`#headerdata`).val(),
    footer: $(`#footerdata`).val(),
    button_url: $(`#buttonurldata`).val(),
    button: $(`#buttondata`).val(),
    agent_Id:selectArr
    };
   
    if(data.name == "") {
    alert('Enter template name')
    } else if(data.text == '') {
    alert(' Enter template data ')
    } else {
    
    $.ajax({
    data: data,
    async: false,
    cache: false,
    method: "POST",
    url: "/createWhatsappTemp",
    success: async function(data) {
   
    if (data.statusCode == 200) {
    // document.getElementById("closeModalManually2").setAttribute("data-dismiss", "modal");
    swal({
    title: data.message,
    icon: "success",
    timer: 3000,
    });
    document.getElementById("closeModalManually").setAttribute("data-dismiss", "modal");
    getWhatsappTempList();
    location.reload();
    } 
    else if (data.statusCode == 201) {
    swal({
    title: data.message,
    icon: "error",
    timer: 3500,
    });
    } else {
    swal({
    title: data.message,
    icon: "error",
    timer: 3500,
    });
    }
    }, error: function (jqXHR, textStatus, err) {  
        if(jqXHR.responseJSON.statusCode == 403) {
        window.location.replace("https://admin.helloyubo.com/login");
        } else {
        alert("Something went wrong, please try again")
        }
      }
    });
   
    }
    
   });
   
   // list approved template 
   
   function getWhatsappTempList() {
   
    $.ajax({
    async: false,
    cache: false,
    method: "POST",
    url: "/whatsappTemplateList",
    data: {},
    success: function(data) {
    if (data.statusCode == 200) {
    data.data.reverse();
    $("#temp_data").html("");
    for (const iterator of data.data) {
   
    let html = `
    <h3 class="head_temp" tempid=${iterator._id} >${iterator.name}</h3>

    <div class="row approve-content" id="temp_data">
    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 email-camp">
    <div class="temp_1" >
   
    <div class="tempuser_1" id=${iterator._id}>
   
    <p> ${iterator.text} </p>
   
    </div>
    
    </div>
   
    </div>
    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="    /* padding-right: 39px; */
    text-align: -webkit-right;
    /* padding-left: 15px; */
    margin-left: -19px;
    /* margin-right: 32px;">
   
    <div class="temp_detail_btn">
    <button class="temp_edit_btns" onclick="onEdit(id, this)"
    clientId=${iterator.clientId} id=${iterator._id}
    data-target="#edit_approve" data-toggle="modal"><i class='fa fa-edit'></i></button>
    <span class="my-group-action_span">|</span>

   
    <button class="temp_detail_btn1" clientId=${iterator.clientId} id=${iterator._id} 
    onclick="onDelete(id, this)" ><i class='fa fa-trash-o'></i></button>
   
    <button class="temp_detail_btn2" clientId=${iterator.clientId} id=${iterator._id} 
    onclick="onUse(id, this)">Apply</button>
   
    </div>
    
    </div>
    </div>
    <hr class="line_horizontal">
    ` 
   
    $("#temp_data").append(html);
    }
    
    } else if (data.statusCode == 203) {
    alert(data.message);
   
    
    }
   
    }, error: function (jqXHR, textStatus, err) {  
        if(jqXHR.responseJSON.statusCode == 403) {
        window.location.replace("https://admin.helloyubo.com/login");
        } else {
        alert("Something went wrong, please try again")
        }
      }
    });
   
   }
   
   getWhatsappTempList()
   
   // edit or update list approved template 
   clientId = ""
   tempId = ""
   
   
    function onEdit(id ,el) {
    selectedAgentIdArray=[];
    clientId = $(el).attr("clientId")
    tempId = id
   
    var data = {
    tempId: tempId,
    clientId: clientId
    }; 
   
    $.ajax({
    data: data,
    async: false,
    cache: false,
    method: "POST",
    url: '/getWhatsappTempById/',
    success:function(data) {
    if (data.statusCode == 200) {
    let agentData = data.data;
    //console.log("send agent",agentData)
    
    $(`#temp_text`).val(agentData.text)
    $(`#namedata`).val(agentData.name)
    $(`#type_dropdown_edit`).val(agentData.msg_type)
    $(`#lang_dropdown_edit`).val(agentData.lang)
    $(`#headerdataedit`).val(agentData.header)
    if(agentData.msg_type!='TEXT'){
        $(`#headerdataedit`).val('');
        $('.headerrow').hide();
    } else {
        $('.headerrow').show();
    }
    $(`#footerdataedit`).val(agentData.footer)
    let buttonurldata=(agentData.button_url==true)?"true":"false"
    let buttondata=(agentData.button==true)?"true":"false"
    $(`#buttonurldataedit`).val(buttonurldata)
    $(`#buttondataedit`).val(buttondata)
    if(buttondata=="false"){
        $(`#buttonurldataedit`).val("false");
        $('.buttonurlrow').hide();
    } else {
        $('.buttonurlrow').show();
    }
    var agentIdArray = agentData.agent_Id;
    $('.js-example-basic-multiple').val(agentIdArray).trigger("change");
    selectedAgentIdArray = agentIdArray;
   
    } else {
    swal({
    title: data.message,
    icon: "error",
    timer: 3000,
    });
    alert(data.message);
    }
    }, error: function (jqXHR, textStatus, err) {  
        if(jqXHR.responseJSON.statusCode == 403) {
        window.location.replace("https://admin.helloyubo.com/login");
        } else {
        alert("Something went wrong, please try again")
        }
      }
    })
   
   };
   
   // update approved template 
    function updateUser(){ 
    var data = {
    name: $(`#namedata`).val(),
    text: $(`#temp_text`).val(),
    msg_type: $(`#type_dropdown_edit`).val(),
    lang: $(`#lang_dropdown_edit`).val(),
    header: $(`#headerdataedit`).val(),
    footer: $(`#footerdataedit`).val(),
    button_url: $(`#buttonurldataedit`).val(),
    button: $(`#buttondataedit`).val(),
    clientId: clientId,
    tempId: tempId,
    agent_Id: selectedAgentIdArray
    };
   
    if(data.name == "") {
    alert('Enter template name')
    } else if(data.text == '') {
    alert(' Enter template data ')
    } else { 
    $.ajax({
    data: data,
    async: false,
    cache: false,
    method: "POST",
    url: "/updateWhatsappTemp",
    success: function(data) {
    
    if (data.statusCode == 200) {
    // document.getElementById("closeModalManually2").setAttribute("data-dismiss", "modal");
    swal({
    title: data.message,
    icon: "success",
    timer: 3000,
    });
    document.getElementById("closeModalManually1").setAttribute("data-dismiss", "modal");
    // $('#edit_approve').modal('hide')
    getWhatsappTempList()
    location.reload();
   
    } else {
    swal({
    title: data.message,
    icon: "error",
    timer: 3000,
    });
    }
    }, error: function (jqXHR, textStatus, err) {  
        if(jqXHR.responseJSON.statusCode == 403) {
        window.location.replace("https://admin.helloyubo.com/login");
        } else {
        alert("Something went wrong, please try again")
        }
      }
    }); 
    }
   
    };
   
    function onDelete(id, el) {
    // alert('Delete')
   
    clientId = $(el).attr("clientId")
    tempId = id
   
    var data = {
    clientId: clientId,
    tempId: tempId
    };
   
    swal({
    title: "Are you sure want to delete ?",
    text: "Once deleted, you will not be able to recover this record!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    }).then((willDelete) => {
    if (willDelete) {
    $.ajax({
    data: data,
    async: false,
    cache: false,
    method: 'POST',
    url: '/deleteWhatsappTemp',
    success: async function (data) {
    if (data.statusCode == 200) {
    await swal({
    title: "Data deleted successfully",
    icon: "success",
    timer: 3000
    });
   
    // $('#edit_agents').modal('hide');
    // getHumanAgentList();
    getWhatsappTempList()
   
    } else {
    alert(data.message)
    }
    }, error: function (jqXHR, textStatus, err) {  
        if(jqXHR.responseJSON.statusCode == 403) {
        window.location.replace("https://admin.helloyubo.com/login");
        } else {
        alert("Something went wrong, please try again")
        }
      }
    });
    } else {
    swal("Your data is safe now!");
    }
    });
    }
   
   
    (function () {
   
    var data = localStorage.getItem('savedTemplateData');
   
    var dataitem = JSON.parse(data)
   
   
    if(dataitem==null) {
        $('#onHide').hide()
    } else {
        $('#onHide').show()
        $(`#userText`).val(dataitem.templateData)
        $(`#msgtypeText`).val(dataitem.msgtypeData)
        $(`#headerText`).val(dataitem.headerData)
        $(`#mediaText`).val('')
        $(`#footerText`).val(dataitem.footerData)
        $(`#buttonurlText`).val('')
        if(dataitem.msgtypeData=='TEXT'){
            $(`#mediaText`).hide(); 
            $(`#mediaLabel`).hide(); 
            $(`#chooseMediaId`).hide();
            if(dataitem.headerData=='' || dataitem.headerData==undefined){
                $(`#headerText`).hide(); 
                $(`#headerLabel`).hide(); 
            } else {
                $(`#headerText`).show(); 
                $(`#headerLabel`).show(); 
            }
        } else {
            $(`#headerText`).hide(); 
            $(`#headerLabel`).hide(); 
            $(`#mediaText`).show(); 
            $(`#mediaLabel`).html(dataitem.msgtypeData+' URL'); 
            $(`#mediaLabel`).show();
            $(`#chooseMediaId`).show(); 
        }
        if(dataitem.footerData=='' || dataitem.footerData==undefined){
            $(`#footerText`).hide(); 
            $(`#footerLabel`).hide(); 
        } else {
            $(`#footerText`).show(); 
            $(`#footerLabel`).show(); 
        }
        if(dataitem.buttonurlData=="false" || dataitem.buttonurlData==false){
            $(`#buttonurlText`).hide(); 
            $(`#buttonurlLabel`).hide(); 
        } else {
            $(`#buttonurlText`).show(); 
            $(`#buttonurlLabel`).show(); 
        }
    }
    
    })();
   
    function onUse(id, el) {
    
    parametersArrayObj = [];
    parametersArray =[] ;
    clientId = $(el).attr("clientId")
    tempId = id
    // $('.no-selection').hide();
    $('#onHide').show()
    // $('.Approved_sec2').show();
   
    $('.tempuser_1').css({'border':''}) 
    $(`#${id}`).css({'border': '4px solid rgb(112, 173, 71)'})
    
   
    var data = {
    tempId: tempId,
    clientId: clientId
    }; 
   
    $.ajax({
    data: data,
    async: false,
    cache: false,
    method: "POST",
    url: '/getWhatsappTempById/',
    success:function(data) {
   
    if (data.statusCode == 200) {
    let templateData = data.data;
    //console.log('templateData',templateData)
   
    // $('.temp_detail_btn2').click(function() {
    // $('.head_temp').css({'color': 'red'})
    // })
    // $('#takeControl').attr('style', 'display:block')
    // $('.head_temp').css('color', 'red');
    // $('.head_temp').attr('tempid','value')
    // $("#tempid").css("font-weight","bold");
    // localStorage.setItem('savedTemplateData', templateData.text);
    // var x = localStorage.getItem("savedTemplateData");
    var existing = localStorage.getItem('savedTemplateData');
    existing = JSON.parse(existing);
    if(existing === null){
        savedTemplateData =
        {
        templateData : templateData.text,
        templateName : templateData.name,
        headerData : templateData.header,
        footerData : templateData.footer,
        msgtypeData:templateData.msg_type,
        buttonurlData : templateData.button_url,
        buttonData : templateData.button,
        dropdownData :[]
        }
        localStorage.setItem('savedTemplateData', JSON.stringify(savedTemplateData));
    }else{
      
    }

    localStorage.setItem('savedTemplateText',templateData.text );
    localStorage.setItem('templateName',templateData.name );
    localStorage.setItem('savedTemplateHeader',templateData.header );
    localStorage.setItem('savedTemplateFooter',templateData.footer );
    localStorage.setItem('savedTemplateButtonUrl',templateData.button_url );
    localStorage.setItem('savedTempLang',templateData.lang );
    localStorage.setItem('savedTemplateButton',templateData.button );
    localStorage.setItem('savedMsgType',templateData.msg_type );
    let savedTemplateText = localStorage.getItem('savedTemplateText');
    let savedTemplateHeader = localStorage.getItem('savedTemplateHeader');
    let savedTemplateFooter = localStorage.getItem('savedTemplateFooter');
    let savedTemplateButtonUrl = localStorage.getItem('savedTemplateButtonUrl');
    let savedTemplateButton = localStorage.getItem('savedTemplateButton');
    let savedTempLang = localStorage.getItem('savedTempLang');
    let savedMsgType = localStorage.getItem('savedMsgType');
    $(`#userText`).val(savedTemplateText)
    $(`#headerText`).val(savedTemplateHeader)
    $(`#msgtypeText`).val(savedMsgType)
    $(`#buttonText`).val(savedTemplateButton)
    $(`#tempLang`).val(savedTempLang)
    $(`#footerText`).val(savedTemplateFooter)
    $(`#buttonurlText`).val('')
    $(`#mediaText`).val('')
    if(savedMsgType=='TEXT'){
        $(`#mediaText`).hide(); 
        $(`#mediaLabel`).hide(); 
        $(`#chooseMediaId`).hide();
        if(savedTemplateHeader=='' || savedTemplateHeader==undefined){
            $(`#headerText`).hide(); 
            $(`#headerLabel`).hide(); 
        } else {
            $(`#headerText`).show(); 
            $(`#headerLabel`).show(); 
        }
    } else {
        $(`#headerText`).hide(); 
        $(`#headerLabel`).hide(); 
        $(`#mediaText`).show(); 
        $(`#mediaLabel`).html(savedMsgType+' URL'); 
        $(`#mediaLabel`).show();
        $(`#chooseMediaId`).show(); 
    }
    if(savedTemplateFooter=='' || savedTemplateFooter==undefined){
        $(`#footerText`).hide(); 
        $(`#footerLabel`).hide(); 
    } else {
        $(`#footerText`).show(); 
        $(`#footerLabel`).show(); 
    }
    if(savedTemplateButtonUrl=="false" || savedTemplateButtonUrl==false){
        $(`#buttonurlText`).hide(); 
        $(`#buttonurlLabel`).hide(); 
    } else {
        $(`#buttonurlText`).show(); 
        $(`#buttonurlLabel`).show(); 
    }
    } else {
    swal({
    title: data.message,
    icon: "error",
    timer: 3500,
    });
    alert(data.message);
    }
        let selectedTemplate = $(`#userText`).val();
        let headerText = $(`#headerText`).val();
        var isDynamicHeader = (headerText.match(/{{/g) || []).length;
        if(isDynamicHeader==1){
            $(`.headerParameterSection`).html('<input type = "text" class="headerParams" id="headerParams" placeholder="Enter value of header paramater {{1}}" onkeyup="setHeaderVal()" style="width:100% ;margin-top: 5px;">'); 
        } else {
            $(`.headerParameterSection`).html('')
        }
        //var numberOfBrckets = (selectedTemplate.match(/{{/g) || []).length;
        let paramsarr=selectedTemplate.split("}}").map(x=>x.split("{{")[1]).filter(Number)
        paramsarr=paramsarr.filter((item,index) => paramsarr.indexOf(item) === index);
        let numberOfBrckets=paramsarr.length
        if(numberOfBrckets>0){
            $('.parameterClass').show();
        }else{
            $('.parameterClass').hide();
        }
        $(`.parameterSection`).html(''); 
        for (i = 0; i < numberOfBrckets; i++) {
            parametersArray.push("{{"+`${i+1}`+"}}");
            $('.parameterSection').append(
            `<input type = "text" class="inputParams" id="inputParams${[i]}" placeholder="Enter value of {{${[i+1]}}}" onkeyup="setInputVal(inputParams${[i]},${[i]})" style="width:100% ;margin-top: 5px;"> <br>` 
            )
        }
    }, error: function (jqXHR, textStatus, err) {  
        if(jqXHR.responseJSON.statusCode == 403) {
        window.location.replace("https://admin.helloyubo.com/login");
        } else {
        alert("Something went wrong, please try again")
        }
      }
    })
   
    }
   
    //sending test whatsapp message to a number
    $(document).on("click", ".sendTestWhatsappMessage", async function() {
    let userText = $(`#userText`).val() ;
    let testMobile = $(`#testMobile`).val() ;
    let templateName = localStorage.getItem("templateName");
    if(!userText || userText == 'undefined' || userText == null ){
    swal({
    title: "Please select a template to send ",
    icon: "warning",
    timer: 3500,
    });
    return ;
    }
    if(!testMobile || testMobile == 'undefined' || testMobile == null ){
        swal({
        title: "Please enter test mobile number ",
        icon: "warning",
        timer: 3500,
        });
        return ;
    }
    let data ={
    "send_to" : $(`#testMobile`).val(),
    "msg" : $(`#userText`).val(),
    "msg_type":$(`#msgtypeText`).val().trim(),
    "lang":$(`#tempLang`).val().trim(),
    "header":$(`#headerText`).val().trim(),
    "footer":$(`#footerText`).val().trim(),
    "buttonUrlParam":$(`#buttonurlText`).val().trim(),
    "headerParam":$(`#headerParams`).val(),
    "button":$(`#buttonText`).val().trim(),
    "media_url":$(`#mediaText`).val().trim(),
    "parameters":parametersArray,
    "parametersArrayObj": parametersArrayObj,
    "templateName" : templateName,
    "clientId": $(".clientId").val(),
    }
    
    if($(`#msgtypeText`).val().trim() == "VIDEO" && $(`#mediaText`).val().trim()==''){
        swal({
            title: "Please enter video url ",
            icon: "warning",
            timer: 4500,
            });
            return ;
    }
    if($(`#msgtypeText`).val().trim() == "IMAGE" && $(`#mediaText`).val().trim()==''){
        swal({
            title: "Please enter image url ",
            icon: "warning",
            timer: 4500,
            });
            return ;
    }
    if($(`#msgtypeText`).val().trim() == "DOCUMENT" && $(`#mediaText`).val().trim()==''){
        swal({
            title: "Please enter document url ",
            icon: "warning",
            timer: 4500,
            });
            return ;
    }

    let clientId = $(".clientId").val()
    $.ajax({
    contentType: 'application/json',
    dataType: 'json',
    type: "POST",
    // url: "/sendWhatsAppCampaign",
    url: "https://campaign.helloyubo.com/sendWaCampaignToParticularUser",
    data: JSON.stringify(data),
    // timeout: 300000,
    beforeSend: function() {
    swal({
    title: "Sending message...",
    button: false,
    closeOnClickOutside: true,
    closeOnEsc: false,
    });
    },
    success: function(data) {
    if (data.statusCode == 200) {
    swal({
    title: data.message,
    icon: "success",
    });
   
    } else {        
        if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 131009 && data.message.error.error_subcode == 2494010) {
            swal({
                    title: "Invalid phone number or country code is missing, please check and try again.",
                    icon: "error",
                });
        }else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 131009 && data.message.error.error_subcode == 2494073) {
            swal({
                    title: "Template does not have button url parameter, please remove and try again.",
                    icon: "error"
                });
        }else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 190 ) {
            swal({
                    title: "Invalid access token or access token has expired, Please provide valid access token.",
                    icon: "error"
                });
        }else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 100) {
            swal({
                    title: "Invalid WABA credential, Please provide valid registered info in settings > Whatsapp settings.",
                    icon: "error"
                });
        }else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 132001) {
            swal({
                    title: "Template name does not exist, please use an approved template for whatsapp campaigning.",
                    icon: "error",
                    timer: 5500,
                });
        } else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 132000) {
            swal({
                    title: "Please provide parameters value",
                    icon: "error",
                    timer: 5500,
                });
        } else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 131008) {
            swal({
                    title: "Button url missing, Please provide button url.",
                    icon: "error",
                    timer: 5500,
                });
        } else if (data.statusCode == 201 && data.res !=undefined) {
            let msg="Button url missing, Please provide button url."
            if(data.res.error!=undefined){
                msg=data.res.error.message
            }
            swal({
                    title: msg,
                    icon: "error",
                    timer: 5500,
                });
        }else{
            swal({
                    title: data.message,
                    icon: "error",
                    timer: 5500,
                });
        } 
    }
    }, error: function (jqXHR, textStatus, err) {  
        if(jqXHR.responseJSON.statusCode == 403) {
        window.location.replace("https://admin.helloyubo.com/login");
        } else {
        alert("Something went wrong, please try again")
        }
      }
    });
    });
   
    //sending whatsapp message to multiple of users
    $(document).on("click", ".sendWhatsCampaigningToMultiUser", async function() {
        let creategroup = [],
            selectedUserWithGroup=[],
            parentobj = {};
            $(".parameterSection").each(function() 
            { 
             accountcodes = $(this).val();   
             console.log('accountcodes', accountcodes)
            })
        $(".parameterSection").each(function(i) {
            let groupobj = {
                userId: $(this).find(".userdataid").attr("data-id"),
                name: $(this).find(".userdataname").attr("data-name"),
                mail: $(this).find(".userdatamail").attr("data-mail"),
                phone: $(this).find(".userdataphone").attr("data-phone"),
            };
            creategroup.push(groupobj);
        });
    let userListArray = [];
    let userText = $(`#userText`).val() ;
    if(!userText || userText == 'undefined' || userText == null ){
    swal({
    title: "Please select a template to send ",
    icon: "warning",
    timer: 3500,
    });
    return ;
    }
    // let userPhoneArray = getPhoneOfUser();
    
    var existing = localStorage.getItem('savedTemplateData');
    if(existing === null){
    let obj = {
    dropdownData :[],
    templateData : "",
    headerData : "",
    footerData : "",
    msgtypeData:"TEXT",
    buttonurlData : ""
    }
    localStorage.setItem('savedTemplateData', JSON.stringify(obj));
    }else{
    let userList = JSON.parse(existing) ;
    
    userList.dropdownData.map(ele=>{
        if(ele.type == 'group'){
                ele.phone.map(item=>{
                let obj2 = {};
                let name = ele.name.replaceAll(' ', '_').trim();
                obj2.groupName = name;
                obj2.phone = item.phone ;   
                selectedUserWithGroup.push(obj2);
             })
        }else{
            let name = ele.name.replaceAll(' ', '_').trim(); 
            let obj = {};
            obj.groupName = name;
            obj.phone = ele.phone ;  
            selectedUserWithGroup.push(obj);
        }
    });

    let userGroupData = userList.dropdownData.filter(ele=> ele.type == 'group');
    for(let data of userGroupData){
    for(let ele of data.phone){
    userListArray.push(ele);
    }
    }
    let singleUserData = userList.dropdownData.filter(ele=> ele.type != 'group');
    for(let data of singleUserData){
    userListArray.push(data);
    }
    
    }
    
    if(userListArray.length <= 0){
    swal({
    title: "Please select group or user from dropdown",
    icon: "warning",
    timer: 3500,
    });
    return ;
    }
    let templateName = localStorage.getItem("templateName");
    let data ={
    "selectedUserWithGroup" : selectedUserWithGroup,
    "send_to" : userListArray ,
    "msg" : $(`#userText`).val(),
    "msg_type":$(`#msgtypeText`).val().trim(),
    "lang":$(`#tempLang`).val().trim(),
    "header":$(`#headerText`).val().trim(),
    "footer":$(`#footerText`).val().trim(),
    "buttonUrlParam":$(`#buttonurlText`).val().trim(),
    "headerParam":$(`#headerParams`).val(),
    "button":$(`#buttonText`).val().trim(),
    "media_url":$(`#mediaText`).val().trim(),
    "parameters":parametersArray,
    "parametersArrayObj": parametersArrayObj,
    "templateName":templateName,
    "clientId": $(".clientId").val(),
    }

    var resArr = [];
    selectedUserWithGroup.filter(function(item){
    var i = resArr.findIndex(x => (x == item.groupName));
    if(i <= -1){
            resArr.push(item.groupName);
    }
    return null;
    });
    let newData = {
        "clientId" : $(".clientId").val(),
        "groupName" : resArr,
        "buttonUrlParam": $(`#buttonurlText`).val().trim(),
        "headerParam":$(`#headerParams`).val(),
        "media_url": $(`#mediaText`).val().trim(),
        "templateName": templateName,
        "parameters": parametersArray,
        "lang":$(`#tempLang`).val().trim(),
    }

    if($(`#msgtypeText`).val().trim() == "VIDEO" && $(`#mediaText`).val().trim()==''){
        swal({
            title: "Please enter video url ",
            icon: "warning",
            timer: 4500,
            });
            return ;
    }
    if($(`#msgtypeText`).val().trim() == "IMAGE" && $(`#mediaText`).val().trim()==''){
        swal({
            title: "Please enter image url ",
            icon: "warning",
            timer: 4500,
            });
            return ;
    }
    if($(`#msgtypeText`).val().trim() == "DOCUMENT" && $(`#mediaText`).val().trim()==''){
        swal({
            title: "Please enter document url ",
            icon: "warning",
            timer: 4500,
            });
            return ;
    }
    let clientId = $(".clientId").val()

    $.ajax({
    contentType: 'application/json',
    dataType: 'json',
    type: "POST",
    url: "https://campaign.helloyubo.com/sendWaCampaignToGroups",
    data: JSON.stringify(newData),
    // timeout: 300000,
    beforeSend: function() {
    swal({
    title: "Sending message...",
    button: false,
    closeOnClickOutside: true,
    closeOnEsc: false,
    });
    },
    success: function(data) {
        if (data.statusCode == 200) {
            let msgFailed = [];
            if(data.sendMsgStatus && data.sendMsgStatus.length > 0){
              data.sendMsgStatus.map(ele=>{
                 if(ele.status == 'rejected')
                  msgFailed.push(ele)
            })
            swal({
                title: data.message,
                icon: "success",
                text: `${msgFailed.length} whatsApp messages could not send due to Invalid number or missing country code.` ,
                type: 'success',
                confirmButtonText: 'OK'
              }).then(() => {
                console.log('Message sent successfully ');
              });
           }else{
            swal({
                title: data.message,
                icon: "success",
                type: 'success',
                confirmButtonText: 'OK'
              }).then(() => {
                console.log('Message sent successfully ');
              });
           }
        }else{
                if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 131009 && data.message.error.error_subcode == 2494010) {
                    swal({
                            title: "Invalid phone number or country code is missing, please check and try again.",
                            icon: "error",
                        });
                }else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 131009 && data.message.error.error_subcode == 2494073) {
                    swal({
                            title: "Template does not have button url parameter, please remove and try again.",
                            icon: "error"
                        });
                }else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 190 ) {
                    swal({
                            title: "Invalid access token or access token has expired, Please provide valid access token.",
                            icon: "error"
                        });
                }else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 100) {
                    swal({
                            title: "Invalid WABA credential, Please provide valid registered info in settings > Whatsapp settings.",
                            icon: "error"
                        });
                } else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 132001) {
                    swal({
                            title: "Template name does not exist, please use an approved template for whatsapp campaigning.",
                            icon: "error",
                            timer: 5500,
                        });
                } else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 132000) {
                    swal({
                            title: "Please provide parameters value.",
                            icon: "error",
                            timer: 5500,
                        });
                } else if (data.statusCode == 201 && data.message && data.message.error && data.message.error.code == 131008) {
                    swal({
                            title: "Button url missing, Please provide button url.",
                            icon: "error",
                            timer: 5500,
                        });
                } else if (data.statusCode == 201 && data.message && data.error && data.error.error.code == 132001) {
                    swal({
                            title: "Template has incorrect language",
                            icon: "error",
                            timer: 5500,
                        });      
                } else{
                        swal({
                            title: data.message,
                            icon: "error",
                            text: `` ,
                            type: 'success',
                            confirmButtonText: 'OK'
                        }).then(() => {
                        });
                } 
        }
       
    
    }, error: function (jqXHR, textStatus, err) {  
        if(jqXHR.responseJSON.statusCode == 403) {
        window.location.replace("https://admin.helloyubo.com/login");
        } else {
        alert("Something went wrong, please try again")
        }
    }
    });
    });
   
function getPhoneOfUser() {
    let phonearray = [];
    $(".rec_phone").each(function() {
        if ($(this).attr("usertype") == "group2") {
            let phonedata = JSON.parse($(this).attr("data-phone"));
            for (i = 0; i < phonedata.length; i++) {
                phonearray.push(phonedata[i].phone);
            }
        } else {
            phonearray.push($(this).attr("data-phone"));
        }
    });
    /**for uique phone from array */
    let unique = [...new Set(phonearray)];
    return unique;
}
   
function getTempDataFromLocalStor(){
    templatedat = localStorage.getItem('savedTemplateData');
    return JSON.parse(templatedat);
}
   
   
$(document).ready(function(){
    let savedTemplateText = localStorage.getItem('savedTemplateText');
    let savedTemplateHeader = localStorage.getItem('savedTemplateHeader');
    let savedTemplateFooter = localStorage.getItem('savedTemplateFooter');
    let savedTemplateButtonUrl = localStorage.getItem('savedTemplateButtonUrl');
    let savedTempLang = localStorage.getItem('savedTempLang');
    let savedTemplateButton = localStorage.getItem('savedTemplateButton');
    let savedMsgType = localStorage.getItem('savedMsgType');
    $(`#userText`).val(savedTemplateText)
    //var numberOfBrckets = (savedTemplateText.match(/{{/g) || []).length;
    let paramsarr=savedTemplateText.split("}}").map(x=>x.split("{{")[1]).filter(Number)
    paramsarr=paramsarr.filter((item,index) => paramsarr.indexOf(item) === index);
    let numberOfBrckets=paramsarr.length
    if(numberOfBrckets>0){
        $('.parameterClass').show();
    }else{
        $('.parameterClass').hide();
    }
    $(`.parameterSection`).html(''); 
    parametersArray=[];
    for (i = 0; i < numberOfBrckets; i++) {
        parametersArray.push("{{"+`${i+1}`+"}}");
        $('.parameterSection').append(
        `<input type = "text" class="inputParams" id="inputParams${[i]}" placeholder="Enter value of {{${[i+1]}}}" onkeyup="setInputVal()" style="width:100% ;margin-top: 5px;"> <br>` 
        )
    }

    var isDynamicHeader = (savedTemplateHeader.match(/{{/g) || []).length;
    if(isDynamicHeader==1){
        $(`.headerParameterSection`).html('<input type = "text" class="headerParams" id="headerParams" placeholder="Enter value of {{1}}" onkeyup="setHeaderVal()" style="width:100% ;margin-top: 5px;">'); 
    } else {
        $(`.headerParameterSection`).html('')
    }

    $(`#headerText`).val(savedTemplateHeader)
    $(`#msgtypeText`).val(savedMsgType)
    $(`#buttonText`).val(savedTemplateButton)
    $(`#tempLang`).val(savedTempLang)
    $(`#footerText`).val(savedTemplateFooter)
    $(`#buttonurlText`).val('')
    $(`#mediaText`).val('')
    if(savedMsgType=='TEXT'){
        $(`#mediaText`).hide(); 
        $(`#mediaLabel`).hide(); 
        $(`#chooseMediaId`).hide();
        if(savedTemplateHeader=='' || savedTemplateHeader==undefined){
            $(`#headerText`).hide(); 
            $(`#headerLabel`).hide(); 
        } else {
            $(`#headerText`).show(); 
            $(`#headerLabel`).show(); 
        }
    } else {
        $(`#headerText`).hide(); 
        $(`#headerLabel`).hide(); 
        $(`#mediaText`).show(); 
        $(`#mediaLabel`).html(savedMsgType+' URL'); 
        $(`#mediaLabel`).show(); 
        $(`#chooseMediaId`).show();
    }
    if(savedTemplateFooter=='' || savedTemplateFooter==undefined){
        $(`#footerText`).hide(); 
        $(`#footerLabel`).hide(); 
    } else {
        $(`#footerText`).show(); 
        $(`#footerLabel`).show(); 
    }
    if(savedTemplateButtonUrl=="false" || savedTemplateButtonUrl==false){
        $(`#buttonurlText`).hide(); 
        $(`#buttonurlLabel`).hide(); 
    } else {
        $(`#buttonurlText`).show(); 
        $(`#buttonurlLabel`).show(); 
    }

    let tempLocalStoreData = getTempDataFromLocalStor();
    if(tempLocalStoreData && tempLocalStoreData.dropdownData.length>0){
        $(".created_entity1").html('')
        $(".created_entity1").show();
        $(".created_entity1").css("display", "block", "width", "500px");
        for (const iterator of tempLocalStoreData.dropdownData) {
            let campaign_tag1 = `<span class="campaign_tags1" tag-id='${iterator.id}'><span class="campaign_entitiesTag rec_phone" usertype='${iterator.type}' data-phone='${iterator.phone}'>${
            iterator.name
            }</span><img class="campaign_entity-close" entity_id='${iterator.id}' src="dist/img/close-button.png" alt="close-button"></span>`;
            $(".created_entity1").append(campaign_tag1);
        }
    } else {
        $(".created_entity1").css("display", "none");
        return;
    }

    //** on page load create entity from local  storage selected data  */
    let tempLocalStoreData2 = getMailLocalstorageUserData();
    if(tempLocalStoreData2 && tempLocalStoreData2.dropdownEmailData.length>0){
        $(".created_entity").html('')
        $(".created_entity").show();
        $(".created_entity").css("display", "block", "width", "500px");
        for (const iterator of tempLocalStoreData2.dropdownEmailData) {
        let campaign_tag1 = `<span class="campaign_tags" tag-id='${iterator.id}'><span class="campaign_entitiesTag rec_phone" usertype='${iterator.type}' data-phone='${iterator.phone}'>${
        iterator.name
        }</span><img class="campaign_entity-close" entity_id='${iterator.id}' src="dist/img/close-button.png" alt="close-button"></span>`;
        $(".created_entity").append(campaign_tag1);
        }
    } else {
        $(".created_entity").css("display", "none");
        return;
    }
})

// $(document).ready(function(){
//     $('.Approved_sec2').hide();
// })
function Clearall(){
    swal({
        title: "Are you sure want to clear all saved template data ?",
        text: "Once cleared, you will not be able to recover this record!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        }).then((willDelete) => {
        if (willDelete) {
            $('#onHide').hide()
            $(`#userText`).val('')
            localStorage.removeItem('savedTemplateText');
            localStorage.removeItem('savedTemplateHeader');
            localStorage.removeItem('savedTemplateFooter');
            localStorage.removeItem('savedTemplateButtonUrl');
            localStorage.removeItem('savedTemplateButton');
            localStorage.removeItem('savedTempLang');
            localStorage.removeItem('savedMsgType');
            $(".created_entity1").html('')
            let obj = {
                        dropdownData :[],
                        templateData : "",
                        headerData : "",
                        footerData : "",
                        msgtypeData:"TEXT",
                        buttonurlData : ""
                    }
                localStorage.setItem('savedTemplateData', JSON.stringify(obj));
                singleUserArray = [];
        } else {
            swal("Your data is safe now!");
        }
    });   
}
function getMailLocalstorageUserData(){
    let savedTemplateText = localStorage.getItem('savedEmailData');
    return JSON.parse(savedTemplateText); 
} 
function clearTextboxOnAdd(){
    $("#tempName").val('');
    $("#tempText").val('');
    $(`#headerText`).val('')
    $(`#footerText`).val('')
    $(`#buttonurlText`).val("false")
    $(`#buttonText`).val("false")
    $('.headerrow').show();
    $('.buttonurlrow').hide();
}
function typeSelection(obj){
    if(obj.value=='TEXT'){
        $('.headerrow').show();
    } else {
        $('.headerrow').hide();
    }
}
function selectButton(obj){
    if(obj.value=='true'){
        $('.buttonurlrow').show();
    } else {
        $('.buttonurlrow').hide();
    }
}
let parametersArray = [];
let parametersArrayObj = [];

function setInputVal(){
    parametersArray = [];
    parametersArrayObj = [];
    var OriginalTemp = localStorage.getItem('savedTemplateText');

    var valueArray = $('.inputParams').map(function(index,currentElement) {
        if(this.value == '' || this.value == 'undefined' ){
            let obj = {
                ["{{"+`${index+1}`+"}}"]  : "{{"+`${index+1}`+"}}" 
            }
            parametersArrayObj.push(obj);
            return  "{{"+`${index+1}`+"}}" ;
        }else{
            let obj = {
                ["{{"+`${index+1}`+"}}"] : this.value
            }
            parametersArrayObj.push(obj);
            return this.value;
        }
    }).get();
    parametersArray = valueArray;
    let msg=replaceParams(OriginalTemp,parametersArray)
    $(`#userText`).val(msg);   
}

function setHeaderVal(){
    var OriginalHeader = localStorage.getItem('savedTemplateHeader');
    let item=$(`#headerParams`).val()
    let msg=replaceParams(OriginalHeader,[item])
    console.log("message",msg)
    $(`#headerText`).val(msg);   
}

$("#search").keyup(function () {
    var data = this.value.split(" ");
    console.log(data);
    var jo = $("#userlistWhatsapp").find("li");
    if (this.value == "") {
        jo.show();
        return;
    }
    jo.hide();
    jo.filter(function (i, v) {
        var $t = $(this);
		var matched = true;
        for (var d = 0; d < data.length; ++d) {
            var value = "";
            $t.find("span").each(function(){
                var li = $(this);               
                value += li.text();
            });
			if (data[d].match(/^\s*$/)) {
				continue;
			}
			var regex = new RegExp(data[d].toLowerCase());
			if (value.toLowerCase().replace("").match(regex) === null) {
                matched = false;
            }
        }
        return matched;
    })
    .show();
}).focus(function () {
    this.value = "";
    $(this).css({
        "color": "black"
    });
    $(this).unbind('focus');
}).css({
    "color": "yellow"
});

$("#search2").keyup(function () {
    var data = this.value.split(" ");
    console.log(data);
    var jo = $("#userlist").find("li");
    if (this.value == "") {
        jo.show();
        return;
    }
    jo.hide();
    jo.filter(function (i, v) {
        var $t = $(this);
		var matched = true;
        for (var d = 0; d < data.length; ++d) {
            var value = "";
            $t.find("span").each(function(){
                var li = $(this);               
                value += li.text();
            });
			if (data[d].match(/^\s*$/)) {
				continue;
			}
			var regex = new RegExp(data[d].toLowerCase());
			if (value.toLowerCase().replace("").match(regex) === null) {
                matched = false;
            }
        }
        return matched;
    })
    .show();
}).focus(function () {
    this.value = "";
    $(this).css({
        "color": "black"
    });
    $(this).unbind('focus');
}).css({
    "color": "yellow"
});

function replaceParams(msg, params){
    let count=1;
    if(params && params.length>0){
        params.forEach(function(item){
            msg = msg.replaceAll("{{"+count+"}}",item);
            count ++;
        })
    }
    return msg;
}

function chooseMedia() {
    var fileInput = document.getElementById('chooseMedia');
    var file = fileInput.files[0];
    var formData = new FormData()
    formData.append("whatsappMedia", file);

    if(!file) {
          swal({
              title: "Please select file",
              icon: "error",
              timer: 3500,
          });

      } else {
          var fileType = (file.type).split("/");

          if(fileType[0]=='image') {
              if(file.size<=(5000000)) {
                  chooseMediaAjaxCall()
              } else {
                  swal({
                      title: "Please select image less than 5mb",
                      icon: "error",
                      timer: 3500,
                  })
              }
              
          } else if(fileType[0]=='application') {
              if(file.size<=(100000000)) {
                  chooseMediaAjaxCall()
              } else {
                  swal({
                      title: "Please select documents less than 100mb",
                      icon: "error",
                      timer: 3500,
                  })
              }
  
          } else if(fileType[0]=='video') {
              if(file.size<=(16000000)) {
                  chooseMediaAjaxCall()
              } else {
                  swal({
                      title: "Please select video less than 16mb",
                      icon: "error",
                      timer: 3500,
                  })
          }
      }     
  }       
}

function chooseMediaAjaxCall() {

    var fileInput = document.getElementById('chooseMedia');
    var file = fileInput.files[0];
    var formData = new FormData()
    formData.append("whatsappMedia", file);
    formData.append("tempName",localStorage.getItem("templateName"));

  $.ajax({
      method: "POST",
      url: "/uploadMedia",
      data: formData,
      async: false,
      cache: false,
      contentType: false,
      processData: false,
      success: async function(data) {
        if(data.statusCode == 200)  {
            $('#mediaText').val(data.s3_url)
        }  
      },
      error: function(jqXHR, textStatus, err) {
          console.log('errr', err);
      alert("Please refresh !");
      },
 });

}

function previewImg() {
  var fileInput = document.getElementById('chooseMedia');
  var previewImg = document.getElementById('previewImg');
    var file = fileInput.files[0]
    var fileType = (file.type).split("/");

      if(fileType[0]=='image') {
          previewImg.src = URL.createObjectURL(file)
          $('#previewImg').css({"width": "150px", "height": "150px","margin-left":"35%"})
          
        } else if(fileType[0]=='application'){
            previewImg.src = 'dist/img/document.png'
            $('#previewImg').css({"width": "150px", "height": "150px","margin-left":"35%"})
            //return;

        } else if(fileType[0]=='video'){
            previewImg.src = 'dist/img/video-button.png'
            $('#previewImg').css({"width": "150px", "height": "150px","margin-left":"35%"})
           // return;
        } else {
            previewImg.src = '' 
        }
}

// human agent list showing into the edit approved template
function agentListToEditApprovedTemp(){
    
    $.ajax({
        // data: data,
        async: false,
        cache: false,
        method: "GET",
        url: "/findHumanAgent",
        success: function(data) {
            if(data.statusCode==200){
                if(data.data[0]!=undefined){
                    for(let i=0;i<data.data[0].agents.length;i++){
                     let objvals = Object.values(data.data[0].agents);
                   $(".myselect").append(`<option value="${objvals[i]._id}" >${objvals[i].name}</option>`);
                   }
                }
       $('.js-example-basic-multiple').select2();
            }
        },error: function (jqXHR, textStatus, err) {  
        alert("someting went wrong,please try again");
        }
    });
};

// find the current  logedId client
function currentLogedInClient(){
    $.ajax({
        data:{},
        async:false,
        caches:false,
        method:"GET",
        url:"/currentLogedInClientData",
        success:function(data){
            if(data.statusCode==200){
                if(data.data.isHumanAgentEnabled===true){
                    $("#agentContainerOnEditTemp").show();
                    $("#agentContainerOnAddTemp").show();
                }else{
                    $('#agentContainerOnEditTemp').empty();
                    $("#agentContainerOnAddTemp").empty();
                }
            }
        },
        error:function(jqXHR,textStatus,err){
            alert("someting went wrong,please try again");

        }
    })
}
$(document).ready(function() {
    document.getElementById("whatsapp-Campaigning").style.display = 'block';
    $('.js-example-basic-single').select2();
    currentLogedInClient();
    agentListToEditApprovedTemp();
});

// on select of agent from select 2 dropdown
$('.js-example-basic-multiple').on('select2:select', function (e) {
    let data = e.params.data.id;
    selectedAgentIdArray.push(data);
    selectedAgentIdArray = [...new Set(selectedAgentIdArray)];

});
// on unselect of agent from select 2 dropdown
$('.js-example-basic-multiple').on('select2:unselect', function (e) {
    let data = e.params.data.id;
    selectedAgentIdArray = [...new Set(selectedAgentIdArray)];
    
    const index = selectedAgentIdArray.indexOf(data);
    if (index > -1) { // only splice array when item is found
        selectedAgentIdArray.splice(index, 1); // 2nd parameter means remove one item only
    }

});


async function ScheduledCampaign() {

    let creategroup = [],
            selectedUserWithGroup=[],
            parentobj = {};
            $(".parameterSection").each(function() 
            { 
             accountcodes = $(this).val();   
             console.log('accountcodes', accountcodes)
            })
        $(".parameterSection").each(function(i) {
            let groupobj = {
                userId: $(this).find(".userdataid").attr("data-id"),
                name: $(this).find(".userdataname").attr("data-name"),
                mail: $(this).find(".userdatamail").attr("data-mail"),
                phone: $(this).find(".userdataphone").attr("data-phone"),
            };
            creategroup.push(groupobj);
        });
    let userListArray = [];
    let userText = $(`#userText`).val() ;
    if(!userText || userText == 'undefined' || userText == null ){
    swal({
    title: "Please select a template to send ",
    icon: "warning",
    timer: 3500,
    });
    return ;
    }
    // let userPhoneArray = getPhoneOfUser();
    
    var existing = localStorage.getItem('savedTemplateData');
    if(existing === null){
    let obj = {
    dropdownData :[],
    templateData : "",
    headerData : "",
    footerData : "",
    msgtypeData:"TEXT",
    buttonurlData : ""
    }
    localStorage.setItem('savedTemplateData', JSON.stringify(obj));
    }else{
    let userList = JSON.parse(existing) ;
    
    userList.dropdownData.map(ele=>{
        if(ele.type == 'group'){
                ele.phone.map(item=>{
                let obj2 = {};
                obj2.groupName = ele.name;
                obj2.phone = item.phone ;   
                selectedUserWithGroup.push(obj2);
             })
        }else{
            let name = ele.name.replaceAll(' ', '_'); 
            let obj = {};
            obj.groupName = name;
            obj.phone = ele.phone ;  
            selectedUserWithGroup.push(obj);
        }
    });

    let userGroupData = userList.dropdownData.filter(ele=> ele.type == 'group');
    for(let data of userGroupData){
    for(let ele of data.phone){
    userListArray.push(ele);
    }
    }
    let singleUserData = userList.dropdownData.filter(ele=> ele.type != 'group');
    for(let data of singleUserData){
    userListArray.push(data);
    }
    
    }
    
    if(userListArray.length <= 0){
    swal({
    title: "Please select group or user from dropdown",
    icon: "warning",
    timer: 3500,
    });
    return ;
    }
    let templateName = localStorage.getItem("templateName");
    let data ={
    "selectedUserWithGroup" : selectedUserWithGroup,
    "send_to" : userListArray ,
    "msg" : $(`#userText`).val(),
    "msg_type":$(`#msgtypeText`).val().trim(),
    "lang":$(`#tempLang`).val().trim(),
    "header":$(`#headerText`).val().trim(),
    "footer":$(`#footerText`).val().trim(),
    "buttonUrlParam":$(`#buttonurlText`).val().trim(),
    "headerParam":$(`#headerParams`).val(),
    "button":$(`#buttonText`).val().trim(),
    "media_url":$(`#mediaText`).val().trim(),
    "parameters":parametersArray,
    "parametersArrayObj": parametersArrayObj,
    "templateName":templateName
    }
    
    if($(`#msgtypeText`).val().trim() == "VIDEO" && $(`#mediaText`).val().trim()==''){
        swal({
            title: "Please enter video url ",
            icon: "warning",
            timer: 4500,
            });
            return ;
    }
    if($(`#msgtypeText`).val().trim() == "IMAGE" && $(`#mediaText`).val().trim()==''){
        swal({
            title: "Please enter the Media URL and/or variables' values.",
            icon: "warning",
            timer: 4500,
            });
            return ;
    }
    if($(`#msgtypeText`).val().trim() == "DOCUMENT" && $(`#mediaText`).val().trim()==''){
        swal({
            title: "Please enter document url ",
            icon: "warning",
            timer: 4500,
            });
            return ;
    }
    if($("#frmSaveOffice_startdt").val()=='') {
        swal({
            title: "Please enter date and time ",
            icon: "warning",
            timer: 4500,
            });
            return ;
    }

    const groups = []
    for(let obj in selectedUserWithGroup) {
        if(selectedUserWithGroup[obj].groupName!=undefined || selectedUserWithGroup[obj].groupName!='' ){
            groups.push(selectedUserWithGroup[obj].groupName)
        } 
        
    }

    const newGroups = [...new Set(groups)];
    const savedMsgType = localStorage.getItem('savedMsgType')
    const frequency = $("#frequency option:selected").val()

    const createCampaignObj = {
        "clientId": "", 
        "templateName" : data.templateName,
        "groups" : newGroups,
        "buttonUrlParam": data.buttonUrlParam,
        "headerParam":data.headerParam,
        "media_url": data.media_url,
        "parameters": data.parameters,
        "scheduledAt": new Date($("#frmSaveOffice_startdt").val()),
        "status": 'Scheduled',
        "savedMsgType": savedMsgType,
        "frequency": frequency,
        "lang":data.lang
    }

    $.ajax({
        method: "POST",
        url: "/createCampaign",
        data: createCampaignObj,
        success: async function(data) {
          if(data.statusCode == 200)  {
            // $('#scheduled-campaign-model').modal('hide');
            if(data.data.upsertedCount ==1) {
                swal({
                    title: "Campaign Scheduled Successfully!",
                    icon: "success",
                    timer: 3500,
                });
            } else {
                swal({
                    title: "Campaign already scheduled on this time!",
                    icon: "warning",
                    timer: 3500,
                });
            }
          } else {
            swal({
                title: data.message,
                icon: "warning",
            });
          }
          
        },
        error: function(jqXHR, textStatus, err) {
            console.log('errr', err);
        alert("Please refresh !");
        },
   });
  
}

async function scheduledCampaignList() {
    
    $.ajax({
        method:'GET',
        url:'/scheduledCampaignList',
        success: async function(data) {
            if(data.statusCode===200) {
                $('#scheduledCampaignList').html("")
                var html;
                for(let obj of data.data) {
                    var newDateFormat = moment(obj.scheduledAt).format('DD-MM-YYYYTHH:mm');
                    html =`
                   <div class="list_scheduled">
                   <div>
                     <div style="display:flex;">
                        <p> <b>Campaign</b> : ${obj.templateName} </p>  
                        <p class="to_texts"> <b>To</b> : ${obj.groups} </p> 
                    </div>
                    <div style="display:flex;">
                        <p> <b>Scheduled on</b>: ${newDateFormat} </p> 
                        <p class="status_texts"> <b>Status</b>: ${obj.status} </p>
                        <p class="status_texts"> <b>Frequency</b>: ${obj.frequency} </p>
                    </div>
                   </div>
                    
                   <div class="get_scheduled">
                      <button id="${obj._id}" onclick="getScheduledCampaign(id)" class=" edit-schdle" data-target="#update-scheduled-campaign" 
                        data-toggle="modal" value="${obj._id}" ><img src="./dist/img/edit@2x.png" width="25" height="25"></button>
                      <span class="span-lines">|</span>  
                      <button id="${obj._id}" onclick="campaignDelete(id)" class="delete-schdle" value="${obj._id}" ><img src="./dist/img/delete@2x.png" width="25" height="25"></button>
                    </div>
                   
                
                  </div>
                  <hr class="schedule-hr">`
                  $('#scheduledCampaignList').append(html)
                  
                }
                
            }
        },
        error: function(jqXHR, textStatus, err) {
            console.log('errr', err);
        alert("Please refresh !");
        },
    })
}

$("#scheduledCampaignId").click(function(){
    scheduledCampaignList()
})

async function getScheduledCampaign(data){
    const id = data;

    $.ajax({
        method:'POST',
        url:'/getScheduledCampaign',
        data:{"id":id},
        success: async function(data) {

            if(data.statusCode==200) {
                var dataObj = data.data[0]
               // var testDateUtc = moment.utc(dataObj.scheduledAt);
                // var aaa = moment(testDateUtc._d).format('YYYY-MM-DDTHH:mm'); .local()
               // var newDateFormat = moment(dataObj.scheduledAt).format('YYYY-MM-DDTHH:mm');
                var newDateFormat = moment(dataObj.scheduledAt).local().format('YYYY-MM-DDTHH:mm');

                 $('#objectId').val(dataObj._id),
                 $('#templateName').val(dataObj.templateName),
                 $('#groups').val(dataObj.groups),
                 $('#buttonURL').val(dataObj.buttonUrlParam),
                 $('#headerParam').val(dataObj.headerParam),
                 $('#mediaURL').val(dataObj.media_url),
                 $('#parameters').val(dataObj.parameters),
                 $('#frmSaveOffice_startdt2').val(newDateFormat),
                 $("#frequencyOnModal").val(dataObj.frequency)

             }
        },
        error: function(jqXHR, textStatus, err) {
            console.log('errr', err);
        alert("Please refresh !");
        },
    })

}

async function updateScheduledCampaign(){
    const frequency = $("#frequencyOnModal option:selected").val()
    var groupsData = document.getElementsByName("groups-data")[0];
    var group = groupsData.value.replace(/\s/g, "");
    var newGroup = group.split(',')

    const scheduledCampaignObj = {
        id: $('#objectId').val(),
        templateName: $('#templateName').val(),
        groups: newGroup,
        buttonUrlParam: $('#buttonURL').val(),
        headerParam: $('#headerParam').val(),
        media_url: $('#mediaURL').val(),
        parameters: $('#parameters').val(),
        scheduledAt: new Date($('#frmSaveOffice_startdt2').val()),
        frequency:frequency
    }

    $.ajax({
        method:'PUT',
        url:'/updateScheduledCampaign',
        data: scheduledCampaignObj,
        success: async function(data) {

            if(data.statusCode==200) {
                $('#update-scheduled-campaign').modal('hide');
                swal({
                    text: "Campaign is update successfully",
                    icon: "success",
                    button: "Ok",
                    timer: 3500,
                });
                scheduledCampaignList()

            }
        },
        error: function(jqXHR, textStatus, err) {
            console.log('errr', err);
        alert("Please refresh !");
        },
    })

}

async function campaignDelete(data) {
    const id = data;

    swal({
        title: "Are you sure want to delete ?",
        text: "Once deleted, you will not be able to recover this record!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                method:'DELETE',
                url:'/deleteScheduledCampaign',
                data:{"id":id},
                success: async function(data) {
                    if(data.statusCode===200) {
                        swal({
                            text: "Campaign is delete successfully",
                            icon: "success",
                            button: "Ok",
                            timer: 3500,
                        });
        
                        scheduledCampaignList()
                    }
                },
                error: function(jqXHR, textStatus, err) {
                    console.log('errr', err);
                alert("Please refresh !");
                },
            })

        } else {
          swal("Your scheduled campaign is safe!");
        }
    });

}

$('#scheduledCampaignCkbx').click(function(){
    if ($(this).prop("checked")) {
       $("#scheduledCampaignDiv").removeClass("hide");
    } else {
       $("#scheduledCampaignDiv").addClass("hide");
    }
});

$(document).ready(function() {
    $('#datepick').datetimepicker({
        sideBySide: true,
        minDate:new Date(),
        format :"YYYY-MM-DD HH:mm",
        stepping: 5
    });
});

$(document).ready(function() {
    $('#datepickUpdate').datetimepicker({
    sideBySide: true,
    minDate:new Date(),
    format :"YYYY-MM-DD HH:mm",
    stepping: 5
    });
});

// assign agent to template ajax calls here
function assignTemplateTouser(){ 
        var data = {
            clientId: clientId,
            tempId: tempId,
            agent_Id: selectedAgentIdArray
        };
        $.ajax({
            data: data,
            async: false,
            cache: false,
            method: "POST",
            url: "/assignAgentToTemplate",
            success: function(data) {
            
            if (data.statusCode == 200) {
            // document.getElementById("closeModalManually2").setAttribute("data-dismiss", "modal");
            swal({
            title: data.message,
            icon: "success",
            timer: 3000,
            });
            document.getElementById("closeModalManually1").setAttribute("data-dismiss", "modal");
            // $('#edit_approve').modal('hide')
            getWhatsappTempList()
               location.reload();
            } else {
            swal({
                title: data.message,
                icon: "error",
                timer: 3000,
            });
            }
            }, error: function (jqXHR, textStatus, err) {  
                if(jqXHR.responseJSON.statusCode == 403) {
                window.location.replace("https://admin.helloyubo.com/login");
                } else {
                alert("Something went wrong, please try again")
                }
            }
            }); 
    };