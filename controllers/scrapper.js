const puppeteer = require('puppeteer');

const redis = require("redis");

let redisClient;

(async () => {
  redisClient = redis.createClient({"password":process.env.NODE_REDIS_KEY, expire: 1});

  redisClient.on("error", (error) => console.error(`Error : ${error}`));

  await redisClient.connect();
})();

module.exports = {
    extractStory:extractStory
};
 
async function extractStory(clientId, id, url, set) {
    try{
        if(set==true || set=="true"){
            console.log("######setting story true#######")
            await redisClient.set("curl_"+id,url)
            await redisClient.expire("curl_"+id,7200)
            let value=await redisClient.get("cstory_"+url)
            if(value){
                console.log("cstory already set");
                return true;
            }
            console.log("setting cstory");
            const browser = await puppeteer.launch({
              executablePath: '/usr/bin/chromium-browser',
              args: ['--no-sandbox'],
              headless: "new"
            })
            const page = await browser.newPage()
            await page.goto(url)
            
            await page.waitForSelector('.entry-content')
            let element = await page.$('.entry-content')
            value = await page.evaluate(el => el.textContent, element)
            value=cleanText(value)
            console.log("__------------------___PARSED VALUE______------------------________",value)
            await redisClient.set("cstory_"+url,value)
            await redisClient.expire("cstory_"+url,86400)
            browser.close()
            return true;
        } else {
            console.log("######setting story false#######")
            await redisClient.del("curl_"+id)
            await redisClient.del("cstory_"+url)
            return false;
        }
    } catch(err){
        console.log("extractStory error ",err);
        return false;
    }
};


function cleanText(txt){
  txt = txt.replace(/(\r\n|\n|\r)/gm, " ");
  txt = removeTags(txt)
  txt = txt.substring(txt.indexOf("minutes") + 7);
  txt = txt.split("Share on FacebookShare")[0];
  txt = txt.split("FacebookTwitter")[0];
  txt = txt.trim();
  return txt;
}

function removeTags(str) {
    if ((str===null) || (str===''))
        return false;
    else
        str = str.toString();
          
    return str.replace( /(<([^>]+)>)/ig, '');
}
