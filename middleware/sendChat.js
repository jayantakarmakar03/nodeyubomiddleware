const nodemailer = require('nodemailer');

const email = process.env.YUBO_EMAIL_ADDRESS
const password = process.env.YUBO_EMAIL_PASSWORD
 //sending mail using nodemailer. 
module.exports=function(mailId, html, subject,attachments='',user=email,pwd=password,from="Yubo Chatbot <contact@helloyubo.com>"){
  let transporter = nodemailer.createTransport({
  host:'smtp.gmail.com',
  port:465,
  auth: {
    user:user,
    pass:pwd
   }
  });
 

  let mailOptions = {
    from:"Yugasa Bot <contact@helloyubo.com>",
    to:mailId,
    subject:subject,
    html:html,
    attachments
  }
  transporter.sendMail(mailOptions, async function(error, info){
    if(error){
      console.log("error",error);
    }
  });
}
