const { Configuration, OpenAIApi } = require("openai");
const db = require("../models/all-models");
const redis = require("redis");

let redisClient;

(async () => {
  redisClient = redis.createClient({"password":process.env.NODE_REDIS_KEY});

  redisClient.on("error", (error) => console.error(`Error : ${error}`));

  await redisClient.connect();
})();

let clients_openai=[];

module.exports = {
    detectLang: detectLang,
    convertResponse: convertResponse,
    makeGenAiConfig: makeGenAiConfig
};

async function get_completion(prompt,clientId) {

    const chatCompletion = await clients_openai[clientId].createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [{role: "user", content: prompt}],
      temperature: 0
    });
    
    return chatCompletion.data.choices[0].message.content;
}

////////////////////////////////////////////////////////////
// Code to detect language of user input
////////////////////////////////////////////////////////////


async function detectLang(user_input,clientId){

  let isHinglish=false;

  let new_usr_ip = user_input

  if(clients_openai[clientId]==undefined){
    return {"isHinglish":isHinglish,"text":new_usr_ip}
  }

  let prompt_in = "Detect the language of the text that is delimited by triple backticks and output its English language name. The language of this text can be any language including Hinglish. Hinglish is a hybrid language including English words in Hindi sentences written using English letters or Latin Script. If you detect it to be Hinglish, please return the language of the text as Hinglish. text: ```"+user_input+"``` "

  console.log("~~~~prompt:~~~~" ,prompt_in)
  console.log("~~~~clientId:~~~~",clientId)
  console.log("~~~~user_input:~~~~",user_input)

  try{

    let style = await get_completion(prompt_in,clientId)
    let style_tokens = style.split(" ")
    let language = style_tokens[style_tokens.length -1]
    console.log("~~~~detected language:~~~~", language)
    
    if (language.toLowerCase().includes("hinglish")) {
      isHinglish=true;
      let prompt_hien_to_en = "Translate the text that is delimited by triple backticks into English. Strictly give only the translation as output. Do not give any explanation for the translation please. text: ```"+user_input+"``` "
      new_usr_ip = await get_completion(prompt_hien_to_en,clientId)
      console.log("~~~~translated to english:~~~~",new_usr_ip)
    }
  } catch(err){
    console.log("detectLang error: ",err);
  }

  console.log("isHinglish from genAi",isHinglish,clientId)

  return {"isHinglish":isHinglish,"text":new_usr_ip};
  
}


////////////////////////////////////////////////////////////
// Code to respond in the same language
////////////////////////////////////////////////////////////

async function convertResponse(bot_response,clientId){

  if(clients_openai[clientId]==undefined){
    return bot_response;
  }

  let prompt_resp = "Hinglish, a portmanteau of Hindi and English, is the macaronic hybrid use of English and languages of the Indian subcontinent. Translate the text that is delimited by triple backticks into a language that is Hinglish. If a direct precise translation to Hinglish is not available or possible, please keep the text in its original English only. text: ```"+bot_response+"``` "

  console.log(prompt_resp)

  let response = bot_response
  try{
      response = await get_completion(prompt_resp,clientId)
      response=response.replace(/```/g,"")
      console.log("final answer in Hinglish : ", response) //Final response to be sent to the user
  } catch(err){
    console.log("convertResponse error: ",err);
  }
  return response;
}


//*********Get Gen AI Data of client ******************************/

async function getGenAiConfig(clientId){
  try{
      let genai_key=await redisClient.get("genai_key_"+clientId)
      if(genai_key){
        return genai_key;
      }
      let result= await db.Client.findOne({clientId : clientId},{ genAiConfig:1, clientId:1, isGenAiEnabled: 1})
      if(!result){
          return false;
      }else{
          console.log("genAiConfig data", result)
          if(result.isGenAiEnabled!==undefined && result.isGenAiEnabled!==false && result.genAiConfig!==undefined){
            if(result.genAiConfig['genai_key']==undefined || result.genAiConfig['genai_key']==""){
              return false;
            } else {
              await redisClient.set("genai_key_"+clientId,result.genAiConfig['genai_key'])
              return result.genAiConfig['genai_key'];
            }
          } else{
            return false;
          }
      }
  }catch(err){
      console.log("getGenAiConfig error ",err)
      return false;
  }
}

//*********Make Gen AI Object for the client ******************************/

async function makeGenAiConfig(clientId,update=false){
  if(clients_openai[clientId]!=undefined && update==false){
    return true;
  }
  try{
      let genai_key=await getGenAiConfig(clientId)
      if(genai_key){
        let configuration = new Configuration({
          apiKey: genai_key,
        });
        clients_openai[clientId] = new OpenAIApi(configuration);
        return true;
      } else {
        clients_openai[clientId] = undefined;
        return false;
      }
  }catch(err){
      console.log("getGenAiConfig error ",err)
      clients_openai[clientId] = undefined;
      return false;
  }
}