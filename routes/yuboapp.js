var express = require('express');
var yuboApp=require('../controllers/yuboApp.js');
const db = require('../models/all-models');
const auth = require('../middleware/auth');
var router = express.Router();


router.post('/login', yuboApp.login);
router.post('/forgot/password', yuboApp.forgotPassword);
router.post('/reset/password',  yuboApp.userResetPassword);
router.post('/chat/list', auth,  yuboApp.userChatList);
router.post('/lead/generate', auth,  yuboApp.userLeadGenerate);
router.post('/lead/conversion', auth,  yuboApp.leadConversionGraph);
router.post('/chat/control', auth,  yuboApp.appChatControl);
router.post('/send/Message', /*auth,*/  yuboApp.appSendMessage);
router.get('/profile', auth, yuboApp.getProfile);
router.post('/get/message', yuboApp.getMessage);
router.post('/get/chat', auth, yuboApp.getChat);

module.exports = router;
