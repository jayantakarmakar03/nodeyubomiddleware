  
// for input field
var source1 = document.getElementById('source1');
var result1 = document.getElementById('result1');
// const source2 = document.getElementById('source2');
// const result2 = document.getElementById('result2');

var inputHandler1 = function (e) {
  result1.innerHTML = e.target.value;
}
// const inputHandler2 = function (e) {
//   result2.innerHTML = e.target.value;
// }

source1.addEventListener('input', inputHandler1);
source1.addEventListener('propertychange', inputHandler1);
// source2.addEventListener('input', inputHandler2);
// source2.addEventListener('propertychange', inputHandler2);


//for chatbot head
var headBgColor;
var defaultColorhead = "#0000ff";

window.addEventListener("load", head_fnctn, false);
function head_fnctn() {
  headBgColor = document.getElementById("headBgColor");
  headBgColor.value = defaultColorhead;
  headBgColor.addEventListener("input", head_bg_fnctn, false);
}

function head_bg_fnctn(event) {
  var chatboad_head = document.querySelectorAll(".chatboad_head");
  // var enterBtn_bg = document.querySelector(".enterBtn_bg");

    if (chatboad_head) {
      var b;
      for (b = 0; b < chatboad_head.length; b++) {
        chatboad_head[b].style.background = event.target.value;
      }
    }
    // enterBtn_bg.style.background = event.target.value;
  }


//for chatbot head
var iconBgColor;
var defaultColorhead = "#0000ff";

window.addEventListener("load", icon_fnctn, false);
function icon_fnctn() {
  console.log('changing')
  iconBgColor = document.getElementById("iconBg_color");
  iconBgColor.value = defaultColorhead;
  iconBgColor.addEventListener("input", icon_bg_fnctn, false);
}

function icon_bg_fnctn(event) {
  var chatboad_icon = document.querySelectorAll(".icon_head");
  console.log(chatboad_icon);
  // var enterBtn_bg = document.querySelector(".enterBtn_bg");

    if (chatboad_icon) {
      var b;
      for (b = 0; b < chatboad_icon.length; b++) {
        chatboad_icon[b].style.background = event.target.value;
      }
    }
    // enterBtn_bg.style.background = event.target.value;
  }

//for cross color
var cross_head_color;
var defaultColorhead = "#0000ff";

window.addEventListener("load", cross_fnctn, false);
function cross_fnctn() {
  cross_head_color = document.getElementById("cross_head_color");
  cross_head_color.value = defaultColorhead;
  cross_head_color.addEventListener("input", cross_bg_fnctn, false);
}

function cross_bg_fnctn(event) {
  var cross_head = document.getElementById("cross_head");

  if (cross_head) {
    cross_head.style.color = event.target.value;
  }
}

//for bot widget Background Color
var botwidgetbgcolor;
var defaultColorbotwidgetbg = "#0000ff";

window.addEventListener("load", wg_bg_fnctn, false);
function wg_bg_fnctn() {
  botwidgetbgcolor = document.getElementById("botwidgetbgcolor");
  botwidgetbgcolor.value = defaultColorbotwidgetbg;
  botwidgetbgcolor.addEventListener("input", wg_bg_color_fnctn, false);
}

function wg_bg_color_fnctn(event) {
  var botwidget_background = document.getElementById("botwidget_background");

  if (botwidget_background) {
    botwidget_background.style.background = event.target.value;
  }
}

//for bot Widget Text-Color
var bottext_head;
var defaultColorhead1 = "#0000ff";

window.addEventListener("load", cross_fnctn11, false);
function cross_fnctn11() {
  bottext_head = document.getElementById("bottext_head");
  bottext_head.value = defaultColorhead1;
  bottext_head.addEventListener("input", cross_bg_fnctn22, false);
}

function cross_bg_fnctn22(event) {
  var bottext_bg = document.getElementById("bottext_bg");

  if (bottext_bg) {
    bottext_bg.style.color = event.target.value;
  }
}

//for User Widget Text-Color
var usertext_head;
var defaultColorhead1 = "#0000ff";

window.addEventListener("load", usertext_fnctn, false);
function usertext_fnctn() {
  usertext_head = document.getElementById("usertext_head");
  usertext_head.value = defaultColorhead1;
  usertext_head.addEventListener("input", usertext_color_fnctn, false);
}

function usertext_color_fnctn(event) {
  var usertext_bg = document.getElementById("usertext_bg");

  if (usertext_bg) {
    usertext_bg.style.color = event.target.value;
  }
}

//for User Widget Background-Color
var userBg_head;
var defaultColorhead1 = "#0000ff";

window.addEventListener("load", userBg_fnctn, false);
function userBg_fnctn() {
  userBg_head = document.getElementById("userBg_head");
  userBg_head.value = defaultColorhead1;
  userBg_head.addEventListener("input", userBg_color_fnctn, false);
}

function userBg_color_fnctn(event) {
  var userBg_bg = document.getElementById("userBg_bg");

  if (userBg_bg) {
    userBg_bg.style.background = event.target.value;
  }
}

//for cross color
var userBg_head;
var defaultColorhead1 = "#0000ff";

window.addEventListener("load", userBg_fnctn, false);
function userBg_fnctn() {
  userBg_head = document.getElementById("userBg_head");
  userBg_head.value = defaultColorhead1;
  userBg_head.addEventListener("input", userBg_color_fnctn, false);
}

function userBg_color_fnctn(event) {
  var userBg_bg = document.getElementById("userBg_bg");

  if (userBg_bg) {
    userBg_bg.style.background = event.target.value;
  }
}
//for chat Screen
var chatScreen_color;
var defaultColor_chatScreen = "#ffffff";

window.addEventListener("load", chatScreen_fnctn, false);
function chatScreen_fnctn() {
  chatScreen_color = document.getElementById("chatScreen_color");
  chatScreen_color.value = defaultColor_chatScreen;
  chatScreen_color.addEventListener("input", chatScreen_color_fnctn, false);
}

function chatScreen_color_fnctn(event) {
  var chatScreen_bg = document.querySelectorAll(".chatScreen_bg");
  if (chatScreen_bg) {
    var b;
    for (b = 0; b < chatScreen_bg.length; b++) {
      chatScreen_bg[b].style.background = event.target.value;
    }
  }
}
//for Bot Border color
var botBorder_color;
var defaultColor_botBorder = "#0000ff";

window.addEventListener("load", botBorder_fnctn, false);
function botBorder_fnctn() {
  botBorder_color = document.getElementById("botBorder_color");
  botBorder_color.value = defaultColor_botBorder;
  botBorder_color.addEventListener("input", botBorder_color_fnctn, false);
}
function botBorder_color_fnctn(event) {
  var botBorder = document.querySelectorAll(".botBorder");
  if (botBorder) {
    var a;
    for (a = 0; a < botBorder.length; a++) {
      botBorder[a].style.borderColor = event.target.value;
    }
  }
}

//for head text
var headText_color;
var defaultColor_headText = "#0000ff";

window.addEventListener("load", headText_fnctn, false);
function headText_fnctn() {
  headText_color = document.getElementById("headText_color");
  headText_color.value = defaultColor_headText;
  headText_color.addEventListener("input", headText_color_fnctn, false);
}

function headText_color_fnctn(event) {
  var headText = document.querySelectorAll(".headText");
  if (headText) {
    var i;
    for (i = 0; i < headText.length; i++) {
      headText[i].style.color = event.target.value;;
    }
  }
}
$(function () {
  var $select = $('.js-select'),
    $setting = $('.js-setting');

  $select.on('change', function () {
    var value = '.' + $(this).val();
    $setting.show().not(value).hide();
  });
});

//for Bot Border color
var botBorder_color;
var defaultColor_botBorder = "#0000ff";

window.addEventListener("load", botBorder_fnctn, false);
function botBorder_fnctn() {
  botBorder_color = document.getElementById("botBorder_color");
  botBorder_color.value = defaultColor_botBorder;
  botBorder_color.addEventListener("input", botBorder_color_fnctn, false);
}
function botBorder_color_fnctn(event) {
  var botBorder = document.querySelectorAll(".botBorder");
  if (botBorder) {
    var a;
    for (a = 0; a < botBorder.length; a++) {
      botBorder[a].style.borderColor = event.target.value;
    }
  }
}

//to validate the size of image 
Filevalidation = (inpt) => {
  const fi = document.querySelector('.file_logo');
  console.log(fi.files)
  // Check if any file is selected. 
  if(fi.files[0].type!='image/png'){
    alert("Please upload a valid PNG image only.");
    inpt.value=""
    return false;
  }
  if (fi.files.length > 0) {
      const file_logo = Math.round((fi.files[0].size / 1024));
      // The size of the file. 
      if (file_logo <= 50) {
        document.querySelector('.size_logo').innerHTML = '<b>'
          + file_logo + '</b> KB';
        readURL(inpt);
        return true;
      } else {
        alert("Image size must be less than 50 KB.");
        inpt.value=""
        return false;
      }
  }
}


//to read the url of the uploaded image

function readURL(input) {
  // console.log("input", input)
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#logo_svg').hide();
      $('#logo_img').show();
      // $('.botLogo_dlt').show()
      $('#logo_img')
        .attr('src', e.target.result);
    };
    // $(this).removeClass('open').addClass('open');
    reader.readAsDataURL(input.files[0]);
    if(input.files[0]?.name){
      $('#upload_filename').html(input.files[0].name)
      $('.deleteicon').removeClass('hide')
    }
  }
}

// for pro section contact popup
$('.contact-popUp-open').click(function (e) { //get open by clicking on button
  $('.contact-popUp-body').fadeIn();
  e.stopPropagation();
});
$('.contact-popUp-close').click(function () {
  $('.contact-popUp-body').fadeOut(); // get close by clicking on button

});
$('.contact-popUp-body-Inner').click(function (e) { // will not close by clicking on body of code
  e.stopPropagation();
});
$(document).click(function () { // will close by clicking on body of code
  $('.contact-popUp-body').fadeOut();
});


// for enterprise section contact popup
$('.contact1-popUp-open').click(function (e) { //get open by clicking on button
  $('.contact1-popUp-body').fadeIn();
  e.stopPropagation();
});
$('.contact1-popUp-close').click(function () {
  $('.contact1-popUp-body').fadeOut(); // get close by clicking on button

});
$('.contact1-popUp-body-Inner').click(function (e) { // will not close by clicking on body of code
  e.stopPropagation();
});
$(document).click(function () { // will close by clicking on body of code
  $('.contact1-popUp-body').fadeOut();
});

//for chatbot logo
var botLogo_color;
var defaultColorbotLogo = "#0000ff";

window.addEventListener("load", botLogo_fnctn, false);
function botLogo_fnctn() {
  botLogo_color = document.getElementById("botLogo_color");
  botLogo_color.value = defaultColorbotLogo;
  botLogo_color.addEventListener("input", botLogo_bg_fnctn, false);
}

function botLogo_bg_fnctn(event) {
  var botLogo_bg = document.querySelector(".botLogo_bg");

  if (botLogo_bg) {
    botLogo_bg.style.color = event.target.value;
  }
}

//to delete uploaded chatbot logo 
$("#botoLogo_dlt").click(function(){
  $("botLogo").remove();
});

var numData=500;
function addCategory() {
     ++numData;

    $("#categoryId").append(`
    <div style="margin:10px 0px 10px 0px;" id="${numData}" class="agentTemplateList">
      <div style="display:flex;">
        <p style="color:white">Category</p>
        <input class="category_input1" type="text" id="category" value="" />
        <button class="category_delete01" onclick="onDelete(this)" value="${numData}">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16" >
            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
          </svg>
        </button>
      </div>

      <div id="inputBoxList_${numData}">
          <input type="text" class="form-control nextTab inputBoxList_${numData}" data-key="inputBoxList_${numData}" />
          <button class="append_div1" onclick="getAppentDivId(this)" append-id="inputBoxList_${numData}"  >
             <svg class="svg_delete2 " xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16" >
             <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
           </svg>
          </button>
      </div>
  
    </div>
    
  `)
}


function onDelete(data) {
  
   var dataId = $(data).val()
   $(`#${dataId}`).empty()

}

var appendedId;
var singleFieldList=1000;
function getAppentDivId(data) {
   ++singleFieldList;
  appendedId = $(data).attr('append-id') 

  $(`#${appendedId}`).append(`
    <div id="deleteSingleField_${singleFieldList}" class="agentTemplateArray">
      <input class="form-control nextTab ${appendedId} " />
      <button onclick="deleteSingleField(this)" delete-id="deleteSingleField_${singleFieldList}" style="border:none;background-color:transparent;color:#f6f2f2;padding-left: 8px;">
      <svg class="delete2_svg " xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16" >
          <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
          <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
      </svg> </button>

    </div>`)
  
}

function deleteSingleField(data) {
   var dataId = $(data).attr('delete-id')
   $(`#${dataId}`).empty()
}

async function agentTempData() {
  let obj={};
  var duplicateKey = []
  const blankKey = []
  const blankValue = []

  $('.agentTemplateList').each(function(index){
     var innerId = $(this).find('.nextTab').attr('data-key')
     const key = $(this).find('input#category').val()
     const valueArr = []
     $(`.${innerId}`).each(function(index){
       if($(this).val()) {
        valueArr.push($(this).val())
       } else {
         blankValue.push($(this).val())
       }

     })
     
     if(key == '' || key =='undefined') {
       blankKey.push(key)
     } else if(valueArr.length<0) {
       blankValue.push(valueArr)
     }

     if(key) { 
      for(let objKey in obj) {
        if(key.toLowerCase()==objKey.toLowerCase()) {
          duplicateKey.push(key)
        } else {
          // console.log('10',key,valueArr);
        }
      }
    Object.assign(obj, {[key]: valueArr});
     }
  })

  if(blankKey.length>0) {
    swal({
      title: 'Category name can not be empty',
      icon: "error",
    }) 
  } else if(blankValue.length>0) {
    swal({
      title: "Template message can not be empty",
      icon: "error",
    })  
  } else if(duplicateKey.length>0) {
    swal({
      title: `Category name: "${duplicateKey}" already exist`,
      icon: "error",
    })
  } else if(Object.keys(obj).length>=1 && duplicateKey.length == 0) {
          $.ajax({
            method:'POST',
            url:'/agentTemplate',
            data:{"template":{obj}},
            success: async function(data){
              if(data.statusCode){
                swal({
                  title: "Agent template save successfully",
                  icon: "success",
                }) 
                $('#addTemplate').modal('hide');
              }
              location.reload();
            },
            error: function (jqXHR, textStatus, err) {
              alert('Please refresh !');
            }
      })
  }

}

function addTemplateData()  {
  $("#appendTopId").empty()
    $.ajax({
        method:'POST',
        url:'/getAgentTempList',
        success: function(data){
          if(data.statusCode){
            var newArr = data.data[0].template.obj;
            let html = '';
            let idData = 0;
            let innerCounter = 0;
            for(const obj in newArr) {
              var key = obj
              var valueArr = newArr[obj]
              ++idData;
              html =`<div style="margin:10px 0px 10px 0px;" id="${idData}" class="agentTemplateList">
                        <div style="display:flex;">
                          <p class="category_text1">Category</p>
                          <input class="category_input1" type="text" id="category" value="${key}" style="" />
                          <button class="category_delete01" onclick="onDelete(this)" value="${idData}" 
                          style="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                              <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                              <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                            </svg>
                          </button>
                        </div>`  
                
                for(const i in valueArr) {
                  ++innerCounter;
                 
                   if(i==0) {
                      html += `<div id="inputBoxList_${idData}">
                      <input type="text" class="form-control nextTab inputBoxList_${idData}" data-key="inputBoxList_${idData}" value="${valueArr[i]}" />
                      <button class="append_div1" onclick="getAppentDivId(this)" append-id="inputBoxList_${idData}">
                        <svg class="svg_delete2" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                          <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                        </svg>
                        </button> 
                      
                      
                    </div>`
                   } else {
                    html += `<div id="deleteSingleField_${innerCounter}" class="agentTemplateArray">
                    <input class="form-control nextTab inputBoxList_${idData}" value="${valueArr[i]}" />
                    <button class="category_delete02" onclick="deleteSingleField(this)" delete-id="deleteSingleField_${innerCounter}">
                    <svg class="delete2_svg" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                    </svg> </button>
              
                  </div>`
                   }
                } 

              `</div>`
              $("#appendTopId").append(html);
               
              }
              
          }   
          
        },
        error: function (jqXHR, textStatus, err) {
          alert('Please refresh !');
        }
    })

}

function updateClientConfigData(){
    let nodeName = $("#nodesList").val();
    
    let minutes   = $('#minutes').val();
    let hours     = $('#hours').val();
    let seconds   = $('#seconds').val();
    let timerDetails ={
      hours     : hours,
      seconds   : seconds,
      minutes   : minutes
    }
    let InactivityTime  = 60000;
    let cronStatus = false;
    if(resetContextChecked == "true" || triggerNodeChecked == "true"){
      cronStatus = true;
    }else{
      cronStatus = false;
    }
    if(triggerNodeChecked == 'false'){
      nodeName ='false';
    }
  
      $.ajax({
        type: "POST",
        url: "/updateTimerConfig",
        data: {
          resetContext      : resetContextChecked,
          triggerNode       : triggerNodeChecked,
          nodeName          : nodeName,
          InactivityTime    : InactivityTime,
          timerDetails      : timerDetails,
          cronStatus        : cronStatus
        },
        timeout: 10000,
        success: function (resp) {
          if (resp) {
            if (resp.statusCode === 200) {
              swal({
                title: "Configuration updated successfully",
                icon: "success",
                button: "OK",
              })
            } else {
              alert(resp.message);
            }
          } else {
            console.log("error occures");
          }
        },
        error: function (jqXHR, textStatus, err) {
          alert("Something went wrong, please refresh the page", err);
        },
      });
}
let resetContextChecked ='false';
let triggerNodeChecked ='false';
$('#resetContext').click(function() {

  if(this.checked) {
    resetContextChecked ='true';
  }else{
    resetContextChecked ='false';
  }
});

$('#triggerNode').click(function() {

  if(this.checked) {
    triggerNodeChecked ='true';
  }else{
    triggerNodeChecked ='false';
  }
});

function updateApiSecredFun(event){
    let edit_api_secret = $('#api_secret_input_id').val();
    let data = {
      api_secret: edit_api_secret
    }
    swal({
      title: "Are you sure want to update api secret?",
      text: "",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        $.ajax({
          data: data,
          async: false,
          cache: false,
          method: 'POST',
          url: '/updateClientApiSecret',
          success: async function (data) {
      
            if (data.statusCode == 200) {
              $('#editButttonId').show();
              $('#api_secret_input_id').attr('readonly', true);
              document.getElementById('btnHolder').innerHTML = '';
      
              await swal({
                title: "Api secret updated successfully",
                icon: "success",
                timer: 1500
              });
              // getHumanAgentList();
            } else if(data.statusCode == 201) {
              alert(data.message)
            }
          }
        });


      } else {
        swal("Your data is safe now!");
      }
    });
}

function getApiSecret(){
  let data = {};
  $.ajax({
    data: data,
    async: false,
    cache: false,
    method: 'POST',
    url: '/getTokenList',
    success: async function (data) {
      
      let tokenList = data.res.api_keys;
      if (data.statusCode == 200) {
          $("#api_secret_input_id").val(data.res.api_secret);

          $(".apiTokenListDiv").html("");
          let index =100 ;
          for (const iterator of tokenList) {
            ind =  'ID'+index++;
            html = 
              `<div class="web_div">
                <div class="row inside_web">
                  <p class="heading_text">${iterator.channel} </p>
                  <span class="token_icons">
                    <button class="icon_button" 
                            type="button"
                            onclick="handleCopyTextFromArea(${ind})">
                           <i class="fa fa-clone icon_templates" aria-hidden="true"></i>
                    </button>
                      <button  class="icon_button" 
                               type="button"
                               id=${iterator._id}
                               onclick="deleteToken(id)"
                               >
                        <svg xmlns="http://www.w3.org/2000/svg" 
                             width="16" 
                             height="16" 
                             fill="currentColor"
                             class="bi bi-trash icon_t_delete" 
                             viewBox="0 0 16 16">
                          <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                          <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                        </svg>
                      </button>
                  </span>
                </div>
                <div class="row textarea_tokens" >
                    <textarea class="token-text-area"
                              id=${ind} >${iterator.token}</textarea>
                </div>
              </div>`;
                $(".apiTokenListDiv").append(html);

          }

      } else if(data.statusCode == 203) {
        alert(data.message)
      }
    }
  });
}

function generateToken(event){
  let channel = $('#channel_input').val();
  let data = {
    channel: channel.trim()
  }
  if(channel.trim()=='' ||channel.trim() == 'undefined' ){
    swal({
      title: "Channel name can not be empty, Please provide channel name.",
      icon: "warning",
      timer: 2500
    });
  }else{
    $.ajax({
      data: data,
      async: false,
      cache: false,
      method: 'POST',
      url: '/generateToken',
      success: async function (data) {
        if (data.statusCode == 200) {
          $('#channel_input').val('');
          await swal({
            title: "Api secret generated successfully",
            icon: "success",
            timer: 1500
          });
          getApiSecret();
        } else if(data.statusCode == 201) {
          alert(data.message)
        }
      }
    });
  }

}

function deleteToken(tokenId){
  
  let data = {
    tokenId: tokenId
  }
  swal({
    title: "Are you sure want to delete ?",
    text: "Once deleted, you will not be able to recover this record!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  }).then((willDelete) => {
    if (willDelete) {
        $.ajax({
          data: data,
          async: false,
          cache: false,
          method: 'POST',
          url: '/deleteTokenById',
          success: async function (data) {
            if (data.statusCode == 200) {
              await swal({
                title: "Api Token deleted successfully",
                icon: "success",
                timer: 1500
              });
              getApiSecret();
            } else if(data.statusCode == 201) {
              alert(data.message)
            }
          }
        });    
    } else {
      swal("Your data is safe now!");
    }
  });
}

function onEditButtonClick(event){
   $('#editButttonId').hide();
   document.getElementById("api_secret_input_id").readOnly = false;
   document.getElementById('btnHolder').innerHTML = '<input type="button" id="updateBtnId" class="update_buttons" onClick="updateApiSecredFun();" value="Update" />';
}

function handleCopyTextFromArea(_this) {
  let textareaId = `#${_this.id}`
  const area = document.querySelector(textareaId);
  
  area.select();
  document.execCommand('copy')
  alert("Copied to clipboard!");
}