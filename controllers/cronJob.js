const db = require("../models/all-models"),
  CronJob = require("cron").CronJob,
  os = require("os"),
  sendMail = require("../middleware/sendChat.js");
  const moment =  require("moment");



 

/**Mail Scheduler*/
/**##########################################################################*/
var job = new CronJob(
  "0 0 */2 * * *",
  async function () {
  try{
    let freeMemory = os.freemem() / 1024 / 1024;
    let html =
      '<div style="width:400px;border:thin solid #dadce0;border-radius:10px;padding:5px 0px" align="center" ><h2 style="color:blue">Hello</h2><h2 style="color:blue">Your 172.105.42.134 server memory is low:' +
      freeMemory +
      ". Please free some space</h2></div>";
    if (freeMemory < 1000) {
      sendMail(
        "hari.yugasa@gmail.com, shobhit@yugasasoftware.com, pratap.yugasa@gmail.com",
        html,
        "Alert Yugasa Bot Memory Space",
        ''
      );
    }
    let date = new Date();
    date.setHours(date.getHours() - 2);
    let client = await db.Client.findOne({ clientId: "yugasa_chatbot" });
    let lists = await db.Userchat.find({
      clientId: client._id,
      date: { $gt: new Date(date) },
    }).sort("-createdAt");
    for (list of lists) {
      let str = "";
      if (list.chats.length < 3) {
        continue;
      }
      
  let attachment = list.fileUploaded;
  let filearray =[];
if(attachment.length>0){
  for(attach of attachment){
    let filedata ={
      path :attach.path,
      filename :attach.filename
    }
    filearray.push(filedata)
  }
}
 
      for (chat of list.chats) {
        if (chat.messageType == "outgoing") {
          let str1 =
            '<li class="yubo" style="color:blue;font-size: 16px;font-weight:600;margin-bottom: 15px;"><span style="margin-right: 15px;text-decoration: underline;">Yugasa Bot:</span>' +
            chat.text +
            "</li>";
          str += str1;
        } else {
          let str2 =
            '<li class="user" style="color: green;font-size: 16px;font-weight: 600;margin-bottom: 15px;"><span style="margin-right: 15px;text-decoration: underline; ">User:</span>' +
            chat.text +
            "</li>";
          str += str2;
        }
      }
      let html =
        '<div style="width: 100%; border: thin solid #dadce0;text-align: left;"><ul style="list-style:none;">' +
        str +
        "</ul></div>";
      sendMail(
          "ashish@yugasa.com",
        html,
        "Yugasa Bot ("+getsrc(list.userId)+") Conversation Id:" + list.userId,
        filearray
      );
    }

    /*End for yugasa_chatbot*/

    let packageBabaclient = await db.Client.findOne({
      clientId: "packagingBaba_chatbot",
    });
    let plists = await db.Userchat.find({
      clientId: packageBabaclient._id,
      date: { $gt: new Date(date) },
    }).sort("-createdAt");

    for (plist of plists) {
      let str = "";
      if (plist.chats.length < 3) {
        continue;
      }
      for (chat of plist.chats) {
        if (chat.messageType == "outgoing") {
          let str1 =
            '<li class="yubo" style="color:blue;font-size: 16px;font-weight:600;margin-bottom: 15px;"><span style="margin-right: 15px;text-decoration: underline;">Yugasa Bot:</span>' +
            chat.text +
            "</li>";
          str += str1;
        } else {
          let str2 =
            '<li class="user" style="color: green;font-size: 16px;font-weight: 600;margin-bottom: 15px;"><span style="margin-right: 15px;text-decoration: underline; ">User:</span>' +
            chat.text +
            "</li>";
          str += str2;
        }
      }
      let html =
        '<div style="width: 100%; border: thin solid #dadce0;text-align: left;"><ul style="list-style:none;">' +
        str +
        "</ul></div>";
      sendMail(
        "p.mishra@yugasasoftware.com",
        html,
        "Yugasa Bot Packaging Baba Conversation Id:" + plist.userId
      );
    }
  }catch(error){
     console.log('An error occurs in the catch block:-', error)
  }
  },
  null,
  true,
  "Asia/Kolkata"
);
job.start();

var amitjob = new CronJob(
  "0 0 */12 * * *",
  async function () {
    //Amit crone job
    let date = new Date();
    date.setHours(date.getHours() - 12);
    let client = await db.Client.findOne({ clientId: "amit_chatbot" });
    let lists = await db.Userchat.find({
      clientId: client._id,
      date: { $gt: new Date(date) },
    }).sort("-createdAt");
    for (list of lists) {
      let str = "";
      if (list.chats.length < 3) {
        continue;
      }
      for (chat of list.chats) {
        if (chat.messageType == "outgoing") {
          let str1 =
            '<li class="yubo" style="color:blue;font-size: 16px;font-weight:600;margin-bottom: 15px;"><span style="margin-right: 15px;text-decoration: underline;">Yugasa Bot:</span>' +
            chat.text +
            "</li>";
          str += str1;
        } else {
          let str2 =
            '<li class="user" style="color: green;font-size: 16px;font-weight: 600;margin-bottom: 15px;"><span style="margin-right: 15px;text-decoration: underline; ">User:</span>' +
            chat.text +
            "</li>";
          str += str2;
        }
      }
      let html =
        '<div style="width: 100%; border: thin solid #dadce0;text-align: left;"><ul style="list-style:none;">' +
        str +
        "</ul></div>";
      sendMail(
        "connect@amitkukreja.com",
        html,
        "Yugasa Bot ("+getsrc(list.userId)+") Conversation Id:" + list.userId
      );
    }
  },
  null,
  true,
  "Asia/Kolkata"
);
amitjob.start();


/********  cronjob for basic clients   ***********/

//to start the job
 var chatJob = new CronJob('0 0 * * *', async function() {
       let clients = await db.Client.find({type:'basic'}).lean().exec();
       await jobSchedulerFunc(clients)
   }, null, true, 'Asia/Kolkata');
   chatJob.start();


 //function to send data via mail
async function jobSchedulerFunc(clients) {
try{
  let clientId;
  let emails;
  for(let clientData of clients){
    clientId = clientData.clientId
    emails = clientData.email
    let date = new Date();
    date.setHours(date.getHours() - 12);
    let client = await db.Client.findOne({ clientId: clientId }).lean().exec();
    let lists = await db.Userchat.find({
      clientId: client._id,
      date: { $gt: new Date(date) },
    }).sort("-createdAt").lean().exec();
    for (list of lists) {
    let str = "";
    if (list.chats.length < 3) {
      continue;
    }
    for (chat of list.chats) {
        if (chat.messageType == "outgoing") {
          let str1 =
          '<li class="yubo" style="color:blue;font-size: 16px;font-weight:600;margin-bottom: 15px;"><span style="margin-right: 15px;text-decoration: underline;">Yugasa Bot:</span>' +
            chat.text +
          "</li>";
          str += str1;
        }else{
        let str2 =
          '<li class="user" style="color: green;font-size: 16px;font-weight: 600;margin-bottom: 15px;"><span style="margin-right: 15px;text-decoration: underline; ">User:</span>' +
            chat.text +
          "</li>";
        str += str2;
      }
    }
    let html =
      '<div style="width: 100%; border: thin solid #dadce0;text-align: left;"><ul style="list-style:none;">' +
        str +
      "</ul></div>";
    sendMail(
      emails,
      html,
      "Yugasa Bot ("+getsrc(list.userId)+") Conversation Id:" + list.userId
    );
    }
  }
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

function getsrc(userid) {
    if (!userid) {
        return false;
    }
    let src = userid.split("_")[0].toLowerCase();
    switch (src) {
        case "wa":
            return "WhatsApp"
            break;
        case "fb":
            return "Facebook"
            break;
        case "gbm":
            return "Google"
            break;
        case "tg":
            return "Telegram"
            break;
        case "android":
            return "Android"
            break;
        case "ios":
            return "iOS"
            break;
        default:
            return "Website"
            break;
    }
}

/*
var lastMonthvisitor = new CronJob('* * * 1 * *', async () => {
  try {
    const startDate = moment().subtract(1, 'month').startOf('month').format('MMMM D, YYYY');
    const endDate = moment().subtract(1, 'month').endOf('month').format('MMMM D, YYYY');
    const dateRange = `${startDate} - ${endDate}`
    const response = await getLastMonthVisitors();
    sendMail("ashish@yugasa.com, vivek@yugasa.com, d.jaggi@helloyubo.com , p.mishra@yugasasoftware.com, shobhit@yugasasoftware.com", response, dateRange)
    } catch(error) {
    console.log('💣 Error occurred: ');
    console.log(error)
  }
});
lastMonthvisitor.start();

async function getLastMonthVisitors() {
  try {
    const clients = await db.Client.find({});
    const clientChatPromise = clients.map(async (client) => {
      const count = await getClientChatCount(client._id, client.clientId);
      return { username: client.username, count };
    });

    const tableRows = await Promise.all(clientChatPromise);
    tableRows.sort((a, b) => b.count.count - a.count.count);

    let tableHTML = `
      <h1> Last month number of visitors </h1>
      <table style="border-collapse: collapse; width: 100%;">
        <tr>
          <th style="border: 1px solid black; padding: 8px;">Username/Client</th>
          <th style="border: 1px solid black; padding: 8px;">Visitor Count</th>
        </tr>
    `;

    for (let i = 0; i < tableRows.length; i++) {
      const row = tableRows[i];
      // console.log("row", row)
      const rowColor = i % 2 === 0 ? '#ffffff' : '#f2f2f2';
      const username = row.username || ''; // Handle undefined username
      tableHTML += `
        <tr style="background-color: ${rowColor};">
          <td style="border: 1px solid black; padding: 8px;">${row.count.username}</td>
          <td style="border: 1px solid black; padding: 8px;">${row.count.count}</td>
        </tr>
      `;
    }

    tableHTML += '</table>';
    return tableHTML;
  } catch (error) {
    console.error(error);
  }
}


const getClientChatCount = async (clientId, username) => {
try{
  const startDate = moment().subtract(1, 'month').startOf('month');
  const endDate = moment().subtract(1, 'month').endOf('month');

  const visitorCount = await db.Userchat.find({
    $and: [
      {clientId},
      { date: { $gte: startDate, $lte: endDate } }
    ]
  }).count();
  const deletedChats = await db.UserDeletedChat.find({
    clientId : username,
    deleteChatDate: { $gte: startDate, $lte: endDate }
  }).count();
  return { username, count: parseInt(visitorCount + deletedChats) };
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}

};

*/
