/**for user creation */
$(".createUser").click(function() {
    var data = {
        name: $("#userName").val(),
        phone: $("#userPhone").val(),
        email: $("#userEmail").val(),
    };
    let phone = $("#userPhone").val();
    let email = $("#userEmail").val();
    let phoneValidation = /[91]{2}[7-9][0-9]{9}$/;
    // var phoneValidation = /^\+(?:[0-9] ?){6,14}[0-9]$/;
    const emailValidation =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(!$("#userName").val()) {
            swal({
            title: "Please enter user name",
            icon: "warning",
            timer: 3500,
        });
    } else if ($("#userPhone").val() < 0) {
        swal({
            title: "Phone number can not be negative",
            icon: "warning",
            timer: 3500,
        });
    } else if (phone && !phoneValidation.test(phone)) {
        swal({
            title: "Please select your country code and Enter valid mobile number!",
            icon: "warning",
            timer: 3500,
        });
    } else if (email && !emailValidation.test(String(email).toLowerCase())) {
        swal({
            title: "Please provide valid email id",
            icon: "warning",
            timer: 3500,
        });
    } else if (!phone && !email) {
        swal({
            title: "Phone or email is required",
            icon: "warning",
            timer: 3500,
        });
    } else {
        $.ajax({
            data: data,
            async: false,
            cache: false,
            method: "POST",
            url: "/add-user",
            success: function(data) {
                if (data.statusCode == 200) {
                    swal({
                        title: data.message,
                        icon: "success",
                        timer: 3500,
                    });
                    location.reload();
                } else if (data.statusCode == 201) {
                    swal({
                        title: data.message,
                        icon: "error",
                        timer: 3500,
                    });
                } else {
                    swal({
                        title: data.message,
                        icon: "error",
                        timer: 3500,
                    });
                }
            },error: function (jqXHR, textStatus, err) {  
                if(jqXHR.responseJSON.statusCode == 403) {
                window.location.replace("https://admin.helloyubo.com/login");
                } else {
                alert("Something went wrong, please try again")
                }
            }
        });
    }
});

var limit = 50;
var skip = 0;
var docCount = 0; 

function getUserList() {
    let searchData = $("#searchBoxId").val();
    showLoader();
    let clientId = document.getElementById("clientId").value;
    let data = {
        clientId : clientId,
        searchItem : searchData,
        skip     : skip,
        limit    : limit
    };
    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: "POST",
        url: "/getSubscriberList",
        beforeSend: function() {
            swal({
                title: "Loading...",
                button: false,
                closeOnClickOutside: true,
                closeOnEsc: false,
                timer: 2000,
            });
        },
        success: function(data) {
            // document.getElementById('docCount').innerHTML = `Total Subscribers : ${data.docCount}`
            $("#docCount").html(`Total Subscribers : ${data.docCount}`)
            hideLoader();
            if (data.statusCode == 200) {
                  var index=0;
                  let subscribersList = data.data;
                  skip = data.skip;
                  limit =data.limit;
                  docCount = data.docCount;
                  subscribersList.forEach(ele=>{
                      if(ele.name=="" || ele.name=="null")
                        ele.name =null ;
                      if(ele.email=="" || ele.email=="null")
                        ele.email =null ;
                      if(ele.phone=="" || ele.phone=="null")
                        ele.phone =null ;
                  })
                for (const iterator of subscribersList) {
                        index++
                    //  console.log('opt_in------',iterator);
                    let val = "" ;
                    if(iterator.opt_in){
                        checkValue = "checked"
                        val = 'checked'
                        title = "Active"
                    }else{
                        checkValue = "unchecked"
                        val = "unchecked"
                        title = "Inactive"
                    }
                    let name;
                    if(iterator.name){
                        name = iterator.name.replace(
                            /[-#'"!@$*%,;)(]/g,
                            ""
                          )
                    }else {
                        name = iterator.name
                    }
                    let html = `<tr class="tr2" id="tr2"
                                 data-index="${index}"
                                 userId = "${iterator._id}"
                                 data-name="${name}"
                                 data-email="${iterator.email}"
                                 data-phone="${iterator.phone}"
                                 data-source="${iterator.source}"
                             >
                                <td width="1">
                                   <input type="checkbox" 
                                          class="chkbx" 
                                          onclick="onCheckboxClick(this)"
                                          name=${name}
                                          value=${iterator._id} />
                                </td>` 

                                if (name == null || name == 'undefined') {
                                    html += `<td data-label="Name"> </td>`;
                                } else {
                                    html += `<td data-label="Name">${name.replace(
                                        /[-#'"!@$*%,;)(]/g,
                                        ""
                                      )}</td>`;
                                }         

                                //  <td data-label="Name">${name}</td>`;

                                if (iterator.email == null || iterator.email == 'undefined') {
                                    html += `<td data-label="Email"> </td>`;
                                } else {
                                    html += `<td data-label="Email">${iterator.email}</td>`;
                                }

                                if (iterator.phone == null || iterator.phone == 'undefined') {
                                    html += `<td data-label="Phone"> </td>`;
                                } else {
                                    html += `<td data-label="Phone">${iterator.phone}</td>`;
                                }

                        html += `<td data-label="Opt-In">
                                <label class="toggleSwitch" onclick="">
                                    <input type="checkbox" 
                                           name="myRadios"  
                                           id=${iterator._id}  
                                           phone=${iterator.phone}
                                           onclick="optInOptOutUser(this,id)" 
                                           data-source=${iterator.source}
                                           checkValue =${checkValue}
                                           title =${title}
                                           ${val}/>
                                    <span></span>
                                    <a></a>
                                </label>
                            </td> `

                        html += `<td data-label="Action">
                                <img src="dist/img/updated-icon/edit@2x.png"
                                    data-target="#edit_agents" 
                                    id=${iterator._id}  
                                    data-toggle="modal"
                                    class="fa fa-pencil editUser" 
                                    onclick="onEditBtn(id,this)" 
                                    data-backdrop="static" 
                                    data-source=${iterator.source}
                                    data-keyboard="false" 
                                    title="Edit"> 
                                
                                <img src="dist/img/updated-icon/delete@2x.png"
                                    class="fa fa-trash deletUser" 
                                    id=${iterator._id} 
                                    data-source=${iterator.source}
                                    onclick="onDeleteUser(id,this)"
                                    title="Delete" 
                                    delete-id=${iterator._id}> 
                                
                            </tr>`;
                    $(".ExcelData").append(html);
                }
            } else if (data.statusCode == 203) {
                alert(data.message);
            }
        },error: function (jqXHR, textStatus, err) {  
            if(jqXHR.responseJSON.statusCode == 403) {
            window.location.replace("https://admin.helloyubo.com/login");
            } else {
            alert("Something went wrong, please try again")
            }
        }
    });
}
getUserList();

//**get a particular user data to render on edit model */
selectedSource = "";

function onEditBtn(id, el) {
    editSelectedUser = id;
    selectedSource = $(el).attr("data-source");
    var data = {
        userId: editSelectedUser,
        source: selectedSource,
    };
    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: "GET",
        url: "/viewUserById",
        success: function(data) {
            if (data.statusCode == 200) {
                let agentData = data.data;
                for (const key in agentData) {
                    $(`#${key}`).val(agentData[key]);
                }
            } else {
                swal({
                    title: data.message,
                    icon: "error",
                    timer: 3500,
                });
                alert(data.message);
            }
        },error: function (err) {  
            alert("Something went wrong, please refresh the page")
        }
    });
}

function discard(e) {
    var r = confirm("Are you sure you want to discard changes");
    if (r === true) {
        $(".closeModal").attr("data-dismiss", "modal");
    } else {
        $(".closeModal").removeAttr("data-dismiss");
    }
}

function addUserBtn() {
    // clear all the previous data on add user btn click
    $("#userName").val("");
    $("#userPhone").val("");
    $("#userEmail").val("");
}
//********** on select excel file get the array  of document *******//
var ExcelToJSON = function() {
    this.parseExcel = function(file) {
        let filename = file.name.split('.')[0];
        var reader = new FileReader();
        reader.onload = function(e) {
            var data = e.target.result;
            var workbook = XLSX.read(data, {
                type: "binary",
            });
            workbook.SheetNames.forEach(function(sheetName) {
                // Here is your object
                var XL_row_object = XLSX.utils.sheet_to_row_object_array(
                    workbook.Sheets[sheetName]
                );
                var json_object = JSON.stringify(XL_row_object);
                jQuery("#xlx_json").val(json_object);
                let agentdata = JSON.parse(json_object);
                let clientId = document.getElementById("clientId").value;

                let filteredUserArray = [];
                agentdata.forEach((ele) => {
                    let obj ={};
                    for(let key in ele){
                         let key2 = key.trim().toLowerCase();
                         obj[key2] = ele[key] 
                    }
                    obj.clientId = clientId;
                    filteredUserArray.push(obj);
                });
                var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?]+/;
                let formatStatus = false;
                filteredUserArray.forEach(ele=>{
                    if(format.test(ele.name) == 'true' || format.test(ele.name) === true)
                        formatStatus =true;
                        if (
                            format.test(ele.name) === "false" ||
                            format.test(ele.name) === false
                          )
                            formatStatus = true;
                    if(ele.phone != "" && ele.phone != undefined && ele.phone != null && ele.phone !='undefined')
                      ele.phone = ele.country_code + ele.phone
                }) 
                if(!formatStatus){
                    swal({
                        title: "You can not use special character in the name field of excel sheet, please remove and try again.",
                        icon: "error",
                        type: 'info',
                        confirmButtonText: 'OK'
                      }).then(() => {
                           location.reload();
                      });
                      return ;
                }
               
                let allKeysOfExcellSheet = Object.keys(filteredUserArray[0]);
                let tempArray = [];
                allKeysOfExcellSheet.forEach(ele=>{
                    tempArray.push(ele.trim().toLowerCase());
                })
                allKeysOfExcellSheet = tempArray ;
                // if (
                //     allKeysOfExcellSheet.includes("name") == true &&
                //     allKeysOfExcellSheet.includes("email") == true &&
                //     allKeysOfExcellSheet.includes("country_code") == true &&
                //     allKeysOfExcellSheet.includes("phone") == true
                // ) {
                    addMultipleUser(filteredUserArray,filename);
                // } else {
                //     swal({
                //         title: "Looks like the file format you have uploaded is not valid.",
                //         icon: "",
                //         timer: 4500,
                //     });
                // }
            });
        };
        reader.onerror = function(ex) {
            console.log(ex);
        };
        reader.readAsBinaryString(file);
    };
};

function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    var xl2json = new ExcelToJSON();
    xl2json.parseExcel(files[0]);
}
document
    .getElementById("loadSlotJson")
    .addEventListener("change", handleFileSelect, false);

function addMultipleUser(data,filename) {
    filename=filename.replace(' ', '_');
    showLoader();
    $.ajax({
        data: { userArray: data ,filename : filename },
        method: "POST",
        url: "/addMultipleUser",
        beforeSend: function() {
            swal({
                title: "Processing...",
                button: false,
                closeOnClickOutside: true,
                closeOnEsc: false,
                timer: 3500,
            });
        },
        success: function(res) {
            hideLoader();
            if (res.statusCode == 200) {
                // console.log('res data',res)
                if(res.data.accountType == "Gupshup"){
                swal({
                    title:  res.message,
                    icon: "success",
                    text: `${res.data.optIned_number} phone numbers have been opted in while ${res.data.not_optIned_number} phone numbers are either invalid or already opted in. ${res.data.numberOfRejectedUser} users already existed in the subscribers list.`,
                    type: 'info',
                    confirmButtonText: 'OK'
                  }).then(() => {
                    location.reload();
                  });
                }else{
                    swal({
                        title:  res.message,
                        icon: "success",
                        text: `${res.data.numberOfRejectedUser} users already existed in the subscribers list.`,
                        type: 'info',
                        confirmButtonText: 'OK'
                      }).then(() => {
                        location.reload();
                      });
                }

            } else if (res.statusCode == 201) {
                swal({
                    title: 'error!',
                    icon: "info",
                    text: res.message,
                    type: 'info',
                    confirmButtonText: 'OK'
                  }).then(() => {
                    location.reload();
                  });
            } else {
                swal({
                    title: data.message,
                    icon: "error",
                    timer: 9500,
                });
            }
        }, 
        timeout: 2000000 ,
        error: function (jqXHR, textStatus, err) {  
            hideLoader();
            if(jqXHR.responseJSON.statusCode == 403) {
            window.location.replace("https://admin.helloyubo.com/login");
            } else {
            alert("Something went wrong, please try again")
            }
        }
    });
}
/**To user update data */
$(".saveUpdates").click(function() {
    var data = {
        name: $("#name").val(),
        email: $("#email").val(),
        phone: $("#phone").val(),
        userId: editSelectedUser,
        source: selectedSource,
    };
    let phone = $("#phone").val();
    let email = $("#email").val();
    let phoneValidation = /[91]{2}[7-9][0-9]{9}$/;
    const emailValidation =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(!$("#name").val()) {
            swal({
            title: "Please enter user name",
            icon: "warning",
            timer: 3500,
        });
    } else if ($("#phone").val() < 0) {
        swal({
            title: "Phone number can not be negative",
            icon: "warning",
            timer: 3500,
        });
    } else if (phone && !phoneValidation.test(phone)) {
        swal({
            title: "Please select your country code and Enter valid mobile number!",
            icon: "warning",
            timer: 3500,
        });
    } else if (email && !emailValidation.test(String(email).toLowerCase())) {
        swal({
            title: "Please provide valid email id",
            icon: "warning",
            timer: 3500,
        });
    } else if (!phone && !email) {
        swal({
            title: "Phone or email is required",
            icon: "warning",
            timer: 3500,
        });
    }else {
    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: "POST",
        url: "/updateUser",
        success: function(data) {
            if (data.statusCode == 200) {
                swal({
                    title: data.message,
                    icon: "success",
                    timer: 3000,
                });
                location.reload();
            } else {
                swal({
                    title: data.message,
                    icon: "error",
                    timer: 3000,
                });
            }
        }, error: function (jqXHR, textStatus, err) {  
            if(jqXHR.responseJSON.statusCode == 403) {
            window.location.replace("https://admin.helloyubo.com/login");
            } else {
            alert("Something went wrong, please try again")
            }
        } 
    });
}
});
$(".chkall").click(function() {
    var z = this.checked;
    $(".chkbx").each(function() {
        this.checked = z;
    });
    showHideCreateGrpBtn();
});


/****************************
initially the group create button will hidden when start select user group create icon will be visible
 ****************************/
$(".bt2").addClass("hide");
// $(".creategroup").addClass("hide");
function showHideCreateGrpBtn() {
    // var i = 0;
    // window.a = [];
    // $(".chkbx").each(function() {
    //     if (this.checked == true) {
    //         $(".bt2").removeClass("hide");
    //         $(".creategroup").removeClass("hide");
    //         a.push($(this).attr("value"));
    //         i = 1;
    //     }
    // });
    // if (i != 1) {
    //     $(".bt2").addClass("hide");
    //     $(".creategroup").addClass("hide");
    //     $(".chkall").prop("checked", false);
    // } else if (i == 1) {
    //    $(".chkall").prop("checked", true);
    // }
}

/***************************************
for adding members to modal of given leads
*****************************************/
$(".creategroup").click(function(i) {
    $(".groupName").val('');
    selectedGroupid='';

    /***************************************
     Fetch list of created groups earlier to show it on create modal
    *****************************************/
    $.ajax({
        type: "POST",
        url: "/usergroupdata",
        data: "",
        success: function(data) {
            if (data) {
                $(".group_select").html("");
                $(".group_select").append(`<option>Create New</option>`);
                for (i = 0; i < data.data.length; i++) {
                    $(".group_select").append(
                        `<option groupId = ${data.data[i].groupId}>${data.data[i].groupName}</option>`
                    );
                }
            }
        },
        error: function(jqXHR, textStatus, err) {
            alert("Please refresh !");
        },
    });
    $("#creategroupmodel").modal("show");
});

/***************************************
add more user manually in modal ,presing add button 
*****************************************/
$(document).on("click", ".add_more_user", function(e) {
    let phone =$('.instant_phone').val() ;
    let email =$('.instant_email').val() ;
    const phoneValidation = /[91]{2}[7-9][0-9]{9}$/;
    const emailValidation = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(email=='' && $('.instant_phone').val()==''){
        swal({
            title: 'Email or phone is required' ,
            icon: "warning",
            timer: 3500
        });
        return;
    }else if(email && !emailValidation.test(String(email).toLowerCase())){
      swal({
          title: "Please provide valid email id",
          icon: "warning",
          timer: 3500
      }); 
      return;
    } else if ($(".instant_phone").val() < 0) {
        swal({
            title: "Phone number can not be negative",
            icon: "warning",
            timer: 3500,
        });
        return;
    } else if (phone && !phoneValidation.test(phone)) {
        swal({
            title: "Please select your country code and Enter valid mobile number!",
            icon: "warning",
            timer: 3500,
        });
        return;
    }
    $(".adduserdata").prepend(`<tr class ="userdatalist">
   <td  class="userdataname" data-name="${$(".instant_user").val()}"></span></label>${$(".instant_user").val()}</td>
   <td class="userdatamail" data-mail="${$(".instant_email").val()}">${$(".instant_email").val()}</td>
   <td class="userdataphone" data-phone="${$(".instant_phone").val()}">${$(".instant_phone").val()}</td>
    <td class="userdataid" data-id="Random${Math.random().toString(36).substr(2, 5)}">&#10005;</td> </tr>`);
    $(".instantuserdata").val("");
});

/******************************************
for deleting users from modal
*******************************************/
$(document).on("click", ".userdataid", function(e) {
    if ($(".userdatalist").length < 2) {
        return;
    }
    $(this).parent("tr").remove();
});

/*************************************
on chnage of group dropdown get group id and group name to add data in existing group
**************************************/
    var selectedGroupid = "";
    var selectedGroupName = "";

    $(".group_select").change(function () {
        // var selectedText = $(this).find("option:selected").text();
        selectedGroupName = $(this).val();
        var group_select = document.querySelector(".group_select");
        selectedGroupid = group_select.options[group_select.selectedIndex].getAttribute('groupid');
        // console.log('selectedGroupid 111',selectedGroupid);
        // console.log('selectedGroupName 111',selectedGroupName);
    });

/***********************************
for creating group of users ,create group api call
************************************/
$(document).on("click", ".createNewGroup", function() {
    let grpValidation = /^[a-zA-Z-0-9_]+$/;
    let grpname = $(".groupName").val();
    grpname=grpname.split(' ').join('_');
    $(".groupname").val(grpname)

    if ( grpname == "" && selectedGroupid=="") {
        alert("Please enter a group name or select existing group");
        return;
    } else {
        let creategroup = [],
            parentobj = {};
        $(".userdatalist").each(function(i) {
            let groupobj = {
                userId: $(this).find(".userdataid").attr("data-id"),
                name: $(this).find(".userdataname").attr("data-name"),
                mail: $(this).find(".userdatamail").attr("data-mail"),
                phone: $(this).find(".userdataphone").attr("data-phone"),
            };
            creategroup.push(groupobj);
        });

       if(selectedGroupid){
           let tempObj = { [selectedGroupName]  : creategroup } 
           parentobj['group_name']= selectedGroupName;
           parentobj['group_id']= selectedGroupid;
           parentobj['createdgroup'] = tempObj;
       }else{
           parentobj[$(".groupName").val()] = creategroup;
       }
       if(creategroup.length == 0){
        alert("Please select/add more than one user to create group");
        return;
       } else  if (grpname && !grpValidation.test(String(grpname))) {
        swal({
            title: "Groupname should not contain spaces and special characters. Underscore (_) is allowed.",
            icon: "warning",
            timer: 3500,
        });
       }else{
        $.ajax({
            type: "POST",
            url: "/creategroup",
            data: parentobj,
            success: function(data) {
                if (data.statusCode =="200" || data.statusCode == 200) {
                    swal({
                        title: data.message,
                        icon: "success",
                        timer: 3500,
                    });
                    location.reload();
                }else{
                    swal({
                        title: data.message,
                        icon: "error",
                        timer: 3500,
                    });
                }
            },
            error: function(jqXHR, textStatus, err) {
                alert("Please refresh !");
            },
        });
       }

     }
});


/***************************************
for adding more member,add new data of user to create group in modal
*****************************************/
$(document).on("click", ".addNewmember", function() {
    let creategroup = [];
    let groupId = $(".group_select option:selected").attr("groupid");

    $(".userdatalist").each(function(i) {
        let groupobj = {
            userId: $(this).find(".userdataid").attr("data-id"),
            name: $(this).find(".userdataname").attr("data-name"),
            mail: $(this).find(".userdatamail").attr("data-mail"),
            phone: $(this).find(".userdataphone").attr("data-phone"),
        };
        creategroup.push(groupobj);
    });
    $.ajax({
        type: "POST",
        url: "/addmoremember",
        data: { groupId: groupId, data: creategroup },
        success: function(data) {
            if (data) {
                alert(data.msg);
                location.reload();
            }
        },
        error: function(jqXHR, textStatus, err) {
            alert("Please refresh !");
        },
    });
});

/***************************************
 Delete agent function ajex call 
*****************************************/
function onDeleteUser(userId, el) {
    selectedSource = $(el).attr("data-source");
    var data = {
        userId: userId,
        source: selectedSource,
    };

    swal({
        title: "Are you sure want to delete ?",
        text: "Once deleted, you will not be able to recover this record!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                data: data,
                async: false,
                cache: false,
                method: "POST",
                url: "/deleteUser",
                success: function(data) {
                    if (data.statusCode == 200) {
                        swal({
                            title: data.message,
                            icon: "success",
                            timer: 3000,
                        });
                        location.reload();
                    } else {
                        swal({
                            title: data.message,
                            icon: "success",
                            timer: 3000,
                        });
                    }
                },error: function (jqXHR, textStatus, err) {  
                    location.reload();
                    if(jqXHR.responseJSON.statusCode == 403) {
                    window.location.replace("https://admin.helloyubo.com/login");
                    } else {
                    alert("Something went wrong, please try again")
                    }
                }  
            });
        } else {
            swal("Your data is safe now!");
        }
    });
}

/***************************************
// optIn opt out user api call 
*****************************************/
var currentValue = 0;
function optInOptOutUser(myRadio,id) {

    editSelectedUser = id;
    selectedSource = $(myRadio).attr("data-source");
    phone          = $(myRadio).attr("phone");
    checkValue     = $(myRadio).attr("checkValue");
    title          = $(myRadio).attr("title");

    if(phone === null || phone == 'null' ){
        swal({
            title: "Phone number is must to Opt-In a user ",
            icon: "error",
            timer: 3500,
        });
        // location.reload()
        return;
    }
    let opt_in_status = false;
    if(checkValue == 'checked'){
        opt_in_status = false ;
    }else{
        opt_in_status = true ;
    }

    currentValue = myRadio.value;
    let data ={
        "source"       :  selectedSource,
        "userId"       :  editSelectedUser,
        "opt_in"       :  opt_in_status,
        "phone"        :  phone
    }

    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: "POST",
        url: "/optInOptoutUser",
        beforeSend: function() {
            swal({
                title: "Processing...",
                button: false,
                closeOnClickOutside: true,
                closeOnEsc: false,
                timer: 3500,
            });
        },
        success: function(data) {
            if (data.statusCode == 200) {                
                swal({
                    title: 'Success!',
                    icon: "success",
                    text: data.message ,
                    type: 'success',
                    confirmButtonText: 'OK'
                  }).then(() => {
                    if(checkValue == 'checked')
                      $(myRadio).attr("checkValue","unchecked");
                    else
                      $(myRadio).attr("checkValue","checked");
                  });
            } else if (data.statusCode == 201) {
                swal({
                    title: 'Error!',
                    icon: "error",
                    text: data.message ,
                    type: 'error',
                    confirmButtonText: 'OK'
                  }).then(() => {
                    location.reload();
                  });
            } else {
                swal({
                    title: data.message,
                    icon: "error",
                    timer: 3500,
                });
            }
        }, error: function (jqXHR, textStatus, err) {  
            if(jqXHR.responseJSON.statusCode == 403) {
            window.location.replace("https://admin.helloyubo.com/login");
            } else {
            alert("Something went wrong, please try again")
            }
        }
    });
}



/***************************************
for adding members to modal of given leads
*****************************************/
$(".optinMultipleUser").click(function(i) {
    setMultiUserOptin();
});
/***********************************
for creating group of user for optin multiple user
************************************/
function setMultiUserOptin() {
        let userPhoneArray = [];
        $(".userdatalist").each(function(i) {
            let  phone =  $(this).find(".userdataphone").attr("data-phone");
            userPhoneArray.push(phone);
        });

        let userPhoneList =  userPhoneArray.filter(phone=>
        {
            return phone != 'null' && phone != null && phone != undefined && phone != 'undefined';
        });
        $.ajax({
            type: "POST",
            url: "/setMultiUserOptin",
            data: {clientId :  $("#clientId").val() , phoneArray: userPhoneList ,method : 'OPT_IN' },
            success: function(data) {
                if (data) {
                    if (data.statusCode == 200) {

                        swal({
                            title: 'Success!',
                            icon: "success",
                            text: `${data.data.listOfOptinNumbers.length} phone numbers have been opted in while ${data.data.listOfNotOptinNumbers.length} phone numbers were invalid` ,
                            type: 'success',
                            confirmButtonText: 'OK'
                          }).then(() => {
                            console.log('multi user optin  done ');
                            location.reload();
                          });
                    } else {
                        swal({
                            title:  data.message,
                            icon: "error",
                            type: 'info',
                            confirmButtonText: 'OK'
                          }).then(() => {
                            location.reload();
                          });
                    }
                }
            }, error: function (jqXHR, textStatus, err) {  
                if(jqXHR.responseJSON.statusCode == 403) {
                window.location.replace("https://admin.helloyubo.com/login");
                } else {
                alert("Something went wrong, please try again")
                }
            }
        });
};

$(".optoutMultipleUser").click(function(i) {
    let userPhoneArray = [];
    $(".userdatalist").each(function(i) {
        let  phone =  $(this).find(".userdataphone").attr("data-phone");
        userPhoneArray.push(phone);
    });

    let userPhoneList =  userPhoneArray.filter(phone=>
    {
        return phone != 'null' && phone != null && phone != undefined && phone != 'undefined';
    });
    $.ajax({
        type: "POST",
        url: "/setMultiUserOptin",
        data: {clientId :  $("#clientId").val() , phoneArray: userPhoneList ,method : 'OPT_OUT' },
        success: function(data) {
            if (data) {
                if (data.statusCode == 200) {

                    swal({
                        title: 'Success!',
                        icon: "success",
                        text: `${data.data.listOfOptinNumbers.length} phone numbers have been opted out while ${data.data.listOfNotOptinNumbers.length} phone numbers were invalid` ,
                        type: 'success',
                        confirmButtonText: 'OK'
                      }).then(() => {
                        console.log('multi user optin  done ');
                        location.reload();
                      });
                } else {
                    swal({
                        title:  data.message,
                        icon: "error",
                        type: 'info',
                        confirmButtonText: 'OK'
                      }).then(() => {
                        location.reload();
                      });
                }
            }
        }, error: function (jqXHR, textStatus, err) {  
            if(jqXHR.responseJSON.statusCode == 403) {
            window.location.replace("https://admin.helloyubo.com/login");
            } else {
            alert("Something went wrong, please try again")
            }
        }
    });
});

hideLoader();
function showLoader(){
    $('#loaderDivContainer').removeClass("hide-loader");
    $('.content').hide(); 
}
function hideLoader(){
    $('#loaderDivContainer').addClass("hide-loader");
    $('.content').show();
}

/***************************************
delete multiple user 
*****************************************/
$(".deleteMultipleUser").click(function(i) {
    DeleteMultipleUser();
});
function DeleteMultipleUser() {
swal({
    title: "Are you sure?",
    text: "Once selected users deleted, you will not be able to recover these users!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
}).then((isconfirm) => {
  if (isconfirm) {
        
    
    let userIdArray = [];
    $(".userdatalist").each(function(i) {
        let  userIds =  $(this).find(".userdataid").attr("data-id");
        userIdArray.push(userIds);
    });
    if(userIdArray.length == 0){
        swal({
            title: 'Error!',
            icon: "error",
            text: `Please select at least one user to delete ` ,
            type: 'error',
            confirmButtonText: 'OK'
        }).then(() => {
        });
    }else{
        $.ajax({
            type: "POST",
            url: "/deleteMultipleUser",
            data: { userIds : userIdArray  },
            success: function(data) {
                if (data) {
                    if (data.statusCode == 200) {
                        swal({
                            title: 'Success!',
                            icon: "success",
                            text: data.message,
                            type: 'success',
                            confirmButtonText: 'OK'
                        }).then(() => {
                            location.reload();
                        });
                    } else {
                        swal({
                            title:  data.message,
                            icon: "error",
                            type: 'info',
                            confirmButtonText: 'OK'
                        }).then(() => {
                            location.reload();
                        });
                    }
                }
            }, error: function (jqXHR, textStatus, err) {  
                if(jqXHR.responseJSON.statusCode == 403) {
                window.location.replace("https://admin.helloyubo.com/login");
                } else {
                alert("Something went wrong, please try again")
                }
            }
        });
    }
  }
 });
};

function loadMore(event){   
    skip = parseInt(skip) + parseInt(limit); 
    if(docCount >= skip){
        getUserList();
    }else{
        $( "#loadMoreButtonId" ).hide();
    }
}
function searchFunction(e){
    $(".ExcelData").html("");
    skip = 0;
    getUserList();
    return;
}
function onTypingEvent(e){
    let ascii, key = e.key;
        ascii = key.charCodeAt(0);
    let searchData = $("#searchBoxId").val();
    if(ascii != 66){
        if (searchData.length > 3){
            $(".ExcelData").html("");
            skip = 0;
            getUserList();
            return;
        }
    }
    if (searchData.length == 0){
        $( "#loadMoreButtonId" ).show();
        $(".ExcelData").html("");
        skip = 0;
        getUserList();
    }
}
// Listen for click on toggle checkbox
$('#select-all').click(function(event) {   
    $(".chkbx").each(function() {
        if(this.checked) {
            // Iterate each checkbox
                this.checked = true;   
                $(".adduserdata").append(
                    `<tr class ="userdatalist" data_id="${$(this).closest(".tr2").attr("userid")}">`
        
                  + ($(this).closest(".tr2").attr("data-name") == "null" || $(this).closest(".tr2").attr("data-name") == "undefined" ?
                           `<td  class="userdataname" data-name="${$(this).closest(".tr2").attr("data-name")}"> </td>` :
                                   `<td  class="userdataname" data-name="${$(this).closest(".tr2").attr("data-name")}">
                                    ${$(this).closest(".tr2").attr("data-name")} </td>`)
        
                  + ($(this).closest(".tr2").attr("data-email") == "null" || $(this).closest(".tr2").attr("data-email") == "undefined" ?
                        `<td class="userdatamail" data-mail="${$(this).closest(".tr2").attr("data-email")}"> </td>` :
        
                        `<td class="userdatamail" data-mail="${$(this).closest(".tr2").attr("data-email")}"> 
                        ${$(this).closest(".tr2").attr("data-email")}</td>`) 
                  + ($(this).closest(".tr2").attr("data-phone") == "null" || $(this).closest(".tr2").attr("data-phone") == "undefined" ? 
                      `<td class="userdataphone" data-phone="${$(this).closest(".tr2").attr("data-phone")}"> </td>` : 
                        `<td class="userdataphone" data-phone="${$(this).closest(".tr2").attr("data-phone")}">
                          ${$(this).closest(".tr2").attr("data-phone")}</td>`) +
        
                `<td class="userdataid" data-id="${$(this).closest(".tr2").attr("userid")}">&#10005;</td>
               </tr>`
                );                     
        } else {
                this.checked = false;  
                $(`tr[data_id=${$(this).closest(".tr2").attr("userid")}]`).remove();

        }
    });   
}); 

function onCheckboxClick(_this){
        
    /***************************************
     if leads checkbox is checked then add then in modal "userdatalist" class
    *****************************************/        
     if (_this.checked == true) {
        
        $(".adduserdata").append(
            `<tr class ="userdatalist" data_id="${$(_this).closest(".tr2").attr("userid")}">`

          + ($(_this).closest(".tr2").attr("data-name") == "null" || $(_this).closest(".tr2").attr("data-name") == "undefined" ?
                   `<td  class="userdataname" data-name="${$(_this).closest(".tr2").attr("data-name")}"> </td>` :
                           `<td  class="userdataname" data-name="${$(_this).closest(".tr2").attr("data-name")}">
                            ${$(_this).closest(".tr2").attr("data-name")} </td>`)

          + ($(_this).closest(".tr2").attr("data-email") == "null" || $(_this).closest(".tr2").attr("data-email") == "undefined" ?
                `<td class="userdatamail" data-mail="${$(_this).closest(".tr2").attr("data-email")}"> </td>` :

                `<td class="userdatamail" data-mail="${$(_this).closest(".tr2").attr("data-email")}"> 
                ${$(_this).closest(".tr2").attr("data-email")}</td>`) 
          + ($(_this).closest(".tr2").attr("data-phone") == "null" || $(_this).closest(".tr2").attr("data-phone") == "undefined" ? 
              `<td class="userdataphone" data-phone="${$(_this).closest(".tr2").attr("data-phone")}"> </td>` : 
                `<td class="userdataphone" data-phone="${$(_this).closest(".tr2").attr("data-phone")}">
                  ${$(_this).closest(".tr2").attr("data-phone")}</td>`) +

        `<td class="userdataid" data-id="${$(_this).closest(".tr2").attr("userid")}">&#10005;</td>
       </tr>`
        );
    }else{
        // $(`div[data_id=${$(_this).closest(".tr2").attr("userid")}]`).remove();
        $(`tr[data_id=${$(_this).closest(".tr2").attr("userid")}]`).remove();
    }
    showHideCreateGrpBtn();

}
