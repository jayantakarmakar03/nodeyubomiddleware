const db=require('../models/all-models');
const jwt = require('jsonwebtoken');
var moment = require('moment');

module.exports = {
    agentTemplate:agentTemplate,
    getAgentTempList:getAgentTempList,
    handleTemplateRequest: handleTemplateRequest,
}

async function agentTemplate(req,res) {

  const obj = req.body
  obj.clientId = req.user.clientId

  try {
    const agentTemp = await db.AgentTemplate.find({"clientId":obj.clientId}) 
     if(agentTemp.length>0) {
          const agentTemplate = await db.AgentTemplate.updateMany({"clientId": obj.clientId},
           {$set: {"clientId": obj.clientId, "template":obj.template}});

           return res.send({
            data:agentTemplate,
            statusCode:200,
            msg:"Agent template save successfully"
        })

     } else {
          const agentTemplateSave = await db.AgentTemplate(obj);
           agentTemplateSave.save()
           return res.send({
            data:agentTemplateSave,
            statusCode:200,
            msg:"Agent template save successfully"
        })

     }  

  } catch (error) {
    return res.send({
        error:error,
        statusCode:500,
        msg:"Agent template data not save"
    })
  }
}
  
async function getAgentTempList(req,res) {

  var clientId;
  if(req.body.clientId) {
     clientId = req.body.clientId
  } else {
     clientId = req.user.clientId
  }
  try {
    const agentTempList = await db.AgentTemplate.find({"clientId":clientId})
    return res.send({
      data:agentTempList,
      statusCode:200,
      msg:"Agent template data find successfully"
  })
    
  } catch (error) {
    return res.send({
      error:error,
      statusCode:500,
      msg:"Agent template data not found"
  })
  }
}


async function handleTemplateRequest(req, res) {
  const userInput = req.body.text;

  if (userInput) {
    const input = userInput.trim().toLowerCase(); // Trim and convert input to lowercase

    try {
      const template = await db.AgentTemplate.findOne({clientId: req.body.clientId});
      if (template) {
        const dynamicData = template.template.obj;
        const matchedSentences = [];

        for (const key in dynamicData) {
          const values = dynamicData[key];
          for (const sentence of values) {
            if (sentence.toLowerCase().includes(input)) { // Convert sentence to lowercase for comparison
              matchedSentences.push(sentence);
            }
          }
        }

        if (matchedSentences.length > 0) {
          res.json({ message: 'Successfully get the data!', sentences: matchedSentences });
        } else {
          res.json({ text: 'Data not found' });
        }
      } else {
        res.json({ text: 'Template not found' });
      }
    } catch (error) {
      res.status(500).json({ error: 'An error occurred' });
    }
  } else {
    res.json({ text: 'Invalid input' });
  }
}


