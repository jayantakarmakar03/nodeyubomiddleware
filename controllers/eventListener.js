const db = require("../models/all-models");
const fs = require("fs");
const config = require("../config/config");
CronJob = require("cron").CronJob;
const botController = require("../routes/bot");
const botApiController = require("../controllers/botApi");
scheduledCampaignController = require("../controllers/scheduledCampaign")

axios = require("axios");

// console.log("event listener .js");

module.exports = {
    eventListner: eventListner
};
async function eventListner() {
       
    const deactivatedUsers= await db.clientConfig.aggregate([  
        { $match:{cronStatus : true }},
        { $project: {
            clientId       : 1,
            clientObjId    : 1,
            cronStatus     : 1,
            resetContext   : 1,
            eventName      : 1,
            InactivityTime : 1,
            nodeName       : 1
          }
        },
        {
            $lookup: {
              from         : "userchats",
              localField   : "clientObjId",
              foreignField : "clientId",
              as           : "userchats",
            }
        },
        {
            $project: {
                _id            : 0,
                clientId       : 1,
                clientObjId    : 1,
                nodeName       : 1,
                cronStatus     : 1,
                resetContext   : 1,
                eventName      : 1,
                InactivityTime : 1,
                userchats : {
                    lastActiveTime    :1,
                    userId            :1,
                    userState         :1,
                    session           : {
                        email:1,
                        name : 1,
                        phone : 1
                    }
                }                
            }
          },
          {
            $project: {
                clientId       : 1,
                clientObjId    : 1,
                nodeName       : 1,
                cronStatus     : 1,
                eventName      : 1,
                resetContext   : 1,
                InactivityTime : 1,
                userchats: 
                    {
                        $filter: {
                            input : "$userchats",
                            as    : "userchats",
                            cond  : { $and: [{ "$eq": ["$$userchats.userState", "OPEN"] },
                                             { "$lt": ["$$userchats.lastActiveTime", { $subtract: [ new Date(Date.now()) ,  "$InactivityTime" ] }] }
                                            ] }
                        }
                    }
             }
          }
    ]);
    
    if(deactivatedUsers && deactivatedUsers.length>0){
        await deactivatedUsers.forEach(element => {
            // console.log('==============================' );
             element.userchats.forEach(item=>{
                  updateUserAndHitNode(element.nodeName,item.userId,element.clientObjId,element.clientId ,element.resetContext);
            })
        });
    }
}

async function updateUserAndHitNode(nodeName,userId,clientObjId,clientId,resetContext){
    try{
        if(!nodeName){
            nodeName= 'false' ;
        }
        /****Emit message to inactive user ***************/
        let data = {
           clientObjId  : clientObjId ,
           nodeName     : nodeName,
           userId       : userId,
           resetContext : resetContext,
           clientId     : clientId
        };
        // console.log('Emiting these data object via timeOut event 111111111111111111', data);
        io.sockets.to(userId).emit('timeOut',data);

        /****Update the user a inactive/closed since the user is not active for given span of time  ***************/
        let updateChat =  await db.Userchat.updateOne(
            { userId : userId, clientId: clientObjId },
            { $set:{
                userState      : "CLOSED"
              }
            });
        // console.log('===updateChat ', updateChat );
    }catch(err){
        console.log('Error occured in update user function:', err);
    }
}

/*********Run the cron job in every minutes [use it when code deployed on server]***************/

const job = new CronJob('*/1 * * * *', function() {
	const d = new Date();
	// console.log(' eventListner calling', d);
    eventListner();
    scheduledCampaignController.sendWhatsupCampaignInGroup()
});
job.start();

//*********************************************************** */

/*********Run the cron job in every seconds for testing purpose used it local***************/
// const job = new CronJob('* * * * * *', function() {
// 	const d = new Date();
// 	// console.log(' eventListner calling', d);
//     eventListner();
// });
// job.start();
//*********************************************************** */



