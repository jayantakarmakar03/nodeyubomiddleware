//**************** To add new webhook *******************//
document.getElementById("add_webhook").addEventListener("submit", function (event) {
    event.preventDefault();
    var webhookName, webhookUrl, webhookParameters, webhookApiKey, webhookRqwstMthd, webhookApiScrtKey;
    webhookName = $("#wName").val();
    webhookUrl = $("#wUrl").val();
    webhookParameters = $("#wParameters").val();
    webhookApiKey = $("#wApiKey").val();
    webhookRqwstMthd = $("#wRqwstMthd").val();
    webhookApiScrtKey = $("#wApiScrtKey").val();
    console.log("jgjg " + webhookName, webhookUrl, webhookParameters, webhookApiKey, webhookRqwstMthd, webhookApiScrtKey)
    $.ajax({
        type: "POST",
        url: "/addWebhook",
        timeout: 10000,
        data: {
            webhookName: webhookName, webhookUrl: webhookUrl, webhookParameters: webhookParameters,
            webhookApiKey: webhookApiKey, webhookRqwstMthd: webhookRqwstMthd, webhookApiScrtKey: webhookApiScrtKey
        },
        success: async function (resp) {
            if (resp == 'exists') {
                await alert('Webhook Name Exists');
            }
            else {
                location.reload();
            }
        },
        error: function (jqXHR, textStatus, err) {
            alert('Something went wrong, please refresh the page')
        }
    });
});


//**************** To show all webhooks on webhook page ******************//
// $.ajax({
//     type: "POST",
//     url: "/webhook",
//     data: {},
//     timeout: 10000,
//     success: function (resp) {
//         var trHTML = '';
//         resp.forEach(function (item, i) {
//             trHTML += '<div class="card intent_fader"><div class="intent-cont-header"><div class="intent-box-header"> <div class="user card-header">Webhook</div> <div class="user card-header" value=' + item._id + ' id=' + i + '>' + item.webhookName + '</div><div class="editWebhook" data-toggle="modal" data-target="#edit_webhook" value=' + item._id + '><i class="fa fa-edit" title="Edit Webhook"></i></div><div class="delBtn" value=""><button id=' + item._id + ' value="" type="button" onclick="deleteWebhook(this.id,this.value)" class="delete_webhook"><i class="fa fa-trash-o" title="Delete"></i></button></div></div></div></div>'
//             $('#tBody').html(trHTML).fadeIn();
//         });
//     }, error: function (jqXHR, textStatus, err) {
//         alert('Something went wrong, please refresh the page')
//     }
// });

//**************** To show all webhooks and actions on webhook page ******************//
$.ajax({
    type: "POST",
    url: "/webhook-actions-list",
    data: {},
    timeout: 10000,
    success: function (resp) {
        console.log("list", resp.webhookList, resp.actionList)
        if (resp.webhookList.length > 0) {
            let trHTML = '', webhookResp = resp.webhookList;
            webhookResp.forEach(function (item, i) {
                // trHTML += '<div class="card intent_fader "><div class="intent-cont-header"><div class="intent-box-header"> <div class="user card-header new-card-header">WebHook</div> <div class="user card-header" value=' + item._id + ' id=' + i + '>' + item.webhookName + '</div><div class="editWebhook" data-toggle="modal" data-target="#edit_webhook" value=' + item._id + '><i class="fa fa-edit" title="Edit Webhook"></i></div><div class="delBtn" value=""><button id=' + item._id + ' value="" type="button" onclick="deleteWebhook(this.id,this.value)" class="delete_webhook"><i class="fa fa-trash-o" title="Delete"></i></button></div></div></div></div>'

                trHTML += `<div class="col-lg-3 col-md-4 col-sm-6 webhook_actions"><div class="card intent_fader backColor-1"><div class="intent-cont-header"><div class="intent-box-header"> <div class="user card-header new-card-head  new-card-disp">WebHook</div> <div class="user card-header  card-name-cont " value='${item._id}' id='${i}'>${item.webhookName}</div><div class="editWebhook" data-toggle="modal" data-target="#edit_webhook" value='${item._id}' style="position:relative;"><img src="dist/img/updated-icon/edit@2x.png" class="action-edits" title="Edit webhook"></div><div class="delBtn" value=""><button id='${item._id}' value="" type="button" onclick="deleteWebhook(this.id,this.value)" class="delete_webhook"><img src="dist/img/updated-icon/delete@2x.png" class="del-actions" title="Delete"></button></div></div></div></div></div>`

                $('#tBody').html(trHTML).fadeIn();
            });
        }

        if (resp.actionList.length > 0) {
            let trHTML = '', actionResp = resp.actionList;
            actionResp.forEach(function (item, i) {
                // trHTML += '<div class="card intent_fader backColor-2"><div class="intent-cont-header"><div class="intent-box-header"> <div class="user card-header new-card-head">Action</div> <div class="user card-header" value=' + item._id + ' id=' + i + '>' + item.actionName + '</div><div class="editAction" data-toggle="modal" data-target="#edit_action" value=' + item._id + '><i class="fa fa-edit" title="Edit Action"></i></div><div class="delBtn" value=""><button id=' + item._id + ' value="" type="button" onclick="deleteAction(this.id,this.value)" class="delete_action"><i class="fa fa-trash-o" title="Delete"></i></button></div></div></div></div>'
                trHTML += `<div class="col-lg-3 col-md-4 col-sm-6 webhook_actions"><div class="card intent_fader backColor-2"><div class="intent-cont-header"><div class="intent-box-header"> <div class="user card-header new-card-head  new-card-disp">Action</div> <div class="user card-header  card-name-cont" value='${item._id}' id='${i}'>${item.actionName}</div><div class="editAction" data-toggle="modal" data-target="#edit_action" value='${item._id}'><img src="dist/img/updated-icon/edit@2x.png"  class="user_icons new-fa-edit" title="Edit Action"></div><div class="delBtn" value=""><button id='${item._id}' value="" type="button" onclick="deleteAction(this.id,this.value)" class="delete_action"><img src="dist/img/updated-icon/delete@2x.png" class="del-actions" title="Delete"></button></div></div></div></div></div>
                `
                $('#action-tBody').html(trHTML).fadeIn();
            });
        }
        if(resp.actionList.length > 0 || resp.webhookList.length > 0) {
            $(".back-img-story").css('background-image','none');
        } else {
            $(".back-img-story").css('background-image',"url('./dist/img/webhook.jpg')");}

    }, error: function (jqXHR, textStatus, err) {
        alert('Something went wrong, please refresh the page')
    }
});

//********** To delete the webhooks **********//
function deleteWebhook(id, name) {
    checkUserType("show", function (resp) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this webhook!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "POST",
                        url: "/deleteWebhook",
                        timeout: 2000,
                        data: { webhookId: id },
                        success: function (data) {
                            location.reload();
                        },
                        error: function (jqXHR, textStatus, err) {
                            //alert('text status '+textStatus+', err '+err)
                            swal("Something went wrong, please refresh the page");
                        }
                    });
                } else {
                    swal("Your webhook is safe!");
                }
            });
    });
}

//******************** To edit webhook *****************//
$("#joy").on("click", ".editWebhook", function () {
    var id = $(this).attr('value');
    $("#webhookId").attr("value", id)
    checkUserType("show", function (resp) {
        $.ajax({
            type: "POST",
            url: "/webhookbyid",
            timeout: 2000,
            data: { webhookId: id },
            success: function (data) {
                console.log("dff " + data)
                $('#webhookName').val(data[0].webhookName)
                $('#webhookUrl').val(data[0].webhookUrl)
                $('#webhookParameters').val(data[0].webhookParameters)
                $('#webhookApiKey').val(data[0].webhookApiKey)
                $('#webhookApiScrtKey').val(data[0].webhookApiSecretKey)
                $('#webhookRqwstMthd').val(data[0].webhookRequestMethod)
            }, error: function (jqXHR, textStatus, err) {
                alert("Something went wrong, please refresh the page")
            }
        });
    })
});

//*********** To update webhook *************//
document.getElementById("edit_webhook").addEventListener("submit", function (event) {
    event.preventDefault();
    checkUserType("show", function (resp) {
        var id, webhookName, webhookUrl, webhookParameters, webhookApiKey, webhookRqwstMthd, webhookApiScrtKey;
        id = $("#webhookId").val();
        webhookName = $('#webhookName').val();
        webhookUrl = $('#webhookUrl').val();
        webhookParameters = $('#webhookParameters').val();
        webhookApiKey = $('#webhookApiKey').val();
        webhookRqwstMthd = $('#webhookRqwstMthd').val();
        webhookApiScrtKey = $('#webhookApiScrtKey').val();

        $.ajax({
            type: "POST",
            url: "/updateWebhook",
            timeout: 10000,
            data: {
                webhookId: id, webhookName: webhookName, webhookUrl: webhookUrl, webhookParameters: webhookParameters,
                webhookApiKey: webhookApiKey, webhookRqwstMthd: webhookRqwstMthd, webhookApiScrtKey: webhookApiScrtKey
            },
            success: function (resp) {
                if (resp == 'exists') {
                    $('#webhookName').css('border-color', 'red');
                    $('#webhookName').val('Webhook name exits');
                } else {
                    location.reload();
                }
            },
            error: function (jqXHR, textStatus, err) {
                alert('Something went wrong, please refresh the page')
            }
        });
    });
});

$(".add_webhook").click(function () {
    let id = '#' + $(this).attr("modal-id");
    checkUserType("show", function (resp) {
        $(id).modal("show");
    })
})