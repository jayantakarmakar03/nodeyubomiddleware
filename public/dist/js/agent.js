function submitData() {

    $(document).ready(function () {

        let client_id = $("#client_id").val();
        let agentUserName = $("#agentUserName").val();
        let agentPassword = $("#agentPassword").val();

        if(client_id.trim().length == 0 ){
             swal({
                title: "Please provide client id",
                icon: "warning",
                timer: 5000
              });
        }else  if(agentUserName.trim().length  == 0 ){
            swal({
                title: "Please provide user name",
                icon: "warning",
                timer: 5000
              });
        }else if(agentPassword.trim().length  == 0 ){
            swal({
                title: "Please provide password",
                icon: "warning",
                timer: 5000
              });
        }else{
            $.ajax({
                type: "POST",
                url: "/agent/agentLogin",
                data: {
                    "password": agentPassword,
                    "clientId": client_id.trim(),
                    "email": agentUserName.trim()
                },
                timeout: 10000,
                success: async function (resp) {
                    if (resp) {
                        if (resp.statusCode === 200) {
                            swal({
                                title: "Logged in successfully",
                                icon: "success",
                                timer: 10000
                              });

                            let agentDetailsObj = {
                                "authtoken": resp.authtoken,
                                "clientId" : btoa(resp.clientId),
                                "agentId" : btoa(resp._id),
                                "agentName" : btoa(resp.name),
                                "agentEmail" : btoa(resp.email),
                            }
                            let agentDetails;
                            let checkboxValue = $("#checkboxButton").val();
                           
                            if (checkboxValue === true || checkboxValue == 'true') {
                                localStorage.setItem('agentDetails', JSON.stringify(agentDetailsObj));
                                agentDetails = localStorage.getItem('agentDetails');
                            } else {
                                sessionStorage.setItem('agentDetails', JSON.stringify(agentDetailsObj));
                                agentDetails = sessionStorage.getItem('agentDetails');
                            }

                            window.location = "/agent/agentPanel";
                            localStorage.setItem('NotifiedUserIds',JSON.stringify([]));
                            localStorage.setItem('agentNotificationData',JSON.stringify([]));
                            localStorage.setItem('myRequestsToAgents',JSON.stringify([]));

                        } else {
                            console.log(resp.message)
                            swal({
                                title: resp.message,
                                icon: "warning",
                                timer: 10000
                              });
                        }
                    }
                    else {
                        console.log('error occures')
                    }
                },
                error: function (jqXHR, textStatus, err) {
                    alert('Something went wrong, please refresh the page', err)
                }
            });
       }
    });
}


$(document).ready(function () {
    $('#checkboxButton').change(function () {
        yyyy = $('#checkboxButton').val(this.checked);
        let checkboxValue = $("#checkboxButton").val();
    });

    let agentDetails = JSON.parse(localStorage.getItem('agentDetails')) || JSON.parse(sessionStorage.getItem('agentDetails'));

    if(agentDetails){
        if(agentDetails.authtoken){
            window.location = "/agent/agentPanel";
        }else{
            window.location.replace("/agent/agentLogin");
        }
    }
 


});

$(window).on("keydown", function (e) {
    if (e.which == 13) {
        submitData();
    }
});

function forgotPassword(){
 swal({
    title: "Are you  sure want to recover your password",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  }).then((forgot) => {
    if (forgot) {
      window.location.replace('/agent/agentForgotPassword')
    }
  });
}



