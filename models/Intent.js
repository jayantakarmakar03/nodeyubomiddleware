var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
    mongoose.connect(require('./connection-string'));
}


//Guest model schema.
var newSchema = new Schema({
    'clientId': { type: String },
    'tag': { type: String },
    'patterns': { type: Array },
    'responses': { type: Array },
    'entities': { type: Array, default: [] },
    'entityType': { type: String }, //strong or weak entity type
    'active_status': { type: String, default: "on" },
    'createdAt': { type: Date, default: Date.now },
    'updatedAt': { type: Date, default: Date.now },
    'analyticsFlag': { type: Boolean, default: false }
});

newSchema.pre('save', function(next) {
    this.updatedAt = Date.now();
    next();
});

newSchema.pre('update', function() {
    this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
    this.update({}, { $set: { updatedAt: Date.now() } });
});


module.exports = mongoose.model('Intent', newSchema);