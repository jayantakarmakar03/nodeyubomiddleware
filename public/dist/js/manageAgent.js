var totalOnlineUser = 0;
var totalAgent = 0;
var agentIds = [];
var editSelectedUser;
let userHandledByAgent = 0;

function getHumanAgentList() {
    $.ajax({
        async: false,
        cache: false,
        method: 'POST',
        url: '/agent/view-all-human-agent',
        success: async function(data) {
            if (data.statusCode == 200) {
                let agentdata = data.res;
                if (agentdata.length > 0) {
                    await agentdata.forEach(element => {
                        if (element.totalMilliSec > 0) {
                            let avgResTime = element.totalMilliSec / element.totalCount;
                            element.avgResTime = millisToMinutesAndSeconds(avgResTime); // "4:59"

                        } else {
                            element.avgResTime = 'NA';
                        }
                    });
                    await agentdata.map(ele => {
                        userHandledByAgent = userHandledByAgent + ele.handledUser;
                    })
                }
                $(".agentHandovers").html("");
                $(".agentHandovers").html(userHandledByAgent);

                await localStorage.setItem('access-token', data.accessToken);
                totalAgent = agentdata.length;
                $('.agentlist').html('')
                $('.totalAgent').html(agentdata.length);
                for (const iterator of agentdata) {

                    let html = `<tr>
          <td data-label="Name">${iterator.name}</td>
          <td data-label="Email">${iterator.email}</td>
          <td data-label="Avg Response">${iterator.avgResTime} </td>
          <td data-label="handledd users">${iterator.handledUser}</td>
          <td data-label="Avalibility">
             <i class ="fa fa-circle text-danger mr-2
  checkAvailable" availibilityId = ${iterator._id} ></i>
          </td>
          <td data-label="Action">
                <i data-target="#edit_agents"
  id=${iterator._id}  data-toggle="modal" class="fa fa-pencil editAgent" onclick="onEditBtn(id)" data-backdrop="static" data-keyboard="false"
  title="Edit"></i>
                <i class="fa fa-trash deletAgent" id=${iterator._id} onclick="onDeleteAgent(id)" title="Delete" delete-id=${iterator._id}></i>`
                    if (iterator.isBlocked) {
                        html += ` <i class="fa fa-ban blockAgent" blocktype="false"
  title="click to Unblock" id=${iterator._id} onclick="onBlockAgent(id,'false')" block-id=${iterator._id}  ></i>`
                    } else {
                        html += ` <i class="fa fa-unlock blockAgent" blocktype="true"
  title="click to block" id=${iterator._id} onclick="onBlockAgent(id,'true')" block-id=${iterator._id}  ></i>`
                    }

                    `</td>
                    </tr>`
                    $('.agentlist').append(html);
                }

            } else {
                console.log(data.message)
            }
        }
    });
}
getHumanAgentList();
var socket = io.connect("https://helloyubo.com:4900", {
    transports: ["websocket"],
    secure: true
});
// after send agent id receive the online agents id in array so that we can count how many agent are online  now //
socket.on('onlineAgentsList', function(onlineAgentsList) {
    let clientId = getClientId()
        //** from all agent list  (onlineAgentsList) filter agents by clientid ,so client get only agents working usnder client//
    let agentIdsbyClientId = onlineAgentsList.filter(Element => {
        return clientId == Element.clientId;
    });
    //** store (agentIds) only ids of agents  in an array ,which we can user for online ofline functionality in next step//
    agentIds = agentIdsbyClientId.map(ele => {
        return ele.agentId;
    })
    $('.checkAvailable').each(function() {
        if (agentIds.includes($(this).attr('availibilityId'))) {
            $(this).addClass('text-success')
            $(this).removeClass('text-danger')
        } else {
            $(this).removeClass('text-success')
            $(this).addClass('text-danger')
        }
    })

    //** render in html the number of total online agents //
    $('.availableOnlineAgents').html(agentIdsbyClientId.length);
    //** get total number of online agents  //
    totalOnlineUser = agentIdsbyClientId.length;
    //** render in html the number of total offline agents //
    $('.offlineAgents').html(totalAgent - totalOnlineUser);
});

function addAgentsBtn() {
    // clear all the previous data on add agents btn click 
    $('#agentName').val(''),
        $('#agentphone').val(''),
        $('#agentPassword').val(''),
        $('#agentEmail').val(''),
        $('#AgentDepartment').val(''),
        $('#AgentPriority').val('')
}
/**for agent creation */
$('.createagent').click(function() {
    var data = {
        name: $('#agentName').val(),
        mobile: $('#agentphone').val(),
        password: $('#agentPassword').val(),
        email: $('#agentEmail').val(),
        department: $('#AgentDepartment').val(),
        periority: $('#AgentPriority').val()
    }
    let email = $('#agentEmail').val();
    let password = $('#agentPassword').val();
    let agentName = $('#agentName').val();
    let agentphone = $('#agentphone').val();

    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    phoneValidation = /^[0-9]*$/;
    if (!agentName) {
        swal({
            title: "Please provide agent name.",
            icon: "warning",
            timer: 3500
        });
    } else if (!agentphone) {
        swal({
            title: "Please provide a phone number.",
            icon: "warning",
            timer: 3500
        });
    } else if (!phoneValidation.test(agentphone)) {
        swal({
            title: "Phone provide a valid phone number.",
            icon: "warning",
            timer: 3500
        });
    } else if (!re.test(String(email).toLowerCase())) {
        swal({
            title: "Please provide a valid email address.",
            icon: "warning",
            timer: 3500
        });
    } else if (!password) {
        swal({
            title: "Please provide a password.",
            icon: "warning",
            timer: 3500
        });
    } else {
        $.ajax({
            data: data,
            async: false,
            cache: false,
            method: 'POST',
            url: '/agent/add-human-agent',
            success: async function(data) {
                if (data.statusCode == 200) {
                    await swal({
                        title: "Agent added successfully",
                        icon: "success",
                        timer: 1500
                    });
                    $('#add_agents').modal('hide');
                    getHumanAgentList();
                } else if (data.statusCode == 203) {
                    alert(data.message)
                }
            }
        });
    }
})


//**get client id from session **//
function getClientId() {
    let clientId = $('.clientId').val();
    return clientId;
}


/**To agent update data */
$('.saveUpdates').click(function() {

    let agentName = $('#name').val();
    let agentphone = $('#mobile').val();
    phoneValidation = /^[0-9]*$/;
    if (!agentName) {
        swal({
            title: "Please provide agent name",
            icon: "warning",
            timer: 3500
        });
    } else if (!agentphone) {
        swal({
            title: "Phone provide number ",
            icon: "warning",
            timer: 3500
        });
    } else if (!phoneValidation.test(agentphone)) {
        swal({
            title: "Phone provide valid phone number ",
            icon: "warning",
            timer: 3500
        });
    } else {
        var data = {
            name: $('#name').val(),
            periority: $('#periority').val(),
            email: $('#email').val(),
            department: $('#department').val(),
            mobile: $('#mobile').val(),
            agentId: editSelectedUser,
        }
        $.ajax({
            data: data,
            async: false,
            cache: false,
            method: 'POST',
            url: '/agent/update-human-agent',
            success: async function(data) {
                if (data.statusCode == 200) {
                    await swal({
                        title: "Agent Updated successfully",
                        icon: "success",
                        timer: 3000
                    });
                    $('#edit_agents').modal('hide');
                    getHumanAgentList();
                } else {
                    alert(data.message)
                }
            }
        });
    }
})


function getAgentTakingControls() {
    $.ajax({
        async: false,
        cache: false,
        method: 'POST',
        url: '/agent/agentTakencontrolsCount',
        success: async function(data) {
            if (data.statusCode == 200) {
                $('#chat_handling_by_agent').html(data.res.chat_handling_by_agent);
            } else {
                console.log(data.message)
            }
        }
    });
}
getAgentTakingControls();

function discard(e) {
    var r = confirm("Are you sure you want to discard changes");
    if (r === true) {
        $('.closeModal').attr('data-dismiss', 'modal');
    } else {
        $(".closeModal").removeAttr('data-dismiss');
    }

}
//****** on document load execute this code ******// 
$(document).ready(function() {
        //  getUserHandledByAgent();
        AgentChatExchanges();
        let clientId = getClientId();
        socket.emit('getOnlineAgentsArrayList', { "message": "get the array of online agents " });
        socket.on('getOnlineAgentsArrayList', onlineAgentsList => {

            //** from all agent list  (onlineAgentsList) filter agents by clientid ,so client get only agents working usnder client//
            let agentIdsbyClientId = onlineAgentsList.filter(Element => {
                return clientId == Element.clientId;
            });
            //** store (agentIds) only ids of agents  in an array ,which we can user for online ofline functionality in next step//
            agentIds = agentIdsbyClientId.map(ele => {
                return ele.agentId;
            })

            // **** set the red and green dot of agent list , on page load****/
            $('.checkAvailable').each(function() {
                if (agentIds.includes($(this).attr('availibilityId'))) {
                    $(this).addClass('text-success')
                    $(this).removeClass('text-danger')
                } else {
                    $(this).removeClass('text-success')
                    $(this).addClass('text-danger')
                }
            })

            //** render in html the number of total online agents //
            $('.availableOnlineAgents').html(agentIdsbyClientId.length);

            //** get total number of online agents  //
            totalOnlineUser = agentIdsbyClientId.length;

            //** render in html the number of total offline agents //
            $('.offlineAgents').html(totalAgent - totalOnlineUser);


        })
    })
    //**get a particular agent data to render on edit model */
function onEditBtn(id) {
    editSelectedUser = id;
    var data = {
        "agentId": editSelectedUser,
    }
    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: 'GET',
        url: '/agent/view-agent',
        success: async function(data) {
            if (data.statusCode == 200) {
                let agentData = data.res[0];
                for (const key in agentData) {
                    $(`#${key}`).val(agentData[key])
                }
            } else {
                alert(data.message)
            }
        }
    });
}
//**block agent function ajex call */
function onBlockAgent(id, status) {
    let blocktype;
    if (status == 'true') {
        blocktype = true;
    } else {
        blocktype = false;
    }
    var agntid = id;
    var data = {
        "agentId": agntid,
        "isBlocked": blocktype
    }
    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: 'POST',
        url: '/agent/block-unblock-agent',
        success: async function(data) {
            if (data.statusCode == 200) {
                if (blocktype === true) {
                    socket.emit('onAgentBlock', { agentId: agntid, clientId: getClientId() })
                }
                await swal({
                    title: data.message,
                    icon: "success",
                    timer: 3000
                });
                $('#edit_agents').modal('hide');
                getHumanAgentList();
            } else {
                alert(data.message)
            }
        }
    });
}
//**Delete agent function ajex call */
function onDeleteAgent(agentId) {
    var data = {
        "agentId": agentId,
    }
    swal({
        title: "Are you sure want to delete ?",
        text: "Once deleted, you will not be able to recover this record!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                data: data,
                async: false,
                cache: false,
                method: 'POST',
                url: '/agent/delete-human-agent',
                success: async function(data) {
                    if (data.statusCode == 200) {
                        await swal({
                            title: "Agent deleted successfully",
                            icon: "success",
                            timer: 3000
                        });
                        $('#edit_agents').modal('hide');
                        getHumanAgentList();
                    } else {
                        alert(data.message)
                    }
                }
            });
        } else {
            swal("Your data is safe now!");
        }
    });
}
//** agent to user total chat conversation ***/
function AgentChatExchanges(agentId) {
    $.ajax({
        data: {},
        method: 'GET',
        url: '/agent/userAgentConversation',
        success: function(data) {
            if (data.statusCode == 200) {
                $(".agentChatExchanges").html(data.dataLength);
            } else {
                alert(data.message)
            }
        }
    });
}
//** number of agent handled user **/
//not in user for now 
function getUserHandledByAgent() {
    $.ajax({
        data: {},
        method: 'GET',
        url: '/agent/getHandledUserByAgent',
        success: function(data) {
            if (data.statusCode == 200) {
                $(".agentHandovers").html(data.data);
            } else {
                alert(data.message)
            }
        }
    });
}


function millisToMinutesAndSeconds(millis) {
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds + ' minutes';
}