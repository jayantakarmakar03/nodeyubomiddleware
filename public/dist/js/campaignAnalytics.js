$(document).ready(function(){
    getAnalyticsList();
})

var limit = 50;
var skip = 0;
var docCount = 0; 

let campaignEndpoint="https://campaign.helloyubo.com"

function getAnalyticsList() {
    let startDate = document.getElementById('sDate').value;
    let endDate = document.getElementById('eDate').value;
   
    if(!startDate)
        startDate =new Date();
    if(!endDate)
        endDate = new Date();

    let clientId = document.getElementById("clientId").value;
    let clientObjId = document.getElementById("clientObjId").value;
    let searchData = $("#searchBoxId").val();
    showLoader();
    let data = {
        skip        : skip,
        limit       : limit,
        clientId    : document.getElementById("clientId").value,
        groupName   : searchData,
        tempName    : searchData,
        startDate   : startDate,
        endDate     : endDate 
    };
    $.ajax({
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        async: false,
        cache: false,
        method: "POST",
        url: campaignEndpoint+"/getCampaignAnalytics",
        beforeSend: function() {
            swal({
                title: "Loading...",
                button: false,
                closeOnClickOutside: true,
                closeOnEsc: false,
                timer: 3000,
            });
        },
        success: function(data) {
            hideLoader();
            if (data.statusCode == 200) {
                // $(".analyticsList").html("");
                let analytcsDataArray = data.data;
                skip = data.skip;
                limit =data.limit;
                docCount = data.docCount;
                var index=0;
                for (const iterator of analytcsDataArray) {
                    let dateTime = convertdate(iterator.dataItem.createdOn);
                    index++;
                    let html = `<tr class="tr2" id="tr2"
                                 data-index="${index}"
                                 createdOn = ${iterator.dataItem.createdOn}
                                 tempName=${iterator.dataItem.tempName}
                                 sent=${iterator.sent}
                                 delivered=${iterator.delivered}
                                 read=${iterator.read}
                                 responded=${iterator.responded}
                                 failed=${iterator.failed}`
                        html+=' groupName="'+(iterator.dataItem.groupName ? iterator.dataItem.groupName :'')+'">';
                        html+=`<td data-label="createdOn">${dateTime}</td>
                             <td data-label="tempName">${iterator.dataItem.tempName}</td>
                             <td data-label="groupName">${iterator.dataItem.groupName}</td>
                             <td data-label="sent">${iterator.sent}</td> 
                             <td data-label="delivered">${iterator.delivered}</td>
                             <td data-label="read">${iterator.read}</td>
                             <td data-label="responded">${iterator.responded}</td>
                             <td data-label="failed">
                                 <a hre="javascript:void(0)"
                                    style = "color:#a00cf1;text-decoration: underline"
                                    createdOn = ${iterator.dataItem.createdOn}
                                    tempName=${iterator.dataItem.tempName}
                                    onclick="getFailedData(this)"`
                        html+=' groupName="'+(iterator.dataItem.groupName ? iterator.dataItem.groupName :'')+'">';            
                        html+=`${iterator.failed} 
                                 </a>
                             </td>
                             <td> 
                                <a hre="javascript:void(0)"
                                    createdOn = ${iterator.dataItem.createdOn}
                                    tempName= ${iterator.dataItem.tempName}
                                    onclick="getInfoData(this)"`
                        html+=' groupName="'+(iterator.dataItem.groupName ? iterator.dataItem.groupName :'')+'">';  
                        html+=`<img src="./dist/img/updated-icon/info@2x.png" class="info-analytic" height = "24px" title="Get Info"> 
                                </a>
                                <img src="dist/img/updated-icon/delete@2x.png"
                                    class="delete-analytic" 
                                    createdOn = ${iterator.dataItem.createdOn}
                                    tempName= ${iterator.dataItem.tempName}
                                    groupName= ${iterator.dataItem.groupName} 
                                    onclick="deleteAnalytics(this)"
                                    title="Delete">
                            </td>
                             
                             </tr>` ;
                    $(".analyticsList").append(html);
                }
            } else if (data.statusCode == 203) {
                alert(data.message);
            }else if(data.statusCode ==201 && data.error && data.error.code == 292){
                swal({
                    title: "Please select shorter date range",
                    icon: "info",
                    timer: 9000,
                });
            }else{
                console.log("No data found, Please select another date range ");
            }
        },error: function (jqXHR, textStatus, err) {  
            if(jqXHR.responseJSON.statusCode == 403) {
            window.location.replace("https://admin.helloyubo.com/login");
            } else {
            alert("Something went wrong, please try again")
            }
        }
    });
}
function showLoader(){
    $('#loaderDivContainer').removeClass("hide-loader");
    $('.content').hide(); 
}
function hideLoader(){
    $('#loaderDivContainer').addClass("hide-loader");
    $('.content').show();
}
function convertdate(dateTz){
    var date = new Date(dateTz);
    let fdate = moment(date).format("DD-MM-YYYY");
    let time = moment(date).format("hh:mm A");
    let formatedDate = `${fdate} at ${time}`
    return formatedDate;
}
    

function getFailedData(_this){
    $("#creategroupmodel").modal("show");

    let data = {
        clientId       : document.getElementById("clientId").value,
        createdOn      : $(_this).attr("createdOn"),
        groupName      : $(_this).attr("groupName"),
        tempName       : $(_this).attr("tempName"),
        sentStatus     : "failed"
    }
    console.log("dataaa",data)
    let clientId = document.getElementById("clientId").value;
    $.ajax({
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        async: false,
        cache: false,
        method: "POST",
        url: campaignEndpoint+"/getCampSpecificData",
        beforeSend: function() {
            swal({
                title: "Loading...",
                button: false,
                closeOnClickOutside: true,
                closeOnEsc: false,
                timer: 1000,
            });
        },
        success: function(data) {
            
            if (data.statusCode == 200) {
                $("#creategroupmodel").modal("show");
                $(".viewAnalyticsDiv").html("");
                var index=0;
                let analyticsData = data.data;
                let dateTime = convertdate(analyticsData[0].createdOn);
                $(".groupNameClass").html(analyticsData[0].groupName);
                $(".templateNameClass").html(analyticsData[0].tempName);
                $(".dateTime").html(dateTime);
              for (const iterator of analyticsData) {
                  let failedStatus = iterator.statuses.find(item => item.status == iterator.sentStatus);
                  index++
                  let html = `<tr class="tr2" id="tr2"
                                    data-index="${index}"
                                    _id = ${iterator._id}
                                    createdOn = ${iterator.createdOn}
                                    tempName=${iterator.tempName}
                                    sent_to=${iterator.sent_to}
                                    messageId=${iterator.messageId}
                                    sentStatus=${iterator.sentStatus}`

                            html+=' groupName="'+(iterator.groupName ? iterator.groupName :'NA')+'">';  
                            html+=`<td width="1"> ${iterator.sent_to} </td>
                              <td width="1"> ${iterator.sentStatus} </td>
                              ` 
                                if (iterator.errorReason && iterator.errorReason.code && iterator.errorReason.code == 131009 && iterator.errorReason.error_subcode && iterator.errorReason.error_subcode == 2494010) {
                                    html += `<td width="1"> Either phone number is invalid or country code is missing</td>`;
                                } else if(typeof iterator.errorReason == 'string') {
                                    html += `<td width="1"> ${iterator.errorReason}</td>`;
                                } else if(typeof iterator.errorReason == 'object') {
                                    html += `<td width="1"> ${iterator.errorReason.error_data.details}</td>`;
                                } else {
                                    html += `<td width="1"></td>`;
                                }
                                if(failedStatus != undefined && failedStatus.timeStamp) {
                                    html += `<td width="1"> ${convertdate(failedStatus.timeStamp) }</td>`;
                                } else {
                                    html += `<td width="1"> ${convertdate(iterator.createdOn) }</td>`;
                                }

                  $(".viewAnalyticsDiv").append(html);
              }
              
            } else if (data.statusCode == 203) {
                alert(data.message);
            }
        },error: function (jqXHR, textStatus, err) {  
            if(jqXHR.responseJSON.statusCode == 403) {
            window.location.replace("https://admin.helloyubo.com/login");
            } else {
            alert("Something went wrong, please try again")
            }
        }
    });
}


function getInfoData(_this){

    let data = {
        clientId       : document.getElementById("clientId").value,
        createdOn      : $(_this).attr("createdOn"),
        groupName      : $(_this).attr("groupName"),
        tempName       : $(_this).attr("tempName")   
    }
    let clientId = document.getElementById("clientId").value;
    $.ajax({
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        async: false,
        cache: false,
        method: "POST",
        url: campaignEndpoint+"/getCampSpecificData",
        beforeSend: function() {
            swal({
                title: "Loading...",
                button: false,
                closeOnClickOutside: true,
                closeOnEsc: false,
                timer: 1000,
            });
        },
        success: function(data) {
            
            if (data.statusCode == 200) {
                $("#creategroupmodel").modal("show");
                $(".viewAnalyticsDiv").html("");
                var index=0;
                let analyticsData = data.data;
                let dateTime = convertdate(analyticsData[0].createdOn);
                $(".groupNameClass").html(analyticsData[0].groupName);
                $(".templateNameClass").html(analyticsData[0].tempName);
                $(".dateTime").html(dateTime);
              for (const iterator of analyticsData) {
                let failedStatus = iterator.statuses.find(item => item.status == iterator.sentStatus);
                console.log(failedStatus)
                  index++;

                  let html = `<tr class="tr2" id="tr2"
                                    data-index="${index}"
                                    _id = ${iterator._id}
                                    createdOn = ${iterator.createdOn}
                                    tempName=${iterator.tempName}
                                    sent_to=${iterator.sent_to}
                                    messageId=${iterator.messageId}
                                    sentStatus=${iterator.sentStatus}`
                            html+=' groupName="'+(iterator.groupName ? iterator.groupName :'NA')+'">';  
                            html+=`<td width="1"> ${iterator.sent_to} </td>
                              <td width="1"> ${iterator.sentStatus} </td>` 
                                if (iterator.errorReason && iterator.errorReason.code && iterator.errorReason.code == 131009 && iterator.errorReason.error_subcode && iterator.errorReason.error_subcode == 2494010) {
                                    html += `<td width="1"> Either phone number is invalid or country code is missing</td>`;
                                } else if(typeof iterator.errorReason == 'string') {
                                    html += `<td width="1"> ${iterator.errorReason}</td>`;
                                } else if(typeof iterator.errorReason == 'object') {
                                    html += `<td width="1"> ${iterator.errorReason.error_data.details}</td>`;
                                } else {
                                    html += `<td width="1"></td>`;
                                }
                                if(failedStatus != undefined && failedStatus.timeStamp) {
                                    html += `<td width="1"> ${convertdate(failedStatus.timeStamp) }</td>`;
                                } else {
                                    html += `<td width="1"> ${convertdate(iterator.createdOn) }</td>`;
                                }
                  $(".viewAnalyticsDiv").append(html);
              }
              
            } else if (data.statusCode == 203) {
                alert(data.message);
            }
        },error: function (jqXHR, textStatus, err) {  
            if(jqXHR.responseJSON.statusCode == 403) {
            window.location.replace("https://admin.helloyubo.com/login");
            } else {
            alert("Something went wrong, please try again")
            }
        }
    });
}


function loadMore(event){   
    skip = parseInt(skip) + parseInt(limit); 
    if(docCount >= skip){
        getAnalyticsList();
    }else{
        $( "#loadMoreButtonId" ).hide();
    }
}

/***************************************
 Delete analytics data
*****************************************/
function deleteAnalytics(el) {
    let data = {
        clientId  : document.getElementById("clientId").value,
        createdOn : $(el).attr("createdOn"),
        tempName  : $(el).attr("tempName"),
        groupName : $(el).attr("groupName")
    }
    let clientId = document.getElementById("clientId").value;

    swal({
        title: "Are you sure want to delete ?",
        text: "Once deleted, you will not be able to recover this record!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(data),
                async: false,
                cache: false,
                method: "POST",
                url: campaignEndpoint+"/deleteCampAnalytics",
                success: function(data) {
                    if (data.statusCode == 200) {
                        swal({
                            title: data.message,
                            icon: "success",
                            timer: 3000,
                        });
                        location.reload();
                    } else {
                        swal({
                            title: data.message,
                            icon: "success",
                            timer: 3000,
                        });
                    }
                },error: function (jqXHR, textStatus, err) {  
                    location.reload();
                    if(jqXHR.responseJSON.statusCode == 403) {
                    window.location.replace("https://admin.helloyubo.com/login");
                    } else {
                    alert("Something went wrong, please try again")
                    }
                }  
            });
        } else {
            swal("Your data is safe now!");
        }
    });
}

function onTypingEvent(e){
    let ascii, key = e.key;
        ascii = key.charCodeAt(0);
        
    let searchData = $("#searchBoxId").val();
    if(ascii != 66){
        if (searchData.length > 3){
            $(".analyticsList").html("");
            skip = 0;
            getAnalyticsList();
            return;
        }
    }
    if (searchData.length == 0){
        $( "#loadMoreButtonId" ).show();
        $(".analyticsList").html("");
        skip = 0;
        getAnalyticsList();
    }
}
function searchFunction(e){
    $(".analyticsList").html("");
    skip = 0;
    getAnalyticsList();
    return;
}

function optedOutUserBtn(){
    $.ajax({
        type: "GET",
        url: "/stopPramotionNumber",
        success: function (resp) {
           $("#optedOutUsers").html("");

           let html;
           if (resp.statusCode === 200) {
              let index=0;
               const arrData = resp.data
               for(const obj of arrData) {
                  ++index;
                 // obj.createdAt=new Date(obj.createdAt);
                  obj.createdAt = moment(obj.createdAt).local().format('DD-MM-YYYYTHH:mm');
                   html += `
                      <tr>
                         <td> ${index} </td>
                        <td> ${obj.optOutNumber} </td>
                        <td> ${obj.createdAt} </td>
                        <td> <button id="${obj._id}" onclick="deleteOptedOutUser(id)" class="deleteMobileNo" > 
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                         <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                        </svg>
                         </button> 
                        </td>
                         
                      </tr>
                      `
               }
               $("#optedOutUsers").append(html);
            }
        },
        error: function (jqXHR, textStatus, err) {
            alert('Something went wrong, please refresh the page')
        }
    });
}

function downloadOptedUser() {

  $("#downloadTable").tableHTMLExport({
    type: 'csv',
    filename: 'OptedUser.csv',
    ignoreColumns: '.acciones,#primero',
    ignoreRows: '#ultimo'
  }); 
}

$(document).ready(function(){
    $("#searchOptOutData").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#optedOutUsers tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
});

async function deleteOptedOutUser(data) {
    const id = data

    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this mobile number!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {

          $.ajax({
                type: "DELETE",
                url: "/deleteStopPramotionNumber",
                data:{"id":id},
                success: function (resp) {
                    if(resp.statusCode==200) {
        
                        swal("Your mobile number has been deleted!", {
                            icon: "success",
                        });
                        $('#optedOutUser').modal('toggle');
                    }
                },
                error: function (jqXHR, textStatus, err) {
                    alert('Something went wrong, please refresh the page')
                }
            });

        } else {
          swal("Your mobile number is safe!");
          $('#optedOutUser').modal('toggle');
        }
    });

}

