window.onload = function() {
	var domain = "https://yubo.yugasa.org"
	let host = location.hostname;
	let url=location.href
	var clientId="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6ImZhcm1pdHJhX2NoYXRib3RfaGkiLCJpYXQiOjE2MDY3MzU0ODV9.VISw2-SaC_PqCp3mS-yYXu2HPNVQgBBh2fFiloeqzc0";

	var parent = document.createElement('div');
	parent.setAttribute('class', 'iframe-box');
	parent.setAttribute('id', 'iframe-box');

	var bootm = document.createElement('div');
	bootm.setAttribute('class', 'bootm-box');
	bootm.setAttribute('id', 'bootm-box');

	var chatstart = document.createElement('div');
	chatstart.setAttribute('class', 'chat-start bounce-top');

	var closeimg = document.createElement('div');
	closeimg.setAttribute('class', 'close-img');
	var imgclose  = document.createElement('img');
	imgclose.src = domain+"/img/cancel.png";
	closeimg.appendChild(imgclose);

	var outercircle = document.createElement('div');
	outercircle.setAttribute('class', 'outerCircle');

	var innercircle = document.createElement('div');
	innercircle.setAttribute('class', 'innerCircle');

	var icon = document.createElement('div');
	icon.setAttribute('class', 'icon');

	var bubbleimg  = document.createElement('img');
	bubbleimg.setAttribute('class', 'bubble-img');
	bubbleimg.src = domain+"/img/chat-bubble.svg";

	icon.appendChild(bubbleimg);

	chatstart.appendChild(outercircle)
	chatstart.appendChild(innercircle)
	chatstart.appendChild(icon)

	bootm.appendChild(chatstart);
    bootm.appendChild(closeimg);
    var iframe = document.createElement('iframe');
   	iframe.setAttribute('id', 'frame-lode');
    iframe.src = domain+"/chatWindow?clientId="+clientId+"&host="+host+"&url="+url;
    parent.appendChild(bootm);
    parent.appendChild(iframe);
    document.body.appendChild(parent);
	document.getElementById("bootm-box").addEventListener("click",function(){
		document.getElementById("iframe-box").classList.toggle("open-chat-window");
	});
};