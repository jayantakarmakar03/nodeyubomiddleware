var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
    mongoose.connect(require('./connection-string'));
}


var newSchema = new Schema({
    'clientId': {
        type: Schema.ObjectId,
        ref: 'Client'
    },
    "backgroundColor": { type: String },
    "onlineStatus": { type: String },
    "logoImage": { type: String },
    "widgetColor": { type: String },
    "emailCheckbox": { type: String, default: "off" },  //"on" for sending chat on email and "off" for not sending
    "registeredEmail": { type: String },
    "industryType": { type: String },
    "bgTemplate": { type: String, default: "off" }, // "on" to show selected bg template, "off" don't show bg template
    "chatbotDevice": { type: String }, //Desktop or mobile etc..
    "websiteURL": { type: String },
    "chatbotVisibility": { type: String, default: "off" }, //on means the bot will be displayed on website and off means remove from the website
    'createdAt': { type: Date, default: Date.now },
    'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});

newSchema.pre('update', function () {
    this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function () {
    this.update({}, { $set: { updatedAt: Date.now() } });
});



module.exports = mongoose.model('Settings', newSchema);
