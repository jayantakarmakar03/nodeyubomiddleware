window.onload = function() {
    var domain = "https://yubo.yugasa.org";
    let host = location.hostname;
    let url=location.href
    	// var domain = "https://localhost:4900";
    
    var parent = document.createElement('div');
    parent.setAttribute('class', 'iframe-box');
    parent.setAttribute('id', 'iframe-box');

    var bootm = document.createElement('div');
    bootm.setAttribute('class', 'bootm-box');
    bootm.setAttribute('id', 'bootm-box');

    var chatstart = document.createElement('div');
    chatstart.setAttribute('class', 'chat-start bounce-top');

    var closeimg = document.createElement('div');
    closeimg.setAttribute('class', 'close-img');
    var imgclose = document.createElement('img');
    imgclose.src = domain + "/img/close.svg";
    closeimg.appendChild(imgclose)

    //    var refreshdiv = document.createElement('div');
    //    refreshdiv.setAttribute('class', 'refresh');
    // var refreshimg  = document.createElement('img');
    // refreshimg.src=domain+"/img/refresh.png";
    // refreshimg.setAttribute("title","Restart")

    // refreshdiv.appendChild(refreshimg);


    var outercircle = document.createElement('div');
    // outercircle.setAttribute('class', 'outerCircle');

    var innercircle = document.createElement('div');
    // innercircle.setAttribute('class', 'innerCircle');

    var icon = document.createElement('div');
    icon.setAttribute('class', 'icon');
    var bubbleAnimation = document.createElement('div');
    bubbleAnimation.setAttribute('class', 'bubble-animation');
    var bubbleimg = document.createElement('img');
    bubbleimg.setAttribute('class', 'bubble-img');
    bubbleimg.src = domain + "/img/askgray.png";
    // icon.appendChild(bubblespan);
    icon.appendChild(bubbleAnimation);
    bubbleAnimation.appendChild(bubbleimg);

    chatstart.appendChild(outercircle)
    chatstart.appendChild(innercircle)
    chatstart.appendChild(icon)

    bootm.appendChild(chatstart);
    bootm.appendChild(closeimg);
    // parent.appendChild(refreshdiv);


    var iframe = document.createElement('iframe');
    iframe.setAttribute('id', 'frame-lode');
    // iframe.style.display = "none";
    iframe.src = domain + "/chatWindow?clientId=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6ImFtaXRfY2hhdGJvdCIsImlhdCI6MTU4MjI5MTM1Nn0.usXCALvPYRuzEour-DAUR2OUFZZfcIME8F84_of2EKA"+"&host="+host+"&url="+url;

    parent.appendChild(bootm);
    parent.appendChild(iframe);
    document.body.appendChild(parent);
    document.getElementById("bootm-box").addEventListener("click", function() {
        document.getElementById("iframe-box").classList.toggle("open-chat-window");

    });
};