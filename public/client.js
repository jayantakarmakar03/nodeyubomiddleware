var slider_count = 0;
var cvalue = userIdGenerator();
let storageData = document.getElementById('storageData').value
if (storageData != '') {
    console.log("storageData client.js", typeof(storageData), storageData)
    var session = { name: "", email: "", phone: "", lang: "en", date: "", storageData: storageData }
} else {
    var session = { name: "", email: "", phone: "", lang: "en", date: "" }
}
//var session = { name: "", email: "", phone: "", lang: "en", date: "" }
let firstMsg = true;
let setStory = false;
var default_node = 'False'
var Yubo = {
    request: function(url, body) {
        var that = this;
        return new Promise(function(resolve, reject) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                    if (xmlhttp.status == 200) {
                        var response = xmlhttp.responseText;
                        var message = null;
                        try {
                            message = JSON.parse(response);
                        } catch (err) {
                            reject(err);
                            return;
                        }
                        resolve(message);
                    } else {
                        reject(new Error("status_" + xmlhttp.status));
                    }
                }
            };
            xmlhttp.open("POST", url, true);
            xmlhttp.setRequestHeader("Content-Type", "application/json");
            xmlhttp.send(JSON.stringify(body));
        });
    },
    upload: async function(clientId) {
        const fileList = document.getElementById("file-input");
        var msg = "",
            fileSizeRequire = 512000;
        if (fileList.files[0].size <= fileSizeRequire) {
            try {
                if (!localStorage.getItem(clientId)) {
                    await Yubo.setCookie(clientId);
                    // Yubo.setCookie('Yubo_userId')
                }
            } catch (e) {
                await Yubo.setCookie(clientId);
            }

            const file = fileList.files[0];
            let base64File = await toBase64(file);
            let fileType = file.type.split('/');
            let fileValidation = validateFileType(file.type);
            if (!fileValidation) {
                let msg = "Selected file is not valid,Please choose an image file"
                return Yubo.createParagraph(msg);
            }
            const fdata = new FormData();
            fdata.append("fileList", fileList.files[0]);
            fdata.append("fileList", Yubo.getCookie(clientId));
            fdata.append("fileList", clientId);
            fdata.append("fileList", fileType[1]);
            await fetch("/yuboBot/fileUpload", {
                method: "post",
                body: fdata,
            }).then((resp) => {
                if (resp.status == 200 || resp.status == "200")
                    return resp.json();
            }).then(function(data) {
                let name = getnameEmail();
                socket.emit("message", {
                    clientId: document.getElementById("clientId").value,
                    userId: Yubo.getCookie(document.getElementById("clientId").value),
                    message: "Attachment sent",
                    messagetype: 'usermessage',
                    attachment: true,
                    name: name ? name : 'Unknown',
                    attachmentType: data.attachmentType,
                    attachmentLink: data.attachmentLink,
                    msgType: 'attachment'
                });
                return Yubo.createFileForHistory(data);
            });
        } else {
            msg = "The max file size is 500Kb";
            return Yubo.createParagraph(msg);

        }
    },
    createImageDiv: function() {
        loader.stop();
        let node = document.createElement("LI");
        let link = document.createElement("a");
        let img = document.createElement("img");
        link.setAttribute("href", "img/man_with_ipad.png");
        link.setAttribute("target", "_blank");

        link.innerHTML =
            '<img src="img/man_with_ipad.png" style="text-align: center; border-radius: 0;" width="100 px">';

        node.classList.add("sent");
        node.classList.add("mySlides");
        node.classList.add("fade");
        // node.appendChild(img);
        node.appendChild(link);
        document
            .getElementById("myList")
            .setAttribute("class", "slideshow-container");
        document.getElementById("myList").appendChild(node);
        scrollDown();
    },
    send: async function(text, clientId, options, optionsId, node, nodeText, nodeScore, payload, hubprop) {
        try {
            if (!localStorage.getItem(clientId)) {
                Yubo.setCookie(clientId);
            }
        } catch (e) {}
        let name = getnameEmail();
        socket.emit("message", {
            clientId: clientId,
            userId: Yubo.getCookie(document.getElementById("clientId").value),
            message: node ? nodeText : text,
            messagetype: 'usermessage',
            name: name ? name : 'Unknown'
        });
        var fb_rendered_form = document.getElementById("fb-rendered-form");
        url = document.getElementById("url").value;
        if (text.length == 0) {
            return;
        }
        if (fb_rendered_form) {
            fb_rendered_form.parentNode.previousSibling.remove();
            fb_rendered_form.parentNode.remove();
        }
        if (document.getElementById("node").value == "False" && !node) {
            node = "False";
            document.getElementById("node").value = "true";
        }
        if (Yubo.getLocalStorage(Yubo.getCookie(clientId)) && JSON.parse(Yubo.getLocalStorage(Yubo.getCookie(clientId))).chatControl == "yes") {
            loader.stop();
        } else {
            loader.start();
        }
        var value;

        if (options == true) {
            let id = text;
            value = document.getElementById(id).innerHTML; //here text is id.
            let x = document.getElementById(optionsId);
            x.style.display = "none";
            let node = document.createElement("LI");
            let para = document.createElement("P");
            para.innerHTML = value;
            node.classList.add("replies");
            node.appendChild(para);
            document.getElementById("myList").appendChild(node);
            scrollDown();
        } else {
            /**Create user chat node*/
            let node = document.createElement("LI");
            let para = document.createElement("P");
            value = text;
            para.innerHTML = text;
            node.classList.add("replies");
            node.appendChild(para);
            document.getElementById("myList").appendChild(node);
            scrollDown();
        }
        document.getElementById("msg").value = "";
        value = value.replace(/\&amp;/g, "&");
        //To check the first msg of user
        if (firstMsg) {
            locationobj = await ipLookUp();
            firstMsg = false;
        }
        if (node == undefined || node == 'False') {
            node = default_node;
        }
        let requestData = {
            userId: Yubo.getCookie(clientId),
            session: session,
            clientId: clientId,
            node: node,
            text: value,
            hubprop: hubprop,
            url: url,
            location: locationobj,
            category: document.getElementById("client_type").value,
            train: "False",
            unsubsribe: "False",
        };
        //Check for node score
        let getUserSessionData = Yubo.getSession(Yubo.getCookie(clientId));

        if (nodeScore) {
            const {
                ["nodeId"]: _, ...rest
            } = nodeScore; //Removing the nodeId
            if (getUserSessionData && "score" in getUserSessionData) {
                getUserSessionData.score[nodeScore.nodeId] = rest
            } else {
                let nodeScore2 = {};
                nodeScore2[nodeScore["nodeId"]] = rest;
                getUserSessionData = {...getUserSessionData, score: nodeScore2 }
            }
        };
        if (payload || (payload != 'false')) {
            getUserSessionData = {...getUserSessionData, payload: payload }
        };
        Yubo.setSession(Yubo.getCookie(clientId), getUserSessionData);
        requestData.session = getUserSessionData;

        this.request("/yuboBot/sendMessage", requestData).then(async function(reply) {
            if (reply.message) {
                default_node = reply.message.default_opt;
            } else {
                default_node = 'False';
            }
            if (reply.chatControl == 1) {
                loader.stop();
                return;
            }
            Yubo.setSession(Yubo.getCookie(document.getElementById("clientId").value), reply.message.session)
                // reply.message.firstMsg ? await ipLookUp() : "";
            if (reply.message && reply.message.hasOwnProperty("updateHubProp") && reply.message.updateHubProp) {
                await Yubo.updateHubProp(reply.message.updateHubProp)
            }
            let name = getnameEmail();
            setTimeout(async function() {
                socket.emit("botmessage", {
                    clientId: clientId,
                    userId: Yubo.getCookie(document.getElementById("clientId").value),
                    botmessage: reply.message.text,
                    messagetype: 'botmessage',
                    name: name ? name : ''
                });
                if (reply.message.image != undefined) {
                    reply.message.text = '<img class="media" src="' + reply.message.image + '">' + reply.message.text;
                }
                if (reply.message.tree && reply.message.tree != "false" && reply.message.tree.attachment != undefined && reply.message.tree.attachment != false) {
                    let ext = reply.message.tree.attachment.split('.').pop();
                    let attachmentType = getMediaType(ext);
                    if (ext == "image") {
                        reply.message.text = '<img class="media" src="' + reply.message.tree.attachment + '">' + reply.message.text;
                    } else {
                        let attachment = { "attachmentType": ext, "attachmentLink": reply.message.tree.attachment }
                        Yubo.createFile(attachment);
                    }
                }
                Yubo.createParagraph(reply.message.text);
                if (reply.message.reset != undefined && (reply.message.reset == true || reply.message.reset == "true")) {
                    let cid = userIdGenerator();
                    Yubo.setCookie(clientId, cid);
                    let uid = Yubo.getCookie(clientId);
                    Yubo.setSession(uid)
                    await Yubo.clearChatHistory();
                    return;
                }
                if (reply.message.tree && reply.message.tree != "false") {
                    if (reply.message.tree.hasOwnProperty("products") && reply.message.tree.type_of_node == "product") {
                        Yubo.createProducts(
                            reply.message.tree.products,
                            reply.message.client_id,
                            reply.message.intent
                        );
                    }
                    if (reply.message.tree.hasOwnProperty("videos")) {
                        Yubo.createVideos(
                            reply.message.tree.videos,
                            reply.message.client_id
                        );
                    }
                    if (reply.message.tree.hasOwnProperty("followup")) {
                        if (reply.message.tree.followup != "false") {
                            let delay = parseInt(reply.message.tree.followup.delayInMs);
                            let nodeName = reply.message.tree.followup.nodeName;
                            if (delay > 0 && nodeName != "" && nodeName != "Select Node") {
                                setTimeout(function() {
                                    Yubo.followup(clientId, nodeName);
                                }, delay);
                            }
                        }
                    }
                }
                if (reply.message.replies.length > 0) {
                    Yubo.createOptions(
                        reply.message.replies,
                        reply.message.client_id,
                        reply.message
                    );
                }
                if (reply.message.tree && reply.message.tree.form) {
                    await Yubo.form(
                        reply.message.client_id,
                        reply.message.tree.form,
                        reply.message.tree.webhook,
                        reply.message.tree.default,
                        reply.message.tree.node_name,
                        reply
                    );
                }
            }, 500);
            if (reply.message.type_option || reply.message.type_option == "true") {
                typingBox.on();
            } else {
                typingBox.off();
            }
            if (reply.message.typeNumber) {
                let input = document.getElementById("msg");
                input.setAttribute("type", "number");
            } else {
                let input = document.getElementById("msg");
                input.setAttribute("type", "text");
            }
        });
        
        if(!setStory && (clientId=='yugasa_chatbot' || clientId=='yubo_bot')){
            let story=document.getElementById("story").value
            console.log("steStory............. ",story)
            Yubo.setStory(story); 
            setStory=true;
        }

    },
    followup: async function(clientId, node, payload=false) {
        let trigger=await Yubo.nodeExists(node);
        if(!trigger){
            return;
        }
        var fb_rendered_form = document.getElementById("fb-rendered-form");
        if (fb_rendered_form) {
            fb_rendered_form.parentNode.previousSibling.remove();
            fb_rendered_form.parentNode.remove();
        }

        if (document.getElementById("node").value == "False" && !node) {
            node = "False";
            document.getElementById("node").value = "true";
        }
        var userId;
        try {
            if (!localStorage.getItem(clientId)) {
                Yubo.setCookie(clientId);
            }
        } catch (e) {}

        loader.start();
        // if (options == true) {
        //     let id = text;
        //     value = document.getElementById(id).innerHTML;//here text is id.
        //     let x = document.getElementById(optionsId);
        //     x.style.display = "none";
        //     let node = document.createElement("LI");
        //     let para = document.createElement("P");
        //     para.innerHTML = value;
        //     node.classList.add("replies");
        //     node.appendChild(para);
        //     document.getElementById("myList").appendChild(node);
        //     scrollDown();

        // } else {
        //     /**Create user chat node*/
        //     let node = document.createElement("LI");
        //     let para = document.createElement("P");
        //     value = text;
        //     para.innerHTML = text;
        //     node.classList.add("replies");
        //     node.appendChild(para);
        //     document.getElementById("myList").appendChild(node);
        //     scrollDown();
        // }

        if (node == undefined || node == 'False') {
            node = default_node;
        }

        document.getElementById("msg").value = "";
        let sess=session
        let getUserSessionData = Yubo.getSession(Yubo.getCookie(clientId));
        if (payload || (payload != 'false')) {
            getUserSessionData = {...getUserSessionData, payload: payload }
        };
        Yubo.setSession(Yubo.getCookie(clientId), getUserSessionData);
        sess = getUserSessionData;
        this.request("/yuboBot/followUp", {
            userId: Yubo.getCookie(clientId),
            session: sess,
            clientId: clientId,
            node: node,
            text: "",
            category: document.getElementById("client_type").value,
            train: "False",
            unsubsribe: "False",
        }).then(async function(reply) {
            if (reply.message) {
                default_node = reply.message.default_opt;
            } else {
                default_node = 'False';
            }
            if (reply.chatControl == 1) {
                return;
            }
            if (reply.message && reply.message.hasOwnProperty("updateHubProp") && reply.message.updateHubProp) {
                await Yubo.updateHubProp(reply.message.updateHubProp)
            }
            // reply.message.updateHubProp ? (await Yubo.updateHubProp(reply.message.updateHubProp)) : "";
            setTimeout(function() {
                if (reply.message.image != undefined) {
                    reply.message.text = '<img class="media" src="' + reply.message.image + '">' + reply.message.text;
                }
                if (reply.message.tree && reply.message.tree != "false" && reply.message.tree.attachment != undefined && reply.message.tree.attachment != false) {
                    let ext = reply.message.tree.attachment.split('.').pop();
                    let attachmentType = getMediaType(ext);
                    if (ext == "image") {
                        reply.message.text = '<img class="media" src="' + reply.message.tree.attachment + '">' + reply.message.text;
                    } else {
                        let attachment = { "attachmentType": ext, "attachmentLink": reply.message.tree.attachment }
                        Yubo.createFile(attachment);
                    }
                }
                Yubo.createParagraph(reply.message.text);
                if (reply.message.tree && reply.message.tree != "false") {
                    if (reply.message.tree.hasOwnProperty("products")) {
                        Yubo.createProducts(
                            reply.message.tree.products,
                            reply.message.client_id,
                            reply.message.intent
                        );
                    }
                    if (reply.message.tree.hasOwnProperty("videos")) {
                        Yubo.createVideos(
                            reply.message.tree.videos,
                            reply.message.client_id
                        );
                    }
                    if (reply.message.tree.hasOwnProperty("followup")) {
                        if (reply.message.tree.followup != "false") {
                            let delay = parseInt(reply.message.tree.followup.delayInMs);
                            let nodeName = reply.message.tree.followup.nodeName;
                            if (delay > 0 && nodeName != "" && nodeName != "Select Node") {
                                setTimeout(function() {
                                    Yubo.followup(clientId, nodeName);
                                }, delay);
                            }
                        }
                    }
                }
                if (reply.message.replies.length > 0) {

                    Yubo.createOptions(
                        reply.message.replies,
                        reply.message.client_id,
                        reply.message
                    );
                }
                if (reply.message.tree && reply.message.tree.form) {
                    Yubo.form(
                        reply.message.client_id,
                        reply.message.tree.form,
                        reply.message.tree.webhook,
                        reply.message.tree.default,
                        reply.message.tree.node_name,
                        reply
                    );
                }
            }, 500);
            if (reply.message.type_option || reply.message.type_option == "true") {
                typingBox.on();
            } else {
                typingBox.off();
            }
            if (reply.message.typeNumber) {
                let input = document.getElementById("msg");
                input.setAttribute("type", "number");
            } else {
                let input = document.getElementById("msg");
                input.setAttribute("type", "text");
            }
        });
    },
    resetContext: async function(clientId, userId) {
        // reset context code here
        console.log("context reset")
        default_node = 'False'
        typingBox.on();
    },
    chatHistory: function(clearChat = false) {
        var clientId = document.getElementById("clientId").value,
            userId = (Yubo.getCookie(clientId) == null) ? Yubo.setCookie(clientId) : Yubo.getCookie(clientId);
        this.request("/yuboBot/chatHistory", {
            userId: userId,
            clientId: clientId,
            clearChat: clearChat
        }).then(async function(chats) {
            if (chats.message) {
                firstMsg = false;
                for (chat of chats.message.userChat) {
                    if (!chat.hideChathistory) {
                        if (chat.messageType == "outgoing") {
                            if (chat.image != undefined) {
                                chat.text = '<img class="media" src="' + chat.image + '">' + chat.text;
                            }
                            if (chat.attachment) {
                                Yubo.createFile(chat);
                            } else {
                                Yubo.createParagraph(chat.text, true);
                            }
                            chat.form ? Yubo.form(
                                clientId,
                                chat.form,
                                chat.webhook ? chat.webhook : "",
                                chat.default,
                                // chat.nodeId,
                                chat.node,
                                chat.formAlreadyFilled ? chat.formAlreadyFilled : false
                            ) : "";
                            if (chat.products && chat.products.length > 0) {
                                Yubo.createProducts(
                                    chat.products,
                                    document.getElementById("clientId").value,
                                    chat.intent);
                            }
                            if (chat.replies && chat.replies.length > 0) {
                                let treeData = {
                                    tree: {
                                        node_id: chat.node ? chat.node : false
                                    }
                                }
                                Yubo.createOptions(
                                    chat.replies,
                                    document.getElementById("clientId").value,
                                    treeData);
                            }
                        } else {
                            if (chat.attachment) {
                                Yubo.createFileForHistory(chat);
                            } else {
                                Yubo.createParagraphForHistory(chat.text);
                            }
                        }
                        default_node = (chat.default) ? chat.default : "False";
                        if (chat.type_option || chat.type_option == "true") {
                            typingBox.on();
                        } else {
                            typingBox.off();
                        }
                    }
                }
                Yubo.setSession(userId, chats.message.sessionData);
            } else {
                // await ipLookUp();
                firstMsg = true;
                await startNodeFunc(chats, chats.formData, clientId);
            }
        });
    },
    launch: function(clientId) {
        try {
            if (!localStorage.getItem(clientId)) {
                Yubo.setCookie(clientId);
            }
        } catch (e) {}

        loader.start();
        this.request("/yuboBot/launch", {
            userId: Yubo.getCookie(clientId),
            session: session,
            clientId: clientId,
            node: "launch",
            text: "",
            category: document.getElementById("client_type").value,
            train: "False",
            unsubsribe: "False",
        }).then(function(reply) {
            if (reply.message) {
                default_node = reply.message.default_opt;
            } else {
                default_node = 'False';
            }
            if (reply.chatControl == 1) {
                return;
            }
            if (reply.message.replies.length > 0) {
                setTimeout(function() {
                    Yubo.createParagraph(reply.message.text);
                    Yubo.createOptions(
                        reply.message.replies,
                        reply.message.client_id,
                        reply.message
                    );
                }, 500);
            } else {
                setTimeout(function() {
                    Yubo.createParagraph(reply.message.text);
                }, 500);
            }
            if (reply.message.type_option || reply.message.type_option == "true") {
                typingBox.on();
            } else {
                typingBox.off();
            }
            if (reply.message.typeNumber) {
                let input = document.getElementById("msg");
                input.setAttribute("type", "number");
            } else {
                let input = document.getElementById("msg");
                input.setAttribute("type", "text");
            }
        });
    },
    createParagraph: async function(text, history) {
        if (!text) {
            loader.stop();
            typingBox.on();
            return;
        }
        text=text.replace(new RegExp("\n", "g"),'<br/>');
        loader.stop();
        let node = document.createElement("LI");
        let para = document.createElement("P");
        let chatLogo = document.getElementById("chatLogo").value;
        if (chatLogo != "") {
            let img = document.createElement("img");
            img.src = chatLogo;
            node.appendChild(img);
        }
        if (text.name || text.email || text.phone) {
            para.innerHTML =
                "Thanks for sharing the details.";
        } else {
            para.innerHTML = URLify(text)
        }

        node.classList.add("sent");
        // node.appendChild(img);
        node.appendChild(para);

        document.getElementById("myList").appendChild(node);
        scrollDown();
        if (document.getElementById("incoming_sound").value == "True") {
            playSound(history);
        }
        // $(".messages").animate({ scrollTop: $(document).height() }, "fast");
    },
    createSpan: async function(text) {
        loader.stop();
        let node = document.createElement("LI");
        let span = document.createElement("span");
        // let chatLogo = document.getElementById("chatLogo").value;
        // if (chatLogo != "") {
        // let img = document.createElement("img");
        // img.src = chatLogo;
        // node.appendChild(img);
        // }
        span.innerHTML = text.substring(0, 48)

        node.classList.add("control");
        // node.appendChild(img);
        node.appendChild(span);
        document.getElementById("myList").appendChild(node);
        scrollDown();
        // if(document.getElementById("incoming_sound").value=="True"){
        // playSound(history);
        // }
        // $(".messages").animate({ scrollTop: $(document).height() }, "fast");
    },
    createFile: async function(data) {
        loader.stop();
        let node = document.createElement("LI");
        if (data.attachmentType == 'png' || data.attachmentType == 'jpg' || data.attachmentType == 'jpeg') {
            const img = document.createElement("img");
            let a = document.createElement("a");
            img.src = data.attachmentLink;
            img.style.width = '50%';
            img.style.borderRadius = '0px;';
            img.style.float = 'left';
            a.href = data.attachmentLink;
            a.target = "_blank";
            a.appendChild(img);
            node.appendChild(a);
        } else if (data.attachmentType == 'pdf') {
            let a = document.createElement("a");
            let img = document.createElement("img");
            img.src = "/dist/img/pdf_download_icon.png";
            img.style.width = '30%';
            img.style.float = 'left';
            a.href = data.attachmentLink;
            // a.innerHTML = "This is a File";
            a.target = "_blank";
            a.appendChild(img);
            node.appendChild(a);
        } else if (data.attachmentType == 'msword' || data.attachmentType == 'doc' || data.attachmentType == 'docx') {
            let a = document.createElement("a");
            let img = document.createElement("img");
            img.src = "/dist/img/word_icon.png";
            img.style.width = '30%';
            img.style.float = 'left';
            a.href = data.attachmentLink;
            // a.innerHTML = "This is a File";
            a.target = "_blank";
            a.appendChild(img);
            node.appendChild(a);
        } else {
            let a = document.createElement("a");
            let img = document.createElement("img");
            img.src = "/dist/img/document.png";
            img.style.width = '30%';
            img.style.float = 'left';
            a.href = data.attachmentLink;
            // a.innerHTML = "This is a File";
            a.target = "_blank";
            a.appendChild(img);
            node.appendChild(a);
        }
        document.getElementById("myList").appendChild(node);
        scrollDown();
    },
    createFileForHistory: async function(data) {
        loader.stop();
        let node = document.createElement("LI");
        if (data.attachmentType == 'png' || data.attachmentType == 'jpg' || data.attachmentType == 'jpeg') {
            let a = document.createElement("a");
            const img = document.createElement("img");
            img.src = data.attachmentLink;
            img.style.width = '50%';
            img.style.borderRadius = '0px;';
            img.style.float = 'right';
            a.href = data.attachmentLink;
            a.target = "_blank";
            a.appendChild(img);
            node.appendChild(a);
        } else if (data.attachmentType == 'pdf') {
            let a = document.createElement("a");
            let img = document.createElement("img");
            img.src = "/dist/img/pdf_download_icon.png";
            img.style.width = '30%';
            img.style.float = 'right';
            a.href = data.attachmentLink;
            // a.innerHTML = "This is a File";
            a.target = "_blank";
            a.appendChild(img);
            node.appendChild(a);
        } else if (data.attachmentType == 'msword') {
            let a = document.createElement("a");
            let img = document.createElement("img");
            img.style.width = '30%';
            img.src = "/dist/img/word_icon.png";
            img.style.float = 'right';
            a.href = data.attachmentLink;
            // a.innerHTML = "This is a File";
            a.target = "_blank";
            a.appendChild(img);
            node.appendChild(a);
        }
        document.getElementById("myList").appendChild(node);
        scrollDown();
    },
    form: async function(
        clientId,
        form_data,
        webhook,
        nodee = "False",
        nodeid,
        nodeData
    ) {
        loader.stop();
        // checking for localStorage userId
        try {
            if (!localStorage.getItem(clientId)) {
                Yubo.setCookie(clientId);
            }
        } catch (e) {}

        // creating form elemnet and appending in div mentioned in chat.hbs
        var node = document.createElement("LI");
        node.classList.add("fb-rendered-form");

        var randomString = Math.random().toString(36).slice(2);

        var div = document.createElement("DIV");
        div.setAttribute("id", "fb-rendered-form");

        var formId = "'" + randomString + "'";
        var form = document.createElement("form");
        form.setAttribute("id", formId);
        div.innerHTML = "";
        div.appendChild(form);
        $("form", div).formRender({
            formData: form_data,
        });
        node.appendChild(div);
        document.getElementById("myList").appendChild(node);

        scrollDown(true);

        // To check form filled earlier or not by the user;
        if (nodeData.formAlreadyFilled) {
            setTimeout(
                Yubo.cnfrmtnDialog(
                    nodeData.message.client_id,
                    nodeData.message.tree.followup.nodeName,
                    nodeData.message.formValues
                ),
                3000
            );
        }

        formId = document.getElementById(formId);

        formId.addEventListener("submit", async function(e) {
            $(this).find(':input[type=submit]').prop('disabled', true);
            let formdata = formId.elements;

            try {
                formdata = $(e.target).serializeArray();
            } catch (err) {
                console.log(err);
            }

            hideModal();
            e.preventDefault();
            //To check the first msg of user
            if (firstMsg) {
                locationobj = await ipLookUp();
                firstMsg = false;
            }
            let obj = {},
                formobj = {},
                dataob = {};

            for (i = 0; i < formdata.length; i++) {
                if (formdata[i].name == "submit") {
                    continue;
                } else {
                    // obj[formId.elements[i].name] = formId.elements[i].value ? formId.elements[i].value : "";
                    formdata[i].value ?
                        (obj[formdata[i].name] = formdata[i].value) :
                        "";
                }
            }
            dataob[nodeid] = obj;
            Object.assign(formobj, dataob);
            document.getElementById("hutk").value ?
                (obj["hutk"] = document.getElementById("hutk").value) :
                "";
            $.ajax({
                type: "POST",
                url: "/yuboBot/formSubmit",
                timeout: 10000,
                data: {
                    formData: obj,
                    webhook: webhook,
                    clientId: document.getElementById("clientId").value,
                    userId: Yubo.getCookie(document.getElementById("clientId").value),
                    location: locationobj,
                    formobj: formobj,
                },
                success: async function(resp) {
                    firstMsg = false;
                    if (resp.success) {
                        obj["userId"] = Yubo.getCookie(document.getElementById("clientId").value)
                            // resp.firstMsg ? await ipLookUp() : "";
                        let webhookResp = resp.data ? await Yubo.callToWebhook(resp.data, obj) : "";
                        webhookResp ? await Yubo.createParagraph(webhookResp) : await Yubo.createParagraph(resp.message);

                        div.parentNode.previousSibling.remove();
                        div.parentNode.remove();
                        typingBox.on();

                        if (nodee != "False" && nodee) {
                            Yubo.followup(clientId, nodee);
                        }
                        Yubo.setSession(Yubo.getCookie(document.getElementById("clientId").value), resp.session)
                        socket.emit("message", {
                            userId: Yubo.getCookie(document.getElementById("clientId").value),
                            message: JSON.stringify(obj),
                            name: getnameEmail() ? getnameEmail() : "Unknown",
                            messagetype: 'usermessage',
                            clientId: document.getElementById("clientId").value
                        });
                    } else {
                        await Yubo.createParagraph(
                            "Something went wrong, please try again later"
                        );
                        typingBox.on();
                        if (nodee != "False" && nodee) {
                            Yubo.followup(clientId, nodee);
                        }
                    }
                },
                error: async function(jqXHR, textStatus, err) {
                    var errorDiv = document.getElementById("error");
                    errorDiv.innerHTML = "Something went wrong, please try again later";
                    errorDiv.style.display = "block";
                    await setTimeout(function() {
                        errorDiv.style.display = "none";
                    }, 2000);
                },
            });
        });
    },
    createParagraphForHistory: function(text) {
        if (!text) {
            loader.stop();
            typingBox.on();
            return;
        }
        loader.stop();
        let node = document.createElement("LI");
        let para = document.createElement("P");
        // let img = document.createElement("img");
        // img.src=document.getElementById('chatLogo').value;
        // if(img.src!=""){
        //     node.appendChild(img);
        // }
        para.innerHTML = text;
        node.classList.add("replies");
        // node.appendChild(img);
        node.appendChild(para);
        document.getElementById("myList").appendChild(node);
        scrollDown();
        // $(".messages").animate({ scrollTop: $(document).height() }, "fast");
    },
    createOptions: function(options, clientId, replyData) {
        loader.stop();
        var hubProps;
        if (replyData && replyData.tree) {
            if (replyData.tree.hasOwnProperty("crm")) {
                if (
                    replyData.tree.crm.checkbox ||
                    replyData.tree.crm.checkbox == true
                ) {
                    hubProps = replyData.tree.crm.property;
                }
            }
        }
        var randomString = Math.random().toString(36).slice(2);
        var optionId = "'" + randomString + "'";
        if (clientId !== 'nirogam_chatbot') {
            var node = document.createElement("LI");
        } else {
            var node = document.createElement("DIV");
        }
        node.classList.add("optlist");
        node.setAttribute("id", randomString);
        var i = document.getElementById("bottonId").value;

        for (option of options) {
            let button = document.createElement("BUTTON");
            button.setAttribute("id", i);
            button.classList.add("quickReplies");
            if (hubProps && hubProps != "") {
                button.setAttribute("hubprop", hubProps);
            }
            let link = option.link ? option.link : "False",
                buttonText = option.option,
                nodeScore = option.score ? option.score : false;
            let payload = option.payload ? option.payload : false;
            option.score ? (option.score.nodeId = replyData ? (replyData.tree ? replyData.tree.node_id : (replyData ? replyData.intent : "")) : "") : "";
            button.setAttribute(
                "onclick",
                "Yubo.send(this.id, " +
                '"' +
                clientId +
                '"' +
                ", true," +
                optionId +
                ",  " +
                '"' +
                link +
                '",' +
                '"' +
                buttonText +
                '",' +
                JSON.stringify(nodeScore) +
                ',' + payload +
                ', $(this).attr("hubprop"))'
            );
            button.innerText = option.option;
            node.appendChild(button);
            document.getElementById("bottonId").value = parseInt(i) + 1;
            i++;
            // $(".messages").animate({ scrollTop: $(document).height() }, "fast");
        }
        if (clientId !== 'nirogam_chatbot') {
            document.getElementById("myList").appendChild(node);
        } else {
            document.querySelectorAll(".sent:last-child")[0].appendChild(node);
        }
        scrollDown();
    },
    createProducts: function(products, clientId, intent = '') {
        if (products.length > 0) {
            loader.stop();
            var randomString = Math.random().toString(36).slice(2);
            var optionId = "'" + randomString + "'";

            slider_count++;
            var node = document.createElement("LI");
            node.setAttribute("id", randomString);

            let ans = "";
            ans += '<div class="products yubo_products_' + intent + '">';
            ans +=
                '<div class="splide" id="slider_' +
                slider_count +
                '"><div class="splide__track"><ul class="splide__list">';
            products.forEach(function(value, index, array) {
                let title = value.title;
                let price = value.price;
                let prod_img = value.image;
                let info_link = value.info_link;
                let cart_link = value.cart_link;
                let payload = (value.payload) ? value.payload : false;
                let prod_desc = value.description.replace(/(\r\n|\n|\r)/gm, "");
                ans += '<li class="splide__slide">';
                ans += '<div class="prod_card yubo_prod_card_' + intent + '">';
                ans += '<div class="prod_img"><img src="' + prod_img + '" loading="lazy" onerror="this.onerror=null;this.src=\'dist/img/image-placeholder-icon.png\';" ></div>';
                ans += '<div class="prod_info">';
                ans += '<div class="title_price">';
                ans += '<h4 class="text-center title">' + title + '</h4>';
                ans += '<h5 class="text-center price">' + price + '</h5>';
                ans += '</div>';
                ans += '<div class="prod_desc"><h5 class="text-center desc">' + prod_desc + '</h5></div>';
                ans += '</div>';
                ans += '<div class="cart_icon"><div class="cart_icon_hover"><i class="fa fa-shopping-cart" title="' + title + '" onclick="Yubo.send(\'You have selected ' + title + '\', \'' + clientId + '\', false,\'' + randomString + '\', \'' + cart_link + '\', false, false,\'' + payload + '\', $(this).attr(\'hubprop\'))"></i><a class="yubo_info_link" target="_blank" href="' + info_link + '"><i class="fa fa-info" title="View Details"></i></a></div></div></div>';
                ans += "</li>";
            });
            ans += "</ul></div></div>";
            ans += "</div>";
            node.innerHTML = ans;
            document.getElementById("myList").appendChild(node);
            scrollDown();
            let obj = $(".splide");
            if (obj.length > 0) {
                start_slider("#slider_" + slider_count);
            }
        }
    },
    createVideos: function(videos, clientId) {
        loader.stop();
        var randomString = Math.random().toString(36).slice(2);
        var optionId = "'" + randomString + "'";

        var node = document.createElement("LI");
        node.setAttribute("id", randomString);
        let ans = "";
        videos.forEach(function(value, index, array) {
            let title = value.title;
            let description = value.description;
            let url = value.url;
            ans +=
                '<iframe src="' +
                url +
                '?controls=0&frameborder=0&picture-in-picture=1"></iframe>';
        });
        node.innerHTML = ans;
        document.getElementById("myList").appendChild(node);
        scrollDown();
    },
    setCookie: function(cname, cval = cvalue) {
        try {
            localStorage.setItem(cname, cval);
            return cval;
        } catch (e) {}
        // var d = new Date();
        // d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        // var expires = "expires=" + d.toUTCString();
        // document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    setSession: function(uid, sess = session) {
        try {
            if (sess == null || sess == undefined) {
                sess = session
            }
            if (storageData != '') {
                sess={...sess, ...{"storageData":storageData}}
            }
            localStorage.setItem(uid, JSON.stringify(sess));
            return sess;
        } catch (e) {
            session = sess;
            return session;
        }
    },
    getSession: function(uid) {
        try {
            return (JSON.parse(localStorage.getItem(uid)) == null) ? session : JSON.parse(localStorage.getItem(uid));
        } catch (e) {
            return session;
        }
    },
    getCookie: function(cname) {
        try {
            return localStorage.getItem(cname);
        } catch (e) {
            return cvalue;
        }

        // var name = cname + "=";
        // var decodedCookie = decodeURIComponent(document.cookie);
        // var ca = decodedCookie.split(';');
        // for (var i = 0; i < ca.length; i++) {
        //   var c = ca[i];
        //   while (c.charAt(0) == ' ') {
        //     c = c.substring(1);
        //   }
        //   if (c.indexOf(name) == 0) {
        //     return c.substring(name.length, c.length);
        //   }
        // }
        // return "";
    },
    getCookieByName: function(name) {
        let match = document.cookie.match(
            RegExp("(?:^|;\\s*)" + name + "=([^;]*)")
        );
        return match ? match[1] : null;
    },
    setLocalStorage: function(key, value) {
        try {
            localStorage.setItem(key, value);
            return value;
        } catch (e) {
            return ""
        }
    },
    getLocalStorage: function(key) {
        try {
            let value = localStorage.getItem(key);
            return value;
        } catch (e) {}
    },
    cnfrmtnDialog: async function(clientId, nodeName, formValues) {
        nodeName =
            nodeName == "" || nodeName.toLowerCase() == "select node" ? "" : nodeName;
        formValues = formValues ? formValues : "";
        let modal = "",
            parentDiv = document.createElement("div");
        parentDiv.setAttribute("id", "mi-modal");
        parentDiv.setAttribute("class", "popUp mi-modal");
        modal +=
            `<div class="popUp-body">
<div class="popUp-body-In">
<div id="popUp-body-Inner" class="popUp-body-Inner">
<p>We already have your information with us. Do you want to update it?</p>` +
            /* `<p>` + formValues +  `</p>` */
            `<button type="button"  onclick="hideModal()" id="modal-btn-si">Yes</button>
<button type="button" onclick="showModal(` +
            `'` +
            clientId +
            `'` +
            `,'` +
            nodeName +
            `')" id="modal-btn-no">No</button>
</div>
</div>
</div>`;
        parentDiv.innerHTML = modal;
        document.getElementById("down").appendChild(parentDiv);
        return;
    },
    // Passing form data to the integrated webhook
    callToWebhook: async function(webhookDetails, formData) {
        const url = webhookDetails.webhookUrl;
        const token = webhookDetails.webhookApiSecretKey;
        const apiKey = webhookDetails.webhookApiKey;
        const method = webhookDetails.webhookRequestMethod;
        var data = webhookDetails.webhookParameters;
        var accessKey = token ? "Bearer " + token : apiKey;
        let resp = false;

        for (var formKey of Object.keys(formData)) {
            data = JSON.stringify(JSON.parse(data), function(key, value) {
                // resp = formKey == "hutk" ? formData[formKey] : false;
                if (value == "[" + formKey + "]") {
                    return formData[formKey];
                } else if (value == "[uniqueId]") {
                    return Date.now();
                } else {
                    return value;
                }
            });
        }

        try {
            await $.ajax({
                    method: method,
                    url: url,
                    data: data,
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: accessKey,
                    },
                })
                .then(async function(response) {
                    resp = (response && response.inlineMessage) ? response.inlineMessage : false;
                })
                .catch(async function(err) {});
        } catch (err) {}
        return resp;
    },

    //Updating hubspot properties
    updateHubProp: async function(properties) {
        try {
            await $.ajax({
                    method: "POST",
                    url: "https://api.hsforms.com/submissions/v3/integration/submit/4741732/ffdbf28c-4266-4474-b596-f8268323a6b2",
                    data: JSON.stringify(properties.data),
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: properties.apiKey,
                    },
                })
                .then(async function(response) {})
                .catch(async function(err) {});
        } catch (err) {}
        return
    },
    clearChatHistory: async function() {
        $('#myList li:not(:first-child)').remove();
        await Yubo.chatHistory(true);
    },
    setStory: function(set) {
        let url = document.getElementById("url").value;
        console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ story $$$$$$$$$$$$$$$$$$$$$$$$",set)
        $.ajax({
            type: "POST",
            url: "/yuboBot/setStory",
            timeout: 10000,
            data: {
                clientId: document.getElementById("clientId").value,
                userId: Yubo.getCookie(document.getElementById("clientId").value),
                url: url,
                story:set
            },
            success: async function(resp) {
                if (resp.status=='success') {
                    return true;
                } else {
                    return false;
                }
            },
            error: async function(jqXHR, textStatus, err) {
                return false;
            },
        });
    },
    nodeExists: async function(node) {
        try{
            let resp=await $.ajax({
                type: "POST",
                url: "/yuboBot/nodeExists",
                timeout: 10000,
                data: {
                    clientId: document.getElementById("clientId").value,
                    node: node
                }
            });
            if(resp){
                if(resp.status=="success"){
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }catch(err){
            return false
        }
    }
};

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}

function userIdGenerator() {
    var d = new Date().getTime();
    var d2 = (performance && performance.now && (performance.now() * 1000)) || 0; //Time in microseconds since page-load or 0 if unsupported
    var userId = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;
        if (d > 0) {
            var r = (d + r) % 16 | 0;
            d = Math.floor(d / 16);
        } else {
            var r = (d2 + r) % 16 | 0;
            d2 = Math.floor(d2 / 16);
        }
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    return userId;
};


document.addEventListener("DOMContentLoaded", function() {
    //var socket = io.connect({ transports: ['websocket'] });

    socket.on("appChatMessage", (message) => {
        if (
            Yubo.getCookie(document.getElementById("clientId").value) ==
            message.chatMessage.userId
        ) {
            Yubo.createParagraph(message.chatMessage.text);
        }
    });
    socket.emit("login", {
        userId: Yubo.getCookie(document.getElementById("clientId").value),
    });
    var timeout = null;

    var typing = 0;
    document.addEventListener("keydown", function(e) {
        let date = new Date();
        if (e.which == 13) {
            if (document.getElementById("msg").value == "") {
                return;
            }
            typing = 0;
            Yubo.send(
                document.getElementById("msg").value,
                document.getElementById("clientId").value
            );
            socket.emit("typing", {
                typing: 0,
                userId: Yubo.getCookie(document.getElementById("clientId").value),
                date: date,
            });
            return false;
        } else {
            if (typing == 0) {
                typing = 1;
                socket.emit("typing", {
                    typing: 1,
                    userId: Yubo.getCookie(document.getElementById("clientId").value),
                    date: date,
                });
            }
        }
    });
    // Init a timeout variable to be used below
    document.addEventListener("keyup", function(e) {
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            let date = new Date();
            typing = 0;
            socket.emit("typing", {
                typing: 0,
                userId: Yubo.getCookie(document.getElementById("clientId").value),
                date: date,
            });
        }, 3000);
    });
    socket.on("display", (data) => {
        if (data.typing == "true") {
            clearTimeout(timeout);

            // Make a new timeout set to go off in 1000ms (1 second)
            timeout = setTimeout(function() {}, 2000);
            // document.getElementById('msg').value='client is typing ...';
            setTimeout(function() {
                // document.getElementById('msg').value="";
            }, 5000);
        }
    });
});

//Yubo.chatHistory();
validateUser();
var locationobj = {};
async function ipLookUp() {
    await $.ajax("https://ipapi.co/json").then(
        function success(response) {
            if (response.city) {
                locationobj = {
                    city: response.city,
                    state: response.region,
                    longitude: response.longitude,
                    latitude: response.latitude,
                    ip: response.ip,
                    usertimezone: response.timezone,
                    callingcode: response.country_calling_code,
                    currency: response.currency,
                };
            } else {
                locationobj = "";
            }
        },
        function fail(data, status) {
            console.log("Request failed. Returned status of", status);
        }
    );
    return locationobj;
}

function hideModal() {
    $(".mi-modal").fadeOut("slow");
}

function showModal(clientId, nodeName) {
    Yubo.followup(clientId, nodeName);
    hideModal();
}

function URLify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(^|[^\"\'])(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '$1<a href="$2" target="_blank">$2</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/\'\"])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a target="_blank" href="mailto:$1">$1</a>');

    return replacedText;
}

function playSound(history = false) {
    if (!history) {
        let audio = new Audio('tones/bell.ogg');
        audio.play();
        audio = undefined
    }
}

function getnameEmail() {
    var name = "";
    try {
        let user_id = localStorage.getItem(document.getElementById("clientId").value);
        let sessionobj = JSON.parse(localStorage.getItem(user_id));
        name = sessionobj ? (sessionobj.name ? sessionobj.name : "") : "";
    } catch (err) {
        console.log(err);
        name = session ? (session.name ? session.name : "") : "";
    }
    return name;
}
async function startNodeFunc(nodeData, tree, clientId) {
    if (tree != undefined && tree.node_name == "start") {
        Yubo.createParagraph(tree.text);

        if (tree.default) {
            default_node = tree.default;
        }

        if (tree.options.length > 0) {
            setTimeout(function() {
                Yubo.createOptions(
                    tree.options,
                    clientId,
                    tree
                );
            }, 500);
        }
        tree.form ?
            Yubo.form(
                clientId,
                tree.form,
                tree.webhook,
                tree.default,
                // tree.nodeId,
                tree.node_name,
                nodeData
            ) :
            "";
        if (tree.hasOwnProperty("followup")) {
            if (tree.followup != "false") {
                let delay = parseInt(tree.followup.delayInMs);
                let nodeName = tree.followup.nodeName;
                if (delay > 0 && nodeName != "" && nodeName != "Select Node") {
                    setTimeout(function() {
                        Yubo.followup(clientId, nodeName);
                    }, delay);
                }
            }
        }
        // console.log("producr", tree)
        if (tree.hasOwnProperty("products") && tree.type_of_node == "product") {
            Yubo.createProducts(
                tree.products,
                clientId,
                tree.node_id
            );
        }
        if (tree.type_opt || tree.type_opt == "true") {
            typingBox.on();
        } else {
            typingBox.off();
        }
    }
    return
}

function onTypingUser() {
    let user_id = Yubo.getCookie(document.getElementById("clientId").value);
    socket.emit('userTyping', { clientId: document.getElementById("clientId").value, userId: user_id });
}
async function onFileSelect(e) {
    const file = e.target.files[0];
    filesize = file.size / 1024;
    if (filesize < 500) {
        let base64Image = await toBase64(file);
        let name = getnameEmail();
        let fileType = file.type.split('/');
    }
}
const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});

function getAccessToken() {
    var data = {
        _id: cvalue,
        clientId: document.getElementById("clientId").value
    }
    $.ajax({
        data: data,
        async: false,
        cache: false,
        method: 'POST',
        url: '/agent/generateToken',
        success: async function(data) {
            if (data.statusCode == 200) {
                await Yubo.setLocalStorage('accessToken', data.accessToken);
            } else {
                console.log('Error occured ', data.message);
            }
        }
    });
}
getAccessToken();

var socket = io.connect("https://helloyubo.com:4900", {
    transports: ["websocket"],
    secure: true,
    reconnection: true,
    reconnectionDelay: 500,
    reconnectionAttempts: Infinity
});


socket.on("connect", () => {
    console.log("socket.connected ",socket.connected); 
    let user_id = Yubo.getCookie(document.getElementById("clientId").value);
    socket.emit('join', user_id);
});

socket.io.on("reconnect", (attempt) => {
  console.log("socket reconnect attempt ",attempt); 
});

socket.on("disconnect", () => {
    console.log("socket disconnected"); 
});


// $(document).ready(function() {

// });

socket.on('agentTyping', data => {
    loader.start();
    scrollDown();
});

socket.on('typingStopped', data => {
    loader.stop();
    scrollDown();
});

socket.on('send_message', (data) => {
    if (data.clientId == document.getElementById("clientId").value) {
        if (data.attachment) {
            Yubo.createFile(data)
        } else {
            if (data.control) {
                typingBox.on();
                Yubo.createSpan(data.message)
            } else {
                Yubo.createParagraph(data.message);
            }
        }
    }
})
socket.on('timeOut', (data) => {
    console.log('timeOut event data :', data)
    if (data.resetContext != false && data.resetContext != 'false') {
        console.log('resetContext true ')
        Yubo.resetContext(data.clientId, data.userId);
    }
    if (data.nodeName != false && data.nodeName != 'false') {
        console.log('nodeName = nodename or true  :')
        Yubo.followup(data.clientId, data.nodeName);
    }
})

socket.on('notify', (data) => {
    console.log('*********notification event data********* :', data)
    let payload=false
    if(data.payload){
        payload=data.payload;
    }
    if (data.resetContext) {
        console.log('resetContext true ')
        Yubo.resetContext(data.clientId, data.userId);
    }
    if (data.nodeName) {
        Yubo.followup(data.clientId, data.nodeName, payload);
    } else if(data.message){
        Yubo.createParagraph(data.message);
    }
    return;
})
socket.on('yubo_new_user_start', (data) => {
    Yubo.followup(data.clientId, data.nodeName);
})

function validateFileType(mimetype) {
    let matched = false;
    switch (mimetype) {
        case "image/jpeg":
            matched = true;
            break;
        case "image/jpg":
            matched = true;
            break;
        case "image/gif":
            matched = true;
            break;
        case "image/webp":
            matched = true;
            break;
        case "image/webp":
            matched = true;
            break;
        case "image/svg+xml":
            matched = true;
            break;
        case "image/png":
            matched = true;
            break;
        case "application/pdf":
            matched = true;
            break;
        case "text/plain":
            matched = true;
            break;
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
            matched = true;
            break;
        case "application/vnd.ms-excel":
            matched = true;
            break;
        case "application/msword":
            matched = true;
            break;
        case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
            matched = true;
            break;
        case "application/vnd.ms-powerpoint":
            matched = true;
            break;
    }
    return matched;
}
// var socket = io.connect("https://helloyubo.com:4900", {
//   transports: ["websocket"],
//   secure: true,
//   query: {
//     token: Yubo.getLocalStorage('accessToken')
//   }
// });


// $(document).ready(function () {
//   let user_id = localStorage.getItem(document.getElementById("clientId").value);
//   socket.emit('join', user_id);
// });

// socket.on('agentTyping', data => {
// });

// socket.on('send_message', (data) => {
//   if (data.clientId == document.getElementById("clientId").value) {
//     if (data.base64File) {
//       Yubo.createFile(data)
//     } else {
//       if (data.control) {
//         typingBox.on();
//         Yubo.createSpan(data.message)
//       } else {
//         Yubo.createParagraph(data.message);
//       }
//     }
//   }
// })

const loader = {
    start: function() {
        document.getElementById("wave").style.display = "block";
    },
    stop: function() {
        document.getElementById("wave").style.display = "none";
    }
}

const typingBox = {
    on: function() {
        let input = document.getElementById("message-input");
        input.setAttribute("style", "display:block;");
        $(".messages").removeClass('notype');
    },
    off: function() {
        let input = document.getElementById("message-input");
        input.setAttribute("style", "display:none;");
        $(".messages").addClass('notype');
    }
}

function getMediaType(ext) {
    switch (ext.toLowerCase()) {
        case "gif":
        case "png":
        case "jpg":
        case "jpeg":
            return "IMAGE";
            break;
        case "pdf":
        case "doc":
        case "txt":
        case "ppt":
        case "pptx":
        case "docx":
        case "xls":
        case "xlsx":
            return "DOCUMENT";
            break;
        case "amr":
        case "mp3":
        case "aac":
        case "wav":
            return "AUDIO";
            break;
        case "mp4":
        case "mpeg":
        case "avi":
        case "ogv":
        case "ts":
        case "3gp":
        case "3g2":
        case "webm":
            return "VIDEO";
            break;
        default:
            return "IMAGE";
    }
}

function scrollDown(form = false) {
    let myConsole = document.getElementById('down');
    if (form) {
        window.scrollBy(0, 200);
        myConsole.scrollTop = myConsole.scrollHeight - 500;
    } else {
        window.scrollTo(0, document.body.scrollHeight);
        myConsole.scrollTop = myConsole.scrollHeight;
    }
}
scrollDown();

function validateUser() {
    let userId=Yubo.getCookie(document.getElementById("clientId").value);
    console.log("old user id",userId)
    let sessionStorageData=Yubo.getSession(userId)
    console.log(sessionStorageData.storageData!=storageData)
    if(sessionStorageData.storageData!=undefined && storageData!='' && sessionStorageData.storageData!=storageData){
        console.log("user logged out")
        cvalue=userIdGenerator();
        console.log("new user id",cvalue)
        localStorage.removeItem(document.getElementById("clientId").value)
        localStorage.removeItem(userId)
        Yubo.setCookie(document.getElementById("clientId").value)
        Yubo.chatHistory();
    } else {
        Yubo.chatHistory();
    }
}

$(window).on("unload", function(e) {
    let userId = Yubo.getCookie(document.getElementById("clientId").value),
    clientId =  document.getElementById("clientId").value;
    socket.emit('userOffline', { userId : userId, clientId : clientId ,event :'unload11'});
  });
 
  socket.on("disconnect", () => {
    // console.log('disconnet socket id--------------',socket.id); // undefined
    let userId = Yubo.getCookie(document.getElementById("clientId").value),
    clientId =  document.getElementById("clientId").value;
    socket.emit('userOffline', { userId : userId, clientId : clientId ,event :'disconnect'});
  });

$(document).ready(function() {

    socket.on("connect", () => {
        let userId = Yubo.getCookie(document.getElementById("clientId").value),
        clientId =  document.getElementById("clientId").value;
        socket.emit('userOnline', { userId : userId, clientId : clientId });
        // console.log('connect socket id -----------------',socket.id); // x8WIv7-mJelg7on_ALbx
    });

    setInterval(() => {
        let userId = Yubo.getCookie(document.getElementById("clientId").value),
            clientId =  document.getElementById("clientId").value;
        socket.emit('userOnline', { userId : userId, clientId : clientId });
    }, 100);

});