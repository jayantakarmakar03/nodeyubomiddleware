const path = require("path");
const db = require("../models/all-models"),
  jwt = require("jsonwebtoken"),
  axios = require("axios"),
  fs = require("fs");
  XLSX = require("xlsx");
  const util = require('util');

module.exports = {
  trainIntents: trainIntents,
  trainFaqs: trainFaqs,
  saveBasicIntentsInDb: saveBasicIntentsInDb,
  loadIntentsName: loadIntentsName,
  chckDir: chckDir,
  // createEntitiesJson: createEntitiesJson,
  clientJsCssFileCreation: clientJsCssFileCreation, 
  createdirWritefile,
  checkdirexist,
  updatedirWritefile,
  createEntitydata,
  deleteintentdata,
  updatePatterns:updatePatterns,
  insertDataBulkIntent,
  downloadExcel,
  insertFAQ,
  getBulkIntentData,
  editBulkIntentData,
  inActiveStatusBulkShot,
  inActiveAnalyticStatusBulkShot,
  deleteBulkIntent,
  deleteSelected,
  getZeroShotDataById
};

async function trainIntents(clientType, data, clientId) {
try{
  console.log("trainintents", clientType, data, clientId)
  await fs.writeFileSync(
    process.env.pythonDir + clientId + "/intents.json",
    data,
    { encoding: "utf8", flag: "w" },
    (err) => {
      if (err) {
        console.log("trainintents error", err)
        return res.send({ message: "error" });
      }
    }
  );
  let requestData = {
    userId: "c1fcfbca-af31-5885-5779-dc9f0c91dd27",
    client_id: clientId,
    train: "True",
    unsubsribe: "False",
    text: "",
    node: "False",
    session: {
      name: "",
      phone: "",
      email: "",
    },
    category: clientType ? clientType : "basic",
    update_tree: "False",
    update_story: "False",
  };
  await axios({
    method: "POST",
    url: process.env.pythonUrl,
    data: { data: requestData },
  })
    .then(function (response) {
      if (response.data.status == "success") {
        console.log("train", response.data)
        msg = { status: "success", data: response.data };
      } else {
        console.log("error", response.data)
        msg = { status: "error" };
      }
    })
    .catch(function (error) {
      console.log("error", error)
      msg = { status: "error" };
    });
    return msg;
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

async function trainFaqs(clientType, data, clientId) {
try{
  console.log("trainFaqs", clientType, data, clientId)
  await fs.writeFileSync(
    process.env.pythonDir + clientId + "/faqs.json",
    data,
    { encoding: "utf8", flag: "w" },
    (err) => {
      if (err) {
        console.log("trainFaqs error", err)
        return res.send({ message: "error" });
      }
    }
  );
  let requestData = {
    userId: "c1fcfbca-af31-5885-5779-dc9f0c91dd27",
    client_id: clientId,
    train: "False",
    unsubsribe: "False",
    text: "",
    node: "False",
    session: {
      name: "",
      phone: "",
      email: "",
    },
    category: clientType ? clientType : "basic",
    update_tree: "False",
    update_story: "False",
    train_es_faq: "True"
  };
  await axios({
    method: "POST",
    url: process.env.pythonUrl,
    data: { data: requestData },
  })
    .then(function (response) {
      if (response.data.status == "success") {
        console.log("train", response.data)
        msg = { status: "success", data: response.data };
      } else {
        console.log("error", response.data)
        msg = { status: "error" };
      }
    })
    .catch(function (error) {
      console.log("error", error)
      msg = { status: "error" };
    });
    return msg;
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

async function saveBasicIntentsInDb(clientType, clientId) {
try{
  console.log("hgdhs", clientType, clientId)
  await fs.readFile(
    "./public/assets/basicIntents.json",
    async function (err, data) {
      console.log("err", err, data)
      let intent = JSON.parse(data);
      for (int of intent.intents) {
        let savedata = new db.Intent({
          clientId: clientId,
          tag: int.tag,
          patterns: int.patterns,
          responses: int.responses,
          entities: int.entities,
        });
        await savedata.save();
      }
      let feed = await db.Intent.find(
          { clientId: clientId, active_status: { $ne: "off" } },
          { tag: 1, patterns: 1, responses: 1, entities: 1, _id: 0 },
          { updatedAt: 0, createdAt: 0 }
        ),
        obj = {
          intents: feed,
        },
        intentsdata = JSON.stringify(obj, null, 2);
      await trainIntents(clientType, intentsdata, clientId);
    }
  );
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function chckDir(clientId) {
try{
  let dir = process.env.pythonDir + clientId;

  if (!fs.existsSync(dir)) {
    await fs.mkdirSync(dir, { recursive: true });
  }
  return;
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

//To create entities.json file for every client
// async function createEntitiesJson(clientId, data){
// try{
//   await chckDir(clientId)
//   data = JSON.stringify(data)
//   await fs.writeFileSync(
//     process.env.pythonDir + clientId + "/entities.json",
//     data,
//     { encoding: "utf8", flag: "w" },
//     (err) => {
//       if (err) {
//         return res.send({ message: "error" });
//       }
//     }
//   );
// }catch(error){
//   console.log('An error occurs in the catch block:-', error)
// }
// }

function checkdirexist(clientId) {
try{
  let dir = process.env.pythonDir + clientId;
  if (!fs.existsSync(dir)) {
    return Boolean(fs.existsSync(dir));
  } else {
    let directory = process.env.pythonDir + clientId + "/entities.json";
    return fs.existsSync(directory);
  }
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

function createdirWritefile(clientId, data) {
try{
  let dir = process.env.pythonDir + clientId;
  fs.mkdirSync(dir, { recursive: true });
  data = JSON.stringify(data);
  fs.writeFileSync(
    process.env.pythonDir + clientId + "/entities.json",
    data,
    { encoding: "utf8", flag: "w" },
    (err) => {
      if (err) {
        return res.send({ message: "error" });
      }
    }
  );
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function updatedirWritefile(clientId, data) {
try{
  let filedata = await getFileData(`${process.env.pythonDir}${clientId}/entities.json`);
  let fetchedata = JSON.parse(filedata);
  var newData = [],
    entitesjson = {},
    existingIndex;
  for (let i = 0; i < fetchedata.entities.length; i++) {
    const iterator = fetchedata.entities[i];
    if (iterator.tag == data.tag) {
      existingIndex = i;
      break;
    }
  }
  if (existingIndex || existingIndex == 0) {
    if (data.entites.length<1) {
      fetchedata.entities.splice(existingIndex, 1);
      if (fetchedata.entities.length < 1) {
        /*************/ / put the condition for deleting the file/; /***********/
        let path = process.env.pythonDir + clientId + "/entities.json";
        await fs.unlink(path, (err) => {
          if (err) return console.log(err);
        });
      }
    } else {
      fetchedata.entities[existingIndex].entities = data.entites;
      fetchedata.entities[existingIndex].entitytype = data.entitytype;
    }
  } else {
    if (data.entites.length<1) {
      return;
    } else {
      const obj = {
        tag: data.tag,
        entities: data.entites,
        entitytype: data.entitytype,
      };
      newData.push(obj);
    }
  }
  let finalarray = fetchedata["entities"].concat(newData);
  entitesjson = { entities: finalarray };
  return createdirWritefile(clientId, entitesjson);
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

function getFileData(path) {
try{
  return new Promise((resolve, reject) => {
    fs.readFile(path, function (err, data) {
      if (err) {
        return reject(err);
      }
      resolve(data);
    });
  });
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

function createEntitydata(data1) {
try{
  let entitiesarray = [],
    entitesjson = {};
  let entiesdata = {
    tag: data1.tag,
    entities: data1.entites,
    entitytype: "",
  };
  entitiesarray.push(entiesdata);
  entitesjson = { entities: entitiesarray };
  return entitesjson;
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function deleteintentdata(clientId, data) {
try{
  let existingIndex,
    entitesjson = {};
  let filedata = await getFileData(`${process.env.pythonDir}${clientId}/entities.json`);
  let fetchedata = JSON.parse(filedata);
  for (let i = 0; i < fetchedata.entities.length; i++) {
    const iterator = fetchedata.entities[i];
    if (iterator.tag == data) {
      existingIndex = i;
      break;
    }
  }
  if (existingIndex || existingIndex == 0) {
    fetchedata.entities.splice(existingIndex, 1);
  }
  if (fetchedata.entities.length < 1) {
    /*************/ / put the condition for deleting the file/; /***********/
    let data = `${process.env.pythonDir}${clientId}/entities.json`;
    await fs.unlink(data, (err) => {
      if(err) return console.log(err);
     // console.log('file deleted successfully');
    });
  } else {
    entitesjson = { entities: fetchedata.entities };
    return createdirWritefile(clientId, entitesjson);
  }
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function loadIntentsName(req, res) {
try{
  let intents = await db.Intent.find(
    { clientId: req.user.clientId },
    { tag: 1 }
  );
  return res.send({ data: intents });
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function clientJsCssFileCreation(req, res, username){
try{
  var authtoken = await jwt.sign({ clientId: username }, "yubo2020");
  await fs.readFile(
    "./public/assets/master.bot.js",
    "utf8",
    function (err, data) {
      if (err) {
        return;
      }
      var result = data.replace(/#CID#/g, '"' + authtoken + '"');

      fs.writeFile(
        "./public/assets/" + username + ".bot.js",
        result,
        "utf8",
        function (err) {
          if (err) return;
        }
      );
    }
  );

  await fs.copyFile(
    "./public/assets/master.bot.css",
    "./public/assets/" + username + ".bot.css",
    (err) => {
      if (err) throw err;
    }
  );

  await fs.copyFile(
    "./public/dist/img/speech-bubble.png",
    "./public/img/" + username + ".png",
    (err) => {
      if (err) throw err;
    }
  );

  // let client = getClient(req);
  let subsdata = {
    userId: "c1fcfbca-af31-5885-5779-dc9f0c91dd27",
    client_id: username,
    train: "False",
    unsubsribe: "False",
    text: "",
    node: "False",
    session: {
      email: "",
      name: "",
      phone: "",
    },
    category: "basic",
    update_tree: "False",
    update_story: "False",
    basic: "True",
  };
  let resp = await axios({
    method: "post",
    url: process.env.pythonUrl,
    data: { data: subsdata },
  });
  console.log("resp", resp.data , " subsdata ", subsdata)

  /*** For creating intents.json file and cnfig.json file ***/
  await chckDir(username);
  await saveBasicIntentsInDb("basic", username);
  await fs.copyFile(
    "./public/assets/config.json",
    process.env.pythonDir + username + "/config.json",
    (err) => {
      if (err) throw err;
    }
  );
  
  return 
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function updatePatterns(req,res) {
try{
  var url = process.env.intentpattern
  var data = req.body
  var patternData = {
                phrase_array:data.data,
                client_id:req.user.clientId
          }
  await axios({
          method: "POST",
          url: url,
          data: patternData,
        }).then(function (response) {
          if (response.status == 200) {
            msg = { status: "success", data: response.data };
          } else {
            msg = { status: "error" };
          }
        })
        .catch(function (error) {
          console.log("error", error)
          msg = { status: "error" };
        });

        res.json({'msg':msg})
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}



//==================from======here=====I====Have=====Strated===============>


//download excel file====>
const readFile = util.promisify(fs.readFile);

// async function downloadExcel(req, res) {
//   let client = req.user.clientId;
//   const filePath = `${process.env.yubo}/${client}/faqs.json`;

//   try {
//     let data = await readFile(filePath, 'utf-8');
    
//     let finalResult = {};
//     let dataArray = [];

//     if (data) {
//       try {
//         finalResult = JSON.parse(data);
//         dataArray = Object.keys(finalResult).map(key => finalResult[key]);
//       } catch (error) {
//         // Handle invalid JSON error here
//         console.error('Invalid JSON in faqs.json:', error);
//         return res.status(500).send('Invalid JSON in FAQs file');
//       }
//     }

//     if (dataArray.length === 0) {
//       // Download the existing Excel file if available
//       const existingFilePath = `${process.env.yubo}/${client}/faqs.xlsx`;
//       try {
//         const excelFile = fs.readFileSync(existingFilePath);
//         res.setHeader('Content-disposition', 'attachment; filename=faqs.xlsx');
//         res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//         return res.send(excelFile);
//       } catch (error) {
//         console.error('Error reading existing_faqs.xlsx:', error);
//         // Handle the error accordingly
//         return res.status(500).send('Error reading existing FAQs file');
//       }
//     }

//     const wb = XLSX.utils.book_new();
//     const ws = XLSX.utils.json_to_sheet(dataArray);
//     XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
//     const excelFile = XLSX.write(wb, { bookType: 'xlsx', type: 'buffer' });

//     res.setHeader('Content-disposition', 'attachment; filename=faqs.xlsx');
//     res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//     res.send(excelFile);
//   } catch (error) {
//     console.error('Error reading faqs.json:', error);
//     // Handle the error accordingly
//     res.status(500).send('Error reading FAQs data');
//   }
// }

async function downloadExcel(req, res) {
  try {
    const client = req.user.clientId;

 
    const data = await db.ZeroShot.find({clientId: client});

    const existingFilePath = `${process.env.baseDir}/public/sample/faqs.xlsx`;

    if (data.length === 0) {
       try {
        // Download the existing Excel file if available
        const excelFile = fs.readFileSync(existingFilePath);
        res.setHeader('Content-disposition', 'attachment; filename=faqs.xlsx');
        res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        return res.send(excelFile);
       } catch (error) {
        console.error('Error reading existing_faqs.xlsx:', error);
        return res.status(500).send('Error reading existing FAQs file');
       }
    } else{
      const dataArray = data.map(item => ({
        Tag: item.tagName,
        question: item.question,
        answer: item.answer
      }));

      const wb = XLSX.utils.book_new();
      const ws = XLSX.utils.json_to_sheet(dataArray);
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      const excelFile = XLSX.write(wb, { bookType: 'xlsx', type: 'buffer' });

      res.setHeader('Content-disposition', 'attachment; filename=faqs.xlsx');
      res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      res.send(excelFile);

      // Close the database connection
      // await client.close();
    }
  } catch (error) {
    console.error('Error fetching data from the database:', error);
    res.status(500).send('Error fetching data from the database');
  }
}

const allowedFileFormats = ['.xlsx'];

async function insertDataBulkIntent(req, res) {
  try {
    const file = req.file;

    const fileFormat = path.extname(file.originalname);
    if (!allowedFileFormats.includes(fileFormat)) {
      return res.status(400).send('Invalid file format. Only Excel files (.xlsx) are allowed.');
    }

    const workbook = XLSX.readFile(file.path);
    if (!workbook.SheetNames.length) {
      return res.status(400).send('Invalid Excel file. The file does not contain any sheets.');
    }

    const sheet = workbook.Sheets[workbook.SheetNames[0]];
    if (!sheet || typeof sheet !== 'object') {
      return res.status(400).send('Invalid Excel file. The sheet is missing or not formatted correctly.');
    }

    const data = XLSX.utils.sheet_to_json(sheet);

    const expectedKeys = ['Tag', 'question', 'answer'];
    const hasExpectedKeys = expectedKeys.every(key => Object.keys(data[0]).includes(key));
    if (!hasExpectedKeys) {
      return res.status(400).send('Invalid Excel file. The data format does not match the expected structure.');
    }

    const uniqueDocuments = [...new Set(data.map(item => JSON.stringify({
      clientId: req.user.clientId,
      tagName: item.Tag,
      question: item.question,
      answer: item.answer,
    })))]
      .map(str => JSON.parse(str));

    const existingDocuments = await db.ZeroShot.find({
      $and: [
        { clientId: req.user.clientId },
        {
          $or: uniqueDocuments.map(document => ({
            $and: [
              { tagName: document.tagName },
              { question: document.question },
              { answer: document.answer },
            ],
          })),
        },
      ],
    });

    // Filter unique documents for insertion
    const documentsToInsert = uniqueDocuments.filter(document => {
      const existingDocument = existingDocuments.find(existing => (
        existing.tagName === document.tagName &&
        existing.question === document.question &&
        existing.answer === document.answer
      ));
      return !existingDocument;
    }).map(document => ({
      ...document,
      updatedAt: Date.now(),
      createdAt: Date.now(),
      status: true,
      analyticStatus: false,
    }));

    const result = await db.ZeroShot.insertMany(documentsToInsert);

    // Save the data to a JSON file
    // const client = req.user.clientId;
    // const jsonData = JSON.stringify(data, null, 2);
    // const filePath = `${process.env.yubo}/${client}/faqs.json`;
    // fs.writeFile(filePath, jsonData, (err) => {
    //   if (err) throw err;
    //   console.log('Data written to file');
    // });
    fs.unlink(file.path, (err) => {
      if (err) {
        console.error(err);
      }
      console.log('Uploaded file deleted');
    });

    res.send(`Inserted ${result.length} documents.`);
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal server error.');
  }
}




async function insertFAQ(req, res) {
  try {
    const { tagName, question, answer } = req.body;

    // Check if the tagName already exists
    const existingFAQ = await db.ZeroShot.findOne({ tagName });
    if (existingFAQ) {
      return res.status(400).json({ message: 'Tag name already exists.' });
    }

    const documentToInsert = {
      clientId: req.user.clientId,
      tagName,
      question,
      answer,
      updatedAt: Date.now(),
      createdAt: Date.now(),
      status: true,
      analyticStatus: false,
    };

    const result = await db.ZeroShot.insertMany(documentToInsert);

    // const client = req.user.clientId;
    // const filePath = path.join(process.env.yubo, client, 'faqs.json');

    // fs.readFile(filePath, 'utf8', (err, data) => {
    //   if (err) {
    //     console.error(err);
    //     return res.status(500).json({ message: 'Internal server error.' });
    //   }

    //   let faqs = [];
    //   if (data) {
    //     try {
    //       faqs = JSON.parse(data);
    //     } catch (parseError) {
    //       console.error(parseError);
    //       return res.status(500).json({ message: 'Internal server error.' });
    //     }
    //   }

    //   faqs.push({
    //     Tag: documentToInsert.tagName,
    //     question: documentToInsert.question,
    //     answer: documentToInsert.answer,
    //   });

    //   const jsonData = JSON.stringify(faqs, null, 2);

    //   fs.writeFile(filePath, jsonData, (writeErr) => {
    //     if (writeErr) {
    //       console.error(writeErr);
    //       return res.status(500).json({ message: 'Internal server error.' });
    //     }
    //     console.log('Data written to file');
    //     res.status(201).json({ message: 'FAQ added successfully.' });
    //   });
    // });
    res.status(201).json({ message: 'FAQ added successfully.' });

  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error.' });
  }
}

async function getBulkIntentData(req, res) {
  try {
    let skip = Number(req.query.skip) || 0;
    let limit = Number(req.query.limit) || 10;
    let search = req.query.searchItem;
    let sortField = req.query.sortField || 'createdAt';
    let sortOrder = req.query.sortOrder || 'desc';

    let docCount = 0;
    let data = [];

    const query = { clientId: req.user.clientId };
    if (search !== "") {
      query.$or = [
        { tagName: { $regex: search, $options: "i" } },
        { question: { $regex: search, $options: "i" } },
        { answer: { $regex: search, $options: "i" } },
      ];
    }

    docCount = await db.ZeroShot.countDocuments(query);

    const sortOptions = {};
    sortOptions[sortField] = sortOrder === 'asc' ? 1 : -1;

    data = await db.ZeroShot.find(query)
      .sort(sortOptions)
      .skip(skip)
      .limit(limit);

    if (data) {

      res.send({
        status: 200,
        message: "successfully get data",
        skip: skip,
        limit: limit,
        docCount: docCount,
        result: data,
      });
    }
  } catch (error) {
    console.log("error", error);
    res.status(500).send("Something went wrong!");
  }
}


async function editBulkIntentData(req, res) {
  try {
    const { _id, tagName, question, answer} = req.body;
    console.log("dasdasad",_id)

    // Validate required fields
    if (!_id) {
      return res.status(400).json({ message: 'ID is required.' });
    }

    // Find the document to edit
    const existingData = await db.ZeroShot.findById(_id);
    if (!existingData) {
      return res.status(404).json({ message: 'Data not found.' });
    }

    existingData.tagName = tagName || existingData.tagName;
    existingData.question = question || existingData.question;
    existingData.answer = answer || existingData.answer;

    // Save the updated document
    await existingData.save();

    res.status(200).json({ message: 'Data updated successfully.', data: existingData });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: 'Internal server error.' });
  }
}
async function getZeroShotDataById(req, res) {
  try {
    const { _id } = req.body;
    const existingData = await db.ZeroShot.findOne({ _id : _id });
    if (!existingData) {
      return res.status(404).json({ message: 'Data not found.' });
    }
    res.status(200).json({
        statusCode : 200, 
        message: 'Data updated successfully.', 
        data: existingData 
    });
  } catch (error) {
    console.log("Error occure in catch block",error);
    res.status(500).json({ message: 'Error occure in catch block.' });
  }
}
async function inActiveStatusBulkShot (req, res) {
  try {
    let data = await db.ZeroShot.updateOne({ _id: req.body.id, clientId: req.user.clientId},  {
      $set: {
          status: req.body.status,
      },
  });

  res.send({ message: "success" });

  } catch (error) {
    console.log(error);
    res.status(201).send({message: "internal error!"});
  }
};


async function inActiveAnalyticStatusBulkShot (req, res) {
  try {
    let data = await db.ZeroShot.updateOne({ _id: req.body.id, clientId: req.user.clientId},  {
      $set: {
        analyticStatus: req.body.analyticStatus
      },
  });

  res.send({ message: "success" });

  } catch (error) {
    console.log(error);
    res.status(201).send({message: "internal error!"});
  }
};

// const filePath = `${process.env.yubo}/${client}/faqs.json`

async function deleteBulkIntent(req, res) {
  const Tag = req.params.Tag;
  const client = req.user.clientId;

  try {
    const deletedFAQ = await db.ZeroShot.findOneAndDelete({ tagName: Tag.trim() }).exec();

    // if (!deletedFAQ) {
    //   return res.sendStatus(404);
    // }

    // fs.readFile(`${process.env.yubo}/${client}/faqs.json`, 'utf8', (err, data) => {
    //   if (err) {
    //     console.error(err);
    //     return res.sendStatus(500);
    //   }

    //   let faqs = JSON.parse(data);

    //   const index = faqs.findIndex(faq => faq.Tag.trim() === Tag.trim());

    //   if (index !== -1) {
    //     faqs.splice(index, 1);

    //     fs.writeFile(`${process.env.yubo}/${client}/faqs.json`, JSON.stringify(faqs, null, 2), err => {
    //       if (err) {
    //         console.error(err);
    //         return res.sendStatus(500);
    //       }

    //       res.sendStatus(204);
    //     });
    //   } else {
    //     console.log('Record not found');
    //     res.sendStatus(404);
    //   }
    // });
    res.status(200).send({message: "deleted successfully!"})
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
}


async function deleteSelected(req, res) {
  try {
    const { ids } = req.body;
    const data = await db.ZeroShot.deleteMany({ _id: { $in: ids } });

    if (data) {
      res.status(201).send({ message: "Deleted successfully!" });
    } else {
      res.status(404).send({ message: "Data not found!" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error!" });
  }
};






