// *****connect the socket with token *****//
var socket = io.connect("https://helloyubo.com:4900", {
  transports: ["websocket"],
  secure: true
});

let typingTimer;
let currentActiveTab;
var limit = 50;
var skip = 0;
var docCount = 0; 

// when agent close the page or leave the page send the agent id to backend so
// that online agent array can remove the user id inappchatmessage.js file

$(window).on("unload", function(e) {
  socket.emit("agentOffline", {
    agentId: getAgentIdFromSession(),
    clientId: getClientFromSession(),
  });
  removeSelectedUserIdFromSession();
});

// $(window).unload(function () {
//   socket.emit("agentOffline", {
//     agentId: getAgentIdFromSession(),
//     clientId: getClientFromSession(),
//   });
//   removeSelectedUserIdFromSession();
// });

$(document).ready(function () {
  $('#contacts').show();
  $('.contacts').hide();
  getClientData();
  $('.typingdot').hide();
  agentUserlist();
  getHumanAgentList();
  // take control and leave control function call on each take and leave control click
  $("#checkBoxBtn").change(function () {
    if (this.checked === true) {
      $(".controlText").text("Leave Control");
      takeControlFun();
      callingTemplate();
    } else {
      $(".controlText").text("Take Control");
      leaveControlFun();
    }
  });
  //************************************************* */
  // emit agent id when page is loaded so that agent id can push in the online agent array present in appchatmessage.js file
  socket.emit("agent_online", {
    agentId: getAgentIdFromSession(),
    clientId: getClientFromSession(),
  });

  $(".send-message").hide();
  $("#takeControlLabe").hide();
  $("#checkboxSlider").hide();

  let agentDetails =
    JSON.parse(localStorage.getItem("agentDetails")) ||
    JSON.parse(sessionStorage.getItem("agentDetails"));
  if (!agentDetails) {
    window.location.replace("/agent/agentlogin");
  }

  let clientIdBase64 = agentDetails.clientId;
  let clientId = atob(clientIdBase64);
  $("#client_Id").val(clientId);

  socket.emit("sendMessage", "message from agent .js");

  // for user message
  socket.on("message", (message) => {
    
       /**********get notified user id array data from local storage in case
              page relode notification will still visible   *****************/
              let localstorageNotifiedUSerList =[];
              let NotifiedUserIds =localStorage.getItem('NotifiedUserIds');
              if(NotifiedUserIds == null || NotifiedUserIds == 'undefined'|| NotifiedUserIds == '' ){
                let newArray = [];
                localStorage.setItem('NotifiedUserIds',JSON.stringify(newArray));
              }else{
                localstorageNotifiedUSerList = JSON.parse(NotifiedUserIds);
              }
        // ******************************************************************//
  if (message.clientId == getClientFromSession() && (currentActiveTab == 'ALL_CHATS' || currentActiveTab == 'MY_CHATS')) {
      if(($('.selectuser').length>1) && $(".myChatUserList").attr("alluserchatlist") =='true'){
        $('li[data-userid="' + message.userId + '"]').remove();
      }
      let userarray = [];
      $(".selectuser").each(function () {
        userarray.push($(this).attr("data-userid"));
      });
      if(userarray.includes(message.userId)){
        $(`#${message.userId}`).html(message.message);
        $(`#${message.userId}`).css({ "font-weight": "400" });
        $(`#${message.userId}`).prev().css({ "font-weight": "400" });
      } else {
      if($(".myChatUserList").attr("alluserchatlist") =='true'){
        let src=getsrc(message.userId).src;
        let html =
         `<li class="contact selectuser ${src}" data-clientid ="${message.clientId}" data-userid ="${message.userId.trim()}">    
            <div class="wrap"> 
              <!-- <span class="contact-status online"></span> -->
              <div class="circle" style ="background :${randomColorGenerator(message.name[0].toUpperCase()) }">
                <div class="chat_name">
                  <h2 class="chat_name_head"> ${message.name[0].toUpperCase()}</h2>
                </div>
              </div>
              <div class="meta">
                <p class="name">${message.name}</p>
                <p class="preview" id="${message.userId}">
                  ${message.message}
                </p>
                <div class="userStatus onlineStatus" data-label="onlineStatus" data-onlineUserId = ${message.userId}>
                    <i class="fa fa-circle text-success mr-2 userOnlineStatus"  ></i>
                </div>
                `
                if(localstorageNotifiedUSerList.includes(message.userId)){
                  html += ` <div class="contact_sec gotNotificationClass" data-userid ="${message.userId.trim()}" >
                               <img class="notify_img" src = "/dist/img/red_notification.png" style="height:30px; width:30px; border-radius: initial; margin: auto; width: 25px; height: 25px;">
                             </div>` 
                 } 
              `</div>
            </div>
          </li>`;
          $(".userList").prepend(html);
        }else{
           console.log('else part');
        }
      
      }

      if ($(".userchatData").attr("userId") == message.userId && !message.attachment) {
        html = `<li class="sent">
  <div class="wrap">
  <div class="circle" style ="background :${randomColorGenerator(
    message.name[0].toUpperCase()
  )}">
  <div class="chat_name">
  <h2 class="chat_name_head"> ${message.name[0].toUpperCase()}</h2>
  </div>
  </div>
  <p>
  ${message.message}
  <span class="date_time" 
  style="position: relative;
  font-size: 12px; 
  display: block;
  text-align:
  right;bottom: -5px;">
  ${message.date} at ${message.time}
  </span>
  </p>
  </div>
  </li>`;
        $(".userchatData").append(html);
        $(".messages").animate({ scrollTop: $(".messages").prop("scrollHeight") },0);
      }
      if(message.attachment){
        if ($(".userchatData").attr("userId") == message.userId) {
          html =
           `<li class="sent">
              <div class="wrap">
                <div class="circle" style ="background :${randomColorGenerator(message.name[0].toUpperCase()) }">
                  <div class="chat_name">
                    <h2 class="chat_name_head"> ${message.name[0].toUpperCase()}</h2>
                  </div>
                </div>
                <p>`
                if(message.attachmentType == 'png' || message.attachmentType == 'jpg' || message.attachmentType == 'jpeg'){
                  html += `<a href= ${message.attachmentLink} target="_blank"> <img src = ${message.attachmentLink} style="width:100%"> </a>`
                }else if(message.attachmentType == 'pdf'){
                  html += `<a href=${message.attachmentLink} target="_blank"> <img src =" /dist/img/pdf_download_icon.png" style="width:100%"></a>`
                }else if(message.attachmentType == 'msword'){
                  html += `<a href=${message.attachmentLink} target="_blank"> <img src =" /dist/img/word_icon.png" style="width:100%"></a>`
                }
                  `<span class="date_time" 
                        style="position: relative;
                        font-size: 12px; 
                        display: block;
                        text-align:
                        right;bottom: -5px;">
                      ${message.date} at ${message.time}
                  </span>
                </p>
              </div>
            </li>`;
          $(".userchatData").append(html);
        }
      } 
    }
  if (message.clientId == getClientFromSession() && currentActiveTab == 'CAMPAIN_CHATS') {
    campaignChatDataUpdate(message,localstorageNotifiedUSerList);
  }
  });
  // for bot message
  socket.on("botmessage", (botmessage) => {
  
    $(`#${botmessage.userId}`).html(botmessage.botmessage);
    $(`#${botmessage.userId}`).addClass("chngetextcolor");
    $(`#${botmessage.userId}`).css({ "font-weight": "bolder" });
    $(`#${botmessage.userId}`).prev().css({ "font-weight": "bolder" });
    // $(`#${botmessage.userId}`).prependTo($('ul.userList'))
    if ($(".userchatData").attr("userId") == botmessage.userId) {
      botmessage.botmessage=removeTags(botmessage.botmessage)
      html = ` <li class="replies">     
  <img src="/dist/files/favicon.png" alt="favicon">
  <p>
  ${botmessage.botmessage}
  <span class="date_time" style="position: relative;
  font-size: 12px; display: block; text-align: right;bottom: -5px;">
  ${botmessage.date} at ${botmessage.time}
  </span>
  </p>
  </li>`;

      $(".userchatData").append(html);
      $(".messages").animate({ scrollTop: $(".messages").prop("scrollHeight") },0);
    }

  });
  // to emit data in agent panel
  socket.on("sendMessageToAgentPanel", (botmessage) => {
    if (botmessage.clientId == getClientFromSession()) {
      $(`#${botmessage.userId}`).html(botmessage.botmessage);
      $(`#${botmessage.userId}`).addClass("chngetextcolor");
      $(`#${botmessage.userId}`).css({ "font-weight": "bolder" });
      $(`#${botmessage.userId}`).prev().css({ "font-weight": "bolder" });
      if ($(".userchatData").attr("userId") == botmessage.userId) {
        if (botmessage.control == "yes" || botmessage.control == "no") {
          html = `<p class="takeControlLabelCss"> 
  ${botmessage.botmessage}
  </p>`;
        } else {
          botmessage.botmessage=removeTags(botmessage.botmessage)
          html = ` <li class="replies">
  <img src="/dist/files/favicon.png" alt="favicon">
  <p>
  ${botmessage.botmessage}
  <span class="date_time" style="position: relative;
  font-size: 12px; display: block; text-align: right;bottom: -5px;">
  ${botmessage.date} at ${botmessage.time} 
  </span>
  </p>
  </li>`;
        }

        $(".userchatData").append(html);
        $(".messages").animate({ scrollTop: $(".messages").prop("scrollHeight") },0);
      }
    }
  });
  socket.on("getDataOnTakeControl", (data) => {
    let selected_user_id = localStorage.getItem("selected_user_id");
    if (
      data.clientId == getClientFromSession() &&
      data.userId == selected_user_id &&
      data.agentId != getAgentIdFromSession(0)
    ) {
      $(".controlText").text("Already in Chat with Agent");
      $("#checkboxSlider").hide();
      $("#checkBoxBtn").prop(
        "checked",
        true
      );
      $(".send-message").hide();
    }
  });
  socket.on("getDataOnLeaveControl", (data) => {
    let selected_user_id = localStorage.getItem("selected_user_id");
    if (data.clientId == getClientFromSession() && data.userId == selected_user_id && data.agentId != getAgentIdFromSession(0)) {
      $(".controlText").text("Take Control");
      $("#checkboxSlider").show();
      $("#checkBoxBtn").prop("checked",false);
      $(".send-message").hide();
    }
  });
  socket.on("onAgentBlock", (data) => {
    if (
      data.clientId == getClientFromSession() &&
      data.agentId == getAgentIdFromSession()
    ) {
      socket.emit("agentOffline", {
        agentId: getAgentIdFromSession(),
        clientId: getClientFromSession(),
      });

      localStorage.removeItem("agentDetails");
      sessionStorage.removeItem("agentDetails");
      window.location.replace("/agent/agentlogin");
    }
  });

  socket.on("userTyping", (data) => {
    if (
      data.clientId == getClientFromSession() &&
      data.userId == localStorage.getItem("selected_user_id")
    ) {
      $(".typingdot").show();
      setTimeout(function () {
        $(".typingdot").hide();
      }, 3000);
    }
  });
  socket.on("getSameAgentData", (data) => {
    let selected_user_id = localStorage.getItem("selected_user_id");
    if (data.clientId == getClientFromSession() && data.userId == selected_user_id) {
      if(data.control){
        $(".controlText").text("Leave Control");
        $("#checkboxSlider").show(); 
        $("#checkBoxBtn").prop("checked",data.control);
        $(".send-message").show();
      }else{
        $(".controlText").text("Take Control");
        $("#checkboxSlider").show(); 
        $("#checkBoxBtn").prop("checked",data.control);
        $(".send-message").hide();
      }
    }
  });
  socket.on("getRequestControlOfOtherAgent", (data) => {
    if(data.clientId == getClientFromSession() && data.RequestFromAgentId != getAgentIdFromSession()){
      // let data4 = "[agentidfornotification='616e5df408570a4bb072730e']"
      let data4 = "[agentIdForNotification="+"'"+data.RequestFromAgentId+"']"
      let elementData = document.querySelectorAll(data4);
      // let data5 = document.querySelectorAll(`"[agentid='${data.RequestFromAgentId}']"`);
      if(data.requestToAgentId == getAgentIdFromSession()){
        elementData[0].style.display='block'
      }
      let agentNotificationData =getAgentNotificationFromSession();
      if(agentNotificationData == null || agentNotificationData == 'undefined'|| agentNotificationData == '' ){
        let newArray = [];
        newArray.push(data);
        localStorage.setItem('agentNotificationData',JSON.stringify(newArray));
      }else{ 
        agentNotificationData.push(data);
        localStorage.setItem('agentNotificationData',JSON.stringify(agentNotificationData));
      }
      var audio = document.getElementById("audio");
      audio.play();
    }
  });
socket.on("userOnline", (data) => {
     let elementData =  document.querySelectorAll(`[data-onlineUserId='${data.userId}']`);
     elementData.length>0 ?  elementData[0].style.display='block' : ''
});
socket.on("userOffline", (data) => {
  // console.log('user Offline ---------:', data)
  let elementData =  document.querySelectorAll(`[data-onlineUserId='${data.userId}']`);
  elementData.length>0 ?  elementData[0].style.display='none' : ''
  // console.log('elementData:', elementData)
});
});
let agentUserList_Array;
function onClickAllChatButton(activetable) {
  $('#contacts').show();
  $('.contacts').hide();
  skip  =0;
  limit =50;
  $(".userList").empty();
  $( ".campaignTempltDropdown" ).hide();
  $( "#loadMoreButtonId" ).show();
  $('.searchBoxDiv').show();
  currentActiveTab = 'ALL_CHATS';
  $('.myChatUserList').attr('allUserChatlist',true)
  agentUserlist(currentActiveTab);
}

function onClickAllWhatsapp(userType) {
  $('#contacts').show();
  $('.contacts').hide();
  skip  =0;
  limit =50;
  $(".userList").empty();
  $( ".campaignTempltDropdown" ).hide();
  $( "#loadMoreButtonId" ).show();
  $('.searchBoxDiv').show();
  $('.myChatUserList').attr('allUserChatlist',true)

  
  agentUserlist(userType);
}

function onClickAllWhatsapp2(agentId) {
  $('#contacts').show();
  $('.contacts').hide();
  skip  =0;
  limit =50;
  $(".userList").empty();
  $( ".campaignTempltDropdown" ).hide();
  $( "#loadMoreButtonId" ).show();
  $('.searchBoxDiv').show();
  $('.myChatUserList').attr('allUserChatlist',true)

  
  agentUserlist(null, agentId);
}

function agentUserlist(userType = null, agentId ) {
  $( ".campaignTempltDropdown" ).hide();
  // $( "#loadMoreButtonId" ).show();
  // $('.searchBoxDiv').show();
  // currentActiveTab = 'ALL_CHATS';
  $('.myChatUserList').attr('allUserChatlist',true)
  currentActiveTab = 'ALL_CHATS';
  let agentDetails = JSON.parse(localStorage.getItem('agentDetails')) || JSON.parse(sessionStorage.getItem('agentDetails'));
  if (!agentDetails) {
    window.location.replace("/agent/agentlogin");
  }
  if(['all'].includes(userType)) {
    userType = null
  }
  $.ajax({
    type: "POST",
    url: "/agent/agentUserlist",
    // data: { clientId: getClientFromSession() , userType, agentId, skip : skip, limit : limit },
    data: {
      clientId: getClientFromSession(),
      userType: userType,
      agentId: agentId,
      skip: skip,
      limit: limit
    },
    beforeSend: function (xhr) {
      xhr.setRequestHeader("authtoken", agentDetails.authtoken);
    },
    success: function (data) {
      if (data.statusCode === 400) {
        localStorage.removeItem("agentDetails");
        sessionStorage.removeItem("agentDetails");
        window.location.replace("/agent/agentlogin");
      } else if (data.statusCode === 201) {
        localStorage.removeItem("agentDetails");
        sessionStorage.removeItem("agentDetails");
        window.location.replace("/agent/agentlogin");
      }

      if(data.config!=undefined){
        localStorage.setItem("agentConfig",JSON.stringify(data.config))
      } else {
        localStorage.setItem("agentConfig",false)
      }

      if (data) {
        skip     = data.skip;
        limit    = data.limit;
        docCount = data.docCount;

        agentUserList_Array = data.res;
        // $(".userList").html("");
        $(".campList").empty();
        /**********get notified user id array data from local storage in case
                  page relode notification will still visible   *****************/
        let localstorageNotifiedUSerList =[];
        let NotifiedUserIds =localStorage.getItem('NotifiedUserIds');
        if(NotifiedUserIds == null || NotifiedUserIds == 'undefined'|| NotifiedUserIds == '' ){
          let newArray = [];
          localStorage.setItem('NotifiedUserIds',JSON.stringify(newArray));
        }else{
          localstorageNotifiedUSerList = JSON.parse(NotifiedUserIds);
        }
        for (const iterator of data.res) {
          let name, html;
          if (iterator.session && iterator.session.name != "") {
            name = iterator.session.name;
          } else if (iterator.name != null) {
            name = iterator.name;
          } else {
            name = "Visitor";
          }
          let src=getsrc(iterator.userId).src;
          html = `<li  class="contact selectuser ${src}" data-clientid ="${iterator.clientId}" data-userId="${iterator.userId}">
                    <div class="wrap">
                      <!-- <span class="contact-status online"></span> -->
                      <div class="circle" style ="background :${randomColorGenerator(name[0].toUpperCase())}">
                        <div class="chat_name">
                        <h2 class="chat_name_head"> ${name[0].toUpperCase()}</h2>
                        </div>
                      </div>
                      <div class="meta">
                        <p class="name">${name}</p>
                        <p class="preview" data-chatid="${iterator.userId}">${iterator.lastchat[0]}</p>
                        <div class="userStatus onlineStatus" data-label="onlineStatus" data-onlineUserId = ${iterator.userId}>
                           <i class="fa fa-circle text-success mr-2 userOnlineStatus"  ></i>
                        </div>
                     `
  
                        if(localstorageNotifiedUSerList.includes(iterator.userId)){
                         html += ` <div class="contact_sec gotNotificationClass" data-userid ="${iterator.userId.trim()}" >
                                      <img class="notify_img" src = "/dist/img/red_notification.png" style="height:30px; width:30px; border-radius: initial; margin: auto; width: 25px; height: 25px;">
                                    </div>` 
                        } 
                      `</div>
                    </div>
                  </li>`;
          $(".userList").append(html);
        }
      } else {
        alert(data.msg);
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Please refresh !");
    },
  });
}

function takeControlFun() {
  let agentName = getAgentName();
  let selected_user_id = localStorage.getItem("selected_user_id");
  let agentConfig=(localStorage.getItem("agentConfig"))?JSON.parse(localStorage.getItem("agentConfig")):false;
  socket.emit("join", selected_user_id);
  socket.emit("sendMessageInRoom", {
    userId: selected_user_id,
    message: (agentConfig && agentConfig.agent_start_text!=undefined)?agentConfig.agent_start_text.replace("[AGENT]",agentName):` ${agentName} taking control now`,
    control: "yes",
    clientId: getClientFromSession(),
    agentName:agentName,
    avatar: "https://helloyubo.com/avatar.png"
  });

  let agentId = getAgentIdFromSession();
  $.ajax({
    type: "POST",
    url: "/agent/agentTakeControl",
    data: {
      userId: selected_user_id,
      agentCurrentConrol: {
        agentId: agentId,
        currentControl: true,
      },
      chatControl: 1,
    },
    timeout: 10000,
    success: function (resp) {
      if (resp) {
        if (resp.statusCode === 200) {
          createTicket();
          $(".send-message").show();
          socket.emit("emitSameAgentData", {
            clientId: getClientFromSession(),
            userId: selected_user_id,
            agentId: getAgentIdFromSession(),
            control: true
          });
          socket.emit("emitDataOnTakeControl", {
            clientId: getClientFromSession(),
            userId: selected_user_id,
            agentId: getAgentIdFromSession(),
          });
        } else {
          console.log(resp.message);
          alert(resp.message);
        }
      } else {
        console.log("error occures");
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page", err);
    },
  });
}

function sendmessagekeyup(e){
if(e.which == 13) {
    sendMessage();
  } else {
  	typingFun();
  }
}




function sendMessage() {
  $(".messages").animate({ scrollTop: $(".messages").prop("scrollHeight") },0);
  let agentName = getAgentName();

  // agentDataSave($("#client_Id").val(), $("#textMessage").val());
  let message = $("#textMessage").val();
  if (message.trim().length > 0) {
    agentDataSave($("#client_Id").val(), $("#textMessage").val());
    let selected_user_id = localStorage.getItem("selected_user_id");
    $("p[data-chatid='" + selected_user_id + "']").html(message);
    $(`#${selected_user_id}`).html(message);
    socket.emit("join", selected_user_id);
    socket.emit("sendMessageInRoom", {
      userId: selected_user_id,
      message: message,
      clientId: getClientFromSession(),
      agentName:agentName,
      avatar: "https://helloyubo.com/avatar.png"
    });
    $("#textMessage").val("");
  } else {
    alert("please enter text before send it");
  }
}
/** for user chat */
$(document).on("click", ".selectuser", function () {
 if(currentActiveTab == 'ALL_CHATS' || currentActiveTab == 'MY_CHATS'){
  hideSelectuser(this);
  //***** On click of user remove notification icon instantly *******//
  $(this).find('.gotNotificationClass').remove();
  // remove  user id from localstorage 
  let NotifiedUserIds =localStorage.getItem('NotifiedUserIds');
  if(NotifiedUserIds == null || NotifiedUserIds == 'undefined'|| NotifiedUserIds == '' ){
    let newArray = [];
    localStorage.setItem('NotifiedUserIds',JSON.stringify(newArray));
  }else{
     let userArray = JSON.parse(NotifiedUserIds);
     let index = userArray.indexOf($(this).attr("data-userId"))
     if(index > -1){
      userArray.splice(index,1);
     }
     localStorage.setItem('NotifiedUserIds',JSON.stringify(userArray));
  }

  $('.typingdot').hide();
  socket.emit("agent_online", {
    agentId: getAgentIdFromSession(),
    clientId: getClientFromSession(),
  });

  let userName = $(this).find(".name").html();
  localStorage.setItem('selectedUserName',userName);

  $(".contact-profile h2").html($(this).find(".name").html());
  $(".userchatData").html("");
  $("#contacts li.contact").removeClass("active");
  $(".contacts li.contact").removeClass("active");
  $(this).addClass("active");
  $(this).find(".preview").css({ color: "black", "font-weight": "400" });
  $(this).find(".name").css({ color: "black", "font-weight": "600" });

  let data = {
    clientId: $(this).attr("data-clientid"),
    userId: $(this).attr("data-userId"),
  };
  $(".userchatData").attr("userId", $(this).attr("data-userId"));
  localStorage.setItem("selected_user_id", $(this).attr("data-userId"));
  getTicketData();
  $.ajax({
    type: "POST",
    url: "/agent/agentUserChat",
    data: data,
    success: function (data) {
      if (data.statusCode == 200) {
        let userSession = data.res[0].session;
        if (userSession && userSession.name) {
          $("#user_name").val(userSession.name);
          $("#user_email").val(userSession.email);
          $("#user_phone").val(userSession.phone);
        } else {
          $("#user_name").val("");
          $("#user_email").val("");
          $("#user_phone").val("");
        }
        let agentId = getAgentIdFromSession();
        if (data.res[0].agentCurrentConrol) {
          $("#takeControlLabe ").show();
          data.res[0].agentCurrentConrol.currentControl
            ? $(".controlText").text("Leave Control")
            : $(".controlText").text("Take Control");
          if (data.res[0].agentCurrentConrol.agentId == agentId) {
            $("#checkboxSlider").show();
            $("#checkBoxBtn").prop(
              "checked",
              data.res[0].agentCurrentConrol.currentControl
            );
            $(".send-message").show();
          } else if (
            data.res[0].agentCurrentConrol.agentId != agentId &&
            data.res[0].agentCurrentConrol.currentControl === true
          ) {
            $(".controlText").text("Already in Chat with Agent");
            $("#checkboxSlider").hide();
            $("#checkBoxBtn").prop(
              "checked",
              data.res[0].agentCurrentConrol.currentControl
            );
            $(".send-message").hide();
          } else {
            $("#checkboxSlider").show();
            $("#checkBoxBtn").prop(
              "checked",
              data.res[0].agentCurrentConrol.currentControl
            );
            $(".send-message").hide();
          }
        } else {
          $("#checkboxSlider").show();
        }
        let userchat = data.res[0].chats;
        if (data.res[0].session) {
          $(".userName").html(data.res[0].session.name);
          $(".userMail").html(data.res[0].session.email);
        }
        if (data.res[0].location) {
          let completeLocation =
            data.res[0].location.city + ", " + data.res[0].location.state;
          $(".userLocation").html(completeLocation);
        }

        var html;
        $(".userNamedata").html("");
        for (const iterator of userchat) {
          let format_Date = moment(new Date(iterator.createdAt)).format(
            "DD/MM/YYYY"
          );
          iterator.date = format_Date;
          now = moment(new Date(iterator.createdAt));
          iterator.time = now.hour() + ":" + now.minutes();

          if (iterator.messageType == "outgoing") {
            if (iterator.attachment == true && iterator.attachmentType == "jpeg" || iterator.attachmentType == "png" || iterator.attachmentType == "jpg") {
              html = ` <li class="replies">
                          <img src="/dist/files/favicon.png" alt="favicon">
                          <p style="background : white">
                            <a href= ${iterator.attachmentLink} target="_blank">
                              <img src = ${iterator.attachmentLink} style="width:100%"> 
                              <span class="date_time" 
                                    style="position: relative;
                                    font-size: 12px; 
                                    display: block; 
                                    color: black;
                                    text-decoration: none;
                                    text-align: right;
                                    bottom: -5px;">
                                  ${iterator.date} at ${iterator.time} 
                              </span>
                            </a>
                          </p>
                        </li>`;
                   $(".userchatData").append(html);
            }else if (iterator.attachment == true && iterator.attachmentType == "pdf") {
                  html = ` <li class="replies">
                          <img src="/dist/files/favicon.png" alt="favicon">
                          <p style="background : white">
                            <a href= ${iterator.attachmentLink} target="_blank">
                              <img src = "/dist/img/pdf_download_icon.png" style="width:100%"> 
                              <span class="date_time" 
                                    style="position: relative;
                                    font-size: 12px; 
                                    display: block; 
                                    color: black;
                                    text-decoration: none;
                                    text-align: right;
                                    bottom: -5px;">
                                  ${iterator.date} at ${iterator.time} 
                              </span>
                            </a>
                          </p>
                        </li>`;
                  $(".userchatData").append(html);
            }else if (iterator.attachment == true && iterator.attachmentType == "msword") {
              html = ` <li class="replies">
                          <img src="/dist/files/favicon.png" alt="favicon">
                          <p style="background : white">
                            <a href= ${iterator.attachmentLink} target="_blank">
                              <img src ="/dist/img/word_icon.png" style="width:100%"> 
                              <span class="date_time" 
                                    style="position: relative;
                                    font-size: 12px; 
                                    display: block; 
                                    color: black;
                                    text-decoration: none;
                                    text-align: right;
                                    bottom: -5px;">
                                  ${iterator.date} at ${iterator.time} 
                              </span>
                            </a>
                          </p>
                        </li>`;
                  $(".userchatData").append(html);
            }else if(iterator.text.trim()!=""){
              iterator.text=removeTags(iterator.text)
            html = ` <li class="replies">
  <img src="/dist/files/favicon.png" alt="favicon">
  <p>
  ${iterator.text}
  <span class="date_time" style="position: relative;
  font-size: 12px; display: block; text-align: right;bottom: -5px;">
  ${iterator.date} at ${iterator.time} 
  </span>
  </p>
  </li>`;
            $(".userchatData").append(html);
            }
          } else {
            if (iterator.attachment == true && iterator.attachmentType == "jpeg" || iterator.attachmentType == "png" || iterator.attachmentType == "jpg") {
              html = `<li class="sent">
                        <div class="wrap">
                            <div class="circle" style ="background :${randomColorGenerator(userName[0].toUpperCase())}">
                              <div class="chat_name">
                              <h2 class="chat_name_head"> ${userName[0].toUpperCase()}</h2>
                              </div>
                            </div>
                            <p>
                            <a href= ${iterator.attachmentLink} target="_blank">
                                <img src = ${iterator.attachmentLink} style="width:100%"> 
                                <span class="date_time" 
                                      style="position: relative;
                                      text-decoration: none;
                                      color: black;
                                      font-size: 12px; display: block;
                                      text-align: right;bottom: -5px;">
                                  ${iterator.date} at ${iterator.time} 
                                </span>
                            </a>
                            </p>
                        </div>
                      </li>`;
                      $(".userchatData").append(html);
            }else if (iterator.attachment == true && iterator.attachmentType == "pdf") {
                html = `<li class="sent">
                          <div class="wrap">
                              <div class="circle" style ="background :${randomColorGenerator(userName[0].toUpperCase())}">
                                <div class="chat_name">
                                <h2 class="chat_name_head"> ${userName[0].toUpperCase()}</h2>
                                </div>
                              </div>
                              <p>
                              <a href= ${iterator.attachmentLink} target="_blank">
                                  <img src ="/dist/img/pdf_download_icon.png" style="width:100%"> 
                                  <span class="date_time" 
                                        style="position: relative;
                                        text-decoration: none;
                                        color: black;
                                        font-size: 12px; display: block;
                                        text-align: right;bottom: -5px;">
                                    ${iterator.date} at ${iterator.time} 
                                  </span>
                              </a>
                              </p>
                          </div>
                        </li>`;
                        $(".userchatData").append(html);
            }else if (iterator.attachment == true && iterator.attachmentType == "msword") {
              html = `<li class="sent">
                          <div class="wrap">
                              <div class="circle" style ="background :${randomColorGenerator(userName[0].toUpperCase())}">
                                <div class="chat_name">
                                <h2 class="chat_name_head"> ${userName[0].toUpperCase()}</h2>
                                </div>
                              </div>
                              <p>
                                  <a href= ${iterator.attachmentLink} target="_blank">
                                      <img src ="/dist/img/word_icon.png" style="width:100%"> 
                                      <span class="date_time" 
                                            style="position: relative;
                                            text-decoration: none;
                                            color: black;
                                            font-size: 12px; display: block;
                                            text-align: right;bottom: -5px;">
                                        ${iterator.date} at ${iterator.time} 
                                      </span>
                                  </a>
                              </p>
                          </div>
                        </li>`;
                        $(".userchatData").append(html);
            }else if(iterator.text.trim()!=""){
            html = `<li class="sent">
  <div class="wrap">
  <div class="circle" style ="background :${randomColorGenerator(
    userName[0].toUpperCase()
  )}">
  <div class="chat_name">
  <h2 class="chat_name_head"> ${userName[0].toUpperCase()}</h2>
  </div>
  </div>
  <p>
  ${iterator.text}
  <span class="date_time" style="position: relative;
  font-size: 12px; display: block;
  text-align: right;bottom: -5px;">
  ${iterator.date} at ${iterator.time} 
  </span>
  </p>
  </div>
  </li>`;
            $(".userchatData").append(html);
          }
         }
        }
        $(".new-content").removeClass("col-sm-9").addClass("col-sm-6");
        $("#frame .content .contact-profile").css({ background: "#fff" });
        $(".contact-profile h2").css({ display: "block" });
        $(".detail_hide").css({ display: "block" });
        $(".messages").animate({ scrollTop: $(".messages").prop("scrollHeight") },0);

        //***on click of user show message  at bottom that user asked for agent  */
        let  userarray = [];
        $(".gotNotificationClass").each(function () {
          userarray.push($(this).attr("data-userid"));
        });
        let NewUserarray = [...new Set(userarray)];
        let selected_user_id = localStorage.getItem("selected_user_id");
        if(NewUserarray.includes(selected_user_id)){
          let agentName = getAgentName();
          socket.emit("UserNeedAgentMessageToAgent", {
            userId: selected_user_id,
            message: `User asked for human agent`,
            control: "yes",
            clientId: getClientFromSession(),
            agentName:agentName,
            avatar: "https://helloyubo.com/avatar.png"
          });
        }
      } else {
        console.log("Error occured", data);
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Please refresh !");
    },
  });
 }
 if(currentActiveTab == 'CAMPAIN_CHATS'){
   onSelecteOfCampaignUser(this);
 }
});
function leaveControlFun() {
  let agentName = getAgentName();
  let selected_user_id = localStorage.getItem("selected_user_id");
  let agentConfig=(localStorage.getItem("agentConfig"))?JSON.parse(localStorage.getItem("agentConfig")):false;
  socket.emit("leave", selected_user_id);
  socket.emit("sendMessageInRoom", {  
    userId: selected_user_id,
    message: (agentConfig && agentConfig.agent_end_text!=undefined)?agentConfig.agent_end_text.replace("[AGENT]",agentName):`${agentName} transferred the chat control to the bot`,
    control: "no",
    clientId: getClientFromSession(),
    agentName:agentName,
    avatar: "https://helloyubo.com/avatar.png"
  });

  socket.emit("emitDataOnLeaveControl", {
    clientId: getClientFromSession(),
    userId: selected_user_id,
    agentId: getAgentIdFromSession(),
  });
  $.ajax({
    type: "POST",
    url: "/agent/agentTakeControl",
    data: {
      userId: selected_user_id,
      agentCurrentConrol: {
        agentId: "",
        currentControl: false,
      },
      chatControl: 0,
    },
    timeout: 10000,
    success: function (resp) {
      if (resp) {
        if (resp.statusCode === 200) {
          $('li[data-chatid="' + selected_user_id + '"]').remove();
          $(".send-message").hide();

          socket.emit("emitSameAgentData", {
            clientId: getClientFromSession(),
            userId: selected_user_id,
            agentId: getAgentIdFromSession(),
            control: false
          });

        } else {
          console.log(resp.message);
          alert(resp.message);
        }
      } else {
        console.log("error occures");
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page", err);
    },
  });
}
let agentTookControlUserIds;
async function logoutFun() {
  swal({
    title: "Are you sure want to logout ?",
    text: "Once you logout, you will leave all chat controls!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  }).then((willDelete) => {
    if (willDelete) {
      leaveAllControlOnLogout();
      socket.emit("agentOffline", {
        agentId: getAgentIdFromSession(),
        clientId: getClientFromSession(),
      });
    }
  });
}

async function logoutmobileviewFun() {
  swal({
    title: "Are you sure want to logout ?",
    text: "Once you logout, you will leave all chat controls!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  }).then((willDelete) => {
    if (willDelete) {
      leaveAllControlOnLogout();
      socket.emit("agentOffline", {
        agentId: getAgentIdFromSession(),
        clientId: getClientFromSession(),
      });
    }
  });
}

function getAgentName() {
  let agentDetails =
    JSON.parse(localStorage.getItem("agentDetails")) ||
    JSON.parse(sessionStorage.getItem("agentDetails"));
  let agentNameBase64 = agentDetails.agentName;
  let agentName = atob(agentNameBase64);
 return agentName;
  // return agentName.charAt(0).toUpperCase();
}
function getAgentIdFromSession() {
  let agentDetails =
    JSON.parse(localStorage.getItem("agentDetails")) ||
    JSON.parse(sessionStorage.getItem("agentDetails"));
  if (!agentDetails) {
    window.location.replace("/agent/agentlogin");
  }
  let agentIdBase64 = agentDetails.agentId;
  let agentId = atob(agentIdBase64);
  return agentId;
}
function myChatsData(activetable) {
  $('#contacts').hide();
  $('.contacts').hide();
  $( "#contact-list" ).show();  
  $('.searchBoxDiv').hide();
  $( "#loadMoreButtonId" ).hide();
  currentActiveTab = activetable;
  $('.myChatUserList').attr('allUserChatlist',false)
  let agentDetails = JSON.parse(localStorage.getItem('agentDetails')) || JSON.parse(sessionStorage.getItem('agentDetails'));
  if (!agentDetails) {
    window.location.replace("/agent/agentlogin");
  }
  $.ajax({
    type: "POST",
    url: "/agent/myChatsUserList",
    data: {
      clientId: getClientFromSession(),
      agentId: getAgentIdFromSession(),
    },
    beforeSend: function (xhr) {
      xhr.setRequestHeader("authtoken", agentDetails.authtoken);
    },
    success: function (data) {
      if (data) {
        if (data.statusCode === 400) {
          localStorage.removeItem("agentDetails");
          sessionStorage.removeItem("agentDetails");
          window.location.replace("/agent/agentlogin");
        } else if (data.statusCode === 201) {
          localStorage.removeItem("agentDetails");
          sessionStorage.removeItem("agentDetails");
          window.location.replace("/agent/agentlogin");
        }
        $(".myChatUserList").html("");
        $(".campList").html("");
        for (const iterator of data.res) {
          let name, html;
          if (iterator.session && iterator.session.name != "") {
            name = iterator.session.name;
          } else if (iterator.name != null) {
            name = iterator.name;
          } else {
            name = "Visitor";
          }
          let src=getsrc(iterator.userId).src;
          html = `<li class="contact selectuser ${src}" data-userId="${
            iterator.userId
          }" data-clientid ="${iterator.clientId}" data-chatid="${
            iterator.userId
          }">
  <div class="wrap">
  <div class="circle" style ="background :${randomColorGenerator(
    name[0].toUpperCase()
  )}">
  <div class="chat_name">
  <h2 class="chat_name_head"> ${name[0].toUpperCase()}</h2>
  </div>
  </div>
  <div class="meta">
  <p class="name">${name}</p>
  <p class="preview" id="${iterator.userId}">${iterator.lastchat[0]}</p>
  </div>
  </div>
  </li>`;
          $(".myChatUserList").append(html);
        }
      } else {
        alert(data.msg);
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Please refresh !");
    },
  });
}

function agentDataSave(clientId, text) {
  let agentId = getAgentIdFromSession();
  let client_id = localStorage.getItem('client_id');
  $.ajax({
    type: "POST",
    url: "/agent/agentDataSave",
    data: {
      text: text,
      userId: localStorage.getItem("selected_user_id"),
      clientId: clientId,
      client_id : client_id,
      agentControl : true,
      agentId : agentId 
    },
    success: function (data) {
      console.log(data);
    },
  });
}

function getClientFromSession() {
  let agentDetails =
    JSON.parse(localStorage.getItem("agentDetails")) ||
    JSON.parse(sessionStorage.getItem("agentDetails"));
  if (!agentDetails) {
    window.location.replace("/agent/agentlogin");
  }
  let agentIdBase64 = agentDetails.clientId;
  let clientId = atob(agentIdBase64);
  return clientId;
}


function searchbox() {
  $(".campList").empty();
  $(".tempChatList").empty();
  let searchedText = $(".searchBox").val();
  if (searchedText.length > 3) {
    $.ajax({
      type: "POST",
      url: "/agent/searchData",
      data: {
        searchText: searchedText,
        clientId: getClientFromSession(),
      },
      timeout: 10000,
      success: function (resp) {
        if (resp && resp.statusCode === 200) {
          $(".userList").html("");
          if (resp.res.length < 1) {
            $(".searchBox").val("");
          } else {
            for (const iterator of resp.res) {
              let name = iterator.userData.name || iterator.userData.session.name || "Visitor";
              let src = getsrc(iterator.userData.userId).src;
              let html = `<li class="selectuser contact ${src}" style="padding:10px 5px 10px 5px; font-weight:bold;" 
                data-clientid="${iterator.userData.clientId}" data-userId="${iterator.userData.userId}">
                <div class="wrap">
                  <div class="circle" style="background:${randomColorGenerator(name[0].toUpperCase())}">
                    <div class="chat_name">
                      <h2 class="chat_name_head">${name[0].toUpperCase()}</h2>
                    </div>
                  </div>
                  <div class="meta">
                    <p class="name">${name}</p>`;
              for (let chat of iterator.chats) {
                html += `<p style="font-weight: 500; padding-top: 9px;">${chat.text}</p>`;
              }
              html += `</div>
                <div></div>
              </li>`;

              $(".userList").append(html);
            }
          }
        } else {
          console.log(resp ? resp.message : "error occurred");
        }
      },
      error: function (jqXHR, textStatus, err) {
        alert("Something went wrong, please refresh the page", err);
      },
    });
  } else if (searchedText.length === 0) {
    $(".userList").empty();
    agentUserlist();
  }
}
/*Leave all user controls of agent on logout or close current window*/
function leaveAllControlOnLogout() {
  $.ajax({
    type: "POST",
    url: "/agent/leaveAllControlsOnLogout",
    data: {
      agentId: getAgentIdFromSession(),
    },
    timeout: 10000,
    success: function (resp) {
      if (resp) {
        if (resp.statusCode === 200) {
          agentTookControlUserIds = resp.agentTookControlUserIds;

          let agentName = getAgentName();
          agentTookControlUserIds.map((ele) => {
            socket.emit("sendMessageInRoom", {
              userId: ele,
              message: `${agentName} transferred the chat control to the bot`,
              control: "no",
              clientId: getClientFromSession(),
              agentName:agentName,
              avatar: "https://helloyubo.com/avatar.png"
            });
          });
          localStorage.removeItem("agentDetails");
          sessionStorage.removeItem("agentDetails");
          window.location.replace("/agent/agentlogin");
        } else {
          console.log(resp.message);
          alert(resp.message);
        }
      } else {
        console.log("error occures");
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page", err);
    },
  });
}
/** browser notification for user initiative */
document.addEventListener("DOMContentLoaded", function () {
  if (!Notification) {
    alert("Desktop notifications not available in your browser. Try Chromium.");
    return;
  }
  if (Notification.permission !== "granted") Notification.requestPermission();
});

// function notifyMe(data) {

//   if (Notification.permission !== "granted") Notification.requestPermission();
//   else {
//     var title = "JavaScript Jeep";
//     icon = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQiiZXMPV7K63lyZ63XdVx3Md4QzphY_3G2cw&usqp=CAU';
//     var body = "It's Your boarding time";
//     var notification = new Notification(title, { body, icon });

//      // close the notification after 10 seconds
//      setTimeout(() => { 
//         notification.close();
//       }, 900 * 1000);

//     notification.onclick = function () {
//       window.open("http://stackoverflow.com/a/13328397/1269037", "_self");
//     };
//   }
// }
// notifyMe('Jayanta');
// socket.on("send-notification", (data) => {
//   // if(data.clientId == getClientFromSession){
//   // }
//   notifyMe(data);
// });

/** */

function randomColorGenerator(string) {
  let stringObj = {
    0: ["A", "B", "C", "D", "E","9","4"],
    1: ["F", "G", "H", "I", "J","8","3"],
    2: ["K", "L", "M", "N", "O","7","2"],
    3: ["P", "Q", "R", "S", "T","6","1"],
    4: ["U", "V", "W", "X", "Y", "Z","5"],
  };
  var colorArray = ["#086805", "#D906AE", "#a23a07", "#8c0677", "#8c0606"];
  for (let elem in stringObj) {
    if (stringObj[elem].includes(string)) {
      return colorArray[elem];
    }
  }
}

function updateUserData() {
  let email = $("#user_email").val();
  let phone = $("#user_phone").val();

  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if(phone < 0){
    swal({
      title: "Phone number can not be negative",
      icon: "warning",
      timer: 3500
    });
  } else if(email && !re.test(String(email).toLowerCase())){
      swal({
        title: "Please provide valid email id",
        icon: "warning",
        timer: 3500
      }); 
  }else{
      $.ajax({
        type: "POST",
        url: "/agent/updateUserData",
        data: {
          name: $("#user_name").val(),
          email: $("#user_email").val(),
          phone: $("#user_phone").val(),
          userId: localStorage.getItem("selected_user_id"),
        },
        timeout: 10000,
        success: function (resp) {
          if (resp) {
            if (resp.statusCode === 200) {
              swal({
                title: resp.message,
                icon: "success",
                timer: 3000,
              });
            } else {
              console.log(resp.message);
            }
          } else {
            console.log("error occures");
          }
        },
        error: function (jqXHR, textStatus, err) {
          alert("Something went wrong, please refresh the page", err);
        },
      });
  }

}

function typingFun() {
  let agentName=getAgentName();
  socket.emit("agentTyping", {
    clientId: getClientFromSession(),
    userId: localStorage.getItem("selected_user_id"),
    agentName: agentName,
    avatar: "https://helloyubo.com/avatar.png"
  });
  clearTimeout(typingTimer);
  typingTimer = setTimeout(doneTyping, 1000);
}

//agent is "finished typing,"
function doneTyping () {
  let agentName=getAgentName();
  socket.emit("typingStopped", {
    clientId: getClientFromSession(),
    userId: localStorage.getItem("selected_user_id"),
    agentName: agentName,
    avatar: "https://helloyubo.com/avatar.png"
  });
}


function removeSelectedUserIdFromSession() {
  localStorage.removeItem("selected_user_id");
  sessionStorage.removeItem("selected_user_id");
}
async function uploadfile(e){

  const file =e.target.files[0]
  let base64File = await toBase64(file);
  let fileType = file.type.split('/');

  let uploadFile = await uploadFileToS3Server(e.target,getClientFromSession(),localStorage.getItem('selected_user_id'));

}
async function uploadFileToS3Server(fileList,clientId,userId){
  
    var msg = "",
    fileSizeRequire = 512000;
    if (fileList.files[0].size <= fileSizeRequire) {
        const file = fileList.files[0];
        let base64File = await toBase64(file);

        let fileType = file.type.split('/');
        let fileValidation= validateFileType(file.type);
        if(!fileValidation){
          let msg = "Selected file is not valid,Please choose an image file"
          alert(msg); 
          return msg;
        }
        const fdata = new FormData();
        fdata.append("fileList", fileList.files[0]);
        fdata.append("fileList", clientId);
        fdata.append("fileList", userId);
        fdata.append("fileList", fileType[1]);
        await fetch("/agent/saveAgentAttachment", {
          method: "post",
          body: fdata,
        }).then((resp) => {
          if (resp.status == 200 || resp.status == "200")
            return resp.json();
        }).then(function(data) {
           console.log('data 1111',data);

           let agentName = getAgentName();
           let selected_user_id =localStorage.getItem('selected_user_id');
           socket.emit("sendAttachmentToBot", {
               userId: selected_user_id,
               control: "",
               message :'Attachment sent',
               clientId: getClientFromSession(),
               attachmentType: data.attachmentType,
               attachmentLink : data.attachmentLink,
               attachment : true ,
               agentName:agentName,
               avatar: "https://helloyubo.com/avatar.png"
           });
           
           html = ` <li class="replies">
           <img src="/dist/files/favicon.png" alt="favicon">
           <p style="background: white;">`
             if(fileType[1] == 'png' ||fileType[1] == 'jpeg' || fileType[1] == 'jpg'){
               html+= `<a href=${data.attachmentLink} target="_blank"> 
                       <img src=${data.attachmentLink} style="width: 100%; margin: 0px;" id="attach" alt=""></a>`
             }else if(fileType[1] == 'pdf'){
               html += `<a href=${data.attachmentLink} target="_blank"> 
                       <img src ="/dist/img/pdf_download_icon.png" style="width:100%"></a>`
             }else if(fileType[1] == 'msword'){
               html += `<a href=${data.attachmentLink} target="_blank"> 
                       <img src ="/dist/img/word_icon.png" style="width:100%"></a>`
             }
             `<span class="date_time" style="position: relative;font-size: 12px; display: block; text-align: right;bottom: -5px;">
             </span>
           </p>
           </li>`;
           $(".userchatData").append(html);

           return data;
        });
  } else {
    msg = "The max file size is 500Kb";
    alert(msg);
  }
}
const toBase64 = file => new Promise((resolve, reject) => {
const reader = new FileReader();
reader.readAsDataURL(file);
reader.onload = () => resolve(reader.result);
reader.onerror = error => reject(error);
});
var humanAgentList = [];
function getHumanAgentList(){
  $.ajax({
    async: false,
    cache: false,
    method: 'POST',
    data: {
      clientId: getClientFromSession()
    },
    url: '/agent/agent-list-by-clientId',
    success: function (data) {
      if (data.statusCode == 200) {
        let agentdata = data.res.agents;
        humanAgentList = agentdata;
        agentdata = agentdata.filter(ele=>ele._id != getAgentIdFromSession());
        agentListExceptMe = agentdata;
        totalAgent = agentdata.length;
        agentdata.forEach(el=>{
          let lastname = el.name.split(' ')
          if(lastname[1])
             el.lastname = lastname[1];
        })
        $('.agent_list').html('')
        // $('.totalAgent').html(agentdata.length);
        for (const iterator of agentdata) {
          checkValue = "unchecked"
          let html = 
            `<div class="all_agentdetails">
            <div class="agent_list_img">
              <div class="agent_bordr">
                <i class="fa fa-user-secret" aria-hidden="true"></i>
              </div>
            </div>
            <div class="other_agent_desc">
                <div class="agent_desc0">
                  <h3 class="agent_name">${iterator.name.toUpperCase()}</h3>
                </div>
                <div class="switch_toggle_btn">
                  <label class="switch">
                    <input type="checkbox" 
                           id=${iterator._id}
                           agentidforchecked=${iterator._id}
                           agentName = ${ iterator.name }
                           onclick="handleClick(this,id)"
                           checkValue =${checkValue}>
                    <span class="slider round  round_button" title="Transfer Control"></span>
                  </label>
                </div>
                <div class="agent_status" data-label="Avalibility">
                  <i class="fa fa-circle text-danger mr-2 checkAvailable" availibilityId = ${iterator._id} title="Online"></i>
                </div>
                <button type="button" class="contact_sec gotNotificationClass_0 div-hide"  agentIdForNotification=${iterator._id} data-toggle="modal" onclick="openModal(this)">
                  <img class="notify_img agentNotification" agentIdForNotification=${iterator._id} src = "/dist/img/purple_notification.png" style="height:30px; width:30px; border-radius: initial; margin: auto; width: 25px; height: 25px;">
                </button>
            </div>
          </div>`
          $('.agent_list').append(html);
        }
  
      } else {
        console.log(data.message)
      }
    }
  });
}

// after send agent id receive the online agents id in array so that we can count how many agent are online  now //
socket.on('onlineAgentsList', function (onlineAgentsList) {
  let clientId = getClientFromSession();
  //** from all agent list  (onlineAgentsList) filter agents by clientid ,so client get only agents working usnder client//
  let agentIdsbyClientId = onlineAgentsList.filter(Element => {
    return clientId == Element.clientId;
  });
  //** store (agentIds) only ids of agents  in an array ,which we can user for online ofline functionality in next step//
  agentIds = agentIdsbyClientId.map(ele => {
    return ele.agentId;
  })
  $('.checkAvailable').each(function () {
    if (agentIds.includes($(this).attr('availibilityId'))) {
      $(this).addClass('text-success')
      $(this).removeClass('text-danger')
    } else {
      $(this).removeClass('text-success')
      $(this).addClass('text-danger')
    }
  })
});
socket.on("sendAgentControlRequest", (message) => {    
    let NotifiedUserIds =localStorage.getItem('NotifiedUserIds');
    if(NotifiedUserIds == null || NotifiedUserIds == 'undefined'|| NotifiedUserIds == '' ){
      let newArray = [];
      newArray.push(message.userId);
      localStorage.setItem('NotifiedUserIds',JSON.stringify(newArray));
    }else{
       let NotifiedUserIdsArray = JSON.parse(NotifiedUserIds);
       NotifiedUserIdsArray.push(message.userId);
       let newArray =[];
       newArray = [...new Set(NotifiedUserIdsArray)];
       localStorage.setItem('NotifiedUserIds',JSON.stringify(newArray));
    }
  if (message.clientId == getClientFromSession()) {
    if(($('.selectuser').length>1) && $(".myChatUserList").attr("alluserchatlist") =='true'){
      $('li[data-userid="' + message.userId + '"]').remove();
    }  
  let userarray = [];
  $(".selectuser").each(function () {
    userarray.push($(this).attr("data-userid"));
  });

  if(userarray.includes(message.userId)){
    $(`#${message.userId}`).html(message.message);
    $(`#${message.userId}`).css({ "font-weight": "400" });
    $(`#${message.userId}`).prev().css({ "font-weight": "400" });
  } else {
  if($(".myChatUserList").attr("alluserchatlist") =='true'){
    let src=getsrc(message.userId).src;
    let html = `<li class="contact selectuser ${src}" data-clientid ="${message.clientId
    }" data-userid ="${message.userId.trim()}">
      <div class="wrap">
      <!-- <span class="contact-status online"></span> -->
      <div class="circle" style ="background :${randomColorGenerator(message.name[0].toUpperCase()) }">
      <div class="chat_name">
      <h2 class="chat_name_head"> ${message.name[0].toUpperCase()}</h2>
      </div>
      </div>
      <div class="meta">
      <p class="name">${message.name}</p>
      <p class="preview" id="${message.userId}">
      ${message.message}
      </p>
      <div class="contact_sec gotNotificationClass" data-userid ="${message.userId.trim()}" >
        <img class="notify_img" src = "/dist/img/red_notification.png" style="height:30px; width:30px; border-radius: initial; margin: auto; width: 25px; height: 25px;">
      </div>
      </div>
      </div>
      </li>`;
      $(".userList").prepend(html);
      var audio = document.getElementById("audio");
      audio.play();
    }else{
      console.log('else part');

    }

  }
    $(".messages").animate({ scrollTop: $(".messages").prop("scrollHeight") },0);
  }
});

function handleClick(myRadio,id) {
  checkValue     = $(myRadio).attr("checkValue");
  $(`#${id}`).closest('.agentNotification' ).addClass( "" );
  /*******************************************************************
   on agent transfer control to another agent toggle switche checked
   ******************************************************************/
  if($(myRadio).attr("checkValue") == 'unchecked'){
    
         /************************************************************************
         * maintain an array in session to identify agent sent request to other agent
         *************************************************************************/
        let requestsToAgentsArray =getMyRequestsToAgentFromSession();
        if(requestsToAgentsArray == null || requestsToAgentsArray == 'undefined'|| requestsToAgentsArray == '' ){
          let newArray = [] ,data ={};
          data["requestToAgentId"]= id;
          data["requestFromAgentId"]= getAgentIdFromSession();
          data["userId"] =  localStorage.getItem('selected_user_id') ; 
          newArray.push(data);
          localStorage.setItem('myRequestsToAgents',JSON.stringify(newArray));
        }else{
          let data ={};
          data["requestToAgentId"] =id;
          data["requestFromAgentId"] =getAgentIdFromSession();
          data["userId"] =localStorage.getItem('selected_user_id'); 
          requestsToAgentsArray.push(data);
          localStorage.setItem('myRequestsToAgents',JSON.stringify(requestsToAgentsArray));
          // let filterData = requestsToAgentsArray.filter(ele=> ele.requestToAgentId != data.requestToAgentId && ele.userId != data.userId )
        }

        /************************************************************************
         * check the current toggle button and emmit data to other agent panel
         *************************************************************************/
        $(myRadio).attr("checkValue",'checked');
        socket.emit("sendRequestControlToOtherAgent", {
        userId: localStorage.getItem('selected_user_id'),
        userName : localStorage.getItem('selectedUserName'),
        RequestFromAgentId: getAgentIdFromSession(),
        RequestFromAgentName: getAgentName() ,
        requestToAgentName: $(myRadio).attr("agentName"),
        requestToAgentId : id ,
        clientId: getClientFromSession(),
      });
  }else{
     /*****uncheck the current toggle and change the attribute value   ********/
       $(myRadio).attr("checkValue",'unchecked');
       
         /************************************************************************
         * maintain an array in session to identify agent sent request to other agent
         *************************************************************************/
          let requestsToAgentsArray =getMyRequestsToAgentFromSession();
          if(requestsToAgentsArray == null || requestsToAgentsArray == 'undefined'|| requestsToAgentsArray == '' ){
            // localStorage.setItem('myRequestsToAgents',JSON.stringify(newArray));
          }else{
            let filterData = requestsToAgentsArray.filter(ele=> ele.requestFromAgentId != id && ele.userId != localStorage.getItem('selected_user_id') );
            localStorage.setItem('myRequestsToAgents',JSON.stringify(filterData));
          }
  }
}
function getAgentNotificationFromSession(){
  let agentNotificationData =localStorage.getItem('agentNotificationData');
  let userArray = JSON.parse(agentNotificationData);
  return userArray;
}
function getMyRequestsToAgentFromSession(){
  let myRequestsToAgents =localStorage.getItem('myRequestsToAgents');
  let userArray = JSON.parse(myRequestsToAgents);
  return userArray;
}

function openModal(){
  $('#myModal').modal('show');
}
//get and set client id to hit save agent message api 
function getClientData() {
  let clientId = getClientFromSession();
  $.ajax({
    type: "POST",
    url: "/agent/getClientData",
    data: {
      clientId: clientId
    },
    success: function (data) {
      localStorage.setItem('client_id',data.data._id);
    },
  });
}

function validateFileType(mimetype){
  let matched = false ; 
   switch(mimetype){
      case "image/jpeg":
          matched = true;    
          break;
      case "image/jpg":
        matched = true;    
        break;
      case "image/gif":
        matched = true;    
        break;
      case "image/webp":
        matched = true;    
        break;
      case "image/webp":
        matched = true;    
        break;
      case "image/svg+xml":
        matched = true;    
        break;
      case "image/png":
        matched = true;    
        break;
        case "application/pdf":  
        matched = true;    
        break;
      case "text/plain":  
        matched = true;    
        break;
      case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":  
        matched = true;    
        break;
      case "application/vnd.ms-excel":  
        matched = true;    
        break;
      case "application/msword":  
        matched = true;    
        break;
      case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":  
        matched = true;    
        break;
      case "application/vnd.ms-powerpoint":  
        matched = true;    
        break;
   }
   return matched; 
}

/*******************************************************************
 *get ticket data of a particular user
*******************************************************************/
function getTicketData() {
  $.ajax({
    type: "POST",
    url: "/agent/getRepliesByTicketId",
    data: {
      "userId"             :  localStorage.getItem("selected_user_id"),
      "clientId"           :  getClientFromSession(),
      "agentId"            :  getAgentIdFromSession()
    },
    timeout: 10000,
    success: function (resp) {
      if (resp) {
        if (resp.statusCode === 200) {
          $(".userNotes").html("");
          if(resp.data && Object.keys(resp.data).length > 0 ){
            $(".userNotesDiv").show();
            let TicketRepliesData = resp.data.pagination.data.reverse();
            let shift = TicketRepliesData.shift();
            // <h6 class="agent">  ${iterator.user.display_name} </h6>

            for (const iterator of TicketRepliesData) {
                let html;
                html =
                `
                <div class="note-content">
                    <p class="message" ticketId =  ${iterator.ticket_id} replyId= ${iterator.id} id="agent_notes">
                       ${iterator.body}
                    </p>
                </div>
                `;
                  $(".userNotes").append(html);
            }
            $("#userNotes").val('');
          }else{
            $(".userNotesDiv").hide();
          }

        } else {
          // console.log(resp.message);
          alert(resp.message);
        }
      } else {
        console.log("error occures");
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page", err);
    },
  });
} 
/*******************************************************************
 *add user note  to a perticular user by agent
*******************************************************************/
function addUserNotes(){
  let userNote = $("#userNotes").val().trim();
  if(userNote == '' || userNote == 'undefined' || userNote ==  null ){
    alert("Notes can not be blank, Please type notes then press enter button")
     return ; 
  }else{
      $.ajax({
        type: "POST",
        url: "/agent/addRepliesOnTicket",
        data: {
          "body"               :  `${ getAgentName().toUpperCase()}: ${userNote}`,
          "status"             :  'open',
          "userId"             :  localStorage.getItem("selected_user_id"),
          "clientId"           :  getClientFromSession(),
          "agentId"            :  getAgentIdFromSession()
        },
        timeout: 10000,
        success: function (resp) {
          if (resp) {
            if (resp.statusCode === 200) {
              getTicketData();
            } else {
              // console.log(resp.message);
              alert(resp.message);
            }
          } else {
            console.log("error occures");
          }
        },
        error: function (jqXHR, textStatus, err) {
          alert("Something went wrong, please refresh the page", err);
        },
      });
  }
}
/*******************************************************************
 *generate ticket when agent taking control over a user 
*******************************************************************/
function createTicket(){
  $.ajax({
    type: "POST",
    url: "/agent/addTicket",
    data: {
      "subject"            :  "Agent Notes",
      "body"               :  'Ticket generate for user to add notes',
      "userId"             :  localStorage.getItem("selected_user_id"),
      "clientId"           :  getClientFromSession(),
      "agentId"            :  getAgentIdFromSession()
    },
    timeout: 10000,
    success: function (resp) {
      if (resp) {
        if (resp.statusCode === 200) {
          getTicketData();
        } else {
          console.log(resp.message);
          //alert(resp.message);
        }
      } else {
        console.log("error occures");
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page", err);
    },
  });
}
function addNotesOnEnterPress(e){
  if(e.which == 13) {
    addUserNotes();
    }
}

function loadMore(event){   
  skip = parseInt(skip) + parseInt(limit); 
  if(docCount >= skip){
    if(currentActiveTab == 'ALL_CHATS'){
      agentUserlist();
      console.log('done:')
    }else if(currentActiveTab == 'CAMPAIN_CHATS'){
      //  let clientId = getClientFromSession(),
      //       agentId = getAgentIdFromSession(),
      //       data = {
      //           clientId:clientId,
      //           agentId:agentId,
      //           skip  : skip,
      //           limit : limit
      //       }
      //       findCampaignUserList(data); 
      let selectedVal= JSON.parse($(".tempList").val())
      let clientId = getClientFromSession(),
          agentId= getAgentIdFromSession(),
          data;
      if(selectedVal.name == 'All'){
          data = {
            clientId : clientId,
            agentId  : agentId
          };
      }else{
          data = {
            tempName : selectedVal.name,
            clientId : clientId
          };
      }
      data.skip = skip;
      data.limit = limit;
      findCampaignUserList(data);
    }
  }else{
      console.log('not done:')
      $( "#loadMoreButtonId" ).hide();
  }
}

/****************************************************************************
 * get campaign user for all the current agent assignd templates
 ***************************************************************************/
function campaignChatUserList(activetable){
  $('#contacts').hide();
  $('.contacts').show();
  limit =50;
  skip  =0;
  $("#loadMoreButtonId" ).show();
  $(".campList").html(""); 
  findAllTemplates();
  $('.searchBoxDiv').hide();
  $( ".campaignTempltDropdown" ).show();
  $('.myChatUserList').attr('allUserChatlist',false);
  currentActiveTab = activetable;
  //console.log('currentActiveTab:', currentActiveTab)
  let clientId = getClientFromSession(),
      agentId= getAgentIdFromSession(),
      data = {
          clientId:clientId,
          agentId:agentId,
          skip  : skip,
          limit : limit
      }
      findCampaignUserList(data);
};
/****************************************************************************
 *find the template list on the basis of clientId and agentId 
 ***************************************************************************/
async function findAllTemplates(){
  $(".tempList").empty();
  let clientId = getClientFromSession(); 
  let agentId= getAgentIdFromSession();
  $.ajax({
    data: {
      clientId:clientId,
      agentId:agentId
    },
    async: false,
    cache: false,
    method: "POST",
    url: "/findTempLists",  
    success: function (data) {
      if(data.statusCode==200){
        let allTempData = {name : "All" , _id : '' };
        $(".tempList").append(`<option value= ${JSON.stringify(allTempData)}>All Users </option>`);
        for( const iterator of data.data){
            $(".tempList").append(`<option value=${JSON.stringify(iterator)}>${iterator.name}</option>`);
        }
      }
    },error: function (jqXHR, textStatus, err) {  
    alert("something went wrong,please try again")
    }
});
}
/****************************************************************************
 *on select the specific template from dropdown get the related user chat.
 ***************************************************************************/
function selectSpecificTemplate(){
   skip  = 0;
   limit = 50;
  $('.searchBoxDiv').hide();
  $('.myChatUserList').attr('allUserChatlist',false);
  $(".campList").empty();
  $( "#loadMoreButtonId" ).show();
  //console.log('xxxx:')
  let selectedVal= JSON.parse($(".tempList").val())
  //console.log('selectedVal:', selectedVal.name)
  //console.log('name:', selectedVal)
  let clientId = getClientFromSession(),
      agentId= getAgentIdFromSession(),
      data;
  if(selectedVal.name == 'All'){
      data = {
        clientId : clientId,
        agentId  : agentId
      };
  }else{
      data = {
        tempName : selectedVal.name,
        clientId : clientId,
      };
  }
  data.limit =limit ;
  data.skip  =skip  ;
  findCampaignUserList(data);
}
/****************************************************************************
 *socket event campaign user  chat data emits here
 *when a campaign user starts chat this function calls with message data
 ***************************************************************************/
campUsersArray = [] ; 
function campaignChatDataUpdate(message,localstorageNotifiedUSerList){
  let usrId = message.userId.trim(),
     userIdStartsWith = usrId.split("_")[0];
  if(userIdStartsWith == 'wa'){

    $('li[data-userid="' + message.userId + '"]').remove();

          if($(".myChatUserList").attr("alluserchatlist") =='false' && currentActiveTab == 'CAMPAIN_CHATS'){
            //console.log('message 11111111111111111111 :', message)
            let src=getsrc(message.userId).src;
            let html =
            `<li class="contact campUser selectuser ${src}" 
                 data-clientid ="${message.clientId}" 
                 data-userid   ="${message.userId.trim()}"
                 name          = "${message.name}"
                 createdAt     = "${message.createdAt}"
                 >    
                <div class="wrap camp-circles"> 
                  <div class="circle" style ="background :${randomColorGenerator(message.name[0].toUpperCase()) }">
                    <div class="chat_name">
                      <h2 class="chat_name_head"> ${message.name[0].toUpperCase()}</h2>
                    </div>
                  </div>
                  <div class="meta">
                    <p class="name">${message.name}</p>
                    <p class="preview" id="${message.userId}">
                      ${message.message}
                    </p>
                  </div>
                </div>
                <div style="text-align: end;
                            padding-right:15px;
                            padding-top:10px;">
                    <small style="position:relative;left:">  ${message.date} at ${message.time} </small>
                </div>
              </li>`;
              $(".campList").prepend(html);
            }else{
              console.log('else part');
            }

    let selected_user_id = localStorage.getItem("selected_user_id");
    if (selected_user_id == message.userId && !message.attachment  && currentActiveTab == 'CAMPAIN_CHATS') {
      //console.log('selected_user_id:', selected_user_id);
      //console.log(' AAAAAAAAAAAAAAAAAAA 11111111111111111 ', $(".myChatUserList").attr("alluserchatlist") ); 
      //console.log('currentActiveTab:', currentActiveTab)

      html = `<li class="sent">
                    <div class="wrap">
                      <div class="circle" style ="background :${randomColorGenerator(message.name[0].toUpperCase())}">
                        <div class="chat_name">
                            <h2 class="chat_name_head"> ${message.name[0].toUpperCase()}</h2>
                        </div>
                      </div>
                      <p>
                          ${message.message}
                          <span class="date_time" 
                                style="position: relative;
                                font-size: 12px; 
                                display: block;
                                text-align:
                                right;bottom: -5px;">
                                ${message.date} at ${message.time}
                          </span>
                      </p>
                    </div>
              </li>`;
      $(".userchatData").append(html);
      //console.log('1444444444444444444444444444444 jay:')

      $(".messages").animate({ scrollTop: $(".messages").prop("scrollHeight") },0);
    }
    
      //console.log('KIIIIIIIIIIIIIIIIIIIIIII:', message)
    if (message.attachment && selected_user_id == message.userId && currentActiveTab == 'CAMPAIN_CHATS') {
      html =
        `<li class="sent">
          <div class="wrap">
            <div class="circle" style ="background :${randomColorGenerator(message.name[0].toUpperCase()) }">
              <div class="chat_name">
                <h2 class="chat_name_head"> ${message.name[0].toUpperCase()}</h2>
              </div>
            </div>
            <p>`
            if(message.attachmentType == 'png' || message.attachmentType == 'jpg' || message.attachmentType == 'jpeg'){
              html += `<a href= ${message.attachmentLink} target="_blank"> <img src = ${message.attachmentLink} style="width:100%"> </a>`
            }else if(message.attachmentType == 'pdf'){
              html += `<a href=${message.attachmentLink} target="_blank"> <img src =" /dist/img/pdf_download_icon.png" style="width:100%"></a>`
            }else if(message.attachmentType == 'msword'){
              html += `<a href=${message.attachmentLink} target="_blank"> <img src =" /dist/img/word_icon.png" style="width:100%"></a>`
            }
              `<span class="date_time" 
                    style="position: relative;
                    font-size: 12px; 
                    display: block;
                    text-align:
                    right;bottom: -5px;">
                  ${message.date} at ${message.time}
              </span>
            </p>
          </div>
        </li>`;
      $(".userchatData").append(html);
      //console.log('1555555555555555555555555555555 jay:')

      $(".messages").animate({ scrollTop: $(".messages").prop("scrollHeight") },0);
      
    } 
  }
}
/****************************************************************************
 *Get user data on click of campaign specific user.
 ***************************************************************************/
function onSelecteOfCampaignUser(_this){
  //console.log(' select campaign working fine ',_this)
   let userid      =  $(_this).attr("data-userid"),
       clientId    =  $(_this).attr("data-clientid"),
       userName    =  $(_this).attr("name");
   $(".userchatData").attr("userId", userid);
  
  //console.log('userid:', userid);
  // //console.log('msg_type:', msg_type);
  
  // //console.log('parameters:', parameters);
  // //console.log('media_url:', media_url);

  let selectedUserName = $(_this).find(".name").html();
  $(".contact-profile h2").html(selectedUserName);

  localStorage.setItem('selectedUserName',selectedUserName);
  /****************show user details panel in right side ************************/
  $(".detail_hide").css({ display: "block" });
  $(".contact-profile h2").css({ display: "block" });
  $("#frame .content .contact-profile").css({ background: "#fff" });
  $(".new-content").removeClass("col-sm-9").addClass("col-sm-6");
  

    if(userid != undefined && userid != 'undefined' && userid != null && userid != '' ){
        localStorage.setItem("selected_user_id", userid);
        let data = {
          clientId:clientId ,
          userId: userid ,
        };
        $.ajax({
          type: "POST",
          url: "/agent/agentUserChat",
          data: data,
          success: function (data) {
            if (data.statusCode == 200) {
              //console.log('data: 3333333333333333333333', data)
                /********************************************************
                 * append template data In user communication 
                 * so that agent can see the sent whatsapp template 
                ************************************************************/
                      createdat   =  data.sentOn,
                      format_Date =  moment(new Date(createdat)).format("DD/MM/YYYY"),
                      date        =  format_Date,
                      now         =  moment(new Date(createdat)),
                      time        =  now.hour() + ":" + now.minutes(),
                      msg         =  replaceParams(data.templateData.text,data.parameters);

                      if(data.templateData.msg_type == 'VIDEO'){
                          $(".userchatData").html("");
                          html = ` <li class="replies">
                                        <img src="/dist/files/favicon.png" alt="favicon">
                                      <div class="users_chat">
                                        <p class="users-videos" style="">
                                            <a href= ${data.media_url} target="_blank">
                                              <video width="220" height="140" controls>
                                                <source src= ${data.media_url} type="video/mp4">
                                              </video>
                                            </a>
                                        </p>
                                        <p class="users-mesgs">
                                            ${msg}
                                            <div class="date_time" 
                                                  style="position: relative;font-size: 12px; display: block; text-align: right;bottom: -5px;color: white;
                                                  padding-right: 15px;">
                                                  ${date} at ${time} 
                                            </div>
                                        </p>

                                      </div>
                                        
                                  </li>`;
                        $(".userchatData").append(html);
                      }else if (data.templateData.msg_type == 'IMAGE'){
                          $(".userchatData").html("");
                            html = ` <li class="replies">
                                          <img src="/dist/files/favicon.png" alt="favicon">
                                        <div class="users_chat" style="float:right;">
                                          <p class="users-images" style="background : ">
                                              <a href= ${data.media_url} target="_blank">
                                                <img src = ${data.media_url} style="width:100%;border-radius: 10px;"> 
                                              </a>
                                          </p>
                                          <p class="users-mesgs">
                                              ${msg}
                                              <div class="date_time" 
                                                    style="position: relative;font-size: 12px; display: block; text-align: right;bottom: -5px;color:white;padding-right: 10px;">
                                                    ${date} at ${time} 
                                              </div>
                                          </p>
                                        </div>
                                          
                                    </li>`;
                          $(".userchatData").append(html);
                      }else{
                        $(".userchatData").html("");
                        html = ` <li class="replies">
                                      <img src="/dist/files/favicon.png" alt="favicon">
                                    <div class="users_chat" style="float:right;">
                                      <p class="users-images" style="background : ">
                                          <a href= ${data.media_url} target="_blank">
                                            <img src = "./dist/img/file.png" style="width:100%;border-radius: 10px;"> 
                                          </a>
                                      </p>
                                      <p class="users-mesgs">
                                          ${msg}
                                          <div class="date_time" 
                                                style="position: relative;font-size: 12px; display: block; text-align: right;bottom: -5px;color:white;padding-right: 10px;">
                                                ${date} at ${time} 
                                          </div>
                                      </p>
                                    </div>
                                      
                                </li>`;
                      $(".userchatData").append(html);
                      }
                //************************************//
              let userSession = data.res[0].session;
              if (userSession && userSession.name) {
                $("#user_name").val(userSession.name);
                $("#user_email").val(userSession.email);
                $("#user_phone").val(userSession.phone);
              } else {
                $("#user_name").val("");
                $("#user_email").val("");
                $("#user_phone").val("");
              }
              let agentId = getAgentIdFromSession();
              $(".userNotesDiv").show();

              if (data.res[0].agentCurrentConrol) {
                $("#takeControlLabe ").show();
                data.res[0].agentCurrentConrol.currentControl
                  ? $(".controlText").text("Leave Control")
                  : $(".controlText").text("Take Control");
                if (data.res[0].agentCurrentConrol.agentId == agentId) {
                  $("#checkboxSlider").show();
                  $("#checkBoxBtn").prop(
                    "checked",
                    data.res[0].agentCurrentConrol.currentControl
                  );
                  $(".send-message").show();
                } else if (
                  data.res[0].agentCurrentConrol.agentId != agentId &&
                  data.res[0].agentCurrentConrol.currentControl === true
                ) {
                  $(".controlText").text("Already in Chat with Agent");
                  $("#checkboxSlider").hide();
                  $("#checkBoxBtn").prop(
                    "checked",
                    data.res[0].agentCurrentConrol.currentControl
                  );
                  $(".send-message").hide();
                } else {
                  $("#checkboxSlider").show();
                  $("#checkBoxBtn").prop(
                    "checked",
                    data.res[0].agentCurrentConrol.currentControl
                  );
                  $(".send-message").hide();
                }
              } else {
                $("#checkboxSlider").show();
              }
              let userchat = data.res[0].chats;
              if (data.res[0].session) {
                $(".userName").html(data.res[0].session.name);
                $(".userMail").html(data.res[0].session.email);
              }
              if (data.res[0].location) {
                let completeLocation =
                  data.res[0].location.city + ", " + data.res[0].location.state;
                $(".userLocation").html(completeLocation);
              }
      
              var html;
              $(".userNamedata").html("");
              for (const iterator of userchat) {
                let format_Date = moment(new Date(iterator.createdAt)).format(
                  "DD/MM/YYYY"
                );
                iterator.date = format_Date;
                now = moment(new Date(iterator.createdAt));
                iterator.time = now.hour() + ":" + now.minutes();
      
                if (iterator.messageType == "outgoing") {
                  if (iterator.attachment == true && iterator.attachmentType == "jpeg" || iterator.attachmentType == "png" || iterator.attachmentType == "jpg") {
                    html = ` <li class="replies">
                                <img src="/dist/files/favicon.png" alt="favicon">
                                <p style="background : white">
                                  <a href= ${iterator.attachmentLink} target="_blank">
                                    <img src = ${iterator.attachmentLink} style="width:100%"> 
                                    <span class="date_time" 
                                          style="position: relative;
                                          font-size: 12px; 
                                          display: block; 
                                          color: black;
                                          text-decoration: none;
                                          text-align: right;
                                          bottom: -5px;">
                                        ${iterator.date} at ${iterator.time} 
                                    </span>
                                  </a>
                                </p>
                              </li>`;
                        $(".userchatData").append(html);
                  }else if (iterator.attachment == true && iterator.attachmentType == "pdf") {
                        html = ` <li class="replies">
                                <img src="/dist/files/favicon.png" alt="favicon">
                                <p style="background : white">
                                  <a href= ${iterator.attachmentLink} target="_blank">
                                    <img src = "/dist/img/pdf_download_icon.png" style="width:100%"> 
                                    <span class="date_time" 
                                          style="position: relative;
                                          font-size: 12px; 
                                          display: block; 
                                          color: black;
                                          text-decoration: none;
                                          text-align: right;
                                          bottom: -5px;">
                                        ${iterator.date} at ${iterator.time} 
                                    </span>
                                  </a>
                                </p>
                              </li>`;
                        $(".userchatData").append(html);
                  }else if (iterator.attachment == true && iterator.attachmentType == "msword") {
                    html = ` <li class="replies">
                                <img src="/dist/files/favicon.png" alt="favicon">
                                <p style="background : white">
                                  <a href= ${iterator.attachmentLink} target="_blank">
                                    <img src ="/dist/img/word_icon.png" style="width:100%"> 
                                    <span class="date_time" 
                                          style="position: relative;
                                          font-size: 12px; 
                                          display: block; 
                                          color: black;
                                          text-decoration: none;
                                          text-align: right;
                                          bottom: -5px;">
                                        ${iterator.date} at ${iterator.time} 
                                    </span>
                                  </a>
                                </p>
                              </li>`;
                        $(".userchatData").append(html);
                  }else if(iterator.text.trim()!=""){
                    iterator.text=removeTags(iterator.text)
                  html = ` <li class="replies">
        <img src="/dist/files/favicon.png" alt="favicon">
        <p>
        ${iterator.text}
        <span class="date_time" style="position: relative;
        font-size: 12px; display: block; text-align: right;bottom: -5px;">
        ${iterator.date} at ${iterator.time} 
        </span>
        </p>
        </li>`;
                  $(".userchatData").append(html);
                  }
                } else {
                  if (iterator.attachment == true && iterator.attachmentType == "jpeg" || iterator.attachmentType == "png" || iterator.attachmentType == "jpg") {
                    html = `<li class="sent">
                              <div class="wrap">
                                  <div class="circle" style ="background :${randomColorGenerator(userName[0].toUpperCase())}">
                                    <div class="chat_name">
                                    <h2 class="chat_name_head"> ${userName[0].toUpperCase()}</h2>
                                    </div>
                                  </div>
                                  <p>
                                  <a href= ${iterator.attachmentLink} target="_blank">
                                      <img src = ${iterator.attachmentLink} style="width:100%"> 
                                      <span class="date_time" 
                                            style="position: relative;
                                            text-decoration: none;
                                            color: black;
                                            font-size: 12px; display: block;
                                            text-align: right;bottom: -5px;">
                                        ${iterator.date} at ${iterator.time} 
                                      </span>
                                  </a>
                                  </p>
                              </div>
                            </li>`;
                            $(".userchatData").append(html);
                  }else if (iterator.attachment == true && iterator.attachmentType == "pdf") {
                      html = `<li class="sent">
                                <div class="wrap">
                                    <div class="circle" style ="background :${randomColorGenerator(userName[0].toUpperCase())}">
                                      <div class="chat_name">
                                      <h2 class="chat_name_head"> ${userName[0].toUpperCase()}</h2>
                                      </div>
                                    </div>
                                    <p>
                                    <a href= ${iterator.attachmentLink} target="_blank">
                                        <img src ="/dist/img/pdf_download_icon.png" style="width:100%"> 
                                        <span class="date_time" 
                                              style="position: relative;
                                              text-decoration: none;
                                              color: black;
                                              font-size: 12px; display: block;
                                              text-align: right;bottom: -5px;">
                                          ${iterator.date} at ${iterator.time} 
                                        </span>
                                    </a>
                                    </p>
                                </div>
                              </li>`;
                              $(".userchatData").append(html);
                  }else if (iterator.attachment == true && iterator.attachmentType == "msword") {
                    html = `<li class="sent">
                                <div class="wrap">
                                    <div class="circle" style ="background :${randomColorGenerator(userName[0].toUpperCase())}">
                                      <div class="chat_name">
                                      <h2 class="chat_name_head"> ${userName[0].toUpperCase()}</h2>
                                      </div>
                                    </div>
                                    <p>
                                        <a href= ${iterator.attachmentLink} target="_blank">
                                            <img src ="/dist/img/word_icon.png" style="width:100%"> 
                                            <span class="date_time" 
                                                  style="position: relative;
                                                  text-decoration: none;
                                                  color: black;
                                                  font-size: 12px; display: block;
                                                  text-align: right;bottom: -5px;">
                                              ${iterator.date} at ${iterator.time} 
                                            </span>
                                        </a>
                                    </p>
                                </div>
                              </li>`;
                              $(".userchatData").append(html);
                  }else if(iterator.text.trim()!=""){
                  html = `<li class="sent">
                              <div class="wrap">
                                <div class="circle" style ="background :${randomColorGenerator(userName[0].toUpperCase())}">
                                    <div class="chat_name">
                                      <h2 class="chat_name_head"> ${userName[0].toUpperCase()}</h2>
                                    </div>
                                </div>
                              <p>
                                ${iterator.text}
                                <span class="date_time" style="position: relative;
                                    font-size: 12px; display: block;
                                    text-align: right;bottom: -5px;">
                                    ${iterator.date} at ${iterator.time} 
                                </span>
                              </p>
                              </div>
                          </li>`;
                  $(".userchatData").append(html);
                }
              }
              }
              $(".new-content").removeClass("col-sm-9").addClass("col-sm-6");
              $("#frame .content .contact-profile").css({ background: "#fff" });
              $(".contact-profile h2").css({ display: "block" });
              $(".detail_hide").css({ display: "block" });
              $(".messages").animate({ scrollTop: $(".messages").prop("scrollHeight") },0);
      
              //***on click of user show message  at bottom that user asked for agent  */
              let  userarray = [];
              $(".gotNotificationClass").each(function () {
                userarray.push($(_this).attr("data-userid"));
              });
              let NewUserarray = [...new Set(userarray)];
              let selected_user_id = localStorage.getItem("selected_user_id");
              if(NewUserarray.includes(selected_user_id)){
                let agentName = getAgentName();
                socket.emit("UserNeedAgentMessageToAgent", {
                  userId: selected_user_id,
                  message: `User asked for human agent`,
                  control: "yes",
                  clientId: getClientFromSession(),
                  agentName:agentName,
                  avatar: "https://helloyubo.com/avatar.png"
                });
              }
            } else {
              console.log("Error occured", data);
            }
          },
          error: function (jqXHR, textStatus, err) {
            alert("Please refresh !");
          },
        });
    }else{
        let  createdat   =  $(_this).attr("createdat");
        let data ={
          createdAt   :  createdat,
          sent_to     :  userName
        }
        $.ajax({
          type: "POST",
          url: "/getSentUserTemplate",
          data: data,
          success: function (data) {
            //console.log('data: 33333333333333333', data)
            if (data.statusCode == 200) {
                 let  createdAt   =  data.data.createdAt,
                  format_Date =  moment(new Date(createdAt)).format("DD/MM/YYYY"),
                  date        =  format_Date,
                  now         =  moment(new Date(createdAt)),
                  time        =  now.hour() + ":" + now.minutes();
                  msg=replaceParams(data.data.text,data.data.parameters);
                  
                //***********append the media*********//
                if(data.data.msg_type == 'VIDEO'){
                      $(".userchatData").html("");
                      html = ` <li class="replies">
                                    <img src="/dist/files/favicon.png" alt="favicon">
                                    <div class="users_chat" style:"float:right">
                                    <p style="">
                                    <a href= ${data.data.media_url} target="_blank">
                                      <video width="220" height="140" controls>
                                        <source src= ${data.data.media_url} type="video/mp4">
                                      </video>
                                    </a>
                                </p>
                                <p>
                                    ${msg}
                                    <span class="date_time" 
                                          style="position: relative;font-size: 12px; display: block; text-align: right;bottom: -5px;">
                                          ${date} at ${time} 
                                    </span>
                                </p>

                                    </div>
                                  
                              </li>`;
                    $(".userchatData").append(html);
                }else if (data.data.msg_type == 'IMAGE'){
                    $(".userchatData").html("");
                      html = ` <li class="replies">
                                    <img src="/dist/files/favicon.png" alt="favicon">
                                  <div class="users_chat">
                                    <p class="users-images" style="background : ">
                                    <a href= ${data.data.media_url} target="_blank">
                                      <img src = ${data.data.media_url} style="width:100%;border-radius: 10px;"> 
                                    </a>
                                </p>
                                <p class="users-mesgs">
                                    ${msg}
                                    <div class="date_time" 
                                          style="position: relative;font-size: 12px; display: block; text-align: right;bottom: -5px;color:white;padding-right:15px;">
                                          ${date} at ${time} 
                                    </div>
                                </p>

                                </div>
                                  
                              </li>`;
                    $(".userchatData").append(html);
                }else{
                  $(".userchatData").html("");
                  html = ` <li class="replies">
                                <img src="/dist/files/favicon.png" alt="favicon">
                              <div class="users_chat" style="float:right;">
                                <p class="users-images" style="background : ">
                                    <a href= ${data.data.media_url} target="_blank">
                                      <img src = "./dist/img/file.png" style="width:100%"> 
                                    </a>
                                </p>
                                <p class="users-mesgs">
                                    ${msg}
                                    <div class="date_time" 
                                          style="position: relative;font-size: 12px; display: block; text-align: right;bottom: -5px;color:white;    padding-right: 10px;">
                                          ${date} at ${time} 
                                    </div>
                                </p>
                              </div>
                                
                          </li>`;
                $(".userchatData").append(html);
                }
              //************************************//
            } else {
              console.log("Error occured", data);
            }
          },
          error: function (jqXHR, textStatus, err) {
            alert("Please refresh !");
          },
        });


          $("#user_name").val("");
          $("#user_email").val("");
          $("#user_phone").val("");
          $(".userNotesDiv").hide();

          $("#checkboxSlider").hide();
          $(".send-message").hide();
          $("#takeControlLabe ").hide();
          $("#checkBoxBtn").prop("checked",false);
          localStorage.setItem("selected_user_id", '');
    }

    
}

function findCampaignUserList(tempData){
  $.ajax({
    data: tempData,
    method: "POST",
    url: "/campaignUserListForAgent",  
    success: function (data) {
      skip     = data.skip;
      limit    = data.limit;
      docCount = data.docCount;
      if(docCount > 0){
        $( "#loadMoreButtonId" ).show();
      }else{
        $( "#loadMoreButtonId" ).hide();
      }
      $(".userList").empty();
      // $(".campaignTempltDropdown").empty();
      $(".tempChatList").empty();
     if(data.statusCode==200){
      // $(".campList").html(""); 
      campUsersArray = [];
      campUsersArray = data.data.map( ele => ele.chatId );
      for( const iterator of data.data){
         let  html,msg,name = iterator.sent_to,createdAt,format_Date,date,now ,time;
        // const timeStamp=iterator.createdAt;
        // const dateObj= new Date(timeStamp);
        // const displayDateTime=`${dateObj.toLocaleDateString()} ${dateObj.toLocaleTimeString()}`;
        //console.log('iterator.lastChatData:', iterator.lastChatData)
        if(iterator.lastChatData === null || iterator.lastChatData == 'null'){
          msg = replaceParams(iterator.tempMsg,iterator.parameters);
          createdAt   =  iterator.createdAt;
        }else{
          msg = iterator.lastChatData.text;
          createdAt   =  iterator.lastChatData.createdAt;
        }
        format_Date =  moment(new Date(createdAt)).format("DD/MM/YYYY"),
        date        =  format_Date,
        now         =  moment(new Date(createdAt)),
        time        =  now.hour() + ":" + now.minutes();
        let src=getsrc(iterator.chatId).src;
      html = `<li class="contact campUser selectuser ${src}" 
                  data-clientid = "${iterator.clientObjId}" 
                  data-userId   = "${iterator.chatId}" 
                  msg_type      = "${iterator.msg_type}"
                  name          = "${name}"
                  createdAt     = "${iterator.createdAt}"
              >
                    <div class="wrap camp-circles">
                      <div class="circle" style ="background :${randomColorGenerator(name[0])}">
                        <div class="chat_name">
                         <h2 class="chat_name_head"> ${name[0]}</h2>
                         </div>
                       </div>
                       <div class="meta">
                         <p class="name" style="font-weight:bold;">${name}</p>
                        <p class="preview" data-chatid="${iterator.chatId}" style="padding-top:5px;">${msg}</p></div>
                     </div>
                     <div style="    text-align: end;
                                     padding-right:15px;
                                     padding-top:10px;
                     ">
                        <small style="position:relative;left:">  ${date} at ${time} </small>
                     </div>
                    </li>`;
          $(".campList").append(html);
      }
         
     } 
      else {
        alert(data.msg);
      }
    },error: function (jqXHR, textStatus, err) {  
    alert("something went wrong,please try again")
    }
});
}

/****************************************************************************
 *Replace template message curley bracess {{1}} in their respective parameters.
 ***************************************************************************/
function replaceParams(msg, params){
  let count=1;
  if(params && params.length>0){
      params.forEach(function(item){
          msg = msg.replace("{{"+count+"}}",item);
          count ++;
      })
  }
  return msg;
}

// agent take control template ajax
function callingTemplate (){
  let clientId = $("#client_Id").val();
  try{
  let selected_user_id = localStorage.getItem("selected_user_id");
  selected_user_id.toString();

  let startValueCheck = selected_user_id.startsWith('wa');
  let userMobileNumber;
  userMobileNumber = selected_user_id.substr(5,10);
  
  $.ajax({
  type:"get",
  url:'/agent/send-template-data',
  data : {"clientId":clientId},
  success : function(resp){
    if(resp.template == false){
      console.log('No template is selected.')
    }
   
let data ={
  "clientId":resp.client_id,
  "send_to" : userMobileNumber,
  "msg" : (resp.template.text) ? resp.template.text : "",
  "msg_type":(resp.template.msg_type) ? resp.template.msg_type : "",
  "lang":(resp.template.lang) ? resp.template.lang : "",
  "header":(resp.template.header) ? resp.template.header : "",
  "footer":(resp.template.footer) ? resp.template.footer : "",
  "buttonUrlParam":(resp.template.button_url) ? resp.template.button_url : "",
  "button":(resp.template.button) ? resp.template.button : "",
  "media_url":(resp.template.media_url) ? resp.template.media_url : "",
  "parameters":(resp.template.parameters) ? resp.template.parameters :"",
  "parametersArrayObj": (resp.template.parametersArrayObj) ? resp.template.parametersArrayObj :"",
  "templateName" : resp.template.name,
  }
    if(startValueCheck){
      $.ajax({
        type:'post',
        contentType: 'application/json',
        dataType: 'json',
        url:'https://campaign.helloyubo.com/sendWaCampaignToParticularUser',
        data:JSON.stringify(data),
        success: function(response){
          if(response.statusCode == 201){
            console.log('No template is selected')
          }
            if(response){
              if(response.statusCode === 200){
                swal({
                      title: 'Template send successfully.',
                      icon: "success",
                      timer: 3000,
                    }); 
                  }
            }else{
              swal({
                title: 'Template send Failed.',
                icon: "error",
                timer: 3000,
              }); 
            }
        }
      })
    }
  }
})
}catch(err){
  console.log(err)
  swal({
    title: 'Some error occurs.Please try again',
    icon: "error",
    timer: 3000,
  }); 
}
}


function getsrc(userid) {
    if (!userid) {
        return false;
    }
    let src = userid.split("_")[0];
    switch (src) {
        case "wa":
            return { "src": src, "title": "WhatsApp" }
            break;
        case "fb":
            return { "src": src, "title": "Facebook Messenger" }
            break;
        case "gbm":
            return { "src": src, "title": "Google Business Messages" }
            break;
        case "tg":
            return { "src": src, "title": "Telegram Messenger" }
            break;
        case "ig":
            return { "src": src, "title": "Instagram" }
            break;
        case "android":
            return { "src": src, "title": "Android App" }
            break;
        case "ios":
            return { "src": src, "title": "iOS App" }
            break;
        default:
            return { "src": "web", "title": "Website" };
            break;
    }
}

//##############################################################################
// IF AGENT IS ONLINE BUT NOT DOING ANYTHING ACTIVITY TO KEEP 
//THE ACHENT ACTIVE EMIT AGENT ID EACH TIME TO REMAIN THE AGENT ACTIVE 
//If agent panel is open active the user each 30Sec
//##############################################################################

setInterval(activeAgentEachMinutes, 30000);
function activeAgentEachMinutes() {
  console.log('Active the user--',new Date());
  socket.emit("agent_online", {
    agentId: getAgentIdFromSession(),
    clientId: getClientFromSession(),
  });
}
// Human Agent if agent want template directly then starts with # 
let debounceTimer;

function handleInput(event) {
  clearTimeout(debounceTimer);

  const textMessage = event.target.value;

  if (textMessage.startsWith('#')) {
    const templateName = textMessage.slice(1);

    debounceTimer = setTimeout(() => {
      fetch('/api/template', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          text: templateName,
          clientId: getClientFromSession(),
        }),
      })
        .then(response => response.json())
        .then(data => {
          console.log('API response:', data); 
          if (data.message === 'Successfully get the data!' && Array.isArray(data.sentences)) {
            showPopup(data.sentences);
          } else {
            hidePopup();
          }
        })
        .catch(error => {
          console.error('An error occurred:', error);
        });
    }, 800); 
  } else {
    hidePopup();
  }
}

function showPopup(sentences) {
  const messageList = document.getElementById('messageList');
  messageList.innerHTML = '';

  // Create list items for the sentences
  sentences.forEach(sentence => {
    const listItem = document.createElement('li');
    listItem.textContent = sentence;
    listItem.addEventListener('click', () => {
      selectMessage(sentence);
    });
    messageList.appendChild(listItem);
  });

  const popup = document.getElementById('popup');
  popup.style.display = 'block';
}

function hidePopup() {
  const popup = document.getElementById('popup');
  popup.style.display = 'none';
}

function selectMessage(message) {
  const textMessage = document.getElementById('textMessage');
  textMessage.value = message;
  hidePopup();
}

function removeTags(str) {
    if ((str===null) || (str===''))
        return false;
    else
        str = str.toString();
          
    // Regular expression to identify HTML tags in
    // the input string. Replacing the identified
    // HTML tag with a null string.
    return str.replace( /(<([^>]+)>)/ig, '');
}