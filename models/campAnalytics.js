var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
    mongoose.connect(require('./connection-string'));
}

//message info  model schema.
var newSchema = new Schema({
    tempName: {
        type:String,
        default:""
    },
    createdOn:{
        type:Date,
        default:Date.now()
    },
    sent_to:{
        type:String,
        default:""
    },
    text:{
        type:String,
        default:""
    },
    msg_type:{
        type:String,
        default:""
    },
    clientId: {
        type: String,
        default:""
    },
    clientObjId: {
        type: Schema.Types.ObjectId,
        // mongoose.ObjectId 
        default:""
    },
    sentStatus:{
        type: String,
        enum:['success','failed','In-queue','sent','delivered','read','responded'],
        default:'In-queue'
    },
    errorReason:{
        type:  Schema.Types.Mixed,
        default : ""
    },
    statuses : {
        type : Array ,
        default : []
    },
    messageId:{
        type:String,
        default:""
    },
    chatId:{
        type:String,
        default:""
    },
    createdAt:{
        type:Date,
        default:Date.now()
    },
    updatedAt:{
        type:Date,
        default:Date.now()
    },
    campaignId:{
        type:String,
        default:""
    },
    groupName:{
        type : String ,
        default : ""
    },
    media_url:{
        type : String ,
        default : ""
    },
    parameters:{
        type : Array ,
        default : []
    }
    
})

newSchema.index({tempName:1,groupName:1,chatId:1,campaignId:1,messageId:1,clientId:1})

module.exports = mongoose.model('campAnalytics', newSchema);