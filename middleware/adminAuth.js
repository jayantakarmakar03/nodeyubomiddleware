const jwt = require('jsonwebtoken');
const db = require('../models/all-models');
module.exports=async(req, res, next) => {
let authHeader = req.get('Authorization');
if(!authHeader){
	return res.status(422).json({
	  status:'failed',
	  message:'Not Authenticated!'
	});
}
const token = authHeader.split(' ')[1];
 console.log("token", token);
let Admin=await db.Superadmin.findOne({authToken:token});
 console.log("Admin", Admin)
if(!Admin){
	return res.status(200).json({
	  status:'sessionExpired',
	  message:'Your session has expired please login again!'
	});
}
let decodedToken;
try{
  decodedToken = jwt.verify(token, 'YuboSecretKey');
}catch (err){
    return res.status(500).json({
	  status:'failed',
	  message:err
	});
}
if(!decodedToken) {
	return res.status(422).json({
	  status:'failed',
	  message:'Not Authenticated!'
	});
}
req.user = decodedToken.id;
next();
};



