const db = require("../models/all-models");

module.exports = {
    stopPromotion:stopPromotion,
    getStopPramotionNumber:getStopPramotionNumber,
    deleteStopPramotionNumber:deleteStopPramotionNumber
}


async function stopPromotion(req,res) {

    try {
        const optOutData = await db.optOutList(req.body)
        await optOutData.save()
        return res.json({
            statusCode: 201,
            data:optOutData,
            message: "Phone number is opt out successfully"
        });
        
    } catch (error) {
        return res.json({
            statusCode: 500,
            message: "Phone number is not opt out",
            error:error
        }); 
    }
}

async function getStopPramotionNumber(req,res) {

    const clientId = req.user.clientId
    try {
        const optOutList = await db.optOutList.find({clientId:clientId})
        return res.json({
            statusCode: 200,
            data:optOutList,
            message: "Opt out list find successfully"
        });
        
    } catch (error) {
        return res.json({
            statusCode: 500,
            message: "Opt out list is not find successfully",
            error:error
        }); 
    }

}
async function deleteStopPramotionNumber(req,res) {

    const clientId = req.user.clientId
    const id = req.body.id

    try {
        const optOutList = await db.optOutList.findOneAndDelete({clientId:clientId,_id:id})
        return res.json({
            statusCode: 200,
            data:optOutList,
            message: "Mobile number is delete successfully"
        });
        
    } catch (error) {
        return res.json({
            statusCode: 500,
            message: "Mobile number is not delete",
            error:error
        }); 
    }

}
