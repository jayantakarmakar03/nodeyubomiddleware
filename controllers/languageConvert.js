const path = require('path')
const i18n = require('i18n')
const db = require("../models/all-models")
const axios = require('axios');
const util = require('util');
const XLSX = require('xlsx');
const fs = require("fs")

module.exports = {
    changeLanguage,
    readLanguage,
    convertExcelToJson
  };
async function changeLanguage(data){
    try{
        let clientsLanguage = await db.Client.findOne({ clientId: data.client_id },{ locales : 1 });
        let clientlang ;
        if(clientsLanguage === null || clientsLanguage  =='null'){
            clientlang =['en'];
        }else{
            clientlang = clientsLanguage.locales;
        }

        i18n.configure({
            locales: clientlang ,
            directory: process.env.pythonDir+data.client_id+"/locales"
          });
        i18n.setLocale(data.session.lang);

        if(data.session.lang!="en"){
            let phrase=[]
            let translated_text=i18n.__(data.text)
            if(data.text==translated_text && data.text.trim()!=""){
                phrase.push(data.text)
            }
            if(data.replies.length>0){
               data.replies.forEach(async element => {
                    let translated_option=i18n.__(element.option)
                    if(element.option==translated_option && element.option.trim()!=""){
                        phrase.push(element.option)
                    }
                });
            }
            if (data.tree != undefined && data.tree != 'false' && data.tree.products != undefined && data.tree.products.length > 0) {
                data.tree.products.forEach(async product => {
                    let translated_title=i18n.__(product.title)
                    let translated_description=i18n.__(product.description)
                    if(product.title==translated_title && product.title.trim()!=""){
                        phrase.push(product.title)
                    }
                    if(product.description==translated_description && product.description.trim()!=""){
                        phrase.push(product.description)
                    }
                });
            }
            if(phrase.length>0){
                await updateTranslation(data.client_id,phrase,data.session.lang)
            }
        }

        if (data.textParams !== undefined) {
            data.text = i18n.__(data.text, data.textParams);
        } else {
            data.text = i18n.__(data.text);
        }

        if(data.replies.length>0){
            data.replies.forEach(element => {
                element.option = i18n.__(element.option);
            });
        }
        if (data.tree != undefined && data.tree != 'false' && data.tree.products != undefined && data.tree.products.length > 0) {
            data.tree.products.forEach(product => {
                product.title = i18n.__(product.title);
                product.description = i18n.__(product.description);
            });
        }
    } catch(err){
        console.log("error",err)
    }
    return data;
    
}


async function updateTranslation(client_id,phrase,lang){
    try{
        await axios({
          method: "POST",
          url: process.env.updateTranslationAPI,
          data: {"client_id":client_id,"phrase":phrase,"lang":lang}
        }).then(async function (ress) { 
            let body =ress.data
            console.log('response from  updateTranslation API',body);
        });
    }catch(err){
        console.log("Error calling updateTranslation API",err)
    }
}


const readFile = util.promisify(fs.readFile);

async function readLanguage (req, res) {
    try {
        const clientData = await db.Client.findOne({ clientId: req.user.clientId }, { locales: 1, _id:0, clientId:1 });
  let ele = clientData.locales;
  let client = req.user.clientId;
  const result = {}
  const langResult = ele.map(async ele => {
    const data = await readFile(`${process.env.pythonDir}/${client}/locales/${ele}.json`, 'utf-8');

    let sentense = JSON.parse(data);
    const lang = ele;

    if(!Object.keys(result).length) {
      for(let key in sentense) {
          result[key] = {
              [lang]: sentense[key]
          }
      }
    } else {
      for(let key in sentense) {
          if(result[key]) {
            result[key][lang] = sentense[key];
          }
      }
    }

    return result
  });

  let formatLanguage = await Promise.all(langResult);
  let finalResult = Object.values(formatLanguage[0]);
 
  const wb = XLSX.utils.book_new();
  const ws = XLSX.utils.json_to_sheet(finalResult);
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  const excelFile = XLSX.write(wb, { bookType: 'xlsx', type: 'buffer' });

  res.setHeader('Content-disposition', 'attachment; filename=Translation.xlsx');
  res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  res.send(excelFile);
    } catch (error) {
        console.log(error)
    }
  
};




const writeFile = util.promisify(fs.writeFileSync);


async function convertExcelToJson(req, res) {
  try {
    console.log("=====function is being fired============");
    const clientData = await db.Client.findOne({ clientId: req.user.clientId }, { locales: 1, _id: 0, clientId: 1 });
    let ele = clientData.locales;
    let client = req.user.clientId;
    const workbook = XLSX.readFile(req.file.path);
    const sheetNames = workbook.SheetNames;
    let Data = XLSX.utils.sheet_to_json(workbook.Sheets[sheetNames[0]]);
    let result = {};

    // Validate the Excel format
    const expectedColumns = ele;
    const isValidFormat = Data.every(row => expectedColumns.every(column => column in row));

    if (!isValidFormat) {
      deleteFile(req.file.path);

      return res.status(400).send({
        message: "Invalid Excel format..",
      });
    }

    for (let i = 0; i < Data.length; i++) {
      let obj = Data[i];
      for (let lang in obj) {
        if (!result[lang]) {
          result[lang] = {};
        }
        result[lang][obj["en"]] = obj[lang];
      }
    }
    console.log(result);
    await ele.map((ele) => {
      writeFile(`${process.env.pythonDir}/${client}/locales/${ele}.json`, JSON.stringify(result[ele]));
    });

    deleteFile(req.file.path);

    res.status(200).send({
      status: 200,
      message: "Successfully updated into JSON!",
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      status: 500,
      message: "An error occurred while converting Excel to JSON.",
    });
  }
}

// Function to delete a file
function deleteFile(filePath) {
  fs.unlink(filePath, (err) => {
    if (err) {
      console.error(err);
    } else {
      console.log('File deleted successfully');
    }
  });
};