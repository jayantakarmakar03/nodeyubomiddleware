const express = require('express');
const router = express.Router();
const humanAgentController = require('../controllers/humanAgent');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const secretKey = process.env.secretKey;
const multer = require("multer");
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/uploads");
  },
  filename: function (req, file, cb) {
    let extArray = file.mimetype.split("/");
    let extension = extArray[extArray.length - 1];
    cb(null, file.fieldname + "-" + Date.now() + "." + extension);
  },
});
var upload = multer({ storage: storage });
const WhatsAppTemplate = require('../models/whatsAppTemplate');
const ClientModel = require('../models/Client');

var cors = require('cors');
router.use(cors());
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});
router.post('/add-human-agent', isLoggedIn, (req, res) => {
  humanAgentController.addHumanAgent(req, req.user.clientId, (result) => {
    res.json(result);
  });
});
router.post('/view-all-human-agent', isLoggedIn, (req, res) => {
  humanAgentController.viewHumanAgent(req.user.clientId, (result) => {
    res.json(result);
  });
});
router.post('/delete-human-agent', isLoggedIn, (req, res) => {
  humanAgentController.deleteHumanAgent(req, req.user.clientId, (result) => {
    res.json(result);
  });
});
router.post('/update-human-agent', isLoggedIn, (req, res) => {
  humanAgentController.updateHumanAgent(req.body, req.user.clientId, (result) => {
    res.json(result);
  });
});
router.get('/view-agent', isLoggedIn, (req, res) => {
  humanAgentController.viewParticularAgent(req.query, req.user.clientId, (result) => {
    res.json(result);
  });
});
router.post('/agentUserList', checkTokenMiddleware, (req, res) => {
  humanAgentController.agentUserList(req.body, (result) => {
    res.json(result)
  })
})
router.post('/agentUserChat', (req, res) => {
  humanAgentController.agentUserChat(req.body, (result) => {
    res.json(result)
  })
})
router.post('/block-unblock-agent', isLoggedIn, (req, res) => {
  humanAgentController.blockUnblockAgent(req.body, req.user.clientId, (result) => {
    res.json(result);
  });
});

router.get("/agentPanel", async function (req, res) {
  res.render("agentPanel.hbs")
})

router.post('/agentLogin', (req, res) => {
  humanAgentController.login(req.body, (result) => {
    res.json(result)
  });
});
router.post('/getUserDetailsByToken', checkTokenMiddleware, (req, res) => {
  res.json(req.decoded);
});

router.get("/agentLogin", async function (req, res) {
  res.render("agentLogin.hbs")
})
router.get("/agentForgotPassword", async function (req, res) {
  res.render("agentForgotPassword.hbs")
})
router.post('/agentTakeControl', (req, res) => {
  humanAgentController.agentTakeOrLeaveControl(req.body, (result) => {
    res.json(result);
  });
});

router.post('/agentTakencontrolsCount', (req, res) => {
  humanAgentController.agentTakencontrolsCount(req.body, req.user.clientId, (result) => {
    res.json(result);
  });
});

router.post('/myChatsUserList', (req, res) => {
  humanAgentController.myChatsUserList(req.body, (result) => {
    res.json(result);
  });
});
router.post('/agentDataSave', (req, res) => {
  humanAgentController.agentDataSave(req, (result) => {
    res.json(result);
  });
}); 
router.post('/v2_agentSendMessageApi', (req, res) => {
  humanAgentController.v2_agentSendMessageApi(req, (result) => {
    res.json(result);
  });
});
router.post('/v2_agentTakeControlApi', (req, res) => {
  humanAgentController.v2_agentTakeControlApi(req, (result) => {
    res.json(result);
  });
});
router.post('/v2_agentLeaveControlApi', (req, res) => {
  humanAgentController.v2_agentLeaveControlApi(req, (result) => {
    res.json(result);
  });
});
router.post('/saveAgentAttachment',upload.single("fileList"), (req, res) => {
  humanAgentController.saveAgentAttachment(req,(result) => {
    res.json(result);
  });
});
router.post('/leaveAllControlsOnLogout', (req, res) => {
  humanAgentController.leaveAllControlsOnLogout(req.body, req.body.clientId, (result) => {
    res.json(result);
  });
});

/***  Search Route  ***/
router.post('/searchData', (req, res) => {
  humanAgentController.searchData(req.body, req.body.clientId, (result) => {
    res.json(result);
  });
});

router.post('/agentListByClientId',(req,res)=>{
  humanAgentController.agentListByClientId(req.body,req.body.clientId,(result)=>{
    res.json(result);
 });
});
router.post('/changeAgentpassword',(req,res)=>{
humanAgentController.changeAgentpassword(req.body,(result)=>{
  res.json(result);
})
})
router.post('/updateAgentPassword',checkTokenMiddleware,(req,res)=>{
  req.body.decoded = req.decoded ;
  humanAgentController.updateAgentPassword(req.body,req.headers.authtoken,(result)=>{
    res.json(result);
  })
})
/**** * get active bot in manage agentcard ******/
router.post('/activeBotCount',(req,res)=>{
  humanAgentController.activeBotCount(req.body,req.body.clientId,(result)=>{
    res.json(result);
 });
});
/**** update user data  ******/
router.post('/updateUserData',(req,res)=>{
  humanAgentController.updateUserData(req.body,(result)=>{
    res.json(result);
 });
});
/**** generate token api for bot used in client.js file   ******/
router.post('/generateToken',(req,res)=>{
  humanAgentController.generateToken(req.body,(result)=>{
    res.json(result);
 }); 
});
router.post('/agent-list-by-clientId', (req, res) => {
  humanAgentController.agent_List_By_ClientId_for_agentpanel(req.body,(result) => {
    res.json(result);
  });
});
/*******get total handled unique user by agents  ******************************/
router.get('/getHandledUserByAgent',isLoggedIn, (req, res) => {
  humanAgentController.getHandledUserByAgent(req,(result) => {
    res.json(result);
  });
});
//*** get agent and user total count  of conversation **********/
router.get('/userAgentConversation',isLoggedIn, (req, res) => {
  humanAgentController.userAgentConversation(req,(result) => {
    res.json(result);
  });
});

router.post('/getClientData', (req, res) => {
  humanAgentController.getClientData(req,(result) => {
    res.json(result);
  });
});

router.post('/addTicket', (req, res) => {
  humanAgentController.addTicket(req,(result) => {
    res.json(result);
  });
});
router.post('/getTicketById', (req, res) => {
  humanAgentController.getTicketById(req,(result) => {
    res.json(result);
  });
});
router.post('/getRepliesByTicketId', (req, res) => {
  humanAgentController.getRepliesByTicketId(req,(result) => {
    res.json(result);
  });
});
router.post('/addRepliesOnTicket', (req, res) => {
  humanAgentController.addRepliesOnTicket(req,(result) => {
    res.json(result);
  });
});
router.post('/updateRepliesById', (req, res) => {
  humanAgentController.updateRepliesById(req,(result) => {
    res.json(result);
  });
});
router.post('/deleteRepliesById', (req, res) => {
  humanAgentController.deleteRepliesById(req,(result) => {
    res.json(result);
  });
});

//  *****************************************************************************//
//   Middleware to check agent user token is valid or not
//  *****************************************************************************//

function checkTokenMiddleware(req, res, next) {
  var token = req.body.authtoken || req.query.authtoken || req.headers['authtoken'];
  console.log('token====>', token)
  if (token) {
    jwt.verify(token, secretKey, function (err, decoded) {
      if (err) {
        res.json({
          statusCode: 400,
          message: "Session timeout! Please login again.",
          res: err
        });
      } else {
        req.decoded = decoded;
        console.log('verified')
        next();
      }
    });
  } else {
    res.json({
      statusCode: 201,
      message: "Please provide authtoken with the request "
    });
  }
}


//  *****************************************************************************//
//  *****************************************************************************//
// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
  if (req.user) return next();
  res.redirect("/agent/agentlogin");
}

// send template data to agent
router.get("/send-template-data", async function (req, res) {
  try{
  let client_id = req.query.clientId;
  let clientData = await ClientModel.findOne({clientId:client_id});
  let template;
  if(clientData.agentTemplate == ""){
    template = false;
  }else{
    template = await WhatsAppTemplate.findOne({_id:clientData.agentTemplate});
  }
  res.json({template,client_id});
  }catch(err){
    res.status(200).json({status: "failed"});
  }
})




module.exports = router;