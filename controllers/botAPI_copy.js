const db = require("../models/all-models"),
    Hubspot = require("./hubspot"),
    axios = require("axios");

module.exports = {
    sendMessage: sendMessage,
    checkForClient: checkForClient,
    checkForUser: checkForUser
};

async function sendMessage(req, res) {
    let client = await checkForClient(req.body.data.client_id);
    await saveUpdateBotRespInDb(req.body.data, client);
    let userData = await checkForUser(req.body.data.userId, client._id);
    let pythonResp = await sendMessageToPython(req, res);
    await updatePythonResponseIndb(req, res, pythonResp, userData);
}

async function sendMessageToPython(req, res) {
    //Sending request to python
    let data;
    try {
        data = await axios({
            method: "post",
            url: process.env.pythonUrl,
            data: { data: req.body.data },
        });
    } catch (err) {
        console.log("error " + err);
        return res.json({
            success: false,
            message: errorMessage,
            type_option: true,
        });
    }
    return data.data;
}

//Function to update the bot messages
async function updatePythonResponseIndb(req, res, pythonData, userData) {
    let dateTime = new Date(), date = dateTime;
    date.setSeconds(date.getSeconds() + 2);
    let response_source =
        pythonData.tree !== "false"
            ? "tree"
            : pythonData.intent == "basic_yubo"
                ? "story"
                : "intent";

    if (userData) {
        if (userData.session && pythonData.session) {
            if (
                JSON.stringify(userData.session.email) ===
                JSON.stringify(pythonData.session.email) &&
                JSON.stringify(userData.session.phone) ===
                JSON.stringify(pythonData.session.phone)
            ) {
                pythonData.session.createdDate = pythonData.session.createdDate;
            } else {
                pythonData.session.createdDate = new Date();
            }
        }

        await db.Userchat.updateOne(
            { userId: req.body.data.userId, clientId: userData.clientId },
            {
                $addToSet: {
                    chats: {
                        text: pythonData.text,
                        ignoreMsg: false,
                        messageType: "outgoing",
                        replies: pythonData.replies,
                        type_option: pythonData.type_option,
                        possible_conflict: pythonData.possible_conflict,
                        intent: pythonData.intent,
                        response_source: response_source,
                        score: pythonData.score,
                        node: req.body.data.node,
                        createdAt: date,
                    },
                },
                $set: {
                    session: pythonData.session,
                    node: pythonData.default_opt,
                    date: dateTime,
                },
            }
        );
    }

    //checking for the webhook and the action key to hit the hubspot API
    let resp = ((req.body.webhook.action == "true" || req.body.webhook.action) && "webhook" in req.body) ? await checkForWebhook(req.body) : false;
    pythonData.session["visitor_id"] = resp;

    return res.json(pythonData);
}

//webhook checking for the respective client and calling the create and update API based on Visitor's id
async function checkForWebhook(data) {
    let webhookDetails = await db.Webhook.findOne({
        clientId: data.data.client_id,
        webhookName: { $regex: data.webhook.name, $options: "$i" },
    });

    if (webhookDetails) {
        //Call the update API, if visitor's id is present in the request.body webhook
        if (data.webhook.data.visitor_id && (data.webhook.data.visitor_id != false)) {
            console.log('vid ', data.webhook.data.visitor_id);
            await Hubspot.updateCntctOfflineByVid(webhookDetails.webhookApiKey, data.webhook.data);
            return data.webhook.data.visitor_id;
        } else {  //create new entry in hubspot and return visitor id
            console.log('novid');
            let resp = await Hubspot.createNewCntctOffline(webhookDetails.webhookApiKey, data.webhook.data);
            return (resp && resp.vid) ? resp.vid : false;
        }
    }
}

async function saveUpdateBotRespInDb(botRespData, client) {
    let user = await checkForUser(botRespData.userId, client._id),
        date = new Date();

    if (!user) {
        //If user doesn't exist, create a new user
        let saveData = await new db.Userchat({
            userId: botRespData.userId,
            clientId: client._id,
            chats: [
                {
                    text: botRespData.text,
                    messageType: "incoming",
                    replies: [],
                    typing: true,
                    session: botRespData.session,
                    node: botRespData.node,
                    createdAt: date,
                    date: date,
                    node: "root",
                },
            ],
        });
        await saveData.save();
    } else {
        //If user exist, update the user details
        await db.Userchat.updateOne(
            { userId: botRespData.userId, clientId: client._id },
            {
                $addToSet: {
                    chats: {
                        text: botRespData.text,
                        messageType: "incoming",
                        replies: [],
                        node: botRespData.node,
                        type_option: true,
                        createdAt: date,
                    },
                },
            }
        );
    };
    return
}

async function checkForClient(clientId){
    let client = await db.Client.findOne({
        clientId: clientId,
    });

//Client not exist message
if (!client) {
    return res.json({
        success: false,
        message: "Client does not exists",
        type_option: true,
    });
}else{
    return client
}
}

//Checking the user associated with client Id
async function checkForUser(userId, clientId) {
    return await db.Userchat.findOne({
        userId: userId,
        clientId: clientId,
    });
}
