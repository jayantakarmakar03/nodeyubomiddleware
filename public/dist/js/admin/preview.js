var cssdata = {}
var customizedata = ""

//Action on Page Load
$(document).ready(function () {
  cssdata = { headbackgroundcolor: "rgb(15, 115, 173)", headtext1: "rgb(255, 255, 255)", headtext2: "rgb(255, 255, 255)", closebuttoncolor: "rgb(255, 255, 255)", speechbubble: "rgb(255, 255, 255)", chatbottext: "rgb(51, 51, 51)", userspeech: "rgb(255, 255, 255)", usertext: "rgb(51, 51, 51)", botborder: "rgb(53, 152, 219)", messagechatscreeen: "rgba(0, 0, 0, 0)", iconbackgroundcolor: "rgb(15, 115, 173)"}
  customizedata = ".icon {background:" + cssdata.iconbackgroundcolor + ";}.contact-profile{  background: " + cssdata.headbackgroundcolor + ";}#frame .content .message-input .wrap button{ background: " + cssdata.iconbackgroundcolor + ";}.tag-line.sub{color:" + cssdata.headtext1 + " ;}.tag-line{color: " + cssdata.headtext1 + ";}.clo']se-img svg {fill:" + cssdata.closebuttoncolor + ";}#frame .content .messages ul li.sent p{background:" + cssdata.speechbubble + ";}#']frame .content .messages ul li.sent p{ color:" + cssdata.chatbottext + ";}#']frame .content .messages ul li.replies p{background:" + cssdata.userspeech + ";}#f']rame .content .messages ul li.replies p{color:" + cssdata.usertext + ";}.iframe-box {border: 1px solid " + $('.testborder').val() + ";}#frame{background:" + cssdata.messagechatscreeen + ";}";
  /****************************/
  /****************************/
  $.ajax({
    url: '/templateload',
    type: 'POST',
    data: '',
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
      if (data) {
        $('.chatboad_head').css({ 'background-color': data.data.botcss.headbackgroundcolor });
        $('.icon_head').css({ 'background-color': data.data.botcss.iconbackgroundcolor });
        $('#enter_btn_bot').css({ 'color': data.data.botcss.iconbackgroundcolor });
        $('#result1').css({ 'color': data.data.botcss.headtext1 });
        $('#result2').css({ 'color': data.data.botcss.headtext2 });
        $('#cross_head').css({ 'color': data.data.botcss.closebuttoncolor });
        $('#botwidget_background').css({ 'background-color': data.data.botcss.speechbubble });
        $('#bottext_bg').css({ 'color': data.data.botcss.chatbottext });
        $('#userBg_bg').css({ 'background-color': data.data.botcss.userspeech });
        $('#usertext_bg').css({ 'color': data.data.botcss.usertext });
        $('.botBorder').css({ 'border-color': data.data.botcss.botborder });
        $('#down').css({ 'background-color': data.data.botcss.messagechatscreeen });
        $('#powered_sec').css({ 'background-color': data.data.botcss.messagechatscreeen });
        $('#message-input').css({ 'background-color': data.data.botcss.messagechatscreeen });
        $('#result1').text(data.data.botcss.headtextfirst);
        $('#result2').text(data.data.botcss.headtextsecond);
        $('#source1').val(data.data.botcss.headtextfirst)
        $('#source2').val(data.data.botcss.headtextsecond)
        if (data.data.logoupload != '') {
          $('.deleteicon').removeClass('hide')
          $('#upload_filename').html(data.data.logoupload)
        }
      } else if (data.data == "database blank") {
        console.log('database blank')
      }
    },
    error: function () {
      alert("error in ajax form submission");
    }
  });
});

$('.deleteicon').click(function () {
  if ($('#upload_filename').html() != '') {
    $('#upload_filename').html('')
    $('#logo_img').attr('src', '/dist/img/chatbubble.png')
    $('.deleteicon').addClass('hide')
  }
})

//Action on Preview Button Click
$('#somelink').on('submit', function (e) {
  var url = $('#websiteUrl').val()
  var clientId = $('#clientID').val();
  let preview_url="http://preview.helloyubo.com";
  if(url.indexOf("https://") == 0){
    preview_url="https://preview.helloyubo.com";
  }
  $('#somelink').attr('action', preview_url+"?url=" + url + "&username=" + clientId)
});

//Action on Save Button Click
$('.bot_save').click(function () {
  let headerTextData = $('#result1').text();
  let jsonobject = { headbackgroundcolor: $('.chatboad_head').css('background-color'), headtext1: $('#result1').css('color'), headtext2: $('#result2').css('color'), closebuttoncolor: $('#cross_head').css('color'), speechbubble: $('#botwidget_background').css('background-color'), chatbottext: $('#bottext_bg').css('color'), userspeech: $('#userBg_bg').css('background-color'), usertext: $('#usertext_bg').css('color'), botborder: $('.botBorder').css('borderTopColor'), messagechatscreeen: $('#down').css('background-color'), headtextfirst: $("<div>").text(headerTextData).html(), headtextsecond: $('#result2').text(), iconbackgroundcolor: $('.icon_head').css('background-color')}

  var file = $('#botlogo')[0].files;
  var formData = new FormData();
  formData.append('cssdata', JSON.stringify(jsonobject))
  if ($('#upload_filename').html() != '') {
    formData.append('profile_img', file[0])
  }
  $.ajax({
    url: '/botsavepreview',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
      if (data) {
        alert("Style changes saved successfully!")
        // location.reload();
      }
    },
    error: function () {
      alert("error in ajax form submission");
    }
  });
  return false;
})
