var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
  mongoose.connect(require('./connection-string'));
}
var newSchema = new Schema({
    'clientId' : { type:String },
    "errorData": { type:String },
    "nodeName" : { type:String },
    "userId"   : { type:String },
    'createdAt': { type: Date, default: Date.now },
    'updatedAt': { type: Date, default: Date.now }
});
module.exports = mongoose.model('codeNodeLogs', newSchema);