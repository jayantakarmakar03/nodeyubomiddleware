  //integrations

  $(".integration").click(function () {
    let id = '#' + $(this).attr("modal-id"),
      name = $(this).attr("name");
    integration(id, name, "", "false")  //false flag is set to indicate normal checking of APIKey exist or not
  })

  $("#save-apikey").submit(function () {
    let name = document.getElementById("intgrtn-name").value //$("#intgrtn-name").attr("value"),
    apiKey = document.getElementById("apikey").value //$("#apikey").attr("value")
    integration("", name, apiKey, "true")  //true flag is to indicate the data is going to save the APIKey
  })

  async function integration(id, name, apiKey, flag) {
    $.ajax({
      type: "post",
      url: "/integration",
      data: { name: name, apiKey: apiKey, flag: flag },
      timeout: 20000,
      success: async function (resp) {
        if (resp == "Configured" || resp == "Saved") {
          await alert(resp)
          window.location.href = "/integration"
        } else if (resp == "not_saved") {
          await alert("There is some problem while saving the API key, please try again later")
          location.reload();
        } else {
          $("#intgrtn-name").attr("value", name)
          alert("API key is required for further connection")
          $(id).modal('show');
        }
      },
      error: function (jqXHR, textStatus, err) {
        alert('Something went wrong, please refresh the page')
      }
    })
  }