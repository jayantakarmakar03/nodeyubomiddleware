document.onreadystatechange = function(e){
    if ((document.readyState === 'interactive' || document.readyState === 'complete') && document.getElementById("bootm-box")==undefined){
		(function() {
			console.log('Bot loaded');
			const domain = "https://yubo.yugasa.org"
			// const domain = "https://localhost:4900"
			let host = location.hostname;
			let url=location.href;
			let storageData =encodeURIComponent(window.localStorage.getItem('storageData'))

			console.log("storageData",storageData)

			const clientId="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6InRlc3RfY2hhdGJvdCIsImlhdCI6MTYwMjk0OTg4N30.SLIZ-2ZiTtv6XVt9YuUC16pmMWqm79Nb8LyK6FPWMN4";

			var parent = document.createElement('div');
			parent.setAttribute('class', 'iframe-box');
			parent.setAttribute('id', 'iframe-box');

			var bootm = document.createElement('div');
			bootm.setAttribute('class', 'bootm-box');
			bootm.setAttribute('id', 'bootm-box');

			var chatstart = document.createElement('div');
			chatstart.setAttribute('class', 'chat-start bounce-top');

			var closeimg = document.createElement('div');
			closeimg.setAttribute('class', 'close-img');
			closeimg.innerHTML='<svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg>'

			var outercircle = document.createElement('div');
			outercircle.setAttribute('class', 'outerCircle');

			var innercircle = document.createElement('div');
			innercircle.setAttribute('class', 'innerCircle');

			var icon = document.createElement('div');
			icon.setAttribute('class', 'hy-icon');

			var bubbleimg  = document.createElement('img');
			bubbleimg.setAttribute('class', 'bubble-img');
			bubbleimg.src = domain+"/img/chat-bubble.svg";

			icon.appendChild(bubbleimg);

			chatstart.appendChild(outercircle)
			chatstart.appendChild(innercircle)
			chatstart.appendChild(icon)

			bootm.appendChild(chatstart);
		    bootm.appendChild(closeimg);
		    var iframe = document.createElement('iframe');
		   	iframe.setAttribute('id', 'frame-lode');
		    iframe.src = domain+"/chatWindow?clientId="+clientId+"&host="+host+"&url="+url + "&storageData=" + storageData;
		    parent.appendChild(bootm);
		    parent.appendChild(iframe);
		    document.body.appendChild(parent);
			document.getElementById("bootm-box").addEventListener("click",function(){
				document.getElementById("iframe-box").classList.toggle("open-chat-window");
				console.log('Bot opened');
			});
			launchBot();

// // Make the DIV element draggable:
// dragElement(document.getElementById("iframe-box"));

// function dragElement(elmnt) {
//   var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
//   if (document.getElementById("frame-lode")) {
//     // if present, the header is where you move the DIV from:
//     document.getElementById("frame-lode").onmousedown = dragMouseDown;
//   } else {
//     // otherwise, move the DIV from anywhere inside the DIV:
//     elmnt.onmousedown = dragMouseDown;
//   }

//   function dragMouseDown(e) {
//     e = e || window.event;
//     e.preventDefault();
//     // get the mouse cursor position at startup:
//     pos3 = e.clientX;
//     pos4 = e.clientY;
//     document.onmouseup = closeDragElement;
//     // call a function whenever the cursor moves:
//     document.onmousemove = elementDrag;
//   }

//   function elementDrag(e) {
//     e = e || window.event;
//     e.preventDefault();
//     // calculate the new cursor position:
//     pos1 = pos3 - e.clientX;
//     pos2 = pos4 - e.clientY;
//     pos3 = e.clientX;
//     pos4 = e.clientY;
//     // set the element's new position:
//     elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
//     elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
//   }

//   function closeDragElement() {
//     // stop moving when mouse button is released:
//     document.onmouseup = null;
//     document.onmousemove = null;
//   }
// }
		})();
    }
};

function launchBot() {
	// First check, if localStorage is supported.
	if (window.localStorage) {
		// Get the expiration date of the previous popup.
		var nextPopup = localStorage.getItem( 'nextyubo' );

		if (nextPopup > new Date()) {
			return;
		}
		// Store the expiration date of the current popup in localStorage.
		var expires = new Date();
		expires = expires.setHours(expires.getHours() + 24);

		localStorage.setItem( 'nextyubo', expires );
	}

	setTimeout(function(){document.getElementById("bootm-box").dispatchEvent(new Event('click'));},5000)
};
