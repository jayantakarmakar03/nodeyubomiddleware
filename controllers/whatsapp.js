const jwt = require("jsonwebtoken");
const db = require('../models/all-models');
const mongo = require('mongodb');
const axios = require('axios');
const createBot = require('whatsapp-cloud-api').createBot;
const ObjectID = mongo.ObjectID;
const escapeHtml = require('escape-html');
var AWS = require('aws-sdk');
var unirest = require('unirest');
var fs = require('fs');
let emailverifycontroller = require("./emailverification.js");
const excel = require("exceljs");
const secretKey = process.env.secretKey;

module.exports = {
    getWhatsappMsg             : getWhatsappMsg,
    addUser                    : addUser,
    viewUserById               : viewUserById,
    updateUser                 : updateUser,
    addMultipleUser            : addMultipleUser,
    getSubscriberList          : getSubscriberList,
    deleteUser                 : deleteUser,
    getWhatsappUserList        : getWhatsappUserList,
    createWhatsappTemp         : createWhatsappTemp,
    updateWhatsappTemp         : updateWhatsappTemp,
    getWhatsappTempById        : getWhatsappTempById,
    deleteWhatsappTemp         : deleteWhatsappTemp,
    deleteCloudWhatsappTemp    : deleteCloudWhatsappTemp,
    whatsappTemplateList       : whatsappTemplateList,
    whatsappCloudTemplateList  : whatsappCloudTemplateList,
    updateGupshupConfig        : updateGupshupConfig,
    onBoardWABA                : onBoardWABA,
    getGupshupConfig           : getGupshupConfig,
    optInOptoutUser            : optInOptoutUser,
    sendWhatsAppCampaign       : sendWhatsAppCampaign,
    sendWhatsappCampToMultiUser: sendWhatsappCampToMultiUser,
    setMultiUserOptin          : setMultiUserOptin,
    sendAgentControlRequest    : sendAgentControlRequest,
    updateStatusByMsgId        : updateStatusByMsgId,
    getCampaignAnalytics       : getCampaignAnalytics,
    getCampSpecificData        : getCampSpecificData,
    updateTimerConfig          : updateTimerConfig,
    getTimerConfigData         : getTimerConfigData,
    deleteMultipleUser         : deleteMultipleUser,    
    uploadMedia                : uploadMedia,
    sendWaCampaignFromSpreadsheet : sendWaCampaignFromSpreadsheet,
    sendWaCampaignToParticularUser : sendWaCampaignToParticularUser,
    getTokenList                   : getTokenList,
    generateToken                  : generateToken,
    deleteTokenById                : deleteTokenById,
    updateClientApiSecret          : updateClientApiSecret,
    deleteCampAnalytics            : deleteCampAnalytics,
    campaignUserListForAgent       : campaignUserListForAgent,
    findTempLists                  : findTempLists,
    getSentUserTemplate            : getSentUserTemplate,
    findHumanAgent                 : findHumanAgent,
    currentLogedInClientData       : currentLogedInClientData,
    getAllSubscriberList           : getAllSubscriberList,
    createNewWhatsappTemp          : createNewWhatsappTemp,
    uploadWhatsappMedia            : uploadWhatsappMedia,
    deleteAccountRequest           : deleteAccountRequest,
    getDeleteRequestStatus         : getDeleteRequestStatus,
    assignAgentToTemplate          : assignAgentToTemplate,
    getClientDataByClientId        : getClientDataByClientId,
    getGroupDataByGroupName        : getGroupDataByGroupName,
    findOptOutUserList             : findOptOutUserList,
    findTemplateData               : findTemplateData,
    updateWACampStatus             : updateWACampStatus,
    getUserByPhoneNumber           : getUserByPhoneNumber
}

async function getWhatsappMsg(req, res) {
    var resp;
    try {
        await jwt.verify(req.headers.authtoken, process.env.WASECRETKEY);
        resp = {
            statusCode: 200,
            status: "success",
            data: req.body
        }
    }
    catch (err) {
        resp = {
            statusCode: 200,
            status: "failed",
            data: err.message
        }
    }
    res.send(resp)
}
async function addUser(req,res,next){
    checkForToken(req.user._id).then(async function(response) {
        req.body.name = escapeHtml(req.body.name);
        try{   
            let client = await db.Client.findOne({ clientId: req.user.clientId });
            if (!client) {
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: "Client doesn't exist"
                });
            }
            if(req.body.phone){
                let user = await db.Users.findOne({
                    clientId: req.user.clientId, phone: req.body.phone
                });
                if (user) {
                    return res.json({
                        statusCode: 201, 
                        status: false,
                        message: "Phone number already registered please try again",
                        data : user
                    });
                }
            }
            if(req.body.email){
                let user = await db.Users.findOne({
                    clientId: req.user.clientId, email: req.body.email
                });
                let userChatEmailData = await db.Userchat.findOne({
                    clientId: req.user._id, "session.email": req.body.email
                });
                if (user || userChatEmailData ) {
                    return res.json({
                        statusCode: 201, 
                        status: false,
                        message: "Email already registered please try again",
                        data : user
                    });
                }
            }
                if (req.body.phone || req.body.email) {
                    let userInfo = new db.Users({
                        clientId: req.user.clientId,
                        name: req.body.name ? req.body.name : "",
                        phone: req.body.phone ? req.body.phone : null,
                        email: req.body.email ? req.body.email : null,
                        whatsappConsent: req.body.whatsappConsent ? req.body.whatsappConsent : false
                    })
                    await userInfo.save();
                    return res.json({
                        statusCode: 200,
                        status: true,
                        message: "User created successfully"
                    });
                } else {
                    return res.json({
                        statusCode: 201,
                        status: false,
                        message: "Phone or email is required"
                    });
                }
        }catch(err){
            console.log('error occured==',err)
               return res.json({
                        statusCode: 201,
                        status: false,
                        message: err
                    });
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })
}


//*****get user data by id ,search in specefic table to find user ,because we have two table user, userchat******/
async function viewUserById(req,res,next){
    checkForToken(req.user._id).then(async function(response) {
        try{
            if(req.query.source == 'userchat'){
            let userlist = await db.Userchat.findOne({_id : req.query.userId },{session:1,formdata:1 });
                  let  data={};
                      data.email=  userlist.session.email;
                      data.name =  userlist.session.name;
                      data.phone=  userlist.session.phone;
                      data._id  =  userlist._id;
               return res.json({
                   statusCode: 200,
                   status: true,
                   message: "User list",
                   data : data
               });
            }else{
               let userlist = await db.Users.findOne({ _id : req.query.userId },{email:1,phone:1,name:1});
               if(userlist){
                   return res.json({
                       statusCode: 200,
                       status: true,
                       message: "User list",
                       data : userlist
                   });
               }
            }
       }catch(error){
           console.log('error',error)
           return res.json({
               statusCode: 201,
               status: false,
               message: error
           });
       }
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })

}
// *****update the user data email ,phone etc bit we have to tablel user and userchat ***//
async function updateUser(req,res,next){
    checkForToken(req.user._id).then(async function(response) {
        req.body.name = escapeHtml(req.body.name);
        try{
            let client = await db.Client.findOne({ clientId: req.user.clientId });
            if (!client) {
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: "Client doesn't exist"
                });
            }
            if(req.body.source == 'userchat'){
                let data = req.body; 
                db.Userchat.updateOne(
                    { _id: data.userId },
                    {
                        $set: {
                               'session.name' : data.name,
                               'session.email' : data.email,
                               'session.phone' : data.phone,
                        }
                    },function(err,result){
                        if(err){
                            return res.json({
                                statusCode: 201,
                                status: false,
                                message: "INTERNAL DB ERROR",
                                res : err
                            })
                        }else{
                            return res.json({
                                statusCode: 200,
                                message: "User data updated successfully",
                                status: true,
                            });
                        }
                });
            }else{
                let user = await db.Users.findOne({
                    _id: req.body.userId
                });
                if (user) {
                    let userInfo = {
                        name: req.body.name ? req.body.name : null ,
                        phone: req.body.phone,
                        email: req.body.email,
                        whatsappConsent: req.body.whatsappConsent ? req.body.whatsappConsent : user.whatsappConsent,
                        updatedAt: Date.now()
                    }
                    
                    await db.Users.updateOne({
                        _id: req.body.userId
                    },
                        { $set: userInfo },function(err,result){
                            if(err){
                                return res.json({
                                    statusCode: 201,
                                    status: false,
                                    message: 'INTERNAL DB ERROR',
                                    error : err
                                });
                            }else{
                                return res.json({
                                    statusCode: 200,
                                    status: true,
                                    message: "User data updated successfully"
                                });
                            }
                        });
                }else{
                    return res.json({
                        statusCode: 201,
                        status: false,
                        message: `User not found with this id : ${req.body.userId}`
                    });
                }
            }
           
    
        }catch(error){
            console.log('error',error)
        } 
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
    });
}
//*****add excel data of user to add in db******/
async function addMultipleUser(req,res,next){ 
    checkForToken(req.user._id).then(async function(response) {
        try{
            //*************check the group name already exist or not***************************************//
            let groupName = `createdgroup.`+`${req.body.filename}` 
            let checkGroupNameExists = await db.Group.findOne({ [groupName] : { $exists : true },clientId : req.user._id });

            if(checkGroupNameExists){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: `Group name already exists. Please rename the excel sheet. Excel Sheet name is taken as the new group name`,
                });
            }

            //*************Check client had gupshup credentials set or not otherwise through a error *****************************//

            let clientData = await db.Client.findOne(
                {clientId : req.user.clientId},
                { gupShupConfig:1, clientId:1});
    
            let gupshupUserId ;
            let gupshupPassword;
            if(clientData.gupShupConfig){
                gupshupUserId =  clientData.gupShupConfig.userId;
                gupshupPassword = clientData.gupShupConfig.password;
            }
            if(!clientData.gupShupConfig || !clientData.gupShupConfig.userId || !clientData.gupShupConfig.password ){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: 'It seems your WABA account setup is not complete yet , Hence your are not allowed to add user and create groups , Please contact Yugasa Bot team for help. '
                });
            }

            //*************check email or phone already exist in db return an array if already exist in db***************************************//
            let clientId = req.body.userArray[0].clientId;
            let excelUserEmail = req.body.userArray.map(ele=>ele.email);
            let getExistingUser =await db.Users.find({clientId : clientId, email : { $in : excelUserEmail }},{email:1});
            let existingUserMailsArray = getExistingUser.map(ele=>ele.email);
           
    
            let excelUserPhone = req.body.userArray.map(ele=>ele.phone);
            let getExistingUserbyPhone =await db.Users.find({clientId : clientId, phone : { $in : excelUserPhone }},{phone:1});
            let existingUSerByPhone = getExistingUserbyPhone.map(ele=>ele.phone);
           
            //*************insert non existing users in user collection***************************************//
            let excelUsersArray = req.body.userArray ;
            let newUsers = excelUsersArray.filter(ele=>{
                 if(existingUserMailsArray.includes(ele.email) == false && existingUSerByPhone.includes(ele.phone) == false)
                 return ele;
            });
            
            let adduser = await db.Users.insertMany(newUsers);

            // calculate numbers of users not being added due to existing email or phone 
            let countNewUserAdded = newUsers.length;
            let totalNumberOfUserInExcell = excelUsersArray.length;
            let numberOfRejectedUser =totalNumberOfUserInExcell - countNewUserAdded;

            //*************optin multiple user***************************************//
          let optinedNumberList =[];
          let notOptinedNum =[];
          if(clientData.gupShupConfig.accountType == 'Gupshup'){ 
            let promiseArray = [];
            await excelUserPhone.forEach(element=>{
                let tempObj = {
                    gupshupUserId  : gupshupUserId,
                    gupshupPassword: gupshupPassword,
                    phone_number   : element,
                }
                promiseArray.push(callGupshupApiForOneUser(tempObj,'OPT_IN'));
    
            })
           await Promise.allSettled(promiseArray).then((promiseReturnArray) => {
                // let optinedNumberList =[];
                // let notOptinedNum =[];
                // console.log('promiseReturnArray 1111',promiseReturnArray);
    
                promiseReturnArray.filter(ele=>{
    
                    if(ele.status == 'fulfilled'){
                        optinedNumberList.push(ele.value.phone);
                    }else{
                        notOptinedNum.push(ele.reason.phone);
                    }
                });
    
                 db.Users.update(
                        {"phone": {$in: optinedNumberList}},
                        {$set:{"opt_in": true}},
                        {multi: true},
                        (err, data) =>{
                            console.log('updated user optIn true in db for multiple user',data)
                        });
                // console.log(`listOfOptinNumbers === ${optinedNumberList} ,listOfNotOptinNumbers  === ${notOptinedNum} `)
            //   return res.json({
            //         statusCode: 200,
            //         status: true,
            //         message: 'user optined successfully',
            //         data : {'listOfOptinNumbers':optinedNumberList ,'listOfNotOptinNumbers': notOptinedNum }
            //     });
            }).catch((error) => {
               console.log("error occured in promise all catch block", error);
                return res.json({
                    statusCode: 201,
                    status: true,
                    message: 'Error occured during optin user',
                    error : error
                });
            });
          }
          
          //***********create group as filename *****************//

           let groupUserArray = [];
           let count = 0;
            await req.body.userArray.filter(ele=>{
                const d = new Date();
                count++;
                let userId = d.getTime() + count ;
                let obj={};
                obj['mail'] = ele.email;
                obj['name'] = ele.name;
                obj['phone'] = ele.phone;
                obj['userId'] = userId;
                groupUserArray.push(obj);
            });
            let groupFinalData = {
                [req.body.filename] : groupUserArray
            };
            let data = new db.Group({
                clientId: req.user._id,
                groupName: req.body.filename,
                createdgroup: groupFinalData
            });
            await data.save();

           return res.json({
                statusCode: 200,
                status: true,
                message: "User Added successfully",
                getExistingUser:getExistingUser,
                data:{
                    optIned_number : optinedNumberList.length,
                    not_optIned_number : notOptinedNum.length,
                    numberOfRejectedUser :numberOfRejectedUser,
                    accountType :clientData.gupShupConfig.accountType
                }
           });
        }catch(error){
        console.log('error in catch block : \n',error);
            return res.json({
                statusCode: error.code,
                status: false,
                message: error.errmsg,
                error : error
            });
        }       
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
    });

}

async function getSubscriberList(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            let clientData = await db.Client.findOne({clientId: req.body.clientId },{_id:1,userchatsToUsersMerged:1});
            // UserchatUSerList = await db.Userchat.find({clientId: clientData._id},{session:1,formdata:1 ,_id:0,createdAt:1,updatedAt : 1});
            USerList = await db.Users.find({clientId: req.body.clientId},{name:1,email:1,phone:1,_id:0});
           let clientId = req.body.clientId;
           if(clientData.userchatsToUsersMerged == false){
             UserchatUSerList = await db.Userchat.aggregate([
                { $match:{ clientId:  clientData._id } },
                { "$group": {
                    "_id": {
                        "email": "$session.email",
                        "phone": "$session.phone",
                        "name": "$session.name"
                    },
                    "userCount": { "$sum": 1 }
                }},
                { "$sort": { "count": -1 } },
                { "$project": { session : "$session",createdAt:"$createdAt"}}
             ])
             await UserchatUSerList.forEach(ele=>{ 
               if((ele._id  && 
                   ele._id.email != '' && 
                  Array.isArray(ele._id.phone) ==  false && 
                  Array.isArray(ele._id.email) ==  false && 
                  ele._id.email != null && 
                  ele._id.email != 'undefined' && 
                  ele._id.phone !=  null && 
                  ele._id.phone != 'undefined' )||
                  (ele._id  && 
                   ele._id.phone != '' && 
                  Array.isArray(ele._id.phone) ==  false && 
                  Array.isArray(ele._id.email) ==  false && 
                  ele._id.email != null && 
                  ele._id.email != 'undefined' && 
                  ele._id.phone !=  null && 
                  ele._id.phone != 'undefined' )
                  )
                addusertosubscriber(ele._id,clientId);
              });
           }
          let search = `${req.body.searchItem}`
           if(req.body.skip == undefined ){
              req.body.skip = 0;
           }else{
              req.body.skip = parseInt(req.body.skip);
           }
           if(req.body.limit == undefined ){
              req.body.limit = 10;
           }else{
              req.body.limit = parseInt(req.body.limit);
           }

            let docCount = await db.Users.count({
                clientId: req.user.clientId,
                $or: [
                        {
                            "email":{$regex: search ,'$options': 'i'}
                        },
                        {
                            "phone":{$regex: search ,'$options': 'i'}
                        }, 
                        {
                            "name":{$regex: search ,'$options': 'i'}
                        }
                ]
            });

            let userModelUserList = await db.Users.find({
                   clientId: req.user.clientId,
                    $or: [
                            {
                                "email":{$regex: search ,'$options': 'i'}
                            },
                            {
                                "phone":{$regex: search ,'$options': 'i'}
                            }, 
                            {
                                "name":{$regex: search ,'$options': 'i'}
                            }
                    ]
                }, {  email: 1, phone: 1, name: 1 , "source" :"users",opt_in :1 }).sort({createdAt : -1}).skip(req.body.skip).limit(req.body.limit);

            await db.Client.updateOne(
                    { clientId : req.body.clientId },
                    { $set : {userchatsToUsersMerged : true }});

            return res.json({
                    statusCode : 200,
                    status     : 'success',
                    data       : userModelUserList,
                    limit      : req.body.limit,
                    skip       : req.body.skip,
                    docCount   : docCount
                })
        }catch(error){
            console.log('error',error)
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
    })
}
async function addusertosubscriber(ele,clientId){
         
    let user_exists = await db.Users.findOne({
        clientId : clientId,
        email    : ele.email,  
        phone    : ele.phone
    });

    if(user_exists){
        // console.log('ddata exist  in db');
    }else{
        let userdata = new db.Users({
            "clientId"  : clientId,
            "name"      : ele.name,
            "phone"     : ele.phone,
            "email"     : ele.email
        });
        addUser = await userdata.save();
        return addUser;
    }
 }
async function deleteUser(req, res) {
    checkForToken(req.user._id).then(async function (response) {
        try {
            if (req.body.source == 'userchat') {
                let deleteUser = await db.Userchat.deleteOne({ _id: req.body.userId });

                return res.json({
                    statusCode: 200,
                    status: true,
                    message: "User deleted successfully ",
                    data: deleteUser
                });
            } else {
                let userDb = await db.Users.findOne({ _id: req.body.userId });
                let groupData = await db.Group.find({ clientId: req.user._id });

                let data = {};
                for (let i = 0; i < groupData.length; i++) {
                    data = Object.keys(groupData[i].createdgroup);
                    let Id = groupData[i]._id;
                    let existingdata = groupData[i].createdgroup[data]
                         for (let j = 0; j < existingdata.length; j++) {
                             if (existingdata[j].phone == userDb.phone) {
                                existingdata.splice(j, 1)
                                 let temp = {
                                     [data]: existingdata
                                 };
    
                                let upateddata = await db.Group.updateOne({ _id: Id }, { $set: { createdgroup: temp } })
                            }

                        }
                }

                let deleteUser = await db.Users.deleteOne({ _id: req.body.userId });
                return res.json({
                    statusCode: 200,
                    status: true,
                    message: "User deleted successfully ",
                    data: deleteUser
                });
            }

        } catch (error) {
            return res.json({
                statusCode: 201,
                status: false,
                message: error
            });
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
    })
}
// **********get user list having phone number for wahtsapp campaigning  ***********************//
async function getWhatsappUserList(req, res) {
    checkForToken(req.user._id).then(async function(response) {    
        // let clientId = await db.Client.findOne({_id: req.user._id },{_id:1,clientId :1,gupShupConfig : 1 });
        // let obj ;
        // let obj2 ;
        // if(clientId.gupShupConfig && clientId.gupShupConfig.accountType == 'Gupshup'){ 
        //     obj = { clientId: req.user._id , 'session.phone' : { $ne:null },opt_in : true }
        //     obj2 =  {
        //                 $or:[{clientId: clientId.clientId },{clientId: req.user._id }],
        //                 phone : { $ne:null },
        //                 opt_in : true
        //             }
        // }
        // if(clientId.gupShupConfig && clientId.gupShupConfig.accountType == 'Yugasa'){ 
        //     obj = { clientId: req.user._id , 'session.phone' : { $ne:null }}
        //     obj2 =  {
        //         $or:[{clientId: clientId.clientId },{clientId: req.user._id }],
        //         phone : { $ne:null }
        //     }
        // }
            // let userData = await db.Userchat.find(
            //     obj, 
            //     { userId: 1, email: 1, "session.name": 1,"session.phone": 1, "session.email": 1 }
            // );
        // let userModelUserList = await db.Users.find(
        //     obj2,
        //     {  email: 1, phone: 1, name: 1 , "source" :"users" }).sort({createdAt : -1});

        // let finaldata = [];
        // if (userData.length > 0) {
        //     userData.map((element) => {
        //         let data;
        //         if (element.session && element.session.phone) {
        //             data = {
        //                 userId: element.userId,
        //                 name: element.session.name ? element.session.name : "unknown user",
        //                 phone: element.phone != null ? element.phone : element.session.phone,
        //                 group: "",
        //             };
        //             finaldata.push(data);
        //         } else if (element.phone != null && element.phone != "") {
        //             data = {
        //                 userId: element.userId,
        //                 name: "unknown user",
        //                 phone: element.phone,
        //                 group: "",
        //             };
        //             finaldata.push(data);
        //         }
        //     });
        // }
            
            // let result = [...userModelUserList,...finaldata] ;
            let fetchgroup = await db.Group.find({ clientId: req.user._id }, { createdgroup: 1 });
            return res.status(200).json({
                status: "success",
                message: "User name and email found successfully.",
                groupcreated: fetchgroup,
            });
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
    })
}
// *******whatsapp template add edit delete api below*************************//
// ***************************************************************************//

// ********create  whatsapp template*****************************************//
async function createWhatsappTemp(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            console.log('req.body 111 ', req.body);
            let fetchNametemp = await db.whatsAppTemplate.findOne({clientId : req.body.clientId, name : req.body.name});
            if(fetchNametemp){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: "Template name already exist",
                });
            }
            // let fetchTempContent = await db.whatsAppTemplate.findOne({clientId : req.body.clientId, text : req.body.text});
            // if(fetchTempContent){
            //     return res.json({
            //         statusCode: 201,
            //         status: false,
            //         message: "Template with the same content already exist",
            //     });
            // }
            let data = new db.whatsAppTemplate(req.body);
            console.log('rew body ========== ', req.body);
             data.save(function(err,result){
                if(err){
                    return res.json({
                        statusCode: 201,
                        status: false,
                        message: "INTERNAL DB ERROR",
                        error : err
                    });
                }else{
                    return res.json({
                        statusCode: 200,
                        status: true,
                        message: "Template created successfully"
                    }); 
                }
            })
        }catch(err){
            console.log('Error on catch block ' , err);
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })

}
async function updateWhatsappTemp(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            let tempData = db.whatsAppTemplate.findOne({_id : ObjectID(req.body.tempId) ,clientId : req.body.clientId });
            if (tempData) {
                let userInfo = {
                    name      : req.body.name ? req.body.name : tempData.name,
                    text      : req.body.text ? req.body.text : tempData.text,
                    msg_type   : req.body.msg_type,
                    header      : (req.body.msg_type!='TEXT')?'':req.body.header,
                    footer      : req.body.footer,
                    button       : req.body.button,
                    lang       : req.body.lang,
                    button_url  : req.body.button_url,
                    agent_Id    : req.body.agent_Id == undefined ? [] : req.body.agent_Id  ,
                    updatedAt : Date.now()
                }
                console.log('body:', req.body);
                db.whatsAppTemplate.updateOne({_id : req.body.tempId },
                    {$set : userInfo },function(err,result){
                        if(err){
                            return res.json({
                                statusCode: 201,
                                status: false,
                                message: 'INTERNAL DB ERROR',
                                error : err
                            });
                        }else{
                            return res.json({
                                statusCode: 200,
                                status: true,
                                message: "template updated successfully"
                            });
                        }
                    })
            }else{
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: `Template not found with this id : ${req.body.tempId}`
                });
            }
        }catch(err){
            return res.json({
                statusCode: 201,
                status: false,
                message: "Error on catch block",
                error : err
            });
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })

}
async function getWhatsappTempById(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            let tempData = await db.whatsAppTemplate.findOne({
                _id : ObjectID(req.body.tempId) ,
                clientId : req.body.clientId 
            },
            {
                clientId:1,
                name:1,
                approved:1,
                text:1,
                msg_type: 1,
                header:1,
                footer:1,
                agent_Id:1,
                button:1,
                button_url:1,
                lang:1
            });
            
            return res.json({
                statusCode: 200,
                status: true,
                message: "Template data fetched successfully",
                data : tempData
            }); 
    }catch(err){
        return res.json({
            statusCode: 201,
            status: false,
            message: "Error on catch block",
            error : err
        });
    }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
    })
}
async function deleteWhatsappTemp(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            db.whatsAppTemplate.deleteOne(
               { _id : ObjectID(req.body.tempId), clientId : req.body.clientId },function(err,result){
                   if(err){
                     return res.json({
                         statusCode: 201,
                         status: false,
                         message: 'INTERNAL DB ERROR',
                         error : err
                     });
                   }else{
                     return res.json({
                         statusCode: 200,
                         status: true,
                         message: "template delete successfully"
                     });
                   }
               })
        }catch(err){
             return res.json({
                 statusCode: 201,
                 status: false,
                 message: "Error on catch block",
                 error : err
             });
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })


}
async function whatsappTemplateList(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            db.whatsAppTemplate.find({clientId: req.user.clientId},function(err,result){
            if(err){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: 'INTERNAL DB ERROR',
                    error : err
                });
                }else{
                return res.json({
                    statusCode: 200,
                    status: true,
                    message: "template list found successfully",
                    data : result
                });
                }
        });
        }catch(err){
            console.log('err in catch block',err)
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })

}

async function whatsappCloudTemplateList(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            let clientData=req.user;
            if(!clientData.gupShupConfig || !clientData.gupShupConfig.userId || !clientData.gupShupConfig.password || (clientData.gupShupConfig.accountType != "Yugasa" && clientData.gupShupConfig.accountType != "AiSensy")){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: 'It seems your WABA account setup is not complete yet. Please contact Yugasa Bot team for help.'
                });
            }
            let wabaId=clientData.gupShupConfig.wabaId 
            let access_token=clientData.gupShupConfig.password


            let url="https://graph.facebook.com/v16.0/"+wabaId+"/message_templates"

            let option={
                params: {
                    access_token : access_token
                }
            }
            if(clientData.gupShupConfig.accountType=="AiSensy"){
                url="https://backend.aisensy.com/direct-apis/t1/get-templates"
                option = {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer '+access_token
                    }
                };
            }

            let templates=await fetAllTemplates(url,option)

            if(templates!=undefined && templates!=[] && templates.length>0){
                let ids=[]
                templates.forEach(async function(item){
                    ids.push(item.id);
                    await syncTemplate(req.user.clientId,item);
                });
                db.whatsAppTemplate.deleteMany({clientId: req.user.clientId,templateId:{ $nin: ids}},function(err,result){
                    if(err){
                        console.log("error ",err)
                    }else{
                        console.log("result",JSON.stringify(result))
                    }
                });
                db.whatsAppTemplate.find({clientId: req.user.clientId,templateId:{ $in: ids}},function(err,result){
                    if(err){
                        return res.json({
                            statusCode: 201,
                            status: false,
                            message: 'INTERNAL DB ERROR',
                            error : err
                        });
                        }else{
                        return res.json({
                            statusCode: 200,
                            status: true,
                            message: "template list found successfully",
                            data : result
                        });
                        }
                });
            } else {
                return res.json({
                    statusCode: 200,
                    status: true,
                    message: "No templates found",
                    data : []
                });
            }
        }catch(err){
            console.log('err in catch block',err)
            return res.json({
                statusCode: 201,
                status: false,
                message:"There was some error processing your request.",
                error : err
            });
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })

}


// ********Submit New  whatsapp template to Meta*****************************************//
async function createNewWhatsappTemp(req,res){
    checkForToken(req.user._id).then(async function(response) {
        console.log("userssssss clienr",req.user)
        let clientData=req.user
        let templateData=req.body
        try{
            if(!clientData.gupShupConfig || !clientData.gupShupConfig.userId || !clientData.gupShupConfig.password || (clientData.gupShupConfig.accountType != "Yugasa" && clientData.gupShupConfig.accountType != "AiSensy")){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: 'It seems your WABA account setup is not complete yet. Please contact Yugasa Bot team for help.'
                });
            }
            let wabaId=clientData.gupShupConfig.wabaId 
            let access_token=clientData.gupShupConfig.password


            let url="https://graph.facebook.com/v16.0/"+wabaId+"/message_templates"

            let config= {}

            console.log("templateData",templateData)

             if(clientData.gupShupConfig.accountType=="AiSensy"){
                //url="https://backend.aisensy.com/direct-apis/t1/wa_template"
                url="https://backend.aisensy.com/direct-apis/v16.0/wa_template"
                config= {
                    headers: { Authorization: 'Bearer '+access_token }
                }
            }

            await axios.post(url, {
                    name:templateData.name,
                    language:templateData.lang,
                    category:templateData.category,
                    components:templateData.components,
                    access_token : access_token
            },config).then(async function (response) {
                let template=response.data;
                let id=template.id
                let item={
                    "name":templateData.name,
                    "language":templateData.lang,
                    "status":'PENDING',
                    "id":id,
                    "components":templateData.components,
                    "category":templateData.category
                }
                await syncTemplate(req.user.clientId,item);
                return res.json({
                    statusCode: 200,
                    status: true,
                    message: "Template created successfully",
                    data : id
                });

            }).catch(function (error) {
                console.log('error in catch',error.response.data)
                let error_user_msg="There was some error processing your request."
                if(error.response.data.error!=undefined){
                    error_user_msg=error.response.data.error.message
                } else if(error.response.data.message!=undefined){
                    error_user_msg=error.response.data.message
                }
                return res.json({
                    statusCode: 201,
                    status: false,
                    message:error_user_msg,
                    error : error.response.data
                });
            });
        }catch(err){
            console.log('error in catch main',err)
            let error_user_msg="There was some error processing your request."
            if(err.message!=undefined){
                error_user_msg=err.message;
            }
            return res.json({
                statusCode: 201,
                status: false,
                message: error_user_msg,
                error : err
            });
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })

}
//*********Update Gupshup Data of client ******************************/
async function updateGupshupConfig(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            req.body.gupShupConfig.userId           = escapeHtml(req.body.gupShupConfig.userId);
            req.body.gupShupConfig.accountType      = escapeHtml(req.body.gupShupConfig.accountType);
            req.body.gupShupConfig.appId            = escapeHtml(req.body.gupShupConfig.appId);
            req.body.gupShupConfig.wabaId           = escapeHtml(req.body.gupShupConfig.wabaId);
            let gupShupConfig = req.body.gupShupConfig;

            db.Client.updateOne(
                {clientId : req.user.clientId},
                {$set:{ gupShupConfig : gupShupConfig}  },(err,result)=>{
                    if(err){
                        return res.json({
                            statusCode: 201,
                            status: false,
                            message: 'INTERNAL DB ERROR',
                            error : err
                        });
                    }else{
                        return res.json({
                            statusCode: 200,
                            status: true,
                            message: "Config update successfully",
                            data : result
                        });
                    }
                })
        }catch(err){
            console.log('err in catch block===',err)
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })

}
async function getGupshupConfig(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            db.Client.findOne(
            {clientId : req.user.clientId},{ gupShupConfig:1, clientId:1},(err,result)=>{
                if(err){
                    return res.json({
                        statusCode: 201,
                        status: false,
                        message: 'INTERNAL DB ERROR',
                        error : err
                    });
                    }else{
                    return res.json({
                        statusCode: 200,
                        status: true,
                        message: "Gupshup config fetched successfully",
                        data : result
                    });
                    }
            })
        }catch(err){
            console.log('err in catch block',err)
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })

}
async function optInOptoutUser(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
        
            let method ;
            if(req.body.opt_in == 'true' || req.body.opt_in === true )
                method = 'OPT_IN'
            else
                method = 'OPT_OUT'

            let clientData = await db.Client.findOne(
                                    {clientId : req.user.clientId},
                                    { gupShupConfig:1, clientId:1});
        
            let gupshupUserId ;
            let gupshupPassword;
            if(clientData.gupShupConfig){
                gupshupUserId =  clientData.gupShupConfig.userId;
                gupshupPassword = clientData.gupShupConfig.password;
            }
            if(!clientData.gupShupConfig || !clientData.gupShupConfig.userId || !clientData.gupShupConfig.password ){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: 'It seems your WABA account setup is not complete yet. Please contact Yugasa Bot team for help.'
                });
            }
            if(clientData.gupShupConfig && clientData.gupShupConfig.accountType == "Yugasa"){
                let data ={ opt_in : req.body.opt_in } 
                let result = await db.Users.updateOne({_id : req.body.userId},{$set : data })
                return res.json({
                    statusCode: 200,
                    status: true,
                    message:  `User ${method} successfully`,
                    data : result
                });
            }else
            {
        
            await axios.get('https://media.smsgupshup.com/GatewayAPI/rest', {
                params: {
                userid       : gupshupUserId,
                password     : gupshupPassword,
                phone_number : req.body.phone,
                method       : method,
                auth_scheme  : 'plain',
                channel      : 'whatsapp',
                format       : 'json',
                v            : 1.1
                }
            }).then(function (response) {

                if(response.data.response.status == 'success'){
                    let data ={ opt_in : req.body.opt_in } 
                    if(req.body.source == 'userchat'){
                        db.Userchat.updateOne({_id : req.body.userId},
                            {$set : data },function(err,result){
            
                                if(err){
                                    return res.json({
                                        statusCode: 201,
                                        status: false,
                                        message: 'INTERNAL DB ERROR',
                                        error : err
                                    });
                                }else{
                                    return res.json({
                                        statusCode: 200,
                                        status: true,
                                        message: `User ${method} successfully`,
                                        data : result
                                    });
                                }
                                })
                    }else{
                        db.Users.updateOne({_id : req.body.userId},
                            {$set : data },function(err,result){
                                if(err){
                                    return res.json({
                                        statusCode: 201,
                                        status: false,
                                        message: 'INTERNAL DB ERROR',
                                        error : err
                                    });
                                }else{
                                    return res.json({
                                        statusCode: 200,
                                        status: true,
                                        message:  `User ${method} successfully`,
                                        data : result
                                    });
                                }
                                })
                    }
                }
                if(response.data.response.status == 'error'){
                    return res.json({
                        statusCode: 201,
                        status: false,
                        message: response.data.response.details
                    });
                }
            }).catch(function (error) {
                console.log('error in catch',error)
                return res.json({
                    statusCode: 201,
                    status: false,
                    message:error.message,
                    error : error
                });
            })
            }
        }catch(error){
            console.log('error data ', error)
            return res.json({
                statusCode: 201,
                status: false,
                message: error
            });
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })

}
async function sendWhatsAppCampaign(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            let clientData = await db.Client.findOne(
                {clientId : req.user.clientId},
                { gupShupConfig:1, clientId:1});
            if(!clientData.gupShupConfig || !clientData.gupShupConfig.userId || !clientData.gupShupConfig.password ){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: 'It seems your WABA account setup is not complete yet. Please contact Yugasa Bot team for help.'
                });
            }
            const optedNumber = await db.optOutList.find({clientId:req.user.clientId})
            for(const obj of optedNumber){
                 if(obj.optOutNumber===req.body.send_to){
                    return res.json({
                        statusCode: 201,
                        status: false,
                        message:  "User opted out",
                    })
                 } 
            }
            if(clientData.gupShupConfig.accountType == "Yugasa" || clientData.gupShupConfig.accountType == "AiSensy" ){
                    // console.log('req.body', req.body );
                    let token ;
                    let from;
                    if(clientData.gupShupConfig){
                        from  =  clientData.gupShupConfig.userId;
                        token =  clientData.gupShupConfig.password;
                    }
                    // let  token="EAAFPoNcU2REBADF5zfeoDwZAUOQ4fXJt075oKSJM0KPRSZBhYG6dLZAHrte229o7gZCONy3DIEw4sQkc5ZAQI7Tn59FMkeSMHyFmuYhsyMaZBGg84MIRYkm06gZAXhZC46HZBOvT3ZAocHO3PaswBlmeVEXifrTq3ItQ1K4cKXj1k5tZATzlmA5ZAHWq0V9FMNCXeJVqVdXsmX0jZCwZDZD";
                    // let from = "111359084974852";
                    let paramsArray = [];
                    let component ; 
                    if(req.body.parameters && req.body.parameters.length>0){
                        await req.body.parameters.map(ele=>{
                            let obj = {
                                "type": "text",
                                "text": ele
                            };
                            paramsArray.push(obj);
                        });
                    }
                    let buttonParams ;
                    if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  ){
                        buttonParams={
                            "type": "button",
                            "sub_type" : "url",
                            "index": "0", 
                            "parameters": [
                                {
                                    "type": "text",
                                    "text": req.body.buttonUrlParam
                                }
                            ]
                        }
                    }
                    if(req.body.msg_type =="IMAGE"){
                        component = [
                            {
                                "type":"header",
                                "parameters":[
                                    {
                                        "type": "image",
                                        "image": {
                                        "link": req.body.media_url
                                        }
                                    }
                                ]
                            },
                            {
                            "type": "body",
                            "parameters": paramsArray
                            } 
                        ];
                        if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                           component.push(buttonParams);
                    }
                    if(req.body.msg_type =="VIDEO"){
                        component = [
                            {
                                "type":"header",
                                "parameters":[
                                    {
                                        "type": "video",
                                        "video": {
                                        "link": req.body.media_url
                                        }
                                    }
                                ]
                            },
                            {
                            "type": "body",
                            "parameters": paramsArray
                            } 
                        ];
                        if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                           component.push(buttonParams);
                    }
                    if(req.body.msg_type =="DOCUMENT"){
                        const urlArray = req.body.media_url.split("/");
                        let filename = urlArray[urlArray.length-1] ;
                        component = [
                        {
                            "type":"header",
                            "parameters":[
                                {
                                    "type": "document",
                                    "document": {
                                    "filename": filename,
                                    "link": req.body.media_url
                                    }
                                }
                            ]
                        },
                        {
                        "type": "body",
                        "parameters": paramsArray
                        } 
                    ];
                    if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                      component.push(buttonParams);
                    }
                    if(req.body.msg_type =="TEXT"){
                        component = [  
                            {
                            "type": "body",
                            "parameters": paramsArray
                            } 
                        ];

                        if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                          component.push(buttonParams);
                    }
                    // console.log("video temp 1111",component)
                    console.log("sending message ",req.body)
                    let lang=(req.body.lang!=undefined)?req.body.lang:'en_us';
                    console.log("sending message ",lang)
                    let result;
                    if(clientData.gupShupConfig.accountType=="AiSensy"){
                        let tempObj = {
                                from           : from,
                                token          : token,
                                send_to        : req.body.send_to,
                                msg            : req.body.msg,
                                msg_type       : req.body.msg_type,
                                button         : req.body.button,
                                media_url      : req.body.media_url,
                                header         : req.body.header,
                                footer         : req.body.footer,
                                buttonUrlParam : req.body.buttonUrlParam,
                                templateName   : req.body.templateName,
                                component      : component,
                                lang           : lang
                            }
                        result= await sendTemplate(tempObj);
                    } else{
                        const bot = createBot(from, token);
                        result = await bot.sendTemplate(req.body.send_to,req.body.templateName,lang,component);
                    }
                    console.log('result =====',result)
                    return res.json({
                                statusCode: 200,
                                status: true,
                                message:  "Message sent successfully",
                                res : result
                            });
            }else{
                       
            let Userchat = await db.Userchat.findOne(
                                    {"session.phone" : req.body.send_to},
                                    { session:1, clientId:1});
            let Usersdata = await db.Users.findOne(
                                    {phone : req.body.send_to},
                                    { phone:1, clientId:1,name : 1});
            let msg=req.body.msg;
            msg=replaceParams(msg,req.body.parameters)
            if(Userchat && Userchat.session && Userchat.session.name){
                msg = msg.replace(new RegExp("{{\\d+}}", "g"), Userchat.session.name);
            }else if(Usersdata && Usersdata.name){
                msg = msg.replace(new RegExp("{{\\d+}}", "g"), Usersdata.name);
            }else{
                msg = msg.replace(new RegExp("{{\\d+}}", "g"), 'User'); 
            }
            let gupshupUserId ;
            let gupshupPassword;
            if(clientData.gupShupConfig){
                gupshupUserId =  clientData.gupShupConfig.userId;
                gupshupPassword = clientData.gupShupConfig.password;
            }
            let params={
                userid       : gupshupUserId,
                password     : gupshupPassword,
                send_to      : req.body.send_to,
                method       : (req.body.msg_type=='TEXT' || req.body.msg_type==undefined)?'SendMessage':'SendMediaMessage',
                auth_scheme  : 'plain',
                channel      : 'whatsapp',
                format       : 'json',
                msg          : msg,
                v            : 1.1,
                msg_type     : (req.body.msg_type=='TEXT' || req.body.msg_type==undefined)?'HSM':req.body.msg_type
                }
            if((req.body.buttonUrlParam!=undefined && req.body.buttonUrlParam!='') || (req.body.button==true || req.body.button=='true') || (req.body.header!=undefined && req.body.header!='') || (req.body.footer!=undefined && req.body.footer!='')){
                params['isHSM']=true
                params['isTemplate']=true
                if(req.body.buttonUrlParam!=undefined && req.body.buttonUrlParam!=''){
                    params['buttonUrlParam']=req.body.buttonUrlParam;
                }
                if(req.body.header!=undefined && req.body.header!=''){
                    params['header']=req.body.header;
                }
                if(req.body.footer!=undefined && req.body.footer!=''){
                    params['footer']=req.body.footer;
                }
            } 
            if(req.body.msg_type!=undefined && req.body.msg_type!='TEXT' && req.body.msg_type!=''){
                params['media_url']=req.body.media_url;
                params['caption']=msg
            }
            let response = await axios.get('https://media.smsgupshup.com/GatewayAPI/rest', {
                params: params
            });
            // console.log('params',params)
            // console.log('response',response.data)
            if(response.data.response.id == 106 ){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message:  "Invalid WABA account credentials ",
                    code :response.data.response.id
                });
            }
            if(response.data.response.id == 175 || response.data.response.id == 315 || response.data.response.id == 106 || response.data.response.id == 102 ||response.data.response.id == 333 || response.data.response.id ==101 || response.data.response.status == "error"){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message:  response.data.response.details,
                    code :response.data.response.id
                });
            }else{
                return res.json({
                    statusCode: 200,
                    status: true,
                    message:  "Message sent successfully",
                    message2:  response.data.response.details,
                    code : response.data.response.id
                });
            }
          }
        }catch(error){
            console.log("req.body ",req.body)
            console.log('try catch block error error data====>> ', error)
            return res.json({
                statusCode: 201,
                status: false,
                message: error
            });
        }       
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })
}
async function sendWhatsappCampToMultiUser(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            let clientData = await db.Client.findOne(
                {clientId : req.user.clientId},
                { gupShupConfig:1, clientId:1,queueSize :1,delayTime:1});

            let gupshupUserId ;
            let gupshupPassword;
            if(clientData.gupShupConfig){
                gupshupUserId =  clientData.gupShupConfig.userId;
                gupshupPassword = clientData.gupShupConfig.password;
            }
            if(!clientData.gupShupConfig || !clientData.gupShupConfig.userId || !clientData.gupShupConfig.password ){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: 'It seems your WABA account setup is not complete yet. Please contact Yugasa Bot team for help.'
                });
            }

            const optedNumber = await db.optOutList.find({clientId:req.user.clientId})

            const arr1 = getUniqueListBy(req.body.send_to, 'phone');
            for(var i=0; i<optedNumber.length;i++) {
                var data = arr1.findIndex(obj=> obj.phone === optedNumber[i].optOutNumber)
                if(data!=-1){
                    arr1.splice(data, 1)
                }
            }
            const arr2 = getUniqueListBy(req.body.selectedUserWithGroup, 'phone');
            let selectedUserWithGroup = arr2 ; 
            let arrayOfUSerNumber = arr1 ; 

            if(clientData.gupShupConfig.accountType == "Yugasa"){
                // console.log('req.body 1111111111',req.body)
                let token ;
                let from;
                if(clientData.gupShupConfig){
                    from  =  clientData.gupShupConfig.userId;
                    token =  clientData.gupShupConfig.password;
                }
                let paramsArray = [];
                let component ; 
                if(req.body.parameters && req.body.parameters.length>0){
                    await req.body.parameters.map(ele=>{
                        let obj = {
                            "type": "text",
                            "text": ele
                        };
                        paramsArray.push(obj);
                    });
                }
                let buttonParams ;
                if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  ){
                    buttonParams={
                        "type": "button",
                        "sub_type" : "url",
                        "index": "0", 
                        "parameters": [
                            {
                                "type": "text",
                                "text": req.body.buttonUrlParam
                            }
                        ]
                    }
                }
                if(req.body.msg_type =="IMAGE"){
                    component = [
                        {
                            "type":"header",
                            "parameters":[
                                {
                                    "type": "image",
                                    "image": {
                                    "link": req.body.media_url
                                    }
                                }
                            ]
                        },
                        {
                        "type": "body",
                        "parameters": paramsArray
                        } 
                    ];
                    if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                    component.push(buttonParams);
                }
                if(req.body.msg_type =="VIDEO"){
                    component = [
                        {
                            "type":"header",
                            "parameters":[
                                {
                                    "type": "video",
                                    "video": {
                                    "link": req.body.media_url
                                    }
                                }
                            ]
                        },
                        {
                        "type": "body",
                        "parameters": paramsArray
                        } 
                    ];
                    if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                       component.push(buttonParams);
                }
                if(req.body.msg_type =="DOCUMENT"){
                    const urlArray = req.body.media_url.split("/");
                    let filename = urlArray[urlArray.length-1] ;
                    component = [
                        {
                            "type":"header",
                            "parameters":[
                                {
                                    "type": "document",
                                    "document": {
                                    "filename":filename,
                                    "link": req.body.media_url
                                    }
                                }
                            ]
                        },
                        {
                        "type": "body",
                        "parameters": paramsArray
                        } 
                    ];
                    if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                    component.push(buttonParams);
                }
                if(req.body.msg_type =="TEXT"){
                    component = [  
                        {
                        "type": "body",
                        "parameters": paramsArray
                        } 
                    ];
                    if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                    component.push(buttonParams);
                }

                const bot = await createBot(from, token);
                let lang=(req.body.lang!=undefined)?req.body.lang:'en_us';
                let sendTest =await bot.sendTemplate('917209238654',req.body.templateName,lang,component);
                
                let promiseArray = [];
                // const arr1 = getUniqueListBy(req.body.send_to, 'phone');
                // const arr2 = getUniqueListBy(req.body.selectedUserWithGroup, 'phone');
                // let selectedUserWithGroup = arr2 ; 
                // let arrayOfUSerNumber = arr1 ; 
                
                let splitedArray = await splitArrayInChunks(arrayOfUSerNumber,clientData.queueSize);         
                await myLoop(splitedArray.length, splitedArray)
                let letTimeStamp = new Date();
                                  
                 function myLoop(i,splitedArray) {
                    setTimeout(function() {
                      let ind = i-1;
                            splitedArray[ind].forEach(element=>{
                                let tempObj = {
                                    from           : from,
                                    token          : token,
                                    send_to        : element.phone,
                                    name           : element.name,
                                    msg            : req.body.msg,
                                    msg_type       : req.body.msg_type,
                                    button         : req.body.button,
                                    media_url      : req.body.media_url,
                                    header         : req.body.header,
                                    footer         : req.body.footer,
                                    buttonUrlParam : req.body.buttonUrlParam,
                                    templateName   : req.body.templateName,
                                    component      : component
                                }
                                promiseArray.push(callYugasaApiSendUserMsg(tempObj));
                            })
                            Promise.allSettled(promiseArray).then((promiseReturnArray) => {
                                if(ind == 0){
                                    let saveCampaignAnalytics = saveCampaignAnalyticsData(promiseReturnArray,req.body.templateName,selectedUserWithGroup,req.user.clientId,req.user._id,req.body.media_url,req.body.parameters,letTimeStamp);
                                    Promise.resolve(saveCampaignAnalytics);
                                    return saveCampaignAnalytics;
                                }
                            }).then((data)=>{
                                if(ind == 0){
                                    return res.json({
                                        statusCode: 200,
                                        status: true,
                                        message: 'Message sent successfully',
                                        sendMsgStatus : data
                                    });
                                }
                            }).catch((error) => {
                               console.log("error occured in promise all catch block", error);
                            });
                       if (--i) 
                       myLoop(i,splitedArray);   //  decrement i and call myLoop again if i > 0
                    }, clientData.delayTime)
                  };    
            }else{

             let promiseArray = [];
            // // let arrayOfUSerNumber = [{phone : "7209238654",name : 'jayanta'},{phone : "8726650277",name :'jayanta'}];
            // const arr1 = getUniqueListBy(req.body.send_to, 'phone');
            // const arr2 = getUniqueListBy(req.body.selectedUserWithGroup, 'phone');
            // let selectedUserWithGroup = arr2 ; 
            // let arrayOfUSerNumber = arr1 ; 


            await arrayOfUSerNumber.forEach(element=>{
                let tempObj = {
                    gupshupUserId  : gupshupUserId,
                    gupshupPassword: gupshupPassword,
                    send_to        : element.phone,
                    name           : element.name,
                    msg            : req.body.msg,
                    msg_type       : req.body.msg_type,
                    button         : req.body.button,
                    media_url      : req.body.media_url,
                    header         : req.body.header,
                    footer         : req.body.footer,
                    buttonUrlParam : req.body.buttonUrlParam
                }
                promiseArray.push(callGupshupApiSendUserMsg(tempObj,req.body.parameters));

            })
            Promise.allSettled(promiseArray).then((promiseReturnArray) => {
            let saveCampaignAnalytics = saveGupshupCampaignAnalyticsData(promiseReturnArray,req.body.templateName,selectedUserWithGroup,req.user.clientId,req.user._id);
            
            if(promiseReturnArray[0].value && promiseReturnArray[0].value.code == 333){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: promiseReturnArray[0].value.message
                });
            }
            return res.json({
                    statusCode: 200,
                    status: true,
                    message: 'Message sent successfully'
                });
            }).catch((error) => {
            console.log("error occured in promise all catch block", error);
            });
          }
        }catch(error){
            console.log('try catch block error error data====>> ', error)
            return res.json({
                statusCode: 201,
                status: false,
                message: error
            });
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access",
            error : err
        }
        res.status(responseObj.statusCode).send(responseObj)
    })
}
 async function saveCampaignAnalyticsData(sentStatus,templateName,userArray,clientId,clientObjId,media_url,parameters,letTimeStamp){
    await sentStatus.map(ele=>{
            userArray.map(item=>{
                if(item.phone == ele.value.phoneNumber ){
                    if(ele.value.status == 'fulfilled'){
                        let saveData = new db.campAnalytics({
                            clientId: clientId,
                            tempName: templateName,
                            createdOn: letTimeStamp,
                            sent_to: ele.value.phoneNumber,
                            clientObjId: clientObjId,
                            media_url : media_url,
                            parameters : parameters,
                            statuses:[],
                            sentStatus : 'In-queue',
                            messageId: ele.value.messageId,
                            campaignId: `${new Date().getTime()}_${clientId}_${templateName}_${item.groupName}`,
                            groupName:item.groupName,
                            createdAt : new Date(),
                            updatedAt : new Date(),
                        });
                        saveData.save();
                    }else{
                        let saveData = new db.campAnalytics({
                            clientId: clientId,
                            tempName: templateName,
                            createdOn: letTimeStamp,
                            sent_to: ele.value.phoneNumber,
                            clientObjId: clientObjId,
                            media_url : media_url,
                            parameters : parameters,
                            statuses:[{"status" : "failed","timeStamp" : new Date() , errorReason : ele.value.error }],
                            messageId: "",
                            errorReason : ele.value.error,
                            sentStatus : 'failed',
                            campaignId: `${new Date().getTime()}_${clientId}_${templateName}_${item.groupName}`,
                            groupName:item.groupName,
                            createdAt : new Date(),
                            updatedAt : new Date(),
                        });
                        saveData.save();
                    }
                }
            }) 
     })
 }
 async function saveGupshupCampaignAnalyticsData(sentStatus,templateName,userArray,clientId,clientObjId){
    letTimeStamp = new Date();
    await sentStatus.map(ele=>{
            userArray.map(item=>{
                if(item.phone == ele.value.response.phone ){
                    if(ele.value.response.status == 'success'){
                        let saveData = new db.campAnalytics({
                            clientId: clientId,
                            tempName: templateName,
                            createdOn: letTimeStamp,
                            sent_to: ele.value.response.phone,
                            clientObjId: clientObjId,
                            statuses:[],
                            sentStatus : 'In-queue',
                            messageId: ele.value.response.id,
                            campaignId: `${new Date().getTime()}_${clientId}_${templateName}_${item.groupName}`,
                            groupName:item.groupName,
                            createdAt : new Date(),
                            updatedAt : new Date(),
                        });
                        saveData.save();
                    }else{
                        let saveData = new db.campAnalytics({
                            clientId: clientId,
                            tempName: templateName,
                            createdOn: letTimeStamp,
                            sent_to: ele.value.response.phone,
                            clientObjId: clientObjId,
                            statuses:[{"status" : "failed","timeStamp" : new Date() , errorReason : ele.value.response.details }],
                            messageId: "",
                            errorReason : ele.value.response.details,
                            sentStatus : 'failed',
                            campaignId: `${new Date().getTime()}_${clientId}_${templateName}_${item.groupName}`,
                            groupName:item.groupName,
                            createdAt : new Date(),
                            updatedAt : new Date(),
                        });
                        saveData.save();
                    }
                }
            }) 
     })
 }
 
async function callGupshupApiSendUserMsg(data,parameters) {
    try{
        let msg=data.msg;
        msg=replaceParams(msg,parameters)
        if(data.name){
            msg = msg.replace(new RegExp("{{\\d+}}", "g"),data.name)
        }else{
            msg = msg.replace(new RegExp("{{\\d+}}", "g"),'User')
        }
        let params={
               userid       : data.gupshupUserId,
               password     : data.gupshupPassword,
               send_to      : data.send_to,
               msg          : msg,
               method       : (data.msg_type=='TEXT' || data.msg_type==undefined)?'SendMessage':'SendMediaMessage',
               auth_scheme  : 'plain',
               channel      : 'whatsapp',
               format       : 'json',
               v            : 1.1,
               msg_type     : (data.msg_type=='TEXT' || data.msg_type==undefined)?'HSM':data.msg_type,
            }
        if((data.buttonUrlParam!=undefined && data.buttonUrlParam!='') || (data.button==true || data.button=='true') || (data.header!=undefined && data.header!='') || (data.footer!=undefined && data.footer!='')){
            params['isHSM']=true
            params['isTemplate']=true
            if(data.buttonUrlParam!=undefined && data.buttonUrlParam!=''){
                params['buttonUrlParam']=data.buttonUrlParam;
            }
            if(data.header!=undefined && data.header!=''){
                params['header']=data.header;
            }
            if(data.footer!=undefined && data.footer!=''){
                params['footer']=data.footer;
            }
        }
        if(data.msg_type!=undefined && data.msg_type!='TEXT' && data.msg_type!=''){
            params['media_url']=data.media_url;
            params['caption']=msg
        }

        let result = await axios.get('https://media.smsgupshup.com/GatewayAPI/rest', {
            params: params
        })
        result.data.response.phone = data.send_to ;
        return result.data;
    }catch(err){
        console.log('Error in catch block (function name : callGupshupApiSendUserMsg) during send WA message using gupshup  ' ,err)
    }
 }
 async function callYugasaApiSendUserMsg(data) {
           try{
                const bot = await createBot(data.from, data.token);
                let lang=(data.lang!=undefined)?data.lang:'en_us';
                let result = await bot.sendTemplate(data.send_to,data.templateName,lang,data.component);
                result.phoneNumber = data.send_to;
                result.status = "fulfilled"; 
                return result;
           }catch(err){
                err.phoneNumber =data.send_to;
                err.status = "rejected"; 
                return err;
           }
 }
 async function setMultiUserOptin(req,res){
    try{
        let clientData = await db.Client.findOne(
            {clientId : req.body.clientId},
            { gupShupConfig:1, clientId:1});

        let gupshupUserId ;
        let gupshupPassword;
        if(clientData.gupShupConfig){
            gupshupUserId =  clientData.gupShupConfig.userId;
            gupshupPassword = clientData.gupShupConfig.password;
        }
        if(!clientData.gupShupConfig || !clientData.gupShupConfig.userId || !clientData.gupShupConfig.password ){
            return res.json({
                statusCode: 201,
                status: false,
                message: 'It seems your WABA account setup is not complete yet. Please contact Yugasa Bot team for help.'
            });
        }

        let promiseArray = [];
        let arrayOfUSerNumber = req.body.phoneArray;
        if(arrayOfUSerNumber == 'undefined' || arrayOfUSerNumber === undefined){
            return res.json({
                statusCode: 201,
                status: false,
                message: 'Please select users having valid phone numbers '
            });
        }
        await arrayOfUSerNumber.forEach(element=>{
            let tempObj = {
                gupshupUserId  : gupshupUserId,
                gupshupPassword: gupshupPassword,
                phone_number   : element,
            }
            promiseArray.push(callGupshupApiForOneUser(tempObj,req.body.method));

        })
        Promise.allSettled(promiseArray).then((promiseReturnArray) => {
            let optinedNumberList =[];
            let notOptinedNum =[];
            // console.log('promiseReturnArray 1111',promiseReturnArray);

            promiseReturnArray.filter(ele=>{

                if(ele.status == 'fulfilled'){
                    optinedNumberList.push(ele.value.phone);
                }else{
                    notOptinedNum.push(ele.reason.phone);
                }
            });
            let status ;
            if(req.body.method == 'OPT_IN'){
                status = true ;
            }else{
                status = false ;
            }
            
             db.Users.update(
                    {"phone": {$in: optinedNumberList}},
                    {$set:{"opt_in": status}},
                    {multi: true},
                    (err, data) =>{
                        console.log('updated user optIn true in db for multiple user',data)
                    });
            
          return res.json({
                statusCode: 200,
                status: true,
                message: 'user optined successfully',
                data : {'listOfOptinNumbers':optinedNumberList ,'listOfNotOptinNumbers': notOptinedNum }
            });
        }).catch((error) => {
           console.log("error occured in promise all catch block", error);
            return res.json({
                statusCode: 201,
                status: true,
                message: 'Error occured during optin user',
                error : error
            });
        });
    }catch(err){
        console.log('try catch block error error data====>> ', err)
        return res.json({
            statusCode: 201,
            status: false,
            message: err
        });
    }
}
function callGupshupApiForOneUser(data,method) {
    return new Promise((resolve, reject) => {

         axios.get('https://media.smsgupshup.com/GatewayAPI/rest', {
            params: {
               userid       : data.gupshupUserId,
               password     : data.gupshupPassword,
               phone_number : data.phone_number,
               method       : method,
               auth_scheme  : 'plain',
               channel      : 'whatsapp',
               format       : 'json',
               v            : 1.1
            }
        }).then(function (response) {
            response.phone =data.phone_number;
            if(response.data.response.status == 'success'){
                resolve(response);
            }else{
                reject(response);
            }
        }).catch(function (error) {
            console.log('Error in catch block1  \n',error.Error)
        })
    })
}

async function sendAgentControlRequest(req,res){
    try{
        let data = req.body ;
        const clientData = await db.Client.findOne(
            { clientId: data.clientId},
            { _id:1 });
        
     const userData = await db.Userchat.findOne(
        { userId: data.userId, clientId: clientData._id},
        { session: 1,name:1 });
        let username;
        if(!userData){
            return res.json({
                statusCode: 201,
                status: false,
                message: 'User is not found',
            });
        }
        if(userData && userData.session && userData.session.name !="" && userData.session.name != "undefined"){
            username = userData.session.name;
        }else if( userData.name && userData.name != "" && userData.name != "undefined"){
            username =userData.name
        }else{
            username = "Unknown";
        }
        let resData = {
            control     : "yes",
            userId      : data.userId,
            message: 'Need to talk to an agent ',
            name: username,
            messagetype: 'usermessage',
            clientId    : data.clientId,
        }
        let onlineAgentArray=[];
         onlineAgentArray = onlineUsers.filter(ele=>{
            if(ele.clientId == data.clientId)
            return ele;
        })
        if(onlineAgentArray.length > 0){
            io.sockets.emit('sendAgentControlRequest', resData);
            return res.json({
                statusCode: 200,
                status: true,
                message: 'data emit successfully',
                name:username
            });
        }else{
            return res.json({
                statusCode: 204,
                status: false,
                message: 'Agent is not available now',
            });
        }     
       
    }catch(err){
        console.log('lll',err)
         return res.json({
             statusCode: 201,
             status: false,
             message: "Error on catch block",
             error : err
         });
    }
 }
 async function checkForToken(user) {
    console.log("In check token function", user)
    return new Promise((resolve, reject) => {
        db.Client.find({ _id: user }, async function(error, clientData) {
            if (error) {
                reject();
            } else if (clientData) {
                let session_token = clientData[0]._doc.sessionToken
                if (session_token == "") {
                    reject();
                } else {
                    try {
                        const verified = jwt.verify(session_token, "YUGASA@BOT@123*");
                        console.log("verified", verified)
                        if (verified) {
                            resolve(true)
                        }
                    } catch (err) {
                        reject();
                    }
                }
            }
        });
    })
}

function replaceParams(msg, params){
    let count=1;
    if(params && params.length>0){
        params.forEach(function(item){
            msg = msg.replace("{{"+count+"}}",item);
            count ++;
        })
    }
    return msg;
}

async function updateStatusByMsgId(req,res){
    try{
        let errorReason = req.body.errorReason != '' ? req.body.errorReason : '';
        let obj = {
            status : req.body.status,
            timeStamp : new Date(req.body.timestamp),
        }
        let sentStatus = req.body.status;
        if(req.body.status == 'FAILED' || req.body.status =='failed'){
            obj.errorReason = errorReason
            sentStatus='failed';
        }
        let saveDataObj = { $push: { statuses: obj }}
        if(req.body.status == 'FAILED' || req.body.status =='failed'){
            saveDataObj.errorReason = errorReason ;
            saveDataObj.sentStatus = sentStatus ;
        } else {
            saveDataObj.sentStatus = sentStatus ;
        }
        let camp=await db.campAnalytics.findOne({ messageId : req.body.messageId});
        if(camp && camp.sentStatus=='responded'){
           /// await optoutUser(camp)


            return res.json({
                message: 'Status already up to date.',
                statusCode: 200,
                status: true
            });
        } if(req.body.chatId!=undefined){
            saveDataObj.chatId = req.body.chatId ;
            await db.campAnalytics.updateOne({ messageId : req.body.messageId},saveDataObj);
        } else {
            await db.campAnalytics.updateOne({messageId : req.body.messageId},saveDataObj);
        }
        return res.json({
            message: 'Status update successfully.',
            statusCode: 200,
            status: true
        });
    }catch(err){
        console.log('Error occured in catch block ',err)
         return res.json({
             statusCode: 201,
             status: false,
             message: "Error on catch block",
             error : err
         });
    }
}
async function getCampaignAnalytics(req,res){

    try{

        if(req.body.skip == undefined ){
            req.body.skip = 0;
         }else{
            req.body.skip = parseInt(req.body.skip);
         }
         if(req.body.limit == undefined ){
            req.body.limit = 50;
         }else{
            req.body.limit = parseInt(req.body.limit);
         }
        
         let docCount =  await db.campAnalytics.aggregate([
                {
                    $match: {
                        clientId  : req.body.clientId,
                        $or: [
                               {  groupName : {'$regex': req.body.groupName ,'$options': 'i'} },
                               {  tempName : { '$regex': req.body.tempName ,'$options': 'i'} }
                             ]
                    },
                },
                {
                        $group: {
                            _id:{ 
                                createdOn :"$createdOn",
                                groupName: "$groupName",
                                tempName: "$tempName"
                
                            },
                            createdAt: {$max: '$createdAt'},
                            items: {$push: '$$ROOT'},
                            count:{$sum:1},
                            statuses: { $push: "$statuses" },
                        },      
                },
                {
                    $count: "documentCount" 
                }
         ]).allowDiskUse(true);;
         
        let analyticsData = await db.campAnalytics.aggregate([
                {
                    $match: {
                        clientId  : req.body.clientId,
                        $or: [
                               {  groupName : {'$regex': req.body.groupName ,'$options': 'i'} },
                               {  tempName : { '$regex': req.body.tempName ,'$options': 'i'} }
                             ]
                    },
                },
                {
                    $group: {
                        _id:{ 
                            createdOn :"$createdOn",
                            groupName: "$groupName",
                            tempName: "$tempName"
            
                        },
                        createdAt: {$max: '$createdAt'},
                        items: {$push: '$$ROOT'},
                        count:{$sum:1},
                        // statuses: { "$push": {"$each" : "$statuses"} 
                        statuses: { $push: "$statuses" },
                        // $unwind: "$statuses" ,
                        //  tempName: { $first: "$$ROOT" }
                    }, 
                    
                },{
                    $project: {
                    createdOn: "$_id.createdOn",
                    dataItem : "$_id",
                    groupName:1,
                    createdAt:1,
                    tempName:1,
                    statuses : 1,
                    count:1,
                    items:1,
                    elem: "$items.statuses.status"
                    }
                },
                {
                $project: {
                    createdOn:1,
                    groupName:1,
                    dataItem:1,
                    createdAt:1,
                    tempName:1,
                    statuses : 1,
                    count:1,
                    items:1,
                    elem: {
                    $reduce: {
                        input: "$elem",
                        initialValue: [],
                        in: { $concatArrays : ["$$value", "$$this"] }
                    }
                    }
                }
                },
                {
                "$project": {
                    createdOn:1,
                    groupName:1,
                    dataItem:1,
                    tempName:1,
                    createdAt:1,
                    // statuses : 1,
                    count:1,
                    // items:1,
                    "sent": {
                        "$size": {
                            "$filter": {
                                "input": "$elem",
                                "as": "item",
                                "cond": {
                                    "$eq": ["$$item", "sent"]
                                }
                            }
                        }
                    },
                    "failed": {
                        "$size": {
                            "$filter": {
                                "input": "$elem",
                                "as": "item",
                                "cond": {
                                    "$eq": ["$$item", "failed"]
                                }
                            }
                        }
                    },
                    "delivered": {
                        "$size": {
                            "$filter": {
                                "input": "$elem",
                                "as": "item",
                                "cond": {
                                    "$eq": ["$$item", "delivered"]
                                }
                            }
                        }
                    },
                    "read": {
                        "$size": {
                            "$filter": {
                                "input": "$elem",
                                "as": "item",
                                "cond": {
                                    "$eq": ["$$item", "read"]
                                }
                            }
                        }
                    },
                    "responded": {
                        "$size": {
                            "$filter": {
                                "input": "$elem",
                                "as": "item",
                                "cond": {
                                    "$eq": ["$$item", "responded"]
                                }
                            }
                        }
                    }
                }
            },
            {
                $sort: { "createdOn": -1 }
            },
            {
                $skip: req.body.skip 
            },
            {
                $limit: req.body.limit 
            }
        ]).allowDiskUse(true);

        return res.json({
                statusCode: 200,
                status: true,
                message: 'Analytics data list',
                data       : analyticsData,
                limit      : req.body.limit,
                skip       : req.body.skip,
                docCount   : docCount[0].documentCount
            });
            
    }catch(err){
        console.log('err:', err)
        return res.json({
            statusCode: 201,
            status: false,
            message: "Error on catch block",
            error : err
        });
    }
}
async function getCampSpecificData(req,res){
    try{
      let obj = { 
            clientId    : req.user.clientId,
            createdOn   : req.body.createdOn,
            tempName    : req.body.tempName,
            groupName   :  req.body.groupName
        }
        if(req.body.sentStatus){
            obj.sentStatus = req.body.sentStatus ;
        }

        let campAnalytics = await db.campAnalytics.find(obj);
        return res.json({
            statusCode: 200,
            status: true,
            message: 'Particular analytics data ',
            data : campAnalytics
        });
    }catch(err){
        console.log('Error occured in catch block ',err)
         return res.json({
             statusCode: 201,
             status: false,
             message: "Error on catch block",
             error : err
         });
    }
}
async function deleteCampAnalytics(req,res){
    try{
      let obj = { 
            clientId    : req.user.clientId,
            createdOn   : req.body.createdOn,
            tempName    : req.body.tempName,
            groupName   :  req.body.groupName
        }
        if(req.body.sentStatus){
            obj.sentStatus = req.body.sentStatus ;
        }
        
        let campAnalytics = await db.campAnalytics.deleteMany(obj);
        
        return res.json({
            statusCode: 200,
            status: true,
            message: 'Analytics Data Deleted Successfully ',
            data : campAnalytics
        });
    }catch(err){
        console.log('Error occured in catch block ',err)
         return res.json({
             statusCode: 201,
             status: false,
             message: "Error on catch block",
             error : err
         });
    }
}
async function deleteMultipleUser(req,res){
    try{
        var myquery = {
             _id : { $in : req.body.userIds }
        };
        let campAnalytics = await db.Users.deleteMany(myquery);
        return res.json({
            statusCode: 200,
            status: true,
            message: 'Users deleted successfully ',
            data : campAnalytics
        });
    }catch(err){
        console.log('Error occured in catch block ',err)
         return res.json({
             statusCode: 201,
             status: false,
             message: "Error on catch block",
             error : err
         });
    }
}
async function updateTimerConfig(req,res){
    try{
        let min_milli_sec = 0;
        let sec_milli_sec = 0;
        let hours_milli_sec = 0;


        if(req.body.timerDetails.minutes){
            min_milli_sec = req.body.timerDetails.minutes * 60000 ;
        }
        if(req.body.timerDetails.seconds){
            sec_milli_sec = req.body.timerDetails.seconds * 1000 ;
        }
        if(req.body.timerDetails.hours){
            hours_milli_sec = req.body.timerDetails.hours * 3600000 ;
        }
        let InactivityTime = min_milli_sec + sec_milli_sec + hours_milli_sec ;
    
        let clientConfig = await db.clientConfig.findOne( {clientId : req.user.clientId});
        if(clientConfig== null){
            let userInfo = new db.clientConfig({
                cronStatus        : req.body.cronStatus,
                clientId          : req.user.clientId,
                clientObjId       : req.user._id,
                resetContext      : req.body.resetContext,
                triggerNode       : req.body.triggerNode,
                nodeName          : req.body.nodeName,
                InactivityTime    : InactivityTime,
                timerDetails      : req.body.timerDetails

            })
            await userInfo.save();
        }else{
            let obj = { 
                cronStatus        : req.body.cronStatus,
                resetContext      : req.body.resetContext,
                clientObjId       : req.user._id,
                triggerNode       : req.body.triggerNode,
                nodeName          : req.body.nodeName,
                InactivityTime    : InactivityTime,
                timerDetails      : req.body.timerDetails

            }
            let campAnalytics = await db.clientConfig.updateOne(
            {clientId : req.user.clientId},
            {
                $set : obj
            });
        }

        return res.json({
            statusCode: 200,
            status: true,
            message: 'Timer configuration set successfully'
        });
    }catch(err){
        console.log('Error occured in catch block ',err)
         return res.json({
             statusCode: 201,
             status: false,
             message: "Error on catch block",
             error : err
         });
    }
}
async function getTimerConfigData(req,res){
    try{
       
        let campAnalytics = await db.clientConfig.findOne({clientId : req.user.clientId});
        return res.json({
            statusCode: 200,
            status: true,
            message: 'Timer configuration data get successfully',
            data   : campAnalytics
        });
    }catch(err){
        console.log('Error occured in catch block ',err)
         return res.json({
             statusCode: 201,
             status: false,
             message: "Error on catch block",
             error : err
         });
    }
}
//********filter duplicate data from aray of obejct based on key like phone ,name etc ********//
function getUniqueListBy(arr, key) {
    return [...new Map(arr.map(item => [item[key], item])).values()]
}

async function uploadMedia(req,res) {  

    await AWS.config.update({
        accessKeyId: config.s3Linode.accessKeyId,
        secretAccessKey: config.s3Linode.secretAccessKey,
        region: config.s3Linode.region,
        s3BucketEndpoint: config.s3Linode.s3BucketEndpoint,
    });
    var s3 = await new AWS.S3({ endpoint: config.s3Linode.url });

    const clientId = req.user.clientId
    try{
        const content =await fs.readFileSync(req.file.path);
        var filetype = req.file.mimetype
        var filesize = req.file.size
        let extArray = filetype.split("/");
        let extension = extArray[extArray.length - 1];
        let filename=req.body.tempName+"."+extension

        var fileType = filetype.split("/");

       if(fileType[0]=='image') {
           if(filesize<=5000000) {
            var params = {
                Bucket: `yugasabot/${clientId}/Media/Admin_files/WhatsApp_campaign/image`,
                Key: filename,
                Body: content,
                ContentType: req.file.mimetype,
                ACL:'public-read'
            }
            } else {
                return res.json({
                    message: 'Please select image less than 5mb',
                    statusCode: 500,
                    status: false,
                    error  : err
                });
            }
            
        }else if(fileType[0]=='application') {
                if(filesize<=100000000){
                    var params = {
                        Bucket: `yugasabot/${clientId}/Media/Admin_files/WhatsApp_campaign/document`,
                        Key: filename,
                        Body: content,
                        ContentType: req.file.mimetype,
                        ACL:'public-read'
                    }

                } else {
                    return res.json({
                        message: 'Please select documents less than 100mb',
                        statusCode: 500,
                        status: false,
                        error  : err
                    }); 
                }       

        }else if(fileType[0]=='video') {
                if(filesize<=16000000){
                    var params = {
                        Bucket: `yugasabot/${clientId}/Media/Admin_files/WhatsApp_campaign/video`,
                        Key: filename,
                        Body: content,
                        ContentType: req.file.mimetype,
                        ACL:'public-read'
                    }

                } else {
                    return res.json({
                        message: 'Please select video less than 16mb',
                        statusCode: 500,
                        status: false,
                        error  : err
                    });
                }       

        } else {
                var params = {
                    Bucket: `yugasabot/${clientId}/Media/Admin_files/WhatsApp_campaign/image`,
                    Key: filename,
                    Body: content,
                    ContentType: req.file.mimetype,
                    ACL:'public-read'
                }      
        }

       await s3.upload(params, (err, result) => {
        
            if (err) {
                return res.json({
                    message: 'Error occured during file upload on s3 server',
                    statusCode: 500,
                    status: false,
                    error  : err
                });
            } else {

                fs.unlink(req.file.path,function(errDel){
                    if(errDel) console.log('Error during unlink file ')
                    console.log('file deleted successfully')
                });

                return res.json({
                    message: 'File upload successfully',
                    statusCode: 200,
                    status: true,
                    s3_url : result.Location
                })
            }            
        }) 

    }catch(error){
        console.log('error in try catch block  ',error);
        return res.json ({
            success: false,
            statusCode : 500,
            message: 'file format and size is not supporting in s3',
            error: error
        });
    }
}


async function sendWaCampaignFromSpreadsheet(req,res) {  
    try{
        if(typeof(req.body.parameters) == "string" && req.body.parameters != "") {
            req.body.parameters = JSON.parse(req.body.parameters)
        }

        let clientData = await db.Client.findOne(
              { clientId : req.body.clientId },
              {_id : 1, clientId:1,gupShupConfig:1}
        );

        if(!clientData){
            return res.json({
                statusCode: 201,
                status: false,
                message: "Client id does not exists"
            });
        }

        let groupName = `createdgroup.${req.body.groupName}`;
        let groupData = await db.Group.findOne(
        { 
            clientId     : clientData._id,
            [groupName]  : { $exists : true } 
        }); 

        if(!groupData){
            return res.json({
                statusCode: 201,
                status: false,
                message: "Group name does not exists"
            });
        }
                    
        let selectedUserWithGroup = groupData.createdgroup[req.body.groupName].map(ele=> {
                return { ...ele, groupName : req.body.groupName }
        })    

        let gupshupUserId ;
        let gupshupPassword;
        if(clientData.gupShupConfig){
            gupshupUserId =  clientData.gupShupConfig.userId;
            gupshupPassword = clientData.gupShupConfig.password;
        }
        if(!clientData.gupShupConfig || !clientData.gupShupConfig.userId || !clientData.gupShupConfig.password ){
            return res.json({
                statusCode: 201,
                status: false,
                message: 'It seems your WABA account setup is not complete yet. Please contact Yugasa Bot team for help.'
            });
        }
        let templateData = await db.whatsAppTemplate.findOne({ clientId : req.body.clientId, name : {'$regex': req.body.templateName ,'$options': 'i'} });
        console.log('templateData: ', templateData)
        console.log('templateDataString: ', JSON.stringify(templateData))
        if(!templateData){
            return res.json({
                statusCode : 201,
                status     : false,
                message    : "Template name does not exists",
            });
        }
         /********************************************************************
         *  check if the whatsapp business account is gupshup or yugasa WABA using by client
         *******************************************************************/
        if(clientData.gupShupConfig.accountType == "Yugasa"){
            let token ;
            let from;
            if(clientData.gupShupConfig){
                from  =  clientData.gupShupConfig.userId;
                token =  clientData.gupShupConfig.password;
            }
            let paramsArray = [];
            let component ; 
            if(req.body.parameters && req.body.parameters.length>0){
                await req.body.parameters.map(ele=>{
                    let obj = {
                        "type": "text",
                        "text": ele
                    };
                    paramsArray.push(obj);
                });
            }
            let buttonParams ;
            if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  ){
                buttonParams={
                    "type": "button",
                    "sub_type" : "url",
                    "index": "0", 
                    "parameters": [
                        {
                            "type": "text",
                            "text": req.body.buttonUrlParam
                        }
                    ]
                }
            }
            if(templateData.msg_type =="IMAGE"){
                component = [
                    {
                        "type":"header",
                        "parameters":[
                            {
                                "type": "image",
                                "image": {
                                "link": req.body.media_url
                                }
                            }
                        ]
                    },
                    {
                    "type": "body",
                    "parameters": paramsArray
                    } 
                ];
                if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                component.push(buttonParams);
            }
            if(templateData.msg_type =="VIDEO"){
                component = [
                    {
                        "type":"header",
                        "parameters":[
                            {
                                "type": "video",
                                "video": {
                                "link": req.body.media_url
                                }
                            }
                        ]
                    },
                    {
                    "type": "body",
                    "parameters": paramsArray
                    } 
                ];
                if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                    component.push(buttonParams);
            }
            if(templateData.msg_type =="DOCUMENT"){
                const urlArray = req.body.media_url.split("/");
                let filename = urlArray[urlArray.length-1] ;
                component = [
                    {
                        "type":"header",
                        "parameters":[
                            {
                                "type": "document",
                                "document": {
                                "filename":filename,
                                "link": req.body.media_url
                                }
                            }
                        ]
                    },
                    {
                    "type": "body",
                    "parameters": paramsArray
                    } 
                ];
                if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                component.push(buttonParams);
            }
            if(templateData.msg_type =="TEXT"){
                component = [  
                    {
                    "type": "body",
                    "parameters": paramsArray
                    } 
                ];
                if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                component.push(buttonParams);
            }

            const bot = await createBot(from, token);
            let lang=(req.body.lang!=undefined)?req.body.lang:'en_us';
            let sendTest =await bot.sendTemplate('917209238654',req.body.templateName,lang,component);
            
            let promiseArray = [];
            const uniqueUserswithGrpName = getUniqueListBy(selectedUserWithGroup, 'phone');
            await uniqueUserswithGrpName.forEach(element=>{
                let tempObj = {
                    from           : from,
                    token          : token,
                    send_to        : element.phone,
                    name           : element.name,
                    msg            : templateData.text,
                    msg_type       : templateData.msg_type,
                    button         : templateData.button,
                    media_url      : req.body.media_url,
                    header         : templateData.header,
                    footer         : templateData.footer,
                    buttonUrlParam : req.body.buttonUrlParam,
                    templateName   : req.body.templateName,
                    component      : component
                }
                promiseArray.push(callYugasaApiSendUserMsg(tempObj));
            })
            Promise.allSettled(promiseArray).then((promiseReturnArray) => {
                let saveCampaignAnalytics = saveCampaignAnalyticsData(promiseReturnArray,req.body.templateName,uniqueUserswithGrpName,req.body.clientId,clientData._id);
                Promise.resolve(saveCampaignAnalytics);
                return saveCampaignAnalytics;
            }).then((data)=>{
                return res.json({
                    statusCode: 200,
                    status: true,
                    message: 'Message sent successfully',
                    sendMsgStatus : data
                });
            }).catch((error) => {
                console.log("error occured in promise all catch block", error);
            });
        }else{

        let promiseArray = [];
        const uniqueUserswithGrpName = getUniqueListBy(selectedUserWithGroup, 'phone');

        await uniqueUserswithGrpName.forEach(element=>{
            let tempObj = {
                gupshupUserId  : gupshupUserId,
                gupshupPassword: gupshupPassword,
                send_to        : element.phone,
                name           : element.name,
                msg            : templateData.text,
                msg_type       : templateData.msg_type,
                button         : templateData.button,
                media_url      : req.body.media_url,
                header         : templateData.header,
                footer         : templateData.footer,
                buttonUrlParam : req.body.buttonUrlParam
            }
            promiseArray.push(callGupshupApiSendUserMsg(tempObj,req.body.parameters));

        })
        Promise.allSettled(promiseArray).then((promiseReturnArray) => {
            let saveCampaignAnalytics = saveGupshupCampaignAnalyticsData(promiseReturnArray,req.body.templateName,uniqueUserswithGrpName,req.body.clientId,clientData._id);
            
            if(promiseReturnArray[0].value && promiseReturnArray[0].value.code == 333){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: promiseReturnArray[0].value.message
                });
            }
            return res.json({
                    statusCode: 200,
                    status: true,
                    message: 'Message sent successfully'
                });
        }).catch((error) => {
            console.log("error occured in promise all catch block", error);
        });
        }
               
    }catch(error){
        console.log('error in try catch block  ',error);
        return res.json ({
            success: false,
            statusCode : 500,
            message: 'error in try catch block',
            error: error
        });
    }
}

async function sendWaCampaignToParticularUser(req,res){
    try{
        if(typeof(req.body.parameters) == "string" && req.body.parameters != "") {
            req.body.parameters = JSON.parse(req.body.parameters)
        }
        let clientData = await db.Client.findOne(
            {clientId : req.body.clientId},
            { gupShupConfig:1, clientId:1,_id : 1});

        if(!clientData){
            return res.json({
                statusCode: 201,
                status: false,
                message: "Client id does not exists"
            });
        }
        if(!clientData.gupShupConfig || !clientData.gupShupConfig.userId || !clientData.gupShupConfig.password ){
            return res.json({
                statusCode: 201,
                status: false,
                message: 'It seems your WABA account setup is not complete yet. Please contact Yugasa Bot team for help.'
            });
        }
        let templateData = await db.whatsAppTemplate.findOne({ clientId : req.body.clientId, name : {'$regex': req.body.templateName ,'$options': 'i'} });
        
        if(!templateData){
            return res.json({
                statusCode : 201,
                status     : false,
                message    : "Template name does not exist",
            });
        }
        /********************************************************************
         *  check if the whatsapp business account is gupshup or yugasa WABA using by client
         *******************************************************************/
        if(clientData.gupShupConfig.accountType == "Yugasa"){
                
                let token ;
                let from;
                if(clientData.gupShupConfig){
                    from  =  clientData.gupShupConfig.userId;
                    token =  clientData.gupShupConfig.password;
                }
                
                let paramsArray = [];
                let component ; 
                if(req.body.parameters && req.body.parameters.length>0){
                    await req.body.parameters.map(ele=>{
                        let obj = {
                            "type": "text",
                            "text": ele
                        };
                        paramsArray.push(obj);
                    });
                }
                let buttonParams ;
                if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  ){
                    buttonParams={
                        "type": "button",
                        "sub_type" : "url",
                        "index": "0", 
                        "parameters": [
                            {
                                "type": "text",
                                "text": req.body.buttonUrlParam
                            }
                        ]
                    }
                }
                if(templateData.msg_type =="IMAGE"){
                    component = [
                        {
                            "type":"header",
                            "parameters":[
                                {
                                    "type": "image",
                                    "image": {
                                    "link": req.body.media_url
                                    }
                                }
                            ]
                        },
                        {
                        "type": "body",
                        "parameters": paramsArray
                        } 
                    ];
                    if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                       component.push(buttonParams);
                }
                if(templateData.msg_type =="VIDEO"){
                    component = [
                        {
                            "type":"header",
                            "parameters":[
                                {
                                    "type": "video",
                                    "video": {
                                    "link": req.body.media_url
                                    }
                                }
                            ]
                        },
                        {
                        "type": "body",
                        "parameters": paramsArray
                        } 
                    ];
                    if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                       component.push(buttonParams);
                }
                if(templateData.msg_type =="DOCUMENT"){
                    const urlArray = req.body.media_url.split("/");
                    let filename = urlArray[urlArray.length-1] ;
                    component = [
                    {
                        "type":"header",
                        "parameters":[
                            {
                                "type": "document",
                                "document": {
                                "filename": filename,
                                "link": req.body.media_url
                                }
                            }
                        ]
                    },
                    {
                    "type": "body",
                    "parameters": paramsArray
                    } 
                ];
                if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                  component.push(buttonParams);
                }
                if(templateData.msg_type =="TEXT"){
                    component = [  
                        {
                        "type": "body",
                        "parameters": paramsArray
                        } 
                    ];

                    if(req.body.buttonUrlParam != '' && req.body.buttonUrlParam != undefined && req.body.buttonUrlParam != 'undefined' && req.body.buttonUrlParam != null  )
                      component.push(buttonParams);
                }
                                
                let configData = {
                    send_to      : req.body.send_to,
                    templateName : req.body.templateName,
                    component    : component,
                    from         : from,
                    token        : token,
                    userName     : req.body.userName,
                    clientObjId  : clientData._id,
                    clientId     : clientData.clientId,
                    lang         : req.body.lang
                }
                let sendStatus = await sendMessageSaveAnalyticsYugasaWaba(configData);

                if(sendStatus.status =='success'){
                    return res.json({
                        statusCode: 200,
                        status: true,
                        message:  "Message sent successfully",
                        res : sendStatus
                    });
                }else{
                    return res.json({
                        statusCode: 201,
                        status: false,
                        res : sendStatus
                    });
                }

        }else{  

            let Userchat = await db.Userchat.findOne(
                                    {"session.phone" : req.body.send_to},
                                    { session:1, clientId:1});
            let Usersdata = await db.Users.findOne(
                                    {phone : req.body.send_to},
                                    { phone:1, clientId:1,name : 1});
            let msg = templateData.text;
            msg=replaceParams(msg,req.body.parameters)
            if(Userchat && Userchat.session && Userchat.session.name){
                msg = msg.replace(new RegExp("{{\\d+}}", "g"), Userchat.session.name);
            }else if(Usersdata && Usersdata.name){
                msg = msg.replace(new RegExp("{{\\d+}}", "g"), Usersdata.name);
            }else{
                msg = msg.replace(new RegExp("{{\\d+}}", "g"), 'User'); 
            }
            let gupshupUserId ;
            let gupshupPassword;
            if(clientData.gupShupConfig){
                gupshupUserId =  clientData.gupShupConfig.userId;
                gupshupPassword = clientData.gupShupConfig.password;
            }
            let params={
                userid       : gupshupUserId,
                password     : gupshupPassword,
                send_to      : req.body.send_to,
                method       : (templateData.msg_type=='TEXT' || templateData.msg_type==undefined)?'SendMessage':'SendMediaMessage',
                auth_scheme  : 'plain',
                channel      : 'whatsapp',
                format       : 'json',
                msg          : msg,
                v            : 1.1,
                msg_type     : (templateData.msg_type=='TEXT' || templateData.msg_type==undefined)?'HSM':templateData.msg_type
                }
            if((req.body.buttonUrlParam!=undefined && req.body.buttonUrlParam!='') || (templateData.button==true || templateData.button=='true') || (templateData.header!=undefined && templateData.header!='') || (templateData.footer!=undefined && templateData.footer!='')){
                params['isHSM']=true
                params['isTemplate']=true
                if(req.body.buttonUrlParam!=undefined && req.body.buttonUrlParam!=''){
                    params['buttonUrlParam']=req.body.buttonUrlParam;
                }
                if(templateData.header!=undefined && templateData.header!=''){
                    params['header']=templateData.header;
                }
                if(templateData.footer!=undefined && templateData.footer!=''){
                    params['footer']=templateData.footer;
                }
            } 
            if(templateData.msg_type!=undefined && templateData.msg_type!='TEXT' && templateData.msg_type!=''){
                params['media_url']=req.body.media_url;
                params['caption']=msg
            }
            let response = await axios.get('https://media.smsgupshup.com/GatewayAPI/rest', {
                params: params
            });
            
            if(response.data.response.id == 106 ){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message:  "Invalid WABA account credentials ",
                    code :response.data.response.id
                });
            }
            
            let configData = {
                send_to      : req.body.send_to,
                templateName : req.body.templateName,
                userName     : req.body.userName,
                clientObjId  : clientData._id,
                clientId     : clientData.clientId,
                errorReason  : response.data.response.details,
                messageId  :  response.data.response.id

            }
            if(response.data.response.id == 175 || response.data.response.id == 315 || response.data.response.id == 106 || response.data.response.id == 102 ||response.data.response.id == 333 || response.data.response.id ==101 || response.data.response.status == "error"){
               
                configData.status = 'failed';
                let saveAnalytics =await saveAnalyticsForGupshupWaba(configData);
                return res.json({
                    statusCode: 201,
                    status: false,
                    message:  response.data.response.details,
                    // code :response.data.response.id,
                    res     : {
                        "messageId"  : "",
                        "phoneNumber": req.body.send_to,
                        "whatsappId" : req.body.send_to,
                        "status"     : "failed"
                    }

                });
            }else{
                configData.status = 'success';
                let saveAnalytics = await saveAnalyticsForGupshupWaba(configData);
                return res.json({
                    statusCode: 200,
                    status: true,
                    message:  "Message sent successfully",
                    message2:  response.data.response.details,
                    // code : response.data.response.id
                    res     : {
                        "messageId"  : response.data.response.id,
                        "phoneNumber": req.body.send_to,
                        "whatsappId" : req.body.send_to,
                        "status"     : "success"
                    }

                });
            }
      }
    }catch(error){
        console.log('try catch block error error data====>> ', error)
        return res.json({
            statusCode: 201,
            status: false,
            message: error
        });
    }       
}
async function sendMessageSaveAnalyticsYugasaWaba(data) {
try{
    
     const bot = await createBot(data.from, data.token);
     let lang=(data.lang!=undefined)?data.lang:'en_us';
     let result = await bot.sendTemplate(data.send_to,data.templateName,lang,data.component);
     
     result.phoneNumber = data.send_to;
     result.status = "success"; 
        let saveData = new db.campAnalytics({
                    clientId: data.clientId,
                    tempName: data.templateName,
                    createdOn: new Date(),
                    sent_to: data.send_to,
                    clientObjId: data.clientObjId,
                    statuses:[],
                    sentStatus : 'In-queue',
                    messageId: result.messageId,
                    campaignId: `${new Date().getTime()}_${data.clientId}_${data.templateName}_${data.userName}`,
                    groupName:data.userName,
                    createdAt : new Date(),
                    updatedAt : new Date()
        });
                    await saveData.save();
                    return result;
}catch(err){
                    
                    let saveData = new db.campAnalytics({
                        clientId: data.clientId,
                        tempName: data.templateName,
                        createdOn:  new Date(),
                        sent_to: data.send_to,
                        clientObjId: data.clientObjId,
                        statuses:[{"status" : "failed","timeStamp" : new Date() , errorReason : err.error }],
                        messageId: "",
                        errorReason : err.error,
                        sentStatus : 'failed',
                        campaignId: `${new Date().getTime()}_${data.clientId}_${data.templateName}_${data.userName}`,
                        groupName:data.userName,
                        createdAt : new Date(),
                        updatedAt : new Date(),
                    });

                    await saveData.save();

                    err.phoneNumber =data.send_to;
                    err.status = "failed"; 
                    return err;
}
}
async function saveAnalyticsForGupshupWaba(data){

                letTimeStamp = new Date();
                if(data.status == 'success'){
                    let saveData = new db.campAnalytics({
                        clientId    : data.clientId,
                        tempName    : data.templateName,
                        createdOn   : new Date(),
                        sent_to     : data.send_to,
                        clientObjId : data.clientObjId,
                        statuses    : [],
                        sentStatus  : 'In-queue',
                        messageId   : data.messageId,
                        campaignId  : `${new Date().getTime()}_${data.clientId}_${data.templateName}_${data.userName}`,
                        groupName   : data.userName,
                        createdAt   : new Date(),
                        updatedAt   : new Date()
            });
                       let  result =await saveData.save();
                       return result;
                }else{

                    let saveData = new db.campAnalytics({
                        clientId    : data.clientId,
                        tempName    : data.templateName,
                        createdOn   : new Date(),
                        sent_to     : data.send_to,
                        clientObjId : data.clientObjId,
                        statuses    : [{"status" : "failed","timeStamp" : new Date() , errorReason : data.errorReason }],
                        messageId   : "",
                        errorReason : data.errorReason,
                        sentStatus  : 'failed',
                        campaignId  : `${new Date().getTime()}_${data.clientId}_${data.templateName}_${data.userName}`,
                        groupName   : data.userName,
                        createdAt   : new Date(),
                        updatedAt   : new Date(),
                    });  

                    let  result =await saveData.save();
                    return result;
                }
}
//*****************UPDATE CLIENT API_SECRET***********************/
async function  generateToken(req,res){
    try{
        req.body.channel = escapeHtml(req.body.channel);
        let client = await db.Client.findOne({  "clientId" : req.user.clientId })

        if(!client){           
            res.json({
                "statusCode" : 201,
                "status"  : "error",
                "message" : "Client not found, Please check and provide correct clientId !"
            });
        } else {

                let timestamp = new Date();
                let data = {
                        clientId     : req.user.clientId,
                        email        : client.email,
                        timestamp    : timestamp,
                        channel      : req.body.channel
                }

                    let token = jwt.sign(data, secretKey); 
                    let keyExistStatus = await client.api_keys.find(item => item.channel == req.body.channel );
                    
                    if(keyExistStatus === undefined || keyExistStatus == 'undefined'){
                       let updateToken=  await db.Client.updateOne(
                            {
                                    _id      : client._id, 
                                    email    : client.email
                            },
                            { 
                                $push:
                                 {
                                    api_keys: 
                                    { 
                                            "token"     : token,
                                            "timestamp" : timestamp,
                                            "channel"   : req.body.channel
                                    }
                                 }

                            });
                    }else{
                        let updateToken = await db.Client.updateOne({ 
                            _id      : client._id,  "api_keys.channel": req.body.channel
                        },
                        { $set: 
                            {
                                    "api_keys.$.token"     : token ,
                                    "api_keys.$.timestamp" : timestamp,
                                    "api_keys.$.channel"   : req.body.channel 
                            }
                        })
                    } 
                    let allToken = await db.Client.findOne({  "clientId" : req.user.clientId }, {api_keys : 1 } );
               
                    res.json({
                        "statusCode" : 200,
                        "status"     : "success",
                        "message"    : "Token generated successfuly.", 
                        "token"      : allToken 
                    });
            
        }
    }catch(err){
        console.log('error in catch block ',err)
        res.json({
            statusCode : 201,
            status     : false,
            message    : "Error on catch block",
            error      : err
        });
    }
}   
async function getTokenList(req,res){
    try{
        let client = await db.Client.findOne(
            {  "clientId" : req.user.clientId },
            { _id:1, clientId:1, api_secret:1,api_keys : 1 });

        if(!client){
             return res.json({
                    statusCode  : 201,
                    status      : false,
                    message     : "Client not found, Please check and provide correct clientId !",
                });
        } else {
                return res.json({
                    statusCode  : 200,
                    status      : true,
                    message     : "Data updated successfully",
                    res         : client
                });  
        }
    }catch(err){
        console.log('error in catch block ',err)
        res.json({
            statusCode : 201,
            status     : false,
            message    : "Error on catch block",
            error      : err
        });
    }
}
async function deleteTokenById(req,res){
    try{
        let client = await db.Client.findOne({  "clientId" : req.user.clientId },{_id:1,clientId:1,api_keys:1})
        
        if(!client){
             return res.json({
                    statusCode  : 201,
                    status      : false,
                    message     : "Client not found, Please check and provide correct clientId !",
                });
        }
        let result = await db.Client.updateOne(
            { clientId: req.user.clientId },
            {
                '$pull': {
                    'api_keys': {
                        '_id':
                            ObjectID(req.body.tokenId)
                    }
                }
            },{ multi : true });

            return res.json({
                statusCode  : 200,
                status      : true,
                message     : "Token deleted successfully.",
                res         : result
            }); 
    
    }catch(err){
        console.log('error in catch block ',err)
        res.json({
            statusCode : 201,
            status     : false,
            message    : "Error on catch block",
            error      : err
        });
    }
 
}
async function  updateClientApiSecret(req,res){
    try{
        req.body.api_secret = escapeHtml(req.body.api_secret);
        let client = await db.Client.findOne({  "clientId" : req.user.clientId },{_id:1,clientId:1,api_keys:1});
        if(!client){
                return res.json({
                    statusCode  : 201,
                    status      : false,
                    message     : "Client not found, Please check and provide correct clientId !",
                });
        }
        let result = await db.Client.updateOne(
            { clientId: req.user.clientId },
            {
                $set : {
                    "api_secret" : req.body.api_secret
                }
            });
            return res.json({
                statusCode  : 200,
                status      : true,
                message     : "Api secret updated successfully.",
                res         : result
            }); 
    }catch(err){
        console.log('error in catch block ',err)
        res.json({
            statusCode : 201,
            status     : false,
            message    : "Error on catch block",
            error      : err
        });
    }
}   
async function campaignUserListForAgent(req,res){
    try {
        if(req.body.skip == undefined ){
            req.body.skip = 0;
         }else{
            req.body.skip = parseInt(req.body.skip);
         }
         if(req.body.limit == undefined ){
            req.body.limit = 50;
         }else{
            req.body.limit = parseInt(req.body.limit);
         }
        
        let templates=[];
        if(req.body.clientId && req.body.agentId){
            const campaigns=  await db.whatsAppTemplate.find(
                {
                    $and:[{clientId:req.body.clientId},{agent_Id:{$in:[req.body.agentId]}}]
                },
                {
                    name:1,
                    clientId:1,
                    agent_Id:1
                }
            );
            campaigns.map(el=>{ templates.push( el.name); });
            
        }else if(req.body.tempName){
            templates.push(req.body.tempName);
        }
        let docCount =  await db.campAnalytics.aggregate([
            {
                $match:
                {
                    tempName  : {$in: templates},
                    clientId  : req.body.clientId,
                    sentStatus: { $ne: 'failed' } 
                }
            },
            {
                $count: "documentCount" 
            }
        ]);
        let documentCount ;
        if(docCount.length>0 ){
            documentCount =docCount[0].documentCount ;
        }else{
            documentCount = 0;
        }
        const chatData = await db.campAnalytics.aggregate([  
            {
                $match:
                {
                    tempName  : {$in: templates},
                    clientId  : req.body.clientId,
                    sentStatus: { $ne: 'failed' } 
                }
            },
            {
                $project: 
                {
                    clientId: 1,
                    clientObjId:1,
                    chatId:1,
                    tempName:1,
                    sent_to:1,
                    text:1,
                    msg_type:1,
                    media_url:1,
                    statuses :1,
                    parameters:1,
                    createdAt :1
                }
            },
            {
                $project: 
                {
                    clientId: 1,
                    clientObjId:1,
                    tempMsg: '$text' ,
                    msg_type: '$msg_type',                       
                    chatId:1,
                    tempName:1 ,
                    media_url:1,
                    sent_to:1,
                    parameters:1,
                    createdAt :1
                }
            },  
            {
                $lookup:
                {
                    from:"userchats",
                    localField:"chatId",
                    foreignField:"userId",
                    as:"final_data"
                }  
            },
            {
                $project:
                {
                    clientId: 1,
                    clientObjId:1,
                    tempMsg:  1 ,
                    msg_type: 1,                       
                    chatId:1,
                    tempName:1 ,
                    media_url:1,
                    sent_to:1,
                    parameters:1,
                    createdAt :1,
                    userId : { $arrayElemAt: [ "$final_data.userId", -1 ] } ,
                    session : { $arrayElemAt: [ "$final_data.session", -1 ] } ,
                    lastChatData: { $arrayElemAt: [ "$final_data.chats", -1 ] }
                }
            },  
            {
                $project:
                {
                        clientId: 1,
                        clientObjId:1,
                        tempMsg:  1 ,
                        msg_type: 1,                       
                        chatId:1,
                        tempName:1 ,
                        media_url:1,
                        sent_to:1,
                        parameters:1,
                        createdAt :1,
                        userId : 1,
                        session : 1,
                        lastChatData: { $arrayElemAt: [ "$lastChatData", -1 ] }
                }
            },  
            {
                $sort: { "createdAt": -1 }
            },
            {
                $skip: req.body.skip 
            },
            {
                $limit: req.body.limit 
            }
        ])
            
          
        if(chatData.length<0){
            return res.status(400).json({
                status  : "fail",
                message : "no campaign found",
                data       : []
            })
        }
        res.status(200).json({
            status     : "sucess",
            data       : chatData,
            statusCode : 200,
            limit      : req.body.limit,
            skip       : req.body.skip,
            docCount   : documentCount
        })
    } catch (err) {
        console.log('error in catch block : ',err)  
    }
}
async function findTempLists(req,res){
    try {
        const campaigns=  await db.whatsAppTemplate.find({$and:[{clientId:req.body.clientId},
        {agent_Id:{$in:[req.body.agentId]}}]},{name:1,clientId:1,agent_Id:1});
    if(campaigns.length<0){
        return res.status(400).json({
        status:"fail",
        message:"no campaign found",
    })
    }
    res.status(200).json({
        status:"success",
        statusCode:200,
        data:campaigns
    })
    } catch (err) {
        res.status(500).json({
            status:"fail",
            message:"Server Error",
            data:err,  
        })
        }
}
async function getSentUserTemplate(req,res){
    try {
        let analyticsData = await db.campAnalytics.findOne(
            { sent_to : req.body.sent_to ,createdAt : req.body.createdAt},
            { parameters : 1 ,sent_to:1, media_url:1, tempName : 1, chatId : 1 , clientId :1,createdAt:1});

        let waTemplateData = await db.whatsAppTemplate.findOne(
                { name : analyticsData.tempName , clientId : analyticsData.clientId },
                { text : 1 ,msg_type : 1 ,name :1 });
            
            let data ={
                msg_type : waTemplateData.msg_type,
                text : waTemplateData.text,
                parameters : analyticsData.parameters,
                sent_to : analyticsData.sent_to,
                media_url : analyticsData.media_url,
                tempName  :analyticsData.tempName,
                tempName  :analyticsData.tempName,
                createdAt  :analyticsData.createdAt,
            }
        res.json({
            statusCode: 200,
            status: true,
            message: "Userlist founded successfully",
            data: data,
        })
    } catch (err) {
        console.log('err in catch block:', err)
        res.status(500).json({
            status:"fail",
            message:"Server Error",
            data:err,  
        })
    }
}
async function findHumanAgent(req,res){
    try {
        const agent=  await db.humanagents.find({"clientId":req.user.clientId})
        res.status(200).json({
            status:"success",
            statusCode:200,
            data:agent
        })
    } catch (err) {
        res.status(400).json({
           message:err.message,
           status:"error in catch block" 
        })
    }
}
async function currentLogedInClientData(req,res){
    try {
        const currentClient= await db.Client.findOne({clientId:req.user.clientId})
        res.status(200).json({
            status:"success",
            statusCode:200,
            data:currentClient
        })
    } catch (err) {
        res.status(400).json({
            message:err.message,
            status:"error in catch block" 
         })
    }
}
/*******************************************************
 * split the array into particular size (chunk size ) arry[100] into arr[60] and array[40];
 * ****************************************************/
function splitArrayInChunks(array,chunkSize){
    if(chunkSize<1){
      return array;
    }
    let chunks=[]
    for (let i = 0; i < array.length; i += chunkSize) {
      let chunk = array.slice(i, i + chunkSize);
        chunks.push(chunk);
    }
    // console.log('chunk 2222 ',chunks)
    return chunks;
  }

  async function getAllSubcriberExcel(headers, rows) {
    const workbook = new excel.stream.xlsx.WorkbookWriter({});
    const sheet = workbook.addWorksheet("simple", { properties: { defaultRowHeight: 20 } })
    sheet.columns = headers;
    rows.forEach(async function (item, index) {
        sheet.addRow({
            "S.no": item.id,
            Name: item.name,
            Email: item.email,
            PhoneNo: item.phone,
            ClientId: item.clientId,
            Opt_in: item.opt_in

        });
    });
    sheet.commit();
    return new Promise((resolve, reject) => {
        workbook
            .commit()
            .then(() => {
                const stream = workbook.stream;
                const result = stream.read();
                resolve(result);
            })
            .catch((e) => {
                reject(e);
            });
    });
}

async function getAllSubscriberList(req, res) {
    let workbook = new excel.Workbook();
    let worksheet = workbook.addWorksheet("Subscribers");
  
    worksheet.columns = [
      {
        header: "S.no",
        key: "S.no",
        width: 10,
        style: { font: { name: "Arial Black", bold: true } },
      },
      {
        header: "Name",
        key: "Name",
        width: 32,
        style: { font: { name: "Arial Black", bold: true } },
      },
      {
        header: "Email",
        key: "Email",
        width: 32,
        style: { font: { name: "Arial Black", bold: true } },
      },
      {
        header: "PhoneNo",
        key: "PhoneNo",
        width: 32,
        style: { font: { name: "Arial Black", bold: true } },
      },
      {
        header: "ClientId",
        key: "ClientId",
        width: 32,
        style: { font: { name: "Arial Black" } },
      },
      {
        header: "Opt_in",
        key: "Opt_in",
        width: 32,
        style: { font: { name: "Arial Black" } },
      },
    ];
    let obj1 = req.body;
  
    // var users = await db.Userchat.find({
    //   $and: [
    //     { clientId: req.user._id },
    //     {
    //       date: { $gte: obj1[0].startDate, $lte: obj1[0].endDate },
    //     },
    //   ],
    // });
  
    checkForToken(req.user._id).then(async function (response) {
      let userData = req.body;
      let userModelUserList = {};
      let value = parseInt(userData[0].filtervalue);
      if (value != null && value != 0) {
        userModelUserList = await db.Users.find({
          clientId: req.user.clientId,
        }).limit(value);
      } else {
        userModelUserList = await db.Users.find({
          clientId: req.user.clientId,
        });
      }
      // console.log("userModelUserList", userModelUserList);
      var data = [];
      var index = 1;
      for (user of userModelUserList) {
        var obj;
        if (userData[0].filtervalue != null) {
          obj = {
            index: index,
            clientId: user.clientId,
            email: user.email,
            name: user.name,
            phone: user.phone,
            opt_in: user.opt_in,
          };
          index++;
          data.push(obj);
        } else {
          obj = {
            index: index,
            clientId: user.clientId,
            email: user.email,
            name: user.name,
            phone: user.phone,
            opt_in: user.opt_in,
          };
          index++;
          data.push(obj);
        }
      }
      // console.log("data________________________", data);
  
      data.forEach((e) => {
        let a = [
          {
            "S.no": e.index,
            ClientId: e.clientId,
            Name: e.name,
            Email: e.email,
            PhoneNo: e.phone,
            Opt_in: e.opt_in,
          },
        ];
        worksheet.addRows(a);
      });
      res.setHeader("fileName", "Subscribers.xlsx");
      res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader(
        "Content-Disposition",
        "attachment; filename=" + "Subscribers.xlsx"
      );
  
      return workbook.xlsx.write(res).then(function () {
        res.status(200).end();
      });
    });
  }


async function syncTemplate(clientId,template){
    let tempId=template.id;
    let fetchIdtemp = await db.whatsAppTemplate.findOne({clientId : clientId, templateId : tempId});
    let tempdata=getComponents(template,clientId);
    //console.log("tempdata",tempdata)
    if(!fetchIdtemp){
        let data = new db.whatsAppTemplate(tempdata);
        data.save(function(err,result){
            if(err){
                return err;
            }else{
                return true;
            }
        })
    } else {
        db.whatsAppTemplate.updateOne({_id : fetchIdtemp._id },
            {$set : tempdata },function(err,result){
                if(err){
                    return err;
                }else{
                    return true;
                }
            });
    }
}

function getComponents(template,clientId){
    let components=template.components
    let arr={
        'clientId': clientId,
        'name': template.name,
        'lang': template.language,
        'status': template.status,
        'templateId':template.id,
        'category':template.category,
        'components':components,
        'text': null,
        'msg_type': 'TEXT',
        'header': '',
        'footer': '',
        'button_url': false,
        'button': false
    }

    let body=components.filter(component => component.type == 'BODY');
    if(body.length>0){
        arr['text']=body[0].text;
    }

    let header=components.filter(component => component.type == 'HEADER');
    if(header.length>0){
        arr['msg_type']=header[0].format
        if(header[0].format=='TEXT'){
            arr['header']=header[0].text;
        }
    }

    let footer=components.filter(component => component.type == 'FOOTER');
    if(footer.length>0){
        arr['footer']=footer[0].text;
    }

    let button=components.filter(component => component.type == 'BUTTONS');
    if(button.length>0){
        arr['button']=true
        let cta=button[0].buttons
        cta=cta.filter(btn => btn.type == 'URL');
        if(cta.length>0){
            if(hasVars(cta[0].url)!=null){
                arr['button_url']=true
            }
        }
    }

    return arr;
}

function hasVars(txt){
    let regex=/{{\w*}}/g;
    return txt.match(regex)
}

async function uploadWhatsappMedia(req,res) {  
    const clientId = req.user.clientId
    let clientData=req.user
    if(!clientData.gupShupConfig || !clientData.gupShupConfig.userId || !clientData.gupShupConfig.password || (clientData.gupShupConfig.accountType != "Yugasa" && clientData.gupShupConfig.accountType != "AiSensy")){
        return res.json({
            statusCode: 201,
            status: false,
            message: 'It seems your WABA account setup is not complete yet. Please contact Yugasa Bot team for help.'
        });
    }
    if(!clientData.gupShupConfig.appId && clientData.gupShupConfig.accountType == "Yugasa"){
        return res.json({
            statusCode: 201,
            status: false,
            message: 'Please update your Meta App ID in the Whatsapp Settings.'
        });
    }
    let wabaId=clientData.gupShupConfig.wabaId 
    let appId=clientData.gupShupConfig.appId 
    let access_token=clientData.gupShupConfig.password
    let phoneId=clientData.gupShupConfig.userId 

   
    try{
        const content =await fs.readFileSync(req.file.path);
        var filetype = req.file.mimetype
        var filesize = req.file.size

        var fileType = filetype.split("/");

       if(fileType[0]=='image' && filesize>5000000) {
            return res.json({
                message: 'Please select image less than 5mb',
                statusCode: 500,
                status: false,
                error  : err
            });
            
        } else if(fileType[0]=='application' && filesize>100000000) {
                    return res.json({
                        message: 'Please select documents less than 100mb',
                        statusCode: 500,
                        status: false,
                        error  : err
                    });     

        } else if(fileType[0]=='video' && filesize>16000000) {
                    return res.json({
                        message: 'Please select video less than 16mb',
                        statusCode: 500,
                        status: false,
                        error  : err
                    });
        }
        if(clientData.gupShupConfig.accountType == "AiSensy"){
            await uploadAiSensyMedia(req,res);
        } else {
            let filepath=process.env.baseDir+req.file.path
            let fdata={
              "file_length":req.file.size,
              "file_type":req.file.mimetype,
              "access_token":access_token
            }
            let url="https://graph.facebook.com/v16.0/"+appId+"/uploads"
            var config = {
              method: 'post',
              url: url,
              data : fdata
            };


            axios(config)
            .then(function (response) {
                    let url2="https://graph.facebook.com/v16.0/"+response.data.id
                    let filepath=process.env.baseDir+req.file.path
                    unirest('POST', url2)
                    .headers({
                        'Authorization': 'OAuth '+access_token,
                        'file_offset': '0',
                        'Content-Type':req.file.mimetype,
                        'Content-Length':req.file.size

                    })
                    .send(fs.createReadStream(filepath))
                    .end(async function (resp) { 
                        if (resp.error){
                            console.log("uploading error")
                            return res.json({
                                statusCode: 201,
                                status: false,
                                message:"There was some error processing your request. Please try again.",
                                error:resp.error
                            });
                        } else {
                            return res.json({
                                statusCode: 200,
                                status: true,
                                message: "File Uploaded Successfuly",
                                data : resp.body
                            });
                        }

                    });
            })
            .catch(function (error) {
              console.log("error in generating upload session ", error);
            });

        }


    }catch(error){
        console.log('error in try catch block  ',error);
        return res.json ({
            success: false,
            statusCode : 500,
            message: 'file format or size is not supported',
            error: error
        });
    }
}

async function deleteCloudWhatsappTemp(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            let clientData=req.user
            if(!clientData.gupShupConfig || !clientData.gupShupConfig.userId || !clientData.gupShupConfig.password || (clientData.gupShupConfig.accountType != "Yugasa" && clientData.gupShupConfig.accountType != "AiSensy")){
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: 'It seems your WABA account setup is not complete yet. Please contact Yugasa Bot team for help.'
                });
            }
            let wabaId=clientData.gupShupConfig.wabaId 
            let access_token=clientData.gupShupConfig.password
            let phoneId=clientData.gupShupConfig.userId 

           
            let url="https://graph.facebook.com/v16.0/"+wabaId+"/message_templates?access_token="+access_token+"&name="+req.body.name;

            var config = {
              method: 'delete',
              url: url
            };

            if(clientData.gupShupConfig.accountType=="AiSensy"){
                url="https://backend.aisensy.com/direct-apis/t1/wa_template/"+req.body.name
                config = {
                  method: 'delete',
                  url: url,
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer '+access_token
                    }
                };
            }

            await axios(config).then(function (response) {
                let responseObj=response.data;
                if(responseObj.success && responseObj.success==true){
                    db.whatsAppTemplate.deleteMany(
                       {clientId : req.body.clientId, name:req.body.name },function(err,result){
                       if(err){
                            return res.json({
                                 statusCode: 201,
                                 status: false,
                                 message: 'INTERNAL DB ERROR',
                                 error : err
                            });
                        }else{
                            return res.json({
                                statusCode: 200,
                                status: true,
                                message: "template delete successfully"
                            });
                        }
                   })
                } else {
                   return res.json({
                         statusCode: 201,
                         status: false,
                         message: 'Some error occured while deleting the template. Please try again.'
                     });
                }
            }).catch(function (error) {
                console.log('error in catch',error.response.data)
                return res.json({
                    statusCode: 201,
                    status: false,
                    message:error.response.data.error.error_user_msg,
                    error : error.response.data
                });
            });
        }catch(err){
            console.log("template cloud deletion error",err)
             return res.json({
                 statusCode: 201,
                 status: false,
                 message: "Error on catch block",
                 error : err
             });
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })


}

/*****************************************************
 *Request to Delete user's yubo admin account api
 ******************************************************/
 async function deleteAccountRequest(req,res){
    try {
        let clientData= await db.Client.findOne({ clientId : req.user.clientId },{ deleteRequest:1,email:1 });
        if(clientData){
            // contact@yugasa.com
            let updateData= await db.Client.updateOne(
                { clientId : req.user.clientId },
                {
                    $set: {
                        'deleteRequest' : true
                    }
                });
            await emailverifycontroller.deleteAccountRequestUserMail(clientData.email,req.user.clientId);
            await emailverifycontroller.deleteAccountRequestOwnerMail(clientData.email,req.user.clientId);

            return res.json({
                statusCode: 200,
                status: true,
                message:  "Request sent successfully",
            });
        }else{
            return res.json({
                statusCode: 200,
                status: true,
                message:  "",
            });
        }
     
    } catch (err) {
        console.log('Error in catch block ---- ', err)
        return res.json({
                statusCode: 201,
                status: false,
                message: err
            });
    }
}
/*****************************************************
 * get user account delete status
 ******************************************************/
async function getDeleteRequestStatus(req,res){
    try {
        console.log('req.user.clientId 1111111 :', req.user.clientId)
         let clientData= await db.Client.findOne({ clientId : req.user.clientId },{ deleteRequest:1 });
            return res.json({
                statusCode: 200,
                status: true,
                message:  "Delete status",
                data   : clientData
            });
    } catch (err) {
        console.log('Error in catch block ---- ', err)
        return res.json({
                statusCode: 201,
                status: false,
                message: err
            });
    }
}


async function onBoardWABA(req,res){
    checkForToken(req.user._id).then(async function(response){
        try{
            const partner_id="63f8522af3818d06f7ca610c"
            const api_key="b85a84304571646425aca_63f8522af3818d06f7ca610c"
            let options = {
              method: 'POST',
              url: 'https://apis.aisensy.com/partner-apis/v1/partner/'+partner_id+'/business',
              headers: {'Content-Type': 'application/json', 'X-AiSensy-Partner-API-Key': api_key},
              data: {
                display_name: req.user.clientId,
                email: req.user.clientId+"@helloyubo.com",
                company: "Yugasa Software Labs",
                contact: "918800332227",
                password: "Yugasa@"+req.user.clientId
              }
            };
            axios.request(options).then(function (response) {
                console.log("AiSensy: Create Business Response:", response.data);

                let business_id=response.data.business_id;
                options = {
                  method: 'POST',
                  url: 'https://apis.aisensy.com/partner-apis/v1/partner/'+partner_id+'/business/'+business_id+'/project',
                  headers: {'Content-Type': 'application/json', 'X-AiSensy-Partner-API-Key': api_key},
                  data: {name: 'Yugasa Bot '+req.user.clientId}
                };

                axios.request(options).then(function (response) {
                    console.log("AiSensy: Create Project Response:", response.data);
                    let assistant_id=response.data.id;
                    options = {
                      method: 'POST',
                      url: 'https://apis.aisensy.com/partner-apis/v1/partner/'+partner_id+'/generate-waba-link',
                      headers: {'Content-Type': 'application/json', 'X-AiSensy-Partner-API-Key': api_key},
                      data: {
                        businessId: business_id,
                        assistantId: assistant_id
                      }
                    };
                    axios.request(options).then(function (response) {
                        console.log("AiSensy: Generate Embedded SignUp Link Response:", response.data);

                        let url=response.data.embeddedSignupURL;
                        url=url.replace("https://waba.aisensy.com/register-waba/","");

                        return res.json({
                            statusCode: 200,
                            status: true,
                            message:  "Link Generated",
                            data   : {"link_id": url}
                        });
                    }).catch(function (error) {
                        console.error("AiSensy: Error while generating Embedded SignUp Link: ", error);
                        return res.json({
                            statusCode: 201,
                            status: false,
                            message:"There was some error while processing your request. Please try again.",
                            error : "Error"
                        });
                    });
                }).catch(function (error) {
                    console.error("AiSensy: Error while creating project: ", error);
                    return res.json({
                        statusCode: 201,
                        status: false,
                        message:"There was some error while processing your request. Please try again.",
                        error : "Error"
                    });
                });
            }).catch(function (error) {
                console.error("AiSensy: Error while creating business: ", error);
                return res.json({
                    statusCode: 201,
                    status: false,
                    message:"There was some error while processing your request. Please try again.",
                    error : "Error"
                });
            });
        }catch(err){
            console.log("AiSensy: OnBoarding Error",err)
            return res.json({
                statusCode: 201,
                status: false,
                message: "There was some error while processing your request. Please try again.",
                error : "Error"
            });
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403,
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
    })
}


//Send Template via AiSensy
async function sendTemplate(tempObj){
   let resdata={
            "messaging_product": "whatsapp",
            "recipient_type": "individual",
            "to": tempObj.send_to,
            "type": "template",
            "template": {
              "name": tempObj.templateName,
              "language": {"code":tempObj.lang },
              "components":tempObj.component
            }
          }

      var options = {
        method: 'POST',
        url: 'https://backend.aisensy.com/direct-apis/t1/messages',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer '+tempObj.token
        },
        data: resdata
      };

  let res= await axios.request(options).then(function (response) {
    console.log('message sentt!', response.data);
    let result = {
      "messageId": response.data.messages[0].id,
      "phoneNumber": response.data.contacts[0].input,
      "whatsappId":response.data.contacts[0].wa_id,
      "status":"fulfilled"
    }
    console.log("result",result)
    return result;
  }).catch(function (error) {
    console.error("Unable to send message:", error);
    error.phoneNumber=tempObj.send_to
    error.status="rejected"
    return error;
  });
  return res;
}

async function  assignAgentToTemplate(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            let tempData = db.whatsAppTemplate.findOne({_id : ObjectID(req.body.tempId) ,clientId : req.body.clientId });
            if (tempData) {
                let userInfo = {
                    agent_Id    : req.body.agent_Id == undefined ? [] : req.body.agent_Id  ,
                    updatedAt : Date.now()
                }
                db.whatsAppTemplate.updateOne({_id : req.body.tempId },
                    {$set : userInfo },function(err,result){
                        if(err){
                            return res.json({
                                statusCode: 201,
                                status: false,
                                message: 'INTERNAL DB ERROR',
                                error : err
                            });
                        }else{
                            return res.json({
                                statusCode: 200,
                                status: true,
                                message: "template updated successfully"
                            });
                        }
                    })
            }else{
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: `Template not found with this id : ${req.body.tempId}`
                });
            }
        }catch(err){
            return res.json({
                statusCode: 201,
                status: false,
                message: "Error on catch block",
                error : err
            });
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })

}

const fetAllTemplates = async (url,option) => {
  const response = await axios.get(url,option);
  if(!response.data) throw new Error('error while fetching templates fetAllTemplates');
  return [
    ...response.data.data,
    ...(response.data.paging.next ? await fetAllTemplates(response.data.paging.next,option) : [])
  ];
}
/*************************************************************
 * Campaign Microservice APIs' here
************************************************************8*/
async function getClientDataByClientId(req,res){
    try {
            let clientData = await db.Client.findOne(
                { clientId : req.body.clientId },
                {_id : 1, clientId:1,gupShupConfig:1,queueSize :1,delayTime:1,WAcampaignSentStatus:1,codeNodeStatus:1}
            );
            if(clientData){
                return res.json({
                    statusCode: 200,
                    status: true,
                    message:  "Client data found successfully",
                    data   : clientData
                });
            }else{
                return res.json({
                    statusCode: 201,
                    status: false,
                    message: "Invalid client id, Please enter correct client id",
                    data : []
                });
            }
        } catch (err) {
            // console.log("template cloud deletion error",err)
            return res.json({
                statusCode: 201,
                status: false,
                message: "Error on catch block",
                error: err
            });
        }
        
}
async function getGroupDataByGroupName(req,res){
    try {

        let groupName = `createdgroup.${req.body.groupName}`;
        let groupData = await db.Group.findOne(
        { 
            clientId     : req.body.clientId,
            [groupName]  : { $exists : true } 
        }); 
        if(groupData){
            return res.json({
                statusCode: 200,
                status: true,
                message:  "group data found successfully",
                data   : groupData
            });
        }else{
            return res.json({
                statusCode: 201,
                status: false,
                message: "Invalid group name, Please enter correct group name and client id",
                data : []
            });
        }
    } catch (err) {
        console.log("error occure in catch block---------------------", err)
        return res.json({
            statusCode: 201,
            status: false,
            message: "Error on catch block",
            error: err
        });
    } 
}
async function findOptOutUserList(req,res){
    try {
        const optedNumber = await db.optOutList.find({clientId:req.body.clientId});
        if(optedNumber){
            return res.json({
                statusCode: 200,
                status: true,
                message:  "List of Opt-out user",
                data   : optedNumber
            });
        }else{
            return res.json({
                statusCode: 201,
                status: false,
                message: "Invalid client id",
                data : []
            });
        }
    } catch (err) {
        console.log("error occure in catch block---------------------", err)
        return res.json({
            statusCode: 201,
            status: false,
            message: "Error on catch block",
            error: err
        });
    } 
}
async function findTemplateData(req,res){
    try {
        let templateData ;
        if(req.body.accountType == "Gupshup"){
            templateData= await db.whatsAppTemplate.findOne({ clientId : req.body.clientId, name : req.body.templateName });
        } else {
            templateData= await db.whatsAppTemplate.findOne({ clientId : req.body.clientId, lang : req.body.lang, name : req.body.templateName });
        }

        if(templateData){
            return res.json({
                statusCode: 200,
                status: true,
                message:  "Template Data found",
                data   : templateData
            });
        }else{
            return res.json({
                statusCode: 201,
                status: false,
                message: "Invalid template name or client id",
                data : []
            });
        }
    } catch (err) {
        console.log("error occure in catch block---------------------", err)
        return res.json({
            statusCode: 201,
            status: false,
            message: "Error on catch block",
            error: err
        });
    } 
}
async function updateWACampStatus(req,res){
    try {
          
             db.Client.updateOne(
                { clientId : req.body.clientId}, 
                { $set: { WAcampaignSentStatus : req.body.status  } }, (err, result) => {
                    if (err) {
                        return res.json({
                            statusCode: 201,
                            status: false,
                            message: 'INTERNAL DB ERROR',
                            error: err
                        });
                    } else {
                        return res.json({
                            statusCode: 200,
                            status: true,
                            message:  "Client whatsapp campaign status updated successfully",
                            data: result
                        });
                    }
            });
    } catch (err) {
        console.log("error occure in catch block---------------------", err)
        return res.json({
            statusCode: 201,
            status: false,
            message: "Error on catch block",
            error: err
        });
    } 
}

async function getUserByPhoneNumber(req,res){
    try {   
            let Userchat = await db.Userchat.findOne(
                {"session.phone" : req.body.send_to},
                { session:1, clientId:1});
            let Usersdata = await db.Users.findOne(
                        {phone : req.body.send_to},
                        { phone:1, clientId:1,name : 1});
                return res.json({
                    statusCode: 200,
                    status: true,
                    message:  "Client data found successfully",
                    data   : {Userchat : Userchat , Usersdata :Usersdata }
                });
            
        } catch (err) {
            // console.log("template cloud deletion error",err)
            return res.json({
                statusCode: 201,
                status: false,
                data : {},
                message: "Error on catch block",
                error: err
            });
        }
        
}

async function uploadAiSensyMedia(req,res){
    await AWS.config.update({
        accessKeyId: config.s3Linode.accessKeyId,
        secretAccessKey: config.s3Linode.secretAccessKey,
        region: config.s3Linode.region,
        s3BucketEndpoint: config.s3Linode.s3BucketEndpoint,
    });
    var s3 = await new AWS.S3({ endpoint: config.s3Linode.url });
    
    const clientId = req.user.clientId

    try{
        const content =await fs.readFileSync(req.file.path);
        var filetype = req.file.mimetype
        var filesize = req.file.size
        let extArray = filetype.split("/");
        let extension = extArray[extArray.length - 1];
        let random_name=(new Date()).getTime().toString(36) + Math.random().toString(36).slice(2);
        let filename=random_name+"."+extension

        var params = {
            Bucket: `yugasabot/${clientId}/Media/Admin_files/WhatsApp_campaign/sample`,
            Key: filename,
            Body: content,
            ContentType: req.file.mimetype,
            ACL:'public-read'
        }
        await s3.upload(params, (err, result) => {
        
            if (err) {
                return res.json({
                        statusCode: 201,
                        status: false,
                        message:"There was some error processing your request. Please try again.",
                        error:err
                    });
            } else {

                fs.unlink(req.file.path,function(errDel){
                    if(errDel) console.log('Error during unlink file ')
                    console.log('file deleted successfully')
                });
                return res.json({
                    statusCode: 200,
                    status: true,
                    message: "File Uploaded Successfuly",
                    data : {"h":result.Location}
                });
            }            
        }) 
    }catch(err){
        console.log("uploadAiSensyMedia error ",err)
    }
}