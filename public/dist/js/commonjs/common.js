/** for edit profile save */
$("#editProfile").click(function () {
  var formData = new FormData($("#editformdata")[0]);
  $.ajax({
    url: "/editProfile",
    type: "POST",
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
      if (data.respcode == "204") {
        alert(data.message);
      } else if (data) {
        alert("Profile updated successfully");
        let obj = {
          cname: data.name,
        };
        if (data?.profile_img) {
          Object.assign(obj, { profile_img: data.profile_img });
        }
        localStorage.setItem("pdata", JSON.stringify(obj));
        window.location.href = "/editProfile";
      }
    },
    error: function () {
      alert("error in ajax form submission");
    },
  });
  return false;
});

/****************************/
// for checking the type of user basic or pro
async function checkUserType(alert,callback) {
  $.ajax({
    method: "POST",
    url: "/checkUserType",
    data: {},
    success: function (resp) {
      if ((resp.type == "basic" || !resp.type) && alert == "show") {
        swal({
          title: "Pro Feature",
          text: "This is a pro feature",
          icon: "warning",
          buttons: [true, "Go pro"],
        }).then((isconfirm) => {
          if (isconfirm) {
            window.location = "/subscription?key=pro";
          }
        });
      } else {
        callback(resp.type)
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page");
    },
  });
}

/****************************/
// for loading nodes name whereever required//
async function loadTreeJson() {
  $.ajax({
    type: "POST",
    url: "/loadtreejson",
    data: {},
    timeout: 10000,
    success: async function (resp) {
      resp = resp.tree;
      let nodes;
      if (resp) {
        nodes = !resp.jsonOfTree.DTree
          ? resp.jsonOfTree
            ? JSON.parse(resp.jsonOfTree).DTree
            : ""
          : resp.jsonOfTree.DTree;
      } else {
        return;
      }
      $(nodes).each(function (index, element) {
        $("<option></option>")
          .attr({
            value: element.node_name,
            text: element.node_name,
          })
          .appendTo("#intents");
      });
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page");
    },
  });
}

//To redirect to other tabs based on the prameters in url
function changeTab(paramVal, firstParam, secondParam) {
  let url = new URL(window.location.href),
    param = url.searchParams.get("key");
  if (param == paramVal) {
    activateTab(firstParam, secondParam);
  }
}
function activateTab(evt, elementId) {
  let i, tabcontent, tablinks;
  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(elementId).style.display = "block";
  if (evt.currentTarget) {
    evt.currentTarget.className += " active";
  } else {
    $("." + evt).addClass("active");
  }
}
// for edit profile
// function readURL(input) {
//   if (input.files && input.files[0]) {
//     var reader = new FileReader();
//     reader.onload = function(e) {
//       $('.profile_image_edit').attr('src', e.target.result);
//     }
//     reader.readAsDataURL(input.files[0]); // convert to base64 string
//   }
// }
// $("#upload").change(function() {
//   readURL(this);
// });

async function twoFaStatus(alert,callback) {
  $.ajax({
    method: "POST",
    url: "/get2FaStatus",
    data: {},
    success: function (resp) {
      if(resp.data._2FA == false){
        $('.next_active_btnoff').show();
        $('.next_active_btnOn').hide();
      }else{
        $('.next_active_btnoff').hide();
        $('.next_active_btnOn').show();
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page");
    },
  });
}

twoFaStatus();

async function twoFactStatus(value) {
  var _2FA = value
  swal({
    title: "Are you sure ??",
    text: "You want to disable two factor authentication !",
    icon: "warning",
    buttons: [true, "Ok"],
  }).then((isconfirm) => {
    if (isconfirm) {
      $.ajax({
        method: "POST",
        url: "/change2FAStatus",
        data: {"_2FA":_2FA},
        success: function (resp) {
          if(resp.status == false){
            $('.next_active_btnoff').show();
            $('.next_active_btnOn').hide();
    
            swal({
              text: "Two factor authentication is disabled successfully",
              icon: "success",
              button: "OK",
            });
          } 
    
        },
        error: function (jqXHR, textStatus, err) {
          alert("Something went wrong, please refresh the page");
        },
      });
    }
  });
 
}

async function getOtpFor2FactAuth(alert) {
  
  $.ajax({
    method: "POST",
    url: "/getOtpFor2FactrAuth",
    data: {},
    success: function (resp) {

      swal({
        icon: "success",
        text: `We have sent an otp on your registered mobile number`,
        type: 'info',
        confirmButtonText: 'OK'
      })
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page");
    },
  });
}

async function verifyTwoFactOtp(alert) {
  var Otp = $('.twofactverifyotp').val();
  $.ajax({
    method: "POST",
    url: "/verfiyOtpFor2Fact",
    data: {"otp":Otp},
    success: function (resp) {
      if (resp.statusCode == "200") {
        $('.next_active_btnoff').hide();
        $('.next_active_btnOn').show();
        swal({
          title:  resp.message,
          icon: "success",
          // text: `Otp verfied Successfully`,
          type: 'info',
          confirmButtonText: 'OK'
        })
      }
      
      if(resp.statusCode =="201" || resp.statusCode == 201){
        swal({
          title:  resp.message,
          icon: "error",
          type: 'warning'
        })
        
        $('.otppage').show();
        $(".authclss").hide();
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page");
    },
  });
}
// delete user account request api calls
async function deleteAccount(){
  swal({
    title: "Are you sure ?",
    text: "You want to delete your account!",
    icon: "warning",
    buttons: [true, "Yes"],
  }).then((isconfirm) => {
    if (isconfirm) {
      $.ajax({
        method: "POST",
        url: "/deleteAccountRequest",
        data: { },
        success: function (resp) {
          if (resp.statusCode == "200") {
            swal({
              title:  resp.message,
              icon: "success",
              type: 'info',
              confirmButtonText: 'OK'
            })
            getDeleteStatus();
          }
          if(resp.statusCode =="201" || resp.statusCode == 201){
            swal({
              title:  resp.message,
              icon: "error",
              type: 'warning'
            })
          }
        },
        error: function (jqXHR, textStatus, err) {
          alert("Something went wrong, please refresh the page");
        },
      });
    }
  });

}
//get account delete status if user requested to delete his  account
async function getDeleteStatus(){
  $.ajax({
    method: "GET",
    url: "/getDeleteRequestStatus",
    data: { },
    success: function (resp) {
      if (resp.statusCode == "200") {
        if(resp.data.deleteRequest == true){
          $('#showDeleteOption').hide();
          $('#deleteRequestSubmitedDiv').show();
         }
      }
      if(resp.statusCode =="201" || resp.statusCode == 201){
        swal({
          title:  resp.message,
          icon: "error",
          type: 'warning'
        })
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page");
    },
  });

}