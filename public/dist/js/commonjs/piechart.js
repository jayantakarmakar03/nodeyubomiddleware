let dtaa=[ {
    "campaign": "Facebook Ads",
    "count": 165,
    "color": "rgba(59,89,153,0.7)"
  }, {
    "campaign": "Google Ads",
    "count": 139,
    "color":"rgba(22,126,230,0.7)"
  }, {
    "campaign": "Instagram Ads",
    "count": 128,
    "color": "rgba(191,63,171,0.7)"
  }, {
    "campaign": "Organic Search",
    "count": 99,
    "color": "rgba(177,224,0,0.7)"
  }
  ];
function loadChart(){
  am4core.useTheme(am4themes_animated);
  // Themes end

  // Create chart instance
  var chart = am4core.create("chartdiv", am4charts.PieChart);

  // Add data
  chart.data = dtaa;

  // Add and configure Series
  var pieSeries = chart.series.push(new am4charts.PieSeries());
  pieSeries.dataFields.value = "count";
  pieSeries.dataFields.category = "campaign";
  pieSeries.slices.template.stroke = am4core.color("#fff");
  pieSeries.slices.template.propertyFields.fill = "color";
  pieSeries.slices.template.strokeOpacity = 1;

  // This creates initial animation
  pieSeries.hiddenState.properties.opacity = 1;
  pieSeries.hiddenState.properties.endAngle = -90;
  pieSeries.hiddenState.properties.startAngle = -90;

  chart.hiddenState.properties.radius = am4core.percent(0);
}
am4core.ready(function() {
loadChart();
});