const { BlobServiceClient } = require("@azure/storage-blob");

const fs = require("fs");

const sasToken="?sv=2021-06-08&ss=bfqt&srt=co&sp=rwdlacupiytfx&se=2023-02-24T22:31:44Z&st=2023-02-24T14:31:44Z&spr=https&sig=Xu%2BuwvedQ6cPgIinqxkMeObUlWPMbEhSUAisKC%2Flerg%3D"
const uploadUrl="https://yubotesting.blob.core.windows.net/?sv=2021-06-08&ss=bfqt&srt=co&sp=rwdlacupiytfx&se=2023-02-24T22:31:44Z&st=2023-02-24T14:31:44Z&spr=https&sig=Xu%2BuwvedQ6cPgIinqxkMeObUlWPMbEhSUAisKC%2Flerg%3D"
const storageAccountName = "yubotesting"
const containerName = 'patientdata'


module.exports = {
  createBlobFromReadStream: createBlobFromReadStream,
  checkIfContainerExists: checkIfContainerExists,
  getBlobsInContainer: getBlobsInContainer,
  isStorageConfigured: isStorageConfigured,
  createBlobInContainer: createBlobInContainer,
  createContainer: createContainer
}


async function createBlobFromReadStream(req) {
try{
    let fileInput = req.file;
    console.log("fileInput", fileInput)

    //const sasToken = "sp=rwl&st=2023-02-13T09:43:41Z&se=2025-12-31T17:43:41Z&sip=172.105.42.134&spr=https&sv=2021-06-08&sr=c&sig=OoVayZ%2FngSI2VL2kOLHsLdOEApPU2seb0FfYYvL8BUc%3D"
    //const uploadUrl = `https://${storageAccountName}.blob.core.windows.net/?${sasToken}` //`https://stgaccinawbprdlrs01.blob.core.windows.net/applicationwhatsappbot?sp=rwl&st=2023-02-13T09:43:41Z&se=2025-12-31T17:43:41Z&sip=172.105.42.134&spr=https&sv=2021-06-08&sr=c&sig=OoVayZ%2FngSI2VL2kOLHsLdOEApPU2seb0FfYYvL8BUc%3D`
    const blobService = new BlobServiceClient(uploadUrl);
    const containerClient = blobService.getContainerClient(containerName);

    let data = await isStorageConfigured(storageAccountName, sasToken)
    console.log("DATA****", data)


    //***********Create container 
    //let containerClient = await createContainer(blobService)
    // console.log("containerClient", containerClient)


    //*****Get blobs in a container 
    //let blobList = await getBlobsInContainer(containerClient)
    //console.log("blobList DATA****", blobList)

    //********* Upload File 
    let blobdata = await createBlobInContainer(containerClient, fileInput)
    console.log("blobdata", blobdata)

}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

async function checkIfContainerExists(req) {
    try{
        let fileInput = req.file;
        console.log("fileInput", fileInput)

        //const containerName = 'applicationwhatsappbot'
        //const storageAccountName = "stgaccinawbprdlrs01"
        //const sasToken = "sp=rwl&st=2023-02-13T09:43:41Z&se=2025-12-31T17:43:41Z&sip=172.105.42.134&spr=https&sv=2021-06-08&sr=c&sig=OoVayZ%2FngSI2VL2kOLHsLdOEApPU2seb0FfYYvL8BUc%3D"
        //const uploadUrl = `https://${storageAccountName}.blob.core.windows.net/?${sasToken}`; //`https://stgaccinawbprdlrs01.blob.core.windows.net/applicationwhatsappbot?sp=rwl&st=2023-02-13T09:43:41Z&se=2025-12-31T17:43:41Z&sip=172.105.42.134&spr=https&sv=2021-06-08&sr=c&sig=OoVayZ%2FngSI2VL2kOLHsLdOEApPU2seb0FfYYvL8BUc%3D`
        const blobService = new BlobServiceClient(uploadUrl);
        var container = blobService.getContainerClient(containerName)
        var isExist = await container.exists();
        console.log("containerName isExist", isExist)
    } catch(e){
        console.log("checkIfContainerExists error",e)
    }
}
async function getBlobsInContainer(req) {
try{
    //const containerName = `applicationwhatsappbot`;
    //const storageAccountName = "stgaccinawbprdlrs01";
    //const sasToken = "sp=rwl&st=2023-02-13T09:43:41Z&se=2025-12-31T17:43:41Z&sip=172.105.42.134&spr=https&sv=2021-06-08&sr=c&sig=OoVayZ%2FngSI2VL2kOLHsLdOEApPU2seb0FfYYvL8BUc%3D"
    console.log("In get blobs in container")
    let fileInput = req.file;
    console.log("fileInput", fileInput)
    //const uploadUrl = `https://${storageAccountName}.blob.core.windows.net/?${sasToken}`; //`https://stgaccinawbprdlrs01.blob.core.windows.net/applicationwhatsappbot?sp=rwl&st=2023-02-13T09:43:41Z&se=2025-12-31T17:43:41Z&sip=172.105.42.134&spr=https&sv=2021-06-08&sr=c&sig=OoVayZ%2FngSI2VL2kOLHsLdOEApPU2seb0FfYYvL8BUc%3D`
    const blobService = new BlobServiceClient(uploadUrl);

    const containerClient = blobService.getContainerClient(containerName);
    console.log("containerClient", containerClient)
    const returnedBlobUrls = [];
    for await (const blob of containerClient.listBlobsFlat()) {
        console.log("BLOB NAME", `${blob.name}`);
        const blobItem = {
            url: `https://${storageAccountName}.blob.core.windows.net/${containerName}/${blob.name}?${sasToken}`, //"https://stgaccinawbprdlrs01.blob.core.windows.net/applicationwhatsappbot?sp=rwl&st=2023-02-13T09:43:41Z&se=2025-12-31T17:43:41Z&sip=172.105.42.134&spr=https&sv=2021-06-08&sr=c&sig=OoVayZ%2FngSI2VL2kOLHsLdOEApPU2seb0FfYYvL8BUc%3D",
            name: blob.name
        }
        returnedBlobUrls.push(blobItem);
    }
    console.log("returnedBlobUrls", returnedBlobUrls)
    return returnedBlobUrls;
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
};

//check if storage is configured /not
async function isStorageConfigured(storageAccountName, sasToken) {
    return !storageAccountName || !sasToken ? false : true;
};

//create blob in container
async function createBlobInContainer(containerClient, file) {
    try{
        // create blobClient for container
        const blobClient = containerClient.getBlockBlobClient(file.filename);
        console.log("blobClient*888", blobClient)
            // set mimetype as determined from browser with file upload control
        const options = { blobHTTPHeaders: { blobContentType: file.mimetype } };

        let buffer = fs.readFileSync(file.path);
        console.log("BUFFER", buffer)
        console.log("BUFFER length", buffer.length)
        console.log("options", options)
            // upload file
        let responseParams = await blobClient.uploadData(buffer, options);
        console.log("responseParams", responseParams)        
    } catch(e){
        console.log("createBlobInContainer error",e)
    }
};

//
async function createContainer(req) {
    try{
        let fileInput = req.file;
        console.log("fileInput", fileInput)

        //const containerName = 'PatientData'
        //const storageAccountName = "stgaccinawbprdlrs01"
        //const sasToken = "sp=rwl&st=2023-02-13T09:43:41Z&se=2025-12-31T17:43:41Z&sip=172.105.42.134&spr=https&sv=2021-06-08&sr=c&sig=OoVayZ%2FngSI2VL2kOLHsLdOEApPU2seb0FfYYvL8BUc%3D"
        //const uploadUrl = `https://${storageAccountName}.blob.core.windows.net/?${sasToken}`; //`https://stgaccinawbprdlrs01.blob.core.windows.net/applicationwhatsappbot?sp=rwl&st=2023-02-13T09:43:41Z&se=2025-12-31T17:43:41Z&sip=172.105.42.134&spr=https&sv=2021-06-08&sr=c&sig=OoVayZ%2FngSI2VL2kOLHsLdOEApPU2seb0FfYYvL8BUc%3D`
        const blobService = new BlobServiceClient(uploadUrl);

        let data = await isStorageConfigured(storageAccountName, sasToken)
        console.log("DATA****", data)
        let containerName = req.containerName
        const containerClient = blobService.getContainerClient(containerName);
        console.log("containerClient is created successfully", containerClient)
        return containerClient
    } catch(e){
        console.log("createContainer error",e)
    }
}
