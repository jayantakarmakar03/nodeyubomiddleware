var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
  mongoose.connect(require('./connection-string'));
}

var newSchema = new Schema({
    'clientId': { type: String }, 
    "templateName" : { type: String },
    "groups" : { type: Array },
    "buttonUrlParam": { type: String },
    "headerParam": { type: String, default: "" },
    "media_url": { type: String },
    "parameters": { type: Array },
    "scheduledAt":{type:Date},
    "status": {type:String},
    'errorReason':{type:String},
    "savedMsgType":{type:String},
    "frequency":{type:String},
    "lang":{type:String},
    'createdAt': { type: Date, default: Date.now },
    'updatedAt': { type: Date, default: Date.now }
   
});

newSchema.pre('save', function (next) {
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function () {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function () {
  this.update({}, { $set: { updatedAt: Date.now() } });
});


module.exports = mongoose.model('ScheduledCampaign', newSchema);
