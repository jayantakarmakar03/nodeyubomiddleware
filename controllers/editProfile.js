const db = require("../models/all-models"),
  bcrypt = require("bcryptjs"),
  escapeHtml = require('escape-html'),
  jwt = require("jsonwebtoken");

const redis = require("redis");

let redisClient;

(async () => {
  redisClient = redis.createClient({"password":process.env.NODE_REDIS_KEY});

  redisClient.on("error", (error) => console.error(`Error : ${error}`));

  await redisClient.connect();
})();

module.exports = {
  editProfileGet: editProfileGet,
  editProfilePost: editProfilePost,
  changePasswordPost: changePasswordPost,
  creditMoneyInWallet:creditMoneyInWallet,
  paymentStatus:paymentStatus,
  walletBalanceCheck:walletBalanceCheck,
  debitMoney:debitMoney,
  paymentSuccess:paymentSuccess,
  updateGenAiConfig:updateGenAiConfig,
  getGenAiConfig:getGenAiConfig
}

async function editProfileGet(req, res) {
  checkForToken(req.user._id).then(async function(response) {
try{
    let data1 = await db.Client.findOne(
      { clientId: req.user.clientId },
      { email: 1, name: 1, profile_img: 1, timezone: 1,phone:1}
    );
    let sideNavValidation = {
      isActionEnabled: req.user.isActionEnabled ? req.user.isActionEnabled : false,
      isHumanAgentEnabled: req.user.isHumanAgentEnabled ? req.user.isHumanAgentEnabled : false,
    };
    const objdata = {
      email: data1.email,
      name: data1.name,
      profile_img: data1.profile_img,
      timezone: data1.timezone,
      phone:data1.phone,
      sideNavValidation: sideNavValidation
    };
    let walletData = await db.Wallet.findOne({clientId:req.user.clientId})
    let transactionData = await db.Transaction.find({walletClientId:req.user.clientId})
    res.render("editProfile.hbs", {objdata,walletData,transactionData, userTypePro : req.userTypePro, showActivateButton : req.showActivateButton});
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
  }).catch(err => {
    req.session.destroy(function(err) {
        req.logout();
        res.render("login.hbs", { title: "Yugasa Bot Admin" });
    });
})
}

async function editProfilePost(req, res) {
try{
  req.body.name = escapeHtml(req.body.name);

  if(req.file){
    let fileValidation= validateFileType(req.file.mimetype);
    if(!fileValidation){
      let msg = "Selected file is not valid please choose an image file"
      return res.json({message:msg,respcode:'204'})
    }
  }

  if(req.body.email || req.body.phone ){
    return res.json({message:'EmailId  && Phone no is not editable',respcode:'204'})
  }

  if (req.file) {
      await db.Client.updateOne(
        { clientId: req.user.clientId },
        {
          $set: {
            name: req.body.name,
            // email: req.body.email,
            profile_img: req.file.filename,
            timezone: req.body.timezone,
          },
        }
      );
    } else {
      await db.Client.updateOne(
        { clientId: req.user.clientId },
        {
          $set: {
            name: req.body.name,
            // email: req.body.email,
            timezone: req.body.timezone,
          },
        }
      );
    }
  let data1 = await db.Client.findOne(
    { clientId: req.user.clientId },
    { email: 1, name: 1, profile_img: 1, timezone: 1,phone:1 }
  );
  const objdata = {
    email: data1.email,
    name: data1.name,
    profile_img: data1.profile_img,
    timezone: data1.timezone,
    phone:data1.phone
  };
  return res.json(objdata);
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function changePasswordPost(req, res) {
  try{
  const client = await db.Client.findOne({ clientId: req.user.clientId });
  if (client) {
    let passwordIsValid = await bcrypt.compareSync(
      req.body.oldPswrd,
      client.password
    );
    if (passwordIsValid) {
      if (req.body.newPswrd == req.body.cnfrmPswrd) {
        await db.Client.updateOne(
          { clientId: req.user.clientId },
          {
            $set: {
              password: bcrypt.hashSync(req.body.newPswrd, 12),
              sessionToken: ""
            },
          }
        );
        req.flash("success_msg", "Password changed successfully.");
      } else {
        req.flash(
          "error_msg",
          "New password and confirm password do not match."
        );
      }
    } else {
      req.flash("error_msg", "Old password do not match.");
    }
    res.redirect("/editProfile?key=chngPswrd");
  } else {
    req.flash("error_msg", "User not found, please login again");
    res.redirect("/login");
  }
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}
function validateFileType(mimetype){
  let matched = false ; 
   switch(mimetype){
      case "image/jpeg":
        matched = true;    
        break;
      case "image/jpg":
        matched = true;    
        break;
      case "image/png":
        matched = true;    
        break;
      case "image/gif":
        matched = true;    
        break;
      case "image/webp":
        matched = true;    
        break;
      case "image/svg+xml":
        matched = true;    
        break;
   }
   return matched; 
}

async function checkForToken(user) {
  console.log("In check token function", user)
  return new Promise((resolve, reject) => {
      db.Client.find({ _id: user }, async function(error, clientData) {
          if (error) {
              reject();
          } else if (clientData) {
              let session_token = clientData[0]._doc.sessionToken
              if (session_token == "") {
                  reject();
              } else {
                  try {
                      const verified = jwt.verify(session_token, "YUGASA@BOT@123*");
                      console.log("verified", verified)
                      if (verified) {
                          resolve(true)
                      }
                  } catch (err) {
                      reject();
                  }
              }
          }
      });
  })
}

async function creditMoneyInWallet(req,res){
try{
  const client_id = req.user.clientId;
  const {amount} = req.body;


  const walletCheck = await db.Wallet.findOne({clientId:client_id});
  if(!walletCheck){
    await (new db.Wallet({clientId:client_id})).save();
  }
  const headers =  { 
    "x-api-version":"2022-09-01",
    "X-Client-Id":'3247453de8c1634c34b7481445547423',
    "X-Client-Secret":'TEST24f3162bfc07cbc32d50fc6a8bf2ac764682a3b6',
    "Content-Type":'application/json'
    }

    let data =  {
      order_id: '',
      order_amount: amount,
      order_currency: 'INR',
      customer_details: {
        customer_id: client_id,
        customer_name:req.user.name.toString(),
        customer_email: req.user.email.toString(),
        customer_phone: req.user.phone.toString(),
      },
      order_meta: {
        return_url: 'https://localhost:4900/payment-status?order_id={order_id}',
        notify_url: 'https://b8af79f41056.eu.ngrok.io/webhook.php'
      },
    }  
  axios
    .post('https://sandbox.cashfree.com/pg/orders',data,{headers:headers}).then(async (response)=>{
      const transaction = new db.Transaction({walletClientId:client_id,amount,order_id:response.data.order_id,transactionType:'CREDIT'});
      await transaction.save();
      
      return res.status(200).json({
        statusCode: 200,
        status: "success",
        data: response.data,
    });


  }).catch((err)=>{
      console.log(err)
    })

}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

async function paymentStatus(req,res){
try{
  const order_id = req.query.order_id;
  const client_id = req.user.client_id;
  const options = {
    method: 'GET',
    url: `https://sandbox.cashfree.com/pg/orders/${order_id}/payments`,
    headers: {accept: 'application/json', 'x-api-version': '2022-09-01', "X-Client-Id":'3247453de8c1634c34b7481445547423',
    "X-Client-Secret":'TEST24f3162bfc07cbc32d50fc6a8bf2ac764682a3b6',}
  };
  
  axios
    .request(options)
    .then(async function(response) {
      let result = response.data;
      let transaction = await db.Transaction.findOne({order_id,status:'PENDING'});
      if(result[0].payment_status == "SUCCESS"){
        transaction.status = 'SUCCESS';
        let wallet = await db.Wallet.updateOne({clientId:transaction.walletClientId},{$inc:{
          balance:result[0].payment_amount
        }});
      }else{
        transaction.status = "FAILED"
      }
      await transaction.save();
     return res.json({status:transaction.status,message:'Payment added to yugasa bot wallet successfully.'})
    })
    .catch(function (error) {
      console.error(error);
});
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function walletBalanceCheck(req,res){
try{
  const client_id = req.user.clientId;
  const {usersCount} = req.body;
  const walletAmount = await db.Wallet.findOne({clientId:client_id});
  let campPrice = 10;

  if(walletAmount){
    if(walletAmount.balance < usersCount*campPrice){
      return res.send({status:false,msg:'Insufficent Balance',neededAmount:(usersCount*campPrice - walletAmount.balance)})
    }else{
      return res.send({status:true,msg:'',amount:usersCount*campPrice});
    }
  }else{
    return res.send({status:false,msg:'Please recharge your wallet.'})
  }
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function debitMoney(req,res){
  try{
  const client_id = req.user.clientId;
  const {amount} = req.body;
  const wallet = await db.Wallet.updateOne({clientId:client_id},{$inc:{
    balance:-amount
  }});

  if(wallet){
    const transaction = new db.Transaction({walletClientId:client_id,amount,transactionType:'DEBIT'});
    await transaction.save();
        return res.send({status:true})
  }else{
    return res.send({status:false})

  }
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}

}

async function paymentSuccess(req,res){
  return res.render('payment-confirm.hbs')
}

//*********Update Gen AI Data of client ******************************/
async function updateGenAiConfig(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            req.body.genAiConfig.genai_key           = escapeHtml(req.body.genAiConfig.genai_key);
            req.body.genAiConfig.genai_org      = escapeHtml(req.body.genAiConfig.genai_org);
            
            let genAiConfig = req.body.genAiConfig;

            db.Client.updateOne(
                {clientId : req.user.clientId},
                {$set:{ genAiConfig : genAiConfig}  },(err,result)=>{
                    if(err){
                        return res.json({
                            statusCode: 201,
                            status: false,
                            message: 'INTERNAL DB ERROR',
                            error : err
                        });
                    }else{
                        redisClient.set("genai_key_"+req.user.clientId,genAiConfig.genai_key)
                        axios.post('https://admin.helloyubo.com/yuboBot/updateGenAiConfig',{"clientId":req.user.clientId});
                        return res.json({
                            statusCode: 200,
                            status: true,
                            message: "Config update successfully",
                            data : result
                        });
                    }
                })
        }catch(err){
            console.log('err in catch block===',err)
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })

}

//*********Get Gen AI Data of client ******************************/

async function getGenAiConfig(req,res){
    checkForToken(req.user._id).then(async function(response) {
        try{
            db.Client.findOne(
            {clientId : req.user.clientId},{ genAiConfig:1, clientId:1},(err,result)=>{
                if(err){
                    return res.json({
                        statusCode: 201,
                        status: false,
                        message: 'INTERNAL DB ERROR',
                        error : err
                    });
                }else{
                    return res.json({
                        statusCode: 200,
                        status: true,
                        message: "GenAi config fetched successfully",
                        data : result
                    });
                }
            })
        }catch(err){
            console.log('err in catch block',err)
        }
    }).catch(err => {
        let responseObj = {
            statusCode: 403, //unauthorized access
            status: "fail",
            message: "Unauthorized Access"
        }
        res.status(responseObj.statusCode).send(responseObj)
      })

}