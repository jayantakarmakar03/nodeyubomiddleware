const db=require('../models/all-models');
const bcrypt = require('bcryptjs');
const randomstring = require("randomstring");
const jwt = require('jsonwebtoken');
const sendMail = require('../middleware/sendMail');

module.exports={
  login:login,
  forgotPassword:forgotPassword,
  userResetPassword:userResetPassword,
  userChatList:userChatList,
  userLeadGenerate:userLeadGenerate,
  leadConversionGraph:leadConversionGraph,
  appChatControl:appChatControl,
  appSendMessage:appSendMessage,
  getProfile:getProfile,
  getMessage:getMessage,
  getChat:getChat,
  userChat:userChat
}



// /** Api for Yugasa Bot client login
//  * @route /app/login
//  * @method post
//  * @Param email, password
//  * @response json 
// */
async function login(req, res){
  const client = await db.Client.findOne({email:req.body.email}); /*fetch the data from Client collection by email key.*/
  if(!client){
    return res.status(200).json({
      status:'failed',
      message:'you are not registered please signup!'
    });
  }
  let passwordIsValid =await bcrypt.compareSync(req.body.password, client.password); //password verify.
  if(!passwordIsValid){
    return res.status(200).json({
      status : 'failed',
      message : 'Password is incorrect.'
    });
   }
  let userToken = randomstring.generate(15); //Generate a user_token.
  let updateDevId = await db.Client.updateOne({_id:client._id},{$set:{deviceId:req.body.deviceId,devType:req.body.devType,userToken:userToken}});
   
  let authToken =await jwt.sign({clientId:client._id, userToken:userToken},'YuboSecretKey'); //Generate a jwt token.
  const updatedUser= await db.Client.findOne({email:client.email},{password:0,tree:0,btree:0, renderData:0});
  return res.status(200).json({
    status : 'success',
    message : 'Login successfully.',
    authToken:authToken,
    userToken:userToken,
    data :updatedUser
  });
}




async function forgotPassword(req, res){
  //fetch the data from User collection by email key.
  const user = await db.Client.findOne({email:req.body.email},{password:0});
  if(!user){
    return res.status(200).json({
      status:'failed',
      message:'This email is not registered.Please check your emailId.'
    });
  }
  //generate a random pin password.
  let otp = randomstring.generate({
              length: 4,
              charset: 'numeric'
            });
  let html='<div style="width:400px;border:thin solid #dadce0;border-radius:10px;padding:5px 0px" align="center" ><h2 style="color:blue">Hello  '+user.name+'</h2><h2 style="color:blue">Your password is:   '+otp+'</h2></div>';
  //sending mail using middleware function.
  sendMail(req.body.email, otp, user.name, html);    
  //Update password in user collection .  
  let updatePassword= await db.Client.updateOne({email:req.body.email},{$set: {otp:otp}});
  return res.status(200).json({
    status : 'success',
    message : 'The otp has been sent. please check your mail',
    user:user
  });
}




/** Api for vendo reset password
 * @route /mobil/userResetPassword
 * @method post
 * @Param  userId, password, newPassword
 * @response json 
*/
async function userResetPassword(req, res){
  const user = await db.Client.findOne({_id:req.body._id});
  if(!user){
    return res.status(200).json({
        status:'failed',
        message:'You are not registered'
      });
  }
  if(user.otp!=req.body.otp){
    return res.status(200).json({
        status:'failed',
        message:'your otp is mismatched'
      });
  }
  //password verify.
  if(req.body.password!=req.body.newPassword){
    return res.status(200).json({
        status : 'failed',
        message : 'Your password and confirm password do not match'
      });
  }
  //Update new password 
  let updatePassword= await db.Client.updateOne({_id:req.body._id},{$set:{password:bcrypt.hashSync(req.body.password)}});
  let data =await db.Client.findOne({_id:req.body._id},{password:0});
  return res.status(200).json({
    status:'success',
    message:'Reset password successfully',
    data:data
  });
}


//Get client profile
async function getProfile(req, res) {
  try{
      const user = await db.Client.findById(req.user._id,'name host email category profile_img')
      return res.status(200).json({
          status : 'success',
          data : user
      }); 
  }catch(e){
      return res.status(500).send({
          status : 'failed',
          message: 'Internal server error.'
      });
  }
}

async function userChatList(req,res){
  let limit = (req.body.limit)?parseInt(req.body.limit):10;
  let page = (req.body.page)?parseInt(req.body.page):1;
  let pages = limit*(page-1);
  const user = await db.Client.findOne({_id:req.body._id});
  if(!user){
    return res.status(200).json({
        status:'failed',
        message:'You are not registered'
      });
  }
  var chatList=await db.Userchat.find({clientId:user._id, leadStatus:req.body.leadStatus},{msgCounter:0,callStatus:0}).sort("-createdAt").skip(pages).limit(limit).lean().exec();
  console.log(chatList,'list')
  let text, textCreatedAt;
  for(let chats of chatList){
    chats.chats.sort(function(a, b){
      if(a.createdAt > b.createdAt) { return -1; }
      if(a.createdAt < b.createdAt) { return 1; }
      return 0;
    });
    chats.chats.map(({text, createdAt},idx, arr) => arr[idx] =  ({text, createdAt}));
    chats.chats.splice(1);
    if(chats.chats<=0){
      text = '';
      textCreatedAt = ''
    }else{
      text = chats.chats[0].text;
      textCreatedAt = chats.chats[0].createdAt;
    }
    chats.text = text;
    chats.textCreatedAt = textCreatedAt;
    delete chats.chats;
  }
  var count=await db.Userchat.find({clientId:user._id, leadStatus:2}).count();
  if(chatList.length<1){
    return res.status(200).json({
      status:'failed',
      count:count,
      message:'Userchat not available'
    });
  }else{
    return res.status(200).json({
      status:'success',
      message:'User chat found successfully',
      count:count,   //count for archieve
      data:chatList
    });
  }
}


async function userChat(req,res){
  let limit = (req.body.limit)?parseInt(req.body.limit):10;
  let page = (req.body.page)?parseInt(req.body.page):1;
  let pages = limit*(page-1);
  const user = await db.Client.findOne({_id:req.body._id});
  if(!user){
    return res.status(200).json({
        status:'failed',
        message:'You are not registered'
      });
  }
  var chatList=await db.Userchat.find({clientId:user._id, leadStatus:req.body.leadStatus},{msgCounter:0,callStatus:0}).sort("-createdAt").skip(pages).limit(limit).lean().exec();
  var count=await db.Userchat.find({clientId:user._id, leadStatus:2}).count();
  if(chatList.length<1){
    return res.status(200).json({
      status:'failed',
      count:count,
      message:'Userchat not available'
    });
  }else{
    return res.status(200).json({
      status:'success',
      message:'User chat found successfully',
      count:count,   //count for archieve
      data:chatList
    });
  }
}

//Change status of Archieve, lead & list.
async function userLeadGenerate(req,res){
  const chatUpdate = await db.Userchat.updateMany({_id:{$in:req.body._id}},{$set:{leadStatus:req.body.leadStatus}});
  return res.status(200).json({
    status:'success',
    message:'User chat updated successfully'
  });
}


async function getUserLeadList(req, res){
  let leadList=await db.Userchat.find({leadStatus:1});
  return res.status(200).json({
    status:'success',
    message:'Lead list found successfully',
    data:leadList
  });
}


async function leadConversionGraph(req, res){
  var totalChat=await db.Userchat.find({clientId:req.body._id}).count();
  var totalLead=await db.Userchat.find({clientId:req.body._id, leadStatus:1}).count();
  var dailyBasis=[];
  if(req.body.type=="filter"){
    var oneDay = 24 * 60 * 60 * 1000;
    var startDate=new Date(req.body.startDate);
    var endDate=new Date(req.body.endDate);
    var totalChat=await db.Userchat.find({clientId:req.body._id, createdAt:{$gte:startDate, $lt:endDate}}).count();
    var totalLead=await db.Userchat.find({clientId:req.body._id, leadStatus:1, createdAt:{$gte:startDate, $lt:endDate}}).count();
    var diffDays = Math.round(Math.abs((startDate.getTime() - endDate.getTime()) / (oneDay)));
    for(i=-1; i<diffDays; i++){
      var endDate=new Date(req.body.endDate);
      let dailyChat=await db.Userchat.find({clientId:req.body._id, createdAt:{$lt:new Date(endDate.setDate(endDate.getDate()-i)), 
              $gte: new Date(endDate.setDate(endDate.getDate()-1))
            }   
      }).count();
     dailyBasis.push({date:new Date(endDate.setDate(endDate.getDate())),value:dailyChat});
    }
  }else{
    for(i=0; i<7;i++){
      var date=new Date();
      let dailyChat=await db.Userchat.find({clientId:req.body._id, createdAt:{$lt:new Date(date.setDate(date.getDate()-i)), 
              $gte: new Date(date.setDate(date.getDate()-1))
            }   
      }).count();
     dailyBasis.push({date:new Date(date.setDate(date.getDate()+1)),value:dailyChat});
    }
  }
  
  return res.status(200).json({
    status:'success',
    message:'Lead and Conversion graph data found successfully',
    totalChat:totalChat,
    totalLead:totalLead,
    dailyBasis:dailyBasis

  });
}

async function appChatControl(req,res){
  const chatUpdate = await db.Userchat.updateOne({userId:req.body.userId},{$set:{chatControl:req.body.chatControl}});
  if(req.body.chatControl==1){
    var msg='You have taken over to chat successfully'
  }else{
    var msg='You have handed over to chat successfully'
  }
  return res.status(200).json({
    status:'success',
    message:msg
  });
}


async function appSendMessage(req,res){
  var date=new Date();
  const user = await db.Userchat.findOne({userId:req.body.userId});
  if(user.chatControl==1){
    let updateOpt=await db.Userchat.updateOne({userId:req.body.userId},{$addToSet:{chats:{
        text:req.body.text, messageType:"outgoing",replies:[],type_option:true,createdAt:date
      }}}); 
    let data=req.body;
    global.io.emit('appChatMessage', {chatMessage:req.body});
    // pusher.trigger('post', 'add', {
    //   chatMessage :req.body
    // });
    return res.status(200).json({
      status:'success',
      message:'Message send successfully',
      data:{
        text:req.body.text, messageType:"outgoing",replies:[],type_option:true,createdAt:date
      }
    });
  }else{
    return res.status(200).json({
      status:'failed',
      message:'You should take over to chat firstly'
    });
  }
}


async function getMessage(req,res){
  let getData = await db.Client.findOne({clientId:req.body.clientId})
  if(!getData){
    return res.status(200).json({
      status:'failed',
      message:'Client not found.'
    });
  }else{
    return res.status(200).json({
      status:'success',
      message:'Data found successfully.',
      data:getData
    });
  }
}

async function getChat(req,res){
  let chat = await db.Userchat.find({clientId:req.body.clientId,userId:req.body.userId})
  if(chat.length<=0){
    return res.status(200).json({
      status:'failed',
      message:'No chat found.'
    });
  }else{
    return res.status(200).json({
      status:'success',
      message:'Chat found successfully.',
      data:chat
    });
  }
}
