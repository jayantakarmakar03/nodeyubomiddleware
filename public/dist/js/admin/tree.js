let gnrlActnCkbx = false;

async function loadintentsname() {
  $.ajax({
    type: "POST",
    url: "/loadintentsname",
    data: {},
    timeout: 10000,
    success: async function (resp) {
      let nodes = resp.data.length > 0 ? resp.data : "";
      $(nodes).each(function (index, element) {
        $("<option></option>")
          .attr({
            value: element.tag,
            text: element.tag,
          })
          .appendTo("#intents");
      });
    },
    error: function (jqXHR, textStatus, err) {
      alert("Something went wrong, please refresh the page");
    },
  });
}

//when associate with chkbx clicked
$("#gnrl_action_ckbx").click(async function () {
  if ($(this).prop("checked")) {
    gnrlActnCkbx = true;
    $(".actn-dropdown").empty();
    $("#gnrl_action_div").removeClass("hide");
    $(".actn-dropdown").append("<option>Select Property</option>");
    await getActnsList(".actn-dropdown");
  } else {
    gnrlActnCkbx = false;
    $("#gnrl_action_div").addClass("hide");
  }
});

// ********** To get the list of actions created ********** //
async function getActnsList(appendTo) {
  var data = [];
  $.ajax({
    type: "POST",
    url: "/getActnsList",
    data: {},
    success: function (response) {
      console.log("res", response)
      let actionList = response;
      if (actionList.length > 0) {
        $(actionList).each(function (index, element) {
          console.log("elel", element, element.actionName)
          $(`<option>${element.actionName}</option>`)
            .attr({
              value: element.actionName,
              title: element.actionName,
              responseVal: element.actionResponse
            })
            .appendTo(appendTo);
        });
      }
    },
  });
}

//*************//
$("#gnrl_action_dropdown").change(async function () {
  $.ajax({
    type: "POST",
    url: "/getActnReturnValues",
    data: { actionName: $(this).val() },
    success: function (resp) {
      if (resp.statusCode == 200) {
        console.log("array", resp.data.actionResponse)
        let data = resp.data.actionResponse;
        data.forEach(item => {
          console.log("elem", Object.keys(item)[0])
          $('.gnrl-response-tags').append(` <li class="tags">
            <span class="entitiesTag">${Object.keys(item)[0]}</span>
            <img class="entity-close" src="./dist/img/cross-sign.png" alt="cross-image">
        </li>`)
        });
      } else {
        alert(resp.message)
      }
    },
    error: function (textStatus, errorThrown) {
      console.log("Error getting the data")
    }
  });
})

/******** To remove the entities **********/
$(document).on("click", ".tags .entity-close", function () {
  $(this).parent("li").remove();
});

$(document).on("click", ".tags .entitiesTag", function () {
  var textInput = document.getElementById("question");
  var cursorPosition = textInput.selectionStart;
  addTextAtCursorPosition(textInput, cursorPosition, $(this).text());
});

$(document).on('focusin', function(e) {
  if ($(e.target).closest(".tox-textfield").length)
      e.stopImmediatePropagation();
});

function addTextAtCursorPosition(textInput, cursorPosition, text) {
  var front = (textInput.value).substring(0, cursorPosition);
  var back = (textInput.value).substring(cursorPosition, textInput.value.length);
  textInput.value = front + "[" +  text + "]" + back ;
}

function previewImg() {
  
  var fileInput = document.getElementById('uploadConversationalMedia');
  var previewImg = document.getElementById('previewImg');
    var file = fileInput.files[0]
    var fileType = (file.type).split("/");

      if(fileType[0]=='image') {
          previewImg.src = URL.createObjectURL(file)
          $('#previewImg').css({"width": "150px", "height": "150px","margin-left":"35%","margin-bottom":"10px"})
          
        } else if(fileType[0]=='application' || fileType[0]=='text'){
            previewImg.src = 'dist/img/document.png'
            $('#previewImg').css({"width": "150px", "height": "150px","margin-left":"35%"})
            //return;

        } else if(fileType[0]=='video'){
            previewImg.src = 'dist/img/video-button.png'
            $('#previewImg').css({"width": "150px", "height": "150px","margin-left":"35%"})
           // return;
        } else {
            previewImg.src = '' 
      }
}

function uploadConversationalMedia() {

  var fileInput = document.getElementById('uploadConversationalMedia');
  var file = fileInput.files[0];
  var formData = new FormData()
  formData.append("conversationalMedia", file);

  const operatorId = $("#operator_title").val()
  formData.append("operatorId", operatorId);
  if(operatorId=="" || operatorId==undefined){
      swal({
            title: "Please provide a title to the node first.",
            icon: "error",
            timer: 3500,
       })
  } else if(!file) {
        swal({
            title: "Please select file",
            icon: "error",
            timer: 3500,
        });

    } else if(file.size>(16777216)) {
      swal({
            title: "Please select file of size less than 16 MB.",
            icon: "error",
            timer: 3500,
       })

    } else {
     // var fileType = (file.type).split("/");
      $.ajax({
        method: "POST",
        url: "/uploadConversationalMedia",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: async function(data) {
          if(data.statusCode == 200)  {
            swal({
              title: "File upload successfully",
              icon: "success",
              timer: 3500,
             })
              $('#conv_fileLink_s3').val(data.s3_url)
          }  
        },
        error: function(jqXHR, textStatus, err) {
            console.log('errr', err);
        alert("Please refresh !");
        },
      });

    } 

}

$('#attach_media_chkbox').click(function(){
  if ($(this).prop("checked")) {
     $("#attach_media").removeClass("hide");
  } else {
     $("#attach_media").addClass("hide");
  }
});


/******************************************
 CodeNodeLogs code here
********************************************/
function showDiv() {
  document.getElementById('welcomeNodeDiv').style.display = "block";
}
$(document).ready(function() {
  $('#closed_node').on('click', function(e) { 
     document.getElementById('welcomeNodeDiv').style.display = "none";
  });
});

function getErrorLogData() {
  let clientId = document.getElementById("clientId").value;
  let data = {
      clientId : clientId,
      searchItem : "",
  };
  $.ajax({
      data: data,
      async: false,
      cache: false,
      method: "POST",
      url: "/getCodeNodeLogs",
      success: function(data) {
        console.log('data:', data)
          if (data.statusCode == 200) {
                var index=0;
                let logData = data.data;
              for (const iterator of logData) {
                      index++
                  let date =  new Date(iterator.createdAt).toLocaleString();
                  let html = `
                    <tr>
                        <td> ${iterator.errorData}</td>
                        <td> ${iterator.lineNumber}</td>
                        <td> ${iterator.nodeName}</td>
                        <td> ${iterator.userId} </td>
                        <td> ${date}</td>
                        <td errorData = '${iterator.errorData}' 
                            lineNumber= '${iterator.lineNumber}'
                            nodeName= '${iterator.nodeName}'
                            userId= '${iterator.userId}'
                            date= '${date}'
                            onclick="getInfoData(this)"
                        > <i class="fa fa-info-circle" aria-hidden="true"></i></td>
                    </tr>
                  ` 
                  $(".errorLogMainDiv").append(html);
              }
          } else {
              alert(data.message);
          }
      },error: function (jqXHR, textStatus, err) {  
          if(jqXHR.responseJSON.statusCode == 403) {
          window.location.replace("https://admin.helloyubo.com/login");
          } else {
          alert("Something went wrong, please try again")
          }
      }
  });
}
getErrorLogData();

function clearErrorLogs() {
  let clientId = document.getElementById("clientId").value;
  let data = {
      clientId : clientId
  };
  $.ajax({
      data: data,
      async: false,
      cache: false,
      method: "POST",
      url: "/removeCodeNodeLogs",
      success: function(data) {
        console.log('data:', data)
          if (data.statusCode == 200) {
            refreshErrorLog();
            swal({
              title: data.message,
              icon: "success",
              timer: 6500
             })
             
          } else {
              alert(data.message);
          }
      },error: function (jqXHR, textStatus, err) {  
          if(jqXHR.responseJSON.statusCode == 403) {
          window.location.replace("https://admin.helloyubo.com/login");
          } else {
          alert("Something went wrong, please try again")
          }
      }
  });
}

function refreshErrorLog() {
  $(".errorLogMainDiv").html("");
  getErrorLogData();
}

function codeNodeSetting(){
  let data = {
   "clientId" : document.getElementById("clientId").value
  }
 $.ajax({
   data: data,
   async: false,
   cache: false,
   method: "POST",
   url: "/getClientDataByClientId",
   success: function(data) {
     $('#codeNodeModal').modal('show');
       if (data.statusCode == 200) {
         document.getElementById("codeNodeUserId").value = data.data.codeNodeStatus.userId;
       } else {
           alert(data.message);
       }
   },error: function (jqXHR, textStatus, err) {  
       if(jqXHR.responseJSON.statusCode == 403) {
       window.location.replace("https://admin.helloyubo.com/login");
       } else {
       alert("Something went wrong, please try again")
       }
   }
 });
}

function activateCodeNodeLog(){
 let userId = document.getElementById("codeNodeUserId").value ;
 let status;
 if(userId){
   status = true;
 }else{
   status = false;
 }
 let data = {
     "status"   : status,
     "userId"   : userId,
     "clientId" : document.getElementById("clientId").value
 }
 $.ajax({
   data: data,
   async: false,
   cache: false,
   method: "POST",
   url: "/activateCodeNodeLog",
   success: function(data) {
       if (data.statusCode == 200) {
       $('#codeNodeModal').modal('hide');
         swal({
           title: data.message,
           icon : "success",
           timer: 6500
         })
       } else {
           alert(data.message);
       }
   },error: function (jqXHR, textStatus, err) {  
       if(jqXHR.responseJSON.statusCode == 403) {
       window.location.replace("https://admin.helloyubo.com/login");
       } else {
       alert("Something went wrong, please try again")
       }
   }
 });
}

function getInfoData(_this){
  let errorData  = $(_this).attr('errorData'),
      lineNumber = $(_this).attr('lineNumber'),
      nodeName   = $(_this).attr('nodeName'),
      userId     = $(_this).attr('userId'),
      date       = $(_this).attr('date');
      $("#viewerrorData").html(errorData);
      $("#viewlineNumber").html(lineNumber);
      $("#viewnodeName").html(nodeName);
      $("#viewuserId").html(userId);
      $("#viewdate").html(date);
      $('#viewLogsModal').modal('show');
}