let skip = 0;
let limit = 30;
let docCount = 0;


function getgroupdata () {

  let searchData = $("#searchBoxId").val();
  showLoader();
  let data = {
    searchItem: searchData,
    skip: skip,
    limit: limit,
  };
    $.ajax({
        type: "POST",
        url: "/getgroupdata",
        data: data,
        beforeSend: function () {
            swal({
              title: "Loading...",
              button: false,
              closeOnClickOutside: true,
              closeOnEsc: false,
              timer: 2000,
            });
          },
        success: function(data) {
            if (data) {
                hideLoader();
                skip = data.skip;
                docCount = data.docCount
                // loop for createdgroup list in createdgroup table
                for (i = 0; i < data.groupcreated.length; i++) {
                    let key = Object.keys(data.groupcreated[i].createdgroup);
                    $(".groupdata").append(`
                    <div class="my_groups">
                    <div class="border-right" style="width: 300px;
                    max-width: 300px;margin-bottom:15px;">
                    <div class="createGroups" style="">
                        <div title="${key[0].trim().replace(/[- )(]/g, '_')}" class="my-group-name">
                            <p>${key[0].trim().replace(/[- )(]/g, "_")}</p>
                            <h3>${data.groupcreated[i].createdgroup[key].length} contacts</h3>
                        </div>
                    <div class="my-group-action group-icons" style="">
                        <button class="view_group_data view_grup_cssbtn" group-id=${data.groupcreated[i]._id} data-toggle="modal" data-target="#viewmodal"></button>
                        <span class="my-group-action_span">|</span>
                        <button class="grp_view edit-grp-data" group-id=${data.groupcreated[i]._id} data-toggle="modal" data-target="#myModal"></button>
                        <span class="my-group-action_span">|</span>
                        <button class="grp_delete delete-grp-data" group-id=${data.groupcreated[i]._id}></button>
                    </div>
                </div>
                </div>
                <hr class="my-group-hr">`);
                }
            }
        },
        error: function(jqXHR, textStatus, err) {
            alert("Please refresh !");
        },
    });
}
getgroupdata();
function loadMore(event) {
  skip = Number(skip) + Number(limit);
  if (docCount >= skip) {
    getgroupdata();
  } else {
    $("#loadMoreButtonId").hide();
  }
}
var delayTimer;
function searchFunction(e) {
    clearTimeout(delayTimer);
    delayTimer= setTimeout(()=>{
        $(".groupdata").html("");
        skip = 0;
        getgroupdata();
        return;
    },400)
    
  }

  let timeout=null;
  function onTypingEvent(e) {
    let ascii,
      key = e.key;
    ascii = key.charCodeAt(0);
  
    let searchData = $("#searchBoxId").val();
    if(timeout){
        clearTimeout(timeout)
    }
    if (ascii != 66) {
      if (searchData.length > 3) {
        timeout=setTimeout(()=>{
          $(".groupdata").html("");
        skip = 0;
        // getgroupdata();
        searchFunction(e)
        return;
        }, 500)
        
      }
    }
    if (searchData.length == 0) {
      $("#loadMoreButtonId").show();
      $(".groupdata").html("");
      skip = 0;
      getgroupdata();
    }
  }




/**************************************************** 
api to delete created group
****************************************************/
$(document).on("click", ".grp_delete", function(e) {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Group!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((isconfirm) => {
        if (isconfirm) {
            $.ajax({
                type: "POST",
                url: "/deletegroup",
                data: { groupId: $(this).attr("group-id") },
                success: function(data) {
                    if (data) {
                        location.reload();
                    }
                },
                error: function(jqXHR, textStatus, err) {
                    alert("Please refresh !");
                },
            });
        }
    });
});

/***************************************************
api to edit  group
***************************************************/
$(document).on("click", ".grp_view", function(e) {
    showLoader();
    $(".table-filter").val('');
    $.ajax({
        type: "POST",
        url: "/editgroup",
        data: { groupId: $(this).attr("group-id") },
        success: function(data) {
            if (data) {
                $('#myModal').modal('show');
                hideLoader();
                $(".removehtml").html("");
                let key = Object.keys(data.data.createdgroup);
                $(".groupname").val(key[0]);
                $(".update_edted_data").attr("usergroup-id", data.data._id);
                for (i = 0; i < data.data.createdgroup[key].length; i++) {
                    $(".userdatamodel").append(`<tr class="lengthcal"
                    userdataId="${data.data.createdgroup[key][i].userId}" data-name ="${data.data.createdgroup[key][i].name}" 
                    data-mail ="${data.data.createdgroup[key][i].mail}" data-phone ="${data.data.createdgroup[key][i].phone}">`

                                            + 
                                            ((data.data.createdgroup[key][i].name == "null" || data.data.createdgroup[key][i].name === undefined || data.data.createdgroup[key][i].name == 'undefined') ? `<td> </td>` :
                                              `<td>${data.data.createdgroup[key][i].name} </td>`) +

                                            ((data.data.createdgroup[key][i].mail == "null" || data.data.createdgroup[key][i].mail === undefined || data.data.createdgroup[key][i].mail == 'undefined') ? `<td> </td>` :
                                              `<td>${data.data.createdgroup[key][i].mail}</td>`) +

                                              ((data.data.createdgroup[key][i].phone == "null" || data.data.createdgroup[key][i].phone === undefined || data.data.createdgroup[key][i].phone == 'undefined') ? `<td> </td>` :
                                               `<td>${data.data.createdgroup[key][i].phone}</td>`) +

                                            `<td  class="dltuser userdataid" data-id = ${data.data.createdgroup[key][i].userId}>&times;</td>
                                        </tr>`);
                }
            }
        },
        timeout: 2000000 ,
        error: function(jqXHR, textStatus, err) {
            alert("Please refresh !");
        },
    });
});
/********************************************/
/**add more user**/
/*******************************************/
$(document).on("click", ".add_more_user", function(e) {
    let phone = $('.instant_phone').val();
    let phoneValidation = /[91]{2}[7-9][0-9]{9}$/;
    let email =$('.instant_email').val() ;
    const emailValidation = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(email=='' && $('.instant_phone').val()==''){
        swal({
            title: 'Email or phone is required' ,
            icon: "warning",
            timer: 3500
        });
        return;
    }else if(email && !emailValidation.test(String(email).toLowerCase())){
      swal({
          title: "Please provide valid email id",
          icon: "warning",
          timer: 3500
      }); 
      return;
    } else if ($(".instant_phone").val() < 0) {
        swal({
            title: "Phone number can not be negative",
            icon: "warning",
            timer: 3500,
        });
      return;
    }else if (phone && !phoneValidation.test(phone)) {
        swal({
            title: "Please select your country code and Enter valid mobile number!",
            icon: "warning",
            timer: 3500,
        });
        return;
    }
     
    $(".userdatamodel").prepend(`<tr class =" lengthcal userdatalist" userdataid="Random${Math.random().toString(36).substr(2, 5)}" data-name="${$(".instant_user").val()}" data-mail="${$(".instant_email").val()}" data-phone="${$(".instant_phone").val()}">
        <td  class="userdataname" data-name="${$(".instant_user").val()}">
          <label class="com_grp_chkbox">
            <input type="checkbox" checked="checked" class="adduserckbox userckbox">
            <span class="grp_checkmark"></span>
          </label>${$(".instant_user").val()}
        </td>
        <td class="userdatamail" data-mail="${$(".instant_email").val()}">${$(".instant_email").val()}</td>
        <td class="userdataphone" data-phone="${$(".instant_phone").val()}">${$(".instant_phone").val()}</td>
        <td class="dltuser userdataid" data-id="Random${Math.random().toString(36).substr(2, 5)}">&#10005;</td> </tr>`);
    $(".instantuserdata").val("");
});
/**************************************/
/**for group update**/
/*************************************/
$(document).on("click", ".update_edted_data", function(e) {
    let grpValidation = /^[a-zA-Z-0-9_]+$/;
    let grpname = $(".groupname").val();
    grpname=grpname.split(' ').join('_');
    $(".groupname").val(grpname)

    if (grpname == "") {
        alert("Group Name cannot be blank");
        return;
    }
    let updategrouparray = {},
        dataobj,
        groupId;
    groupId = $(this).attr("usergroup-id");
    updategrouparray[$(".groupname").val()] = [];
    $(".lengthcal").each(function() {
        // if(this.checked){
        datobj = {
            mail: $(this).closest("tr").attr("data-mail"),
            name: $(this).closest("tr").attr("data-name") == "unknown user" ?
                "" :
                $(this).closest("tr").attr("data-name"),
            phone: $(this).closest("tr").attr("data-phone"),
            userId: $(this).closest("tr").attr("userdataid"),
        };
        console.log("datobj === ", datobj);
        updategrouparray[$(".groupname").val()].push(datobj);
        // }
    });
    if (updategrouparray[$(".groupname").val()].length < 1) {
        alert("Select Minium One User");
        return;
    }else if (grpname && !grpValidation.test(String(grpname))) {
        swal({
            title: "Groupname should not contain spaces and special characters. Underscore (_) is allowed.",
            icon: "warning",
            timer: 3500,
        });
   }else{
    $.ajax({
        type: "POST",
        url: "/updategroup",
        data: { groupId: groupId, updateddata: updategrouparray, groupName: grpname },
        success: async function(data) {
            if (data.status == "Succeesss") {
                await swal({
                    title: data.msg,
                    icon: "success",
                    buttons: true,
                    dangerMode: true,
                });
                location.reload();
            } else {
                await swal({
                    title: data.msg,
                    icon: "error",
                    buttons: false,
                    dangerMode: true,
                });
            }
        },
        error: function(jqXHR, textStatus, err) {
            alert("Please refresh !");
        },
    });
}
});
/**************************************/
/**for group update**/
/*************************************/

$(document).on("click", ".userdataid", function(e) {
    if ($(".lengthcal").length < 2) {
        return;
    }
    $(this).parent("tr").remove();
});

$(document).on("click", ".headingckbox", function() {
    if (this.checked) {
        $(".adduserckbox").attr("checked", true);
    } else {
        $(".adduserckbox").attr("checked", false);
    }
});

$(document).on("click", ".modalclosecls", async function() {
    await swal({
        title: "Warning",
        text: `Your Changes will be lost if You have not Saved it`,
        icon: "error",
        buttons: true,
        dangerMode: true,
    }).then((isconfirm) => {
        if (isconfirm) {
            $("#myModal").modal("hide");
        }
    });
});

$(document).on("click", ".closemodal2", async function() {
    $("#viewmodal").modal("hide");
});

/***************************************************
view edit group data api call and rendering
***************************************************/
$(document).on("click", ".view_group_data", function(e) {
    showLoader();
    $(".table-filter").val('');
    $.ajax({
        type: "POST",
        url: "/editgroup",
        data: { groupId: $(this).attr("group-id") },
        success: function(data) {
            if (data) {
                $(".removehtml").html("");
                $('#viewmodal').modal('show');
                hideLoader();
                let key = Object.keys(data.data.createdgroup);
                $(".groupname").val(key[0]);
                $(".update_edted_data").attr("usergroup-id", data.data._id);
                for (i = 0; i < data.data.createdgroup[key].length; i++) {
                    $(".viewDataModal").append(`<tr class="lengthcal2"
                    userdataId="${data.data.createdgroup[key][i].userId}" data-name ="${data.data.createdgroup[key][i].name}" 
                    data-mail ="${data.data.createdgroup[key][i].mail}" data-phone ="${data.data.createdgroup[key][i].phone}">`

                                            + 
                                            ((data.data.createdgroup[key][i].name == "null" || data.data.createdgroup[key][i].name === undefined || data.data.createdgroup[key][i].name == 'undefined') ? `<td> </td>` :
                                              `<td>${data.data.createdgroup[key][i].name} </td>`) +

                                            ((data.data.createdgroup[key][i].mail == "null" || data.data.createdgroup[key][i].mail === undefined || data.data.createdgroup[key][i].mail == 'undefined') ? `<td> </td>` :
                                              `<td>${data.data.createdgroup[key][i].mail}</td>`) +

                                              ((data.data.createdgroup[key][i].phone == "null" || data.data.createdgroup[key][i].phone === undefined || data.data.createdgroup[key][i].phone == 'undefined') ? `<td> </td>` :
                                               `<td>${data.data.createdgroup[key][i].phone}</td>`) +

                                            `<td  class="dltuser userdataid" data-id = ${data.data.createdgroup[key][i].userId}>&times;</td>
                                        </tr>`);
                }
            }
        },
        timeout: 2000000 ,
        error: function(jqXHR, textStatus, err) {
            alert("Please refresh !");
        },
    });
});

(function() {
	'use strict';

var TableFilter = (function() {
 var Arr = Array.prototype;
 console.log('arrr',Arr)
		var input;
  
		function onInputEvent(e) {
			input = e.target;
			var table1 = document.getElementsByClassName(input.getAttribute('data-table'));
 console.log('table1',table1)

			Arr.forEach.call(table1, function(table) {
				Arr.forEach.call(table.tBodies, function(tbody) {
					Arr.forEach.call(tbody.rows, filter);
				});
			});
		}

		function filter(row) {
			var text = row.textContent.toLowerCase();
       //console.log(text);
      var val = input.value.toLowerCase();
      //console.log(val);
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		}

		return {
			init: function() {
				var inputs = document.getElementsByClassName('table-filter');
				Arr.forEach.call(inputs, function(input) {
					input.oninput = onInputEvent;
				});
			}
		};
 
	})();
  
 TableFilter.init(); 
})();

hideLoader();
function showLoader(){
    $('#loaderDivContainer').removeClass("hide-loader");
    $('.content').hide(); 
}
function hideLoader(){
    $('#loaderDivContainer').addClass("hide-loader");
    $('.content').show();
}
