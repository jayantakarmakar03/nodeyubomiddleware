const db = require("../models/all-models");
const fs = require("fs");

module.exports = {
templateload: templateload,
botsavepreview: botsavepreview,
};
/** for template load */
async function templateload(req, res) {
let updateddata = await db.Custombot.findOne(
  { clientId: req.user.clientId },
  { botcss: 1, logoupload: 1 }
);
if (updateddata != null) {
  return res.json({ data: updateddata });
} else {
  return res.json({ data: "error in data updation" });
}
}
/** for bot customisation save action */
async function botsavepreview(req, res) {
let cssdata = JSON.parse(req.body.cssdata),
  templatelogo;
let botlogo = req.file;
/**************remove logo ******************/
if (req.file) {
  templatelogo = botlogo.originalname;
} else {
  templatelogo = "";
  await fs.copyFile(
    "./public/dist/img/chatbubble.png",
    "./public/img/" + req.user.clientId + ".png",
    (err) => {
      if (err) throw err;
    }
  );
}
/***************************************************/
const getQuery = await db.Custombot.findOne({
  clientId: req.user.clientId,
});
if (!getQuery) {
  let data = new db.Custombot({
    clientId: req.user.clientId,
    botcss: cssdata,
    logoupload: templatelogo,
  });
  await data.save();
} else {
  await db.Custombot.updateOne(
    { clientId: req.user.clientId },
    { $set: { botcss: cssdata, logoupload: templatelogo } }
  );
}
fs.copyFile(
  "./public/assets/master.bot.css",
  "./public/assets/" + req.user.clientId + ".bot.css",
  (err) => {
    if (err) throw err;
  }
);
if (botlogo) {
  await fs.copyFile(
    "./public/dist/img/" + req.file.filename,
    "./public/img/" + req.user.clientId + ".png",
    (err) => {
      if (err) throw err;
    }
  );
  fs.unlink("./public/dist/img/" + req.file.filename, (err) => {
    if (err) {
      throw err;
    }
  });
}
var customizedata =
  ".hy-icon {background:  " +
  cssdata.iconbackgroundcolor +
  ";}.contact-profile{  background: " +
  cssdata.headbackgroundcolor +
  ";}#frame .content .message-input .wrap button{ background: " +
  cssdata.iconbackgroundcolor +
  ";}.tag-line.sub{color: " +
  cssdata.headtext1 +
  " ;}.tag-line{color: " +
  cssdata.headtext1 +
  ";}.close-img svg {fill:" +
  cssdata.closebuttoncolor +
  ";}#frame .content .messages ul li.sent p{background:" +
  cssdata.speechbubble +
  ";}#frame .content .messages ul li.sent p{ color:" +
  cssdata.chatbottext +
  ";}#frame .content .messages ul li.sent p a, #frame .content .messages ul li.sent p a:hover, #frame .content .messages ul li.sent p a:active, #frame .content .messages ul li.sent p a:visited { color:" +
  cssdata.chatbottext +
  "; text-decoration: underline !important;font-weight: 700 !important" +
  ";}#frame .content .messages ul li.replies p{background:" +
  cssdata.userspeech +
  ";}#frame .content .messages ul li.replies p{color:" +
  cssdata.usertext +
  ";}#frame .content .messages ul li.replies p a, #frame .content .messages ul li.replies p a:hover, #frame .content .messages ul li.replies p a:active, #frame .content .messages ul li.replies p a:visited { color:" +
  cssdata.usertext +
  "; text-decoration: underline !important;font-weight: 700 !important" +
  ";}.iframe-box {border: 1px solid " +
  cssdata.botborder +
  ";background:" +
  cssdata.messagechatscreeen +
  ";}#frame{background:" +
  cssdata.messagechatscreeen +
  ";}.outerCircle {border: 4px solid " +
  cssdata.botborder +
  ";border-right: 4px solid transparent;border-left: 4px solid transparent; }.innerCircle {border: 3px solid " +
  cssdata.botborder +
  ";border-left: 3px solid transparent;border-right: 3px solid transparent; } #fb-rendered-form button, #popUp-body-Inner button {background: " +
  cssdata.iconbackgroundcolor +
  ";} #fb-rendered-form, .popUp-body-Inner {box-shadow: 0px 0px 4px 1px " +
  cssdata.botborder +
  ";} .contact-profile .contextmenu svg {fill: "+
  cssdata.closebuttoncolor +
  ";} #frame .content .messages ul li.sent p a, #frame .content .messages ul li.sent p a:hover, #frame .content .messages ul li.sent p a:active, #frame .content .messages ul li.sent p a:visited {background: "+cssdata.chatbottext+"; color: "+cssdata.speechbubble+" !important;border: "+cssdata.chatbottext+";}";

fs.appendFile(
  "./public/assets/" + req.user.clientId + ".bot.css",
  customizedata,
  function (err, data) {
    if (err) throw err;
  }
);
let savedata = await db.Client.updateOne(
  { clientId: req.user.clientId },
  { $set: { "renderData.msg": cssdata.headtextfirst } }
);
return res.json({ msg: "bot updated successfully" });
}
