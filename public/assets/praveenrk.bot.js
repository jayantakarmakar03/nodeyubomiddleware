document.onreadystatechange = function(e){
    if ((document.readyState === 'interactive' || document.readyState === 'complete') && document.getElementById("bootm-box")==undefined){
		(function() {
			const domain = "https://yubo.yugasa.org"
			const clientId="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6InByYXZlZW5yayIsImlhdCI6MTY4MDg1ODUzMH0.DKrpQONVk_ROd24EnW8_xQppdX4889kuZYhemSd43kU";
			
			let host = location.hostname;
			let url=location.href;

			let parent = document.createElement('div');
			parent.setAttribute('class', 'iframe-box');
			parent.setAttribute('id', 'iframe-box');

			let bootm = document.createElement('div');
			bootm.setAttribute('class', 'bootm-box');
			bootm.setAttribute('id', 'bootm-box');

			let chatstart = document.createElement('div');
			chatstart.setAttribute('class', 'chat-start bounce-top');

			let closeimg = document.createElement('div');
			closeimg.setAttribute('class', 'close-img');
			closeimg.innerHTML='<svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg>'

			let outercircle = document.createElement('div');
			outercircle.setAttribute('class', 'outerCircle');

			let innercircle = document.createElement('div');
			innercircle.setAttribute('class', 'innerCircle');

			let icon = document.createElement('div');
			icon.setAttribute('class', 'hy-icon');

			let bubbleimg  = document.createElement('img');
			bubbleimg.setAttribute('class', 'bubble-img');
			bubbleimg.src = domain+"/img/chat-bubble.svg";

			icon.appendChild(bubbleimg);

			chatstart.appendChild(outercircle)
			chatstart.appendChild(innercircle)
			chatstart.appendChild(icon)

			bootm.appendChild(chatstart);
		    bootm.appendChild(closeimg);
		    let iframe = document.createElement('iframe');
		   	iframe.setAttribute('id', 'frame-lode');
		   	iframe.setAttribute('allow','microphone');
		    iframe.src = domain+"/chatWindow?clientId="+clientId+"&host="+host+"&url="+url;

		  	let powered = document.createElement("div");
		  	powered.setAttribute("class","poweredbyyugasa");
		  	let poweredlink = document.createElement("a");
		  	poweredlink.setAttribute("href","https://helloyubo.com/");
		  	poweredlink.setAttribute("target","_blank");
		  	poweredlink.setAttribute("title","Building Blocks for Building Bots");
		  	let poweredbytext = document.createTextNode("Powered by ");
		  	let hyperlinktext = document.createTextNode("Yugasa Bot");
		  	poweredlink.appendChild(hyperlinktext);
		  	powered.appendChild(poweredbytext);
		  	powered.appendChild(poweredlink);

		    parent.appendChild(bootm);
		    parent.appendChild(iframe);
			parent.appendChild(powered);

		    document.body.appendChild(parent);
			document.getElementById("bootm-box").addEventListener("click",function(){
				document.getElementById("iframe-box").classList.toggle("open-chat-window");
			});
			launchBot();
		})();
    }
};

function launchBot() {
	// First check, if localStorage is supported.
	if (window.localStorage) {
		// Get the expiration date of the previous popup.
		var nextPopup = localStorage.getItem( 'nextyubo' );

		if (nextPopup > new Date()) {
			return;
		}
		// Store the expiration date of the current popup in localStorage.
		var expires = new Date();
		expires = expires.setHours(expires.getHours() + 24);

		localStorage.setItem( 'nextyubo', expires );
	}

	setTimeout(function(){
		if(document.getElementById("iframe-box").classList.contains("open-chat-window")){
			return;
		}
		document.getElementById("bootm-box").dispatchEvent(new Event('click'));
	},5000);
};