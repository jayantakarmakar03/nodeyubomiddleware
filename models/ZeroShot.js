var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
    mongoose.connect(require('./connection-string'));
}

var newSchema = new Schema({
    'clientId': { type: String },
    'tagName': { type: String },
    'question': {type: String}, 
    'answer': {type: String},
    'status': { type: Boolean, default: true },
    'createdAt': { type: Date, default: Date.now },
    'updatedAt': { type: Date, default: Date.now },
    'analyticStatus': { type: Boolean, default: false }
});

newSchema.pre('save', function(next) {
    this.updatedAt = Date.now();
    next();
});

newSchema.pre('update', function() {
    this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
    this.update({}, { $set: { updatedAt: Date.now() } });
});


module.exports = mongoose.model('ZeroShot', newSchema);