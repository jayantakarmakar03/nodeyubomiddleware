document.onreadystatechange = function(e){
    if ((document.readyState === 'interactive' || document.readyState === 'complete') && document.getElementById("bootm-box")==undefined){
        (function() {
            const domain = "https://yubo.yugasa.org";
            const clientId="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6IlBhbmthal9CdWNrc2hleSIsImlhdCI6MTYxOTg0OTg0OX0.8-Fl9mAlA0_x2o18MUAopYj7orNDtanDfo5LPz055Ow";

            let host = location.hostname;
            let url=location.href;
           
            var parent = document.createElement('div');
            parent.setAttribute('class', 'iframe-box');
            parent.setAttribute('id', 'iframe-box');

            var bootm = document.createElement('div');
            bootm.setAttribute('class', 'bootm-box');
            bootm.setAttribute('id', 'bootm-box');

            var chatstart = document.createElement('div');
            chatstart.setAttribute('class', 'chat-start bounce-top');

            var closeimg = document.createElement('div');
            closeimg.setAttribute('class', 'close-img');
        	closeimg.innerHTML='<svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg>'

            //    var refreshdiv = document.createElement('div');
            //    refreshdiv.setAttribute('class', 'refresh');
            // var refreshimg  = document.createElement('img');
            // refreshimg.src=domain+"/img/refresh.png";
            // refreshimg.setAttribute("title","Restart")

            // refreshdiv.appendChild(refreshimg);


            var outercircle = document.createElement('div');
            // outercircle.setAttribute('class', 'outerCircle');

            var innercircle = document.createElement('div');
            // innercircle.setAttribute('class', 'innerCircle');

            var icon = document.createElement('div');
            icon.setAttribute('class', 'widget-icon');
            var bubbleAnimation = document.createElement('div');
            bubbleAnimation.setAttribute('class', 'bubble-animation');
            var bubbleimg = document.createElement('img');
            bubbleimg.setAttribute('class', 'bubble-img');
            bubbleimg.src = domain + "/img/pankajbuckshey_ASK-bot.png";
            // icon.appendChild(bubblespan);
            icon.appendChild(bubbleAnimation);
            bubbleAnimation.appendChild(bubbleimg);

            chatstart.appendChild(outercircle)
            chatstart.appendChild(innercircle)
            chatstart.appendChild(icon)

            bootm.appendChild(chatstart);
            bootm.appendChild(closeimg);
            // parent.appendChild(refreshdiv);


            var iframe = document.createElement('iframe');
            iframe.setAttribute('id', 'frame-lode');
            // iframe.style.display = "none";
        	iframe.src = domain+"/chatWindow?clientId="+clientId+"&host="+host+"&url="+url;
            parent.appendChild(bootm);
            parent.appendChild(iframe);
            document.body.appendChild(parent);
            document.getElementById("bootm-box").addEventListener("click", function() {
                document.getElementById("iframe-box").classList.toggle("open-chat-window");

            });
            launchBot();
        })();
    }
};

function launchBot() {
    // First check, if localStorage is supported.
    if (window.localStorage) {
        // Get the expiration date of the previous popup.
        var nextPopup = localStorage.getItem( 'nextyubo' );

        if (nextPopup > new Date()) {
            return;
        }
        // Store the expiration date of the current popup in localStorage.
        var expires = new Date();
        expires = expires.setHours(expires.getHours() + 24);

        localStorage.setItem( 'nextyubo', expires );
    }

    setTimeout(function(){document.getElementById("bootm-box").dispatchEvent(new Event('click'));},5000)
};