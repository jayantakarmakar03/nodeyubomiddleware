// *****************get user group data************************ //
// *********************************************************** //
$.ajax({
    type: "POST",
    url: "/getWhatsappGroupData",
    data: { group: 'true' },
    success: function (data) {
        if (data) {
            // loop for createdgroup list in createdgroup table
            for (i = 0; i < data.groupcreated.length; i++) {
                let key = Object.keys(data.groupcreated[i].createdgroup);
                $('.groupdata').append(`<div class="row">
                    <span class="my-group-span"></span>
                <div class="col-lg-8 col-md-8 col-sm-7 com-xs-12">
                    <div class="my-group-name">
                        <p>${key[0]}</p>
                        <h3>${data.groupcreated[i].createdgroup[key].length} contacts</h3>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-5 com-xs-12">
                    <div class="my-group-action">
                        <button class="grp_view" group-id=${data.groupcreated[i]._id} data-toggle="modal" data-target="#myModal">Update</button>
                        <span class="my-group-action_span">|</span>
                        <button class="grp_delete" group-id=${data.groupcreated[i]._id}>Delete</button>
                    </div>
                </div>
            </div>`)
            }
        }
    }, error: function (jqXHR, textStatus, err) {
        alert('Please refresh !');
    }
})
/***************************************************
api to edit  group
***************************************************/
$(document).on("click", ".grp_view", function (e) {
    $.ajax({
        type: "POST",
        url: "/editWhatsappGroup",
        data: { groupId: $(this).attr('group-id') },
        success: function (data) {
            if (data) {
                $('.removehtml').html('')
                let key = Object.keys(data.data.createdgroup);
                $('.groupname').val(key[0])
                $('.update_edted_data').attr('usergroup-id', data.data._id)
                for (i = 0; i < data.data.createdgroup[key].length; i++) {
                    
                    $('.userdatamodel').append(`<tr class="lengthcal"
                    userdataId="${data.data.createdgroup[key][i].userId}" data-name ="${data.data.createdgroup[key][i].name}" data-mail ="${data.data.createdgroup[key][i].mail}">
                                            <td>${data.data.createdgroup[key][i].name}</td>
                                            <td>${data.data.createdgroup[key][i].mail}</td>
                                            <td  class="dltuser userdataid" data-id = ${data.data.createdgroup[key][i].userId}>&times;</td>
                                        </tr>`)
                }
            }
        }, error: function (jqXHR, textStatus, err) {
            alert('Please refresh !');
        }
    })
})
// ******on  presss close button of modal*********************************************/
$(document).on('click','.modal_close',async function(){
    await swal({
        title: "Warning",
        text: `Your Changes will be lost if You have not Saved it`,
        icon: "error",
        buttons: true,
        dangerMode: true,
    }).then((isconfirm) => {
        if (isconfirm) {
            $('#myModal').modal('hide');
        }
      });
    
})  
/**************************************/
    /**for group update**/
/*************************************/
$(document).on("click", ".userdataid", function (e) {
    if ($('.lengthcal').length < 2) {
        return;
    }
    $(this).parent('tr').remove()
})
/********************************************/
        /**add more user**/
/*******************************************/
$(document).on('click',".add_more_user",function(e){
    if($('.instant_email').val()==''){
    alert('EmailId is mandatory')
    return ;
    } 
    $('.userdatamodel').prepend(`<tr class =" lengthcal userdatalist" userdataid="Random${Math.random().toString(36).substr(2, 5)}" data-name="${$('.instant_user').val()}" data-mail="${$('.instant_email').val()}">
    <td  class="userdataname" data-name="${$('.instant_user').val()}"><label class="com_grp_chkbox"><input type="checkbox" checked="checked" class="adduserckbox userckbox"><span class="grp_checkmark"></span></label>${$('.instant_user').val()}</td>
    <td class="userdatamail" data-mail="${$('.instant_email').val()}">${$('.instant_email').val()}</td>
    <td class="dltuser userdataid" data-id="Random${Math.random().toString(36).substr(2, 5)}">&#10005;</td> </tr>`);
    $('.instantuserdata').val('');

})
/**************************************/
    /**for group update**/
/*************************************/
$(document).on("click", ".update_edted_data", function (e) {
    if ($('.groupname').val() == '') {
        alert('Group Name cannot be blank');
        return;
    }
    let updategrouparray = {}, dataobj, groupId;
    groupId = $(this).attr('usergroup-id')
    updategrouparray[$('.groupname').val()] = [];
    $('.lengthcal').each(function () {
            // if(this.checked){
                datobj = {
                    mail: $(this).closest('tr').attr('data-mail'), 
                    name: $(this).closest('tr').attr('data-name')=='unknown user' ? '' :$(this).closest('tr').attr('data-name'),
                    userId: $(this).closest('tr').attr('userdataid')
                }
        updategrouparray[$('.groupname').val()].push(datobj)
            // }
    })
    if(updategrouparray[$('.groupname').val()].length<1){
            alert('Select Minium One User');
            return;
            }

        console.log('updategrouparray====',updategrouparray)
        $.ajax({
        type: "POST",
        url: "/updateWhatsappGroup",
        data: { groupId: groupId, updateddata: updategrouparray },
        success: async function (data) {
            if (data.status == 'Succeesss') {
                await swal({
                    title:data.msg,
                    icon: "success",
                    buttons: true,
                    dangerMode: true,
                })
                location.reload();
            } else {
                await swal({
                    title:data.msg,
                    icon: "error",
                    buttons: false,
                    dangerMode: true,
                })
            }
        }, error: function (jqXHR, textStatus, err) {
            alert('Please refresh !');
        }
    })
})
/**************************************************** 
api to delete created group
****************************************************/
$(document).on("click", ".grp_delete", function (e) {
     swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Group!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
    }).then((isconfirm) => {
    if (isconfirm) {
        $.ajax({
            type: "POST",
            url: "/deleteWhatsappGroup",
            data: { groupId: $(this).attr('group-id') },
            success: function (data) {
                if (data) {
                    location.reload();
                }
            }, error: function (jqXHR, textStatus, err) {
                alert('Please refresh !');
            }
        })
      }
    })
})

{/* <td>${data.data.createdgroup[key][i].mail}</td> */}
