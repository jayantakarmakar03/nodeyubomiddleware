function openModal(current){
    agentId = $(current).attr("agentIdForNotification");
     let allAgeentNotification = getAgentNotificationFromSession();
     let notificationList1 = allAgeentNotification.filter(item => item.RequestFromAgentId == agentId);
     let notificationList = notificationList1.filter(item => item.requestToAgentId == getAgentIdFromSession());
     if(notificationList.length>0){
      $(".agentNotificationData").html("");
        for (const iterator of notificationList) {
          html = `
          <div class="modal-body">
          <div class="col-12">
            <div  div class="row">                                                               
              <div class="col-6">
                  <span> User Name</span>
              </div>
              <div class="col-6">
                  <span class="requestedUserName"> ${iterator.userName}</span>
              </div>
               <div class="col-6">
                  <span> Agent Name</span>
              </div>
              <div class="col-6">
                  <span class  ="requestedAgentName"> ${iterator.RequestFromAgentName} </span>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" 
                  class="btn btn-danger agent_modal_close" 
                  data-bs-dismiss="modal"
                  onclick = "agentisbusy(this)"
                  userId = ${iterator.userId}
                  RequestFromAgentId = ${iterator.RequestFromAgentId}
                  clientId = ${iterator.clientId}
                  requestToAgentId = ${iterator.requestToAgentId}
                  userName = ${iterator.userName} >I am busy
          </button>
          <button type="button" 
                  id="agentControlButton"
                  onclick = "acceptRequestOfControlTransfer(this)"
                  userId = ${iterator.userId}
                  RequestFromAgentId = ${iterator.RequestFromAgentId}
                  clientId = ${iterator.clientId}
                  requestToAgentId = ${iterator.requestToAgentId}
                  userName = ${iterator.userName}
                  class="btn btn-success agent_transfer_btn" 
                  data-dismiss="modal">
                  Take Control
          </button>
        </div>`;
          $('.agentNotificationData').append(html);
        }
     }
    
    $('#myModal').modal('show');
}
function acceptRequestOfControlTransfer(_this){
    // console.log('__this',_this)
    requestfromagentid = $(_this).attr("requestfromagentid");
    clientid = $(_this).attr("clientid");
    requesttoagentid = $(_this).attr("requesttoagentid");
    userid = $(_this).attr("userid");
    userName = $(_this).attr("username");
    
    // console.log('requestfromagentid',requestfromagentid);
    // console.log('clientid',clientid);
    // console.log('requesttoagentid',requesttoagentid);
    // console.log('userid',userid);
    // console.log('userName 222',userName);
    $('#myModal').modal('hide');
    $(".userNameCenter").html(userName);
    
    $(".contact-profile h2").html($(this).find(".name").html());
    $(".userchatData").html("");
    $("#contacts li.contact").removeClass("active");
    // $(this).addClass("active");
    // $(this).find(".preview").css({ color: "black", "font-weight": "400" });
    // $(this).find(".name").css({ color: "black", "font-weight": "600" });
    let data = {
        clientId: clientid,
        userId: userid,
    };
    $(".userchatData").attr("userId",userid );
    localStorage.setItem("selected_user_id", userid);
    $.ajax({
      type: "POST",
      url: "/agent/agentUserChat",
      data: data,
      success: function (data) {
        if (data.statusCode == 200) {
          let userSession = data.res[0].session;
          if (userSession && userSession.name) {
            $("#user_name").val(userSession.name);
            $("#user_email").val(userSession.email);
            $("#user_phone").val(userSession.phone);
          } else {
            $("#user_name").val("");
            $("#user_email").val("");
            $("#user_phone").val("");
          }
          let agentId = getAgentIdFromSession();
          if (data.res[0].agentCurrentConrol) {
            $("#takeControlLabe ").show();
            data.res[0].agentCurrentConrol.currentControl
              ? $(".controlText").text("Leave Control")
              : $(".controlText").text("Take Control");
            if (data.res[0].agentCurrentConrol.agentId == agentId) {
              $("#checkboxSlider").show();
              $("#checkBoxBtn").prop(
                "checked",
                data.res[0].agentCurrentConrol.currentControl
              );
              $(".send-message").show();
            } else if (
              data.res[0].agentCurrentConrol.agentId != agentId &&
              data.res[0].agentCurrentConrol.currentControl === true
            ) {
              $(".controlText").text("Already in Chat with Agent");
              $("#checkboxSlider").hide();
              $("#checkBoxBtn").prop(
                "checked",
                data.res[0].agentCurrentConrol.currentControl
              );
              $(".send-message").hide();
            } else {
              $("#checkboxSlider").show();
              $("#checkBoxBtn").prop(
                "checked",
                data.res[0].agentCurrentConrol.currentControl
              );
              $(".send-message").hide();
            }
          } else {
            $("#checkboxSlider").show();
          }
          let userchat = data.res[0].chats;
          if (data.res[0].session) {
            $(".userName").html(data.res[0].session.name);
            $(".userMail").html(data.res[0].session.email);
          }
          if (data.res[0].location) {
            let completeLocation =
              data.res[0].location.city + ", " + data.res[0].location.state;
            $(".userLocation").html(completeLocation);
          }
  
          var html;
          $(".userNamedata").html("");
          for (const iterator of userchat) {
            let format_Date = moment(new Date(iterator.createdAt)).format(
              "DD/MM/YYYY"
            );
            iterator.date = format_Date;
            now = moment(new Date(iterator.createdAt));
            iterator.time = now.hour() + ":" + now.minutes();
  
            if (iterator.messageType == "outgoing") {
              html = ` <li class="replies">
                            <img src="/dist/files/favicon.png" alt="favicon">
                            <p>
                                ${iterator.text}
                                <span class="date_time" style="position: relative;font-size: 12px; display: block; text-align: right;bottom: -5px;">
                                    ${iterator.date} at ${iterator.time} 
                                </span>
                            </p>
                        </li>`;
                        $(".userchatData").append(html);
             } else {
                html = `<li class="sent">
                    <div class="wrap">
                        <div class="circle" style ="background :${randomColorGenerator(userName[0].toUpperCase())}">
                            <div class="chat_name">
                            <h2 class="chat_name_head"> ${userName[0].toUpperCase()}</h2>
                            </div>
                        </div>
                        <p>
                            ${iterator.text}
                            <span class="date_time" style="position: relative;font-size: 12px; display: block;text-align: right;bottom: -5px;">
                            ${iterator.date} at ${iterator.time} 
                            </span>
                        </p>
                    </div>
                </li>`;
            $(".userchatData").append(html);
            }
          }
          $(".new-content").removeClass("col-sm-9").addClass("col-sm-6");
          $("#frame .content .contact-profile").css({ background: "#fff" });
          $(".contact-profile h2").css({ display: "block" });
          // $(".messages").animate({ scrollTop: $(document).height() }, "slow");
          $(".detail_hide").css({ display: "block" });
          $(".messages").animate({ scrollTop: $(".messages").prop("scrollHeight") },0);
  
        } else {
          console.log("Error occured", data);
        }
      },
      error: function (jqXHR, textStatus, err) {
        alert("Please refresh !");
      },
    });

    /**********************************************************
            get notified user id array data from local storage in case of 
            page relode notification will still visible   in the left side user panel
      **************************************************************/
      let localstorageNotifiedUSerList =[];
      let NotifiedUserIds =localStorage.getItem('NotifiedUserIds');
      if(NotifiedUserIds == null || NotifiedUserIds == 'undefined'|| NotifiedUserIds == '' ){
        let newArray = [];
        localStorage.setItem('NotifiedUserIds',JSON.stringify(newArray));
      }else{
        localstorageNotifiedUSerList = JSON.parse(NotifiedUserIds);
      }
    // ******************************************************************//

    // *********pull user at the top when accept the control ***************************//
    if (clientid == getClientFromSession()) {
        if(($('.selectuser').length>1) && $(".myChatUserList").attr("alluserchatlist") =='true'){
          $('li[data-userid="' + userid + '"]').remove();
        }
        let message ='Taking control'
        let userarray = [];
        $(".selectuser").each(function () {
          userarray.push($(this).attr("data-userid"));
        });
        if(userarray.includes(userid)){
          $(`#${userid}`).html(message);
          $(`#${userid}`).css({ "font-weight": "400" });
          $(`#${userid}`).prev().css({ "font-weight": "400" });
        } else {
          if($(".myChatUserList").attr("alluserchatlist") =='true'){
            let html =
            `<li class="contact selectuser" data-clientid ="${clientid}" data-userid ="${userid.trim()}">    
                <div class="wrap"> 
                  <!-- <span class="contact-status online"></span> -->
                  <div class="circle" style ="background :${randomColorGenerator(userName[0].toUpperCase()) }">
                    <div class="chat_name">
                      <h2 class="chat_name_head"> ${userName[0].toUpperCase()}</h2>
                    </div>
                  </div>
                  <div class="meta">
                    <p class="name">${userName}</p>
                    <p class="preview" id="${userid}">
                      ${message}
                    </p>`
                    if(localstorageNotifiedUSerList.includes(userid)){
                      html += ` <div class="contact_sec gotNotificationClass" data-userid ="${userid.trim()}" >
                                  <img class="notify_img" src = "/dist/img/red_notification.png" style="height:30px; width:30px; border-radius: initial; margin: auto; width: 25px; height: 25px;">
                                </div>` 
                    } 
                  `</div>
                </div>
              </li>`;
              $(".userList").prepend(html);
          }
        }
        $(".messages").animate({ scrollTop: $(".messages").prop("scrollHeight") },0);
      
    }
    /*****************************************************
   * requested agent taking control and current agent leaving control functionality automatically take 
     control on click of take control button do changes on databaseand update user  chat data that agent taking control  
    ******************************************************/
    let agentName = getAgentName();
    socket.emit("join", userid);
    socket.emit("sendMessageInRoom", {
      userId: userid,
      message: ` ${agentName} taking control now`,
      control: "yes",
      clientId: getClientFromSession(),
      agentName:agentName,
      avatar: "https://helloyubo.com/avatar.png"
    });
    $.ajax({
      type: "POST",
      url: "/agent/agentTakeControl",
      data: {
        userId: userid,
        agentCurrentConrol: {
          agentId: requesttoagentid,
          currentControl: true,
        },
        chatControl: 1,
      },
      timeout: 10000,
      success: function (resp) {
        if (resp) {
          if (resp.statusCode === 200) {
            $(".send-message").show();
            socket.emit("emitSameAgentData", {
              clientId: getClientFromSession(),
              userId: userid,
              agentId: getAgentIdFromSession(),
              control: true
            });
            socket.emit("onAcceptAgentControlRequest", {
              requestFromAgentId: requestfromagentid,
              clientId: getClientFromSession(),
              requestToAgentId: requesttoagentid,
              userId: userid,
              userName: userName
            });

            socket.emit("emitDataOnTakeControl", {
                clientId: getClientFromSession(),
                userId: userid,
                agentId: requesttoagentid,
            });
          } else {
            console.log(resp.message);
            alert(resp.message);
          }
        } else {
          console.log("error occures");
        }
      },
      error: function (jqXHR, textStatus, err) {
        alert("Something went wrong, please refresh the page", err);
      },
    });
   /*****************************************************
     on take control remove bell icon from agent list 
    ******************************************************/
    let data4 = "[agentIdForNotification="+"'"+requestfromagentid+"']"
    // console.log('data4 66',data4);
    let elementData = document.querySelectorAll(data4);
    // console.log('elementData 66',elementData);
    let agentNotificationData =getAgentNotificationFromSession();
 
    if(agentNotificationData == null || agentNotificationData == 'undefined'|| agentNotificationData == '' ){
      let newArray = [];
      newArray.push(data);
      localStorage.setItem('agentNotificationData',JSON.stringify(newArray));
    }else{
      let filterData = agentNotificationData.filter(ele=> ele.requesttoagentid != requesttoagentid && ele.userId != userid )
      localStorage.setItem('agentNotificationData',JSON.stringify(filterData));
    }
    agentNotificationData =getAgentNotificationFromSession();
    if(agentNotificationData.length == 0){
        elementData[0].style.display='none'
    }


}
/*************************************
  on click/select a user manage all things here like agent listing , notification comes 
  form other agent stored in session the radio button also manage for sent request foragent
**********************************************************/
function hideSelectuser(_this){
  
  $(".new_chat_color2").hide()
  $('.new-sidepanel2').hide();
  $('.new-content2').show()
  console.log('_this   8',_this)

  let myRequestsToAgents =localStorage.getItem('myRequestsToAgents');
  let getMyRequestsToAgentFromSession = JSON.parse(myRequestsToAgents);
  let agentdata = humanAgentList ;
  agentdata = agentdata.filter(ele=>ele._id != getAgentIdFromSession());
  agentListExceptMe = agentdata;
  // await localStorage.setItem('access-token',data.accessToken);
  totalAgent = agentdata.length;
  agentdata.forEach(el=>{
    let lastname = el.name.split(' ')
    if(lastname[1])
        el.lastname = lastname[1];
  })
  $('.agent_list').html('')
  let selected_user_id =  $(_this).attr("data-userid");

  for (const iterator of agentdata) {
    console.log('iterator')
    let filteredData =[];
    if(getMyRequestsToAgentFromSession){
      filteredData = getMyRequestsToAgentFromSession.filter((ele)=>{
      });
      filteredData = getMyRequestsToAgentFromSession.filter(ele=>ele.userId == selected_user_id && ele.requestToAgentId ==iterator._id );
    }
    checkValue = "unchecked"
    if(filteredData.length>0){
        checkValue = "checked"
    }
    let html = 
      `<div class="all_agentdetails">
      <div class="agent_list_img">
        <div class="agent_bordr">
          <i class="fa fa-user-secret" aria-hidden="true"></i>
        </div>
      </div>
      <div class="other_agent_desc">
          <div class="agent_desc0">
            <h3 class="agent_name">${iterator.name.toUpperCase()}</h3>
          </div>`
          
            if(filteredData.length>0){
              html += `
              <div class="switch_toggle_btn">
                  <label class="switch">
                    <input type="checkbox" 
                          id=${iterator._id}
                          agentidforchecked=${iterator._id}
                          agentName = ${ iterator.name }
                          onclick="handleClick(this,id)"
                          checked = "checked"
                          checkValue =${checkValue}>
                          <span class="slider round  round_button" title="Transfer Control"></span>
                  </label>
              </div>`
            }else{
              html +=`
              <div class="switch_toggle_btn">
                  <label class="switch">
                    <input type="checkbox" 
                        id=${iterator._id}
                        agentidforchecked=${iterator._id}
                        agentName = ${ iterator.name }
                        onclick="handleClick(this,id)"
                        checkValue =${checkValue}>
                        <span class="slider round  round_button" title="Transfer Control"></span>
                  </label>
              </div>`
            }
          html +=`<div class="agent_status" data-label="Avalibility">
            <i class="fa fa-circle text-danger mr-2 checkAvailable" availibilityId = ${iterator._id} title="Online"></i>
          </div>
          <button type="button" class="contact_sec gotNotificationClass_0 div-hide"  agentIdForNotification=${iterator._id} data-toggle="modal" onclick="openModal(this)">
            <img class="notify_img agentNotification" agentIdForNotification=${iterator._id} src = "/dist/img/purple_notification.png" style="height:30px; width:30px; border-radius: initial; margin: auto; width: 25px; height: 25px;">
          </button>
      </div>
    </div>`;
    $('.agent_list').append(html);
  }

  
   /************************************
     on select a user show bell icon if agent has notification from other agent
   ************************************/
    agentNotificationList = getAgentNotificationFromSession();
    if(agentNotificationList == null || agentNotificationList == 'undefined'|| agentNotificationList == '' ){
      let newArray = [];
      localStorage.setItem('NotifiedUserIds',JSON.stringify(newArray));
    }else{
      if(agentNotificationList.length>0){
        agentNotificationList.forEach(element => {
           let tempData = "[agentIdForNotification="+"'"+element.RequestFromAgentId+"']"
           let elementData = document.querySelectorAll(tempData);
          //  console.log('elementData 679999',elementData)
           if(element.requestToAgentId == getAgentIdFromSession()){
             elementData[0].style.display='block'
           }
        })
}
    }
   
  socket.emit("agent_online", {
    agentId: getAgentIdFromSession(),
    clientId: getClientFromSession(),
  });
  
}
/**********************************************
  on accept take control button emit an event to the other agent who requested and remove agent data 
  from notification array and set the toggle off
***********************************************/
socket.on("onAcceptAgentControlRequest", (data) => {
  // console.log('hiiiiii',data)
  let requestsToAgentsArray =getMyRequestsToAgentFromSession();
  if (data.clientId == getClientFromSession() && data.requestFromAgentId == getAgentIdFromSession()) {
  //  console.log('step 1')
    if(requestsToAgentsArray == null || requestsToAgentsArray == 'undefined'|| requestsToAgentsArray == '' ){
      localStorage.setItem('myRequestsToAgents',JSON.stringify([]));
    }else{
  //  console.log('step 2')
      let tempArray = [];
          requestsToAgentsArray.forEach((ele)=>{
            if(ele.requestFromAgentId == data.requestFromAgentId && 
              ele.userId == data.userId && 
              ele.requestToAgentId == data.requestToAgentId){
              }else{
                tempArray.push(ele);
              }
          })
          // console.log('tempArray 99999',tempArray);
          localStorage.setItem('myRequestsToAgents',JSON.stringify(tempArray));

        /* ==========================================
            if agent reject the request change the radio buton to off 
          ==========================================*/
          if(data.userId == localStorage.getItem('selected_user_id')){
            let myRequestsToAgents =localStorage.getItem('myRequestsToAgents');
            let getMyRequestsToAgentFromSession = JSON.parse(myRequestsToAgents);
            let agentdata = humanAgentList ;
            agentdata = agentdata.filter(ele=>ele._id != getAgentIdFromSession());
            agentListExceptMe = agentdata;
            totalAgent = agentdata.length;
            agentdata.forEach(el=>{
              let lastname = el.name.split(' ')
              if(lastname[1])
                  el.lastname = lastname[1];
            })
            $('.agent_list').html('')
            let selected_user_id =  data.userId;
            for (const iterator of agentdata) {
              let filteredData =[];
              if(getMyRequestsToAgentFromSession){
                filteredData = getMyRequestsToAgentFromSession.filter((ele)=>{
                });
                filteredData = getMyRequestsToAgentFromSession.filter(ele=>ele.userId == selected_user_id && ele.requestToAgentId ==iterator._id );
              }
              checkValue = "unchecked"
              if(filteredData.length>0){
                  checkValue = "checked"
              }
              let html = 
                `<div class="all_agentdetails">
                <div class="agent_list_img">
                  <div class="agent_bordr">
                    <i class="fa fa-user-secret" aria-hidden="true"></i>
                  </div>
                </div>
                <div class="other_agent_desc">
                    <div class="agent_desc0">
                      <h3 class="agent_name">${iterator.name.toUpperCase()}</h3>
                    </div>`
                    
                      if(filteredData.length>0){
                        html += `
                        <div class="switch_toggle_btn">
                            <label class="switch">
                              <input type="checkbox" 
                                    id=${iterator._id}
                                    agentidforchecked=${iterator._id}
                                    agentName = ${ iterator.name }
                                    onclick="handleClick(this,id)"
                                    checked = "checked"
                                    checkValue =${checkValue}>
                                    <span class="slider round  round_button" title="Transfer Control"></span>
                            </label>
                        </div>`
                      }else{
                        html +=`
                        <div class="switch_toggle_btn">
                            <label class="switch">
                              <input type="checkbox" 
                                  id=${iterator._id}
                                  agentidforchecked=${iterator._id}
                                  agentName = ${ iterator.name }
                                  onclick="handleClick(this,id)"
                                  checkValue =${checkValue}>
                                  <span class="slider round  round_button" title="Transfer Control"></span>
                            </label>
                        </div>`
                      }
                    html +=`<div class="agent_status" data-label="Avalibility">
                      <i class="fa fa-circle text-danger mr-2 checkAvailable" availibilityId = ${iterator._id} title="Online"></i>
                    </div>
                    <button type="button" class="contact_sec gotNotificationClass_0 div-hide"  agentIdForNotification=${iterator._id} data-toggle="modal" onclick="openModal(this)">
                      <img class="notify_img agentNotification" agentIdForNotification=${iterator._id} src = "/dist/img/purple_notification.png" style="height:30px; width:30px; border-radius: initial; margin: auto; width: 25px; height: 25px;">
                    </button>
                </div>
              </div>`;
              $('.agent_list').append(html);
            }
            // get online agent and display
            socket.emit("agent_online", {
              agentId: getAgentIdFromSession(),
              clientId: getClientFromSession(),
            });
          }



    }
  }else{
    // console.log("false 5555");
  }
});
socket.on("onAgentBusy", (data) => {
  // console.log('data',data)
  let requestsToAgentsArray =getMyRequestsToAgentFromSession();
  if (data.clientId == getClientFromSession() && data.requestFromAgentId == getAgentIdFromSession()) {
    if(requestsToAgentsArray == null || requestsToAgentsArray == 'undefined'|| requestsToAgentsArray == '' ){
      localStorage.setItem('myRequestsToAgents',JSON.stringify([]));
    }else{
      let tempArray = [];
      // let filterData = requestsToAgentsArray.filter(ele=> ele.requestFromAgentId != data.requestFromAgentId  || ele.userId != data.userId || ele.requestToAgentId != data.requestToAgentId);
      requestsToAgentsArray.forEach((ele)=>{
        if(ele.requestFromAgentId == data.requestFromAgentId && 
           ele.userId == data.userId && 
           ele.requestToAgentId == data.requestToAgentId){
           }else{
             tempArray.push(ele);
           }
      })
      localStorage.setItem('myRequestsToAgents',JSON.stringify(tempArray));
      console.log('data username',data.userName)
      $("#agent_name").html(data.agentName);
      $("#user_name").html(data.userName);
      $('#agentisbusymodal').modal('show');

/* ==========================================
    if agent reject the request change the radio buton to off 
   ==========================================*/
   if(data.userId == localStorage.getItem('selected_user_id')){
        let myRequestsToAgents =localStorage.getItem('myRequestsToAgents');
        let getMyRequestsToAgentFromSession = JSON.parse(myRequestsToAgents);
        let agentdata = humanAgentList ;
        agentdata = agentdata.filter(ele=>ele._id != getAgentIdFromSession());
        agentListExceptMe = agentdata;
        totalAgent = agentdata.length;
        agentdata.forEach(el=>{
          let lastname = el.name.split(' ')
          if(lastname[1])
              el.lastname = lastname[1];
        })
        $('.agent_list').html('')
        let selected_user_id =  data.userId;
        for (const iterator of agentdata) {
          let filteredData =[];
          if(getMyRequestsToAgentFromSession){
            filteredData = getMyRequestsToAgentFromSession.filter((ele)=>{
            });
            filteredData = getMyRequestsToAgentFromSession.filter(ele=>ele.userId == selected_user_id && ele.requestToAgentId ==iterator._id );
          }
          checkValue = "unchecked"
          if(filteredData.length>0){
              checkValue = "checked"
          }
          let html = 
            `<div class="all_agentdetails">
            <div class="agent_list_img">
              <div class="agent_bordr">
                <i class="fa fa-user-secret" aria-hidden="true"></i>
              </div>
            </div>
            <div class="other_agent_desc">
                <div class="agent_desc0">
                  <h3 class="agent_name">${iterator.name.toUpperCase()}</h3>
                </div>`
                
                  if(filteredData.length>0){
                    html += `
                    <div class="switch_toggle_btn">
                        <label class="switch">
                          <input type="checkbox" 
                                id=${iterator._id}
                                agentidforchecked=${iterator._id}
                                agentName = ${ iterator.name }
                                onclick="handleClick(this,id)"
                                checked = "checked"
                                checkValue =${checkValue}>
                                <span class="slider round  round_button" title="Transfer Control"></span>
                        </label>
                    </div>`
                  }else{
                    html +=`
                    <div class="switch_toggle_btn">
                        <label class="switch">
                          <input type="checkbox" 
                              id=${iterator._id}
                              agentidforchecked=${iterator._id}
                              agentName = ${ iterator.name }
                              onclick="handleClick(this,id)"
                              checkValue =${checkValue}>
                              <span class="slider round  round_button" title="Transfer Control"></span>
                        </label>
                    </div>`
                  }
                html +=`<div class="agent_status" data-label="Avalibility">
                  <i class="fa fa-circle text-danger mr-2 checkAvailable" availibilityId = ${iterator._id} title="Online"></i>
                </div>
                <button type="button" class="contact_sec gotNotificationClass_0 div-hide"  agentIdForNotification=${iterator._id} data-toggle="modal" onclick="openModal(this)">
                  <img class="notify_img agentNotification" agentIdForNotification=${iterator._id} src = "/dist/img/purple_notification.png" style="height:30px; width:30px; border-radius: initial; margin: auto; width: 25px; height: 25px;">
                </button>
            </div>
          </div>`;
          $('.agent_list').append(html);
        }
        // get online agent and display
        socket.emit("agent_online", {
          agentId: getAgentIdFromSession(),
          clientId: getClientFromSession(),
        });
      }
    }
  }
});
function agentisbusy(_this){
  // console.log('__this',_this)
    requestfromagentid = $(_this).attr("requestfromagentid");
    clientid = $(_this).attr("clientid");
    requesttoagentid = $(_this).attr("requesttoagentid");
    userid = $(_this).attr("userid");
    userName = $(_this).attr("username");

    // console.log('requestfromagentid',requestfromagentid);
    // console.log('clientid',clientid);
    // console.log('requesttoagentid',requesttoagentid);
    // console.log('userid',userid);
    // console.log('userName 222',userName);

    /*****************************************************
     on take control remove bell icon from agent list if notification array is empty
    ******************************************************/
     let data4 = "[agentIdForNotification="+"'"+requestfromagentid+"']"
     let elementData = document.querySelectorAll(data4);
    //  console.log('elementData 9999',elementData);
     let agentNotificationData =getAgentNotificationFromSession();
     if(agentNotificationData == null || agentNotificationData == 'undefined'|| agentNotificationData == '' ){
       let newArray = [];
       newArray.push(data);
       localStorage.setItem('agentNotificationData',JSON.stringify(newArray));
     }else{
       let filterData = agentNotificationData.filter(ele=> ele.requesttoagentid != requesttoagentid && ele.userId != userid )
       localStorage.setItem('agentNotificationData',JSON.stringify(filterData));
     }
     agentNotificationData =getAgentNotificationFromSession();
     if(agentNotificationData.length == 0){
         elementData[0].style.display='none'
     }

    /*****************************************************
     Emit data on agent busy button press
    ******************************************************/
     socket.emit("onAgentBusy", {
      requestFromAgentId: requestfromagentid,
      requestToAgentId: requesttoagentid,
      clientId: getClientFromSession(),
      userId: userid,
      userName: userName,
      agentName :  getAgentName()
    });
}
function closeModal(){
  $('#agentisbusymodal').modal('hide');
}