// To show all time zones in dropdown
const selectorOptions = moment.tz.names()
    .reduce((memo, tz) => {
        memo.push({
            name: tz,
            offset: moment.tz(tz).utcOffset()
        });

        return memo;
    }, [])
    .sort((a, b) => {
        return a.offset - b.offset
    })
    .reduce((memo, tz) => {
        const timezone = tz.offset ? moment.tz(tz.name).format('Z') : '';
        return memo.concat(`<option value="${tz.name}">(GMT${timezone}) ${tz.name}</option>`);
    }, "");

document.querySelector(".js-Selector").innerHTML = selectorOptions;

// To arrange in alphabetically order
var options = $('select.js-Selector option');
var arr = options.map(function (_, o) {
    return {
        t: $(o).text(),
        v: o.value
    };
}).get();
arr.sort(function (o1, o2) {
    return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
});
options.each(function (i, o) {
    o.value = arr[i].v;
    $(o).text(arr[i].t);
});

document.querySelector(".js-Selector").value = "Asia/Kolkata";



