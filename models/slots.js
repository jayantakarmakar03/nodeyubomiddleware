var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
  mongoose.connect(require('./connection-string'));
}


//Guest model schema.
var newSchema = new Schema({
  'clientId': { type: String },
  'slotName': { type: String },  //slot tag name
  'slotLabel': { type: String },
  'active_status': { type: String, default: "on" },
  'regex': { type: Boolean, default: false },
  'slotsParam': { type: Array },  //slot tag values
  'createdAt': { type: Date, default: Date.now },
  'updatedAt': { type: Date, default: Date.now },
  'ask': { type: String },
  'match': { type: String },
  'mismatch': { type: String }
});

newSchema.pre('save', function (next) {
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function () {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function () {
  this.update({}, { $set: { updatedAt: Date.now() } });
});


module.exports = mongoose.model('Slots', newSchema);
