const db = require("../models/all-models"),
    sgMail = require("@sendgrid/mail"),
    s3_config = require("../config/config.json").s3Linode,
    fs = require("fs"),
    AWS = require("aws-sdk"),
    sendMail = require("../middleware/campaignmail");
module.exports = {
    getuserName,
    creategroup,
    deletegroup,
    editgroup,
    updategroup,
    usergroupdata,
    getgroupdata,
    addmoremember,
    campaigntempdata,
    emailconfig,
    schedulemail,
    sendtestmail,
    mailconfigdata,
    saveDataOnS3,
};

async function getuserName(req, res) {
    let userData = await db.Userchat.find({ clientId: req.user._id }, { userId: 1, email: 1, "session.name": 1, "session.email": 1 });
    let clientId = await db.Client.findOne({_id: req.user._id },{_id:1,clientId :1});
    let userModelUserList = await db.Users.find({
        $or:[{clientId: clientId.clientId },{clientId: req.user._id }],
        email : { $ne:null }
    }, {  email: 1, phone: 1, name: 1 , "source" :"users" ,_id:1}).sort({createdAt : -1});

    let finaldata = [];
    let finaldataOfUsersTable = [];

    grouparray = [];
    //for campaign
    if (userData.length > 0) {
        userData.forEach((element) => {
          let data;
          if (element.session && element.session.email) {
              data = {
                  userId: element.userId,
                  name: element.session.name ? element.session.name : "unknown user",
                  mail: element.email != null ? element.email : element.session.email,
                  group: "",
              };
              finaldata.push(data);
          } else if (element.email != null && element.email != "") {
              data = {
                  userId: element.userId,
                  name: "unknown user",
                  mail: element.email,
                  group: "",
              };
              finaldata.push(data);
          }
      });
    }
    if (userModelUserList.length > 0) {
      userModelUserList.forEach((element) => {
        let data;
        if (element.email) {
            data = {
                userId: element._id,
                name: element.name ? element.name : "unknown user",
                mail: element.email ? element.email : '',
                group: "",
            };
            finaldataOfUsersTable.push(data);
        } 
    });
  }
        // console.log('finaldata===',finaldata);
        let result = [...finaldataOfUsersTable,...finaldata] ;

        let fetchgroup = await db.Group.find({ clientId: req.user._id }, { createdgroup: 1 });
        return res.status(200).json({
            status: "success",
            message: "User name and email found successfully.",
            data: result,
            groupcreated: fetchgroup,
        });
    
    // else
    //  {
    //     return res.status(200).json({
    //         status: "error",
    //         message: "User Data not found.",
    //     });
    // }
}

async function creategroup(req, res) {
  if(req.body.group_id){
    /******* to add more user in the existing group *******/
    let newGroupData = Object.values(req.body.createdgroup)[0];
    if(newGroupData.length == 0){
      return res.json({ message: "Please select user to add in the existing group", status: false ,statusCode:201});
    }

    let groupData = await db.Group.findOne( {_id : req.body.group_id },{createdgroup:1, groupName: 1});
    let existingGrpData = Object.values(groupData.createdgroup)[0];
    let userIdsInExistingGroup_array =await existingGrpData.map(item=>item.userId);
    await newGroupData.filter(ele=>{
      if(userIdsInExistingGroup_array.includes(ele.userId) == false){
        existingGrpData.push(ele);
      }
    })
    let finalData = {
         [req.body.group_name] : existingGrpData
    };
    let upateddata = await db.Group.updateOne({ _id: req.body.group_id }, {$set : {createdgroup: finalData } });
    return res.json({ message: "User added successfully in existing group", status: "Succeesss" ,statusCode:200});

  }else{
    /******* to create a group and add users in that group *******/
    let temp = `createdgroup.`+ Object.keys(req.body)[0];
    let userdata = await db.Group.findOne( {[temp] : { $exists : true },clientId: req.user._id});
    let groupArray =  Object.values(req.body)[0];
    if(groupArray == undefined || groupArray == "" || groupArray.length == 0){
        return res.json({
          statusCode: 201, 
          status: "false", 
          message: "Please select one or more user to create group." });
    }
    if(userdata){ 
       return res.json({statusCode: 201, status: "false", message: "Group name already exists" });
    }
    let data = new db.Group({
       clientId: req.user._id,
       createdgroup: req.body,
       groupName: temp.replace("createdgroup.", ""),

   });
   await data.save();
   return res.json({statusCode: 200, status: "success", message: "Group created successfully" });
  }
}

async function deletegroup(req, res) {
    let deletegroup = await db.Group.deleteOne({ _id: req.body.groupId });
    return res.json({ status: "success", msg: "Group Deleted successfully" });
}
async function editgroup(req, res) {
    let editgroup = await db.Group.findOne({ _id: req.body.groupId }, { createdgroup: 1, groupName:1 });
    let userData = await db.Userchat.find({ clientId: req.user._id }, { userId: 1, email: 1, "session.name": 1, "session.email": 1 });

    if (userData.length > 0) {
        var finaldata = [];
        userData.forEach((element) => {
            let data;

            if (element.session && element.session.email) {
                data = {
                    userId: element.userId,
                    name: element.session.name ? element.session.name : "unknown user",
                    mail: element.email != null ? element.email : element.session.email,
                    group: "",
                };
                finaldata.push(data);
            } else if (element.email != null && element.email != "") {
                data = {
                    userId: element.userId,
                    name: "unknown user",
                    mail: element.email,
                    group: "",
                };
                finaldata.push(data);
            }
        });
    }

    let data = JSON.parse(JSON.stringify(editgroup.createdgroup));
    let abc = Object.keys(data);

    // for (i = 0; i < data[abc[0]].length; i++) {
    //     userId = data[abc[0]][i].userId;
    //     index = finaldata.findIndex((x) => x.userId === userId);
    //     finaldata.splice(index, 1);
    // }

    return res.json({ status: "success", data: editgroup, userdata: finaldata });
}

async function updategroup(req, res) {
    let groupdata = await db.Group.find({ _id: req.body.groupId });
    if (!groupdata) {
        return res.json({ msg: "Group Not Found", status: "false", statusCode: 201 });
    }
    let upateddata = await db.Group.updateOne({ _id: req.body.groupId }, {$set : {createdgroup: req.body.updateddata, groupName: req.body.groupName  } });
    return res.json({ msg: "Data Updated Successfully", status: "Succeesss" });
}

async function usergroupdata(req, res) {
    let groupdata = await db.Group.find({ clientId: req.user._id });
    let grouparray = [];
    for (const iterator of groupdata) {
        for (key in iterator.createdgroup) {
            let groupobj = {
                groupId: iterator._id,
                groupName: key,
            };
            grouparray.push(groupobj);
        }
    }
    return res.json({ data: grouparray });
}

async function getgroupdata(req, res) {
    let skip = Number(req.body.skip) || 0;
    let limit = Number(req.body.limit) || 30;
    let search = req.body.searchItem;
    let docCount=0
    let fetchgroup=[]
    if(search!=""){
        docCount = await db.Group.count({ clientId: req.user._id ,$or: [
            {
              groupName: { $regex: search, $options: "i" },
            },
          ],
        });

        fetchgroup = await db.Group.find({ clientId: req.user._id , $or: [
          {
            groupName: { $regex: search, $options: "i" },
          },
        ]}, { createdgroup: 1, groupName: 1 }).skip(skip).limit(limit).sort({createdAt: -1});
    } else {
        docCount = await db.Group.count({ clientId: req.user._id});
        fetchgroup = await db.Group.find({ clientId: req.user._id }, { createdgroup: 1, groupName: 1 }).skip(skip).limit(limit).sort({createdAt: -1}); 
    }

    if (!fetchgroup || fetchgroup.length==0) {
        return res.json({ msg: "No group Data", status: "false" });
    } else {
        return res.json({
            msg: "Group data found succesfully",
            skip:skip,
            limit: limit,
            docCount: docCount,
            groupcreated: fetchgroup,
        });
    }
}

async function addmoremember(req, res) {
    let finalobj = {},
        temparry = [];
    let groupdata = await db.Group.find({ _id: req.body.groupId }, { createdgroup: 1 });
    if (!groupdata) {
        return res.json({ msg: "User Not Found", status: "fail" });
    }
    let data = Object.keys(groupdata[0].createdgroup);
    let existingdata = groupdata[0].createdgroup[data[0]];

    for (let j = 0; j < existingdata.length; j++) {
        for (let i = 0; i < req.body.data.length; i++) {
            if (req.body.data[i].userId == existingdata[j].userId) {
                req.body.data.splice(i, 1);
            }
        }
    }
    var finalarray = existingdata.concat(req.body.data);
    finalobj[data[0]] = finalarray;

    let upateddata = await db.Group.updateOne({ _id: req.body.groupId }, { createdgroup: finalobj });
    return res.json({ msg: "Data Updated Successfully", status: "Succeesss" });
}

async function campaigntempdata(req, res) {
    let chkclient = await db.maildraft.findOne({ clientId: req.user.clientId }, { maildraft: 1 });
    if (!chkclient) {
        return res.json({ msg: "Data not founded" });
    } else {
        return res.json({ msg: "Data founded successfully", data: chkclient });
    }
}

async function emailconfig(req, res) {
try{
    req.body.obj.accounttype      = escapeHtml(req.body.obj.accounttype);
    req.body.obj.verfiedUser      = escapeHtml(req.body.obj.verfiedUser);
    req.body.obj.accessKeyId      = escapeHtml(req.body.obj.accessKeyId);
    req.body.obj.apiVersion       = escapeHtml(req.body.obj.apiVersion);
    req.body.obj.region           = escapeHtml(req.body.obj.region);
    req.body.obj.verifiedEmailAws = escapeHtml(req.body.obj.verifiedEmailAws);
    let crenditialdata = await db.Client.updateOne({ clientId: req.user.clientId }, { $set: { campaignconfig: req.body.obj } });
    if (!crenditialdata) {
        return res.json({ msg: "error in data update" });
    } else {
        return res.json({
            msg: "Record Updated Successfully",
            status: "Succeesss",
        });
    }
  }catch(error){
    console.log('Error occured in catch block:', error)
  }
}
async function schedulemail(req, res) {
    //   try {
    //       let mailconfig = await db.Client.findOne({ clientId: req.user.clientId }, { campaignconfig: 1 })
    //       if (!mailconfig) {
    //           return res.json({ status: 'error', msg: 'Please update your confinguration details in mail settings' });
    //       }
    //       let imgobj = {},
    //           imgdata = [];
    //       for (i = 0; i < req.files.length; i++) {
    //           imgobj[req.files[i].fieldname] = req.files[i].location;
    //           imgobj['key'] = req.files[i].key;
    //           imgdata.push(imgobj)
    //       }
    //       let obj = {
    //           'bodydata': req.body,
    //           'bannerdata': imgdata
    //       }
    //       let data1 = new db.scheduledmail({
    //           clientId: req.user.clientId,
    //           maildraft: obj,
    //           deliverdate: req.body.scheduledate,
    //           recipients: req.body.mailrecipent,
    //           emailtitle: req.body.emailtitle,
    //           campaignconfig: mailconfig
    //       })
    //       await data1.save();
    //       return res.json({ status: 'success', msg: 'Mail Schedule Successfully' });
    // } catch(err){
    //   // for(i=0;i<req.files.length;i++){
    //   // }
    //     throw err;
    // }
}

function emailerdemo1(templatedata) {
    let demo1 = `<html><head><META http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body>
  <div>
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="margin:30px auto;width:700px;border:1px solid #ddd;border-radius:5px">
      <tbody>
        <tr style="margin:0;padding:0">
          <td align="center" valign="top" style="margin:0" width="700px; ">
 
            <table border="0" cellpadding="0" cellspacing="0" style="width:700px">
              <tbody>
              ${
                templatedata.demo1_headericon
                  ? `<tr style="width:700px;margin:0;padding:0">

                  <td align="left" valign="top" width="700px;" style="margin:0;padding:0px;position:relative;">

                  <div  style="background-color:${templatedata.demo1headercolor}">

                       <img style="width:60px;text-align:left;left:35px;top:30px; padding:7px 0px 0px 8px;" class="letsfind" src="${templatedata.demo1_headericon}" alt="">

                  </div>
                          
                  </td>

                </tr>`
                  : ""
              }
                ${
                  templatedata.demo1banner1
                    ? ` <tr style="width:700px;padding:0;margin:0">
                <td align="center" valign="top" width="700px;" style="margin:0;padding:0px">
                  <img style="height:auto;width:100%" src="${templatedata.demo1banner1}" alt="">
                </td>
              </tr>`
                    : ""
                }
              <tr style="width:700px;padding:0;margin:0">
                  <td align="center" valign="top" width="700px;" style="margin:0;padding:10px 15px 0">
                    <h4 style="font-family:&#39;Source Sans Pro&#39;,sans-serif;font-size:20px;color:#333;margin:0;text-align:left;text-transform:uppercase;color: #2b91c3 ! important;">${
                      templatedata.demo1_bodytext
                    }</h4>
                  </td>
                </tr>
                <tr style="width:700px;padding:0;margin:0">
                  <td align="center" valign="top" width="700px;" style="margin:0;padding:10px 15px 15px 15px;">
                    <p style="font-family:&#39;Source Sans Pro&#39;,sans-serif;font-size:16px;color:#333;margin:0;text-align:left;
                    line-height: 1.4">${
                      templatedata.demo1_maintext
                    }</p>
                  </td>
                </tr>
                ${
                  templatedata.demo1rightimage && templatedata.demo1leftimage
                    ? `<tr style="width:700px;padding:0;margin:0">
                  <td align="center" valign="top" width="326" style="width:340px;margin:0;padding:0px 13px 0 15px;float:left;">
                    <img style="height:auto;width:100%" src="${
                      templatedata.demo1leftimage
                        ? templatedata.demo1leftimage
                        : ""
                    }" alt="">
                   
                    <p style="font-family:&#39;Source Sans Pro&#39;,sans-serif;font-size:14px;color:#333;margin:0;text-align:left;
                    padding: 10px 0px 10px 0px; line-height: 1.4;">${
                      templatedata.demo1_imagesubtitleTitle1
                    }</p>                
                  </td>
                  <td align="center" valign="top" width="327" style="width:340px;margin:0;padding:0px 15px 0 15px;float:left; ">
                    <img style="height:auto;width:100%" src="${
                      templatedata.demo1rightimage
                        ? templatedata.demo1rightimage
                        : ""
                    }" alt="">
                   
                    <p style="font-family:&#39;Source Sans Pro&#39;,sans-serif;font-size:14px;color:#333;margin:0;text-align:left;
                      padding: 10px 0px 10px 0px; line-height: 1.4">${
                      templatedata.demo1_emailsubtitle2
                    }</p>                
                  </td>
                </tr>`
                    : ""
                }
                <tr style="width:700px;padding:0;margin:0">

                  <td align="center" valign="top" width="275" style="width:690px;margin:0;padding:25px;float:left;cursor: pointer; ">
                  <a href="${
                    templatedata.demo2btn1url1
                  }" style="text-decoration: none;min-width:120px;padding:10px;border:1px solid transparent;outline:none;color:#fff;border-radius:5px; background-color:${
    templatedata.demo2_button1color
  };margin:30px 0">${templatedata.demo2_buttontext1}</a>
                  </td>

                </tr>
               
              <tr style="width:700px;padding:0;margin:0">
                  <td align="center" valign="top" width="326" style="width:327px;margin:0;padding:0;float:left;margin-top:10px;">

                  ${ templatedata.emailTemp1 && templatedata.mobileTemp1  ? 
                     `<b style="margin-right: 15%;">${templatedata.emailTemp1} | ${templatedata.mobileTemp1} </b>`: 
                    `<b style="margin-right: 15%;">${templatedata.emailTemp1} ${templatedata.mobileTemp1} </b>`}

                  </td>

                  <td align="right" valign="top" width="327" style="width:328px;margin:0;padding: 0px 15px 0 46px;float:left;">  
                  
                  <a href="${templatedata.facebookLinkTemp1}"  title="Facebook"> <img src="https://yugasabot.ap-south-1.linodeobjects.com/campaigning/social_images/FacebookIcon"  title="Fackebook" style="cursor:pointer;"/> </a>
                  <a href="${templatedata.instaLinkTemp1}"  title="Instagram"> <img src="https://yugasabot.ap-south-1.linodeobjects.com/campaigning/social_images/InstaIcon"  title="Instagram" style="cursor:pointer;" /> </a>
                  <a href="${templatedata.linkedinLinkTemp1}"  title="linkedin"> <img src="https://yugasabot.ap-south-1.linodeobjects.com/campaigning/social_images/LinkedInIcon" title="LinkedIn" style="cursor:pointer;" /> </a>
                       
                  </td>
              </tr>  

              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
    <div style="display:none;white-space:nowrap;font:15px courier;color:#ffffff">
    </div>
  </div>
  </body></html>`;
  return demo1;
}

function emailerdemo2(templatedata) {
  let demo2 = `<html><head><META http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body>
<div>
<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin:30px auto;width:600px;border:1px solid #ddd;border-radius:5px">
<tbody>
<tr style="margin:0;padding:0">
<td align="center" valign="top" style="margin:0" width="600px; ">

<table border="0" cellpadding="0" cellspacing="0" style="width:600px">
<tbody>

<tr style="width:600px;padding:0;margin:0">
<td align="left" valign="top" width="600px;" style="margin:0;padding:0px;position:relative; ">

 <div style="background-color:${templatedata.demo2headercolor}">
   <img style="width:60px;text-align:left;left:35px;top:30px; padding:7px 0px 0px 8px;" src="${
     templatedata.demo2_headicon ? templatedata.demo2_headicon : ""
   }" alt="">
   </div>
 </td>
  
</tr>


                ${
                  templatedata.demo2bannerimage1
                    ? `<tr style="width:600px;padding:0;margin:0">
<td align="center" valign="top" width="600px;" style="margin:0;padding:0px;">
<img style="height:auto;width:100%" src="${
                        templatedata.demo2bannerimage1
                          ? templatedata.demo2bannerimage1
                          : ""
                      }" alt="">
</td>
</tr>`
                    : ""
                }  
<tr style="width:600px;padding:0;margin:0">
<td align="center" valign="top" width="600px;" style="margin:0;padding:0 15px 0">
<h4 style="font-family:&#39;Source Sans Pro&#39;,sans-serif;font-size:20px;color:#333;margin:10px 0 0;text-align:left;font-weight:600;text-transform:uppercase;color: #2b91c3 ! important;">${
    templatedata.demo2bannertext
  }</h4>
</td>
</tr>
<tr style="width:600px;padding:0;margin:0">
<td align="center" valign="top" width="600px;" style="margin:0;padding:10px 15px 0px 15px;">
<p style="font-family:&#39;Source Sans Pro&#39;,sans-serif;font-size:16px;color:#333;margin:0px;text-align:left; line-height: 1.4;">${
    templatedata.demo2bannersubtitle
  }.</p>
</td>
</tr>
               
<tr style="width:600px;padding:0;margin:0">
                ${
                  templatedata.demo2bannerimage2
                    ? `<td align="center" valign="top" width="600" style="width:600px;margin:0;padding:15px 0px 15px 0px;">
              <img style="height:auto;width:100%" src="${
                        templatedata.demo2bannerimage2
                          ? templatedata.demo2bannerimage2
                          : ""
                      }" alt="">`
                    : ""
              }
              
</td>
</tr>


<tr style="width:600px;padding:0;margin:0">


   <td align="center" valign="top" width="400px;" style="margin:0;padding:15px; cursor:pointer">
                    <a href="${
                      templatedata.demo1btn1url
                    }" style="text-decoration: none;padding:10px;border-radius:5px;color:transparent;border:1px solid transparent;outline:none;color:#fff;background-color:${
    templatedata.demo1buttoncolor
  }">${templatedata.demo1_button1text}</a>
                  </td>


</tr>

 <tr style="width:700px;padding:0;margin:0">
      <td align="center" valign="top" width="326" style="width:327px;margin:0;padding:0;float:left;margin-top:10px;">


       ${ templatedata.emailTemp2 && templatedata.mobileTemp2  ? 
            `<b style="margin-right:30%;">${templatedata.emailTemp2} | ${templatedata.mobileTemp2}</b>`: 
            `<b style="margin-right:30%;">${templatedata.emailTemp2}   ${templatedata.mobileTemp2}</b>`} 
                   
      </td>

      <td align="right" valign="top" width="250" style="width:250px;margin:0;padding:0px 15px 0 8px;float:left">  
      
      <a href="${templatedata.facebookLinkTemp2}"  title="Facebook"> <img src="https://yugasabot.ap-south-1.linodeobjects.com/campaigning/social_images/FacebookIcon"  title="Fackebook" /> </a>
      <a href="${templatedata.instaLinkTemp2}"  title="Instagram"> <img src="https://yugasabot.ap-south-1.linodeobjects.com/campaigning/social_images/InstaIcon"  title="Instagram" /> </a>
      <a href="${templatedata.linkedinLinkTemp2}"  title="linkedin"> <img src="https://yugasabot.ap-south-1.linodeobjects.com/campaigning/social_images/LinkedInIcon" title="LinkedIn" /> </a>
          
      </td>
  </tr>  


 
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<div style="display:none;white-space:nowrap;font:15px courier;color:#ffffff">
</div>
</div>
</body></html>`;

  return demo2;
}

async function mailconfigdata(req, res) {
  let mailconfig = await db.Client.findOne(
    { clientId: req.user.clientId },
    { campaignconfig: 1 }
  );
  return res.json({ status: "success", data: mailconfig });
}
async function sendtestmail(req, res) {
  try {
    let clientId = req.user.clientId;
    let sendmaildata = await db.Client.findOne(
      { clientId: clientId },
      { campaignconfig: 1 }
    );
    if (!sendmaildata) {
      return res.json({
        status: false,
        statusCode: 201,
        message: "Client id not found ",
      });
    }
    if (
     // !sendmaildata.campaignconfig || !sendmaildata.campaignconfig.sendgridkey
     !sendmaildata.campaignconfig
    ) {
      return res.json({
        status: false,
        statusCode: 202,
        message: "Please set your email credentials under mail settings.",
      });
    }
    let data = req.body;
    let folderExists = await checkdirexist(clientId);
    if (folderExists === false) await createdirWritefile(clientId);

    let obj = {};
    let count = 0;
    for (var key in data) {
      if (data[key].includes("base64")) {
        let time = new Date().getTime();
        let imageName = key+time ;
        uploadS3data(data[key], key, clientId, imageName);
        obj[key] = `${s3_config.url}/${clientId}/Media/Admin_files/Email_campaign/${imageName}`;
      } else {
        obj[key] = data[key];
      }

      ++count;
      if (count === Object.keys(data).length) {
        let html;
        if (obj.templateid == "#demo1") {
          html = emailerdemo1(obj);
        } else {
          html = emailerdemo2(obj);
        }



        if(sendmaildata.campaignconfig.accounttype==='Sendgrid') {

          await sgMail.setApiKey(sendmaildata.campaignconfig.sendgridkey);
           const msg = {
             to: obj.reciptormail,
             from: sendmaildata.campaignconfig.verfiedUser, // Use the email address or domain you verified above
             subject: obj.Emailtitle,
             text: "Demo text",
             html: html,
           };
           await sgMail
             .sendMultiple(msg)
             .then(() => {
               return res.json({
                 status: true,
                 statusCode: 200,
                 message: "Mail sent successfully",
               });
             })
             .catch((error) => {
               return res.json({
                 status: false,
                 statusCode: error.code,
                 message: error.message,
                 error: error,
               });
             }); 
   
         } else if(sendmaildata.campaignconfig.accounttype==='aws'){
   
               var ses = new AWS.SES({
               accessKeyId: sendmaildata.campaignconfig.accessKeyId,
               secretAccessKey: sendmaildata.campaignconfig.secretAccessKey,
               apiVersion: sendmaildata.campaignconfig.apiVersion,
               region: sendmaildata.campaignconfig.region
               });
   
               var params = {
                Destination: {
                   BccAddresses: ((typeof obj.reciptormail)==='string') ? [] : obj.reciptormail,
                   // CcAddresses: [
                   //   'STRING_VALUE',
                   //   /* more items */
                   // ],
                  // ToAddresses: ((typeof obj.reciptormail)==='string') ? [obj.reciptormail] : obj.reciptormail[0],
                   ToAddresses: ((typeof obj.reciptormail)==='string') ? [obj.reciptormail] : [],
                },
               Message: {
                   Body: {
                   Html: {
                       Data: html
                   },
                   Text: {
                       Data: 'demo test',
                       Charset: 'utf-8'
                   }
                   },
                   Subject: {
                   Data: obj.Emailtitle,
                   Charset: 'utf-8'
                   }
               },
               Source: sendmaildata.campaignconfig.verifiedEmailAws,
               };
   
              const sendEmailData = ses.sendEmail(params).promise()
               sendEmailData.then(()=> {
                 return res.json({
                   status: true,
                   statusCode: 200,
                   message: "Mail sent successfully",
                 });
               }).catch((error)=>{
                    return res.json({
                   status: false,
                   statusCode: error.code,
                   message: error.message,
                   error: error,
               });
             })
   
         }

      }
    }
  } catch (err) {
    console.log("error occured", err);
  }
}
// **********save multiple data on s3 server using promise.all  ***********************//
async function saveDataOnS3(req, res) {
  try {
    let clientId = req.user.clientId;
    let data = req.body;
    let draftData = await db.maildraft.aggregate([
      { $match: { clientId: clientId } },
    ]);

    let promiseArray = [];
    for (var key in data) {
      let time = new Date().getTime();
      let imageName = key+time ;
      promiseArray.push(uploadS3data(data[key], key, clientId ,imageName));
    }
    let dataObj = {};
    Promise.all(promiseArray)
      .then((val) => {
        for (let key of val) {
          dataObj[Object.keys(key)[0]] = key[Object.keys(key)[0]];
        }
        if (draftData.length > 0) {
          let updatedData = { ...draftData[0].maildraft, ...dataObj };
          db.maildraft.update(
            { clientId: clientId },
            { $set: { maildraft: updatedData } },
            { multi: true },
            function (err, result) {
              if (result) {
                res.json({
                  status: true,
                  message: "Draft saved successfully successfully",
                  statusCode: 200,
                });
              } else {
                res.json({
                  status: false,
                  message: "INTERNAL DB ERROR ",
                  statusCode: 201,
                });
              }
            }
          );
        } else {
          let saveData = new db.maildraft({
            clientId: clientId,
            maildraft: dataObj,
          });
          saveData.save(function (err, result) {
            if (err) {
              res.json({
                statusCode: 201,
                status: false,
                message: "INTERNAL DB ERROR",
                res: err,
              });
            } else {
              res.json({
                statusCode: 200,
                status: true,
                message: "Draft created  successfully",
              });
            }
          });
        }
      })
      .catch((error) => {
        console.log("error occured in promise all", error);
      });
  } catch (err) {
    console.log("error occured", err);
  }
}
// **********check directory exist or not ***********************//
function checkdirexist(clientId) {
  let dir = "./public/dist/img/campaigning/" + clientId;
  return fs.existsSync(dir);
}
// **********create directory with client name ***********************//
function createdirWritefile(clientId) {
  let dir = "./public/dist/img/campaigning/" + clientId;
  fs.mkdirSync(dir, { recursive: true });
}
function getFileType(base64) {
  let mimeType = base64.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
  let extension = mimeType.split("/");
  console.log(extension[1]);
  return extension[1];
}
// **********save data on s3 server this is common function used in promise.all ***********************//
function uploadS3data(datakey, key, clientId,timeStampImgName) {
  let obj = {};
  return new Promise((resolve, reject) => {
    let strings = datakey.split(",");
    if (strings[1]) {
      AWS.config.update({
        accessKeyId: s3_config.accessKeyId,
        secretAccessKey: s3_config.secretAccessKey,
        region: s3_config.region,
        s3BucketEndpoint: s3_config.s3BucketEndpoint,
      });
      var s3 = new AWS.S3({ endpoint: s3_config.url });
      buf = Buffer.from(
        datakey.replace(/^data:image\/\w+;base64,/, ""),
        "base64"
      );
      let fileType = getFileType(datakey);
      var params = {
        Bucket: `yugasabot/${clientId}/Media/Admin_files/Email_campaign`,
        Key: `${timeStampImgName}`,
        ACL: "public-read",
        ContentEncoding: "base64",
        ContentType: `image/${fileType}`,
        Body: buf,
        // cache-control:"max-age=0"
      };
      s3.putObject(params, function (err, res) {
        if (err) {
          console.log("Error uploading data: ", err);
          reject(err);
        } else {
          path = `${s3_config.url}/${clientId}/Media/Admin_files/Email_campaign/${timeStampImgName}`;
          obj[key] = path;
          resolve(obj);
          console.log("Successfully uploaded data to myBucket/myKey", res);
        }
      });
    } else {
      obj[key] = datakey;
      resolve(obj);
    }
  });
}
