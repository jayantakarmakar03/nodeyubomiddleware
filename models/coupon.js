const mongoose = require("mongoose");

const couponSchema = new mongoose.Schema({
    code : {
        type : String ,
        require: true,
        unique : true
    },
    duration: {
        type: Number,
        required: true  
    },
    durationType: {
        type: String,
        enum : ['day','month','year'],
        default:'day'
    },
    validFrom: {
        type: Date,
        required: true
    },
    validUntil: {
        type: Date,
        required: true
    },
    isActive: {
        type: Boolean,
        default: true
    },
    isReusable :{
        type: Boolean,
        default: false
    },
    visibility:{
        type: Boolean,
        default: true
    }

});
const couponModel = mongoose.model('Coupon',couponSchema);
module.exports = couponModel ;








