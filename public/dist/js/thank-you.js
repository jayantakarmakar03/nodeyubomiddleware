$(".need-assistance").on("click", async function(e){
    let paramVal = await getParamsFromUrl(window.location.href, "valid")
    let wrapper = document.createElement('div');
        wrapper.id = "assistance-email";
        wrapper.class = "assistance-email";
        wrapper.innerHTML = '<img src="dist/img/assist-email.png"><h4>Your request has been submitted. Our experts will get back to you soon.</h4>';

    $.ajax({
        type: "POST",
        url: "/need-assistance",
        data: {token: paramVal},
        success: async function(resp){
            swal({
                content: wrapper,
                className: "thankyou_page__alrt"
            })
        },
        error: async function(jqXHR, textStatus, err) {
            alert('Something went wrong, please refresh the page')
        }
    })
})

function getParamsFromUrl(url, srchng_param){
  let newUrl = new URL(url),
  paramVal = newUrl.searchParams.get(srchng_param)
  return paramVal;
}