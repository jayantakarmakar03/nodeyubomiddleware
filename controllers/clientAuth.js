const db = require("../models/all-models");

module.exports = {
  clientQuery: clientQuery,
  checkUserType: checkUserType,
};

async function clientQuery(req, res) {
  let clientId = req.user.clientId,
    client = await db.Client.findOne({ clientId: clientId });
  return client ? client : false;
}

async function checkUserType(req, res) {
  let client = await clientQuery(req, res);
  return client ? res.send({ type: client.type }) : false;
}
