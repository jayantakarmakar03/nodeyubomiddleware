const db = require("../models/all-models"),
  clientcontroller = require("../controllers/clientAuth.js"),
  axios = require("axios"),
  fs = require("fs"),
  escapeHtml = require('escape-html'),
  async = require("async"),
  actionController = require("../controllers/action.js");

// let trackerObj = {};

module.exports = {
  loadslot: loadslot,
  getActnReturnValues: getActnReturnValues,
  webhookActionsList: webhookActionsList,
  deleteActionById: deleteActionById,
  uploadSlotJson: uploadSlotJson,
  updateAction: updateAction,
  getActnsList: getActnsList,
  getSlotsList: getSlotsList,
  selectedData: selectedData,
  actionById: actionById,
  addAction: addAction,
  slotview: slotview,
  deleteSlot: deleteSlot,
  updateSlot: updateSlot,
  slotbyId: slotbyId,
  trainslot: trainslot,
  setstatusactive: setstatusactive,
  downloadjsonfile: downloadjsonfile,
  botrequest: botrequest,
  slotadd: slotadd
}


//bot request response gateway :-
async function botrequest(reqObj) {
  try {
    let findSlotApiResp, pythonApiResp, slotObj = {
      client_id: reqObj.data.client_id,
      query: reqObj.data.text
    };
    let client_id = reqObj.data.clientId, userId = reqObj.data.userId, context = reqObj.data.context;
    let inContext = context ? ("inContext" in context ? ((context.inContext == true) ? true : false) : false) : false;
    if (inContext) {
      findSlotApiResp = await callToPython("POST", process.env.findSlotsPythonUrl, slotObj);
    } else {
      findSlotApiResp = await callToPython("POST", process.env.findSlotsPythonUrl, slotObj);
      pythonApiResp = await callToPython("POST", process.env.pythonUrl, reqObj);
    }
    console.log("incone", inContext, findSlotApiResp.data)
    if (findSlotApiResp.data.status == "failed" && inContext) {
      if (context['attempts'] < 2) {
        let slotLabel = context['current_slot']['slotLabel'];
        context['attempts']++;
        let contextupdate = await db.Userchat.updateOne({ clientId: client_id, userId: userId },
          { context: context });
        return {
          status: "failed",
          message: "This input seems to be invalid, please help us with the correct " + slotLabel + "."
        }
      } else {
        let contextupdate = await db.Userchat.updateOne({ clientId: client_id, userId: userId },
          { context: { params: {}, inContext: false, action: "", node_id: false } });
        return {
          status: "failed",
          message: "Sorry, you have reached maximum limit of retries. Please type your query again to start over."
        }
      }
    } else if (findSlotApiResp.data.status == "success" || (findSlotApiResp.data.status == "failed" && !inContext)) {
      await contextobj(reqObj.data, findSlotApiResp.data) //setting the context object
      pythonApiResp = pythonApiResp ? pythonApiResp : reqObj;
      let actionName = reqObj.data.context ? reqObj.data.context.action : "";
      //console.log("actionaname1", actionName)
      var property = pythonApiResp ? (pythonApiResp["data"] ? (pythonApiResp["data"]["tree"] ? (pythonApiResp["data"]["tree"]["action"] ? (pythonApiResp["data"]["tree"]["action"]["property"]) : actionName) : actionName) : (actionName)) : actionName;
      // console.log("propertyname", property)
      if (!property || property == 'Select Action to Associate') {
        return {
          status: "success",
          data: pythonApiResp.data
        }
      }
      var response = await slotmatch(reqObj.data, property, pythonApiResp);
      //console.log('shiam', response)
      return response;
    } else {
        //  console.log('shiam')
      return {
        status: "success",
        data: pythonApiResp.data
      }
    }
  } catch (error) {
    //console.log('errror', error);
    throw error
  }
}


//***********************  To read, extract and deleting the uploaded slots json  **************************//
async function loadslot(req, res) {
  try {
    const data = await db.Slots.find({ clientId: req.user.clientId });
    return res.json({ status: 'success', data: data, msg: 'data fetched successfully' })
  } catch (err) {
    throw err
  }
}

async function slotbyId(req, res) {
  try {
    const data = await db.Slots.findOne({ _id: req.body.id });
    return res.json({ status: 'success', data: data, msg: 'data returned successfully' })
  } catch (err) {
  }
}
/*********************for uploading slot.json file**************/
async function uploadSlotJson(req, res) {
  try {
    let slotNameArray = [];
    await fs.readFile(
      "./public/dist/img/" + req.file.filename,
      async function read(err, data) {

        if (err) {
          res.render("slot.hbs");
        } else {
          var slots = JSON.parse(data);

          if (slots) {
            await db.Slots.deleteMany({ clientId: req.user.clientId });
            await saveSlotDataInDb(req, slots);

            //find query
            fs.unlink(
              req.file.path,
              async function (err, resp) {
                if (err) {
                  return res.send(err)
                }
                else {
                  return res.json({ status: 'success', msg: 'Json uploaded successfully' })
                }
              }
            );
          } else {
            return res.send("The given file format is not valid");
          }
        }

      }
    );
  } catch (err) {
    throw err
  }


}

//***********************  For saving slots uploaded data in db  **************************//
async function saveSlotDataInDb(req, data) {
  try {
    let dataArray = [];
    for (const iterator of data) {
      let dataObj = {
        clientId: req.user.clientId,
        slotName: iterator.key,
        slotLabel: iterator.name,
        regex: iterator.regex,
        slotsParam: iterator.values,
        createdAt: Date.now(),
        updatedAt: Date.now()
      }
      dataArray.push(dataObj);
    }
    await db.Slots.insertMany(dataArray);
  } catch (err) {
    throw err;
  }

}

//***********************  To create new action  **************************//
async function addAction(req, res) {
try{
  let client = await clientcontroller.clientQuery(req, res);
  if (!client) {
    return res.json({ message: "Session expire! Please login again" });
  }
  const actionName_exist = await db.Actions.find({
    clientId: client.clientId,
    actionName: { $regex: req.body.actionName, $options: "$i" },
  });

  if (actionName_exist.length > 0) {
    return res.send("exists");
  } else {
    req.body["clientId"] = client.clientId;
    let data = new db.Actions(req.body);
    await data.save();
  }

  const query = await db.Actions.find({ clientId: req.user.clientId }).sort(
    "-createdAt"
  );
  return res.send(query);
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}
async function getSlotsList(req, res) {
  try {
    const query = await db.Slots.find({ clientId: req.user.clientId }, { _id: 0, createdAt: 0, updatedAt: 0, __v: 0, clientId: 0 });
    let data = slotobj(query)
    return res.json({ status: 'success', data: data })
  } catch (error) {
    throw error;
  }
}
// ***************to get selected data , we need action and slots  by using clientId ********************
async function selectedData(req, res) {

  try {
    let clientAction = await db.Actions.findOne({ clientId: req.body.clientId });

    if (clientAction === null) {
      return res.json({ status: 'Failed', message: `Action not found with this client id : ${req.body.clientId} ` })
    }

    let actionSlotFields = clientAction.actionSlotFields;

    if (actionSlotFields.length === 0) {
      return res.json({ status: 'Failed', message: `Action array is empty of this client : ${req.body.clientId} ` })
    }
    let clientSlot = await db.Slots.find({ clientId: req.body.clientId });

    let slotLabels = clientSlot.map(ele => ele.slotLabel);

    let resultArray = [];
    for (let i = 0; i < slotLabels.length; i++) {

      let obj = {};
      let inc = actionSlotFields.includes(slotLabels[i]);

      if (inc === true) {
        obj.value = slotLabels[i];
        obj.selected = true;
        resultArray.push(obj);
      } else {
        obj.value = slotLabels[i];
        obj.selected = false;
        resultArray.push(obj);
      }
    }
    return res.json({ status: 'success', data: resultArray });

  } catch (err) {
    console.log('error occured \n', err)
    throw error;
  }
}


//********* To list out webhook and actions from db ***********//

async function webhookActionsList(req, res) {
try{
  //Webhook listing
  let webhookList = await db.Webhook.find({ clientId: req.user.clientId }).sort(
    "-createdAt"
  );

  //Action listing
  let actionList = await db.Actions.find({ clientId: req.user.clientId }).sort(
    "-createdAt"
  );

  return res.send({ webhookList: webhookList, actionList: actionList });
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

//********* To delete action by id from db ***********//
async function deleteActionById(req, res) {
  await db.Actions.deleteOne({ _id: req.body.actionId });
  return res.send();
}

//********* To get action by id from db ***********//
async function actionById(req, res) {
  const query = await db.Actions.find({ _id: req.body.actionId }, { _id: 0, createdAt: 0, updatedAt: 0, __v: 0, clientId: 0 });
  return res.send(query);
}

//********* To update action by id from db ***********//
async function updateAction(req, res) {
  try{
  let clientId = req.user.clientId,
    actionId = req.body.actionId;
  req.body["clientId"] = clientId;
  delete req.body["webhookId"];

  let client = await db.Client.findOne({ clientId: clientId });
  if (!client) {
    return res.json({ message: "Session expire! Please login again" });
  }
  const action_exist = await db.Actions.find({ _id: actionId });

  if (action_exist.length > 0) {
    await db.Actions.updateOne(
      { _id: actionId },
      {
        $set: req.body,
      }
    );
    const query = await db.Actions.find({ clientId: clientId }).sort(
      "-createdAt"
    );
    return res.send(query);
    // }
  } else {
    return res.send("Something went wrong");
  }
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

//********* To get actions list from db ***********//
async function getActnsList(req, res) {
  const query = await db.Actions.find({ clientId: req.user.clientId }, { _id: 0, createdAt: 0, updatedAt: 0, __v: 0, clientId: 0 });
  return res.send(query);
}
async function slotview(req, res) {
  res.render('slotview.hbs');
}

async function deleteSlot(req, res) {
  try {
    const query = await db.Slots.deleteOne({ _id: req.body.slotid })
    return res.json({ status: 'success', msg: 'Slot deleted successfully' })
  } catch (err) {
    throw err
  }
}

async function updateSlot(req, res) {
    try {
        req.body.slotName  = escapeHtml(req.body.slotName);
        req.body.slotLabel = escapeHtml(req.body.slotLabel);
        req.body.match     = escapeHtml(req.body.match);
        req.body.ask       = escapeHtml(req.body.ask);
        req.body.mismatch  = escapeHtml(req.body.mismatch);
        let clientId = req.user.clientId;
        let client = await db.Slots.findOne({ clientId: clientId });
        if (!client) {
            return res.json({ message: "Session expire! Please login again" });
        }
        const slot_exist = await db.Slots.find({ _id: req.body.id });
        if (slot_exist) {
            const slotName = await db.Slots.find({
                clientId: req.user.clientId,
                _id: {$nin : req.body.id },
                slotName: { $regex: req.body.slotName, $options: "$i" },
            });
            if(slotName.length>0){
              return res.json({statusCode : 201 , status:false, status: 'exists', msg: 'Slot already exist' });
            }
            await db.Slots.updateOne({ _id: req.body.id }, {
                $set: {
                    slotName: req.body.slotName,
                    slotLabel: req.body.slotLabel,
                    regex: req.body.regex,
                    slotsParam: req.body.slotsParam,
                    ask: req.body.ask,
                    match: req.body.match,
                    mismatch: req.body.mismatch,
                },
            });
            return res.json({statusCode : 200 , status: 'success', msg: 'Slot updated successfully' });
        }
    } catch (err) {
        throw err;
    }
  }
  
/******************************* for training slot **********************/
async function trainslot(req, res) {
  try {
    let clientId = req.user.clientId;
    const data = await db.Slots.find({ clientId: req.user.clientId, active_status: { $ne: "off" } });
    let slotdata = slotobj(data);
    await createslotdir(clientId, slotdata);
    let requestData2 = {
      "client_id": clientId,
      "query": "I am looking for red shoes"
    };
    fData = await axios({
      method: "post",
      url: process.env.updateSlotsPythonUrl,
      data: requestData2,
    });
    return res.json({ status: 'success', msg: 'Training done successfuly' })
  } catch (err) {
    throw err;
  }
}


function slotobj(data) {
try{
  let slotarray = [];
  data.forEach(element => {
    let slotobj = {
      key: element.slotName,
      name: element.slotLabel,
      values: element.slotsParam,
      regex: element.regex
    };
    slotarray.push(slotobj);
  });
  // let slotdata = JSON.parse(JSON.stringify(Object.assign({}, ...slotarray)));
  //console.log('vvvvvvvvv', slotarray)

  return slotarray;
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function createslotdir(clientId, data) {
try{
  let dir = process.env.pythonDir + clientId;
  data = JSON.stringify(data);
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
    writefile(clientId, data);
  } else {
    writefile(clientId, data);
    // fs.unlink(
    //   process.env.pythonDir + clientId + "/slot.json",
    //   async function (err, resp) {
    //     if (err) {
    //       //console.log('shivaii',err)
    //       return err
    //     }
    //     else {
    //       //console.log('fsafasfd')
    //       writefile(clientId, data);
    //     }
    //   }
    // );

  }
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}

}
/*************for writing slot.json******************/
async function writefile(clientId, data) {
try{
  fs.writeFileSync(
    process.env.pythonDir + clientId + "/prod_n_prop.json",
    data,
    { encoding: "utf8", flag: "w" },
    (err) => {
      if (err) {
        throw err
      }
    }
  );
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}
/** for creating context obj in userchat schema {code is untested will need some modifications whiile makeing it dynamic} */
// async function slotcontext(req, res) {
//   let requestData2 = {
//     "client_id": "test_staging",
//     "query": "I am looking for green shoes"
//   };
//     let fData = await axios({
//       method: "post",
//       url: "http://192.46.209.18:5000/findslots",
//       data: requestData2,
//     });
//     let data = await db.Userchat.find({userId:req.body.userId},{$context:1})
//     let finalobj = {...data.context['params'], ...fData.data.slots}
//     let obj = {};
//     obj['params']=finalobj;
//     await db.Userchat.updateOne({userId:req.body.userId},{$set:{$context:obj}})


// }


/************* for status upate active or inactive ******************/
async function setstatusactive(req, res) {
  try {
    await db.Slots.updateOne(
      { _id: req.body.id, clientId: req.user.clientId },
      {
        $set: {
          active_status: req.body.status,
        },
      }
    );
    res.send({ message: "success" });
  } catch (error) {
    throw err
  }

};
/***********************for downloading slot.json***************/
async function downloadjsonfile(req, res) {
  try {
    const data = await db.Slots.find({ clientId: req.user.clientId });
    const filedata = slotobj(data);
    return res.json({ status: 'success', data: filedata, msg: 'File downloaded successfully' })
  } catch (error) {
    //console.log('err', error)
    throw error;

  };
}

async function getActnReturnValues(req, res) {
try{
  if (!req.user.clientId) {
    return res.redirect("/login");
  } else {
    if (req.body.actionName) {
      let actionData = await db.Actions.find({
        clientId: req.user.clientId,
        actionName: { $regex: req.body.actionName, $options: "$i" },
      }, { _id: 0, createdAt: 0, updatedAt: 0, __v: 0, clientId: 0 });

      if (actionData.length > 0) {
        return res.json({
          statusCode: 200,
          status: "success",
          data: actionData[0],
          message: "Data found successfully"
        })
      } else {
        return res.json({
          statusCode: 400,
          status: "failed",
          message: "No data found"
        })
      }
    } else {
      return res.json({
        statusCode: 400,
        status: "failed",
        message: "Action name is required to get the list."
      })
    }
  }
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function contextobj(reqObj, contextdata) {
  let clientId = reqObj.clientId, userId = reqObj.userId;
  try {
    let data = await db.Userchat.findOne({ clientId: clientId, userId: userId }, { context: 1 }),
      obj = {};
    //console.log("contextobj11", clientId, userId)
    //console.log("contextobj1111", contextdata, data)
    if (data && data.context) {
      let finalobj = { ...data.context['params'], ...contextdata.slots }
      obj['params'] = finalobj;
    } else {
      obj['params'] = contextdata.slots;
    }
    //console.log("contextobj", obj)
    await db.Userchat.updateOne({ clientId: clientId, userId: userId }, { $set: { context: obj } });
    let data1 = await db.Userchat.findOne({ clientId: clientId, userId: userId }, { context: 1 });
    //console.log("contextobj2", data1.context)
    return true
  } catch (error) {
    //console.log('err', error)
  }

}

async function slotmatch(reqObj, actionName, botresponse) {
  let clientId = reqObj.client_id, client_id = reqObj.clientId, userId = reqObj.userId;
  try {
    //console.log("inside slotmatch", clientId, actionName, userId)
    const actionData = await db.Actions.findOne({ clientId: clientId, actionName: actionName }, { _id: 0 });
    const usercontextdata = await db.Userchat.findOne({ clientId: client_id, userId: userId }, { context: 1, _id: 0 });
    //console.log("actionData", usercontextdata.context)
    let contextobj = usercontextdata ? (usercontextdata.context ? usercontextdata.context.params : {}) : {};
    let arrayItem = (actionData && actionData.actionSlotFields) ? actionData.actionSlotFields : [];
    if (arrayItem.length > 0) {
      //console.log("hkjhfh", arrayItem.length, arrayItem)
      for (let i = 0; i < arrayItem.length; i++) {
        let contxtObj = botresponse["data"]["context"];
        botresponse["data"]["context"] = {
          params: contextobj,
          inContext: true,
          action: actionName,
          node_id: (contxtObj && contxtObj["node_id"]) ? contxtObj["node_id"] : botresponse.data.tree.node_id
        }
        if (arrayItem[i] in contextobj) {
          if (i == (arrayItem.length - 1)) {
            reqObj.node = botresponse.data.context.node_id;
            let pythonApiResp = await callToPython("POST", process.env.pythonUrl, { data: reqObj });
            //console.log("slotmatcher", pythonApiResp)
            let resp = await actionController.clientAPI({ ...pythonApiResp["data"], contextObj: usercontextdata.context }, actionData)
            botresponse["data"]["context"] = {
              params: contextobj,
              inContext: false,
              action: "",
              node_id: false
            }
            let contextupdate = await db.Userchat.updateOne({ clientId: client_id, userId: userId },
              { context: botresponse["data"]["context"] });
            return { data: resp }
          }
        } else if (arrayItem[i].includes("session.")) {
          botresponse["data"]["context"]["params"][arrayItem[i]] = botresponse.data.session[arrayItem[i].substring(arrayItem[i].indexOf('.')+1)];
          const renameProp = ({context, ...object}) => ({contextObj: botresponse["data"]["context"], ...object}) //Renaming context to contextObj
          // actionData.actionData = JSON.stringify(botresponse.data.session[arrayItem[i].substring(arrayItem[i].indexOf('.')+1)]);
          let resp = await actionController.clientAPI(renameProp(botresponse.data), actionData);
          return { data: resp }
        } else {
          //console.log('else507', clientId, arrayItem[i])
          let slotdata = await db.Slots.findOne({ $and: [{ clientId: clientId }, { $or: [{ slotName: arrayItem[i] }, { slotName: arrayItem[i] + "_regex" }] }] }, { _id: 0, __v: 0 });
          //console.log("slotdata", slotdata)
          // let contxtObj = botresponse["data"]["context"];
          // botresponse["data"]["context"] = {
          //   params: contextobj,
          //   inContext: true,
          //   action: actionName,
          //   node_id: (contxtObj && contxtObj["node_id"]) ? contxtObj["node_id"] : botresponse.data.tree.node_id
          // }

          // if(trackerObj[slotdata.slotLabel] > 2){
          //   botresponse.data["text"] = `We couldn't get the correct data thrice so we're resetting the context, please let me know how can I help you?`;
          //   botresponse.data['replies'] = [];
          //   botresponse.data["type_option"] = true;
          //   botresponse.data.context = {...botresponse.data.context, inContext: false, action: ""};
          //   //console.log("botresposss", botresponse.data.context)
          //   trackerObj = {};
          //   await db.Userchat.updateOne({ clientId: client_id, userId: userId },
          //     { context: botresponse["data"]["context"]});
          //   return { data: botresponse.data }
          // }

          botresponse["data"]["context"]["current_slot"] = slotdata;
          botresponse["data"]["context"]["attempts"] = 0;

          let contextupdate = await db.Userchat.updateOne({ clientId: client_id, userId: userId },
            { context: botresponse["data"]["context"] });
          //console.log('ssffsf', slotdata)
          //console.log('botresponse', botresponse)
          return await followupresponse(slotdata, botresponse);
        }
      }
    }
  } catch (error) {
    //console.log('shivaii', error)
    throw error
  }
}

async function followupresponse(resdata, botresponse) {
try{
  //console.log('shivam!!!', resdata.regex)
  if (resdata.regex) {
    // if((resdata.slotLabel) in trackerObj){
    //   trackerObj[resdata.slotLabel] += 1;
    // }else{
    //   trackerObj[resdata.slotLabel] = 1;
    // }

    //console.log("trackerobj", trackerObj)
    botresponse.data["text"] = `Please help us with your ${resdata.slotLabel}`;
    botresponse.data['replies'] = [];
    botresponse.data["type_option"] = true;
    return { data: botresponse.data }
  } else {
    try {
      let replies = [];
      for (i = 0; i < resdata.slotsParam.length; i++) {
        let obj = {
          link: `p${i + 1}`,
          option: resdata.slotsParam[i]
        };
        replies.push(obj)
      }
      botresponse.data['replies'] = replies;
      botresponse.data["type_option"] = false;
      botresponse.data.tree ? (botresponse.data.tree.products = []) : "";
      botresponse.data["text"] = `Would you like to choose any specific ${resdata.slotLabel}`
      return {
        statusCode: 200,
        status: "success",
        message: "Data found",
        data: botresponse.data
      }
    } catch (error) {
      return {
        statusCode: 201,
        status: "failed",
        message: "Data not found",
      }
    }
  }
}catch(error){
  console.log('An error occurs in the catch block:-', error)
}
}

async function callToPython(method, url, data) {
  //console.log("calltopython", data)
  try {
    data = await axios({
      method: method,
      url: url,
      data: data,
    });
    return {
      statusCode: 200,
      status: "success",
      message: "Data found",
      data: data.data
    };
  } catch (err) {
    return {
      statusCode: 201,
      status: "failed",
      message: "Data not found",
      data: err
    }
  }
}

//function for manually slot add 
async function slotadd(req, res) {
  try {
      req.body.slotName  = escapeHtml(req.body.slotName);
      req.body.slotLabel = escapeHtml(req.body.slotLabel);
      req.body.match     = escapeHtml(req.body.match);
      req.body.ask       = escapeHtml(req.body.ask);
      req.body.mismatch  = escapeHtml(req.body.mismatch);
      const slotName = await db.Slots.findOne({
          clientId: req.user.clientId,
          slotName: { $regex: req.body.slotName, $options: "$i" },
      });
      if (slotName) {
        return res.json({ status: 'exists', msg: 'Slotname  already exist' });
      } else {
          // let params = [{ "test param1": ["test", "params", "1"] }, { "test param2": ["test", "params", "2"] }]
          let paramsnew;
          if(req.body.regex == 'true'){
            paramsnew = req.body.addexp;
          }else{
            paramsnew = req.body.slotsParam;
          }
          let dataObj = {
                  clientId: req.user.clientId,
                  slotName: req.body.slotName,
                  slotLabel: req.body.slotLabel,
                  regex: req.body.regex,
                  slotsParam: paramsnew,
                  createdAt: Date.now(),
                  updatedAt: Date.now(),
                  match: req.body.match,
                  ask: req.body.ask,
                  mismatch: req.body.mismatch
              }
          await db.Slots.create(dataObj)
          return res.json({ status: 'success', msg: 'Slot created successfully' });
      }
  } catch (error) {
      //console.log('error', error);
      throw error;
  }
}
  
