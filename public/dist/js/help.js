//To submit help form
$("#help_form").submit(function (event) {
  let dataObj = {},
    dataArray = $(this).serializeArray();
  $.each(dataArray, function (i, fields) {
    dataObj[fields.name] = fields.value
  })

  $.ajax({
    type: "POST",
    url: "/helpMail",
    timeout: 10000,
    data: dataObj,
    success: async function (resp) {
      if(resp.msg == "empty"){
        alert("Both the fields are mandatory to fill.")
      }else{
        alert(resp.msg)
        location.reload()
      }
    },
    error: function (jqXHR, textStatus, err) {
      alert('Something went wrong, please refresh the page')
    }
  })
  event.preventDefault();
})

window.document.onkeydown = function(e) {
  if (!e) {
    e = event;
  }
  if (e.keyCode == 27) {
    lightbox_close();
  }
}

function lightbox_open(vid) {
  let helpVideo = document.getElementById(vid);
  // helpVideo.setAttribute('src','https://admin.helloyubo.com/videos/help/'+vid+'.webm');
  helpVideo.style.display = 'block';
  document.getElementById('boxclose').setAttribute('data',vid);
  window.scrollTo(0, 0);
  document.getElementById('light').style.display = 'block';
  document.getElementById('fade').style.display = 'block';
  //document.body.classList.add("lightbox-on");
  helpVideo.play();
}

function lightbox_close() {
  let vid=document.getElementById('boxclose').getAttribute('data');
  let helpVideo = document.getElementById(vid);
  helpVideo.style.display = 'none';
  document.getElementById('light').style.display = 'none';
  document.getElementById('fade').style.display = 'none';
  //document.body.classList.remove("lightbox-on");
  helpVideo.pause();
}