<?php
$json = file_get_contents('php://input');


$json=json_decode($json);

    $jsonData  = $json;
    $newJson   = [];
    $finalJson = array('DTree'=>array());
    $options   = [];
    $links     = [];
    $nodeType  = '';
    $i         = 0;

    
    /** Converting Decision Tree Default JSON formate Into Uygasa's JSON formate */

    foreach ($jsonData->operators as $key => $json) {

        $newJson['DTree'][$key]['node_id']        = $i == 0 ? 'root' : 'n'.($i+1);
        $newJson['DTree'][$key]['node_name']      = $i == 0 ? 'root' : $json->properties->title;
        $newJson['DTree'][$key]['text']           = $json->properties->question;
        $newJson['DTree'][$key]['type_of_node']   = '';

        foreach ($json->properties->outputs as $outputKey => $output) {
            $options[$outputKey]['option'] = $output->answer;
        }

        $newJson['DTree'][$key]['options']        = $options;
        $options  = [];

        $i++;
    }

    //** Adding Links Information in JSON  */
    foreach ($jsonData->links as $link) {
        $newJson['DTree'][$link->fromOperator]['options'][$link->fromConnector]['link'] = str_replace('operator', 'n', $link->toOperator);
    }

    /** Generating the final formate of JSON */
    $i = 0;
    foreach ($newJson['DTree'] as $operator) {

        $finalJson['DTree'][$i]['node_id']      = $operator['node_id'];
        $finalJson['DTree'][$i]['node_name']    = $operator['node_name'];
        $finalJson['DTree'][$i]['text']         = $operator['text'];
        $finalJson['DTree'][$i]['type_of_node'] = 'fall_through';
        $finalJson['DTree'][$i]['default']      = 'false';

        foreach($operator['options'] as $option) {
           $options[] = $option;
           $links[]   = key_exists('link', $option) ? $option['link'] : null;
        }

        $finalJson['DTree'][$i]['options']  = $options;

        if (count(array_unique($links)) != 1) {
            $finalJson['DTree'][$i]['type_of_node'] = 'multi_child';
        } else {
            $finalJson['DTree'][$i]['default']      = array_unique($links)[0] != null ? array_unique($links)[0] : 'false';
        }

        if ($links[0] == null) {
            $finalJson['DTree'][$i]['type_of_node'] = 'leaf_node';
            $finalJson['DTree'][$i]['options'] = [];
        }

        $links   = [];
        $options = [];
        $i++;
    }
    //print_r($finalJson['DTree']);die;
echo json_encode($finalJson);

die;