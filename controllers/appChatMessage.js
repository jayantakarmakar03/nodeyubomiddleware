const db=require('../models/all-models');
const jwt = require('jsonwebtoken');
var moment = require('moment');
const d = new Date();
global.now;
global.time;
global.onlineUsers =[];
const secretKey = process.env.secretKey;

    setInterval(()=>
    {
       now = moment(Date.now()).tz('Asia/Kolkata');
       time = now.hour() + ':' + now.minutes() ;
    }, 100);

module.exports = function(io) {
    const users = {};
    io.on('connection', function(socket) {
         //*********on agent accept control from other agent emit  this event */
         socket.on('onAgentBusy',(data)=>{
            global.io.emit('onAgentBusy',data)
        });
        //*********on agent accept control from other agent emit  this event */
            socket.on('onAcceptAgentControlRequest',(data)=>{
                global.io.emit('onAcceptAgentControlRequest',data)
            });
        //*********send request for control to other agent */
        socket.on('sendRequestControlToOtherAgent',(data)=>{
            global.io.emit('getRequestControlOfOtherAgent',data)
        });
        //*********send message to agent for user asking for agent to talk*/
        socket.on('UserNeedAgentMessageToAgent',(data)=>{
            global.io.emit('sendMessageToAgentPanel',{ botmessage : data.message , userId : data.userId , date : moment(new Date()).format("DD/MM/YYYY") ,time: time,clientId:data.clientId ,control : data.control })
        });
        //*********on agent take leave controrol emit the data in same panel */
        socket.on('sendAgentControlRequest',data=>{
            global.io.emit('sendAgentControlRequest',data);
        });
        //*********on agent take leave controrol emit the data in same panel */
        socket.on('emitSameAgentData',data=>{
            global.io.emit('getSameAgentData',data);
        });
         //*********on agent/user typing emit data  */
         socket.on('sendAttachmentToBot',data=>{
            socket.to(data.userId).emit('send_message',data);
        });
        //*********on agent/user typing emit data  */
        socket.on('agentTyping',data=>{
            socket.to(data.userId).emit('agentTyping',data);
        });

        //*********on agent/user typing emit data  */
        socket.on('typingStopped',data=>{
            socket.to(data.userId).emit('typingStopped',data);
        });
        
        socket.on('userTyping',data=>{
            // socket.to(data.userId).emit('userTyping',data);
            global.io.emit('userTyping',data);
        });
        // ********on take control of user emit data to other agent so other can not take control*****//
        socket.on('onAgentBlock',data=>{
            global.io.emit('onAgentBlock',data);
        })
        // ********on take control of user emit data to other agent so other can not take control*****//
        socket.on('emitDataOnTakeControl',data=>{
            global.io.emit('getDataOnTakeControl',data);
        });
        // ********on leave control of user emit data to other agent so other can take control*****//
        socket.on('emitDataOnLeaveControl',data=>{
            global.io.emit('getDataOnLeaveControl',data);
        });
        // ********emit socket on manage agent page to show online ofline users on page load *****//
        socket.on('getOnlineAgentsArrayList',data=>{
            global.io.emit('getOnlineAgentsArrayList',onlineUsers);
        });
        // ********push user in array  when they are online ***********//
        socket.on('agent_online', userData => {
            index = onlineUsers.findIndex((element,index)=>{
                if(element.agentId == userData.agentId)
                return true;
            });
            if(index === -1)
            {
                onlineUsers.push(userData);
            }
            global.io.emit('onlineAgentsList',onlineUsers);
        });
        // ********Runs when agent disconnects/close the browser

        socket.on('agentOffline', (userData) => {
            // remove the user from array when  they got offline /disconnected
            onlineUsers = onlineUsers.filter(element=>{
                return element.agentId != userData.agentId ;
            });
            global.io.emit('onlineAgentsList',onlineUsers);
        });

        socket.on('join',function(userId){
            socket.join(userId);
        });
        socket.on('sendMessageInRoom',(data)=>{
            socket.to(data.userId).emit('send_message',data);
            global.io.emit('sendMessageToAgentPanel',{botmessage :
data.message , userId : data.userId , date :  moment(new
Date()).format("DD/MM/YYYY") ,time: time,clientId:data.clientId ,control : data.control})
        });
        socket.on('leave',(userId)=>{
            socket.leave(userId);
        });


        global.io.emit("sendMessage","message from appchatmessage.js ");
        socket.on("sendMessage",function(msg){
        });

        socket.on('appMessageToUser', function(message) {
            global.io.emit('appChatMessage', {chatMessage:message});
        });
        socket.on('message', function(data){
            data.time =time;
            data.date =  moment(new Date()).format("DD/MM/YYYY") ;
            global.io.emit('message',data);
        });
        socket.on('botmessage',function(botmessage){
            botmessage.time =time;
            botmessage.date =  moment(new Date()).format("DD/MM/YYYY") ;
            global.io.emit('botmessage',botmessage);

        })
        socket.on('login', async function(data){
            // saving userId to array with socket ID
            users[socket.id] = data.userId;
            data.type=1;
            global.io.emit('online',data);
            let chatUpdate = await
db.Userchat.updateOne({userId:data.userId},{$set:{online:1}});
        });
        socket.on('disconnect', async function(){
            if(users[socket.id]){
                let chatUpdate = await
db.Userchat.updateOne({userId:users[socket.id]},{$set:{online:0,
node:"root"}});
                global.io.emit('online', {userId:users[socket.id], type:0});
                delete users[socket.id];
            }
        });
        socket.on('typing', function(data){
            global.io.emit('typing',data);
        });
        socket.on('display', function(data){
            global.io.emit('display',data);
        });
        socket.on('userOnline', function(data){
            global.io.emit('userOnline',data);
        });
        socket.on('userOffline', function(data){
            global.io.emit('userOffline',data);
        });
    });
};
