var colorSet = new am4core.ColorSet();

locations.forEach((item) => {
	item.color = colorSet.next(), 
	item.latitude = parseFloat(item.latitude),
	item.longitude = parseFloat(item.longitude)
});

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create map instance
var chart = am4core.create("location_chart", am4maps.MapChart);

// Set map definition
chart.geodata = am4geodata_worldIndiaLow;

// Set projection
chart.projection = new am4maps.projections.Miller();

// Create map polygon series
var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

// Exclude Antartica
polygonSeries.exclude = ["AQ"];

// Make map load polygon (like country names) data from GeoJSON
polygonSeries.useGeodata = true;
polygonSeries.nonScalingStroke = true;
polygonSeries.strokeWidth = 0.5;
polygonSeries.calculateVisualCenter = true;

polygonSeries.events.on("validated", function(){
  imageSeries.invalidate();
})

// Configure series
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.tooltipText = "{name}";
polygonTemplate.polygon.fillOpacity = 0.6;


// Create hover state and set alternative fill color
var hs = polygonTemplate.states.create("hover");
hs.properties.fill = chart.colors.getIndex(0);

// Add image series
var imageSeries = chart.series.push(new am4maps.MapImageSeries());
imageSeries.mapImages.template.propertyFields.longitude = "longitude";
imageSeries.mapImages.template.propertyFields.latitude = "latitude";
imageSeries.mapImages.template.tooltipText = "{name}: [bold]{value}[/]";


var circle = imageSeries.mapImages.template.createChild(am4core.Circle);
circle.fillOpacity = 0.7;
circle.propertyFields.fill = "color";
circle.nonScaling=true


var circle2 = imageSeries.mapImages.template.createChild(am4core.Circle);
circle2.propertyFields.fill = "color";
circle2.nonScaling=true


circle2.events.on("inited", function(event){
  animateBullet(event.target);
  if(locations.length<1){
    $('.contactimage').show();
    $('#location_chart').hide();
  } else {
    $('.contactimage').hide();
    $('#location_chart').show();
  }
})


// var circle = imageTemplate.createChild(am4core.Circle);
// circle.fillOpacity = 0.7;
// circle.propertyFields.fill = "color";
// circle.tooltipText = "{name}: [bold]{value}[/]";

imageSeries.heatRules.push({
  "target": circle,
  "property": "radius",
  "min": 5,
  "max": 10,
  "dataField": "value"
})

imageSeries.heatRules.push({
  "target": circle2,
  "property": "radius",
  "min": 15,
  "max": 20,
  "dataField": "value"
})


function animateBullet(circle) {
    var animation = circle.animate([{ property: "scale", from: 1, to: 5 }, { property: "opacity", from: 1, to: 0 }], 1000, am4core.ease.circleOut);
    animation.events.on("animationended", function(event){
      animateBullet(event.target.object);
    })
}
imageSeries.data = locations