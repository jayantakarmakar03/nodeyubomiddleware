var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
    mongoose.connect(require('./connection-string'));
}

//hument  agent model schema.
var newSchema = new Schema({
    clientId: {
        type: String,
        required: true
    },
    agentId: Schema.Types.ObjectId,
    userId: {
        type: String,
        required: true
    },
    totalSumOfMilliSec : {
        type : Number,
        default: 0    
    },
    count : {
        type : Number,
        default: 0    
    },
    responseTime: [{type : Number}]

},{ timestamps : true})



module.exports = mongoose.model('agentResponseTime', newSchema);
