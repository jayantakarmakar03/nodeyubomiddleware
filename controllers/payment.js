const db = require('../models/all-models');
const mongo = require('mongodb');
const axios = require('axios');
const moment = require('moment');
const ObjectID = mongo.ObjectID;
const escapeHtml = require('escape-html');
const emailverifycontroller = require("../controllers/emailverification.js");
const secretKey = process.env.secretKey;

module.exports={
    getClientSubscriptionDetails               :    getClientSubscriptionDetails,
}

async function getClientSubscriptionDetails(req,res) {
    try{
        let getClientSubscriptionData = await db.Client.findOne({ clientId: req.user.clientId },{subscription:1});
              return res.json({
                statusCode : 200,
                status     : 'Client subscription data',
                data       : getClientSubscriptionData,
               })

    }catch(err){
        console.log('error in catch block',err);
    }  
}

