let maxLength = $("#tempText").attr("maxlength");
let maxqr=false;
$("#tempText").after("<div class='char-count'><span id='remainingLengthTempId'>" 
                  + maxLength + "</span> / "+maxLength+" remaining</div>");
$("#tempText").bind("keyup change", function(){checkMaxLength(this.id,  maxLength); } )
$("#tempText").bind("blur", function(){
    let txt=$("#tempText").val();
    txt=autoCorrect(txt)[1]
    $("#tempText").val(txt);
    addVars("bodyvars",txt)
})

$("#headerdata").bind("blur", function(){
    let v=$("#headerdata").val();
    let ac=autoCorrectHeader(v);
    v=ac[1]
    $('#headerdata').val(v);
    addVars("headervars",v)
})

$('#tempName').bind("blur",function(){
    let v=$("#tempName").val();
    v=v.replace(new RegExp(" ", "g"),'_').toLowerCase();
    $("#tempName").val(v);
})

$('#ctalink').bind("blur", function(){
    if($('#ctatype').val()=='URL'){
        addUrlVar('ctalink')
    }
})
$('#ctalink2').bind("blur", function(){
    if($('#ctatype2').val()=='URL'){
        addUrlVar('ctalink2')
    }
})

function addUrlVar(id){
    let v=$("#"+id).val();
    let ac=autoCorrectHeader(v,true);
    v=ac[1]
    $('#'+id).val(v);
    addVars(id+"vars",v)
}

let optout=false;

function checkMaxLength(textareaID, maxLength){
    console.log("maxlength function",maxLength)
  
    currentLengthInTextarea = $("#"+textareaID).val().length;
    $(remainingLengthTempId).text(parseInt(maxLength) - parseInt(currentLengthInTextarea));
        
    if (currentLengthInTextarea > (maxLength)) { 
      
      // Trim the field current length over the maxlength.
      $("#"+textareaID).val($("#"+textareaID).val().slice(0, maxLength));
      $(remainingLengthTempId).text(0);
      
    }
}

function typeSelectionCloud(obj){
    if(obj.value=='TEXT'){
        $('.headerrow').show();
        $('.headermedia').html('');
    } else {
        $('.headerrow').hide();
        $('.headermedia').html('<input type="file" id="chooseSample" name="chooseSample" onchange="uploadWhatsappMedia(this)"><input type="text" id="headermedia" class="form-control" disabled  placeholder="No '+obj.value+' selected. Please click Browse button to upload."/>');
    }
}

function categorySelection(obj){
    console.log(obj.value)
    if(obj.value=="MARKETING"){
        $('.optoutbtn').show();
    } else {
        $('.optoutbtn').hide();
        $('.optoutcontainer').hide();
        $('#footerdata').prop('disabled',false);
        $('#footerdata').val('')
    }
}

function ctaTypeSelection(obj,i=''){
    console.log(obj.value,i)
    if(obj.value=='URL'){
        $('#ctalinklabel'+i).html("Website URL")
        $('#ctalink'+i).val('')
        $('#ctalink'+i+'vars').html('')
    } else {
        $('#ctalinklabel'+i).html("Phone (E.164 format)")
        $('#ctalink'+i).val('')
        $('#ctalink'+i+'vars').html('')
    }
}

function addOptOut() {
    optout=true;
    if($('#qrrow2:visible').length == 1 && $('#qrrow3:visible').length == 1){
        alert("You have reached maximum button limit")
    } else {
        $('.optoutcontainer').show();
        $('#footerdata').val('Not interested? Tap Stop promotions')
        $('#footerdata').prop('disabled',true);    
        $('.optoutbtn').prop('disabled',true);    
    }
}


function addVariable(element) {
    if(element=="body"){
        var cursorPos = $('#tempText').prop('selectionStart');
        var v = $('#tempText').val();
        let ac=autoCorrect(v);
        v=ac[1]
        $('#tempText').val(v);
        let count=ac[0]
        var textBefore = v.substring(0,  cursorPos );
        var textAfter  = v.substring( cursorPos, v.length );
        $('#tempText').val( textBefore+ "{{"+count+"}}" +textAfter );
        checkMaxLength("tempText",maxLength)
        document.getElementById("tempText").focus();
        document.getElementById("tempText").selectionEnd= cursorPos + ("{{"+count+"}}").length;
        addVars("bodyvars",$('#tempText').val())
    }
    if(element=="header"){
        var cursorPos = $('#headerdata').prop('selectionStart');
        var v = $('#headerdata').val();
        if(hasVars(v)!=null){
            alert("You can add only one variable in the header text.")
        }
        let ac=autoCorrectHeader(v);
        v=ac[1]
        $('#headerdata').val(v);
        let count=ac[0]
        var textBefore = v.substring(0,  cursorPos );
        var textAfter  = v.substring( cursorPos, v.length );
        if(count==0){
            $('#headerdata').val( textBefore+ "{{1}}" +textAfter );
        }
        document.getElementById("headerdata").focus();
        document.getElementById("headerdata").selectionEnd= cursorPos + 5;
        addVars("headervars",$('#headerdata').val())
    }
}


function autoCorrect(txt){
    let regex=/{{\w*}}/g;
    let arr=txt.match(regex)
    let count=1
    if(arr!=null){
       arr.forEach(function(item){
        txt=txt.replace(item,"{{"+count+"}}");
      count++;
      }); 
    }
    return [count,txt];
}

function autoCorrectHeader(txt,end=false){
    let regex=/{{\w*}}/g;
    let arr=txt.match(regex)
    let count=1
    if(arr!=null){
        if(arr.length>1){
            arr.forEach(function(item){
                if(count==1){
                    if(end){
                        txt=txt.replace(item,""); 
                        txt=txt+"{{"+count+"}}"
                    } else{
                       txt=txt.replace(item,"{{"+count+"}}"); 
                    }
                } else {
                   txt=txt.replace(item,"");
                }
                count++;
            }); 
            count=1;
        } else {
            if(end){
                txt=txt.replace(arr[0],""); 
                txt=txt+"{{1}}"
            } else {
               txt=txt.replace(arr[0],"{{1}}"); 
            }
            count=1;
        }
       
    } else {
        count=0;
    }
    return [count,txt];
}

function selectCTAButton(obj){
    if(obj.value=='CTA'){
        $('#btn-desc').html('Create up to 2 buttons that let customers respond to your message or take action.')
        $('.ctacontainer').show();
        $('.qrcontainer').hide();
    } else if(obj.value=='QUICK_REPLY'){
        $('#btn-desc').html('Create up to 3 buttons that let customers respond to your message or take action.')
        $('.ctacontainer').hide();
        $('.qrcontainer').show();
    } else {
        $('#btn-desc').html('Create buttons that let customers respond to your message or take action.')
        $('.ctacontainer').hide();
        $('.qrcontainer').hide();
    }
}

function dismiss(obj){
    
    if(obj=="optooutrow"){
        optout=false;
        $('.optoutbtn').prop('disabled',false);
        $('.optoutcontainer').hide();
        $('#footerdata').prop('disabled',false);
        $('#footerdata').val('')
    } else if(obj=='ctarow2'){
        $('#ctatypeweb').prop('disabled',false);
        $('#ctatypephone').prop('disabled',false);
        $('#'+obj).find('input:text').val('');
        $('#'+obj).hide();
    } else {
        console.log(obj)
        $('#'+obj).find('input:text').val('');
        $('#'+obj).hide();
        maxqr=false;
    }
    
};

function addButton(element){
    if(element=='cta'){
        if($('#ctarow2:visible').length == 1){
            alert("You have reached maximum number of CTA buttons.")
        }else {
            if($('#ctatype').val()=='PHONE_NUMBER'){
                $('#ctatypeweb').prop('disabled',true);
                $('#ctatypephone2').prop('disabled',true);
                $('#ctatypeweb2').prop('disabled',false);
                $('#ctatypephone').prop('disabled',false);
                $('#ctalinklabel2').html("Website URL");
                $('#ctatype2').val('URL')
                $('#ctalink2').val('')
                $('#ctalink2vars').html('')
            } else if($('#ctatype').val()=='URL'){
                $('#ctatypeweb').prop('disabled',false);
                $('#ctatypephone2').prop('disabled',false);
                $('#ctatypeweb2').prop('disabled',true);
                $('#ctatypephone').prop('disabled',true);
                $('#ctalinklabel2').html("Phone (E.164 format)");
                $('#ctatype2').prop('selectedIndex',0);
                $('#ctatype2').val('PHONE_NUMBER')
                $('#ctalink2').val('')
                $('#ctalink2vars').html('')
            }
            $("#ctarow2").show();
        }
    }
    if(element=='qr'){
        if(optout==false){
            if($('#qrrow2:visible').length == 0){
                $("#qrrow2").show();
            } else if($('#qrrow3:visible').length == 0){
                $("#qrrow3").show();  

            } else {
                maxqr=true;
                alert("You have already added maximum quick replies buttons.");
            }            
        } else {
            if($('#qrrow2:visible').length == 0 && $('#qrrow3:visible').length == 0){
                $("#qrrow2").show(); 
            } else {
                maxqr=true
                alert("You have already added maximum quick replies buttons.");
            }  
        }

    }
}

   $(".createNewTemplate").click(function() {
    $(".createNewTemplate").prop('disabled',true)
    $('.js-example-basic-multiple').select2();
    var selectArr = $(".js-example-basic-multiple").select2().val();

    var data = {
        clientId: $(".clientId").val(),
        name: $("#tempName").val().toLowerCase(),
        text: $("#tempText").val(),
        msg_type: $("#type_dropdown").val(),
        lang: $("#lang_dropdown").val(),
        category: $("#category_dropdown").val(),
        header: ($("#type_dropdown").val()!='TEXT')?'':$(`#headerdata`).val(),
        footer: $(`#footerdata`).val(),
        button: $(`#buttondata`).val(),
        headervars: $('.headervars').val(),
        headermedia: $('#headermedia').val(),
        components:{},
        agent_Id:selectArr
    };

    
    let components=[]

    let validate=validateComponents(data);
    if(validate!==true) {
        $(".createNewTemplate").prop('disabled',false)
        swal({
            title: validate,
            icon: "error",
            timer: 3500,
        });
    } else {

    swal({
      title: "Uploading Template",
      text: "Please wait while we submit your template to Meta for approval. Approval or rejection of a template is subject to Meta's message template guidelines.",
      icon: "https://admin.helloyubo.com/dist/img/yubo_loader.gif",
      closeOnClickOutside: false,
      buttons:false,
      className: "wait-loader"
    });

        //set header component
        if(data.msg_type=="TEXT" && data.header!=''){
            if(data.headervars!='' && data.headervars!=undefined && hasVars(data.header)!=null){
                components.push({
                   "type":"HEADER", 
                   "format":"TEXT",
                   "text":data.header,
                   "example":{ "header_text":[data.headervars] }
                 })
            } else{
                components.push({
                   "type":"HEADER", 
                   "format":"TEXT",
                   "text":data.header
                 })
            }
        } else if(data.msg_type=="IMAGE" || data.msg_type=="DOCUMENT" || data.msg_type=="VIDEO"){
                components.push({
                   "type":"HEADER", 
                   "format":data.msg_type, 
                   "example":{"header_handle":[data.headermedia]}
                 })
        }


        //set the body component
        if(hasVars(data.text)!=null){

            var inputs = $(".bodyvars");
            let arr=[]
            for(var i = 0; i < inputs.length; i++){
                arr.push($(inputs[i]).val());
            }

            components.push({
               "type":"BODY",
               "text":data.text,
               "example":{"body_text":[arr]}
             })
        } else {
            components.push({
               "type":"BODY",
               "text":data.text
            })
        }

        //set the footer component
        if(data.footer!=''){
            components.push( {
               "type":"FOOTER",
               "text":data.footer
             })
        }

        //set the button component
        if(data.button!="NONE"){
            let buttons=[]
            if(data.button=='QUICK_REPLY'){
                buttons.push({"type":"QUICK_REPLY", "text": $('#qrdata').val()})
                if($('#qrrow2:visible').length == 1){
                    buttons.push({"type":"QUICK_REPLY", "text": $('#qrdata2').val()})
                }
                if($('#qrrow3:visible').length == 1){
                    buttons.push({"type":"QUICK_REPLY", "text": $('#qrdata3').val()})
                }
                if(optout==true){
                    buttons.push({"type":"QUICK_REPLY", "text": $('#optoutdata').val()})
                }
            } else if(data.button=='CTA'){
                if($('#ctatype').val()=='PHONE_NUMBER'){
                    buttons.push({"type":"PHONE_NUMBER","text":$('#ctadata').val(),phone_number:$('#ctalink').val()})
                } else if($('#ctatype').val()=='URL'){
                    if(hasVars($('#ctalink').val())!=null && $('.ctalinkvars').val()!=''){
                        buttons.push({
                                "type":"URL",
                                "text":$('#ctadata').val(),
                                "url":$('#ctalink').val(),
                                "example":[$('.ctalinkvars').val()]
                           })
                    } else {
                        buttons.push({
                                "type":"URL",
                                "text":$('#ctadata').val(),
                                "url":$('#ctalink').val()
                           })  
                    }
                }
                if($('#ctarow2:visible').length == 1){
                    if($('#ctatype2').val()=='PHONE_NUMBER'){
                        buttons.push({"type":"PHONE_NUMBER","text":$('#ctadata2').val(),phone_number:$('#ctalink2').val()})
                    } else if($('#ctatype2').val()=='URL'){
                        if(hasVars($('#ctalink2').val())!=null && $('.ctalink2vars').val()!=''){
                            buttons.push({
                                    "type":"URL",
                                    "text":$('#ctadata2').val(),
                                    "url":$('#ctalink2').val(),
                                    "example":[$('.ctalink2vars').val()]
                               })
                        } else {
                            buttons.push({
                                    "type":"URL",
                                    "text":$('#ctadata2').val(),
                                    "url":$('#ctalink2').val()
                               })   
                        }
                    }
                }
            }
            if(buttons.length>0){
                components.push({
                   "type":"BUTTONS",
                   "buttons":buttons
                })
            }
        }


        data['components']=components

        console.log("templatedata",data)
        console.log("templatedata",data.components)
        $.ajax({
            data: data,
            async: false,
            cache: false,
            method: "POST",
            url: "/createNewWhatsappTemp",
            success: async function(data) {
           
                if (data.statusCode == 200) {
                    // document.getElementById("closeModalManually2").setAttribute("data-dismiss", "modal");
                    $(".createNewTemplate").prop('disabled',false)
                    swal({
                        title: data.message,
                        icon: "success",
                        timer: 3000,
                    });
                    document.getElementById("closeModalManually").setAttribute("data-dismiss", "modal");
                    getWhatsappCloudTempList();
                } else {
                    $(".createNewTemplate").prop('disabled',false)
                    let error_msg=""
                    if(data.error_user_msg!=undefined && typeof data.error_user_msg === 'string'){
                        error_msg=data.error_user_msg
                    }
                    swal({
                        title: data.message,
                        text:error_msg,
                        icon: "error"
                    });
                }
            }, error: function (jqXHR, textStatus, err) {  
                if(jqXHR.responseJSON.statusCode == 403) {
                    $(".createNewTemplate").prop('disabled',false)
                    window.location.replace("https://admin.helloyubo.com/login");
                } else {
                    $(".createNewTemplate").prop('disabled',false)
                    alert("Something went wrong, please try again")
                }
              }
        });
   
    }
    
   });

function hasVars(txt){
    let regex=/{{\w*}}/g;
    return txt.match(regex)
}

function addVars(obj,txt){
    $('#'+obj).html('')
    let arr=hasVars(txt)
    if(arr!=null){
        arr.forEach(function(item){
            $('#'+obj).append("<input type='text' class='"+obj+" form-control' placeholder='Enter a sample value for variable "+item+"'/>")
          }); 
    } else {
        $('#'+obj).html('')
    }
}

  function getWhatsappCloudTempList() {
   
    $.ajax({
    async: false,
    cache: false,
    method: "POST",
    url: "/whatsappCloudTemplateList",
    data: {},
    success: function(data) {
    if (data.statusCode == 200) {
        data.data.reverse();
        $("#temp_data_cloud").html("");
        let temp_count=data.data.length
        let appr_count=0
        let rej_count=0
        let pend_count=0
        let statuses=[]
        let languages=[]
        let msg_counts={"total":temp_count}
        $('#total_count').html('('+temp_count+')')
    
        for (const iterator of data.data) {

            if (statuses.indexOf(iterator.status)==-1) statuses.push(iterator.status);
            if (languages.indexOf(iterator.lang)==-1) languages.push(iterator.lang);

            (msg_counts.hasOwnProperty(iterator.status.toLowerCase()))?msg_counts[iterator.status.toLowerCase()]++:msg_counts[iterator.status.toLowerCase()]=1;
            (msg_counts.hasOwnProperty(iterator.lang))?msg_counts[iterator.lang]++:msg_counts[iterator.lang]=1;

            if(iterator.status.toLowerCase()=="approved"){
                appr_count++;
            }
       
            if(iterator.status.toLowerCase()=="rejected"){
                rej_count++;
            }
            if(iterator.status.toLowerCase()=="pending"){
                pend_count++;
            }
       
            let html = `
            <div class="lang_${iterator.lang} status_${iterator.status.toLowerCase()} cloudtemp">
            <h3 class="head_temp" tempid=${iterator._id} >${iterator.name} <span class="templang">${iterator.lang}</span><span class="status ${iterator.status.toLowerCase()}">${iterator.status}</span></h3>

            <div class="row approve-content" id="temp_data_cloud">
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 email-camp">
            <div class="temp_1" >
           
            <div class="tempuser_1" id=${iterator._id}>
           
            <p> ${iterator.text} </p>
           
            </div>
            
            </div>
           
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="    /* padding-right: 39px; */
            text-align: -webkit-right;
            /* padding-left: 15px; */
            margin-left: -19px;
            /* margin-right: 32px;">
           
            <div class="temp_detail_btn">
            <button class="temp_edit_btns" onclick="onEdit(id, this)"
            clientId=${iterator.clientId} id=${iterator._id}
            data-target="#edit_approve" data-toggle="modal"><i class='fa fa-edit'></i></button>
            <span class="my-group-action_span">|</span>

            <button onclick="onEdit(id, this)"
            clientId=${iterator.clientId} id=${iterator._id}
            data-target="#assignAgentModel" class="agentIconstyle" data-toggle="modal"><i class="fa fa-user" aria-hidden="true"></i></button>

            <button class="temp_detail_btn1" data=${iterator.name} clientId=${iterator.clientId} id=${iterator._id} 
            onclick="onCloudDelete(id, this)" ><i class='fa fa-trash-o'></i></button>
           
            <button class="temp_detail_btn2" ${(iterator.status!='APPROVED')?"disabled":""} clientId=${iterator.clientId} id=${iterator._id} 
            onclick="${(iterator.status=='APPROVED')?"onUse(id, this)":"alert('This template can not be used.')"}">Apply</button>
           
            </div>
            
            </div>
            </div>
            <hr class="line_horizontal">
            </div>
            ` 
           
            $("#temp_data_cloud").append(html);
            //$('#temp_count').html('Approved: '+appr_count+' | Rejected: '+rej_count+' | Pending: '+pend_count)
        }

        setFilterOptions(statuses,languages,msg_counts);
        $('#tempFilter').show();
    
    } else {
        console.log("error",data.error.error)
        $('#tempFilter').hide();
        swal({
            title: data.error.error.message,
            icon: "error"
        });
    }
   
    }, error: function (jqXHR, textStatus, err) {  
        if(jqXHR.responseJSON.statusCode == 403) {
        window.location.replace("https://admin.helloyubo.com/login");
        } else {
        alert("Something went wrong, please try again")
        }
      }
    });
   
   }

function uploadWhatsappMedia(obj){
    console.log(obj)
    console.log(obj.id)
    let fileInput=$('#'+obj.id).prop('files');
    var file = fileInput[0];
    var formData = new FormData()
    formData.append("whatsappMedia", file);

    $.ajax({
          method: "POST",
          url: "/uploadWhatsappMedia",
          data: formData,
          async: false,
          cache: false,
          contentType: false,
          processData: false,
          success: async function(data) {
            console.log("responseee data",data)
            if(data.statusCode == 200)  {
                $('#headermedia').val(data.data.h)
                swal({
                    title: data.message,
                    icon: "success",
                    timer: 3000,
                });
            } else {
                $('#chooseSample').val('') 
                $('#headermedia').val('')
                swal({
                    title: data.message,
                    icon: "error",
                    timer: 3500,
                });
            }  
          },
          error: function(jqXHR, textStatus, err) {
              console.log('errr', err);
          alert("Please refresh !");
          },
    });
}


function clearFormOnAdd(){
    $("#tempName").val('');
    $("#tempText").val('');
    $(`#headerText`).val('')
    $(`#footerText`).val('')
    $(`#buttonurlText`).val("false")
    $(`#buttonText`).val("false")
    $('.headerrow').show();
    $('.buttonurlrow').hide();
    $('#category_dropdown').val('UTILITY');
    $('#lang_dropdown').val('en');
    $('#type_dropdown').val('TEXT');
    $('.headermedia').html('')
    $('#headerdata').val('');
    $('#headervars').html('')
    $('#bodyvars').html('')
    $('#footerdata').val('');
    $('#footerdata').prop('disabled',false);
    $('#buttondata').val('NONE');
    $('.ctacontainer').hide();
    $('.qrcontainer').hide();
    $('#ctatype').val('PHONE_NUMBER');
    $('#ctadata').val('');
    $('#ctalinklabel').html("Phone (E.164 format)")
    $('#ctatype2').val('URL');
    $('#ctadata2').val('');
    $('#ctalinklabel2').html("Website URL")
    $('.optoutbtn').hide();
    $('.optoutbtn').prop('disabled',false);
    $('#optooutrow').hide();
    $('#qrrow2').hide();
    $('#qrrow3').hide();
    $('#qrdata').val('');
    $('#qrdata2').val('');
    $('#qrdata3').val('');
    $(".createNewTemplate").prop('disabled',false)
}

function validateComponents(data){
    let msg=true;
    if(data.name.trim()==""){
        msg="Please enter a valid template name.";
        return msg;
    }
    if(data.text.trim()==""){
        msg="Please provide a template message body.";
        return msg;
    }
    if(data.msg_type=="TEXT" && hasVars(data.header)!=null && data.headervars=="") {
        msg="Please provide a sample value for variable {{1}} in the header.";
        return msg;
    }
    if(data.msg_type!="TEXT" && data.headermedia==""){
        msg="Please provide a sample "+data.msg_type+" for header.";
        return msg;
    }
    return msg;
}


    function onCloudDelete(id, el) {       
        clientId = $(el).attr("clientId")
        tempId = id
        let tempName=$(el).attr("data")

        var data = {
            clientId: clientId,
            tempId: tempId,
            name:tempName
        };
       
        swal({
            title: "Are you sure want to delete ?",
            text: "Once deleted, you will not be able to recover this record!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    data: data,
                    async: false,
                    cache: false,
                    method: 'POST',
                    url: '/deleteCloudWhatsappTemp',
                    success: async function (data) {
                        if (data.statusCode == 200) {
                                await swal({
                                    title: "Data deleted successfully",
                                    icon: "success",
                                    timer: 3000
                                });
                                getWhatsappCloudTempList() 
                        } else {
                            swal({
                                title: data.message,
                                icon: "error",
                                timer: 3500,
                            });
                        }
                    }, error: function (jqXHR, textStatus, err) {  
                        if(jqXHR.responseJSON.statusCode == 403) {
                            window.location.replace("https://admin.helloyubo.com/login");
                        } else {
                            alert("Something went wrong, please try again")
                        }
                    }
                });
            } else {
                swal("Your data is safe now!");
            }
        });
    }
   
const lang_list=[
    {
        "lang": "Afrikaans",
        "code": "af"
    },
    {
        "lang": "Albanian",
        "code": "sq"
    },
    {
        "lang": "Arabic",
        "code": "ar"
    },
    {
        "lang": "Azerbaijani",
        "code": "az"
    },
    {
        "lang": "Bengali",
        "code": "bn"
    },
    {
        "lang": "Bulgarian",
        "code": "bg"
    },
    {
        "lang": "Catalan",
        "code": "ca"
    },
    {
        "lang": "Chinese (CHN)",
        "code": "zh_CN"
    },
    {
        "lang": "Chinese (HKG)",
        "code": "zh_HK"
    },
    {
        "lang": "Chinese (TAI)",
        "code": "zh_TW"
    },
    {
        "lang": "Croatian",
        "code": "hr"
    },
    {
        "lang": "Czech",
        "code": "cs"
    },
    {
        "lang": "Danish",
        "code": "da"
    },
    {
        "lang": "Dutch",
        "code": "nl"
    },
    {
        "lang": "English",
        "code": "en"
    },
    {
        "lang": "English (UK)",
        "code": "en_GB"
    },
    {
        "lang": "English (US)",
        "code": "en_US"
    },
    {
        "lang": "Estonian",
        "code": "et"
    },
    {
        "lang": "Filipino",
        "code": "fil"
    },
    {
        "lang": "Finnish",
        "code": "fi"
    },
    {
        "lang": "French",
        "code": "fr"
    },
    {
        "lang": "Georgian",
        "code": "ka"
    },
    {
        "lang": "German",
        "code": "de"
    },
    {
        "lang": "Greek",
        "code": "el"
    },
    {
        "lang": "Gujarati",
        "code": "gu"
    },
    {
        "lang": "Hausa",
        "code": "ha"
    },
    {
        "lang": "Hebrew",
        "code": "he"
    },
    {
        "lang": "Hindi",
        "code": "hi"
    },
    {
        "lang": "Hungarian",
        "code": "hu"
    },
    {
        "lang": "Indonesian",
        "code": "id"
    },
    {
        "lang": "Irish",
        "code": "ga"
    },
    {
        "lang": "Italian",
        "code": "it"
    },
    {
        "lang": "Japanese",
        "code": "ja"
    },
    {
        "lang": "Kannada",
        "code": "kn"
    },
    {
        "lang": "Kazakh",
        "code": "kk"
    },
    {
        "lang": "Kinyarwanda",
        "code": "rw_RW"
    },
    {
        "lang": "Korean",
        "code": "ko"
    },
    {
        "lang": "Kyrgyz (Kyrgyzstan)",
        "code": "ky_KG"
    },
    {
        "lang": "Lao",
        "code": "lo"
    },
    {
        "lang": "Latvian",
        "code": "lv"
    },
    {
        "lang": "Lithuanian",
        "code": "lt"
    },
    {
        "lang": "Macedonian",
        "code": "mk"
    },
    {
        "lang": "Malay",
        "code": "ms"
    },
    {
        "lang": "Malayalam",
        "code": "ml"
    },
    {
        "lang": "Marathi",
        "code": "mr"
    },
    {
        "lang": "Norwegian",
        "code": "nb"
    },
    {
        "lang": "Persian",
        "code": "fa"
    },
    {
        "lang": "Polish",
        "code": "pl"
    },
    {
        "lang": "Portuguese (BR)",
        "code": "pt_BR"
    },
    {
        "lang": "Portuguese (POR)",
        "code": "pt_PT"
    },
    {
        "lang": "Punjabi",
        "code": "pa"
    },
    {
        "lang": "Romanian",
        "code": "ro"
    },
    {
        "lang": "Russian",
        "code": "ru"
    },
    {
        "lang": "Serbian",
        "code": "sr"
    },
    {
        "lang": "Slovak",
        "code": "sk"
    },
    {
        "lang": "Slovenian",
        "code": "sl"
    },
    {
        "lang": "Spanish",
        "code": "es"
    },
    {
        "lang": "Spanish (ARG)",
        "code": "es_AR"
    },
    {
        "lang": "Spanish (SPA)",
        "code": "es_ES"
    },
    {
        "lang": "Spanish (MEX)",
        "code": "es_MX"
    },
    {
        "lang": "Swahili",
        "code": "sw"
    },
    {
        "lang": "Swedish",
        "code": "sv"
    },
    {
        "lang": "Tamil",
        "code": "ta"
    },
    {
        "lang": "Telugu",
        "code": "te"
    },
    {
        "lang": "Thai",
        "code": "th"
    },
    {
        "lang": "Turkish",
        "code": "tr"
    },
    {
        "lang": "Ukrainian",
        "code": "uk"
    },
    {
        "lang": "Urdu",
        "code": "ur"
    },
    {
        "lang": "Uzbek",
        "code": "uz"
    },
    {
        "lang": "Vietnamese",
        "code": "vi"
    },
    {
        "lang": "Zulu",
        "code": "zu"
    }
]


$(document).ready(function(){
    getWhatsappCloudTempList();
    let options=""
    lang_list.forEach(function(item){
        options+=`<option value="${item.code}">${item.lang}</option>`
    });
    $('#lang_dropdown').html(options);
})


function getLang(code){
    let lang = lang_list.filter(item=>{if(item.code==code){
        return item
    }})
    return lang[0]["lang"];
}


function setFilterOptions(statuses,langs,count){
    try{
        let statusOptions='<option value="all">All Templates ('+count['total']+')</option>'
        statuses.forEach(function(item){
            statusOptions+=`<option value="${item.toLowerCase()}">${item} (${count[item.toLowerCase()]})</option>`
        });
        $('#filter_status').html(statusOptions);
        let langOptions='<option value="all">Any Language ('+count['total']+')</option>'
        langs.forEach(function(item){
            langOptions+=`<option value="${item}">${getLang(item)} (${count[item]})</option>`
        });
        $('#filter_lang').html(langOptions);        
    } catch(err){
        console.log("error while setting filter options",err)
        $('#tempFilter').hide();
    }

}


function applyFilter(){
    let tempdivs=Object.keys($('.cloudtemp'))
    let status=$('#filter_status').val()
    let lang=$('#filter_lang').val()
    console.log(tempdivs)
    tempdivs.forEach(function(key,index){
        $($('.cloudtemp')[key]).show()
    })
    if(status=="all" && lang=="all"){
        tempdivs.forEach(function(key,index){
            $($('.cloudtemp')[key]).show()
        })
    } else if(status=="all"){
        tempdivs.forEach(function(key,index){
            let obj=$('.cloudtemp')[key]
            if($(obj).hasClass("lang_"+lang)){
                $(obj).show();
            } else {
                $(obj).hide();
            }
        })
    } else if(lang=="all"){
        tempdivs.forEach(function(key,index){
            let obj=$('.cloudtemp')[key]
            if($(obj).hasClass("status_"+status)){
                $(obj).show();
            } else {
                $(obj).hide();
            }
        })
    } else {
        tempdivs.forEach(function(key,index){
            let obj=$('.cloudtemp')[key]
            if($(obj).hasClass("lang_"+lang) && $(obj).hasClass("status_"+status)){
                $(obj).show();
            } else {
                $(obj).hide();
            }
        })
    }
}
//Model closing code
function discard(e) {
    var r = confirm("Are you sure you want to discard changes");
    if (r === true) {
        $(".closeModal").attr("data-dismiss", "modal");
    } else {
        $(".closeModal").removeAttr("data-dismiss");
    }
}