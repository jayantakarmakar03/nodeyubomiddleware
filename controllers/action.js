const db = require("../models/all-models"),
  jsonpath = require("jsonpath"),
  axios = require("axios");

module.exports = {
  callActionApi: callActionApi,
  clientAPI: clientAPI,
  jsonPathConversion: jsonPathConversion
}


async function jsonPathConversion(clientApiResp, pythonApiResp, actionData) {
  let dbData = actionData.actionResponse,
    newDbData = [], text = pythonApiResp.text, products = pythonApiResp.tree ? (pythonApiResp.tree.products ? pythonApiResp.tree.products : []) : [];
  clientApiResp = clientApiResp.data;

  dbData.forEach(element => {
    let obj = {};
    obj[Object.keys(element)[0]] = jsonpath.query(clientApiResp, element[Object.keys(element)[0]])[0];
    newDbData.push(obj);
    if (jsonpath.query(clientApiResp, element[Object.keys(element)[0]])[0]) {
      text = text.replace("[" + Object.keys(element)[0] + "]", jsonpath.query(clientApiResp, element[Object.keys(element)[0]])[0])
    } else {
      text = "Looks like the parameters required for this are not correct, please check the parameters you have provided and try again."
    }
    (actionData.actionSlotFields).forEach(async element => {
      delete pythonApiResp.contextObj.params[element]
    })
  });
  let prdcts;
  if (products.length > 0) {
    // clientApiResp.data = clientApiResp
    prdcts = await jsonpathdata(products[0], clientApiResp)
    pythonApiResp.tree.products = prdcts
  }
  pythonApiResp["text"] = text;
  return pythonApiResp;
}

// API to hit the client's given url in actions section
async function callActionApi(data) {
  try {
    let response = await axios(data)
    // response = {
    //   "status": "success",
    //   "message": "PRODUCT_FOUND",
    //   "data": [{
    //     "label": "Envigor Tea",
    //     "url": "https://nirogam.com/products/envigor-tea",
    //     "imageUrl": "https://innodesigns.s3.amazonaws.com/product/341/HGdSyeyRMxgSmFT0FlCdLYK8MT5Ax8Lkvfdso2Rh.png"
    //   }]
    // };
    return { statusCode: 200, status: "success", data: response.data }
  } catch (err) {
    return { statusCode: 201, status: "failed", data: err }
  }
}

async function clientAPI(pythonApiResp, actionData) {
  let data = {
    method: actionData.actionMethod,
    url: await setTheUrlParams(pythonApiResp.contextObj.params, actionData.actionUrl),
    // headers: JSON.stringify((actionData.actionHeaders).replace(/\n/g, '')),
    headers: {},
    data: JSON.parse(await setTheBodyParams(pythonApiResp.contextObj.params, actionData.actionData))
  }
  let clientApiResp = await callActionApi(data);
  let jsonresp = await jsonPathConversion(clientApiResp, pythonApiResp, actionData);
  return jsonresp;
}
async function setTheBodyParams(contextObj, bodyData) {
  let data = bodyData;
  for (var contextKey of Object.keys(contextObj)) {
    data = JSON.stringify(JSON.parse(data), function (key, value) {
      if (value == "[" + contextKey + "]") {
        return contextObj[contextKey][0] ? contextObj[contextKey][0] : contextObj[contextKey];
      } else {
        return value;
      }
    });
  }
  return data;
}

async function setTheUrlParams(contextObj, url) {
  for (var contextKey of Object.keys(contextObj)) {
    if (url.includes("[" + contextKey + "]")) {
      url = url.replace("[" + contextKey + "]", contextObj[contextKey][0]);
    } else {
      url = url;
    }
  }
  return url;
}

async function jsonpathdata(products, response) {
  // console.log("jsonpath", products, response)
  try {
    let result = [];
    for (key in products) {
      if (products[key].includes("$.")) {
        console.log("key", key, products[key], response)
        var name = jsonpath.query(response, products[key])
        products[key] = name;
      }
    }
    for (let i = 0; i < name.length; i++) {
      let obj = Object.assign({}, products);
      for (let key in products) {
        if (obj[key]) {
          if (key == "id") {
            obj['id'] = obj['id'] ? obj['id'] : toString(Math.floor(100000 + Math.random() * 900000))
          } else {
            obj[key] = products[key][i];
          }
        }
      }
      result.push(obj);
    }
    return result;
  } catch (err) {
    throw err
  }
}
