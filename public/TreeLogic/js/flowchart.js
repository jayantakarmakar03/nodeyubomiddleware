var globalData = "";
var operatorI = 3;
var selectedOperatorId = "";
var selectedOperatorData = "";
var globalOperatorId = "";
var typeOpt = 0;
var cntctTypeOpt = 0;
var gnrlCrmCkbx = false;
// var cntctCrmCkbx = false;
var deletedElementId = [];
var maxFields = 20;
var x = 1;
var settings = {};
var nodeType = "";
var type = "";
var position = 40;
var pTop = 20;
var pLeft = 20;
var dataChanged = false;
var globalFormData = "";
var formBuilder;
var prdct_count = 1;
var deletedPrdctId = [];
var hubApiKey;
var codeNodeOperatorID
var analyticsOPT = 0

window.addEventListener("beforeunload", function(e) {
    if (dataChanged) {
        var confirmationMessage =
            "It looks like you have been editing something. " +
            "If you leave before saving, your changes will be lost.";
        (e || window.event).returnValue = confirmationMessage;
        return confirmationMessage;
    }
});

$(document).ready(function() {
    $(".product_add_field").attr("data-unique-id", new Date().getTime()); //to get unique product id
    LoadFromDatabase();
    loadintentsname();
    //To check for basic and pro user
    checkUserType("hide", function(resp) {
        if (resp == "basic") {
            $(".form-group.CRMdiv.row").css("visibility", "hidden")
        } else {
            $(".form-group.CRMdiv.row").css("visibility", "visible")
        }
    });

    dataChanged = false;
    var $flowchart = $("#flowchartworkspace");
    var $container = $flowchart.parent();

    settings["canvasHeight"] = $(".flowchart-links-layer").height();
    settings["canvasWidth"] = $(".flowchart-links-layer").width();
    settings["maxFields"] = maxFields;

    $("body").keyup(function(e) {
        // Alt/option + x to delete any node
        if (e.keyCode == 88 && e.altKey) {
            deleteSelectedItem($flowchart);
            // selectedOperatorId = $(".selected").attr('id');

            // if (selectedOperatorId) {
            //     swal({
            //         title: "Are you sure?",
            //         text: "Once deleted, you will not be able to recover this tree node",
            //         icon: "warning",
            //         buttons: true,
            //         dangerMode: true,
            //     })
            //         .then((willDelete) => {
            //             if (willDelete) {
            //                 $flowchart.flowchart('deleteSelected');
            //                 globalData = $flowchart.flowchart('getData');
            //                 createSelectElement();
            //                 swal("Selected item has been deleted!", {
            //                     icon: "success",
            //                 });
            //                 dataChanged = true
            //                 //operatorI--;
            //             }
            //         });
            // } else {
            //     swal("Error", "Please select a node first", "error");
            // }
        }

        // Alt/Option + C
        if (e.keyCode == 67 && e.altKey) {
            selectedOperatorId = $(".selected").attr("id");
            globalData = $flowchart.flowchart("getData");
            operatorJson = getJsonArray(globalData);

            selectedOperatorData = operatorJson[0][selectedOperatorId];
        }

        // Alt/Option + V
        if (e.keyCode == 86 && e.altKey) {
            if (typeof selectedOperatorData != "undefined") {
                var operatorData = createCopyNode(selectedOperatorData);
                var operatorId = "operator" + operatorI;
                $flowchart.flowchart("createOperator", operatorId, operatorData);
                operatorI++;
                globalData = $flowchart.flowchart("getData");
                createSelectElement();
            } else {
                swal("Error", "No nodes are copied. Please copy a node first", "error");
            }
        }
    });

    $flowchart.flowchart({
        verticalConnection: true,
        data: defaultFlowchartData,
        defaultSelectedLinkColor: "#000055",
        linkWidth: 3,
        grid: 20,
        multipleLinksOnInput: true,
        multipleLinksOnOutput: false,
    });

    globalData = $flowchart.flowchart("getData");
    createSelectElement();

    function getOperatorData($element) {
        var nbInputs = parseInt($element.data("nb-inputs"), 10);
        var nbOutputs = parseInt($element.data("nb-outputs"), 10);
        var data = {
            properties: {
                title: $element.text(),
                question: "My first question",
                inputs: {},
                outputs: {},
            },
        };

        var i = 0;
        for (i = 0; i < nbInputs; i++) {
            data.properties.inputs["input_" + i] = {
                label: "Input " + (i + 1),
            };
        }
        for (i = 0; i < nbOutputs; i++) {
            data.properties.outputs["output_" + i] = {
                label: "Output " + (i + 1),
            };
        }

        return data;
    }

    flow2Text();

    //var $operatorProperties = $('#operator_properties');
    //$operatorProperties.hide();
    var $linkProperties = $("#link_properties");
    $linkProperties.hide();
    var $operatorTitle = $("#operator_title");
    var $linkColor = $("#link_color");

    $flowchart.flowchart({
        onOperatorSelect: function(operatorId) {
            //$operatorProperties.show();
            globalOperatorId = operatorId;
            $operatorTitle.val($flowchart.flowchart("getOperatorTitle", operatorId));
            getOperatorJson(operatorId);
            return true;
        },
        onOperatorUnselect: function() {
            //$operatorProperties.hide();
            return true;
        },
        onLinkSelect: function(linkId) {
            -$linkProperties.show();
            $linkColor.val($flowchart.flowchart("getLinkMainColor", linkId));
            return true;
        },
        onLinkUnselect: function() {
            $linkProperties.hide();
            return true;
        },
    });

    $linkColor.change(function() {
        var selectedLinkId = $flowchart.flowchart("getSelectedLinkId");
        if (selectedLinkId != null) {
            $flowchart.flowchart(
                "setLinkMainColor",
                selectedLinkId,
                $linkColor.val()
            );
        }
    });

    $(".delete_selected_button").click(async function() {
        deleteSelectedItem($flowchart);
    });

    $("#set_data").click(text2Flow);

    $(".setting_button").click(function() {
        getSetting();
    });

    $("#save-property").click(function() {
        updateProperty();
    });

    $("#get_data").click(function() {
        var $flowchart = $("#flowchartworkspace");
        globalData = $flowchart.flowchart("getData");
        globalData["settings"] = settings;
        $("#flowchart_data").val(JSON.stringify(globalData, null, 2));
        $flowchart.flowchart("setData", globalData);
        $("#json-modal").modal("show");
    });

    $(".create_root_operator").click(function() {
        operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
        var operatorId = "operator" + operatorI;
        var operatorData = nodeRoot(operatorI); //nodeGeneral(operatorI);
        $flowchart.flowchart("createOperator", operatorId, operatorData);
        operatorI++;
        globalData = $flowchart.flowchart("getData");
        createSelectElement();
        dataChanged = true;
    });

    $(".create_general_operator").click(function() {
        console.log("create general operator")
        operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
        var operatorId = "operator" + operatorI;
        var operatorData = nodeGeneral(operatorI); //nodeGeneral(operatorI);
        $flowchart.flowchart("createOperator", operatorId, operatorData);
        operatorI++;
        globalData = $flowchart.flowchart("getData");
        createSelectElement();
        dataChanged = true;
    });

    $(".create_email_operator").click(function() {
        operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
        var operatorId = "operator" + operatorI;
        var operatorData = nodeEmailCheck(operatorI); //nodeGeneral(operatorI);
        $flowchart.flowchart("createOperator", operatorId, operatorData);
        operatorI++;
        globalData = $flowchart.flowchart("getData");
        createSelectElement();
        dataChanged = true;
    });

    $(".create_phone_operator").click(function() {
        operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
        var operatorId = "operator" + operatorI;
        var operatorData = nodePhoneCheck(operatorI); //nodeGeneral(operatorI);
        $flowchart.flowchart("createOperator", operatorId, operatorData);
        operatorI++;
        globalData = $flowchart.flowchart("getData");
        createSelectElement();
        dataChanged = true;
    });

    $(".create_product_operator").click(function() {
        operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
        var operatorId = "operator" + operatorI;
        var operatorData = nodeProduct(operatorI); //nodeGeneral(operatorI);
        $flowchart.flowchart("createOperator", operatorId, operatorData);
        operatorI++;
        globalData = $flowchart.flowchart("getData");
        createSelectElement();
        dataChanged = true;
    });

    $(".create_contact_operator").click(function() {
        operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
        var operatorId = "operator" + operatorI;
        var operatorData = nodeContact(operatorI); //nodeGeneral(operatorI);
        $flowchart.flowchart("createOperator", operatorId, operatorData);
        operatorI++;
        globalData = $flowchart.flowchart("getData");
        createSelectElement();
        dataChanged = true;
    });

    $(".create_image_operator").click(function() {
        operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
        var operatorId = "operator" + operatorI;
        var operatorData = nodeImage(operatorI); //nodeGeneral(operatorI);
        $flowchart.flowchart("createOperator", operatorId, operatorData);
        operatorI++;
        globalData = $flowchart.flowchart("getData");
        createSelectElement();
        dataChanged = true;
    });

    $(".create_video_operator").click(function() {
        operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
        var operatorId = "operator" + operatorI;
        var operatorData = nodeVideo(operatorI); //nodeGeneral(operatorI);
        $flowchart.flowchart("createOperator", operatorId, operatorData);
        operatorI++;
        globalData = $flowchart.flowchart("getData");
        createSelectElement();
        dataChanged = true;
    });

    $(".create_appointment_operator").click(function() {
        operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
        var operatorId = "operator" + operatorI;
        var operatorData = nodeBookAppointment(operatorI); //nodeGeneral(operatorI);
        $flowchart.flowchart("createOperator", operatorId, operatorData);
        operatorI++;
        globalData = $flowchart.flowchart("getData");
        createSelectElement();
        dataChanged = true;
    });

    $(".create_leaf_operator").click(function() {
        operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
        var operatorId = "operator" + operatorI;
        var operatorData = nodeLeaf(operatorI); //nodeGeneral(operatorI);
        $flowchart.flowchart("createOperator", operatorId, operatorData);
        operatorI++;
        globalData = $flowchart.flowchart("getData");
        createSelectElement();
        dataChanged = true;
    });

    /**Code Node************************************************************************* */
    $(".create_code_operator").click(function() {
        console.log("Create code node function######################")
        operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
        var operatorId = "operator" + operatorI;
        codeNodeOperatorID = "n" + operatorI
        var operatorData = nodeCode(operatorI); //nodeGeneral(operatorI);
        $flowchart.flowchart("createOperator", operatorId, operatorData);
        operatorI++;
        globalData = $flowchart.flowchart("getData");
        createSelectElement();
        dataChanged = true;
    });
    /**************************************************************************** */


    $("#node-list").change(function() {
        var element = $("#" + $(this).children(":selected").attr("key"));
        $(".flowchart-operator").removeClass("highlight");
        element.addClass("highlight");
        $(".panel-body").animate({
                scrollTop: $(element).position().top - 200,
                scrollLeft: $(element).position().left - 200,
            },
            600
        );
    });

    var $draggableOperators = $(".draggable_operator");
    $draggableOperators.draggable({
        cursor: "move",
        opacity: 0.7,

        // helper: 'clone',
        appendTo: "body",
        zIndex: 1000,

        helper: function(e) {
            var $this = $(this);
            var data = getOperatorData($this);
            return $flowchart.flowchart("getOperatorElement", data);
        },
        stop: function(e, ui) {
            var $this = $(this);
            var elOffset = ui.offset;
            var containerOffset = $container.offset();
            if (
                elOffset.left > containerOffset.left &&
                elOffset.top > containerOffset.top &&
                elOffset.left < containerOffset.left + $container.width() &&
                elOffset.top < containerOffset.top + $container.height()
            ) {
                var flowchartOffset = $flowchart.offset();

                var relativeLeft = elOffset.left - flowchartOffset.left;
                var relativeTop = elOffset.top - flowchartOffset.top;

                var positionRatio = $flowchart.flowchart("getPositionRatio");
                relativeLeft /= positionRatio;
                relativeTop /= positionRatio;

                var data = getOperatorData($this);
                data.left = relativeLeft;
                data.top = relativeTop;

                $flowchart.flowchart("addOperator", data);
            }
        },
    });

    $("#text-opt").click(function() {
        if ($(this).prop("checked")) {
            typeOpt = 1;
            //$('#answer-section').addClass('hidden');
        } else {
            typeOpt = 0;
            //$('#answer-section').removeClass('hidden');
        }
    });
    $("#analytics-opt").click(function() {
        if ($(this).prop("checked")) {
            analyticsOpt = 1;
            //$('#answer-section').addClass('hidden');
        } else {
            analyticsOpt = 0;
            //$('#answer-section').removeClass('hidden');
        }
    })

    //To save general node info...
    $("#save").click(function() {
        validate = updateJson();
        var s3_link = $("#conv_fileLink_s3").val()
        if (validate == "duplicate name") {
            swal("Error", "Name already exist. Please change the name", "error");
        } else {
            if (validate == true) {

                if ($("#attach_media_chkbox").prop('checked') == true) {
                    if (!s3_link) {
                        swal("Error", "Please upload file link or file", "error");
                    } else {

                        swal("Tree structure has been updated", {
                            icon: "success",
                            buttons: false,
                            timer: 1500,
                        });
                        $("#property-modal").modal("hide");
                        $flowchart.flowchart(
                            "setOperatorTitle",
                            globalOperatorId,
                            $("#operator_title").val()
                        );
                        createSelectElement();

                    }
                } else {

                    swal("Tree structure has been updated", {
                        icon: "success",
                        buttons: false,
                        timer: 1500,
                    });
                    $("#property-modal").modal("hide");
                    $flowchart.flowchart(
                        "setOperatorTitle",
                        globalOperatorId,
                        $("#operator_title").val()
                    );
                    createSelectElement();

                }

            } else {
                swal("Error", "One or more fields are blank", "error");
            }
        }
    });

    $("#saveCodeNode").click(function() {
        console.log("In save code function", true)
        validate = updateJson();
        console.log("Validate", validate)
        if (validate == "duplicate name") {
            swal("Error", "Name already exist. Please change the name", "error");
        } else {
            if (validate == true) {
                // swal("Tree structure has been updated", {
                //     icon: "success",
                //     buttons: false,
                //     timer: 1500,
                // });
                saveCode()
                $("#property-modal-code").modal("hide");
                $flowchart.flowchart(
                    "setOperatorTitle",
                    globalOperatorId,
                    $("#code_operator_title").val()
                );

                createSelectElement();
            } else {
                swal("Error", "One or more fields are blank", "error");
            }
        }

    })

    async function saveCode() {
        let nodeTitle = document.getElementById("code_operator_title").value
        let nodeID = $("#operator_id").val();
        console.log("nodeID", nodeID)
            //let node_id = (nodeID.split("r"))[2]
        let node_id = nodeID.split(/(\d+)/)[1]
            // console.log("node_id", node_id)
        const userCode = codeEditor.getValue();

        if(userCode.length == 0 || !userCode){
            alert('Codenode can not be empty, please add some code here')
        }else{
        $.ajax({
            type: "POST",
            url: "/saveCodes",
            data: { code: userCode, nodeTitle: nodeTitle, nodeID: "n" + node_id },
            success: function(data) {
                if (data) {
                    if (data.statusCode == 200) {
                        //alert("Code saved successfully")
                        swal("Code saved successfully and tree structure has been updated", {
                            icon: "success",
                            buttons: false,
                            timer: 1500,
                        });
                    } else {
                        swal({
                            title: data.message,
                            icon: "error",
                            type: 'info',
                            confirmButtonText: 'OK'
                        }).then(() => {
                            location.reload();
                        });
                    }
                }
            },
            error: function(jqXHR, textStatus, err) {
                if (jqXHR.responseJSON.statusCode == 403) {
                    window.location.replace("https://admin.helloyubo.com/login");
                } else {
                    alert("Something went wrong, please try again")
                }
            }
        })
        }

    }

    // Form builder integration and data saving
    $("#contact_text_opt").click(function() {
        if ($(this).prop("checked")) {
            cntctTypeOpt = 1;
        } else {
            cntctTypeOpt = 0;
        }
    });

    let options = {
        disabledActionButtons: ["data", "save"],
        // fields : [{
        //   label: 'Appointment',
        //   attrs: {
        //     type: 'datetime-local'
        //   },
        //   icon: '📅'
        // }],
        // templates: {
        //   "datetime-local": function(fieldData) {
        //     return {  field: '<input type="datetime-local" id= "' + fieldData.name + '">',
        //       onRender: function() {

        //       }
        //     };
        // }
        // }
    }
    formBuilder = $(".build-wrap").formBuilder(options);

    //To save contact modal info....
    $("#saveData").on("click", () => {
        $("#formData").val(formBuilder.actions.getData("json"));

        validate = updateJson();
        if (validate == "duplicate name") {
            swal("Error", "Name already exist. Please change the name", "error");
        } else {
            if (validate == true) {
                swal("Tree structure has been updated", {
                    icon: "success",
                    buttons: false,
                    timer: 1500,
                });
                $("#property-modal-contact").modal("hide");
                $flowchart.flowchart(
                    "setOperatorTitle",
                    globalOperatorId,
                    $("#contact_operator_title").val()
                );
                createSelectElement();
            } else {
                swal("Error", "One or more fields are blank", "error");
            }
        }
    });

    $("#save_local").click(saveToLocalStorage);
    $("#save_data").click({ train: false }, saveToDatabase); //passing train false to indicate that we need to only save the tree not train it
    // $("#train_tree").click(traintree);
    $("#train_tree").click(function() {
        // checkUserType(function (resp) {
        traintree();
        // });
    });

    $("#load_local").click(LoadFromDatabase);
    $("#save_tree").click(function() {
        let treedata = JSON.parse($("#flowchart_data").val());
        saveToDatabase({ train: false }, treedata);
        LoadFromDatabase();
    });
    // $('#load_local').click(loadFromLocalStorage);

    var add_button = $(".add_form_field");
    var wrapper = "";

    $(add_button).click(function(e) {
        e.preventDefault();
        if (x < maxFields) {
            x++;
            switch (nodeType) {
                case "image":
                    wrapper = $(".image-container");
                    type = "image";
                    $(wrapper).append(
                        '<div class="form-group new-fields" id="form-group-' +
                        x +
                        '"><div class="col-md-11"><input type="file" name="myFilesUpload[]" class="form-control" id="output_' +
                        x +
                        '"/></div><div class="col-md-1"><span class="delete btn btn-danger tree_model_sustract" onclick="deleteOption(' +
                        x +
                        ')" id="' +
                        x +
                        '" style="font-size:16px; font-weight:bold;"> &nbsp;- </span></div></div>'
                    ); //add input box
                    break;
                case "video":
                    wrapper = $(".video-container");
                    type = "video";
                    $(wrapper).append(
                        '<div class="form-group new-fields" id="form-group-' +
                        x +
                        '"><div class="col-md-11"><input type="file" name="myFilesUpload[]" class="form-control" id="output_' +
                        x +
                        '"/></div><div class="col-md-1"><span class="delete btn btn-danger tree_model_sustract" onclick="deleteOption(' +
                        x +
                        ')" id="' +
                        x +
                        '" style="font-size:16px; font-weight:bold;"> &nbsp;- </span></div></div>'
                    ); //add input box
                    break;
                default:
                    wrapper = $(".general-container");
                    type = "general";

                    $(wrapper).append(
                        '<div class="form-group new-fields" id="form-group-' +
                        x +
                        '"><div class="col-md-10" id="text-field-' + x + '"><input type="text" ' +
                        ' name="mytext[]" placeholder="text" class="form-control" data-text="' + x + '" id="output_' + x +
                        '"/></div>' +

                        '<div class="hidden" id="score-weight-' + x + '"> ' +
                        '<div class="col-sm-2"> <input type="text" placeholder="weight" class="form-control " id="weight_' +
                        x + '" data-text="' + x + '" /> </div>' +

                        '<div class="col-sm-3"> <input type="text" placeholder="label" class="form-control " id="label_' +
                        x + '" data-text="' + x + '" /> </div>' +
                        '</div>' +


                        '<div class="col-md-1"><span class="delete btn btn-danger tree_model_sustract" onclick="deleteOption(' + x + ')" id="' + x + '">- </span></div>' +
                        '<div class="col-md-1"> <button class="btn tree_model_add newStarbtn-' + x + ' " onclick="onAddScoreWeight(' + x + ')" id="' + x + '">' +
                        ' <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">' +
                        '<path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>' +
                        '</svg> </button>  </div> </div>'
                    ); //add input box

            }
        } else {
            swal("You Reached the limits", {
                icon: "warning",
            });
        }
    });

    //for adding products field
    var add_prdct_button = $(".add_prdct_field");
    var wrapper = "";

    $(add_prdct_button).click(function(e) {
        e.preventDefault();
        if (prdct_count < maxFields) {
            prdct_count++;
            if (nodeType == "product") {
                wrapper = $(".prdct-container");
                type = "product";
                $(wrapper).append(
                    '<div class="form-group append_prdct" id="form-group-' +
                    prdct_count +
                    '"><div class="col-md-11 prodcut-add-div">' +
                    '<input type="text" name="prdctstitle[]" class="form-control product_add_field" placeholder="Title" id="prdct-title" data-unique-id="' +
                    new Date().getTime() +
                    '"/>' +
                    '<input type="text" name="prdctsimg[]" class="form-control product_add_field" placeholder="Image Url" id="prdct-img" data-unique-id="' +
                    new Date().getTime() +
                    '"/>' +
                    '<input type="text" name="prdctsprice[]" class="form-control product_add_field" placeholder="Price" id="prdct-price" data-unique-id="' +
                    new Date().getTime() +
                    '"/>' +
                    '<input type="text" name="prdctsdesc[]" class="form-control product_add_field" placeholder="Description" id="prdct-desc" data-unique-id="' +
                    new Date().getTime() +
                    '"/>' +
                    '<input type="text" name="prdctstitle[]" class="form-control product_add_field" placeholder="Cart Link" id="cart-link" data-unique-id="' +
                    new Date().getTime() +
                    '"/>' +
                    '<input type="text" name="prdctstitle[]" class="form-control product_add_field" placeholder="Info Link" id="info-link" data-unique-id="' +
                    new Date().getTime() +
                    '"/>' +
                    '</div><div class="col-md-1"><span class="delete btn btn-danger tree_model_sustract" onclick="deletePrdctField(' +
                    prdct_count +
                    ')" id="' +
                    prdct_count +
                    '" >-</span></div></div>'
                ); //add input box
            }
        } else {
            swal("You Reached the limits", {
                icon: "warning",
            });
        }
    });
    //end of adding products field

    //hubspot integration start
    $("#gnrl_crm_ckbx").click(async function() {
        if ($(this).prop("checked")) {
            gnrlCrmCkbx = true;
            $("#gnrl_properties_div").removeClass("hide");
            if ($("#gnrl_crm_integration").val())
                await property_selected($("#gnrl_crm_integration").val());
        } else {
            gnrlCrmCkbx = false;
            $(".new-fields").remove();
            $("#output_1").val("");
            $("#gnrl_properties_div").addClass("hide");
        }
    });

    //hubspot property integration
    $(".properties").change(async function() {
        await property_selected($(this).val());
    });

    async function property_selected(value) {
        $(".new-fields").remove();
        $("#output_1").val("");
        if (value != "Select Property" && value) {
            $.ajax({
                type: "POST",
                url: "/readAProperty",
                data: { propertyName: value },
                success: async function(resp) {
                    let wrapper = $(".general-container");
                    let options = resp.options;
                    x = 2;
                    options.forEach(function(element, index) {
                        if (index == 0) {
                            $("#output_1").val(element.value);
                        } else {
                            $(wrapper).append(
                                '<div class="form-group new-fields" id="form-group-' +
                                x +
                                '"><div class="col-md-11"><input type="text" placeholder="text" name="mytext[]" value="' +
                                element.value +
                                '" class="form-control" id="output_' +
                                x +
                                '"/></div><div class="col-md-1"><span class="delete btn btn-danger tree_model_sustract" onclick="deleteOption(' +
                                x +
                                ')" id="' +
                                x +
                                '" style="font-size:16px; font-weight:bold;"> &nbsp;- </span></div></div>'
                            ); //add input box
                            x++;
                        }

                    });
                },
                error: function(jqXHR, textStatus, err) {
                    alert("Something went wrong, please refresh the page");
                },
            });
        }
    }
    //hubspot integration end

    $("form[name='image-form']").on("submit", function(e) {
        e.preventDefault();
        formData = new FormData(this);
        formId = "#image-form";
        uploadFile(formData, formId, "image");
    });

    $("form[name='video-form']").on("submit", function(e) {
            e.preventDefault();
            formData = new FormData(this);
            formId = "#video-form";
            uploadFile(formData, formId, "video");
        }) *
        window.setInterval(function() {
            saveToLocalStorage();
            saveToLocalStorage();
        }, 30000);
});

async function createSelectElement() {
    var selectElement = $(".node-list");
    getAllNodes(selectElement);
    //globalData = $flowchart.flowchart('getData');
    // selectElement.html($("<option>Select Node</option>"));
    // operatorJson = getJsonArray(globalData);

    // $.each(operatorJson[0], function (key, value) {
    //     selectElement.append($("<option></option>")
    //         .attr("value", key)
    //         .text(value.properties.title));
    // });
}

function deleteSelectedItem($flowchart) {

    selectedOperatorId = $flowchart.flowchart("getSelectedOperatorId");
    selectedLinkId = $flowchart.flowchart("getSelectedLinkId");
    let deleteNodeType
    console.log("selectedOperatorId", selectedOperatorId)

    if (selectedOperatorId != null) {
        let nodeData = $flowchart.flowchart("getData");
        let nodeJSON = getJsonArray(nodeData);
        deleteNodeType = nodeJSON[0][selectedOperatorId]["node_type"]
        console.log("deleteNodeType", deleteNodeType)
    }

    if (selectedOperatorId != null || selectedLinkId != null) {
        let message = selectedOperatorId ? " node." : " link.";
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this " + message,
            icon: "warning",
            buttons: true,
            dangerMode: true,
            allowEnterKey: true,
            focusConfirm: true,
        }).then(async(willDelete) => {
            if (willDelete) {
                $flowchart.flowchart("deleteSelected");
                globalData = $flowchart.flowchart("getData");
                createSelectElement();
                await swal("Selected item has been deleted!", {
                    icon: "success",
                    buttons: false,
                    timer: 1500,
                });
                dataChanged = true;
                if (deleteNodeType == "code") {
                    let deleteNodeID = "n" + selectedOperatorId.split(/(\d+)/)[1]
                    console.log("deleteNodeID", deleteNodeID)
                    deleteNodeCode(deleteNodeID)
                }
            }
        });
    } else {
        swal("Error", "Please select a node first", "error");
    }
}

function uploadFile(formData, formId, type) {
    var operatorTitle = $("#" + type + "_operator_title").val();
    var operatorQuestion = $("#" + type + "_question").val();
    var operatorTextOpt = $("#" + type + "-text-opt").prop("checked");
    var operatorAnalyticsOpt = $("#" + type + "-analytics-opt").prop("checked");
    var $flowchart = $("#flowchartworkspace");

    if (operatorTitle != "" && operatorQuestion != "") {
        if (!checkOperatorName(operatorJson, operatorTitle)) {
            swal("Error", "Operator with same name exist", "error");
        } else {
            $.ajax({
                type: "POST",
                url: "process-file.php",
                data: formData,
                dataType: "json",
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    operator = $("#operator_id").val();
                    operatorJson = getJsonArray(globalData);
                    if (response.error == 0) {
                        operatorJson[0][operator]["properties"]["outputs"]["output_1"][
                            "answer"
                        ] = response.data;
                        operatorJson[0][operator]["properties"][
                            "question"
                        ] = operatorQuestion;
                        operatorJson[0][operator]["properties"]["title"] = operatorTitle;
                        if (operatorTextOpt) {
                            operatorJson[0][operator]["properties"]["type_opt"] = true;
                        } else {
                            operatorJson[0][operator]["properties"]["type_opt"] = false;
                        }
                        if (operatorAnalyticsOpt) {
                            operatorJson[0][operator]["properties"]["analytics"] = true;
                        } else {
                            operatorJson[0][operator]["properties"]["analytics"] = false;
                        }
                        swal(response.message, { icon: "success" });
                        $("#property-modal-image").modal("hide");
                        $("#property-modal-video").modal("hide");
                    } else {
                        swal(response.message, { icon: "error" });
                    }
                    $flowchart.flowchart("setData", globalData);
                },
            });
        }
    } else {
        swal("Error", "One or more fields are blank", "error");
    }
}

function deleteOption(id) {
    deletedElementId.push(id);
    $("#form-group-" + id).remove();
    x--;
}

//to delete product field
function deletePrdctField(id) {
    deletedPrdctId.push(id);
    $("#form-group-" + id).remove();
    prdct_count--;
}

function updateJson() {
    operator = $("#operator_id").val();
    console.log("OERATOR", operator)
    output = {};
    isBlank = 0;
    var linkKeys = [];
    var $flowchart = $("#flowchartworkspace");

    var data = $flowchart.flowchart("getData");
    console.log("DATA*********************", data)

    var operatorJson = getJsonArray(data);
    var operatorTitle = $("#operator_title").val();
    var contactOperatorTitle = $("#contact_operator_title").val();
    var codeOperatorTitle = $("#code_operator_title").val();
    // var gnrl_crm_prop = $('#gnrl_crm_integration').val();
    // var gnrl_crm_ckbx = $('#gnrl_crm_ckbx').val();

    if (nodeType == "contact") {
        if (!checkOperatorName(operatorJson, contactOperatorTitle)) {
            return "duplicate name";
        }
        if ($("#contact_question").val()) {
            operatorJson[0][operator]["properties"]["question"] = $(
                "#contact_question"
            ).val();
        } else {
            return false;
        }
        if (!$("#contact_operator_title").val()) {
            return false;
        }
        if ($("#formData").val()) {
            operatorJson[0][operator]["properties"]["form"] = $("#formData").val();
        }
        if ($("#integrations").val()) {
            operatorJson[0][operator]["properties"]["webhook"] = $(
                "#integrations"
            ).val();
        }
        cntctTypeOpt = ($('#contact_text_opt').prop("checked")) ? 1 : 0;
        if (cntctTypeOpt == 1) {
            operatorJson[0][operator]["properties"]["type_opt"] = true;
        } else {
            operatorJson[0][operator]["properties"]["type_opt"] = false;
        }

        if (!operatorJson[0][operator]["properties"].hasOwnProperty("followup")) {
            operatorJson[0][operator]["properties"]["followup"] = {
                nodeName: "",
                delayInMs: 0,
            };
        }

        if ($("#cntctfollowupNode").val())
            operatorJson[0][operator]["properties"]["followup"]["nodeName"] = $(
                "#cntctfollowupNode"
            ).val();
        if ($("#cntctdelayInMs").val())
            operatorJson[0][operator]["properties"]["followup"]["delayInMs"] = $(
                "#cntctdelayInMs"
            ).val();
    } else if (nodeType == "code") {
        console.log("nodeTYPE*********", nodeType)
        if (!checkOperatorName(operatorJson, codeOperatorTitle)) {
            return "duplicate name";
        }
        if (!$("#code_operator_title").val()) {
            return false;
        }
    } else {
        if (!checkOperatorName(operatorJson, operatorTitle)) {
            return "duplicate name";
        }
        var botContent = tinyMCE.get("botQuestion").getContent();
        if(botContent) {
            operatorJson[0][operator]["properties"]["question"] = botContent;
        } else {
            return false;
        }
        if (!$("#operator_title").val()) {
            return false;
        }

        if (!operatorJson[0][operator]["properties"].hasOwnProperty("followup")) {
            operatorJson[0][operator]["properties"]["followup"] = {
                nodeName: "",
                delayInMs: 0,
            };
        }

        if (!operatorJson[0][operator]["properties"].hasOwnProperty("crm")) {
            operatorJson[0][operator]["properties"]["crm"] = {
                property: "",
                checkbox: false,
            };
        }
        typeOpt = ($('#text-opt').prop("checked")) ? 1 : 0;
        if (typeOpt == 1) {
            operatorJson[0][operator]["properties"]["type_opt"] = true;
        } else {
            operatorJson[0][operator]["properties"]["type_opt"] = false;
        }
        analyticsOpt = ($('#analytics-opt').prop("checked")) ? 1 : 0;
        if (analyticsOpt == 1) {
            operatorJson[0][operator]["properties"]["analytics"] = true;
        } else {
            operatorJson[0][operator]["properties"]["analytics"] = false;
        }

        if (
            gnrlCrmCkbx == true ||
            operatorJson[0][operator]["properties"]["crm"]["checkbox"] == true
        ) {
            operatorJson[0][operator]["properties"]["crm"]["checkbox"] = true;
        } else {
            operatorJson[0][operator]["properties"]["crm"]["checkbox"] = false;
        }

        if ($("#gnrlfollowupNode").val())
            operatorJson[0][operator]["properties"]["followup"]["nodeName"] = $(
                "#gnrlfollowupNode"
            ).val();
        if ($("#gnrldelayInMs").val())
            operatorJson[0][operator]["properties"]["followup"]["delayInMs"] = $(
                "#gnrldelayInMs"
            ).val();

        if ($("#gnrl_crm_integration").val())
            operatorJson[0][operator]["properties"]["crm"]["property"] = $(
                "#gnrl_crm_integration"
            ).val();


        //******START OF ACTION FUNCTIONALITY*******//

        if (!operatorJson[0][operator]["properties"].hasOwnProperty("action")) {
            operatorJson[0][operator]["properties"]["action"] = {
                property: "",
                checkbox: false,
            };
        }

        if (
            gnrlActnCkbx == true ||
            operatorJson[0][operator]["properties"]["action"]["checkbox"] == true
        ) {
            operatorJson[0][operator]["properties"]["action"]["checkbox"] = true;
        } else {
            operatorJson[0][operator]["properties"]["action"]["checkbox"] = false;
        }

        if ($("#gnrl_action_dropdown").val())
            operatorJson[0][operator]["properties"]["action"]["property"] = $(
                "#gnrl_action_dropdown"
            ).val();

        if ($("#action_response").val())
            operatorJson[0][operator]["properties"]["action_resp"] = $(
                "#action_response"
            ).val();

        if ($("#attach_media_chkbox").prop('checked') == true) {
            if ($("#conv_fileLink_s3").val()) {
                operatorJson[0][operator]["properties"]["attachment"] = $("#conv_fileLink_s3").val();
            } else {
                operatorJson[0][operator]["properties"]["attachment"] = false
            }

        } else {
            operatorJson[0][operator]["properties"]["attachment"] = false
        }
        //******END OF ACTION FUNCTIONALITY*******//

        console.log("data", operatorJson[0][operator]["properties"], JSON.stringify(operatorJson[0][operator]["properties"]))

    }

    if (nodeType != "leaf" && nodeType != "code") {
        if (
            (nodeType == "general" || nodeType == "product" || nodeType == "root") &&
            operatorJson[0][operator]["properties"]["type_opt"] == false
        ) {
            $("input[name='mytext[]']").each(function(key, index) {
                if ($(this).val() == "") {
                    isBlank = 1;
                    return false;
                }
            });
        } else {
            isBlank = 0;
        }

        if (isBlank == 0) {
            $("input[name='mytext[]']").map(function(key, index) {
                output[$(this).attr("id")] = {
                    label: "O" + (key + 1),
                    answer: $(this).val(),

                    score: {
                        weight: $('#score-weight-' + ($(this).attr("data-text"))).find("#weight_" + ($(this).attr("data-text"))).val(),
                        label: $('#score-weight-' + ($(this).attr("data-text"))).find("#label_" + ($(this).attr("data-text"))).val(),

                    },

                };
            });

            // console.log('dadada---------', output);

            for (const obj in output) {

                if (output[obj].score.label == '' || output[obj].score.weight == '' || output[obj].score.label == undefined || output[obj].score.weight == undefined ||
                    output[obj].score.label == 'undefined' || output[obj].score.weight == 'undefined') {

                    delete output[obj].score;
                }
            }

            operatorJson[0][operator]["properties"]["outputs"] = output;

            // for products
            var addproduct = [],
                m = 1,
                obj;
            $(".prodcut-add-div").each(function() {
                if (
                    $(this).find("#prdct-title").val() ||
                    $(this).find("#prdct-img").val() ||
                    $(this).find("#prdct-price").val() ||
                    $(this).find("#cart-link").val() ||
                    $(this).find("#info-link").val() ||
                    $(this).find("#prdct-desc").val()
                ) {
                    obj = {
                        id: $(this).find(".product_add_field").attr("data-unique-id"),
                        title: $(this).find("#prdct-title").val(),
                        price: $(this).find("#prdct-price").val(),
                        image: $(this).find("#prdct-img").val(),
                        cart_link: $(this).find("#cart-link").val(),
                        info_link: $(this).find("#info-link").val(),
                        description: $(this).find("#prdct-desc").val(),
                    };
                }

                addproduct.push(obj);
            });
            operatorJson[0][operator]["properties"]["products"] = [...addproduct];
            // end of products

            keys = Object.keys(operatorJson[1]);
            $.each(deletedElementId, function(i, id) {
                keys.forEach(function(key, index) {
                    if (
                        operatorJson[1][key]["fromOperator"] == $("#operator_id").val() &&
                        operatorJson[1][key]["fromConnector"] == "output_" + id
                    ) {
                        $flowchart.flowchart("deleteLink", key);
                        linkKeys.push(key);
                    }
                });
            });

            $.each(linkKeys, function(i, id) {
                delete operatorJson[1][id];
            });

            globalData = data;
            $flowchart.flowchart("setData", data);
            return true;
        }
        //return false;
    }
    globalData = data;
    console.log("GLOBAL DATA", globalData)
    $flowchart.flowchart("setData", data);
    return true;
}

function checkOperatorName(operatorJson, title) {
    var flag = 1;
    keys = operatorJson[0];
    $.each(keys, function(key, values) {
        if (key != globalOperatorId) {
            if (operatorJson[0][key]["properties"]["title"] == title) {
                flag = 0;
            }
        }
    });
    if (flag == 0) {
        return false;
    }
    return true;
}

function checkExistingNode(operatorJson, operatorI) {
    console.log("checck existing node", operatorJson, operatorI)
    var flag = true;
    keys = operatorJson[0];
    var titles = [];
    var ids = [];
    $.each(keys, function(key, values) {
        titles.push(values["properties"]["title"]);
        ids.push(key);
    });
    while (flag == true) {
        if (
            titles.includes("Operator " + operatorI) ||
            ids.includes("operator" + operatorI)
        ) {
            operatorI++;
        } else {
            flag = false;
        }
    }
    console.log("OPERATOR I", operatorI)
    return operatorI;
}

//when opening modal for editing
function getOperatorJson($element) {
    tinymce.activeEditor.undoManager.clear()
    var $flowchart = $("#flowchartworkspace");
    var textOpt = false;
    (x = 1), (prdct_count = 1);

    globalData = $flowchart.flowchart("getData");
    operatorJson = getJsonArray(globalData);
    nodeType = operatorJson[0][$element]["node_type"];

    $(".new-fields").remove();

    $("#operator_id").val($element);
    if (operatorJson[0][$element]["properties"]["type_opt"] == true) {
        textOpt = true;
    }
    if (operatorJson[0][$element]["properties"]["analytics"] == true) {
        analyticsOPT = true;
    }
    if (nodeType != "leaf") {
        $("#output_1").val(
            operatorJson[0][$element]["properties"]["outputs"]["output_1"]["answer"]
        );
    }

    // $("#label-question").html("Type Your Question");
    $("#label-question").html("Bot Says");

    switch (operatorJson[0][$element]["node_type"]) {
        case "video":
            // $('#property-modal-video').modal('show');
            $("#property-modal-video").modal({ backdrop: "static", keyboard: false });
            var wrapper = $(".video-container");
            $("#video_question").val(
                operatorJson[0][$element]["properties"]["question"]
            );
            $("#video_operator_title").val(
                operatorJson[0][$element]["properties"]["title"]
            );
            $("#video-text-opt").prop("checked", textOpt);
            $("#video-analytics-opt").prop("checked", analyticsOPT);
            keys =
                operatorJson[0][$element]["properties"]["outputs"]["output_1"][
                    "answer"
                ];
            keys.forEach(function(key, index) {
                $(wrapper).append(
                    '<div class="form-group new-fields" id="form-group-' +
                    x +
                    '"><div class="col-md-11"><input type="hidden" name="myFiles[]" class="form-control" id="output_' +
                    x +
                    '" value="' +
                    key +
                    '"/><video width="200px"><source  src="' +
                    key +
                    '" ></video></div><div class="col-md-1"><span class="delete btn btn-danger tree_model_sustract" id="' +
                    x +
                    '" onclick="deleteOption(' +
                    x +
                    ')" style="font-size:16px; font-weight:bold;"> &nbsp;- </span></div></div>'
                );
                x++;
            });
            break;

        case "image":
            //$('#property-modal-image').modal('show');
            $("#property-modal-image").modal({ backdrop: "static", keyboard: false });
            var wrapper = $(".image-container");
            $("#image_question").val(
                operatorJson[0][$element]["properties"]["question"]
            );
            $("#image_operator_title").val(
                operatorJson[0][$element]["properties"]["title"]
            );
            $("#image-text-opt").prop("checked", textOpt);
            $("#image-analytics-opt").prop("checked", analyticsOPT);
            keys =
                operatorJson[0][$element]["properties"]["outputs"]["output_1"][
                    "answer"
                ];
            keys.forEach(function(key, index) {
                $(wrapper).append(
                    '<div class="form-group new-fields" id="form-group-' +
                    x +
                    '"><div class="col-md-11"><input type="hidden" name="myFiles[]" class="form-control" id="output_' +
                    x +
                    '" value="' +
                    key +
                    '"/><img src="' +
                    key +
                    '" width=200px" /></div><div class="col-md-1"><span class="delete btn btn-danger tree_model_sustract" id="' +
                    x +
                    '" onclick="deleteOption(' +
                    x +
                    ')" style="font-size:16px; font-weight:bold;"> &nbsp;- </span></div></div>'
                );
                x++;
            });
            break;

        case "contact":
            $("#property-modal-contact").modal({
                backdrop: "static",
                keyboard: false,
            });
            $("#contact_question").val(
                operatorJson[0][$element]["properties"]["question"]
            );
            $("#contact_operator_title").val(
                operatorJson[0][$element]["properties"]["title"]
            );
            $("#text-opt").prop("checked", textOpt);
            $("#contact-analytics-opt").prop("checked", analyticsOPT);
            $("#formData").val(operatorJson[0][$element]["properties"]["form"]);
            $("#integrations").val(
                operatorJson[0][$element]["properties"]["webhook"]
            );
            if (operatorJson[0][$element]["properties"].hasOwnProperty("followup")) {
                $("#cntctfollowupNode").val(
                    operatorJson[0][$element]["properties"]["followup"]["nodeName"]
                );
                $("#cntctdelayInMs").val(
                    operatorJson[0][$element]["properties"]["followup"]["delayInMs"]
                );
            }

            dropfunc(operatorJson[0][$element]["properties"]["webhook"]);
            globalFormData = {
                formData: $("#formData").val(),
            };
            formBuilder.actions.setData(globalFormData.formData);
            break;
        case "code":
            $("#property-modal-code").modal({ backdrop: "static", keyboard: false });
            $("#code-analytics-opt").prop("checked", analyticsOPT);
            document.getElementById("code_operator_title").value = (operatorJson[0][$element]["properties"]["title"]);
            document.getElementById("code_instructions").value = "";
            let nodeID = $("#operator_id").val();
            console.log("nodeID", nodeID)
            getNodeCode(nodeID)
                //console.log(operatorJson[0][$element]["properties"]["title"])
            break;

        default:
            //$('#property-modal').modal('show');
            $("#property-modal").modal({ backdrop: "static", keyboard: false });
           $("#botQuestion").val(tinyMCE.activeEditor.setContent(operatorJson[0][$element]["properties"]["question"]))
            $("#text-opt").prop("checked", textOpt);
            $("#analytics-opt").prop("checked", analyticsOPT)
            if (operatorJson[0][$element]["properties"]["followup"]) {
                $("#gnrlfollowupNode").val(
                    operatorJson[0][$element]["properties"]["followup"]["nodeName"]
                );
                $("#gnrldelayInMs").val(
                    operatorJson[0][$element]["properties"]["followup"]["delayInMs"]
                );
            }

            if (operatorJson[0][$element]["properties"].hasOwnProperty("crm")) {
                $("#gnrl_crm_integration").val(
                    operatorJson[0][$element]["properties"]["crm"]["property"]
                );
                if (
                    operatorJson[0][$element]["properties"]["crm"]["checkbox"] == true
                ) {
                    $("#gnrl_properties_div").removeClass("hide");
                } else {
                    $("#gnrl_properties_div").addClass("hide");
                }

                $("#gnrl_crm_ckbx").prop(
                    "checked",
                    operatorJson[0][$element]["properties"]["crm"]["checkbox"]
                );
            } else {
                operatorJson[0][$element]["properties"]["crm"] = { property: "", checkbox: false, };
                $("#gnrl_crm_ckbx").prop("checked", operatorJson[0][$element]["properties"]["crm"]["checkbox"]);
                $("#gnrl_properties_div").addClass("hide");
            }

            //******START OF ACTION FUNCTIONALITY*******//

            if (operatorJson[0][$element]["properties"].hasOwnProperty("action")) {
                console.log("property", operatorJson[0][$element]["properties"]["action"]["property"])
                if (
                    operatorJson[0][$element]["properties"]["action"]["checkbox"] == true
                ) {
                    $("#gnrl_action_dropdown").val(operatorJson[0][$element]["properties"]["action"]["property"]);
                    $("#gnrl_action_div").removeClass("hide");
                } else {
                    $("#gnrl_action_div").addClass("hide");
                }

                $("#gnrl_action_ckbx").prop(
                    "checked",
                    operatorJson[0][$element]["properties"]["action"]["checkbox"]
                );
            } else {
                operatorJson[0][$element]["properties"]["action"] = { property: "", checkbox: false, };
                $("#gnrl_action_ckbx").prop("checked", operatorJson[0][$element]["properties"]["action"]["checkbox"]);
                $("#gnrl_action_div").addClass("hide");
            }

            $("#action_response").val(operatorJson[0][$element]["properties"]["action_resp"])

            //******END OF ACTION FUNCTIONALITY*******//

            if (operatorJson[0][$element]["properties"]["products"]) {
                var wrapper = $(".prdct-container");
                // $(".prdct-container .append_prdct").innerHTML = '';
                var keyValue = operatorJson[0][$element]["properties"]["products"];
                if (keyValue != "" || !keyValue) {
                    $(".append_prdct").remove("");
                }
                keyValue.forEach(function(value, index) {
                    if (value.title || value.price || value.description || value.image || value.cart_link || value.info_link) {
                        if (index != 0) {
                            prdct_count++;
                            $(wrapper).append(
                                '<div class="form-group append_prdct" id="form-group-' +
                                prdct_count +
                                '"><div class="col-md-11 prodcut-add-div">' +
                                '<input type="text" name="prdctstitle[]" class="form-control product_add_field" placeholder="Title" id="prdct-title" value="' +
                                value.title +
                                '" data-unique-id="' +
                                new Date().getTime() +
                                '"/>' +
                                '<input type="text" name="prdctsimg[]" class="form-control product_add_field" placeholder="Image Url" id="prdct-img" value="' +
                                value.image +
                                '" data-unique-id="' +
                                new Date().getTime() +
                                '"/>' +
                                '<input type="text" name="prdctsprice[]" class="form-control product_add_field" placeholder="Price" id="prdct-price" value="' +
                                value.price +
                                '" data-unique-id="' +
                                new Date().getTime() +
                                '"/>' +
                                '<input type="text" name="prdctsdesc[]" class="form-control product_add_field" placeholder="Description" id="prdct-desc" value="' +
                                value.description +
                                '" data-unique-id="' +
                                new Date().getTime() +
                                '"/>' +
                                '<input type="text" name="prdctscart_link[]" class="form-control product_add_field" placeholder="Cart Link" id="cart-link" value="' +
                                value.cart_link +
                                '" data-unique-id="' +
                                new Date().getTime() +
                                '"/>' +
                                '<input type="text" name="prdctsinfo_link[]" class="form-control product_add_field" placeholder="Info Link" id="info-link" value="' +
                                value.info_link +
                                '" data-unique-id="' +
                                new Date().getTime() +
                                '"/>' +
                                '</div><div class="col-md-1"><span class="delete btn btn-danger tree_model_sustract" onclick="deletePrdctField(' +
                                prdct_count +
                                ')" id="' +
                                prdct_count +
                                '" style="font-size:16px; font-weight:bold;"> &nbsp;- </span></div></div>'
                            );
                        } else {
                            $("#prdct-title").val(value.title);
                            $("#prdct-img").val(value.image);
                            $("#prdct-price").val(value.price);
                            $("#prdct-desc").val(value.description);
                            $("#cart-link").val(value.cart_link);
                            $("#info-link").val(value.info_link);
                        }
                    }
                });
            }

            var wrapper = $(".general-container");
            keys = Object.keys(operatorJson[0][$element]["properties"]["outputs"]);
            keys.forEach(function(key, index) {

                if (index == 0) {
                    output = operatorJson[0][$element]["properties"]["outputs"][key]["answer"];
                    score = operatorJson[0][$element]["properties"]["outputs"][key]["score"];

                    if (score) {
                        weight = operatorJson[0][$element]["properties"]["outputs"][key]["score"]['weight'];
                        label = operatorJson[0][$element]["properties"]["outputs"][key]["score"]['label'];

                    }

                    if (output != '' && label != '' && weight != '' && label && weight) {

                        $('#score-weight-1').removeClass('hidden')
                        $('#text-field').removeClass('col-md-10')
                        $('#text-field').addClass('col-md-5')

                        $('#label_1').val(label)
                        $('#weight_1').val(weight)

                    } else {

                        $('#score-weight-1').addClass('hidden')
                        $('#text-field').removeClass('col-md-5')
                        $('#text-field').addClass('col-md-10')

                        $('#label_1').val('')
                        $('#weight_1').val('')

                    }
                }


                if (index != 0) {
                    x++;

                    output = operatorJson[0][$element]["properties"]["outputs"][key]["answer"];
                    score = operatorJson[0][$element]["properties"]["outputs"][key]["score"];

                    if (score) {
                        weight = operatorJson[0][$element]["properties"]["outputs"][key]["score"]['weight'];
                        label = operatorJson[0][$element]["properties"]["outputs"][key]["score"]['label'];
                    }

                    if (
                        operatorJson[0][$element]["node_type"] == "general" ||
                        operatorJson[0][$element]["node_type"] == "root" ||
                        operatorJson[0][$element]["node_type"] == "product"
                    ) {

                        if (output != '' && label != '' && weight != '' && output != undefined && label != undefined) {

                            $(wrapper).append(
                                '<div class="form-group new-fields" id="form-group-' +
                                x + '">' +
                                '<div class="col-md-5" id="text-field-' + x + '"><input type="text" placeholder="text" name="mytext[]" class="form-control" id="output_' +
                                x + '" value="' + output + '" data-text="' + x + '" /></div>' +


                                '<div class="" id="score-weight-' + x + '">' +

                                '<div class="col-sm-2"> <input type="text" placeholder="weight" class="form-control " id="weight_' + x + '"  value="' +
                                weight + '" /> </div>' +

                                '<div class="col-sm-3"> <input type="text" placeholder="label" class="form-control " id="label_' + x + '"  value="' +
                                label + '" /> </div>' +

                                '</div>' +


                                '<div class="col-md-1"><span class="delete btn btn-danger tree_model_sustract" id="' +
                                x +
                                '" onclick="deleteOption(' + x + ')" style="font-size:16px; font-weight:bold;"> &nbsp;- </span></div>' +
                                '<div class="col-md-1"> <button class="btn tree_model_add newStarbtn-' + x + ' " onclick="onAddScoreWeight(' + x + ')" id="' + x + '">' +
                                ' <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">' +
                                '<path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>' +
                                '</svg> </button>  </div>' +


                                '</div>'
                            );


                        } else {

                            if (label == '' || weight == '' || label == undefined || weight == undefined) {

                                var label = '';
                                var weight = '';

                                $(wrapper).append(
                                    '<div class="form-group new-fields" id="form-group-' + x + '">' +
                                    '<div class="col-md-10" id="text-field-' + x + '"><input type="text" placeholder="text" name="mytext[]" class="form-control" id="output_' +
                                    x + '" value="' + output + '" data-text="' + x + '" /></div>' +


                                    '<div  class="hidden" id="score-weight-' + x + '">' +

                                    '<div class="col-sm-2"> <input type="text" placeholder="weight" class="form-control " id="weight_' + x + '"  value="' +
                                    weight + '" /> </div>' +

                                    '<div class="col-sm-3"> <input type="text" placeholder="label" class="form-control " id="label_' + x + '"  value="' +
                                    label + '" /> </div>' +
                                    '</div>' +


                                    '<div class="col-md-1"><span class="delete btn btn-danger tree_model_sustract" id="' +
                                    x +
                                    '" onclick="deleteOption(' + x + ')" style="font-size:16px; font-weight:bold;"> &nbsp;- </span></div>' +
                                    '<div class="col-md-1"> <button class="btn tree_model_add newStarbtn-' + x + ' " onclick="onAddScoreWeight(' + x + ')" id="' + x + '">' +
                                    ' <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">' +
                                    '<path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>' +
                                    '</svg> </button>  </div>' +


                                    '</div>'
                                );


                            }
                        }

                    } else {
                        $(wrapper).append(
                            '<div class="form-group new-fields" id="form-group-' +
                            x +
                            '"><div class="col-md-10"><input type="text" placeholder="text" name="mytext[]" class="form-control" id="output_' +
                            x +
                            '" value="' +
                            output +
                            '" disabled/></div></div>'
                        );
                    }
                }
            });

            if (
                operatorJson[0][$element]["node_type"] != "general" &&
                operatorJson[0][$element]["node_type"] != "root" &&
                operatorJson[0][$element]["node_type"] != "image" &&
                operatorJson[0][$element]["node_type"] != "video" &&
                operatorJson[0][$element]["node_type"] != "product"
            ) {
                $(".followup").addClass("hidden");
                $(".option-area").addClass("hidden");
                $("input[name='mytext[]']").prop("disabled", true);
                $(".add_form_field").addClass("hidden");
            } else {
                $(".followup").removeClass("hidden");
                $(".option-area").removeClass("hidden");
                $("input[name='mytext[]']").prop("disabled", false);
                $(".add_form_field").removeClass("hidden");
            }

            //to hide/show product fields
            operatorJson[0][$element]["node_type"] != "product" ?
                $(".prdcts").addClass("hidden") :
                $(".prdcts").removeClass("hidden");

            //Hide/show integration option
            let nodeType = operatorJson[0][$element]["node_type"];
            if (
                nodeType != "general" &&
                nodeType != "product" &&
                nodeType != "root"
            ) {
                $(".CRMdiv").addClass("hide");
            } else {
                $(".CRMdiv").removeClass("hide");
            }

            if (operatorJson[0][$element]["node_type"] == "email_check") {
                $("#label-question").html("Ask For User’s Email Id");
            }
            if (operatorJson[0][$element]["node_type"] == "phone_check") {
                $("#label-question").html("Ask For User’s Phone Number");
            }
            //Hide/show media attach
            if (operatorJson[0][$element]["properties"]["attachment"] == undefined) {
                operatorJson[0][$element]["properties"]["attachment"] = false
                var previewImg = document.getElementById('previewImg');
                previewImg.src = ''
            }

            if (operatorJson[0][$element]["properties"]["attachment"]) {
                $("#attach_media").removeClass("hide");
                $('#attach_media_chkbox').prop('checked', true);
                var previewImg = document.getElementById('previewImg');
                // previewImg.src = operatorJson[0][$element]["properties"]["attachment"]
                // $('#previewImg').removeAttr('style');
                // $("#attach_media_div").addClass("hide");
                $('#conv_fileLink_s3').val(operatorJson[0][$element]["properties"]["attachment"])

                var s3_pathLink = operatorJson[0][$element]["properties"]["attachment"]
                var parts = s3_pathLink.split('.');
                var last_part = parts[parts.length - 1]
                    // var file_type = last_part.split('/')
                if (last_part == "image" || last_part == "png" || last_part == "jpg" || last_part == "jpeg" || last_part == "tiff") {
                    previewImg.src = operatorJson[0][$element]["properties"]["attachment"]
                    $('#previewImg').css({ "width": "150px", "height": "150px", "margin-left": "35%" })

                } else if (last_part == "application" || last_part == "text" || last_part == "pdf") {
                    previewImg.src = 'dist/img/document.png'
                    $('#previewImg').css({ "width": "150px", "height": "150px", "margin-left": "35%" })

                } else if (last_part == "video") {
                    previewImg.src = 'dist/img/video-button.png'
                    $('#previewImg').css({ "width": "150px", "height": "150px", "margin-left": "35%" })

                } else {
                    previewImg.src = ''
                }

            } else {
                $('#attach_media_chkbox').prop('checked', false);
                $("#attach_media").addClass("hide");
                $('#conv_fileLink_s3').val('')
                $('#uploadConversationalMedia').val('')
                $('#previewImg').removeAttr('style');
                var previewImg = document.getElementById('previewImg');
                previewImg.src = ''
            }

            if (operatorJson[0][$element]["node_type"] == "general") {
                $("#attach_media_div").removeClass("hide");
                $('#operatorIdData').val($element);
            } else {
                $("#attach_media_div").addClass("hide");
            }
            break;
    }
}

function onAddScoreWeight(id) {
    var singleField = $('#text-field-' + id).hasClass('col-md-10')
    $('#weight_' + id).val('')
    $('#label_' + id).val('')
    if (singleField == true) {
        $('#text-field-' + id).addClass('col-md-5')
        $('#text-field-' + id).removeClass('col-md-10')
        $("#score-weight-" + id).removeClass('hidden')
        $('.newStarbtn-' + id).addClass('starBtn')
    } else {
        $('#text-field-' + id).addClass('col-md-10')
        $('#text-field-' + id).removeClass('col-md-5')
        $("#score-weight-" + id).addClass('hidden')
        $('.newStarbtn-' + id).removeClass('starBtn')
    }

}

function saveToLocalStorage() {
    var $flowchart = $("#flowchartworkspace");
    globalData = $flowchart.flowchart("getData");
    globalData["settings"] = settings;
    data = JSON.stringify(globalData);
    localStorage.setItem("stgLocalFlowChart", data);
    $("#alert-message").css("display", "block");
    $("#alert-message").fadeOut("slow");
}

function LoadFromDatabase() {
    $.ajax({
        type: "POST",
        url: "/loadtreejson",
        data: {},
        success: function(data) {
            if (data.crm_data) {
                if (data.crm_data.name == "Hubspot") {
                    hubApiKey = data.crm_data.apiKey;
                    $(".CRMdiv").removeClass("hide");
                    $(".properties").append("<option>Select Property</option>");
                    let myArray = data.crm_data.properties;
                    myArray.forEach((element) => {
                        $(".properties").append(
                            "<option value=" +
                            element.name +
                            ">" +
                            element.label +
                            "</option>"
                        );
                    });
                }
            }
            var data = data.tree ? data.tree.tree : "";
            if (data != null) {
                $("#flowchart_data").val(data);
                text2Flow();
                globalData = JSON.parse(data);
                operatorI = length(globalData.operators) + 1;
                updateSettings(globalData);
            } else {
                swal("local storage empty");
            }
        },
        error: function(jqXHR, exception) {
            console.log(jqXHR, exception);
        },
    });
}

//LoadFromDatabase();
saveToLocalStorage();
async function saveToDatabase(event, treedata = false) {
    let data = await getTreeData(treedata);

    $.ajax({
        type: "POST",
        url: "https://helloyubo.com/dtree/json.php",
        data: { json: data },
        success: function(response) {
            $.ajax({
                type: "POST",
                url: "/savetreejson",
                data: { btree: response, tree: data },
                success: function(response) {
                    if (response.message == "success") {
                        // if (event.data && event.data.train == false) {
                        swal("Cool !", "Tree saved successfully!", {
                            icon: "success",
                            buttons: false,
                            timer: 1500,
                        });
                        dataChanged = false;
                    } else {
                        swal({
                            title: "The given file format is not valid.",
                            icon: "error",
                            button: "OK",
                        });
                    }
                },
            });
        },
    });
}

async function getTreeData(treedata) {
    var $flowchart = $("#flowchartworkspace");
    globalData = $flowchart.flowchart("getData");
    if (treedata) {
        globalData = treedata;
    }
    globalData["settings"] = settings;
    data = JSON.stringify(globalData, null, 2);
    localStorage.setItem("stgLocalFlowChart", data);
    return data;
}

async function traintree() {
    let data = await getTreeData((treedata = false));
    let dataLen = JSON.parse(data).operators ?
        Object.keys(JSON.parse(data).operators).length :
        0;

    if (dataLen <= 0) {
        swal({
            title: "There is no data to train, please create some nodes to train.",
            icon: "error",
            button: "OK",
        });
    } else {
        let wrapper = document.createElement("div");
        wrapper.class = "save_loading";
        wrapper.innerHTML = '<img src="/img/loader.gif">';
        await saveToDatabase(true); //call the function to save data in db, then in below code it will hit the route for tree training
        $.ajax({
            type: "POST",
            url: "/traintree",
            data: {},
            beforeSend: function() {
                swal({
                    title: "Training in progress...",
                    content: wrapper,
                    button: false,
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            },
            success: function(response) {
                console.log("RESPONSE", response)
                if (response.message == "success") {
                    swal("Cool !", "Training done successfully!", {
                        icon: "success",
                        buttons: false,
                        timer: 1500,
                    });
                    dataChanged = false;
                } else {
                    swal({
                        title: "The given file format is not valid.",
                        icon: "error",
                        button: "OK",
                    });
                }
            },
        });
    }
}

function length(obj) {
    return Object.keys(obj).length + 1;
}

function loadFromLocalStorage() {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this tree node",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willUpdate) => {
        if (willUpdate) {
            if (typeof localStorage !== "object") {
                swal("local storage not available");
                return;
            }

            data = localStorage.getItem("stgLocalFlowChart");
            if (data != null) {
                $("#flowchart_data").val(data);
                text2Flow();
                globalData = JSON.parse(data);
                // operatorI = length(globalData.operators);
                operatorI = length(globalData.operators) + 1;
                updateSettings(globalData);
                swal("Tree data is updated");
            } else {
                swal("local storage empty");
            }
        }
    });
}

function updateSettings(data) {
    $(".flowchart-links-layer").css("height", data.settings.canvasHeight);
    $(".flowchart-links-layer").css("width", data.settings.canvasWidth);
    $(".flowchart-example-container").css("height", data.settings.canvasHeight);
    $(".flowchart-example-container").css("width", data.settings.canvasWidth);

    settings["canvasHeight"] = data.settings.canvasHeight;
    settings["canvasWidth"] = data.settings.canvasWidth;
    settings["maxFields"] = data.settings.maxFields;

    createSelectElement();
}

function updateProperty() {
    canvasHeight = $("#canvas_height").val();
    canvasWidth = $("#canvas_width").val();
    maxFields = $("#max_field").val();

    $(".flowchart-links-layer").css("height", canvasHeight);
    $(".flowchart-links-layer").css("width", canvasWidth);

    $(".flowchart-example-container").css("height", canvasHeight);
    $(".flowchart-example-container").css("width", canvasWidth);

    settings["canvasHeight"] = canvasHeight;
    settings["canvasWidth"] = canvasWidth;
    settings["maxFields"] = maxFields;

    globalData["settings"] = settings;

    $("#setting-modal").modal("hide");
}

function flow2Text() {
    var $flowchart = $("#flowchartworkspace");
    globalData = $flowchart.flowchart("getData");
    $("#flowchart_data").val(JSON.stringify(globalData, null, 2));
}

function text2Flow() {
    var $flowchart = $("#flowchartworkspace");
    globalData = JSON.parse($("#flowchart_data").val());
    $flowchart.flowchart("setData", globalData);
}

function getJsonArray(data) {
    var operatorJson = [];
    var keys = Object.keys(data);

    keys.forEach(function(key) {
        operatorJson.push(data[key]);
    });

    return operatorJson;
}

function getSetting() {
    $("#setting-modal").modal("show");
    canvasHeight = $(".flowchart-links-layer").height();
    canvasWidth = $(".flowchart-links-layer").width();
    $("#canvas_height").val(canvasHeight);
    $("#canvas_width").val(canvasWidth);
    $("#max_field").val(maxFields);
}

function resetTopLeft() {
    if (pTop >= 400 && pLeft >= 400) {
        position = position + 20;
        pTop = position;
        pLeft = position;
    }
}

function nodeGeneral(operatorNumber) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;
    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: "general",
        properties: {
            title: "Operator " + operatorNumber,
            question: "",
            followup: {
                nodeName: "",
                delayInMs: 0,
            },
            crm: {
                property: "",
                checkbox: false,
            },
            action: {
                checkbox: false,
                property: ""
            },
            action_resp: "",
            inputs: {
                input_1: {
                    label: "In1",
                },
            },
            outputs: {
                output_1: {
                    label: "O1",
                    answer: "",
                },
            },
        },
    };

    resetTopLeft();
    return operatorData;
}

function nodeProduct(operatorNumber) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;
    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: "product",
        properties: {
            title: "Operator " + operatorNumber,
            question: "",
            followup: {
                nodeName: "",
                delayInMs: 0,
            },
            products: [],
            crm: {
                property: "",
                checkbox: false,
            },
            action: {
                checkbox: false,
                property: ""
            },
            action_resp: "",
            inputs: {
                input_1: {
                    label: "In1",
                },
            },
            outputs: {
                output_1: {
                    label: "O1",
                    answer: "",
                },
            },
        },
    };

    resetTopLeft();
    return operatorData;
}

function nodeContact(operatorNumber) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;
    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: "contact",
        properties: {
            title: "Operator " + operatorNumber,
            question: "",
            options: [],
            form: {},
            webhook: "",
            followup: {
                nodeName: "",
                delayInMs: 0,
            },
            inputs: {
                input_1: {
                    label: "In1",
                },
            },
            outputs: {
                output_1: {
                    label: "O1",
                    answer: "",
                },
            },
        },
    };

    resetTopLeft();
    return operatorData;
}

function nodeEmailCheck(operatorNumber) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;
    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: "email_check",
        properties: {
            title: "Operator " + operatorNumber,
            question: "",
            followup: {
                nodeName: "",
                delayInMs: 0,
            },
            type_opt: true,
            inputs: {
                input_1: {
                    label: "In1",
                },
            },
            outputs: {
                output_1: {
                    label: "O1",
                    answer: true,
                },
                output_2: {
                    label: "O2",
                    answer: false,
                },
            },
        },
    };

    resetTopLeft();
    return operatorData;
}

function nodePhoneCheck(operatorNumber) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;
    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: "phone_check",
        properties: {
            title: "Operator " + operatorNumber,
            question: "",
            followup: {
                nodeName: "",
                delayInMs: 0,
            },
            type_opt: true,
            inputs: {
                input_1: {
                    label: "In1",
                },
            },
            outputs: {
                output_1: {
                    label: "O1",
                    answer: true,
                },
                output_2: {
                    label: "O2",
                    answer: false,
                },
            },
        },
    };

    resetTopLeft();
    return operatorData;
}

function nodeRoot(operatorNumber) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;

    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: "root",
        properties: {
            title: "Operator " + operatorNumber,
            question: "",
            followup: {
                nodeName: "",
                delayInMs: 0,
            },
            crm: {
                property: "",
                checkbox: false,
            },
            action: {
                checkbox: false,
                property: ""
            },
            action_resp: "",
            type_opt: false,
            outputs: {
                output_1: {
                    label: "O1",
                    answer: "",
                },
            },
        },
    };

    resetTopLeft();
    return operatorData;
}

function nodeBookAppointment(operatorNumber) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;

    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: "calendar",
        properties: {
            title: "Operator " + operatorNumber,
            question: "",
            followup: {
                nodeName: "",
                delayInMs: 0,
            },
            type_opt: true,
            inputs: {
                input_1: {
                    label: "In1",
                },
            },
            outputs: {
                output_1: {
                    label: "O1",
                    answer: "Next Node",
                },
            },
        },
    };

    resetTopLeft();
    return operatorData;
}

function nodeLeaf(operatorNumber) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;
    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: "leaf",
        properties: {
            title: "Operator " + operatorNumber,
            question: "",
            type_opt: true,
            inputs: {
                input_1: {
                    label: "In1",
                },
            },
            outputs: {},
        },
    };

    resetTopLeft();
    return operatorData;
}

function nodeImage(operatorNumber) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;
    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: "image",
        properties: {
            title: "Operator " + operatorNumber,
            question: "",
            crm: {
                property: "",
                checkbox: false,
            },
            action: {
                checkbox: false,
                property: ""
            },
            action_resp: "",
            inputs: {
                input_1: {
                    label: "In1",
                },
            },
            outputs: {
                output_1: {
                    label: "O1",
                    answer: "Next Link",
                },
                output_2: {
                    label: "O2",
                    answer: "More Images",
                },
            },
        },
    };

    resetTopLeft();
    return operatorData;
}

function nodeVideo(operatorNumber) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;
    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: "video",
        properties: {
            title: "Operator " + operatorNumber,
            question: "",
            crm: {
                property: "",
                checkbox: false,
            },
            action: {
                checkbox: false,
                property: ""
            },
            action_resp: "",
            inputs: {
                input_1: {
                    label: "In1",
                },
            },
            outputs: {
                output_1: {
                    label: "O1",
                    answer: "Next Link",
                },
                output_2: {
                    label: "O2",
                    answer: "More Videos",
                },
            },
        },
    };

    resetTopLeft();
    return operatorData;
}

/*************Code Node ***************************************************/
function nodeCode(operatorNumber) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;

    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: "code",
        properties: {
            title: "Operator " + operatorNumber,
            question: "",
            inputs: {
                input_1: {
                    label: "In1",
                },
            },
            outputs: {
                output_1: {
                    label: "O1",
                    answer: "",
                },
            },
        },
        //code: readCodeFromDB(node_id)
    };
    resetTopLeft();
    return operatorData;
}
/**********************************************************************/

function createCopyNode(operatorData) {
    pTop = pTop + 20;
    pLeft = pLeft + 20;
    operatorI = checkExistingNode(getJsonArray(globalData), operatorI);
    var operatorData = {
        top: pTop,
        left: pLeft,
        node_type: operatorData["node_type"],
        properties: {
            title: "Operator " + operatorI,
            question: operatorData["properties"]["question"],
            type_opt: operatorData["properties"]["type_opt"],
            inputs: {
                input_1: {
                    label: "In1",
                },
            },
            outputs: operatorData["properties"]["outputs"],
        },
    };

    resetTopLeft();
    return operatorData;
}

var defaultFlowchartData = {
    operators: {
        operator1: {
            top: 20,
            left: 260,
            node_type: "general",
            properties: {
                title: "Operator 1",
                question: "What is your question",
                type_opt: "",
                inputs: {},
                outputs: {
                    output_1: {
                        label: "O1",
                        answer: "",
                    },
                },
            },
        },
        operator2: {
            top: 120,
            left: 340,
            node_type: "general",
            properties: {
                title: "Operator 2",
                question: "What is your question two",
                type_opt: "",
                inputs: {
                    input_1: {
                        label: "In1",
                    },
                },
                outputs: {
                    output_1: {
                        label: "O1",
                        answer: "No",
                    },
                },
            },
        },
    },
    links: {
        0: {
            fromOperator: "operator1",
            fromConnector: "output_1",
            toOperator: "operator2",
            toConnector: "input_1",
        },
    },
    settings: {
        canvas_height: "10000",
        canvas_width: "10000",
    },
};

// $("#integrations").click(function () {
function dropfunc(option) {
    var selectElement = $("#integrations");
    document.getElementById("integrations").innerHTML = "";
    selectElement.html(
        $("<option selected disabled>Select Integration</option>")
    );
    $.ajax({
        type: "POST",
        url: "/webhook",
        data: {},
        timeout: 10000,
        success: function(resp) {
            resp.forEach(function(item, i) {
                if (!option || option == "" || item.webhookName != option) {
                    selectElement.append(
                        $("<option></option>")
                        .attr("value", item.webhookName)
                        .text(item.webhookName)
                    );
                } else {
                    selectElement.append(
                        $("<option selected></option>")
                        .attr("value", item.webhookName)
                        .text(item.webhookName)
                    );
                }
            });
        },
        error: function(jqXHR, textStatus, err) {
            alert("Something went wrong, please refresh the page");
        },
    });
}
// });

async function getNodeCode(nodeID) {
    codeEditor.setValue("Loading");
    //let node_id = (nodeID.split("r"))[2]
    let node_id = nodeID.split(/(\d+)/)[1]
    console.log("In getcode function", node_id)
    $.ajax({
        type: "POST",
        url: "/getCodes",
        data: { nodeID: "n" + node_id },
        success: function(data) {
            console.log("DATA 2982", data)
            if (data) {
                if (data.statusCode == 200) {
                    codeEditor.setValue(data.data.code);
                    document.getElementById("code_operator_title").value = data.data.nodeTitle
                    document.getElementById("code_instructions").value = "";
                } else if (data.statusCode == 404) {
                    codeEditor.setValue("/*Write your code here*/");
                    // swal({
                    //     title: data.message,
                    //     icon: "error",
                    //     type: 'info',
                    //     confirmButtonText: 'OK'
                    // }).then(() => {
                    //     location.reload();
                    // });
                }
            }
        },
        error: function(jqXHR, textStatus, err) {
            if (jqXHR.responseJSON.statusCode == 403) {
                window.location.replace("https://admin.helloyubo.com/login");
            } else {
                alert("Something went wrong, please try again")
            }
        }
    });
}

async function generateCode() {
    let wrapper = document.createElement("div");
    wrapper.class = "save_loading";
    wrapper.innerHTML = '<img src="/img/loader.gif">';
    //codeEditor.setValue("Loading");
    let code_instructions=document.getElementById("code_instructions").value
    $.ajax({
        type: "POST",
        url: "/generateCode",
        data: { code_instructions:code_instructions},
        beforeSend: function() {
            swal({
                title: "Generating code...",
                content: wrapper,
                button: false,
                closeOnClickOutside: false,
                closeOnEsc: false,
            });
        },
        success: function(data) {
            if (data) {
                if (data.statusCode == 200) {
                    if(data.data.status=="success"){
                        let code="/*\n"+data.data.code+"\n*/"
                        code=code.replace("```","*/\n")
                        code=code.replace("```","\n/*\n")
                        codeEditor.setValue(code);
                        swal("Code has been generated", {
                            icon: "success",
                            buttons: false,
                            timer: 1500,
                        });
                    } else {
                        //codeEditor.setValue("/*Write your code here*/");
                        swal("Error", data.data.message, "error");
                    }
                    
                } else {
                    //codeEditor.setValue("/*Write your code here*/");
                    swal("Error", data.message, "error");
                }
            }
        },
        error: function(jqXHR, textStatus, err) {
            if (jqXHR.responseJSON.statusCode == 403) {
                window.location.replace("https://admin.helloyubo.com/login");
            } else {
                alert("Something went wrong, please try again")
            }
        }
    });
}

async function getAllNodes(selectElement) {
    selectElement.html($("<option>Select Node</option>"));
    operatorJson = getJsonArray(globalData);

    $.each(operatorJson[0], function(key, value) {
        selectElement.append(
            $("<option></option>")
            .attr("value", value.properties.title)
            .attr("key", key)
            .text(value.properties.title)
        );
    });
}

//Run function in code node (execute wrapper function)
async function readCodeFromDB(nodeID) {
    console.log("In read code function")
    fetch('/getCodes', {
        method: 'POST',
        data: { nodeID: nodeID },
    }).then((response) => {
        return response.json();
    }).then(async function(data) {
        console.log("DATA", data.data.code)
        callWrapperFunction(data.data.code)
    })
}


async function callWrapperFunction(data1) {
    let data = {
        data: data1
    }
    console.log("data1", data1)
    $.ajax({
        type: "POST",
        url: "/executeCode",
        data: data,
        success: function(data) {
            console.log("DATA", data)
        }
    })
}

async function deleteNodeCode(nodeID) {
    console.log("I delete code function", nodeID)
    $.ajax({
        type: "POST",
        url: "/deleteCodeNode",
        data: { nodeID: nodeID },
        success: function(data) {
            if (data) {
                if (data.statusCode == 200) {
                    console.log("code node deleted successfully")
                } else if (data.statusCode == 400) {
                    console.log("Error while deleting code node")
                }
            }
        },
        error: function(jqXHR, textStatus, err) {
            if (jqXHR.responseJSON.statusCode == 403) {
                window.location.replace("https://admin.helloyubo.com/login");
            } else {
                alert("Something went wrong, please try again")
            }
        }
    });
}

if (false) console.log("remove lint unused warning", defaultFlowchartData);
