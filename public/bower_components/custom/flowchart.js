$(document).ready(function() {

    var $flowchart = $('#flowchartworkspace');
    var $container = $flowchart.parent();
    var max_fields = 5;
    var x = 1;
    var localStorage=window.localStorage;
    var tree=   {"json": {
        "operators": {
            "operator1": {
                "top": 20,
                "left": 260,
                "properties": {
                    "title": "Operator 1",
                    "question": "What is your fjsdshdsj",
                    "type_opt": "",
                    "inputs": {},
                    "outputs": {
                        "output_1": {
                            "label": "O1",
                            "answer": ""
                        }
                    }
                }
            },
            "operator2": {
                "top": 120,
                "left": 340,
                "properties": {
                    "title": "Operator 2",
                    "question": "What is your question three",
                    "type_opt": "",
                    "inputs": {
                        "input_1": {
                            "label": "In1",
                            "answer": ""
                        }
                    },
                    "outputs": {
                        "output_1": {
                            "label": "O1",
                            "answer": "Yes"
                        }
                    }
                }
            }
        },
        "links": {
            "0": {
                "fromOperator": "operator1",
                "fromConnector": "output_1",
                "toOperator": "operator2",
                "toConnector": "input_1"
            }
        },
        "operatorTypes": {}
    }
};

    $flowchart.flowchart({
        verticalConnection: true,
        data: defaultFlowchartData,
        defaultSelectedLinkColor: '#000055',
        linkWidth: 5,
        grid: 10,
        multipleLinksOnInput: true,
        multipleLinksOnOutput: false
    });

    function getOperatorData($element) {
        var nbInputs = parseInt($element.data('nb-inputs'), 10);
        var nbOutputs = parseInt($element.data('nb-outputs'), 10);
        var data = {
            properties: {
                title: $element.text(),
                question: 'My first question',
                inputs: {},
                outputs: {}
            }
        };

        var i = 0;
        for (i = 0; i < nbInputs; i++) {
            data.properties.inputs['input_' + i] = {
                label: 'Input ' + (i + 1)
            };
        }
        for (i = 0; i < nbOutputs; i++) {
            data.properties.outputs['output_' + i] = {
                label: 'Output ' + (i + 1)
            };
        }

        return data;
    }
    //LoadFromLocalStorage();
    Flow2Text();

    var $operatorProperties = $('#operator_properties');
    $operatorProperties.hide();
    var $linkProperties = $('#link_properties');
    $linkProperties.hide();
    var $operatorTitle = $('#operator_title');
    var $linkColor = $('#link_color');

    $flowchart.flowchart({
        onOperatorSelect: function(operatorId) {
            //console.log(operatorId);
            $operatorProperties.show();
            $('#property-modal').modal('show');
            $operatorTitle.val($flowchart.flowchart('getOperatorTitle', operatorId));
            getOperatorJson(operatorId);
            return true;
        },
        onOperatorUnselect: function() {
            $operatorProperties.hide();
            return true;
        },
        onLinkSelect: function(linkId) {
            $linkProperties.show();
            $linkColor.val($flowchart.flowchart('getLinkMainColor', linkId));
            return true;
        },
        onLinkUnselect: function() {
            $linkProperties.hide();
            return true;
        }
    });

    $operatorTitle.keyup(function() {
        var selectedOperatorId = $flowchart.flowchart('getSelectedOperatorId');
        if (selectedOperatorId != null) {
            $flowchart.flowchart('setOperatorTitle', selectedOperatorId, $operatorTitle.val());
        }
    });

    $linkColor.change(function() {
        var selectedLinkId = $flowchart.flowchart('getSelectedLinkId');
        if (selectedLinkId != null) {
            $flowchart.flowchart('setLinkMainColor', selectedLinkId, $linkColor.val());
        }
    });

    $('.delete_selected_button').click(function() {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this tree node",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $flowchart.flowchart('deleteSelected');
                swal("Selected item has been deleted!", {
                    icon: "success",
                });
            }
        });
    });

    $('#set_data').click(Text2Flow);

    $('#get_data').click(function () {
        var data = $flowchart.flowchart('getData');
        $.ajax({
          type: "POST",
          url: 'https://helloyubo.com/dtree/json.php',
          data: JSON.stringify(data),
          success: function(response){
            //console.log('response')
            //console.log(JSON.parse(response))
            $('#flowchart_data').val(JSON.stringify(JSON.parse(response), null, 2));
            $('#json-modal').modal('show');
          }
        });
    });


    $('#save_data').click(function () {
        var data = $flowchart.flowchart('getData');
        $.ajax({
          type: "POST",
          url: 'https://helloyubo.com/dtree/json.php',
          data: JSON.stringify(data),
          success: function(response){
             //swal("Cool !", "Tree structure is saved ", "success");
            $.ajax({
              type: "POST",
              url: '/savetreejson',
              data: {btree:JSON.parse(response),tree:data},
              success: function(response){
                //console.log("hariiii");
                swal("Cool !", "Tree structure is saved in database", "success");
                
              }
            });
          }
        });
    });


    function LoadFromDatabase() {
        $.ajax({
            type: "POST",
            url: '/loadtreejson',
            success: function(response){
                //console.log("response", response.operators);
                if (typeof response !== 'object') {
                    alert('Tree not available in db');
                    return;
                }
                if (response != null) {
                    tree=response;
                    defaultFlowchartData=response;
                    Text2Flow();
                    operatorI = length(response.operators);
                }
                else{
                    alert('Database storage empty');
                }
            }
        });
    }
    LoadFromDatabase();








    var operatorI = 3;
    $('.create_operator').click(function() {

        var operatorId = 'operator' + operatorI;
        var operatorData = {
            top: ($flowchart.height() / 2) - 30,
            left: ($flowchart.width() / 2) - 100 + (operatorI * 10),
            properties: {
                title: 'Operator ' + (operatorI),
                question: '',
                inputs: {
                    input_1: {
                        label: 'In1',
                    }
                },
                outputs: {
                    output_1: {
                        label: 'O1',
                    }
                }
            }
        };

        operatorI++;

        $flowchart.flowchart('createOperator', operatorId, operatorData);

    });


    var $draggableOperators = $('.draggable_operator');
    $draggableOperators.draggable({
        cursor: "move",
        opacity: 0.7,

        // helper: 'clone',
        appendTo: 'body',
        zIndex: 1000,

        helper: function(e) {
            var $this = $(this);
            var data = getOperatorData($this);
            return $flowchart.flowchart('getOperatorElement', data);
        },
        stop: function(e, ui) {
            var $this = $(this);
            var elOffset = ui.offset;
            var containerOffset = $container.offset();
            if (elOffset.left > containerOffset.left &&
                elOffset.top > containerOffset.top &&
                elOffset.left < containerOffset.left + $container.width() &&
                elOffset.top < containerOffset.top + $container.height()) {

                var flowchartOffset = $flowchart.offset();

                var relativeLeft = elOffset.left - flowchartOffset.left;
                var relativeTop = elOffset.top - flowchartOffset.top;

                var positionRatio = $flowchart.flowchart('getPositionRatio');
                relativeLeft /= positionRatio;
                relativeTop /= positionRatio;

                var data = getOperatorData($this);
                data.left = relativeLeft;
                data.top = relativeTop;

                $flowchart.flowchart('addOperator', data);
            }
        }
    });

    $('#text-opt').click(function () {
        $('#answer-section').toggle(!this.checked);
        updateJson();
    });

    $('#save').click(function () {
        updateJson();
        swal("Tree structure has been updated");
    });

    function updateJson() {
        operator = $('#operator_id').val();
        output   = {};

        var data = JSON.parse(tree);
        var operatorJson = getJsonArray(data);
        operatorJson[0][operator]['properties']['question'] = $('#question').val();

        if ($("#text-opt").attr("checked")) {
            operatorJson[0][operator]['properties']['type_opt'] = true;
        }

        $("input[name='mytext[]']").map(function(key, index){
            id = $(this).attr('id');
            output['output_'+(key+1)] = {'label':'O'+(key+1),'answer': $(this).val()} ;
        });

        operatorJson[0][operator]['properties']['outputs'] = output;
        $flowchart.flowchart('setData', data);
    }

    function getOperatorJson($element) {

        Flow2Text();
        var wrapper = $(".container1");
        var add_button = $(".add_form_field");

        var data     = JSON.parse(tree);
        operatorJson = getJsonArray(data);
        $('#question').val(operatorJson[0][$element]['properties']['question']);
        $('#operator_id').val($element);

        keys = Object.keys(operatorJson[0][$element]['properties']['outputs']);
        $('.new-fields').remove();
        x = 1;
        $('#output_1').val(operatorJson[0][$element]['properties']['outputs']['output_1']['answer']);
        keys.forEach(function (key, index) {
            if(index != 0) {
                x++;
                output = operatorJson[0][$element]['properties']['outputs'][key]['answer'];
                $(wrapper).append('<div class="form-group new-fields" id="form-group-'+x+'"><div class="col-md-10"><input type="text" name="mytext[]" class="form-control" id="output_'+x+'" value="'+output+'"/></div><div class="col-md-2"><span class="delete btn btn-danger" id="'+x+'" style="font-size:16px; font-weight:bold;"> &nbsp;- </span></div></div>');
            }
        });
    }

    function getJsonArray(data) {
        var operatorJson = [];
        var keys = Object.keys(data);

        keys.forEach(function(key){
            operatorJson.push(data[key]);
        });

        return operatorJson;
    }

    function length(obj) {
        return Object.keys(obj).length+1;
    }

    function Flow2Text() {
        var data = $flowchart.flowchart('getData');
        tree=JSON.stringify(data, null, 2);
    }

    function Text2Flow() {
        //var data = JSON.parse(tree);
        $flowchart.flowchart('setData', tree);
    }

    function SaveToLocalStorage() {
        if (typeof localStorage !== 'object') {
            swal("There is no local storage");
            return;
        }
        Flow2Text();
        localStorage.setItem("stgLocalFlowChart", tree);
        swal("Cool !", "Tree structure is saved", "success");
    }

    $('#save_local').click(SaveToLocalStorage);

    function LoadFromLocalStorage() {

        if (typeof localStorage !== 'object') {
            alert('local storage not available');
            return;
        }
        var s = localStorage.getItem("stgLocalFlowChart");
        //console.log("ssssss",s);
        if (s != null) {
            tree=JSON.parse(s);
            Text2Flow();
            var data = JSON.parse(s);
            operatorI = length(data.operators);
        }
        else {
            alert('local storage empty');
        }
    }
    $('#load_local').click(LoadFromLocalStorage);

    var wrapper = $(".container1");
    var add_button = $(".add_form_field");

    $(add_button).click(function(e) {
        e.preventDefault();
        if (x < max_fields) {
            x++;
            $(wrapper).append('<div class="form-group new-fields" id="form-group-'+x+'"><div class="col-md-10"><input type="text" name="mytext[]" class="form-control" id="output_'+x+'"/></div><div class="col-md-2"><span class="delete btn btn-danger" id="'+x+'" style="font-size:16px; font-weight:bold;"> &nbsp;- </span></div></div>'); //add input box
        } else {
            alert('You Reached the limits')
        }
    });

    $(wrapper).on("click", ".delete", function(e) {
        e.preventDefault();
        var id = $(this).attr('id');
        $('#form-group-'+id).remove();
        x--;
    })

});

    var defaultFlowchartData = {
        operators: {
            operator1: {
                top: 20,
                left: 260,
                properties: {
                    title: 'Operator 1',
                    question: 'What is your question',
                    type_opt: '',
                    inputs: {},
                    outputs: {
                        output_1: {
                            label: 'O1',
                            answer: '',
                        }
                    }
                }
            },
            operator2: {
                top: 120,
                left: 340,
                properties: {
                    title: 'Operator 2',
                    question: 'What is your question two',
                    type_opt: '',
                    inputs: {
                        input_1: {
                            label: 'In1',
                            answer: '',
                        },
                    },
                    outputs: {
                        output_1: {
                            label: 'O1',
                            answer: 'No',
                        }
                    }
                }
            },
        },
        links: {
            0: {
                fromOperator: 'operator1',
                fromConnector: 'output_1',
                toOperator: 'operator2',
                toConnector: 'input_1',
            }
        }
    };
if (false) console.log('remove lint unused warning', defaultFlowchartData);
