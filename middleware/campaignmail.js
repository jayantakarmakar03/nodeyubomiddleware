const nodemailer = require('nodemailer');

 //sending mail using nodemailer. 
module.exports=function(host,port,user,pass,mailId, html,subject,from){
  let transporter = nodemailer.createTransport({
  host:host,
  port:port,
  auth: {
    user:user,
    pass:pass
   }
  });
 

  let mailOptions = {
    from:from,
    to:mailId,
    subject:subject,
    html:html,
  }
  transporter.sendMail(mailOptions, async function(error, info){
    if(error){
      console.log("error",error);
    }
  });
}