
/**for user creation */
$('.createUser').click(function () {
    var data = { 
        name: $('#userName').val(), 
        phone: $('#userPhone').val(), 
        email: $('#userEmail').val()
    }
        let email = $('#userEmail').val();
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if($('#userPhone').val() < 0){
            swal({
                title: "Phone number can not be negative",
                icon: "warning",
                timer: 3500
            });
        } else if(!re.test(String(email).toLowerCase())){
            swal({
                title: "Please provide valid email id",
                icon: "warning",
                timer: 3500
            }); 
        }else{
            $.ajax({
                data: data,
                async: false,
                cache: false,
                method: 'POST',
                url: '/add-user',
                success: function (data) {
                if (data.statusCode == 200) {
                     swal({
                        title: data.message,
                        icon: "success",
                        timer: 3500
                    });
                    location.reload();
                } else if(data.statusCode == 203) {
                     swal({
                        title: data.message,
                        icon: "error",
                        timer: 3500
                    });
                }
                }
            });
        }
})
function getUserList(){
           let clientId = document.getElementById("clientId").value
           let data = {
               clientId  : clientId,
           }
           console.log('data--=-=-=',data)
            $.ajax({
                data: data,
                async: false,
                cache: false,
                method: 'POST',
                url: '/getSubscriberList',
                success:  function (data) {
                    if (data.statusCode == 200) {
                        $('.ExcelData').html('');
                        for (const iterator of data.data) {
                            let html = 
                            `<tr class="tr2" 
                                 id="tr2" 
                                 userId = ${iterator._id}
                                 data-name=${iterator.name}
                                 data-email=${iterator.email}
                                 data-phone=${iterator.phone}
                             >
                                <td width="1">
                                   <input type="checkbox" 
                                          class="chkbx" 
                                          name="" 
                                          value=${iterator._id} />
                                </td>
                                <td data-label="Name">${iterator.name}</td>
                                <td data-label="Email">${iterator.email}</td>
                                <td data-label="Phone">${iterator.phone}</td>
                                <td data-label="Action">
                                <i data-target="#edit_agents" 
                                    id=${iterator._id}  
                                    data-toggle="modal" 
                                    class="fa fa-pencil editUser" 
                                    onclick="onEditBtn(id)" 
                                    data-backdrop="static" 
                                    data-keyboard="false" 
                                    title="Edit">
                                </i>
                                <i class="fa fa-trash deletUser" 
                                    id=${iterator._id} 
                                    onclick="onDeleteAgent(id)"
                                    title="Delete" 
                                    delete-id=${iterator._id}>
                                </i>
                            </tr>`
                            $('.ExcelData').append(html);
                            }
            
                    } else if(data.statusCode == 203) {
                        alert(data.message)
                    }
                }, error: function (jqXHR, textStatus, err) {  
                  if(jqXHR.responseJSON.statusCode == 403) {
                  window.location.replace("https://admin.helloyubo.com/login");
                  } else {
                  alert("Something went wrong, please try again")
                  }
              }  
            });
}
getUserList();
//**get a particular user data to render on edit model */
function onEditBtn(id){
    editSelectedUser = id;
    var data = {
      "userId": editSelectedUser,
    }
    $.ajax({
      data: data,
      async: false,
      cache: false,
      method: 'GET',
      url: '/viewUserById',
      success:  function (data) {
        if (data.statusCode == 200) {
            let agentData = data.data;
          for (const key in agentData) {
            $(`#${key}`).val(agentData[key])
          }
        } else {
             swal({
                title: data.message,
                icon: "error",
                timer: 3500
                });
          alert(data.message)
        }
      }
    });
}
function discard(e) {
    var r = confirm("Are you sure you want to discard changes");
    if (r === true) {
      $('.closeModal').attr('data-dismiss', 'modal');
    } else {
      $(".closeModal").removeAttr('data-dismiss');
    }
}
function addUserBtn() {
    // clear all the previous data on add user btn click 
    $('#userName').val('');
    $('#userPhone').val('');
    $('#userEmail').val('');
}
//********** on select excel file get the array  of document *******//
var ExcelToJSON = function() {
    this.parseExcel = function(file) {
    var reader = new FileReader();
    reader.onload = function(e) {
        var data = e.target.result;
        var workbook = XLSX.read(data, {
        type: 'binary'
        });
        workbook.SheetNames.forEach(function(sheetName) {
            // Here is your object
            var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
            var json_object = JSON.stringify(XL_row_object);
            console.log(JSON.parse(json_object));
            jQuery( '#xlx_json' ).val( json_object );
            let agentdata = JSON.parse(json_object)
            let clientId = document.getElementById("clientId").value;
            agentdata.forEach(ele=>{
                ele.clientId = clientId ;
                if(ele.Name){
                    ele.name = ele.Name; ele.Name ='';}
                if(ele.Email){
                ele.email = ele.Email;ele.Email ='';}
                if(ele.Phone){
                ele.phone = ele.Phone;ele.Phone ='';}
            });
            let allKeysOfExcellSheet = Object.keys(agentdata[0]);
            if(allKeysOfExcellSheet.includes('name') == true && allKeysOfExcellSheet.includes('email') == true && allKeysOfExcellSheet.includes('phone')== true){
                addMultipleUser(agentdata);
            }else{
                swal({
                    title: "Looks like the file format you have uploaded is not valid.",
                    icon: "",
                    timer: 4500
                });
            }
        
        })
    };
    reader.onerror = function(ex) {
        console.log(ex);
    };
    reader.readAsBinaryString(file);
    };
};
function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    var xl2json = new ExcelToJSON();
    xl2json.parseExcel(files[0]);
}
document.getElementById('loadSlotJson').addEventListener('change', handleFileSelect, false);
function addMultipleUser(data){
    $.ajax({
        data: {userArray : data },
        async: false,
        cache: false,
        method: 'POST',
        url: '/addMultipleUser',
        success:  function (res) {
          if (res.statusCode == 200) {
            swal({
                title: res.message,
                icon: "success",
                timer: 3500
            });
            location.reload();
          }else if(res.statusCode == 201){
              swal({
                title: res.message,
                icon: "error",
                timer: 9500
              });
          } else {
               swal({
                    title: data.message,
                    icon: "error",
                    timer: 9500
                  });
          }
        }, error: function (jqXHR, textStatus, err) {  
          if(jqXHR.responseJSON.statusCode == 403) {
          window.location.replace("https://admin.helloyubo.com/login");
          } else {
          alert("Something went wrong, please try again")
          }
        }
      });
}
/**To user update data */
$('.saveUpdates').click(function () {
    var data = {
      name: $('#name').val(),
      email: $('#email').val(),
      phone: $('#phone').val(),
      userId: editSelectedUser,
    }
    $.ajax({
      data: data, 
      async: false,
      cache: false,
      method: 'POST',
      url: '/updateUser',
      success:  function (data) {
        if (data.statusCode == 200) {
           swal({
            title: data.message,
            icon: "success",
            timer: 3000
          });
          location.reload();
        } else {
           swal({
            title: data.message,
            icon: "error",
            timer: 3000
          });
        }
      }, error: function (jqXHR, textStatus, err) {  
        if(jqXHR.responseJSON.statusCode == 403) {
        window.location.replace("https://admin.helloyubo.com/login");
        } else {
        alert("Something went wrong, please try again")
        }
      }
    });
  })
$('.chkall').click(function () {
    var z = this.checked;
    $('.chkbx').each(function () {
      this.checked = z;
    })
    checkbox();
});

$('.chkbx').click(function () {
    checkbox();
})
function checkbox() {
    var i = 0;
    window.a = [];
    $('.chkbx').each(function () {
      if (this.checked == true) {
        $('.bt2').removeClass('hide');
        $('.creategroup').removeClass('hide');
        a.push($(this).attr('value'));
        i = 1;
      }

    })
    if (i != 1) {
      $('.bt2').addClass('hide');
      $('.creategroup').addClass('hide')
      $('.chkall').prop('checked', false)
    } else if (i == 1) {
      $('.chkall').prop('checked', true)
    }
}

/***************************************
for adding members to modal of given leads
*****************************************/
$('.creategroup').click(function (i) {
    $('.removehtml').html('');
    $('.chkbx').each(function (i) {
        if ((this.checked == true) && $(this).closest('.tr2').attr('data-email')) {
            $('.adduserdata').append(`<tr class ="userdatalist">
            <td  class="userdataname" data-name="${$(this).closest('.tr2').attr('data-name')}">${$(this).closest('.tr2').attr('data-name')}</td>
            <td class="userdatamail" data-mail="${$(this).closest('.tr2').attr('data-email')}">${$(this).closest('.tr2').attr('data-email')}</td>
            <td class="userdataid" data-id="${$(this).closest('.tr2').attr('userid')}">&#10005;</td> </tr>`);
        }
    })
        $.ajax({
            type: "POST",
            url: "/userwhatsappGrpdata",
            data: '',
            success: function (data) {
              if (data) {
                  $('.group_select').html('')
                  $('.group_select').append(`<option>Create New</option>`)
                  for(i=0;i<data.data.length;i++){
                      $('.group_select').append(`<option groupId = ${data.data[i].groupId}>${data.data[i].groupName}</option>`)
                  }
              }
            }, error: function (jqXHR, textStatus, err) {
               alert('Please refresh !');
            }
         })
  $('#creategroupmodel').modal('show');
})

$(document).on('click',".add_more_user",function(e){
  if($('.instant_email').val()==''){
     alert('EmailId is mandatory')
     return ;
  }
  $('.adduserdata').prepend(`<tr class ="userdatalist">
   <td  class="userdataname" data-name="${$('.instant_user').val()}"></span></label>${$('.instant_user').val()}</td>
   <td class="userdatamail" data-mail="${$('.instant_email').val()}">${$('.instant_email').val()}</td>
    <td class="userdataid" data-id="Random${Math.random().toString(36).substr(2, 5)}">&#10005;</td> </tr>`);
  $('.instantuserdata').val('');
 
})

/******************************************
for deleting users from modal
*******************************************/
$(document).on("click", ".userdataid", function (e) {
    if ($('.userdatalist').length < 2) {
      return;
    }
    $(this).parent('tr').remove()
})
 
/***********************************
for creating group of users
************************************/
$(document).on("click", ".createNewGroup", function () {
    let grpname = $('.groupName').val();
    if ($(this).html()=='Create Group' && grpname == '') {
      alert('Group name Cannot be blank');
      return;
    }else{
        let creategroup = [], parentobj = {};
            $('.userdatalist').each(function (i) {
              let groupobj = {
                userId: $(this).find('.userdataid').attr('data-id'), 
                name: $(this).find('.userdataname').attr('data-name'), 
                mail: $(this).find('.userdatamail').attr('data-mail')
              }
              creategroup.push(groupobj)
            })
            parentobj[$('.groupName').val()] = creategroup;

            $.ajax({
              type: "POST",
              url: "/createWhatsappGroup",
              data: parentobj,
              success: function (data) {
                if (data) {
                  swal({
                      title: data.message,
                      icon: "success",
                      timer: 3500
                  });
                  setTimeout(() => {
                     location.reload();
                  }, 3500);
                }
              }, error: function (jqXHR, textStatus, err) {
                alert('Please refresh !');
              }
            })
    }
})

/**for adding more member*/
$(document).on("click", ".addNewmember", function () {
    let creategroup=[]; 
    let groupId =  $('.group_select option:selected').attr('groupid');
      
    $('.userdatalist').each(function (i) {
        let groupobj = {
          userId: $(this).find('.userdataid').attr('data-id'), name: $(this).find('.userdataname').attr('data-name'), mail: $(this).find('.userdatamail').attr('data-mail')
        }
        creategroup.push(groupobj)
    })
     $.ajax({
    type: "POST",
    url: "/addmoremember",
    data: {groupId:groupId,data:creategroup},
    success: function (data) {
      if (data) {
        alert(data.msg)
        location.reload();
      }
    }, error: function (jqXHR, textStatus, err) {
      alert('Please refresh !');
    }
  })

})
