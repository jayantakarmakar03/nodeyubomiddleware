const db = require("../models/all-models");

module.exports = {
    saveUpdtUsersInfoInDb: saveUpdtUsersInfoInDb
}

async function saveUpdtUsersInfoInDb(req, res) {
    let client = await db.Client.findOne({ clientId: req.body.clientId });
    if (!client) {
        return res.json({
            statusCode: 200,
            status: "failed",
            message: "Client doesn't exist"
        });
    }
    let user = await db.Users.findOne({
        clientId: req.body.clientId,
        $or: [{ phone: req.body.phone}, { email: req.body.email }]
    });
    if (user) {
        let userInfo = {
            name: req.body.name ? req.body.name : user.name,
            phone: req.body.phone ? req.body.phone : user.phone,
            email: req.body.email ? req.body.email : user.email,
            whatsappConsent: req.body.whatsappConsent ? req.body.whatsappConsent : user.whatsappConsent,
            updatedAt: Date.now()
        }
        await db.Users.updateOne({
            clientId: req.body.clientId,
            $or: [{ phone: req.body.phone }, { email: req.body.email }]
        },
            { $set: userInfo });
        return res.json({
            statusCode: 200,
            status: "success",
            message: "User updated successfully"
        });
    } else {
        if (req.body.phone || req.body.email) {
            let userInfo = new db.Users({
                clientId: req.body.clientId,
                name: req.body.name ? req.body.name : "",
                phone: req.body.phone ? req.body.phone : "",
                email: req.body.email ? req.body.email : "",
                whatsappConsent: req.body.whatsappConsent ? req.body.whatsappConsent : false
            })
            await userInfo.save();
            return res.json({
                statusCode: 200,
                status: "success",
                message: "User created successfully"
            });
        } else {
            return res.json({
                statusCode: 200,
                status: "failed",
                message: "Phone or email is required"
            });
        }

    }

}