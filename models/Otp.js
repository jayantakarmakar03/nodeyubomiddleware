var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
  mongoose.connect(require('./connection-string'));
}


//Guest model schema.
var newSchema = new Schema({
  'phone':{type:String},
  'otp':{type:String},
  'email':{type:String},
  'otp_verified':{type:Boolean,default:false},
  'clientId':{type:String },
  'status':{type: Boolean, default:false},
  'otpFor':{type : String ,enum : ['mobileVerification','twoFactorAuth','loginOtp'],default : 'mobileVerification'},
  'createdAt': { type: Date, default: Date.now },
  'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function(next){
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});


module.exports = mongoose.model('Otp', newSchema);