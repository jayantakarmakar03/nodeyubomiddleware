window.onload = function() {
	var domain = "https://yubo.yugasa.org"
	let host = location.hostname;
	let url=location.href
	var clientId="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6ImJvYl9jaGF0Ym90IiwiaWF0IjoxNjA4MzA4NzM2fQ.0sSdS18Q2MZYh3yu_Hq-Vji_f2KrHKR6bGh_f2mmTME";

	var parent = document.createElement('div');
	parent.setAttribute('class', 'iframe-box');
	parent.setAttribute('id', 'iframe-box');

	var bootm = document.createElement('div');
	bootm.setAttribute('class', 'bootm-box');
	bootm.setAttribute('id', 'bootm-box');

	var chatstart = document.createElement('div');
	chatstart.setAttribute('class', 'chat-start bounce-top');

	var closeimg = document.createElement('div');
	closeimg.setAttribute('class', 'close-img');
	closeimg.innerHTML='<svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg>'


	var outercircle = document.createElement('div');
	outercircle.setAttribute('class', 'outerCircle');

	var innercircle = document.createElement('div');
	innercircle.setAttribute('class', 'innerCircle');

	var icon = document.createElement('div');
	icon.setAttribute('class', 'icon');

	var bubbleimg  = document.createElement('img');
	bubbleimg.setAttribute('class', 'bubble-img');
	bubbleimg.src = domain+"/img/chat-bubble.svg";

	icon.appendChild(bubbleimg);

	chatstart.appendChild(outercircle)
	chatstart.appendChild(innercircle)
	chatstart.appendChild(icon)

	bootm.appendChild(chatstart);
    bootm.appendChild(closeimg);
    var iframe = document.createElement('iframe');
   	iframe.setAttribute('id', 'frame-lode');
    iframe.src = domain+"/chatWindow?clientId="+clientId+"&host="+host+"&url="+url;
    parent.appendChild(bootm);
    parent.appendChild(iframe);
    document.body.appendChild(parent);
	document.getElementById("bootm-box").addEventListener("click",function(){
		document.getElementById("iframe-box").classList.toggle("open-chat-window");
	});
};