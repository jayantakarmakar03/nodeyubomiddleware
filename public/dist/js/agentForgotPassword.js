function submitData() {

        let client_id = $("#client_id").val();
        let agentUserName = $("#agentUserName").val();

        if(client_id.trim().length == 0 ){
             swal({
                title: "Please provide client id",
                icon: "warning",
                timer: 5000
              });
        }else  if(agentUserName.trim().length  == 0 ){
            swal({
                title: "Please provide user name",
                icon: "warning",
                timer: 5000
              });
        }else{
            $.ajax({
                type: "POST",
                url: "/agent/changeAgentpassword",
                data: {
                    "clientId": client_id.trim(),
                    "email": agentUserName.trim()
                },
                timeout: 10000,
                success: async function (resp) {
                    if (resp) {
                        if (resp.statusCode === 200) {
                            console.log('result',resp)
                            swal({
                                title: "A password change request has been send to the client ",
                                icon: "success",
                                timer: 10000
                              });
                              setInterval(() => {
                                    window.location.replace('/agent/agentLogin')
                              }, 3000);

                        } else {
                            console.log(resp.message)
                            swal({
                                title: resp.message,
                                icon: "warning",
                                timer: 10000
                              });
                        }
                    }
                    else {
                        console.log('error occures')
                    }
                },
                error: function (jqXHR, textStatus, err) {
                    alert('Something went wrong, please refresh the page', err)
                }
            });
       }
}
$(window).on("keydown", function (e) {
    if (e.which == 13) {
        submitData();
    }
});

function forgotPassword(){
    window.location.replace('/agent/agentForgotPassword')
}
function goToLoginPage(){
    window.location.replace('/agent/agentlogin')
}