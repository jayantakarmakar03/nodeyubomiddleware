//**************** To open add action modal on click of add action button *******************//
$(".add_actions").click(function() {
    let slotList = {},
        id = '#' + $(this).attr("modal-id"),
        type = "newaction";
    checkUserType("show", async function(resp) {
        slotList = await getAllSlotFields();
        console.log("slotList", slotList)
        slotList.unshift({ key: 'yubo_global', name: 'yubo_global' })
        await slotdropdown(slotList);
        // await apndSlotsAsOptnsInSelect(slotList,[],type);
        $(id).modal("show");
    })
});

//**************** To add new action *******************//
$(".actionsave").click(function(event) {
    if(!$("#actnName").val()){
        return alert('action name is required.');
    }
    if(!$("#actnUrl").val()){
        return alert('action url is required.');
    }
    if(!$("#actnRqwstMthd").val() ){
        return alert('action method is required.');
    };

    let actionresponse = [];
    $('.responeappendiv').each(function() {
        let obj = {};
        obj[$(this).find('.inputkey').val()] = $(this).find('.inputvalue').val();
        actionresponse.push(obj);
    })
    event.preventDefault();
    let dataObj = {};
    checkUserType("show", function(resp) {
        dataObj = {
            actionName: $("#actnName").val() ? $("#actnName").val() : "",
            actionUrl: $("#actnUrl").val() ? $("#actnUrl").val() : "",
            actionHeaders: $("#actnHeaders").val() ? $("#actnHeaders").val() : "",
            actionData: $("#actnData").val() ? $("#actnData").val() : "",
            actionMethod: $("#actnRqwstMthd").val() ? $("#actnRqwstMthd").val() : "",
            actionSlotFields: $("#slotFields").val() ? $("#slotFields").val() : "",
            actionResponse: actionresponse
        };

        $.ajax({
            type: "POST",
            url: "/addAction",
            timeout: 10000,
            data: dataObj,
            success: async function(resp) {
                if (resp == 'exists') {
                    await alert('This action name is already exist.');
                } else {
                    location.reload();
                }
            },
            error: function(jqXHR, textStatus, err) {
                alert('Something went wrong, please refresh the page')
            }
        });
    });
});

//******************** To edit action *****************//
$("#actionTable").on("click", ".editAction", function() {
    var id = $(this).attr('value');
    $("#actionId").attr("value", id)
    checkUserType("show", function(resp) {
        $.ajax({
            type: "POST",
            url: "/actionById",
            timeout: 2000,
            data: { actionId: id },
            success: async function(data) {
                $('.editresponsediv').html('')
                for (const key in data[0]) {
                    if (key == 'actionResponse') {
                        data[0][key].forEach((key, index) => {
                            let uniqueid = Math.random()
                            let html = `<div class="editresponeappendiv" data-id="${uniqueid}">
                    <div class="col-sm-5">
                    <input type="text" name="mytext[]" value="${Object.keys(key)[0]}" class="form-control inputkey">
                    </div>
                    <div class="col-sm-5">
                    <input type="text" name="mytext[]" value="${key[Object.keys(key)[0]]}" class="form-control inputvalue">
                    </div>
                    <div class="col-sm-2 text-right">`
                            if (index == 0) {
                                html += ` <button type ="button" class="add_form_field btn br5 addeditresp">
                    <span>+</span>
                    </button>`
                            } else {
                                html += `<button type ="button" class="editdelteformfiled btn br5" id=${uniqueid}>
                    <span>-</span>
                </button>`
                            }
                            html += `</div>
                    </div>`;
                            $('.editresponsediv').append(html);
                        })

                    } else {
                        $(`#${key}`).val(JSON.parse(JSON.stringify(data[0][key])))
                    }
                }

                let allSlotsList = await getAllSlotFields()
                console.log('shivmmaa', data[0].actionSlotFields, allSlotsList)
                    //await slotfilter(data[0].actionSlotField,allSlotsList)
                await apndSlotsAsOptnsInSelect(data[0].actionSlotFields, allSlotsList)
            },
            error: function(jqXHR, textStatus, err) {
                alert("Something went wrong, please refresh the page")
            }
        });
    })
});

//*********** To update action *************//
$("#edit_action").submit(function(event) {
    let actionresponse = [];
    $('.editresponeappendiv').each(function() {
        let obj = {};
        obj[$(this).find('.inputkey').val()] = $(this).find('.inputvalue').val();
        actionresponse.push(obj);
    })
    event.preventDefault();
    let dataObj = {};
    checkUserType("show", function(resp) {
        dataObj = {
            actionId: $("#actionId").val(),
            actionName: $("#actionName").val(),
            actionUrl: $("#actionUrl").val(),
            actionHeaders: $("#actionHeaders").val(),
            actionData: $("#actionData").val(),
            actionMethod: $("#actionMethod").val(),
            actionSlotFields: $("#actionSlotFields").val(),
            actionResponse: actionresponse
        };

        $.ajax({
            type: "POST",
            url: "/updateAction",
            timeout: 10000,
            data: dataObj,
            success: function(resp) {
                if (resp == 'exists') {
                    $('#actionName').css('border-color', 'red');
                    $('#actionName').val('Action name exits');
                } else {
                    location.reload();
                }
            },
            error: function(jqXHR, textStatus, err) {
                alert('Something went wrong, please refresh the page')
            }
        });
    });
});

//********** To delete the action **********//
function deleteAction(id, name) {
    checkUserType("show", function(resp) {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this action!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "POST",
                        url: "/deleteAction",
                        timeout: 2000,
                        data: { actionId: id },
                        success: function(data) {
                            location.reload();
                        },
                        error: function(jqXHR, textStatus, err) {
                            //alert('text status '+textStatus+', err '+err)
                            swal("Something went wrong, please refresh the page");
                        }
                    });
                } else {
                    swal("Your Action is safe!");
                }
            });
    })
}

//**************** To get the list of all slots *******************//
async function getAllSlotFields() {
    var data = {};
    await $.ajax({
        type: "POST",
        url: "/getSlotsList",
        timeout: 10000,
        data: {},
        success: async function(resp) {
            console.log("slots", resp.data)
            data = resp.data
            data.push({ "key": "session.email", "name": "session.email" }, { "key": "session.name", "name": "session.name" }, { "key": "session.phone", "name": "session.phone" }, { "key": "session.score", "name": "session.score" }, { "key": "session.payload", "name": "session.payload" }, { "key": "session.order", "name": "session.order" }
                // {"key":"session.vid","name":"session.visitorid"}
            );
            return data;
        },
        error: function(jqXHR, textStatus, err) {
            alert('Something went wrong, please refresh the page')
        }
    });
    return data;
}

//**************** To append the slots in select *******************//
async function apndSlotsAsOptnsInSelect(selectedSlots, slotList) {
    console.log('shivamm', selectedSlots)
    for (i = 0; i < selectedSlots.length; i++) {
        $(".slotFields").append($("<option selected></option>").attr("value", selectedSlots[i]).text(selectedSlots[i]));
    }
    // for(const iterator of slotList){
    //     if
    //     for(i=0;i<selectedSlots.length;i++){
    //         if(selectedSlots[i] == iterator.name){
    //             $(".slotFields").append($("<option selected></option>").attr("value", iterator.name)
    //             .text(iterator.name));
    //             selectedSlots.pop(selectedSlots[i])
    //         } else{
    //             $(".slotFields").append($("<option></option>").attr("value", slotList[i])
    //             .text(slotList[i])); 
    //         }
    //     }
    // }

}
async function slotdropdown(dropdownlist) {
    for (const iterator of dropdownlist) {
        console.log("iterator.key", iterator.key)
        $(".addctionslotFields").append($("<option></option>").attr("value", iterator.key)
            .text(iterator.key));
    }
    $(".addctionslotFields").chosen({
        placeholder_text_multiple: "What are your slots" //placeholder
    });
}

//********************append respose field************/
$('.addresp').click(function() {
        let html = `<div class="responeappendiv" data-id="${Date.now()}">
    <div class="col-sm-5">
    <input type="text" name="mytext[]" class="form-control inputkey">
</div>
<div class="col-sm-5">
    <input type="text" name="mytext[]" class="form-control inputvalue">
</div>
<div class="col-sm-2 text-right">
    <button type ="button" class="delteformfiled btn br5" id=${Date.now()}>
        <span>-</span>
    </button>
</div>
</div>`;

        $('.responsediv').append(html)
    })
    /**remove appended field  on add*/
$(document).on('click', '.delteformfiled', function() {
    let deletionId = $(this).attr('id');
    $('.responeappendiv').filter(`[data-id="${deletionId}"]`).remove();
})

/**for edit repsonse */
$(document).on('click', '.addeditresp', function() {
        let html = `<div class="editresponeappendiv" data-id="${Date.now()}">
    <div class="col-sm-5">
    <input type="text" name="mytext[]" value="" class="form-control inputkey">
</div>
<div class="col-sm-5">
    <input type="text" name="mytext[]" value="" class="form-control inputvalue">
</div>
<div class="col-sm-2 text-right">
<button type ="button" class="editdelteformfiled btn br5" id=${Date.now()}>
<span>-</span>
    </button>
</div>
</div>`
        $('.editresponsediv').append(html);
    })
    /** */
$(document).on('click', '.editdelteformfiled', function() {
    let deletionId = $(this).attr('id');
    $('.editresponeappendiv').filter(`[data-id="${deletionId}"]`).remove();
})