window.onload = function() {
	const domain = "https://yubo.yugasa.org"
	let host = location.hostname;
	let url=location.href
	const clientId="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6InByYWJoYWthcmFuIiwiaWF0IjoxNjEzMjM2ODk4fQ.3FuMwFAXFQ4YmhMg4UgDiFYt2bGKJ8obZcwKXm91vR4";

	let parent = document.createElement('div');
	parent.setAttribute('class', 'iframe-box');
	parent.setAttribute('id', 'iframe-box');

	let bootm = document.createElement('div');
	bootm.setAttribute('class', 'bootm-box');
	bootm.setAttribute('id', 'bootm-box');

	let chatstart = document.createElement('div');
	chatstart.setAttribute('class', 'chat-start bounce-top');

	let closeimg = document.createElement('div');
	closeimg.setAttribute('class', 'close-img');
	closeimg.innerHTML='<svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg>'

	let outercircle = document.createElement('div');
	outercircle.setAttribute('class', 'outerCircle');

	let innercircle = document.createElement('div');
	innercircle.setAttribute('class', 'innerCircle');

	let icon = document.createElement('div');
	icon.setAttribute('class', 'icon');

	let bubbleimg  = document.createElement('img');
	bubbleimg.setAttribute('class', 'bubble-img');
	bubbleimg.src = domain+"/img/chat-bubble.svg";

	icon.appendChild(bubbleimg);

	chatstart.appendChild(outercircle)
	chatstart.appendChild(innercircle)
	chatstart.appendChild(icon)

	bootm.appendChild(chatstart);
    bootm.appendChild(closeimg);
    let iframe = document.createElement('iframe');
   	iframe.setAttribute('id', 'frame-lode');
    iframe.src = domain+"/chatWindow?clientId="+clientId+"&host="+host+"&url="+url;
    parent.appendChild(bootm);
    parent.appendChild(iframe);
    document.body.appendChild(parent);
	document.getElementById("bootm-box").addEventListener("click",function(){
		document.getElementById("iframe-box").classList.toggle("open-chat-window");
	});
};