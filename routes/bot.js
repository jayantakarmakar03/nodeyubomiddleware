const { saveBasicIntentsInDb } = require("../controllers/intents.js");
const genAi= require("../controllers/genAi.js");
const scrapper= require("../controllers/scrapper.js");
const config = require("../config/config.json");
const express = require("express"),
    yubo = require("../controllers/yubo.js"),
    db = require("../models/all-models"),
    jwt = require("jsonwebtoken"),
    languageConvert = require("../controllers/languageConvert"),
    bodyParser = require("body-parser"),
    axios = require("axios"),
    Fcm = require("../middleware/fcm"),
    bot = express.Router(),
    multer = require("multer"),
    emailController = require("../controllers/emailverification"),
    dbController = require("../controllers/dbintegration"),
    botApiController = require("../controllers/botApi"),
    fs = require("fs"),
    escapeHtml = require('escape-html'),
    sendMail = require("../middleware/sendChat.js"),
    storageController = require("../controllers/storageController");
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, "public/uploads");
    },
    filename: function(req, file, cb) {
        let extArray = file.mimetype.split("/");
        let extension = extArray[extArray.length - 1];
        cb(null, file.fieldname + "-" + Date.now() + "." + extension);
    },
});

var upload = multer({ storage: storage });

/* GET users listing. */
bot.get("/", function(req, res, next) {
    res.redirect("http://yugasa.org/");
});


bot.post("/updateGenAiConfig", async function(req, res, next) {
    try{
        await genAi.makeGenAiConfig(req.body.clientId,true);
        return res.json({
            success: true,
            message: 'Updated genAi Object for bot'
        });
    }catch(error){
        console.log('An error occurs in the catch block:-', error)
    }
});

bot.post("/notify", async function(req, res, next) {
    try{
        let data=req.body;
        console.log("***************notify url triggered*************",data)
        await io.sockets.to(data.userId).emit('notify',data);
        return res.json({
            success: true,
            message: 'Notification sent successfuly'
        });
    }catch(error){
        console.log('An error occurs in the catch block:-', error)
    }
});


//caal connect event
bot.post("/connect", function(req, res, next) {
    global.io.emit("user", req.body);
    return res.json(req.body);
});

let errorMessage = {
    default_opt: "False",
    replies: [],
    text: "Please come back and chat with me after some time. Right now I am taking some red and blue pills for maintenance.",
    type_option: true,
};

bot.post(
    "/fileUpload",
    upload.single("fileList"),
    async function(req, res, next) {
        try {

            let fileValidation = await validateFileType(req.file.mimetype);
            if (fileValidation === false || fileValidation == 'false') {
                let msg = "Selected file is not valid please choose an image/pdf/excel/word file"
                return res.json({ message: msg, respcode: '201' })
            }
            let client = await db.Client.findOne({ clientId: req.body.fileList[1] });
            let user = await db.Userchat.findOne({ userId: req.body.fileList[0], clientId: client._id }, { chats: 0 });
            let userCount = await db.Userchat.find({ clientId: client._id }).count();
            let userData = await db.Userchat.findOne({ userId: req.body.fileList[0] }, { agentCurrentConrol: 1 });
            let agentControl = "false";
            await storageController.uploadOnSelectedStorage(req, res, client, user, userCount, userData, agentControl)
                /*new Promise((resolve, reject) => {
                    filename = req.file.filename;
                    clientId = req.body.fileList[1];
                    AWS.config.update({
                        accessKeyId: config.s3Linode.accessKeyId,
                        secretAccessKey: config.s3Linode.secretAccessKey,
                        region: config.s3Linode.region,
                        s3BucketEndpoint: config.s3Linode.s3BucketEndpoint,
                    });
                    var s3 = new AWS.S3({ endpoint: config.s3Linode.url });
                    const fileContent = fs.readFileSync(req.file.path);
                    var params = {
                        Bucket: `yugasabot/${clientId}/Media/User_files`,
                        Key: `${filename}`,
                        ACL: "public-read",
                        ContentType: req.file.mimetype,
                        Body: fileContent,
                    };
                    s3.putObject(params, function(err, result) {
                        if (err) {
                            console.log("Error uploading data: ", err);
                            reject();
                        } else {
                            path = `${config.s3Linode.url}/${clientId}/Media/User_files/${filename}`;
                            resolve(path);
                        }
                    });

                }).then(path => {

                    var date = new Date();
                    if (userData) {
                        if (userData.agentCurrentConrol.currentControl == 'true' || userData.agentCurrentConrol.currentControl == true) {
                            agentControl = 'true';
                        }
                    }
                    if (!client) {
                        return res.json({
                            success: false,
                            message: errorMessage,
                            type_option: true,
                        });
                    }
                    console.log('kkk')
                    if (!user) {
                        let userUpload = new db.Userchat({
                            userId: req.body.fileList[0],
                            clientId: client._id,
                            name: "Visitor" + userCount,
                            fileUploaded: { path: path },
                            $addToSet: {
                                chats: {
                                    text: "Attachment sent",
                                    attachment: true,
                                    attachmentType: req.body.fileList[2],
                                    attachmentLink: path,
                                    agentControl: agentControl,
                                    messageType: "incoming",
                                    ignoreMsg: false,
                                    replies: [],
                                    node: 'False',
                                    type_option: true,
                                    createdAt: date,
                                },
                            },
                        });
                        userUpload.save(function(errr, result) {
                            if (result) {
                                return res.json({ attachmentLink: path, attachmentType: req.body.fileList[2], statusCode: 200 });
                            }
                        });
                    } else {
                        db.Userchat.updateOne({ userId: req.body.fileList[0] }, {
                            $push: { fileUploaded: { path: path } },
                            $addToSet: {
                                chats: {
                                    text: "Attachment sent",
                                    attachment: true,
                                    attachmentType: req.body.fileList[2],
                                    attachmentLink: path,
                                    agentControl: agentControl,
                                    messageType: "incoming",
                                    ignoreMsg: false,
                                    replies: [],
                                    node: 'False',
                                    type_option: true,
                                    createdAt: date,
                                },
                            },
                        }, function(err, result) {
                            if (result) {
                                return res.json({ attachmentLink: path, attachmentType: req.body.fileList[2], statusCode: 200 });
                            }
                        });
                    }

                }).catch(error => {
                    console.log('err in promise catch block  ', error);
                    return res.json({
                        success: false,
                        message: 'err in promise catch block',
                        error: error
                    });
                }).finally(() => {
                    fs.unlink(req.file.path, function(err) {
                        if (err) console.log('Error during unlink file ')

                        console.log('file deleted successfully')
                    });
                })*/
        } catch (err) {
            console.log('err in try catch block ', err);
            return res.json({
                success: false,
                message: 'err in try catch block',
                error: err
            });
        }
    }
);

/** This module used for user chat conversation*/
bot.post("/sendMessage", async function(req, res, next) {
try{
    let isHinglish=false;
    let api_key=false;
    req.body.text = escapeHtml(req.body.text);
    let hinglish_text=req.body.text
    api_key=await genAi.makeGenAiConfig(req.body.clientId);
    if(api_key){
        let resp=await genAi.detectLang(hinglish_text,req.body.clientId);
        if(resp.isHinglish){
            isHinglish=true;
            hinglish_text=resp.text;
        }
    }
    /*********************************************************
     to identify which conversation happened during agent communication we added a key agentControl="true" 
     with the message so later we can count this data as user to agent conversation (for /userAgentConversation api)
     *********************************************************/
    let userData = await db.Userchat.findOne({ userId: req.body.userId }, { agentCurrentConrol: 1 });
    let agentControl = "false";
    if (userData) {
        if (userData.agentCurrentConrol.currentControl == 'true' || userData.agentCurrentConrol.currentControl == true) {
            agentControl = 'true';
        }
    }
    //*********************************
    var date = new Date();
    let client = await db.Client.findOne({ clientId: req.body.clientId });
    if (!client) {
        return res.json({
            success: false,
            message: errorMessage,
            type_option: true,
        });
    }
    const user = await db.Userchat.findOne({ userId: req.body.userId, clientId: client._id }, { chats: 0 });

    let userCount = await db.Userchat.find({ clientId: client._id }).count();

    if (!req.body.node) {
        req.body.node = user.node || "False";
    }
    let requestData = {
        userId: req.body.userId,
        client_id: req.body.clientId,
        // category: req.body.category,
        category: client.type ? client.type : "basic",
        node: req.body.node,
        story: client.story ? client.story : "True",
        open: client.open ? client.open : "False",
        session: req.body.session,
        train: "False",
        unsubsribe: "False",
        update_tree: "False",
        update_story: "False",
    };

    if (!user) {
        if (req.body.session && req.body.session.name == undefined) {
            req.body.session['name'] = ''
        }
        if (req.body.session && req.body.session.email == undefined) {
            req.body.session['email'] = ''
        }
        if (req.body.session && req.body.session.phone == undefined) {
            req.body.session['phone'] = ''
        }
        requestData.session = req.body.session;
        requestData.text = "restart yubo";
        requestData.node = req.body.node ? req.body.node : "False";
        let userMsg = new db.Userchat({
            userId: req.body.userId,
            clientId: client._id,
            name: "Visitor" + userCount,
            location: req.body.location,
            session: req.body.session,
            userState: "OPEN",
            lastActiveTime: new Date(),
            chats: [{
                text: req.body.text,
                agentControl: agentControl,
                messageType: "incoming",
                replies: [],
                type_option: true,
                ignoreMsg: false,
                session: req.body.session,
                node: req.body.node,
                url: req.body.url,
                createdAt: date,
            }, ],
        });
        await userMsg.save();
        checkNewUserNotificationFlag(req.body.clientId, req.body.userId)
        axios({
            method: "POST",
            url: process.env.pythonUrl,
            data: { data: requestData },
        }).then(async function(response) {});
    } else {
        let updateChat = await db.Userchat.updateOne({ userId: req.body.userId, clientId: client._id }, {
            lastActiveTime: new Date(),
            userState: "OPEN",
            $addToSet: {
                chats: {
                    text: req.body.text,
                    agentControl: agentControl,
                    messageType: "incoming",
                    ignoreMsg: false,
                    replies: [],
                    node: req.body.node,
                    url: req.body.url,
                    type_option: true,
                    createdAt: date,
                },
            },
        });
    }
    let chatId = await db.Userchat.findOne({ userId: req.body.userId, clientId: client._id }, { chats: 0 });
    if (chatId.session && chatId.session.name == undefined) {
        chatId.session['name'] = ''
    }
    if (chatId.session && chatId.session.email == undefined) {
        chatId.session['email'] = ''
    }
    if (chatId.session && chatId.session.phone == undefined) {
        chatId.session['phone'] = ''
    }
    requestData.session = {...chatId.session, ...req.body.session };
    chatId.session = requestData.session;
    requestData.text = req.body.text;
    console.log("######### isHinglish text #######", isHinglish)
    if(isHinglish){
        requestData.text = hinglish_text
    }
    global.io.emit(client._id, {
        _id: chatId._id,
        name: chatId.name,
        userId: req.body.userId,
        text: req.body.text,
        node: req.body.node,
        url: req.body.url,
        messageType: "incoming",
        replies: [],
        typing: true,
        createdAt: date,
    });
    Fcm.sendMessage(client.deviceId, {
        _id: chatId._id,
        name: chatId.name,
        userId: req.body.userId,
        text: req.body.text,
        node: req.body.node,
        url: req.body.url,
        messageType: "incoming",
        replies: [],
        typing: true,
        createdAt: date,
    });

    if (chatId.chatControl == 1) {
        return res.json({ success: false, chatControl: 1 });
    }
    if (client.middleware != undefined && client.middleware != '') {
        try {
            let pythondata = await axios({
                method: "post",
                url: process.env.pythonUrl,
                data: { data: requestData },
            });
            pythondata = pythondata.data

            let slotdata = await axios({
                method: "post",
                url: process.env.findSlotsPythonUrl,
                data: { client_id: requestData.client_id, query: requestData.text },
            });

            slotdata = slotdata.data
            var data = await axios({
                method: "post",
                url: client.middleware,
                data: { data: requestData, response: pythondata, slotdata: slotdata },
            });
        } catch (err) {
            console.log('pyerrpr', err)
            return res.json({
                success: false,
                message: errorMessage,
                type_option: true,
            });
        }
    } else if (client.isActionEnabled) {
        try {
            let contextObj = chatId.context ? chatId.context : {},
                session = chatId.session ? chatId.session : { email: "", name: "", phone: "", source: "" };
            requestData = {...requestData, context: contextObj, clientId: chatId.clientId, session: session }
            var data = await dbController.botrequest({ data: requestData });
            if (data.status == "failed") {
                errorMessage["text"] = data.message;
                return res.json({
                    success: false,
                    message: errorMessage,
                    type_option: true,
                });
            }
        } catch (err) {
            return res.json({
                success: false,
                message: errorMessage,
                type_option: true,
            });
        }
    } else {
        try {
            var data = await axios({
                method: "post",
                url: req.body.clientId == "farmitra_chatbot_hi" ?
                    process.env.hindiYubo : process.env.pythonUrl,
                data: { data: requestData },
            });
        } catch (err) {
            return res.json({
                success: false,
                message: errorMessage,
                type_option: true,
            });
        }
    }

    date.setSeconds(date.getSeconds() + 2);

    if (chatId.session && data.data.session != undefined) {
        if (
            JSON.stringify(chatId.session.email) ===
            JSON.stringify(data.data.session.email) &&
            JSON.stringify(chatId.session.phone) ===
            JSON.stringify(data.data.session.phone)
        ) {
            data.data.session.createdDate = (data.data.session.createdDate != null) ? data.data.session.createdDate : new Date();
        } else {
            data.data.session.createdDate = new Date();
        }
    } else {
        data.data.session = chatId.session;
    }
    let dateTime = new Date();
    if (user) {
        let updateChat = await db.Userchat.updateOne({ _id: user._id }, {
            $set: {
                session: data.data.session,
                node: data.data.default_opt,
                date: dateTime,
            },
        });
    }

    let response_source =
        data.data.tree !== "false" ?
        "tree" :
        data.data.intent == "basic_yubo" ?
        "story" :
        "intent";
    if (data.data.tree !== "false" && data.data.tree !== undefined && data.data.tree.attachment != false && data.data.tree.attachment != undefined) {
        try {
            let ext = data.data.tree.attachment.split('.').pop();
            let attachment = { "type": ext, "link": data.data.tree.attachment }
            let attchat = {
                text: "Attachment sent",
                ignoreMsg: false,
                messageType: "outgoing",
                attachment: true,
                attachmentType: attachment.type,
                attachmentLink: attachment.link,
                replies: [],
                type_option: data.data.type_option,
                possible_conflict: data.data.possible_conflict,
                user_query: data.data.user_query,
                intent: data.data.intent,
                response_source: response_source,
                score: data.data.score,
                node: data.data.default_opt,
                createdAt: date,
                session: data.data.session,
                client_id: data.data.client_id
            }
            await db.Userchat.updateOne({ userId: req.body.userId, clientId: client._id }, {
                $addToSet: {
                    chats: attchat,
                },
            });
        } catch (e) {
            console.log("attachment save error", e);
        }
    }
    let pythonRply = {
        text: data.data.text,
        textParams: data.data.textParams,
        ignoreMsg: false,
        messageType: "outgoing",
        replies: data.data.replies,
        products: (data.data.tree !== "false" && data.data.tree !== undefined) ? data.data.tree.products : [],
        type_option: data.data.type_option,
        possible_conflict: data.data.possible_conflict,
        user_query: data.data.user_query,
        intent: data.data.intent,
        response_source: response_source,
        score: data.data.score,
        // node: req.body.node,
        node: data.data.default_opt,
        createdAt: date,
    };
    if (data.data.tree) {
        data.data.tree.default ? (pythonRply.default = data.data.tree.default) : "";
        data.data.tree.webhook ? (pythonRply.webhook = data.data.tree.webhook) : "";
        data.data.tree.form ? (pythonRply.form = data.data.tree.form) : "";
    }
    pythonRply.session = data.data.session;
    pythonRply.client_id = data.data.client_id;
    pythonRply = await languageConvert.changeLanguage(pythonRply);

    data.data.text = pythonRply.text

    data.data.replies = pythonRply.replies

    if(isHinglish){
        data.data.text=await genAi.convertResponse(pythonRply.text,pythonRply.client_id);
        pythonRply.text=data.data.text
        // if(data.data.replies.length>0){
        //     console.log("#########data.data.replies############",data.data.replies)
        //     data.data.replies = await Promise.all(data.data.replies.map(async element => {
        //         element.option=await genAi.convertResponse(element.option);
        //         return element;
        //     }));
        //     console.log("#########updated_replies############",data.data.replies)
        //     pythonRply.replies=data.data.replies;
        // }        
    }

    let updateOpt = await db.Userchat.updateOne({ userId: req.body.userId, clientId: client._id }, {
        $addToSet: {
            chats: pythonRply,
        },
    });

    //Hubspot
    let updateHubProp = false;
    if (user) {
        if (
            req.body.hubprop &&
            req.body.hubprop != "" &&
            req.body.hubprop != null &&
            user.session &&
            user.session.vid
        ) {
            updateHubProp = await updateHubProperties(
                req.body.hubprop,
                req.body.text,
                data.data.client_id,
                user.session.vid
            );
        }
    }

    //Flag to indicate the form has submitted or not before by the user
    let formAlreadyFilled = false;
    if (
        data.data.tree &&
        data.data.tree.form &&
        user && user.formdata &&
        user.formdata.hasOwnProperty(data.data.tree.node_name)
    ) {
        data.data["formValues"] = await formDataLabeling(
            client,
            user.formdata[data.data.tree.node_name],
            data.data.tree.node_name
        ).then(function(value) {
            return objToString(value); //To convert object in string formate
        });
        formAlreadyFilled = true;
    }
    data.data["updateHubProp"] = updateHubProp;
    //data.data = await languageConvert.changeLanguage(data.data);
    return res.json({
        success: true,
        formAlreadyFilled: formAlreadyFilled,
        message: data.data,
    });
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});

bot.post("/followUp", async function(req, res, next) {
try{
    var date = new Date();
    let client = await db.Client.findOne({ clientId: req.body.clientId });
    if (!client) {
        return res.json({
            success: false,
            message: errorMessage,
            type_option: true,
        });
    }

    const user = await db.Userchat.findOne({ userId: req.body.userId, clientId: client._id }, { chats: 0 });

    let userCount = await db.Userchat.find({ clientId: client._id }).count();

    if (!req.body.node) {
        req.body.node = user.node || "False";
    }

    let requestData = {
        userId: req.body.userId,
        client_id: req.body.clientId,
        // category: req.body.category,
        category: client.type ? client.type : "basic",
        node: req.body.node,
        story: client.story ? client.story : "True",
        open: client.open ? client.open : "False",
        session: req.body.session,
        train: "False",
        unsubsribe: "False",
        update_tree: "False",
        update_story: "False",
    };

    let chatsObj = {
        text: req.body.text ? req.body.text : "",
        ignoreMsg: false,
        messageType: "incoming",
        replies: req.body.options ? req.body.options : [],
        type_option: req.body.type_opt,
        possible_conflict: false,
        // response_source: "tree",
        node: req.body.node,
        createdAt: date,
    };
    let chatId = undefined;
    if (!user) {
        requestData.text = req.body.text;
        requestData.node = req.body.node ? req.body.node : 'False';
        // if (req.body.text) {

        //commented to avoid entry of followup node during the chat initiation in the db
        // let userMsg = new db.Userchat({
        //     'userId': req.body.userId,
        //     'clientId': client._id,
        //     'name': "Visitor" + userCount,
        //     'chats': [chatsObj]
        // });
        // await userMsg.save();
        //comment end

        // }
        // axios({
        //     method: 'POST',
        //     url: process.env.pythonUrl,
        //     data: { data: requestData }
        // }).then(async function(response) {

        // });
    } else {
        // if (req.body.text) {
        let updateChat = await db.Userchat.updateOne({ userId: req.body.userId, clientId: client._id }, {
            $addToSet: {
                chats: chatsObj
            }
        });
        chatId = await db.Userchat.findOne({ userId: req.body.userId, clientId: client._id }, { chats: 0 });
    }
    if (chatId != undefined) {
        global.io.emit(client._id, {
            _id: chatId._id,
            name: chatId.name,
            userId: req.body.userId,
            text: req.body.text,
            node: req.body.node,
            messageType: "incoming",
            replies: [],
            typing: true,
            createdAt: date,
        });
        Fcm.sendMessage(client.deviceId, {
            _id: chatId._id,
            name: chatId.name,
            userId: req.body.userId,
            text: req.body.text,
            node: req.body.node,
            messageType: "incoming",
            replies: [],
            typing: true,
            createdAt: date,
        });
        if (chatId.chatControl == 1) {
            return res.json({ success: false, chatControl: 1 });
        }

    } else {
        chatId = {}
        chatId['session'] = { email: "", name: "", phone: "", source: "" }
        chatId['context'] = {}
        chatId['clientId'] = req.body.clientId
    }

    requestData.session = req.body.session;
    requestData.text = req.body.text;

    if (client.middleware != undefined && client.middleware != '') {
        try {
            let pythondata = await axios({
                method: "post",
                url: process.env.pythonUrl,
                data: { data: requestData },
            });
            pythondata = pythondata.data

            let slotdata = await axios({
                method: "post",
                url: process.env.findSlotsPythonUrl,
                data: { client_id: requestData.client_id, query: requestData.text },
            });

            slotdata = slotdata.data
            var data = await axios({
                method: "post",
                url: client.middleware,
                data: { data: requestData, response: pythondata, slotdata: slotdata },
            });
        } catch (err) {
            console.log('pyerrpr', err)
            return res.json({
                success: false,
                message: errorMessage,
                type_option: true,
            });
        }
    } else if (client.isActionEnabled) {
        try {
            let contextObj = chatId.context ? chatId.context : {},
                session = chatId.session ? chatId.session : { email: "", name: "", phone: "", source: "" };
            requestData = {...requestData, context: contextObj, clientId: chatId.clientId, session: session }
            var data = await dbController.botrequest({ data: requestData });
            if (data.status == "failed") {
                errorMessage["text"] = data.message;
                return res.json({
                    success: false,
                    message: errorMessage,
                    type_option: true,
                });
            }
        } catch (err) {
            return res.json({
                success: false,
                message: errorMessage,
                type_option: true,
            });
        }
    } else {
        try {
            var data = await axios({
                method: "post",
                url: req.body.clientId == "farmitra_chatbot_hi" ?
                    process.env.hindiYubo : process.env.pythonUrl,
                data: { data: requestData },
            });
        } catch (err) {
            return res.json({
                success: false,
                message: errorMessage,
                type_option: true,
            });
        }
    }

    date.setSeconds(date.getSeconds() + 2);

    //console.log("Data Session print for debugging:", data.data.session)

    if (chatId.session && data.data && data.data.session && data.data.session.email && data.data.session.phone) {
        if (
            JSON.stringify(chatId.session.email) ===
            JSON.stringify(data.data.session.email) &&
            JSON.stringify(chatId.session.phone) ===
            JSON.stringify(data.data.session.phone)
        ) {
            data.data.session.createdDate = (data.data.session.createdDate != null) ? data.data.session.createdDate : new Date();
        } else {
            data.data.session.createdDate = new Date();
        }
    }

    let response_source =
        data.data.tree !== "false" ?
        "tree" :
        data.data.intent == "basic_yubo" ?
        "story" :
        "intent";

    let dateTime = new Date();
    if (user) {
        let updateChat = await db.Userchat.updateOne({ _id: user._id }, {
            $set: {
                session: data.data.session,
                node: data.data.default_opt,
                date: dateTime,
            },
        });
    }
    if (data.data.tree !== "false" && data.data.tree !== undefined && data.data.tree.attachment != false && data.data.tree.attachment != undefined) {
        try {
            let ext = data.data.tree.attachment.split('.').pop();
            let attachment = { "type": ext, "link": data.data.tree.attachment }
            let attchat = {
                text: "Attachment sent",
                ignoreMsg: false,
                messageType: "outgoing",
                attachment: true,
                attachmentType: attachment.type,
                attachmentLink: attachment.link,
                replies: [],
                type_option: data.data.type_option,
                possible_conflict: data.data.possible_conflict,
                user_query: data.data.user_query,
                intent: data.data.intent,
                response_source: response_source,
                score: data.data.score,
                node: data.data.default_opt,
                createdAt: date,
                session: data.data.session,
                client_id: data.data.client_id
            }
            await db.Userchat.updateOne({ userId: req.body.userId, clientId: client._id }, {
                $addToSet: {
                    chats: attchat,
                },
            });
        } catch (e) {
            console.log("attachment save error", e);
        }
    }
    let pythonRply = {
        text: data.data.text,
        textParams: data.data.textParams,
        ignoreMsg: false,
        messageType: "outgoing",
        replies: data.data.replies,
        products: (data.data.tree !== "false") ? (data.data.tree.products) ? data.data.tree.products : [] : [],
        type_option: data.data.type_option,
        possible_conflict: data.data.possible_conflict,
        intent: data.data.intent,
        response_source: response_source,
        score: data.data.score,
        // node: req.body.node,
        node: data.data.default_opt,
        createdAt: date,
    };
    if (data.data.tree) {
        data.data.tree.default ? (pythonRply.default = data.data.tree.default) : "";
        data.data.tree.webhook ? (pythonRply.webhook = data.data.tree.webhook) : "";
        data.data.tree.form ? (pythonRply.form = data.data.tree.form) : "";
    }

    pythonRply.session = data.data.session;
    pythonRply.client_id = data.data.client_id;
    pythonRply = await languageConvert.changeLanguage(pythonRply, false);
    let updateOpt = await db.Userchat.updateOne({ userId: req.body.userId, clientId: client._id }, {
        $addToSet: {
            chats: pythonRply,
        },
    });

    //Hubspot
    let updateHubProp = false;
    if (user) {
        if (
            req.body.hubprop &&
            req.body.hubprop != "" &&
            req.body.hubprop != null &&
            user.session &&
            user.session.vid
        ) {
            updateHubProp = await updateHubProperties(
                req.body.hubprop,
                req.body.text,
                data.data.client_id,
                user.session.vid
            );
        }
    }

    //Flag to indicate the form has submitted or not before by the user
    let formAlreadyFilled = false;
    if (
        data.data.tree.form &&
        user.formdata.hasOwnProperty(data.data.tree.node_name)
    ) {
        formAlreadyFilled = true;
    }

    data.data["updateHubProp"] = updateHubProp;
    data.data.text = pythonRply.text
    data.data.replies = pythonRply.replies
        //data.data = await languageConvert.changeLanguage(data.data);
    return res.json({
        success: true,
        formAlreadyFilled: formAlreadyFilled,
        message: data.data,
    });
}catch(error){
    console.log('Error in catch block:', error)
}
});

/** This module used for user chat conversation*/
bot.post("/launch", async function(req, res, next) {
    try{
    let client = await db.Client.findOne({ clientId: req.body.clientId });
    let requestData = {
        userId: "bf235b58-128d-0b62-a40f-bbd0952109b6",
        client_id: req.body.clientId,
        category: client.type ? client.type : "basic",
        node: "launch",
        story: client.story ? client.story : "True",
        open: client.open ? client.open : "False",
        session: { email: "", name: "", phone: "", source: "" },
        train: "False",
        unsubsribe: "False",
        update_tree: "False",
        update_story: "False",
        text: "",
    };
    // pythonUrl: 'http://172.105.42.134:5000/yugasa'
    try {
        var data = await axios({
            method: "post",
            url: req.body.clientId == "farmitra_chatbot_hi" ?
                process.env.hindiYubo : process.env.pythonUrl,
            data: { data: requestData },
        });
    } catch (err) {
        return res.json({
            success: false,
            message: errorMessage,
            type_option: true,
        });
    }

    return res.json({ success: true, message: data.data });
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});

/** This module used for user chat conversation*/
bot.post("/setStory", async function(req, res, next) {
    console.log("SET STORY **********************/////////////////*************",req.body.story)
    let setstory;
    if(req.body.clientId=="yugasa_chatbot" || req.body.clientId=="yubo_bot"){
        setstory=await scrapper.extractStory(req.body.clientId, req.body.userId,req.body.url,req.body.story)
    }
    if(setstory){
        return res.json({ message: "Story set successfuly", status: "success"});
    } else {
        return res.json({ message: "There was some error setting story", status: "failed"});
    }
});


bot.post("/nodeExists", async(req, res) => {
    let nodelist=[];
    let nodes;
    let status="error"
    try{
        let client = await db.Client.findOne({clientId: req.body.clientId},{_id : 1})
        if(client){
            let tree = await db.Tree.findOne({ client: client._id });
            if (tree) {
                nodes = !tree.jsonOfTree.DTree
                  ? tree.jsonOfTree
                    ? JSON.parse(tree.jsonOfTree).DTree
                    : ""
                  : tree.jsonOfTree.DTree;
            }
            nodes.forEach(function (element) {
                nodelist.push(element.node_name);
                nodelist.push(element.node_id)
            });
            if(nodelist.includes(req.body.node)){
                status="success";
            }            
        }
    } catch(err) {
        console.log("nodeExists error ",err);
    }
    return res.send({"status":status});
});

//When contact form gets submitted from chatbot
bot.post("/formSubmit", async function(req, res) {
try{
    let client = await db.Client.findOne({ clientId: req.body.clientId }),
        webhookResponse = req.body.formData.hutk ? req.body.formData.hutk : "",
        webhookDetails = "";
    if (!client) {
        return res.json({
            success: false,
            message: errorMessage,
            type_option: true,
        });
    }
    //To integrate with any webhook
    if (req.body.webhook) {
        webhookDetails = await db.Webhook.findOne({
            clientId: req.body.clientId,
            webhookName: req.body.webhook,
        });
        // if (webhookDetails) {
        //   webhookResponse = await callToWebhook(webhookDetails, req.body.formData);
        // }
    }

    let key = Object.keys(req.body.formobj)[0];
    // let formdata1 = formDataLabeling(client, req.body.formobj[key], key);
    let user = await db.Userchat.findOne({ userId: req.body.userId, clientId: client._id }, { chats: 0 });
    //To replace the form name key with label, formDataLabeling function returns the promise
    let formdata = await formDataLabeling(
        client,
        req.body.formobj[key],
        key
    ).then(function(value) {
        req.body.location ? (value["City"] = req.body.location.city) : "";
        return objToString(value); //To convert object in string formate
    });

    if (user) {
        if (user.formdata.hasOwnProperty(key)) {
            for (key1 in req.body.formobj[key]) {
                if (user.formdata[key][key1]) {
                    user.formdata[key][key1] = req.body.formobj[key][key1];
                } else {
                    Object.assign(user.formdata[key], req.body.formobj[key]);
                }
            }
            // user.formdata[key] = req.body.formobj[key];
        } else {
            Object.assign(user.formdata, req.body.formobj);
        }
    }
    let session;
    if (!user) {
        session = {
            email: req.body.formData.email,
            name: req.body.formData.fname,
            phone: req.body.formData.phone,
            createdDate: new Date(),
            vid: webhookResponse ? webhookResponse : "",
        }
        let formData = new db.Userchat({
            userId: req.body.userId,
            clientId: client._id,
            location: req.body.location,
            session: session,
            formdata: req.body.formobj,
            chats: [{
                text: formdata,
                messageType: "outgoing",
                createdAt: new Date(),
            }, ],
            //  (webhookResponse && webhookResponse.vid) ? webhookResponse.vid : null
        });

        let dbd = await formData.save();
    } else {
        session = {
            email: user.session.email ? user.session.email : req.body.formData.email,
            name: user.session.name ? user.session.name : req.body.formData.fname,
            phone: user.session.phone ? user.session.phone : req.body.formData.phone,
            createdDate: new Date(),
            vid: webhookResponse ? webhookResponse : "",
        }
        let dbd = await db.Userchat.updateOne({ userId: req.body.userId, clientId: client._id }, {
            $addToSet: {
                chats: {
                    text: formdata,
                    messageType: "outgoing",
                    createdAt: new Date(),
                },
            },
            $set: {
                session: session,
                formdata: user.formdata,
            },
        });
    }

    key == "start" ? await emailController.leadNotificationMail(client.email, formdata, req.body.formData.fname) : "";
    return res.json({
        success: true,
        session: session,
        data: webhookDetails ? webhookDetails : "",
        message: "Thanks for sharing the details.",
    });
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});

/** This module used for user chat history*/
bot.post("/chatHistory", async function(req, res, next) {
try{
    let client = await db.Client.findOne({ clientId: req.body.clientId });
    (req.body.clearChat) ? await clearChatHistory(req.body.clientId, req.body.userId): "";
    let userChat = await db.Userchat.aggregate([{
            $match: {
                userId: req.body.userId,
                clientId: client._id,
                $or: [{ "chats.hideChathistory": { $exists: true } }, { "chats.hideChathistory": { $exists: false } }]
            }
        },
        {
            $project: {
                chats: {
                    $filter: {
                        input: '$chats',
                        as: 'chat',
                        cond: { $ne: ['$$chat.hideChathistory', true] }
                    }
                },
                session: 1,
                _id: 0
            }
        }
    ]);
    userChat = userChat[0];

    if (userChat && userChat.chats.length > 0) {
        userChat.chats.map(function(item) {
            if (item.messageType == "outgoing") {
                if (Array.isArray(item.node)) {
                    item.node = "False"
                }
                if (Array.isArray(item.intent)) {
                    item.intent = ""
                }
            }
            return item;
        })
        return res.json({ success: true, message: { userChat: userChat.chats, sessionData: userChat.session } });
    } else {
        let treeData = await db.Tree.findOne({ client: client._id });
        // To show the contact form for new user on bot
        try {
            if (treeData) {
                treeData = JSON.parse(treeData.jsonOfTree).DTree;
                treeData.forEach(async function(item) {
                    if (item.node_name == "start") {
                        // await saveInDb(client, req.body.userId, item)
                        return res.json({
                            success: true,
                            formData: item,
                        });
                    }
                });
            } else {
                return res.json({ success: true, message: false });
            }
        } catch (err) {
            console.log("err", err)
            return res.json({ success: true, message: false });
        }

    }
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});

function getClient(req) {
try{
    var data = req.headers["host"].split(";");
    var client = "";
    if (data[0] == "yugasa.com") {
        client = "yugasa_chatbot";
    } else {
        client = "amit_chatbot";
    }
    return client;
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

async function updateHubProperties(propertyName, value, clientId, vid) {
try{
    const getQuery = await db.Integrations.findOne({ clientId: clientId });
    if (getQuery) {
        if (getQuery.name == "Hubspot") {
            let data = {
                fields: [{
                    name: propertyName,
                    value: value,
                }, ],
                context: {
                    hutk: vid,
                    // pageUri: "www.example.com/page",
                    // pageName: "Example page",
                },
            };
            return {
                authKey: getQuery.apiKey,
                data: data
            }
        }
    }
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

bot.post("/yuboFirstInstance", yubo.yuboFirstInstance);

async function formDataLabeling(client, formdata1, key) {
try{
    if (!client) return;
    let treeData = await db.Tree.findOne({ client: client._id });
    let newobj = JSON.parse(treeData.jsonOfTree),
        customformdata;
    for (i = 0; i < newobj.DTree.length; i++) {
        if (newobj.DTree[i].node_name == key || newobj.DTree[i].node_id == key) {
            customformdata = newobj.DTree[i].form;
            break;
        }
    }
    let customdata =
        typeof customformdata == "string" ?
        JSON.parse(customformdata) :
        customformdata,
        newdataobj = {};
    if (customdata) {
        for (const key in formdata1) {
            for (const iterator of customdata) {
                if (key === iterator.name) {
                    if (iterator.label == "Submit" || iterator.label == "Button") {
                        continue;
                    } else {
                        newdataobj[iterator.label] = formdata1[key];
                    }
                }
            }
        }
    }
    return newdataobj;
    /************************label code******************************************************* */
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

function objToString(obj) {
try{
    var str = "";
    for (var label in obj) {
        if (obj.hasOwnProperty(label)) {
            str += label + " : " + obj[label] + "<br>";
        }
    }
    return str;
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

async function clearChatHistory(clientId, userId) {
try{
    let client = await db.Client.findOne({ clientId: clientId });
    if (!client) {
        return {
            success: false,
            message: errorMessage,
            type_option: true,
        }
    }
    const user = await db.Userchat.findOne({ userId: userId, clientId: client._id }, { chats: 0 });

    if (!user) {
        return { status: "error", statusCode: 204, data: "No history to clear" }
    } else {
        await db.Userchat.updateOne({
            clientId: client._id,
            userId: userId
        }, { $set: { "chats.$[].hideChathistory": true } }, {
            arrayFilters: [{ "chats.createdAt": 1 }],
            multi: true
        });



        let reqData = {
            "data": {
                "userId": "c1fcfbca-af31-5885-5779-dc9f0c91dd27",
                "client_id": "test_chatbot",
                "train": "False",
                "unsubsribe": "False",
                "text": "restart yubo",
                "node": "False",
                "session": {
                    "detect_lang": false,
                    "email": "",
                    "lang": "en",
                    "name": "",
                    "phone": ""
                },
                "category": "pro",
                "update_tree": "False",
                "update_story": "False",
                "story": "True",
                "open": "True"
            }
        }

        return { status: "success", statusCode: 200, data: "History Cleared" }
    }

}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

async function saveInDb(client, userId, item) {
try{
    let chatsObj = {
        text: item.text,
        ignoreMsg: false,
        messageType: "outgoing",
        replies: item.options,
        type_option: item.type_opt,
        possible_conflict: false,
        response_source: "tree",
        node: item.node_id,
        createdAt: new Date(),
    };
    item.default ? (chatsObj.default = item.default) : "";
    item.webhook ? (chatsObj.webhook = item.webhook) : "";
    item.form ? (chatsObj.form = item.form) : "";

    const user = await db.Userchat.findOne({ userId: userId, clientId: client._id }, { chats: 0 });
    if (!user) {
        let userMsg = new db.Userchat({
            userId: userId,
            clientId: client._id,
            // name: "Visitor" + userCount,
            // location: req.body.location,
            session: item.session ? item.session : { email: "", name: "", phone: "", source: "" },
            chats: [
                chatsObj,
            ],
        });
        await userMsg.save();
    } else {
        let dbdata = await db.Userchat.updateOne({ userId: userId, clientId: client._id }, {
            $addToSet: {
                chats: chatsObj,
            },
        });
    }
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

function validateFileType(mimetype) {
try{
    let matched = false;
    switch (mimetype) {
        case "image/jpeg":
            matched = true;
            break;
        case "image/jpg":
            matched = true;
            break;
        case "image/png":
            matched = true;
            break;
        case "image/gif":
            matched = true;
            break;
        case "image/webp":
            matched = true;
            break;
        case "image/svg+xml":
            matched = true;
            break;
        case "image/*":
            matched = true;
            break;
        case "application/pdf":
            matched = true;
            break;
        case "text/plain":
            matched = true;
            break;
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
            matched = true;
            break;
        case "application/vnd.ms-excel":
            matched = true;
            break;
        case "application/msword":
            matched = true;
            break;
        case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
            matched = true;
            break;
        case "application/vnd.ms-powerpoint":
            matched = true;
            break;
    }
    return matched;
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

// async function checkNewUserNotificationFlag(clientId) {
//     console.log("checkNewUserNotificationFlag", clientId)
//     let clientData = await db.Client.findOne({ "clientId": clientId })
//     if (clientData) {
//         if (clientData.emailNotification == 1) {
//             console.log("client email", clientData.email)
//             email = clientData.email
//             let html = '<div style="width:400px;border:thin solid #dadce0;border-radius:10px;padding:5px 0px" align="center" ><h2 style="color:blue">Hello  ' + clientData.name + '</h2><h2 style="color:blue">New user is interacting on chabot</h2></div>';;
//             sendMail(email, html, "New client");
//         } else if (clientData.whatsappNotification) {
//             phone = clientData.phone
//                 //send notification
//         } else {
//             return
//         }

//     } else {
//         console.log("No client available having clientId = ", clientId)
//     }
// }

async function checkNewUserNotificationFlag(clientId, userId) {
try{
    console.log("checkNewUserNotificationFlag", clientId)
    let clientData = await db.Client.findOne({ "clientId": clientId })
    if (clientData) {
        if (clientData.emailNotification == 1) {
            console.log("client email", clientData.email)
            email = clientData.email
            let html = '<div style="width:400px;border:thin solid #dadce0;border-radius:10px;padding:5px 0px" align="center" ><h2 style="color:blue">Hello  ' + clientData.name + '</h2><h2 style="color:blue">New user is interacting on chabot</h2></div>';;
            sendMail(email, html, "New client");
        } else if (clientData.whatsappNotification == true) {
            phone = clientData.phone
            let data = {
                clientId: clientId,
                nodeName: "yubo_new_user_start"
            }
            io.sockets.to(userId).emit('yubo_new_user_start', data);
        } else {
            return
        }
    } else {
        console.log("No client available having clientId = ", clientId)
    }
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}


module.exports = bot;