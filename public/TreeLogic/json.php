<?php
$json = '{
  "operators": {
    "operator1": {
      "top": 0,
      "left": 460,
      "node_type": "general",
      "properties": {
        "title": "Web App",
        "question": "Sure I can help you on this, These are the services we provide. Please choose one of the following",
        "type_opt": false,
        "inputs": {},
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "I want a new website design"
          },
          "output_2": {
            "label": "O2",
            "answer": "I want to update my existing website design"
          },
          "output_3": {
            "label": "O3",
            "answer": "Its not about website"
          }
        }
      }
    },
    "operator2": {
      "top": 140,
      "left": 60,
      "node_type": "general",
      "properties": {
        "title": "New_website_genre",
        "question": "Ill help you with this. Can you please tell the basic genre or domain of your webapp/website?",
        "type_opt": false,
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Business"
          },
          "output_2": {
            "label": "O2",
            "answer": "Education"
          },
          "output_3": {
            "label": "O3",
            "answer": "Sports"
          },
          "output_4": {
            "label": "O4",
            "answer": "Health"
          },
          "output_5": {
            "label": "O5",
            "answer": "News"
          },
          "output_6": {
            "label": "O6",
            "answer": "Hospitality"
          },
          "output_7": {
            "label": "O7",
            "answer": "Gaming"
          },
          "output_8": {
            "label": "O8",
            "answer": "Social"
          },
          "output_9": {
            "label": "O9",
            "answer": "Other"
          }
        }
      }
    },
    "operator3": {
      "top": 140,
      "left": 390,
      "node_type": "general",
      "properties": {
        "title": "Update_website_link",
        "question": "Ok. Can\'t you please share the link of your existing website?",
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Sure! Let me type it in..."
          },
          "output_2": {
            "label": "O2",
            "answer": "Duh.. I dont have it"
          }
        },
        "type_opt": false
      }
    },
    "operator4": {
      "top": 150,
      "left": 560,
      "node_type": "leaf",
      "properties": {
        "title": "Not about website",
        "question": "Oh ok. Can you please rephrase your query then?",
        "type_opt": true,
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {}
      }
    },
    "operator5": {
      "top": 310,
      "left": 140,
      "node_type": "general",
      "properties": {
        "title": "any_ref_in_mind",
        "question": "Ok. Do you have any similar or reference website in mind?",
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Sure! Let me type it in..."
          },
          "output_2": {
            "label": "O2",
            "answer": "Nah.. Cant think of any."
          }
        },
        "type_opt": false
      }
    },
    "operator6": {
      "top": 450,
      "left": 210,
      "node_type": "general",
      "properties": {
        "title": "How_soon",
        "question": "Ok. How soon do you plan to start working on it?",
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Immediately"
          },
          "output_2": {
            "label": "O2",
            "answer": "Within next 20 days"
          },
          "output_3": {
            "label": "O3",
            "answer": "i am just exploring right now"
          }
        },
        "type_opt": false
      }
    },
    "operator7": {
      "top": 590,
      "left": 210,
      "node_type": "general",
      "properties": {
        "title": "Get_back",
        "question": "Thanks a lot for the information! \\n\\nWould you like our experts to get back to you for serving you better on this requirement?",
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Yes, that will be helpful"
          },
          "output_2": {
            "label": "O2",
            "answer": "No, I am good for now"
          }
        },
        "type_opt": false
      }
    },
    "operator8": {
      "top": 730,
      "left": 110,
      "node_type": "phone_check",
      "properties": {
        "title": "Phone",
        "question": "Great! Please type in your mobile number below.",
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Phone Number is correct"
          },
          "output_2": {
            "label": "O2",
            "answer": "Phone Number is wrong"
          }
        },
        "type_opt": true
      }
    },
    "operator10": {
      "top": 860,
      "left": 210,
      "node_type": "general",
      "properties": {
        "title": "Phone_again",
        "question": "Hmm... This phone number doesnt seem right. Would you like to correct it?",
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Yes, please let me correct it"
          },
          "output_2": {
            "label": "O2",
            "answer": "I am sure I have typed a correct phone number"
          }
        },
        "type_opt": false
      }
    },
    "operator11": {
      "top": 1000,
      "left": 80,
      "node_type": "email_check",
      "properties": {
        "title": "Email",
        "question": "Thanks! Please type in your email id below and it will be done.",
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Email is Correct"
          },
          "output_2": {
            "label": "O2",
            "answer": "Email is not Correct"
          }
        },
        "type_opt": true
      }
    },
    "operator12": {
      "top": 1150,
      "left": 260,
      "node_type": "general",
      "properties": {
        "title": "Email_again",
        "question": "Hmm... This email id doesn3t seem right. Would you like to correct it?",
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Yes, please let me correct it"
          },
          "output_2": {
            "label": "O2",
            "answer": "I am sure I have typed a correct email id"
          }
        },
        "type_opt": false
      }
    },
    "operator13": {
      "top": 1430,
      "left": 30,
      "node_type": "leaf",
      "properties": {
        "title": "Thanks_after_contact",
        "question": "Thanks a lot for chatting.",
        "type_opt": true,
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {}
      }
    },
    "operator14": {
      "top": 740,
      "left": 290,
      "node_type": "leaf",
      "properties": {
        "title": "Thanks_wo_contact",
        "question": "Thanks a lot for chatting. Hope to see you back soon :)",
        "type_opt": true,
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {}
      }
    },
    "operator15": {
      "top": 310,
      "left": 320,
      "node_type": "general",
      "properties": {
        "title": "Pls_type",
        "question": "Great! Please go ahead.",
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Test"
          }
        },
        "type_opt": true
      }
    },
    "operator16": {
      "top": 1320,
      "left": 290,
      "node_type": "general",
      "properties": {
        "title": "Type_email_again",
        "question": "Ok! Please type your correct email id...",
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Test"
          }
        },
        "type_opt": true
      }
    },
    "operator17": {
      "top": 1000,
      "left": 270,
      "node_type": "general",
      "properties": {
        "title": "Type_phone_again",
        "question": "Ok! Please type your correct phone number...",
        "inputs": {
          "input_1": {
            "label": "In1"
          }
        },
        "outputs": {
          "output_1": {
            "label": "O1",
            "answer": "Test"
          }
        },
        "type_opt": true
      }
    }
  },
  "links": {
    "0": {
      "fromOperator": "operator1",
      "fromConnector": "output_1",
      "toOperator": "operator2",
      "toConnector": "input_1"
    },
    "1": {
      "fromOperator": "operator1",
      "fromConnector": "output_2",
      "fromSubConnector": 0,
      "toOperator": "operator3",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "2": {
      "fromOperator": "operator1",
      "fromConnector": "output_3",
      "fromSubConnector": 0,
      "toOperator": "operator4",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "3": {
      "fromOperator": "operator2",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator5",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "4": {
      "fromOperator": "operator2",
      "fromConnector": "output_2",
      "fromSubConnector": 0,
      "toOperator": "operator5",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "5": {
      "fromOperator": "operator2",
      "fromConnector": "output_3",
      "fromSubConnector": 0,
      "toOperator": "operator5",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "6": {
      "fromOperator": "operator2",
      "fromConnector": "output_4",
      "fromSubConnector": 0,
      "toOperator": "operator5",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "7": {
      "fromOperator": "operator2",
      "fromConnector": "output_9",
      "fromSubConnector": 0,
      "toOperator": "operator5",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "8": {
      "fromOperator": "operator2",
      "fromConnector": "output_8",
      "fromSubConnector": 0,
      "toOperator": "operator5",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "9": {
      "fromOperator": "operator2",
      "fromConnector": "output_7",
      "fromSubConnector": 0,
      "toOperator": "operator5",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "10": {
      "fromOperator": "operator2",
      "fromConnector": "output_6",
      "fromSubConnector": 0,
      "toOperator": "operator5",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "11": {
      "fromOperator": "operator2",
      "fromConnector": "output_5",
      "fromSubConnector": 0,
      "toOperator": "operator5",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "12": {
      "fromOperator": "operator5",
      "fromConnector": "output_2",
      "fromSubConnector": 0,
      "toOperator": "operator6",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "13": {
      "fromOperator": "operator6",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator7",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "14": {
      "fromOperator": "operator6",
      "fromConnector": "output_2",
      "fromSubConnector": 0,
      "toOperator": "operator7",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "15": {
      "fromOperator": "operator6",
      "fromConnector": "output_3",
      "fromSubConnector": 0,
      "toOperator": "operator7",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "16": {
      "fromOperator": "operator7",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator8",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "17": {
      "fromOperator": "operator8",
      "fromConnector": "output_2",
      "fromSubConnector": 0,
      "toOperator": "operator10",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "18": {
      "fromOperator": "operator8",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator11",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "19": {
      "fromOperator": "operator10",
      "fromConnector": "output_2",
      "fromSubConnector": 0,
      "toOperator": "operator11",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "20": {
      "fromOperator": "operator11",
      "fromConnector": "output_2",
      "fromSubConnector": 0,
      "toOperator": "operator12",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "21": {
      "fromOperator": "operator11",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator13",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "22": {
      "fromOperator": "operator12",
      "fromConnector": "output_2",
      "fromSubConnector": 0,
      "toOperator": "operator13",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "23": {
      "fromOperator": "operator7",
      "fromConnector": "output_2",
      "fromSubConnector": 0,
      "toOperator": "operator14",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "24": {
      "fromOperator": "operator5",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator15",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "25": {
      "fromOperator": "operator3",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator15",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "26": {
      "fromOperator": "operator15",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator6",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "27": {
      "fromOperator": "operator12",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator16",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "28": {
      "fromOperator": "operator16",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator13",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "29": {
      "fromOperator": "operator10",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator17",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "30": {
      "fromOperator": "operator17",
      "fromConnector": "output_1",
      "fromSubConnector": 0,
      "toOperator": "operator13",
      "toConnector": "input_1",
      "toSubConnector": 0
    },
    "31": {
      "fromOperator": "operator3",
      "fromConnector": "output_2",
      "fromSubConnector": 0,
      "toOperator": "operator2",
      "toConnector": "input_1",
      "toSubConnector": 0
    }
  },
  "settings": {
    "canvasHeight": "1520",
    "canvasWidth": "1108",
    "maxFields": "20"
  },
  "operatorTypes": {}
}';

    $jsonData  = json_decode($json);
    $newJson   = [];
    $finalJson = [];
    $options   = [];
    $links     = [];
    $nodeType  = '';
    $i         = 0;

    
    /** Converting Decision Tree Default JSON formate Into Uygasa's JSON formate */

    foreach ($jsonData->operators as $key => $json) {
    
        $nodeId = substr($key, -1);
        $newJson['DTree'][$key]['node_id']        = str_replace('operator', 'n', $key);
        $newJson['DTree'][$key]['node_name']      = $json->properties->title;
        $newJson['DTree'][$key]['text']           = $json->properties->question;
        $newJson['DTree'][$key]['type_of_node']   = $json->node_type;
        $newJson['DTree'][$key]['type_opt']       = $json->properties->type_opt == true ? $json->properties->type_opt : false;

        foreach ($json->properties->outputs as $outputKey => $output) {
            $options[$outputKey]['option'] = $output->answer;
        }

        $newJson['DTree'][$key]['options']        = $options;
        $options  = [];

        $i++;
    }
    
    //** Adding Links Information in JSON  */
    foreach ($jsonData->links as $link) {
        $newJson['DTree'][$link->fromOperator]['options'][$link->fromConnector]['link'] = str_replace('operator', 'n', $link->toOperator);
    }

    /** Generating the final formate of JSON */

    $i = 0;
    foreach ($newJson['DTree'] as $operator) {

        $finalJson['DTree'][$i]['node_id']      = $operator['node_id'];
        $finalJson['DTree'][$i]['node_name']    = $operator['node_name'];
        $finalJson['DTree'][$i]['text']         = $operator['text'];
        $finalJson['DTree'][$i]['type_of_node'] = $operator['type_of_node'];
        $finalJson['DTree'][$i]['type_opt']     = $operator['type_opt'];
        $finalJson['DTree'][$i]['default']      = 'false';

        foreach($operator['options'] as $option) {
           $options[] = $option;
           $links[]   = key_exists('link', $option) ? $option['link'] : null;
        }

        $finalJson['DTree'][$i]['options']  = $options;

        if (count(array_unique($links)) != 1 && ($operator['type_of_node'] == 'general' || $operator['type_of_node'] == 'root' )) {
            $finalJson['DTree'][$i]['type_of_node'] = 'multi_child';
        } else {
            $finalJson['DTree'][$i]['default']      = array_unique($links)[0] != null ? array_unique($links)[0] : 'false';
        }

        if ($links[0] == null && $operator['type_of_node'] == 'leaf') {
            $finalJson['DTree'][$i]['type_of_node'] = 'leaf_node';
            $finalJson['DTree'][$i]['options'] = [];
        } else {
            $finalJson['DTree'][$i]['type_of_node'] = $operator['type_of_node'];
            //$finalJson['DTree'][$i]['options'] = [];
        }

        $links   = [];
        $options = [];
        $i++;
    }
    echo "<pre>";
    //print_r($finalJson['DTree']);die;
    var_dump(json_encode(array_values($finalJson['DTree'])));die;