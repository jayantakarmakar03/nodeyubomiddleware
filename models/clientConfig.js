var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
    mongoose.connect(require('./connection-string'));
}
var newSchema = new Schema({
    'clientId': { type: String },
    'clientObjId': {type: Schema.Types.ObjectId},
    'cronStatus': { type: Boolean,default:false },
    'resetContext': { type: Boolean,default:false },
    'triggerNode': { type: Boolean },
    'nodeName': { type: String ,default: null},
    "InactivityTime": { type: Number ,default:00},
    'timerDetails': { 
        hours : {
            type: Number
        },
        minutes : {
            type: Number
        },
        seconds : {
            type: Number
        }
    },
});

newSchema.pre('save', function(next) {
    this.updatedAt = Date.now();
    next();
});
newSchema.pre('update', function() {
    this.update({}, { $set: { updatedAt: Date.now() } });
});
newSchema.pre('findOneAndUpdate', function() {
    this.update({}, { $set: { updatedAt: Date.now() } });
});
module.exports = mongoose.model('clientConfig', newSchema);