const mongoose = require("mongoose");
Schema = mongoose.Schema 

const TransactionSchema = new Schema({
    walletClientId:{
        type:String,
        ref:'wallet'
    },
    order_id:{
        type:String
    },
    amount: {
            type: Number,
            required: true,
            default: 0.00
        },
    balanceBefore:{
        type: mongoose.Decimal128,
    },
    transactionType:{
        type:String,
        enum:["CREDIT",'DEBIT']
    },
    balanceAfter:{
        type: mongoose.Decimal128,
    },
    status:{
        type:String,
        enum:['PENDING','SUCCESS','FAILED'],
        default:'PENDING'
    },
    'createdAt': { type: Date, default: Date.now },
    'updatedAt': { type: Date, default: Date.now }
});

const Transaction = mongoose.model("Transaction",TransactionSchema);
module.exports = Transaction