const db = require("../models/all-models");
const axios = require('axios');

module.exports = {
    getCodes: getCodes,
    generateCode: generateCode,
    saveCodes: saveCodes,
    codeNodeFunctions: codeNodeFunctions,
    deleteCodeNode: deleteCodeNode,
    getActionDetails: getActionDetails,
    activateCodeNodeLog  : activateCodeNodeLog,
    getCodeNodeLogs      : getCodeNodeLogs,
    saveCodeNodeErrorLog : saveCodeNodeErrorLog,
    removeCodeNodeLogs   : removeCodeNodeLogs
}

async function getCodes(req, res) {
    console.log("REQ.BODY", req.body)
    let clientId
    try {
        if (req.body.clientId) {
            clientId = req.body.clientId
        } else {
            clientId = req.user.clientId
        }
        let codedata = await db.codenode.aggregate([{
            $match: {
                $and: [{ clientId: clientId }, {$or: [{ nodeID: req.body.nodeID },{ nodeTitle: req.body.nodeID }]}]
            }
        }])
        if (codedata.length > 0) {
            console.log("codedata", codedata)
            return res.json({
                statusCode: 200,
                status: true,
                message: "Data fetch successfully",
                data: codedata[0]
            });
        } else {
            return res.json({
                statusCode: 404,
                status: false,
                message: "Operator not found"
            });
        }
    } catch (err) {
        console.log('lll', err)
        return res.json({
            statusCode: 201,
            status: false,
            message: "Error on catch block",
            error: err
        });
    }
}

async function generateCode(req, res) {
    let clientId
    try {
        if (req.body.clientId) {
            clientId = req.body.clientId
        } else {
            clientId = req.user.clientId
        }

        let data = JSON.stringify({
          "client_id": clientId,
          "code_instructions": req.body.code_instructions
        });

        let config = {
          method: 'post',
          maxBodyLength: Infinity,
          url: process.env.codeGenerationAPI,
          headers: { 
            'Content-Type': 'application/json'
          },
          data : data
        };

        axios.request(config)
        .then((response) => {
          console.log(JSON.stringify(response.data));
          return res.json({
                statusCode: 200,
                status: true,
                message: "Code generated successfully",
                data: response.data
            });
        })
        .catch((error) => {
          console.log(error);
          return res.json({
                statusCode: 404,
                status: false,
                message: "Some error occured, please try again."
            });
        });
    } catch (err) {
        console.log('lll', err)
        return res.json({
            statusCode: 201,
            status: false,
            message: "Error on catch block",
            error: err
        });
    }
}

async function saveCodes(req, res) {
    try {
        // let exixtingCode = await db.codenode.find({ $and: [{ clientId: req.user.clientId }, { nodeID: req.body.nodeID }] });
        let existingCode = await db.codenode.aggregate([{
            $match: {
                $and: [{ clientId: req.user.clientId }, { nodeID: req.body.nodeID }]
            }
        }])
        console.log("existingCode", existingCode)
        if (existingCode.length > 0) {
            let updateData = await db.codenode.updateOne({ _id: existingCode[0]._id }, {
                $set: {
                    code: req.body.code,
                    nodeTitle: req.body.nodeTitle,
                }
            });
            console.log(" updateData ", updateData)
            return res.json({
                statusCode: 200,
                status: true,
                message: "code saved successfully"
            });

        } else {
            console.log("In else loop new node generated")
            console.log('body ', req.body)
            let codedata = new db.codenode({
                clientId: req.user.clientId,
                nodeTitle: req.body.nodeTitle,
                code: req.body.code,
                nodeID: req.body.nodeID
            });
            console.log("CODE DATA", codedata)
            await codedata.save();
            return res.json({
                statusCode: 200,
                status: true,
                message: "code saved successfully"
            });
        }
    } catch (err) {
        console.log('ERROR', err)
        return res.json({
            statusCode: 201,
            status: false,
            message: "Error on catch block",
            error: err
        });
    }
}

async function codeNodeFunctions(req, res) {
    console.log("REQ.BODY", req.body)

    //parse the object
    return res.json({
        statusCode: 200,
        status: true,
        message: "code saved successfully",
        data: { text: "Hello" }
    });
}

async function deleteCodeNode(req, res) {
    try {
        db.codenode.deleteOne({ clientId: req.user.clientId, nodeID: req.body.nodeID }, async function(error, result) {
            if (error) {
                return res.json({
                    statusCode: 200,
                    status: true,
                    message: "Code node successfully"
                });
            } else {
                return res.json({
                    statusCode: 400,
                    status: true,
                    message: "Error while deleting code node"
                });
            }
        })
    } catch (err) {
        console.log('ERROR', err)
        return res.json({
            statusCode: 201,
            status: false,
            message: "Error on catch block",
            error: err
        });
    }
}


async function getActionDetails(req, res) {
    if (!req.body.clientId) {
        return res.redirect("/login");
    } else {
        if (req.body.actionName) {
            let actionData = await db.Actions.find({
                clientId: req.body.clientId,
                actionName: { $regex: req.body.actionName, $options: "$i" },
            }, { _id: 0, createdAt: 0, updatedAt: 0, __v: 0, clientId: 0 });

            if (actionData.length > 0) {
                return res.json({
                    statusCode: 200,
                    status: "success",
                    data: actionData[0],
                    message: "Data found successfully"
                })
            } else {
                return res.json({
                    statusCode: 400,
                    status: "failed",
                    message: "No data found"
                })
            }
        } else {
            return res.json({
                statusCode: 400,
                status: "failed",
                message: "Action name is required to get the list."
            })
        }
    }
}

/************************************************************************
  save code node error log for particular user
 *************************************************************************/
  async function saveCodeNodeErrorLog(req,res){
    try{

            let userInfo = await new db.codeNodeLogs({
                clientId        : req.body.clientId,
                errorData       : req.body.errorData,
                nodeName        : req.body.nodeName,
                lineNumber      : req.body.lineNumber,
                userId          : req.body.userId,
                createdAt       : new Date()
            });
            let savedata =   await userInfo.save(); 
            return res.json({
                statusCode: 200,
                status: true,
                data: savedata,
                message: "Data save successfully"
            })
    }catch(error){
            console.log('error in catch block ', error)
    }
  

}
/************************************************************************
  Active code not log for a particular user 
 *************************************************************************/
async function activateCodeNodeLog(req,res){

    try{
        let status = req.body.status;
        if(status == "false" || status == false){
            req.body.userId = "";
        }
        if((status == "true" || status == true ) && req.body.userId ==""){
            return res.json({
                statusCode: 201,
                status: false,
                message: "Please provide userid to activate codenode logs",
            });

        }
        let updateStatus = await db.Client.updateOne(
            { "clientId" : req.body.clientId },
            {
                $set:
                {
                    codeNodeStatus :{
                        "status" : req.body.status,
                        "userId" : req.body.userId
                    }
                }
            }); 
            return res.json({
                statusCode : 200,
                status     : true,
                message    : "Status change successfully",
                data       : updateStatus
            });
    }catch(error){
            console.log('error in catch block ', error);
            return res.json({
                statusCode: 201,
                status: false,
                message: "Error on catch block",
                error: error
            });
    }
}
/************************************************************************
  Get the list code node logs
 *************************************************************************/
async function getCodeNodeLogs(req,res){
        try{
           
          let search = `${req.body.searchItem}`
           if(req.body.skip == undefined ){
              req.body.skip = 0;
           }else{
              req.body.skip = parseInt(req.body.skip);
           }
           if(req.body.limit == undefined ){
              req.body.limit = 50;
           }else{
              req.body.limit = parseInt(req.body.limit);
           }

            let docCount = await db.codeNodeLogs.count({
                clientId: req.body.clientId,
                $or: [
                        {
                            "nodeName":{$regex: search ,'$options': 'i'}
                        },
                        {
                            "userId":{$regex: search ,'$options': 'i'}
                        }, 
                        {
                            "clientId":{$regex: search ,'$options': 'i'}
                        }
                ]
            });

            let codeNodeLogList = await db.codeNodeLogs.find({
                   clientId: req.body.clientId,
                    $or: [
                            {
                                "nodeName":{$regex: search ,'$options': 'i'}
                            },
                            {
                                "userId":{$regex: search ,'$options': 'i'}
                            }, 
                            {
                                "clientId":{$regex: search ,'$options': 'i'}
                            }
                    ]
                }, {  clientId: 1, errorData: 1, nodeName: 1 , userId :1,createdAt:1 }).sort({createdAt : -1}).skip(req.body.skip).limit(req.body.limit);

            return res.json({
                    statusCode : 200,
                    status     : true,
                    data       : codeNodeLogList,
                    limit      : req.body.limit,
                    skip       : req.body.skip,
                    docCount   : docCount
                })
        }catch(error){
            console.log('error',error)
        }
    
}
/************************************************************************
  Remove code node log
 *************************************************************************/
  async function removeCodeNodeLogs(req,res){
    try{
         let userInfo = await db.codeNodeLogs.remove({
            clientId        : req.body.clientId
        });
        return res.json({
                statusCode : 200,
                status     : true,
                data       : userInfo,
                message    : "Codenode log cleared successfully" 
            })
    }catch(error){
        console.log('error',error)
    }

}
