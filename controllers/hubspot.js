let axios = require("axios");

module.exports = {
    createNewCntctOffline: createNewCntctOffline,
    updateCntctOfflineByVid: updateCntctOfflineByVid
  }
  
//Create new record, email property is mandatory
async function createNewCntctOffline(apiKey, data){
    var resp = false;
    try {
        await axios({
          method: "POST",
          url: `https://api.hubapi.com/contacts/v1/contact/?hapikey=${apiKey}`,
          data: data.data,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': apiKey
          }
        }).then(async function (response) {
          console.log("resp " + response.status);
          resp = response.data ? response.data : false
        }).catch(async function (err) {
          console.log("err " + err)
        });
      } catch (err) {
        console.log("error " + err);
      }

      return resp
}

//Update the record based on visitor's id
async function updateCntctOfflineByVid(apiKey, data){
    try {
        await axios({
          method: "POST",
          url: `https://api.hubapi.com/contacts/v1/contact/vid/${data.visitor_id}/profile?hapikey=${apiKey}`,
          data: data.data,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': apiKey
          }
        }).then(async function (response) {
          console.log("resp " + response.status);
        }).catch(async function (err) {
          console.log("err " + err)
        });
      } catch (err) {
        console.log("error " + err);
      }
}