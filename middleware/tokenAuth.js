const config = require("../config/config"),
  jwt = require("jsonwebtoken"),
  crypto = require("crypto");

module.exports = {
  encryptString: encryptString,
  decryptString: decryptString,
  sign_jwt: sign_jwt,
  verify_jwt: verify_jwt,
};

function encryptString(data) {
  const cipher = crypto.createCipher(
    config.encryption.ALGORITHM,
    config.encryption.PASSWORD
  );
  let crypted = cipher.update(data, "utf8", "hex");
  crypted += cipher.final("hex");
  return crypted;
}

function decryptString(data) {
  const decipher = crypto.createDecipher(
    config.encryption.ALGORITHM,
    config.encryption.PASSWORD
  );
  let dec = decipher.update(data, "hex", "utf8");
  dec += decipher.final("utf8");
  return dec;
}

//JWT sign token
async function sign_jwt(user, expiryTime) {
  const token = jwt.sign({ user: user }, process.env.SECRETKEY, {
    expiresIn: expiryTime,
  });
  return token;
}

//JWT verify token
async function verify_jwt(token) {
  const decodedToken = await jwt.verify(token, process.env.SECRETKEY);
  return decodedToken;
}

