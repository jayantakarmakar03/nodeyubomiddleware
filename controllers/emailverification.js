const sendMail = require("../middleware/sendChat.js"),
  tokenAuth = require("../middleware/tokenAuth.js"),
  intentsController = require("./intents.js"),
  treeController = require("./tree.js"),    
  db = require("../models/all-models");

module.exports = {
  sendverificationmail: sendverificationmail,
  resendVerificationMail: resendVerificationMail,
  emailverification: emailverification,
  resetpasswordmail: resetpasswordmail,
  resetAgentpasswordmail:resetAgentpasswordmail,
  updatepasswordmail: updatepasswordmail,
  helpMail: helpMail,
  newSignupNotification: newSignupNotification,
  leadNotificationMail: leadNotificationMail,
  need_assistanceMail: need_assistanceMail,
  deleteAccountRequestUserMail   : deleteAccountRequestUserMail,
  deleteAccountRequestOwnerMail  : deleteAccountRequestOwnerMail,
  sendReminderMailToBasicUser    : sendReminderMailToBasicUser,
  couponAppliedSuccessfully      : couponAppliedSuccessfully
};

//sendmail parameters
async function sendverificationmail(userEmail, token, username) {
  const verificationLink = `${process.env.NODEURL}/emailverification?token=${token}`,
    subject = "Confirm your email",
    titleName = `Hi ${username}, `;
  html = `<div>
    <p style="font-family: Arial, Helvetica, sans-serif">${titleName}<br><br></p>
    <p style="font-family: Arial, Helvetica, sans-serif"><b>Welcome on board.</b></p>
    <p style="font-family: Arial, Helvetica, sans-serif">Thanks for signing up with Yugasa Bot!</p>
    <p style="font-family: Arial, Helvetica, sans-serif">Claim your Yugasa Bot by verifying your email. Please click on the following link to verify your account - <a href=${verificationLink}>Account Verification Link</a></p>
    <p style="font-family: Arial, Helvetica, sans-serif"><b>Please Note: </b>For security purposes, this link will expire in 24 hours.<br></p>
    <p style="font-family: Arial, Helvetica, sans-serif"><br>- Team Yugasa Bot</p>
    </div>`;
  await sendMail(userEmail, html, subject);

  const subject1 = "New User Registered on Yugasa Bot: " + username;
  html1 = `<div>
    <p style="font-family: Arial, Helvetica, sans-serif"><b>Verification email was sent to:</b> ${userEmail}</p>
    <p style="font-family: Arial, Helvetica, sans-serif">Verification Link: ${verificationLink}</p>
    <p style="font-family: Arial, Helvetica, sans-serif"><br>-Yugasa Bot</p>
    </div>`;
  await sendMail('ashish@yugasa.com, vivek@yugasa.com, d.jaggi@helloyubo.com, shobhit@yugasasoftware.com, p.mishra@yugasasoftware.com', html1, subject1);
}

//To resend the verification link, if expired
async function resendVerificationMail(user) {
  let token = await tokenAuth.encryptString(
    `${user.email}&${new Date().getTime() + 24 * 3600 * 1000}`
  );
  await db.Client.updateOne(
    { clientId: user.clientId },
    {
      $set: {
        email_token: token,
      },
    }
  );
  await sendverificationmail(user.email, token, user.clientId);
}

//To verify the user email
async function emailverification(req, res) {
  const token = req.query.token;
  let get_data = await tokenAuth.decryptString(token),
    code = get_data.split("&"),
    timestamp = code[1],
    user = await db.Client.findOne({ email_token: token.toString() });

  if (user) {
    if (user.email_verified || user.email_verified == true) {
      req.flash("success_msg", "Account already verified");
    } else if (new Date().getTime() > parseInt(timestamp, 10)) {
      req.flash(
        "error_msg",
        "Account verification link has expired, we have sent an email to your registered email, please click on the link to verify your account."
      );
    } else {
      await db.Client.updateOne(
        { clientId: user.clientId },
        {
          $set: {
            email_verified: true,
          },
        }
      );
      req.flash("success_msg", "Hurray! Account verified successfully");
      await treeController.saveContactNode(user.clientId); 
      await intentsController.clientJsCssFileCreation(req, res, user.clientId);
      process.env.PRODUCTION == "true" ? await userVerificationNotification(user, user.clientId) : "";
      await registerationConfirmationToUser(user);
    }
  } else {
    req.flash(
      "error_msg",
      "Email verification link has expired, we have sent an email to your registered email, please check the latest email and click on the link to verify your account(check the spam folder also)."
    );
  }
  return res.redirect("/login");
}
//to send reset password mail
async function resetpasswordmail(userEmail, token, username) {
  const resetPswrdLink = `${process.env.NODEURL}/reset?token=${token}`,
    subject = "Yugasa Bot account password reset",
    titleName = `Hi ${username}, `;
  html = `<div>
      <p style="font-family: Arial, Helvetica, sans-serif">${titleName}<br><br></p>
      <h4 style="font-family: Arial, Helvetica, sans-serif">We've received a request to reset your Yugasa Bot account password. If you didn't make the request, you can safely ignore this email</h4><h4reset password link will expire by itself</h4>
      <h4 style="font-family: Arial, Helvetica, sans-serif">
      Otherwise, you can reset your password by clicking on the following link:</h4>
 
   <p><a href=${resetPswrdLink}>Reset Password Link</a></p>
      </div>`;
  await sendMail(userEmail, html, subject);
}
// to reset agent password mail
async function resetAgentpasswordmail(userEmail, token, username,agentEmail) {
  const resetPswrdLink = `${process.env.NODEURL}/resetAgentPassword?token=${token}`,
    subject = "Agent Password Rest",
    titleName = `Hi ${username}, `;
  html = `<div>
      <p style="font-family: Arial, Helvetica, sans-serif">${titleName}<br><br></p>
      <h4 style="font-family: Arial, Helvetica, sans-serif">
        We've received a request from agent ${agentEmail} to reset password.
        </h4>
        <h4>Reset password link will expire by itself within 24 hours</h4>
      <h4 style="font-family: Arial, Helvetica, sans-serif">
      Otherwise, you can reset your password by clicking on the following link:</h4>
 
   <p><a href=${resetPswrdLink}>Reset Password Link</a></p>
      </div>`;
  await sendMail(userEmail, html, subject);
}
//

//to inform user for successfully reset password
async function updatepasswordmail(userEmail, username) {
  subject = "Your password has been reset", titleName = `Hi ${username}, `;
  html = `<div>
      <p style="font-family: Arial, Helvetica, sans-serif">${titleName}<br><br></p>
      <h4 style="font-family: Arial, Helvetica, sans-serif">
      As requested by you, your Yugasa Bot account password has been reset successfully. Please login using your new password.</h4>
      <h4 style="font-family: Arial, Helvetica, sans-serif">   
If this change has not been done by you, please inform us at contact@helloyubo.com from your registered email id</h4>
      </div>`;
  await sendMail(userEmail, html, subject);
}

async function helpMail(req, res) {
  if (req.user) {
    if (req.body.title && req.body.query) {
      const userEmail = "ashish@yugasa.com, vivek@yugasa.com, d.jaggi@helloyubo.com, shobhit@yugasasoftware.com, p.mishra@yugasasoftware.com",
        subject = "Yugasa Bot Admin Help - "+req.user.clientId,
        html = `<div>
    <p style="font-family: Arial, Helvetica, sans-serif">ClientId: ${req.user.clientId}</p>
    <p style="font-family: Arial, Helvetica, sans-serif">Title: ${req.body.title}</p>
    <p style="font-family: Arial, Helvetica, sans-serif">Email: ${req.user.email}</p>
    <p style="font-family: Arial, Helvetica, sans-serif">Phone: ${req.user.phone}</p>
    <p style="font-family: Arial, Helvetica, sans-serif">Query: ${req.body.query}</p>
    </div>`;
      await sendMail(userEmail, html, subject);
      return res.send({
        msg: "Thanks for your query, our team will get back to you soon!",
      });
    } else {
      return res.send({ msg: "empty" });
    }
  } else {
    res.redirect("/login");
  }
}

//Enail notification to admin on signup
async function newSignupNotification(user, username) {
  const subject = "New User Registered on Yugasa Bot: " + username;
  html = `<div>
    <p style="font-family: Arial, Helvetica, sans-serif">Hi Team,<br><br></p>
    <p style="font-family: Arial, Helvetica, sans-serif"><b>We have a new user on board.</b></p>
    <p style="font-family: Arial, Helvetica, sans-serif">Email: ${user.email}</p>
    <p style="font-family: Arial, Helvetica, sans-serif">Phone: ${user.phone}</p>
    <br>
    <p style="font-family: Arial, Helvetica, sans-serif">To trigger verification email <a href="https://admin.helloyubo.com/approveUser?clientId=${username}" target="_blank">Click Here</a></p>
    <br>
    <p style="font-family: Arial, Helvetica, sans-serif"><br>-Yugasa Bot</p>
    </div>`;
  await sendMail('ashish@yugasa.com, vivek@yugasa.com, d.jaggi@helloyubo.com, shobhit@yugasasoftware.com, p.mishra@yugasasoftware.com', html, subject);
}

// Enail notification to admin on verification  //Email id's to be taken from superadmin(Reminder)
async function userVerificationNotification(user, username) {
  const subject = "New User Registered on Yugasa Bot: " + username,
  html = `<div>
    <p style="font-family: Arial, Helvetica, sans-serif"><b>User verification complete.</b></p>
    <p style="font-family: Arial, Helvetica, sans-serif">Email: ${user.email}</p>
    <p style="font-family: Arial, Helvetica, sans-serif">Phone: ${user.phone}</p>
    <p style="font-family: Arial, Helvetica, sans-serif"><br>-Yugasa Bot</p>
    </div>`;
  await sendMail(
    "ashish@yugasa.com, d.jaggi@helloyubo.com, vivek@yugasa.com, shobhit@yugasasoftware.com, p.mishra@yugasasoftware.com",
    html,
    subject
  );
}

//Yugasa Bot registeration confirmation to user
async function registerationConfirmationToUser(user) {
  const subject = "Welcome! Lets get your ChatBot up and running.",
  html = `<div>
  <p style="font-family: Arial, Helvetica, sans-serif">Dear Yugasa Bot User,<br><br></p>
  <p style="font-family: Arial, Helvetica, sans-serif">
  Thank you for showing your interest in Yugasa Bot and envisioning/considering it for your business needs. We congratulate you and are glad that you took this decision of being ahead of your competition with the power of AI. 
  </p>
  <p style="font-family: Arial, Helvetica, sans-serif">   
  Here is the playlist of some 'getting started' help videos for getting Yugasa Bot working for you.
</p>
<p><b>Yugasa Bot Help Videos:</b><br><a href="https://www.youtube.com/playlist?list=PL9I218ybvv6Tj9Tmkk8BK6vhcuBHF15aX">https://www.youtube.com/playlist?list=PL9I218ybvv6Tj9Tmkk8BK6vhcuBHF15aX</a></p>
<p>Checkout the 'Help' section of your Admin Panel for more upcoming tutorial videos.</p><br>
<p>--</p>
<p>Team Yugasa Bot</p>
<p><a href="www.helloyubo.com">www.helloyubo.com</a></p>
  </div>`
  // attachments = `[{
  //   filename: "Yugasa Bot Free Registeration Help",
  //   path: "https://www.youtube.com/watch?v=D1wBpInYzvg"
  // },
  // {
  //   filename: "Yugasa Bot Intents Help"
  //   path: "https://www.youtube.com/watch?v=NY4CKkCRx4o"
  // },
  // {
  //   filename: "Yugasa Bot Fallbacks Help",
  //   path: "https://www.youtube.com/watch?v=8aZ36hSenBY"
  // },
  // {
  //   filename: "Setting Help to Customize Yugasa Bot Chat Window"
  //   path: "https://www.youtube.com/watch?v=shbfENvPdXY"
  // }]`
  await sendMail(user.email, html, subject);
}

async function leadNotificationMail(clientEmail, userdetails, username) {
  username = username ? username : "unknown";
  const subject = `Yugasa Bot has generated a new lead for you - ${username}`;
  html = `<div>
    <p style="font-family: Arial, Helvetica, sans-serif">Hi Team,<br><br></p>
    <p style="font-family: Arial, Helvetica, sans-serif"><b>Yugasa Bot has generated a new lead for you. Please find details below:</b></p>
    <p style="font-family: Arial, Helvetica, sans-serif">${userdetails}</p>
    <p style="font-family: Arial, Helvetica, sans-serif"><br>-Yugasa Bot</p>
    </div>`;
  await sendMail(clientEmail, html, subject);
}

async function need_assistanceMail(clientDetails){
   const subject = "New User need assistance for verification on Yugasa Bot: " + clientDetails.clientId,
   html = `<div>
     <p style="font-family: Arial, Helvetica, sans-serif"><b>Yugasa bot user need assistance in verification process. Please see the below details:</b></p>
     <p style="font-family: Arial, Helvetica, sans-serif">Email: ${clientDetails.email}</p>
     <p style="font-family: Arial, Helvetica, sans-serif">Phone: ${clientDetails.phone}</p>
     <p style="font-family: Arial, Helvetica, sans-serif"><br>-Yugasa Bot</p>
     </div>`;
   await sendMail(
     "ashish@yugasa.com, d.jaggi@helloyubo.com, vivek@yugasa.com, shobhit@yugasasoftware.com, p.mishra@yugasasoftware.com",
     html,
     subject
   );
   return
}


/**************************************************************************************************
 * Delete request mail sent to user 
 ***************************************************************************************************/
async function deleteAccountRequestUserMail(userEmail, username) {
  subject = "Acount delete request received",
    html = `<div style=" padding:10px; text-align: left; color:black;background:rgb(209, 153, 245)">
                <p>
                <strong>  Hello, ${username} <br> </strong>
                We have received your request to delete your account .We need 3 to 4 working days to process your request. <br/>
                Once deletion process is done we will let you know by mail. <br/>
                <strong> Thanks! <br/> 
                  Yugasa Team </strong>
                </p>
            </div>`;
    await sendMail(userEmail, html, subject);
}
/**************************************************************************************************
 * Delete request mail to owner 
 ***************************************************************************************************/
async function deleteAccountRequestOwnerMail(userEmail, username) {
  let adminEmail = "ashish@yugasa.com, d.jaggi@helloyubo.com, vivek@yugasa.com, shobhit@yugasasoftware.com, p.mishra@yugasasoftware.com";
  subject = "Acount delete request received",
    html = `<div style="padding:10px; text-align: left; color:black;background:rgb(209, 153, 245)">
                <p>
                <strong> Hello, Admin <br> </strong>
                You got an account deletion request from user  <strong> ${username}</strong> registered details are given below <br>  
                <strong>  User Name  </strong>: ${username} , <br>  
                <strong>  Mail Id : </strong> ${userEmail}  <br>    
                 Please take the further steps of account deletion . <br>    
                  <strong> 
                    Thanks! <br/>
                    Yugasa Team 
                  </strong>
                </p>
            </div>`;
            
    await sendMail(adminEmail, html, subject);
}

/**************************************************************************************************
 * send mail when coupon code successfully applied
 ***************************************************************************************************/
async function couponAppliedSuccessfully(data) {
  try{
     if (data) {
        if (data.email ) {
          console.log("email sent = ", data.email)
            const userEmail = data.email,
              subject = "Yugasa Bot | Coupon Applied Successfully",
              html = `<body style="font-family: Arial, sans-serif;background-color: #f4f4f4;margin: 0;padding: 0;">
              <div  style ="padding: 20px;margin: 50px auto;border: 1px solid #ccc;max-width: 400px;box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);">
                <h1>Coupon Applied Successfully!</h1>
                <p>Dear  ${data.clientId},</p>
                <p>You can use our chatbot on your website now. To know more login to yugasa admin panel and watch the videos available in help section. </p>
                <a href="https://admin.helloyubo.com/">Login Here</a>
                <p>If you have any questions or need assistance, please contact our support team at 
                <a href="mailto:contact@helloyubo.com">contact@helloyubo.com</a>.</p>
                <br>
                <strong>  Thanks! <br/> Yugasa Team  </strong>
              </div>
            </body>`;
            await sendMail(userEmail, html, subject);
            return ({message : "Email send successfully"});

          } else {
            return 1;
          }
      } else {
        console.log('Mail not send due to having insufficient data-',data)
        return 1;
      }
  }catch(err){
    console.log('Error in catch block:', err)
  }

}
/**************************************************************************************************
 * send reminder mail to basic user
 ***************************************************************************************************/
async function sendReminderMailToBasicUser(data) {
  try{
     if (data) {
        if (data.email ) {
          console.log("email sent = ", data.email)
            const userEmail = data.email,
              subject = "Yugasa Bot | Subscription expired reminder",
              html = `<body style="font-family: Arial, sans-serif;background-color: #f4f4f4;margin: 0;padding: 0;">
              <div  style ="padding: 20px;margin: 50px auto;border: 1px solid #ccc;max-width: 400px;box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);">
                <h1>Your Subscription Has Ended</h1>
                <p>Dear  ${data.clientId},</p>
                <p>Your Yugasa Bot subscription has expired, and access to our premium content is no longer available.</p>
                <h2>Renew Your Subscription</h2>
                <p>To continue enjoying our services, please renew your subscription by making a payment. Please login on our admin panle to purchase a plan.</p>
                <a href="https://admin.helloyubo.com/login" style="text-decoration: none; background-color: #007BFF; color: #fff; padding: 10px 20px; border-radius: 5px; font-weight: bold;">
                   Go to yugasa admin page 
                </a>
                <p>If you have any questions or need assistance, please contact our support team at 
                <a href="mailto:contact@helloyubo.com">contact@helloyubo.com</a>.</p>
                <br>
                <strong>  Thanks! <br/> Yugasa Team  </strong>
              </div>
            </body>`;
            await sendMail(userEmail, html, subject);
            return ({message : "Email send successfully"});

          } else {
            return 1;
          }
      } else {
        console.log('Mail not send due to having insufficient data-',data)
        return 1;
      }
  }catch(err){
    console.log('Error in catch block:', err)
  }

}
