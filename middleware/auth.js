const jwt = require('jsonwebtoken');
const db = require('../models/all-models');
module.exports=async(req, res, next) => {
let authHeader = req.get('Authorization');
if (!authHeader){
	return res.status(422).json({
	  status:'failed',
	  message:'Not Authenticated!'
	});
}
const token = authHeader.split(' ')[1];
let decodedToken;
try{
  decodedToken = jwt.verify(token, 'YuboSecretKey');
}catch (err){
    return res.status(500).json({
	  status:'failed',
	  message:err
	});
}
let user=await db.Client.findOne({userToken:decodedToken.userToken},{email:1});
// console.log("hariii",user);
if(!user){
	return res.status(200).json({
	  status:'sessionExpired',
	  message:'Your session has expired please login again!'
	});
}
if(!decodedToken) {
	return res.status(422).json({
	  status:'failed',
	  message:'Not Authenticated!'
	});
}
req.user=user;
next();
};



