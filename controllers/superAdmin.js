const db = require("../models/all-models");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");
const fs = require("fs");
const sendMail = require("../middleware/sendMail");
const jwt = require("jsonwebtoken");
const randomstring = require("randomstring");
const config = require("../config/config");
const nodemailer = require("nodemailer");
//const htmlToText= require('nodemailer-html-to-text').htmlToText;
let transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: config.email,
    pass: config.password,
  },
});
let mail = config.email;

//const Joi = require('@hapi/joi');

module.exports = {
  adminLogin: adminLogin,
  resetPassword: resetPassword,
  getAdminProfile: getAdminProfile,
  getClientList: getClientList,
  blockUnblockAdmin: blockUnblockAdmin,
  AddAdmin: AddAdmin,
  deleteAdmin: deleteAdmin,
  editAdmin: editAdmin,
  sendVerificationLink: sendVerificationLink,
  sendForgetPasswordMail: sendForgetPasswordMail,
  forgotPassword: forgotPassword,
  encryptString: encryptString,
  decryptString: decryptString,
  botStatus: botStatus,
  clientInfo,
  // getTableData: getTableData
};

async function adminLogin(req, res) {
  var user = await db.Superadmin.findOne({ email: req.body.username })
    .lean()
    .exec();
  if (!user) {
    return res.status(200).json({
      status: "failed",
      message: "Invalid username or password.",
    });
  } else {
    let checkPassword = await bcrypt.compareSync(
      req.body.password,
      user.password
    );
    if (!checkPassword) {
      return res.status(200).json({
        status: "failed",
        message: "Invalid username or password.",
      });
    } else {
      let authToken = await jwt.sign(
        { email: user.username, id: user._id },
        "YuboSecretKey"
      );
      user.authToken = authToken;
      //Update auth token at the login time in users collection.
      let updateData = await db.Superadmin.updateOne(
        { _id: user._id },
        { $set: { authToken: authToken } }
      )
        .lean()
        .exec();
      return res.status(200).json({
        status: "success",
        message: "User logged-In successfully.",
        data: user,
      });
    }
  }
}

async function resetPassword(req, res) {
  let admin = await db.Superadmin.findOne({ email: req.body.email });
  if (!admin) {
    return res.status(200).json({
      status: "failed",
      message: "Email is not registerd on this plateform.",
    });
  } else {
    let checkOldPassword = await bcrypt.compareSync(
      req.body.oldPassword,
      admin.password
    );
    if (!checkOldPassword) {
      return res.status(200).json({
        type: "failed",
        message: "Old password not correct. Please enter correct password.",
      });
    }
    var updatePassword = await db.Superadmin.updateOne(
      { email: admin.email },
      { $set: { password: bcrypt.hashSync(req.body.newPassword) } }
    );
    if (!updatePassword) {
      return res.status(200).json({
        type: "failed",
        message: "Something went wrong.Please try again.",
      });
    } else {
      return res.status(200).json({
        type: "success",
        message: "Your password has been reset successfully.",
      });
    }
  }
}

async function getAdminProfile(req, res) {
  let admin = await db.Superadmin.findOne(
    { email: req.body.email },
    { authToken: 0, password: 0 }
  );
  if (!admin) {
    return res.status(200).json({
      status: "failed",
      message: "Email is not registerd on this plateform.",
    });
  } else {
    return res.status(200).json({
      type: "success",
      message: "Admin profile get successfully.",
      data: admin,
    });
  }
}

async function getClientListOld(req, res) {
  let getList = await db.Client.find();
  if (getList < 0) {
    return res.status(200).json({
      status: "failed",
      message: "No data found.",
    });
  } else {
    return res.status(200).json({
      status: "success",
      message: "List found successfully.",
      data: getList,
    });
  }
}

async function blockUnblockAdmin(req, res) {
  let checkUser = db.Client.findOne({ email: req.body.email }).lean().exec();
  if (!checkUser) {
    return res.status(200).json({
      status: "failed",
      message: "Email Id not registerd on Yugasa Bot.",
    });
  } else {
    if (req.body.type == "blocked") {
      var blockVendor = await db.Client.updateOne(
        { email: req.body.email },
        { $set: { blockStatus: 1 } }
      );
      return res.status(200).json({
        status: "success",
        message: "Distributor has blocked successfully.",
      });
    } else {
      var blockVendor = await db.Client.updateOne(
        { email: req.body.email },
        { $set: { blockStatus: 0 } }
      );
      return res.status(200).json({
        status: "success",
        message: "Distributor has unblocked successfully.",
      });
    }
  }
}

async function AddAdmin(req, res) {
  req.body.email = req.body.email.toLowerCase();
  let check = await db.Client.findOne({ email: req.body.email });
  if (check) {
    return res.status(200).json({
      status: "failed",
      message:
        "Email already registered with Yugasa Bot. Please use another Email Id.",
    });
  }
  let pwd = password_generator(8);
  let renderData = {
    clientCss: config.clientCss,
    chatMsg: req.body.chatMsg,
    msg: `Hi, we're ${req.body.clientId}`,
    logo: "",
    clientId: req.body.clientId,
  };
  let password = bcrypt.hashSync(pwd);
  req.body.password = password;
  req.body.type = req.body.type; //We don't want type key in renderdata, will capture in the type key outside of renderdata
  req.body.renderData = renderData;
  let registered = await db.Client.create(req.body);
  if (!registered) {
    return res.status(200).json({
      status: "failed",
      message: "Something went wrong. Please try after Sometime.",
    });
  } else {
    let html = `<div>
      <p>Welcome ${req.body.name} on Yugasa Bot.</p>
            <p>Username: ${req.body.clientId}</p>
          
            <p>Password: ${pwd}</p>
    
            <p style="margin-top:10px;">Regards,</p>
            <p>Team: Yugasa Bot</p>
          </div>`;

    sendMail(req.body.email, "", req.body.name, html);
    return res.status(200).json({
      status: "success",
      message: "User registered successfully with Yugasa Bot.",
      data: registered,
    });
  }
}

async function deleteAdmin(req, res) {
  var adminId = req.body.id;
  if (adminId.length > 0) {
    var delAdmin = await db.Client.deleteMany({ _id: { $in: adminId } });
    if (!delAdmin) {
      return res.status(200).json({
        status: "failed",
        message: "Something went wrong. Please try after sometime.",
      });
    } else {
      return res.status(200).json({
        status: "success",
        message: "Admin has deleted successfully.",
      });
    }
  }
}

async function editAdmin(req, res) {
  req.body.email = req.body.email.toLowerCase();
  var admin = await db.Client.findOne({ _id: req.body._id }).lean().exec();
  if (!admin) {
    return res.status(200).json({
      status: "failed",
      message: "User email not registered on Yugasa Bot.",
    });
  } else {
    let checkEmail = await db.Client.findOne({ email: req.body.email })
      .lean()
      .exec();
    if (checkEmail && checkEmail._id != req.body._id) {
      return res.status(200).json({
        status: "failed",
        message: "Email Id has already exits. Please use another Email Id.",
      });
    } else {
      req.body.email = req.body.email.toLowerCase();
      let renderData = {
        clientCss: config.clientCss,
        chatMsg: req.body.chatMsg,
        msg: `Hi, we're ${req.body.clientId}`,
        logo: "",
        clientId: req.body.clientId,
      };
      var update = await db.Client.updateOne(
        { _id: req.body._id },
        {
          $set: {
            name: req.body.name,
            email: req.body.email,
            clientId: req.body.clientId,
            type: req.body.type,
            renderData: renderData,
          },
        }
      );
      if (!update) {
        return res.status(200).json({
          status: "failed",
          message: "Something went wrong. Please try again after some time.",
        });
      } else {
        return res.status(200).json({
          status: "success",
          message: "Admin profile has update successfully.",
        });
      }
    }
  }
}

async function sendVerificationLink(req, res) {
  req.body.email = req.body.email.toLowerCase();
  let user = await db.Superadmin.findOne({
    email: req.body.email,
    role: "super-admin",
  })
    .lean()
    .exec();
  if (!user) {
    res.status(200).send({
      code: 401,
      status: "failed",
      message: "User with this email not registered on Yugasa Bot.",
    });
  } else {
    let email = req.body.email;
    let encrypt_email = await encryptString(
      `${email}&${new Date().getTime() + 20 * 60 * 1000}`
    );
    let verificationLink = `${config.serverHost}/setPassword?email=${encrypt_email}`;
    //https://chaknaboyz.com/#/call-back-page?email=EMAIL_KEY-=
    let mail_message = {
      email: email,
      verificationLink: verificationLink,
      title: "Forgot Password",
      maildata: "Please click on the following link to reset your password.",
    };
    sendForgetPasswordMail(mail_message);
    res.status(200).send({
      status: "success",
      message: "A verification link sent on registered email Id.",
    });
  }
}

async function sendForgetPasswordMail(data) {

  let { title, maildata, email, verificationLink } = data;
  let titleName = `Hi User,`;
  let mailFrom = mail;
  //transporter.use('compile', htmlToText(verificationLink));
  transporter.sendMail(
    {
      from: mailFrom,
      to: email,
      subject: title,
      html: `<div>
            <p>${titleName}</p>
          
            <p>${maildata}</p>
          
            <p style="margin-top:10px;">Reset Password Link:-<a href=${verificationLink}>${verificationLink}</a></p>
            <p>This verification link valid for next 20 minutes.</p>
            <p style="margin-top:10px;">Thanks,</p>
            <p>Yugasa Bot Team</p>
          </div>`,
    },
    (err, info) => {
      if (err) {
        return false;
      } else {
        return true;
      }
    }
  );
}

async function forgotPassword(req, res) {
  let get_data = decryptString(req.body.email);
  var newPassword = req.body.newPassword;
  let code = get_data.split("&");
  let email = code[0];
  let timestamp = code[1];
  //if link time stamp less than current time link expire
  if (new Date().getTime() > parseInt(timestamp, 10)) {
    res.status(200).send({
      code: 401,
      status: "failed",
      message: "This verification link expire. Please try again.",
    });
  } else {
    let users = await db.Superadmin.findOne({
      email: email,
      role: "super-admin",
    })
      .lean()
      .exec();
    if (!users) {
      res.status(200).send({
        status: "failed",
        message: "This email does not exists.",
      });
    } else {
      let encryptPassword = bcrypt.hashSync(newPassword);
      let update = await db.Superadmin.updateOne(
        { email: email },
        { password: encryptPassword },
        { new: true }
      ).exec();
      if (!update) {
        res.status(401).send({
          status: "failed",
          message: "Password not update. Please try again after some time.",
        });
      } else {
        res.status(200).send({
          status: "success",
          message: "Your password has been updated successfully.",
        });
      }
    }
  }
}

function encryptString(data) {
  const cipher = crypto.createCipher(
    config.encryption.ALGORITHM,
    config.encryption.PASSWORD
  );
  let crypted = cipher.update(data, "utf8", "hex");
  crypted += cipher.final("hex");
  return crypted;
}

function decryptString(data) {
  const decipher = crypto.createDecipher(
    config.encryption.ALGORITHM,
    config.encryption.PASSWORD
  );
  let dec = decipher.update(data, "hex", "utf8");
  dec += decipher.final("utf8");
  return dec;
}

function password_generator(len) {
  var length = len ? len : 10;
  var string = "abcdefghijklmnopqrstuvwxyz"; //to upper
  var numeric = "0123456789";
  var punctuation = "!@#$%^&*";
  var password = "";
  var character = "";
  var crunch = true;
  while (password.length < length) {
    entity1 = Math.ceil(string.length * Math.random() * Math.random());
    entity2 = Math.ceil(numeric.length * Math.random() * Math.random());
    entity3 = Math.ceil(punctuation.length * Math.random() * Math.random());
    hold = string.charAt(entity1);
    hold = password.length % 2 == 0 ? hold.toUpperCase() : hold;
    character += hold;
    character += numeric.charAt(entity2);
    character += punctuation.charAt(entity3);
    password = character;
  }
  password = password
    .split("")
    .sort(function () {
      return 0.5 - Math.random();
    })
    .join("");
  return password.substr(0, len);
}

async function getClientList(req, res) {
  let data = await db.Client.aggregate([
    {
      $lookup: {
        from: "userchats",
        // localField: "_id",
        // foreignField: "clientId",
        let: { objId: "$_id", clientId: "$clientId" },
pipeline: [
               {
                   $match: {
                       $expr: {
                           $or: [
                               { $eq: [ "$clientId", "$$objId" ] },
                               { $eq: [ "$clientId", "$$clientId" ] }
                           ]
                       }
                   }
               }
           ],
        as: "userdata",
      },
    },
    {
      $unwind: { path: "$userdata", preserveNullAndEmptyArrays: true },
    },
    {
      $project: {
        _id: 0,
        clientId: 1,
        name: 1,
        phone: 1,
        email: 1,

        users: { $cond: { if: "$userdata.userId", then: 1, else: 0 } },
        leadcount: {
          $cond: {
            if: {
              $or: [
                "$userdata.session.email",
                "$userdata.session.phone",
                "$userdata.email",
                "$userdata.phone",
              ],
            },
            then: 1,
            else: 0,
          },
        },
        chatSize: {
          $cond: {
            if: "$userdata.chats",
            then: { $size: "$userdata.chats" },
            else: 0,
          },
        },
      },
    },
    {
      $group: {
        _id: "$clientId",
        totalLeads: { $sum: "$leadcount" },
        totalUsers: { $sum: "$users" },
        totalConversation: { $sum: "$chatSize" },
        clientData: { $first: "$$ROOT" },
      },
    }, {
      $lookup: {
        from: "logs",
        localField: "_id",
        foreignField: "clientId",
        as: "clientLogs",
      },
    },{
      $lookup: {
        from: "superadminclients",
        localField: "_id",
        foreignField: "clientId",
        as: "clientInfo",
      }
    },
    {
      $project: {
        _id: 1,
        totalLeads:1,
        totalUsers:1,
        totalConversation:1,
        clientData:1,
        clientLogs: { $arrayElemAt: ["$clientLogs", -1] },
        clientdetails: { $arrayElemAt: ["$clientInfo", 0] },
      },
    }
  ]).cursor().exec().toArray();

  if(data.length>0){
    return res.status(200).json({
      status: "success",
      message: "Data found successfully.",
      data: data,
    });
  } else{
    return res.status(200).json({
      status: "failed",
      message: "No data found.",
    });
  }
 
}

async function botStatus(req, res) {

  let data = await db.Client.aggregate([
    {
      $lookup: {
        from: "userchats",
        let: { objId: "$_id", clientId: "$clientId" },
        pipeline: [
          {
            $match: {
              $expr: {
                $or: [
                  { $eq: ["$clientId", "$$objId"] },
                  { $eq: ["$clientId", "$$clientId"] },
                ],
              },
            },
          },
        ],
        as: "userdata",
      },
    },
    {
      $unwind: { path: "$userdata", preserveNullAndEmptyArrays: true },
    },
    {
      $project: {
        _id: 0,
        clientId: 1,
        name: 1,
        phone: 1,
        email: 1,
        botStatus: {
          $let: {
            vars: {
              diff: {
                $divide: [
                  { $subtract: [new Date(), "$userdata.date"] },
                  1000 * 60 * 60 * 24,
                ],
              },
            },
            in: {
              $switch: {
                branches: [
                  {
                    case: { $and: [{ $lte: ["$$diff", 15] }, "$$diff"] },
                    then: "Active",
                  },
                  {
                    case: { $and: [{ $gt: ["$$diff", 15] }, "$$diff"] },
                    then: "Inactive",
                  },
                ],
                default: "No states",
              },
            },
          },
          // in: {$multiply: ["$$diff", 1]}}
        },
        diff: {
          $divide: [
            { $subtract: [new Date(), "$userdata.date"] },
            1000 * 60 * 60 * 24,
          ],
        },
        // userData: "$userdata"
      },
    },
    {
      $group: {
        _id: "$clientId",
        clientDetails: { $last: "$$ROOT" },
      },
    },
  
    {
      $lookup: {
        from: "logs",
        localField: "_id",
        foreignField: "clientId",
        as: "clientLogs",
      },
    },{
      $lookup: {
        from: "superadminclients",
        localField: "_id",
        foreignField: "clientId",
        as: "clientInfo",
      }
    },
    {
      $project: {
        _id: 1,
        clientDetails: 1,
        clientLogs: { $arrayElemAt: ["$clientLogs", -1] },
        clientdetails: { $arrayElemAt: ["$clientInfo", 0] },
      },
    },
  ]).cursor().exec().toArray();

    let inactivedata=[],activedata=[],nostatedata =[];
  for (i=0;i<data.length;i++) {
  if(data[i].clientDetails.botStatus =='Inactive'){
  inactivedata.push(data[i])
  } else if(data[i].clientDetails.botStatus =='Active') {
  activedata.push(data[i])
  } else{
  nostatedata.push(data[i])
  }
  }
  console.log('11122',activedata);
  if(data.length>0){
    return res.status(200).json({
      status: "success",
      message: "Bot Data found successfully.",
      //data: data,
      actData: activedata,
      inActData: inactivedata,
      noActData: nostatedata,
    });
  } else{
    return res.status(200).json({
      status: "failed",
      message: "No Bot data found.",
      
    });
  }
  
}

async function clientInfo(req,res){
let dataobj = JSON.parse(JSON.stringify(req.body));// to filter client id form body obj//
delete dataobj['clientId'];
 let data = await db.superadminclient.find({clientId:req.body.clientId})
 if(data.length>0){
      let updateddata = await db.superadminclient.updateOne({"clientId":req.body.clientId},{ClientInfo:req.body})
      return res.json({msg:'Data updated successfuly'})
 } else {
   let data = new db.superadminclient({
    clientId:req.body.clientId,
    ClientInfo : dataobj
   });

   await data.save();
   return res.json({msg:'Data saved successfuly'})
 }
}