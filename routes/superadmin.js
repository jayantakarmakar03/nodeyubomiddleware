const express = require('express');
const router = express.Router();
const admin = require('../controllers/superAdmin');
const adminAuth = require('../middleware/adminAuth');

var cors = require('cors');

router.use(cors());

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/login', /*validation.userSignup,*/ admin.adminLogin);
router.post('/resetPassword', adminAuth, admin.resetPassword);
router.post('/getAdminProfile', adminAuth, admin.getAdminProfile);
router.get('/getClientList',adminAuth,admin.getClientList);
router.post('/blockUnblockAdmin', admin.blockUnblockAdmin);
router.post('/AddAdmin', admin.AddAdmin);
router.post('/deleteAdmin', admin.deleteAdmin);
router.post('/editAdmin', admin.editAdmin);
router.post('/sendVerificationLink', admin.sendVerificationLink);
router.post('/forgotPassword', admin.forgotPassword);
router.post('/clientInfo',adminAuth,admin.clientInfo)
// router.post('/getClientData', admin.getTableData)
router.get('/botStatus',adminAuth,admin.botStatus);
module.exports = router;