document.onreadystatechange = function(e){
    if ((document.readyState === 'interactive' || document.readyState === 'complete') && document.getElementById("bootm-box")==undefined){
		(function() {
			let host = location.hostname;
			let url=location.href

			//domain for live
			const domain = "https://yubo.yugasa.org";

			//domain for localhost
			// var domain = "https://localhost:4900";

			let parent = document.createElement('div');
			parent.setAttribute('class', 'iframe-box');
			parent.setAttribute('id', 'iframe-box');

			let bootm = document.createElement('div');
			bootm.setAttribute('class', 'bootm-box');
			bootm.setAttribute('id', 'bootm-box');

			let chatstart = document.createElement('div');
			chatstart.setAttribute('class', 'chat-start bounce-top');

			let closeimg = document.createElement('div');
			closeimg.setAttribute('class', 'close-img');
			let imgclose  = document.createElement('img');
			imgclose.src = domain+"/img/close.svg";
			closeimg.appendChild(imgclose)
			
		 //    var refreshdiv = document.createElement('div');
		 //    refreshdiv.setAttribute('class', 'refresh');
			// var refreshimg  = document.createElement('img');
			// refreshimg.src=domain+"/img/refresh.png";
			// refreshimg.setAttribute("title","Restart")

			// refreshdiv.appendChild(refreshimg);
			

			let outercircle = document.createElement('div');
			outercircle.setAttribute('class', 'outerCircle');

			let innercircle = document.createElement('div');
			innercircle.setAttribute('class', 'innerCircle');

			let icon = document.createElement('div');
			icon.setAttribute('class', 'hy-icon');

			let bubbleimg  = document.createElement('img');
			bubbleimg.setAttribute('class', 'bubble-img');
			bubbleimg.src = domain+"/img/chat-bubble.svg";

			icon.appendChild(bubbleimg);

			chatstart.appendChild(outercircle)
			chatstart.appendChild(innercircle)
			chatstart.appendChild(icon)

			bootm.appendChild(chatstart);
		    bootm.appendChild(closeimg);
		   // parent.appendChild(refreshdiv);

			let story=$('.entry-content').text();
			if(story!="" && story!=undefined){
				story=true;
			} else {
				story=false;
			}

		    let iframe = document.createElement('iframe');
		   	iframe.setAttribute('id', 'frame-lode');
		    // iframe.style.display = "none";
		   	iframe.src = domain+"/chatWindow?clientId=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6Inl1Z2FzYV9jaGF0Ym90IiwiaWF0IjoxNTgyODkzMzM0fQ.xQjjea7Cqq5I0-Zn3A_GvJWU3m8JFpp1wFedA9INxsg"+"&host="+host+"&url="+url+"&story="+story;
		    parent.appendChild(bootm);
		    parent.appendChild(iframe);

			let powered = document.createElement("div");
			powered.setAttribute("class","poweredbyyugasa");
			let poweredlink = document.createElement("a");
			poweredlink.setAttribute("href","https://helloyubo.com/");
			poweredlink.setAttribute("target","_blank");
			poweredlink.setAttribute("title","Building Blocks for Building Bots");
			let poweredbytext = document.createTextNode("Powered by ");
			let hyperlinktext = document.createTextNode("Yugasa Bot");
			poweredlink.appendChild(hyperlinktext);
			powered.appendChild(poweredbytext);
			powered.appendChild(poweredlink);
			parent.appendChild(powered);
			
		    document.body.appendChild(parent);
			document.getElementById("bootm-box").addEventListener("click",function(){
				document.getElementById("iframe-box").classList.toggle("open-chat-window");
				
			});
		})();

		// (function(){
		// 	//Make the DIV element draggagle:
		// 	dragElement(document.getElementById("iframe-box"));

		// 	function dragElement(elmnt) {
		// 	  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
		// 	  if (document.getElementById(elmnt.id + "header")) {
		// 	    /* if present, the header is where you move the DIV from:*/
		// 	    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
		// 	  } else {
		// 	    /* otherwise, move the DIV from anywhere inside the DIV:*/
		// 	    elmnt.onmousedown = dragMouseDown;
		// 	  }

		// 	  function dragMouseDown(e) {
		// 	    e = e || window.event;
		// 	    e.preventDefault();
		// 	    // get the mouse cursor position at startup:
		// 	    pos3 = e.clientX;
		// 	    pos4 = e.clientY;
		// 	    document.onmouseup = closeDragElement;
		// 	    // call a function whenever the cursor moves:
		// 	    document.onmousemove = elementDrag;
		// 	  }

		// 	  function elementDrag(e) {
		// 	    e = e || window.event;
		// 	    e.preventDefault();
		// 	    // calculate the new cursor position:
		// 	    pos1 = pos3 - e.clientX;
		// 	    pos2 = pos4 - e.clientY;
		// 	    pos3 = e.clientX;
		// 	    pos4 = e.clientY;
		// 	    // set the element's new position:
		// 	    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
		// 	    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
		// 	  }

		// 	  function closeDragElement() {
		// 	    /* stop moving when mouse button is released:*/
		// 	    document.onmouseup = null;
		// 	    document.onmousemove = null;
		// 	  }
		// 	}
		// })();
	}
}

window.onload = function() {
	let hurl=location.href
	let exclusions=['https://yugasa.com/hire-developer-staff-augmentation/','https://yugasa.com/hire-website-developers-in-india/','https://yugasa.com/ios-application-development/','https://yugasa.com/mobile-application-development/']
	
	// if(!exclusions.includes(hurl)){
	// 	// First check, if localStorage is supported.
	// 	if (window.localStorage) {
	// 		// Get the expiration date of the previous popup.
	// 		var nextPopup = localStorage.getItem( 'nextyubo' );

	// 		if (nextPopup > new Date()) {
	// 			return;
	// 		}
	// 		// Store the expiration date of the current popup in localStorage.
	// 		var expires = new Date();
	// 		expires = expires.setHours(expires.getHours() + 24);

	// 		localStorage.setItem( 'nextyubo', expires );
	// 	}

	// 	document.getElementById("bootm-box").dispatchEvent(new Event('click'));
	// }
};