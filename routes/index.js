const db = require("../models/all-models"),
    mongo = require('mongodb'),
    ObjectID = mongo.ObjectID,
    sendMail = require("../middleware/sendChat.js"),
    jwt = require("jsonwebtoken"),
    axios = require("axios"),
    express = require("express"),
    multer = require("multer"),
    cookieParser = require("cookie-parser"),
    fs = require("fs"),
    CronJob = require("cron").CronJob, //* do not remove this, removing may  cause moment error on communication page
    router = express.Router(),
    dateFormat = require("dateformat"),
    moment = require("moment"),
    excel = require("exceljs"),
    escapeHtml = require('escape-html'),
    humanAgentController = require('../controllers/humanAgent'),

    Hubspot = require("hubspot"),
    tokenAuth = require("../middleware/tokenAuth.js"),
    emailverifycontroller = require("../controllers/emailverification.js"),
    treecontroller = require("../controllers/tree.js"),
    communitycontroller = require('../controllers/communitypage'),
    couponController = require('../controllers/coupon');
clientcontroller = require("../controllers/clientAuth.js"),
    editProfilecontroller = require("../controllers/editProfile.js"),
    paymentController = require("../controllers/payment"),
    yubosettingcontroller = require("../controllers/yubosettting.js"),
    resetcontroller = require("../controllers/resestpassword.js"),
    intentscontroller = require("../controllers/intents.js"),
    emailTemp = require("../controllers/emailTemplate.js"),
    botApi = require("../controllers/botApi.js"),
    AWS = require('aws-sdk'),
    config = require('../config/config.json')
multerS3 = require('multer-s3');
s3 = new AWS.S3({
        accessKeyId: config.s3Linode.accessKeyId,
        secretAccessKey: config.s3Linode.secretAccessKey
    }),
    cronJobController =
    process.env.PRODUCTION == "true" ? require("../controllers/cronJob.js") : "",
    passport = require("passport"),
    LocalStrategy = require("passport-local").Strategy,
    bodyParser = require("body-parser"),
    session = require("express-session"),
    flash = require("connect-flash"),
    async = require("async"),
    whatsappController = require("../controllers/whatsapp"),
    dbController = require("../controllers/dbintegration"),
    actionController = require("../controllers/action"),
    usersController = require("../controllers/users"),
    eventListener = require('../controllers/eventListener'),
    crypto = require('crypto'),
    bcrypt = require("bcryptjs"),
    codeNodeController = require('../controllers/codenode');

let redisClient;

const redis = require("redis");

(async () => {
  redisClient = redis.createClient({"password":process.env.NODE_REDIS_KEY});

  redisClient.on("error", (error) => console.error(`Error : ${error}`));

  await redisClient.connect();
})();

const rateLimit = require("express-rate-limit");
agentTemplateController = require('../controllers/agentTemplate')
optOutController = require("../controllers/opt_outList")
scheduledCampaignController = require("../controllers/scheduledCampaign")

const azureController = require("../controllers/azureStorage.js");
const storageController = require("../controllers/storageController");
// SET STORAGE for file uploads in local folder and for renaming the uploaded file
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        if (req.body.demo1headercolor || req.body.demo2headercolor) {
            let dir = checkdirexist();
            cb(null, dir);
        } else {
            cb(null, "public/dist/img");
        }
    },
    filename: function(req, file, cb) {
        let extArray = file.mimetype.split("/");
        let extension = extArray[extArray.length - 1];
        cb(null, file.fieldname + "-" + Date.now() + "." + extension);
    },
});
var uploadbulk = multer({
    storage: multerS3({
        s3: s3,
        bucket: config.s3Linode.yubobucket,
        acl: 'public-read',
        metadata: function(req, file, cb) {
            cb(null, { fieldName: file.fieldname });
        },
        key: function(req, file, cb) {
            if (req.path == '/schedulemail') {
                cb(null, req.user.clientId + '/template/' + Date.now().toString())
            } else {
                cb(null, req.user.clientId + '/draft/' + file.fieldname)
            }

        }
    })
})

var upload = multer({ storage: storage });

// passport needs ability to serialize and unserialize users out of session
passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(user, done) {
    done(null, user);
});

// body-parser for retrieving form data
// router.use(bodyParser.json());
// router.use(bodyParser.urlencoded({ extended: true }));

//initialize passposrt and and session for persistent login sessions
router.use(
    session({
        secret: "YuboSecretKey",
        resave: true,
        saveUninitialized: true,
    })
);
router.use(passport.initialize());
router.use(passport.session());

router.use(flash());
router.use(cookieParser());

router.use(function(req, res, next) {
    res.locals.success_msg = req.flash("success_msg");
    res.locals.error_msg = req.flash("error_msg");
    next();
});

//request rate limit function
//by mukesh


const getLoginOtpLimiter = rateLimit({
    windowMs: 60 * 60 * 1000, // 15 Minutes
    max: 10, //Limit each IP to 1 requests per `window`
    message: "You have exceeded the maximum request limit, Please wait for one hour, then try again.",
    //   standardHeaders: true,
    //   legacyHeaders: false,
    handler: (req, res, next, options) =>
        res.json({
            statusCode: options.statusCode,
            message: options.message,
        }),
});
const forgotPassApiLimit = rateLimit({
    windowMs: 1440 * 60 * 1000, // 24 Hours
    max: 3, //Limit each IP to 1 requests per `window`
    message: "You have exceeded the maximum request limit. Please wait for 24 hours, then try again.",
    handler: (req, res, next, options) =>
        res.render("forgot.hbs", {message : "You have exceeded the maximum request limit. Please wait for 24 hours, then try again.",limitStatus: "true" })
});

// passport local strategy for local-login, local refers to this app
passport.use(
    "local-login",
    new LocalStrategy({
            passReqToCallback: true,
        },
        async function(req, username, password, done) {

        try{
            var user = await db.Client.find({
                $or: [{ clientId: username }, { email: username }],
            });
            if (user.length > 0) {
                if(user[0].failedLoginCount >= 10){
                    return done(
                        null,
                        false,
                        req.flash("error_msg", "You have tried your maximum request limit, Please attempt the 'Forgot Password' option to recover your password")
                    );
                }
                if (user[0].email_verified == false || !user[0].email_verified) {
                    await emailverifycontroller.resendVerificationMail(user[0]);
                    let maskedemail = emailmasking(user[0].email)
                    return done(
                        null,
                        false,
                        req.flash(
                            "error_msg",
                            "We have sent a verification link on " + maskedemail + ". Please check your spam folder in case you do not see that in your inbox."
                        )
                        // req.flash(
                        //     "error_msg",
                        //     "Your account is under review. Our team will get in touch with you to complete the on-boarding process."
                        // )
                    );
                } else {
                    //password verify.
                    let passwordIsValid = await bcrypt.compareSync(
                        password,
                        user[0].password
                    );
                    if (passwordIsValid) {
                        /******************************************* */
                        //generate jwt token for authentication 
                        let jwtSecretKey = "YUGASA@BOT@123*";
                        let data = {
                            time: new Date(),
                        }
                        sessionToken = jwt.sign(data, jwtSecretKey);

                        db.Client.findOneAndUpdate({ clientId: user[0]._doc.renderData.clientId }, {
                            $set: {
                                sessionToken: sessionToken
                            }
                        }, async function(error, result) {
                            if (error) {
                                console.log("ERROR")
                            } else if (result) {
                                console.log("RESULT", result)
                            } else {
                                console.log("NOT FOUND")
                            }
                        })

                        /**************************************************** 
                        check if 2fa enabled  and otp matched then only allow 
                        ****************************************************/
                        if (req.body._2FAdata._2FA_enabled == false) {
                            return done(null, user[0]);
                        }
                        if (req.body._2FAdata._2FA_enabled == true && req.body._2FAdata.otpMatched == true) {
                            return done(null, user[0]);
                        }
                        console.log("######################2FA data", req.body._2FAdata)
                        if (req.body._2FAdata._2FA_enabled == true && req.body._2FAdata.otpMatched == false) {
                            return done(
                                null,
                                false,
                                req.flash("error_msg", "Invalid OTP entered !")
                            );
                        }

                        /********************************************** */
                        await resetFailedLoginStatus(user[0]);
                        return done(null, user[0]);
                    } else {
                        await updateCountOnFailedLogin(user[0]);
                        return done(
                            null,
                            false,
                            req.flash("error_msg", "Incorrect username or password!")
                        );
                    }
                }
            } else {
                await updateCountOnFailedLogin(user[0]);
                return done(
                    null,
                    false,
                    req.flash("error_msg", "Incorrect username or password!")
                );
            }
        }catch(error){
            console.log('Error in catch block = ', error)
        }
        }
    )
);
/*************************************************************************************
**** count and update the failed user login to avoid unnecessary login API call 
****************************************************************************************/
async function  updateCountOnFailedLogin(user){
    try{
        if(user){
        let failedLoginCount = user.failedLoginCount + 1;
           await db.Client.updateOne(
            {_id : user._id},
            {
                $set: {
                        failedLoginCount : failedLoginCount
                }
            });
            return ;
        }
    }catch(error){
        console.log('Error in catch block', error)
    }
}
/*************************************************************************************
**** reset the failed login flag when user login successfuly
****************************************************************************************/
async function  resetFailedLoginStatus(user){
    try{
           await db.Client.updateOne(
            {_id : user._id},
            {
                $set: {
                        failedLoginCount : 0
                }
            });
            return ;
    }catch(error){
        console.log('Error in catch block', error)
    }
}
// passport local strategy for local-signup, local refers to this app

passport.use(
    "local-signup",
    new LocalStrategy({
            passReqToCallback: true,
        },
        async function(req, username, password, done) {
        try{
            /**************************************************
             * Check phone number of given user is verified or not if not then send response to user 
             * ************************************************/
            let check = await db.Otp.findOne({ phone: req.body.phone, email: req.body.email, clientId: username });
            console.log('check 1111', check)
            if (check) {
                if (check.otp_verified == false || check.otp_verified == 'false') {
                    return done(
                        null,
                        false,
                        req.flash("error_msg", "Phone number is not verified!")
                    );
                }
            } else {
                return done(
                    null,
                    false,
                    req.flash("error_msg", "Phone number is not verified!!")
                );
            }
            /*********************************************** */
            var user = await db.Client.find({
                $or: [{ clientId: username }, { email: req.body.email }],
            });
            if (user.length > 0) {
                if (user[0].clientId == username) {
                    return done(
                        null,
                        false,
                        req.flash("error_msg", "Username already exist!")
                    );
                }
                if (user[0].email == req.body.email) {
                    return done(
                        null,
                        false,
                        req.flash("error_msg", "Email already exist!")
                    );
                }
            } else {
                const emailRegexp = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (!emailRegexp.test(req.body.email)) {
                    return done(
                        null,
                        false,
                        req.flash(
                            "error_msg",
                            "Please enter valid email Example: abc@xyz.com"
                        )
                    );
                }
                const phoneRegexp = /^\d{10}$/;
                if (!phoneRegexp.test(req.body.phone)) {
                    return done(
                        null,
                        false,
                        req.flash(
                            "error_msg",
                            "Please enter valid mobile number, special character not allow"
                        )
                    );
                }
                const pwdRegexp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,32}$/;
                if (!pwdRegexp.test(req.body.password)) {
                    return done(
                        null,
                        false,
                        req.flash(
                            "error_msg",
                            "Password must contain at least one  number, one special character, one uppercase and lowercase letter, and at least 8 or more characters"
                        )
                    );
                }
                const userRegexp = /^(?=.{8,20}$)(?![_.0-9])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/;
                if (!userRegexp.test(req.body.username)) {
                    return done(
                        null,
                        false,
                        req.flash(
                            "error_msg",
                            "Username is required & should only contain alphanumeric characters, underscore and dot. Underscore, number and dot can't be at the end or start. Underscore and dot can't be next to each other. Underscore or dot can't be used multiple times in a row. Number of characters must be between 8 to 20."
                        )
                    );
                }
                const urlRegexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/
                if (req.body.url && !urlRegexp.test(req.body.url)) {
                    return done(
                        null,
                        false,
                        req.flash(
                            "error_msg",
                            "Please enter the valid website url."
                        )
                    );
                }

                if (!req.body.trms_cndtn_chkbx && req.body.trms_cndtn_chkbx != "on") {
                    return done(
                        null,
                        false,
                        req.flash(
                            "error_msg",
                            "You must read & accept the terms and conditions in order to sign up on the platform."
                        )
                    );
                }

                let token = await tokenAuth.encryptString(
                    `${req.body.email}&${new Date().getTime() + 24 * 3600 * 1000}`
                );
                let locationData;
                if (req.body.locationdata) {
                    locationData = JSON.parse(req.body.locationdata);
                } else {
                    locationData = {};
                }

                let newUser = new db.Client({
                    username: username,
                    clientId: username,
                    email: req.body.email,
                    phone: req.body.phone,
                    accepted_trmscndtn: "yes",
                    type: "basic",
                    email_verified: false,
                    email_token: token,
                    location: locationData,
                    password: bcrypt.hashSync(req.body.password, 12),
                    renderData: {
                        clientId: username,
                        logo: "img/" + username + ".png",
                        msg: "Hi, we're " + username,
                        chatMsg: "Welcome! How may I help you!",
                        clientCss: "assets/" + username + ".bot.css",
                        type: "basic",
                        incoming_sound: "False"
                    },
                });
                await newUser.save();
                await emailverifycontroller.sendverificationmail(
                    req.body.email,
                    token,
                    username
                );
                process.env.PRODUCTION == "true" ? await emailverifycontroller.newSignupNotification(req.body, username) : "";
                let maskedemail = emailmasking(req.body.email)
                return done(null, true, {
                    message: "We have sent a verification link on " + maskedemail + ". Please check your spam folder in case you do not see that in your inbox.",
                    //message: "We have received your registration request. Our team will get in touch with you to complete the on-boarding process."
                });
            }
        }catch(error){
            console.log('An error occurs in the catch block:-', error)
        }       
        }
    )
);

//route when user clicks on email verification link sent to their email
router.get("/emailverification", emailverifycontroller.emailverification);

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) return next();
    res.redirect("/login");
}
router.post(
    "/adminPanel", otpVerify,
    passport.authenticate("local-login", { failureRedirect: "/login" }),
    function(req, res) {
        res.redirect("/adminPanel");
    }
);
//middleware to check 2FA otp is valid or not during login time
async function otpVerify(req, res, next) {
    try {
        let clientData = await db.Client.findOne({ clientId: req.body.username }, { phone: 1, _2FA: 1 });
        if (clientData == null) {
            req.body._2FAdata = {
                _2FA_enabled: false,
                otpMatched: false
            }
            return next();
        }
        if (clientData && (clientData._2FA == true) && (req.body.userotp == undefined || req.body.userotp == '' || req.body.userotp == 'undefined')) {
            req.body._2FAdata = {
                _2FA_enabled: true,
                otpMatched: false
            }
            return next()
        }
        if (req.body.userotp == undefined || req.body.userotp == '' || req.body.userotp == 'undefined') {
            req.body._2FAdata = {
                _2FA_enabled: false
            }
            return next()
        }

        let checkdata = await db.Otp.findOne({
            phone: clientData.phone,
            clientId: req.body.username,
            otpFor: 'loginOtp'
        });

        if (checkdata) {
            if (checkdata.status == true) {
                req.body._2FAdata = {
                    _2FA_enabled: false,
                    otp_id: checkdata._id,
                    otpMatched: false
                }
                return next()

            } else {
                if (req.body.userotp == checkdata.otp) {
                    req.body._2FAdata = {
                        _2FA_enabled: true,
                        otp_id: checkdata._id,
                        otpMatched: true
                    }
                    await db.Otp.updateOne({ _id: checkdata._id }, { $set: { otp: "", status: false, otp_verified: false } })
                    return next()
                } else {
                    req.body._2FAdata = {
                        _2FA_enabled: true,
                        otp_id: checkdata._id,
                        otpMatched: false
                    }
                    return next()
                }
            }
        } else {
            req.body._2FAdata = {
                _2FA_enabled: true,
                otpMatched: false
            }
            return next();
        }
    } catch (err) {
        console.log("otpVerify error", err)
    }
}
router.post(
    "/register",
    passport.authenticate("local-signup", {
        failureRedirect: "/register",
        failureFlash: true,
    }),
    async function(req, res) {
        var authtoken = await jwt.sign({ clientId: req.body.username }, "yubo2020");

        // await fs.readFile(
        //   "./public/assets/master.bot.js",
        //   "utf8",
        //   function (err, data) {
        //     if (err) {
        //       return;
        //     }
        //     var result = data.replace(/#CID#/g, '"' + authtoken + '"');

        //     fs.writeFile(
        //       "./public/assets/" + req.body.username + ".bot.js",
        //       result,
        //       "utf8",
        //       function (err) {
        //         if (err) return;
        //       }
        //     );
        //   }
        // );

        // // res.render('master_bot', {
        // //     token: authtoken,
        // // },function (err, template) {
        // // });
        // // await fs.copyFile('./public/assets/master.bot.js', './public/assets/' + req.body.username + '.bot.js', (err) => {
        // //     if (err) throw err;
        // //     //console.log('source.js was copied to destination.txt');
        // // });
        // await fs.copyFile(
        //   "./public/assets/master.bot.css",
        //   "./public/assets/" + req.body.username + ".bot.css",
        //   (err) => {
        //     if (err) throw err;
        //   }
        // );

        // await fs.copyFile(
        //   "./public/dist/img/speech-bubble.png",
        //   "./public/img/" + req.body.username + ".png",
        //   (err) => {
        //     if (err) throw err;
        //   }
        // );

        // // let client = getClient(req);
        // let subsdata = {
        //   userId: "c1fcfbca-af31-5885-5779-dc9f0c91dd27",
        //   client_id: req.body.username,
        //   train: "False",
        //   unsubsribe: "False",
        //   text: "",
        //   node: "False",
        //   session: {
        //     email: "",
        //     name: "",
        //     phone: "",
        //   },
        //   category: "basic",
        //   update_tree: "False",
        //   update_story: "False",
        //   basic: "True",
        // };
        // await axios({
        //   method: "post",
        //   url: process.env.pythonUrl,
        //   data: { data: subsdata },   
        // });

        // /*** For creating intents.json file and cnfig.json file ***/
        // await intentscontroller.chckDir(req.body.username);
        // await intentscontroller.saveBasicIntentsInDb(req, res, req.body.username);
        // await fs.copyFile(
        //   "./public/assets/config.json",
        //   process.env.pythonDir + req.body.username + "/config.json",
        //   (err) => {  
        //     if (err) throw err;
        //   }
        // );
        let maskedemail = emailmasking(req.body.email)
            req.flash("success_msg", "We have sent a verification link on " + maskedemail + ". Please check your spam folder in case you do not see that in your inbox.")
            //req.flash("success_msg", "We have received your registration request. Our team will get in touch with you to complete the on-boarding process.")
            // res.render("login.hbs", {
            //   verification_msg:
            //     "We have sent a verification link on "+maskedemail+". Please check your spam folder in case you do not see that in your inbox.",
            // });
            // res.redirect("/thank-you")
        res.redirect('/thank-you?valid=' + authtoken);
    }
);

// var jwt=require("jsonwebtoken");
// let authToken =await jwt.sign({clientId:"corona_chatbot"},'yubo2020');

/*router.get("/chatWindow", async function(req, res, next) {
    let decodedToken = jwt.verify(req.query.clientId, "yubo2020");
    let client = await db.Client.findOne({ clientId: decodedToken.clientId });
    // client.renderData["hutk"] = req.query.hutk;

    let host = (req.query.host) ? req.query.host : '';
    let url = (req.query.url) ? req.query.url : '';
    // client.renderData["url"] = url;

    let clientLogs = await db.Logs.findOne({ clientId: decodedToken.clientId, host: host, url: url });
    if (!clientLogs) {
        let saveData = new db.Logs({ clientId: decodedToken.clientId, host: host, url: url });
        await saveData.save();
    } else {
        await db.Logs.updateOne({ clientId: decodedToken.clientId, host: host, url: url }, { $set: { updatedAt: Date.now(), count: clientLogs.count + 1 } });
    }

    if (client) {
        client.renderData["hutk"] = req.query ? req.query.hutk : "";
        client.renderData["url"] = url;
        return res.render("chat.hbs", client.renderData);
    }
});*/

router.get("/chatWindow",chatWindowMiddleware, async function(req, res, next) {
try{
    if(!req.query.clientId){
        return res.json({ message: "Please provide clientId" , statusCode : 201});
    }
    let decodedToken = jwt.verify(req.query.clientId, "yubo2020");
    let client = await db.Client.findOne({ clientId: decodedToken.clientId });
    // client.renderData["hutk"] = req.query.hutk;

    let host = (req.query.host) ? req.query.host : '';
    let url = (req.query.url) ? req.query.url : '';

    let storageData = (req.query.storageData) ? req.query.storageData : '';
    let story = (req.query.story) ? req.query.story : '';


    console.log("storageData ******* ", storageData)
        // client.renderData["url"] = url;

    let clientLogs = await db.Logs.findOne({ clientId: decodedToken.clientId, host: host, url: url });
    if (!clientLogs) {
        let saveData = new db.Logs({ clientId: decodedToken.clientId, host: host, url: url });
        await saveData.save();
    } else {
        await db.Logs.updateOne({ clientId: decodedToken.clientId, host: host, url: url }, { $set: { updatedAt: Date.now(), count: clientLogs.count + 1 } });
    }

    if (client) {
        client.renderData["hutk"] = req.query ? req.query.hutk : "";
        client.renderData["url"] = url;
        if (storageData != '') {
            client.renderData["storageData"] = storageData
        }
        if (story != '') {
            client.renderData["story"] = story
        }

          client.renderData["subscribed"] = req.subscribed;
        if((client.renderData["subscribed"] == true ) || (req.isPreviewUrl == true )){
           return res.render("chat.hbs", client.renderData);
        }else{
           return res.render("botPromotionalPage.hbs", client.renderData);
        }
    }
}catch(error){
    console.log('Error in catch block :', error)
}
});

router.get("/fullpage/:id", async function(req, res, next) {
    let clientId = req.params.id;
    let client = await db.Client.findOne({ clientId: clientId });
    // client.renderData["hutk"] = req.query.hutk;

    let host = (req.query.host) ? req.query.host : '';
    let url = (req.query.url) ? req.query.url : '';
    let storageData = (req.query.storageData) ? req.query.storageData : '';
    let story = (req.query.story) ? req.query.story : '';

    console.log("storageData ******* ", storageData)
        // client.renderData["url"] = url;

    let clientLogs = await db.Logs.findOne({ clientId: clientId, host: host, url: url }).allowDiskUse(true);
    if (!clientLogs) {
        let saveData = new db.Logs({ clientId: clientId, host: host, url: url });
        await saveData.save();
    } else {
        await db.Logs.updateOne({ clientId: clientId, host: host, url: url }, { $set: { updatedAt: Date.now(), count: clientLogs.count + 1 } });
    }

    if (client) {
        client.renderData["hutk"] = req.query ? req.query.hutk : "";
        client.renderData["url"] = url;
        if (storageData != '') {
            client.renderData["storageData"] = storageData
        }
        if (story != '') {
            client.renderData["story"] = story
        }

        return res.render("fullchat.hbs", client.renderData);
    }
});

// router.post('/locationcheck',async function (req, res, next){
//   let locationcheck = db.Userchat.findOne({});
// })

router.get("/webhooks", isLoggedIn,accessProFeatures, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        let client = await clientcontroller.clientQuery(req, res);
        if (!client) {
            return res.json({ message: "Session expire! Please login again" });
        }

        let dataToRender = {userTypePro : req.userTypePro, sideNavValidation: await sideNavValidation(req) };

        if (client.type == "basic" || !client.type) {
            return res.render('agentsubscribe.hbs', { clientId: req.user.clientId })
                // return res.render("webhooks.hbs", { type: "basic", sideNavValidation: await sideNavValidation(req) });
        } else {
            return res.render("webhooks.hbs", dataToRender);
        }
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});

router.get("/testBot", isLoggedIn, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        let client = await db.Client.findOne({ clientId: "test_chatbot" });
        if (client) {
            return res.render("testbot.hbs", client.renderData);
        }
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});

router.get("/", async function(req, res, next) {
    if (req.isAuthenticated()) {
        res.redirect("adminPanel");
    } else {
        res.render("login.hbs", { title: "Yugasa Bot Admin" });
    }
});
router.get("/register", function(req, res, next) {
    return res.render("register.hbs");
});

router.get("/logOut", function(req, res, next) {
    req.session.destroy(function(err) {
        req.logout();
        res.render("login.hbs", { title: "Yugasa Bot Admin" });
    });
});

//change for amit_chatbot on 30-03-2021 start
router.get("/adminPanel", isLoggedIn, isUserPro, showActivateButton, async function(req, res) {
    checkForToken(req.user._id).then(async function(response) {
    try{
        var startDate,
            endDate,
            sDate,
            eDate,
            showBClosedCard = false,
            showTotalConvers = true,
            lastChatTime,
            lastLeadTime;
        let totalConversation = (totalLeads = totalBClosed = noOfVisitors =
            totalFallbacks = ignoredFallbacks = internet = linkedin = facebook =
            wordOfMouth = others = 0);
        let sources = { "wa": 0, "web": 0, "tg": 0, "gbm": 0, "fb": 0, "ig": 0, "android": 0, "ios": 0 }

        let totalConversationArray = (totalLeadsArray = totalVisitorsArray =
                totabuisnessclosed = totalFallbacksArray = indexArray = []),
            monthlyLeads = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            months = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

        let monthName = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        let monthLeads = [];
        let clientInfo = await db.Client.findOne({ clientId: req.user.clientId }, { email: 1, name: 1, profile_img: 1, timezone: 1, isHumanAgentEnabled: 1, isActionEnabled: 1, filterDays: 1 });
        const objdata = {
            email: clientInfo.email,
            name: clientInfo.name,
            profile_img: clientInfo.profile_img,
            timezone: clientInfo.timezone,
        };
        if (req.user.clientId == "amit_chatbot") {
            showBClosedCard = true;
            showTotalConvers = false;
        }
        if (req.query.startDate && req.query.endDate) {
            startDate = req.query.startDate;
            endDate = req.query.endDate;

            sDate = dateFormat(new Date(parseInt(startDate)), "dd/mm/yyyy");
            eDate = dateFormat(new Date(parseInt(endDate)), "dd/mm/yyyy");
        } else {
            const days = clientInfo.filterDays ? clientInfo.filterDays : 7;
            var date = new Date();
            var newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - days);
            startDate = Date.parse(newDate);

            endDate = new Date();
            endDate = Date.parse(endDate);

            sDate = dateFormat(startDate, "dd/mm/yyyy");
            eDate = dateFormat(endDate, "dd/mm/yyyy");
        }

        {
            let sdt = (req.query.startDate) ? new Date(parseInt(startDate)) : new Date(new Date().setMonth(new Date().getMonth() - 1));
            let edt = (req.query.endDate) ? new Date(parseInt(endDate)) : new Date();
            while (monthName[sdt.getMonth()] + "-" + sdt.getFullYear().toString() != monthName[edt.getMonth()] + "-" + edt.getFullYear().toString()) {
                let mnth = monthName[sdt.getMonth()] + "-" + sdt.getFullYear().toString();
                monthLeads.push({ month: mnth, value: 0 })
                if (sdt.getMonth() == 11) {
                    sdt = new Date(sdt.getFullYear() + 1, 0, 1);
                } else {
                    sdt = new Date(sdt.getFullYear(), sdt.getMonth() + 1, 1);
                }
            }
            let mnth = monthName[sdt.getMonth()] + "-" + sdt.getFullYear().toString();
            monthLeads.push({ month: mnth, value: 0 })
        }

        let userChat = await db.Userchat.find({
            $and: [
                { clientId: req.user._id },
                {
                    date: { $gte: startDate, $lte: endDate },
                },
            ],
        }).sort("date").allowDiskUse(true);

        let intentAnalytics = await db.Client.aggregate([{
                $lookup: {
                    from: "trees",
                    localField: "_id",
                    foreignField: "client",
                    as: "treeData",
                }
            },
            {
                $unwind: "$treeData"
            },
            {
                $match: { "clientId": req.user.clientId }
            },
            {
                $project: {
                    treeData: 1
                }
            },
        ])

        /**for dashboard graph */
        if (typeof startDate == "string" || typeof endDate == "string") {
            startDate = parseInt(startDate);
            endDate = parseInt(endDate);
        }
        var dateobject = {};
        var buisnessclosed = {};
        var fallabackdata = {};
        var leaddata = {};
        var visitorsData = {};
        for (var j = startDate; j <= endDate; j = j + 86400000) {

            let filterdate = dateFormat(new Date(parseInt(j)), "dd/mm/yyyy");
            dateobject[filterdate] = 0;
            fallabackdata[filterdate] = 0;
            leaddata[filterdate] = 0;
            visitorsData[filterdate] = 0;
            buisnessclosed[filterdate] = 0;
        }
        var lastdate = dateFormat(new Date(parseInt(endDate)), "dd/mm/yyyy");
        obj = {
            [lastdate]: 0,
        };
        Object.assign(dateobject, obj);
        Object.assign(fallabackdata, obj);
        Object.assign(leaddata, obj);
        Object.assign(visitorsData, obj);
        Object.assign(buisnessclosed, obj);

        /************************* */
        if (userChat) {
            noOfVisitors = userChat.length;
            var index = 0;
            for (chat of userChat) {
                sources[getsrc(chat.userId)['src']]++;
                index += 1;
                indexArray.push(index);
                lastChatTime = chat.date ? await chatLeadTime(chat.date) : "";

                var dbdate1 = dateFormat(new Date(chat.createdAt), "dd/mm/yyyy");

                if (visitorsData.hasOwnProperty(dbdate1)) {
                    visitorsData[dbdate1] += 1;
                }

                let len = 5000;

                if (chat.chats.length < len) {
                    len = chat.chats.length;
                }
                console.log(chat.userId, chat.chats.length)

                for (var i = 0; i < len; i++) {
                    var dbdate = dateFormat(
                        new Date(chat.chats[i].createdAt),
                        "dd/mm/yyyy"
                    );

                    if (dateobject.hasOwnProperty(dbdate)) {
                        dateobject[dbdate] += 1;
                    }
                    if (chat.chats[i].text) totalConversation += 1;

                    if (chat.chats[i].intent == "fallback") {
                        totalFallbacks += 1;
                        if (fallabackdata.hasOwnProperty(dbdate)) {
                            fallabackdata[dbdate] += 1;
                        }
                    }
                    if (chat.chats[i].ignoreMsg) {
                        if (
                            chat.chats[i + 1].intent == "fallback" &&
                            chat.chats[i + 1].messageType == "outgoing"
                        ) {
                            ignoredFallbacks += 1;
                        }
                    }

                    //For source of contact
                    if (chat.chats[i].text == "Google Search") internet += 1;
                    if (chat.chats[i].text == "LinkedIn") linkedin += 1;
                    if (chat.chats[i].text == "Facebook") facebook += 1;
                    if (chat.chats[i].text == "Word of Mouth") wordOfMouth += 1;
                    if (chat.chats[i].text == "Others") others += 1;
                }

                if (chat.leadStatus == 1 || chat.email || chat.phone) {
                    totalLeads += 1;
                    if (leaddata.hasOwnProperty(dbdate)) {
                        leaddata[dbdate] += 1;
                    }
                    let M = chat.date.getMonth();
                    let Y = chat.date.getFullYear();
                    let mnth = monthName[M] + "-" + Y.toString();
                    if (months.includes(M)) {
                        monthlyLeads[M] += 1;
                    }
                    let found = monthLeads.find((i) => i.month == mnth);
                    !found
                        ?
                        monthLeads.push({
                            month: mnth,
                            value: 1,
                        }) :
                        found.value++;
                } else if (chat.session) {
                    if (chat.session.email || chat.session.phone) {
                        lastLeadTime = chat.session.createdDate ?
                            await chatLeadTime(chat.session.createdDate) :
                            "";
                        totalLeads += 1;
                        if (leaddata.hasOwnProperty(dbdate)) {
                            leaddata[dbdate] += 1;
                        }
                        let M = chat.date.getMonth();
                        let Y = chat.date.getFullYear();
                        let mnth = monthName[M] + "-" + Y.toString();
                        if (months.includes(M)) {
                            monthlyLeads[M] += 1;
                        }
                        let found = monthLeads.find((i) => i.month == mnth);
                        !found
                            ?
                            monthLeads.push({
                                month: mnth,
                                value: 1,
                            }) :
                            found.value++;
                    }
                }
                if (chat.leadStatus == 1) {
                    totalBClosed += 1;
                    buisnessclosed[dbdate] += 1;
                }
            }
        }
        /**for continuous graph */
        let commu_array = Object.values(dateobject),
            result = commu_array.map(((s) => (a) => (s += a))(0));
        totalConversationArray = result;

        let visitors_array = Object.values(visitorsData),
            visitorsArray = visitors_array.map(((s) => (a) => (s += a))(0));
        totalVisitorsArray = visitorsArray;

        let fallback_arry = Object.values(fallabackdata),
            fallbackarray = fallback_arry.map(((s) => (a) => (s += a))(0));
        totalFallbacksArray = fallbackarray;

        let leadarrayy = Object.values(leaddata),
            leadarray = leadarrayy.map(((s) => (a) => (s += a))(0));
        totalLeadsArray = leadarray;
        let businessclosed = Object.values(buisnessclosed),
            buinessclosedarray = businessclosed.map(((s) => (a) => (s += a))(0));
        totabuisnessclosed = buinessclosedarray;
        /**************************************** */
        let locationData = await db.Userchat.find({ clientId: req.user._id }, { location: 1 }).allowDiskUse(true);

        //Parse location data to be used by the map
        let locations = [];
        locationData.forEach(function(item) {
            if (item.location && item.location.city) {
                let found = locations.find((i) => i.name == item.location.city);
                !found
                    ?
                    locations.push({
                        name: item.location.city,
                        latitude: item.location.latitude,
                        longitude: item.location.longitude,
                        value: 1,
                    }) :
                    found.value++;
            }
        });
        //parsing location data complete
        //for analytics graph
        //For intent analytics
        let analyticsNodeIntents = []
        let userFinalArray = []
        let finalleftNodeArr = []
        let leftNodeDetails = []
        let nodeIdArray = []
        let intentArr = []
        if (intentAnalytics.length > 0 && req.user.clientId != "auro_scholar") {
            let treeIntentData = JSON.parse(intentAnalytics[0].treeData.jsonOfTree)
                /// array of all nodes having analytics option true
            treeIntentData.DTree.forEach(element => {
                if (element.analytics == true) { //for node check if analytics option true 
                    let nodeName = element.node_name
                    let nodeId = element.node_id
                    nodeIdArray.push({ nodeName, nodeId })
                }
            })
            let analyticsintents = await db.Intent.find({
                $and: [
                    { "clientId": req.user.clientId },
                    { "analyticsFlag": true }
                ]
            })

            /// array of all intents having analytics option true
            if (analyticsintents.length > 0) {
                console.log("analyticsintents***********", analyticsintents)
                analyticsintents.forEach(ele => {
                    intentArr.push(ele.tag)
                })
            }
            let chatIntents = userChat.map(a => a.chats);
            console.log("chatIntents", chatIntents)
            let chatIntentsArray = chatIntents.flat(1)
            console.log("chatIntentsArray", chatIntentsArray)
            let outgoingIntentsArr = []
            chatIntentsArray.forEach(ele => {
                if (ele.messageType == "outgoing") {
                    outgoingIntentsArr.push(ele.intent)
                }
            })
            console.log("outgoingIntentsArr", outgoingIntentsArr)

            nodeIdArray.forEach(element => {
                let count = getOccurrence(outgoingIntentsArr, element.nodeId)
                if (count > 0) {
                    analyticsNodeIntents.push({ "nodeName": element.nodeName, "count": count })
                }
            })
            console.log("analyticsNodeIntents", analyticsNodeIntents)
            intentArr.forEach(element => {
                let intentCount = getOccurrence(outgoingIntentsArr, element)
                if (intentCount > 0) {
                    analyticsNodeIntents.push({ "nodeName": element, "count": intentCount })
                }
            })
            console.log("analyticsNodeIntents", analyticsNodeIntents)

            //Per user node and intent analytics
            /************************************************************************** */

            userChat.forEach(element => {
                let userIntents = []
                let userIntentsObjArr = []
                let userIntentsObj = { "userId": element.userId, "userName": element.name }
                element.chats.forEach(ele => {
                    if (ele.messageType == "outgoing") {
                        userIntents.push(ele.intent)
                    }
                })

                nodeIdArray.forEach(nodeElement => {
                    let count = getOccurrence(userIntents, nodeElement.nodeId)
                    let key = nodeElement.nodeName
                    if (count > 0) {
                        userIntentsObj[key] = count
                    }
                })
                intentArr.forEach(intentElement => {
                        let intentCount = getOccurrence(userIntents, intentElement)
                        let key1 = intentElement
                        if (intentCount > 0) {
                            userIntentsObj[key1] = intentCount
                        }
                    })
                    // userFinalArray.push({"intentArr":userIntentsObjArr, "userId": element.userId, "userName": element.name})
                console.log("userIntentsObj", userIntentsObj)
                userFinalArray.push(userIntentsObj)
            })
            console.log("userFinalArray", userFinalArray)

            /************************************************************************** */
            /**************************************************************************** */
            //count the communication left node in given time period
            let leftNodeArray = []

            let userChats1 = await db.Userchat.find({
                $and: [
                    { clientId: req.user._id },
                    {
                        updatedAt: { $gte: startDate, $lte: endDate },
                    },
                ],
            }).sort("updatedAt").allowDiskUse(true);
            console.log("userChats1", userChats1.length)

            userChats1.forEach(e => {
                console.log("E", e)
                let leftIntentUserData = { "userId": e.userId, "userName": e.name }
                console.log("leftIntentUserData", leftIntentUserData)

                let outgoingMsg = e.chats.filter(async(filterObj) => await (filterObj.messageType == 'outgoing'))
                console.log("leftIntentUserData*****", leftIntentUserData)
                let lastEle = outgoingMsg.pop()
                leftNodeArray.push(lastEle.intent)
                leftIntentUserData['intent'] = lastEle.intent
                leftNodeDetails.push(leftIntentUserData)
                console.log("leftNodeDetails###########", leftNodeDetails)
            })
            console.log("leftNodeDetails&&&&&&&&&&&&", leftNodeDetails.length, leftNodeDetails)
            nodeIdArray.forEach((element, index) => {
                let count = getOccurrence(leftNodeArray, element.nodeId)
                console.log("leftNodeArray" + index, element.nodeId)
                if (count > 0) {
                    finalleftNodeArr.push({ "nodeName": element.nodeName, "count": count })
                }
            })
            intentArr.forEach((element, index) => {
                let intentCount = getOccurrence(leftNodeArray, element)
                console.log("leftNodeArray" + index, element)
                if (intentCount > 0) {
                    finalleftNodeArr.push({ "nodeName": element, "count": intentCount })
                }
            })
        }

        const dataObj = {
            clientName: req.user.name,
            sideNavValidation: await sideNavValidation(req),
            // logo: profile.logoImage,
            profilePic: req.user.profile_img,
            totalConversation: abbrNum(totalConversation, 1),
            totalLeads: abbrNum(totalLeads, 1),
            totalBClosed: abbrNum(totalBClosed, 1),
            showBClosedCard: showBClosedCard,
            showTotalConvers: showTotalConvers,
            noOfVisitors: abbrNum(noOfVisitors, 1),
            totalFallbacks: abbrNum(totalFallbacks - ignoredFallbacks, 1),
            internet: internet,
            linkedin: linkedin,
            facebook: facebook,
            wordOfMouth: wordOfMouth,
            others: others,
            sources: sources,
            totalConversationArray: totalConversationArray,
            totalVisitorsArray: totalVisitorsArray,
            totalLeadsArray: totalLeadsArray,
            totalFallbacksArray: totalFallbacksArray,
            totalbusinessclosedArraay: totabuisnessclosed,
            startDateInMs: startDate,
            monthlyLeads: monthlyLeads,
            monthLeads: JSON.stringify(monthLeads),
            endDateInMs: endDate,
            indexArray: indexArray,
            startDate: sDate,
            endDate: eDate,
            lastChatTime: lastChatTime,
            lastLeadTime: lastLeadTime,
            locations: JSON.stringify(locations),
            totalConversationDaily: commu_array,
            totalVisitorsDaily: visitors_array,
            totalLeadsDaily: leadarrayy,
            totalFallbacksDaily: fallback_arry,
            totalbusinessclosedDaily: businessclosed,
            showCampaignTabs: (req.user.clientId == "yugasa_chatbot") ? true : false,
            userTypePro : req.userTypePro, 
            showActivateButton : req.showActivateButton,
            intentSources: JSON.stringify(analyticsNodeIntents),
            userIntents: JSON.stringify(userFinalArray),
            leftNodes: JSON.stringify(finalleftNodeArr),
            node: JSON.stringify(nodeIdArray),
            intents: JSON.stringify(intentArr),
            leftNodeDetails: JSON.stringify(leftNodeDetails),
            filterDays: clientInfo.filterDays
        };
        console.log("dataObj", dataObj)
        Object.assign(dataObj, objdata);
        return res.render("dashboard.hbs", dataObj);
    }catch(error){
        console.log('An error occurs in the catch block:-', error)
    }
    }).catch(err => {
        console.log('err:---------------------------------------------------------', err)
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});

//change for amit_chatbot on 30-03-2021 end

//change for amit_chatbot on 30-03-2021 end

router.get("/chat", isLoggedIn, isUserPro, showActivateButton, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
    try{
        var startDate,
            endDate,
            sDate,
            eDate = false;

        if (req.query.startDate && req.query.endDate) {

            startDate = req.query.startDate;
            endDate = req.query.endDate;
            sDate = dateFormat(new Date(parseInt(startDate)), "dd/mm/yyyy");
            eDate = dateFormat(new Date(parseInt(endDate)), "dd/mm/yyyy");
        } else {
            // startDate = new Date().setMonth(new Date().getMonth() - 1);
            var date = new Date();
            var newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 7);
            startDate = Date.parse(newDate);

            endDate = new Date();
            endDate = Date.parse(endDate);
            sDate = dateFormat(startDate, "dd/mm/yyyy");
            eDate = dateFormat(endDate, "dd/mm/yyyy");
        }

        var users = await db.Userchat.find({
            $and: [
                { clientId: req.user._id },
                {
                    date: { $gte: startDate, $lte: endDate },
                },
            ],
        }).sort("-date").allowDiskUse(true);
        var data = [];
        var index = 1,
            filterValue;
        var imgType,
            pdfType,
            wordType,
            username = "",
            location = "";
        for (user of users) {
            if (user.session) {
                username = user.session.name;
            } else {
                username = '';
            }
            if (user.location && user.location.city) {
                location = user.location.city;
            } else {
                location = '';
            }
            var fileupload = JSON.parse(JSON.stringify(user._doc.fileUploaded));
            var now = new Date();
            var chatDate = new Date(user.date);
            var currentdate =
                ("0" + now.getDate()).slice(-2) +
                "-" +
                ("0" + (now.getMonth() + 1)).slice(-2) +
                "-" +
                now.getFullYear();
            var dbdate =
                ("0" + chatDate.getDate()).slice(-2) +
                "-" +
                ("0" + (chatDate.getMonth() + 1)).slice(-2) +
                "-" +
                chatDate.getFullYear() +
                " " +
                ("0" + chatDate.getHours()).slice(-2) +
                ":" +
                ("0" + chatDate.getMinutes()).slice(-2);
            const date = new Date(now);
            date.setDate(date.getDate() - 1);
            const prevdate =
                ("0" + date.getDate()).slice(-2) +
                "-" +
                ("0" + (date.getMonth() + 1)).slice(-2) +
                "-" +
                date.getFullYear();

            let clientId = req.user.clientId;
            // let client = await db.Client.findOne({ clientId: clientId });
            // let dbtimezone = client._doc.timezone;
            let dbtimezone = req.user.timezone;

            // if (dbtimezone == "") {
            //   dbtimezone = "Asia/Kolkata";
            // }
            // const timechat = moment
            //   .utc(chatDate, "YYYYMMDD HH:mm:ss")
            //   .clone()
            //   .tz(dbtimezone)
            //   .format("HH:mm");
            // const Chatdate = moment
            //   .utc(chatDate, "YYYYMMDD HH:mm:ss")
            //   .clone()
            //   .tz(dbtimezone)
            //   .format("DD-MM-YYYY");

            const timechat = dateTimeConverter(chatDate, dbtimezone, "HH:mm");
            const Chatdate = dateTimeConverter(chatDate, dbtimezone, "DD-MM-YYYY");

            /** for time extraction */

            if (Chatdate == currentdate) {
                chatTime = "Today at " + timechat;
            } else if (Chatdate == prevdate) {
                chatTime = "Yesterday at " + timechat;
            } else {
                chatTime = Chatdate + " at " + timechat;
            }

            let userphone = user.phone == null ? "" : user.phone;
            let useremail = user.email == null ? "" : user.email;
            if (user.session) {
                if (Array.isArray(user.session.phone)) {
                    userphone =
                        userphone == "" ?
                        user.session.phone.join(", ") :
                        user.session.phone.join().includes(userphone) ?
                        user.session.phone.join(", ") :
                        userphone + ", " + user.session.phone.join(", ");
                } else if (user.session.phone) {
                    userphone = user.session.phone;
                }

                if (Array.isArray(user.session.email)) {
                    useremail =
                        useremail == "" ?
                        user.session.email.join(", ") :
                        user.session.email.join().includes(useremail) ?
                        user.session.email.join(", ") :
                        useremail + ", " + user.session.email.join(", ");
                } else if (user.session.email) {
                    useremail = user.session.email;
                }
            }

            let updatedUserName
            console.log("user.name*******", user.name)
            if (user.name !== undefined && user.name !== "") {
                let visitorName = user.name.split(/[0-9]/)[0]
                if (visitorName == "Visitor") {
                    updatedUserName = username
                } else {
                    updatedUserName = user.name
                }
            } else {
                updatedUserName = username
            }

            var obj;
            if (req.query.filter == "Leads") {
                if (useremail || userphone) {
                    filterValue = "Leads";
                    obj = {
                        index: index,
                        userId: user.userId,
                        email: useremail,
                        name: updatedUserName, //user.name,
                        status: user.userStatus,
                        time: chatTime,
                        phone: userphone,
                        location: location,
                        //name: username,
                        src: getsrc(user.userId)
                    };
                    if (fileupload.length > 0) {
                        let imgType = "1";
                        Object.assign(obj, { imgType: imgType });
                    }
                    index++;
                    data.push(obj);
                }
            } else if (req.query.userId) {
                console.log("TYPE", typeof(req.query.userId), req.query.userId)
                if (req.query.userId.includes(',')) {
                    let userArray = req.query.userId.split(',')
                    console.log("USER ARRAY ", userArray)
                    userArray.forEach(element => {
                        if (user.userId == element) {
                            filterValue = "userIntents";
                            obj = {
                                index: index,
                                userId: user.userId,
                                email: useremail,
                                name: updatedUserName, //user.name,
                                status: user.userStatus,
                                time: chatTime,
                                phone: userphone,
                                location: location,
                                //name: username,
                                src: getsrc(user.userId)
                            };
                            if (fileupload.length > 0) {
                                let imgType = "1";
                                Object.assign(obj, { imgType: imgType });
                            }
                            data.push(obj);
                        }
                    })
                    index++;
                } else {
                    console.log("IN ELSE LOOP")
                    if (user.userId == req.query.userId) {
                        filterValue = "userIntents";
                        obj = {
                            index: index,
                            userId: user.userId,
                            email: useremail,
                            name: updatedUserName, //user.name,
                            status: user.userStatus,
                            time: chatTime,
                            phone: userphone,
                            location: location,
                            //name: username,
                            src: getsrc(user.userId)
                        };
                        if (fileupload.length > 0) {
                            let imgType = "1";
                            Object.assign(obj, { imgType: imgType });
                        }
                        index++;
                        data.push(obj);
                    }
                }
            } else {
                filterValue = "All";
                obj = {
                    index: index,
                    userId: user.userId,
                    email: useremail,
                    name: updatedUserName, //user.name,
                    status: user.userStatus,
                    time: chatTime,
                    phone: userphone,
                    location: location,
                    //name: username,
                    src: getsrc(user.userId)
                };
                if (fileupload.length > 0) {
                    let imgType = "1";
                    Object.assign(obj, { imgType: imgType });
                }
                index++;
                data.push(obj);
            }
        }

        if (data.length > 500) {
            data = data.slice(0, 500)
        }

        return res.render("userCommunication.hbs", {
            data: data,
            filterValue: filterValue,
            startDate: sDate,
            endDate: eDate,
            milistartDate: startDate,
            miliendDate: endDate,
            userTypePro : req.userTypePro,
            showActivateButton : req.showActivateButton,
            sideNavValidation: await sideNavValidation(req)
        });
    }catch(error){
        console.log('An error occurs in the catch block:-', error)
    }
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});
/**for clip icon on usercommunication page */
router.post("/imageupload", isLoggedIn, async function(req, res) {
    const getQuery = await db.Userchat.findOne({ userId: req.body.userId }, { fileUploaded: 1 }).allowDiskUse(true);
    let uploadedfile = JSON.parse(JSON.stringify(getQuery.fileUploaded));
    return res.json({ data: uploadedfile });
});
/********************************************** */

router.get("/possibleconflicts", isLoggedIn, async function(req, res) {
    fallbackFunc(req, res);
});

async function fallbackFunc(req, res) {
try{
    var startDate, endDate, sDate, eDate, showSDate, showEDate;
    if (req.query.startDate && req.query.endDate) {
        startDate = req.query.startDate;
        endDate = req.query.endDate;

        sDate = dateFormat(new Date(parseInt(startDate)), "yyyy/mm/dd");
        eDate = dateFormat(new Date(parseInt(endDate)), "yyyy/mm/dd");

        showSDate = dateFormat(new Date(parseInt(startDate)), "dd/mm/yyyy");
        showEDate = dateFormat(new Date(parseInt(endDate)), "dd/mm/yyyy");

    } else {
        //startDate = new Date().setMonth(new Date().getMonth() - 1);
        var date = new Date();
        var newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 7);
        startDate = Date.parse(newDate);
        endDate = new Date();

        sDate = dateFormat(startDate, "yyyy/mm/dd");
        eDate = dateFormat(endDate, "yyyy/mm/dd");

        showSDate = dateFormat(startDate, "dd/mm/yyyy");
        showEDate = dateFormat(endDate, "dd/mm/yyyy");

    }

    let userChat = await db.Userchat.aggregate([

        {
            $match: {
                clientId: new ObjectID(req.user._id),
            }
        },
        {
            $match: {
                $and: [{
                    updatedAt: {
                        $gte: new Date(sDate)
                    }
                }, {
                    updatedAt: {
                        $lte: new Date(eDate)
                    }
                }]
            }
        },
        {
            $project: {
                chats: 1,
                userId: 1,
                createdAt: 1,
                _id: 0
            }
        }
        // Expand the scores array into a stream of documents
        // { $unwind: '$chats' },
        // Sort in descending order
        // { $sort: {
        //    'chats.createdAt': -1
        // }}
    ]).sort("-createdAt").allowDiskUse(true);

    var newArrData = [];
    for (var obj in userChat) {
        var newObj = userChat[obj].chats
        for (var obj2 in newObj) {
            var newData = newObj[obj2]
            newData['userId'] = userChat[obj].userId
        }
        newArrData.push(newObj);
    }
    var userChatData = [].concat.apply([], newArrData);

    var data = [],
        index = 1,
        text,
        sNo = 0;
    for (var i = 1; i < userChatData.length; i++) {
        if (userChatData[i].intent == "fallback") {

            if (userChatData[i - 1]) {
                if (userChatData[i - 1].ignoreMsg == true) {
                    text = "";
                } else if (
                    userChatData[i - 1].messageType == "incoming" &&
                    userChatData[i - 1].text != '' && userChatData[i - 1].text != null
                ) {
                    //for date on fallback************
                    var date1 = new Date(userChatData[i - 1].createdAt);

                    //     var fallbacktime = dateTimeConverter(date1, req.user.timezone, "HH:mm");
                    //     var fallbackdate = dateTimeConverter(date1, req.user.timezone, "DD-MM-YYYY");

                    //    var now = new Date();

                    //    var currentdate =
                    //        ("0" + now.getDate()).slice(-2) +
                    //        "-" +
                    //        ("0" + (now.getMonth() + 1)).slice(-2) +
                    //        "-" +
                    //        now.getFullYear();

                    //     const date = new Date(now);
                    //     date.setDate(date.getDate() - 1);

                    //     const prevdate =
                    //         ("0" + date.getDate()).slice(-2) +
                    //         "-" +
                    //         ("0" + (date.getMonth() + 1)).slice(-2) +
                    //         "-" +
                    //         date.getFullYear();    

                    //    if (fallbackdate == currentdate) {
                    //        fallbackdate = "Today at " + fallbacktime;
                    //    } else if (fallbackdate == prevdate) {
                    //        fallbackdate = "Yesterday at " + fallbacktime;
                    //    } else {
                    //        fallbackdate = fallbackdate + " at " + fallbacktime;
                    //    }

                    text = userChatData[i - 1].text;
                    sNo++;
                }
            }

            var obj = {
                text: text,
                index: index,
                sNo: sNo,
                userId: userChatData[i - 1].userId,
                // date: fallbackdate
                date: date1
            }
            index++;
            data.push(obj);
        }
    }

    var newData = data.filter((value, index, array) =>
        array.findIndex(item => (item.text === value.text && item.date === value.date && item.sNo === value.sNo)) === index)

    var newDatas = newData.sort(function(a, b) {
        return new Date(b.date) - new Date(a.date);
    });

    var filterData = []
    for (var filterObj in newDatas) {
        if (newDatas[filterObj].text != '') {
            filterData.push(newDatas[filterObj])
        }
    }

    var i = 1;
    for (var newobj in filterData) {
        var updateObj = filterData[newobj]
        updateObj['sNo'] = i
        updateObj['date'] = moment(updateObj.date).format('DD-MM-YYYY, HH:mm');
        i = i + 1;
    }

    return res.render("fallback.hbs", {
        data: filterData,
        sDate: showSDate,
        eDate: showEDate,
        userTypePro : req.userTypePro,
        showActivateButton : req.showActivateButton,
        sideNavValidation: await sideNavValidation(req)
    });
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

router.get("/fallbacks", isLoggedIn, isUserPro, showActivateButton, async function(req, res) {
    fallbackFunc(req, res);
});

async function logsFunc(req, res) {
try{
    var startDate, endDate, sDate, eDate;
    if (req.query.startDate && req.query.endDate) {
        startDate = req.query.startDate;
        endDate = req.query.endDate;

        sDate = dateFormat(new Date(parseInt(startDate)), "dd/mm/yyyy");
        eDate = dateFormat(new Date(parseInt(endDate)), "dd/mm/yyyy");
    } else {
        startDate = new Date().setMonth(new Date().getMonth() - 1);
        endDate = new Date();

        sDate = dateFormat(startDate, "dd/mm/yyyy");
        eDate = dateFormat(endDate, "dd/mm/yyyy");
    }

    let client = req.user.clientId;

    var dbtimezone = 'Asia/Kolkata';

    let logs = [];
    if (client) {
        logs = await db.Logs.find({
            clientId: client,
            updatedAt: { $gte: startDate, $lte: endDate },
        }).sort(
            "-updatedAt"
        ).allowDiskUse(true);
    } else {
        logs = await db.Logs.find({
            updatedAt: { $gte: startDate, $lte: endDate },
        }).sort(
            "-updatedAt"
        ).allowDiskUse(true);
    }
    var data = [],
        sNo = 0;
    let fbcount = 0,
        orgcount = 0,
        instacount = 0,
        goglcount = 0;
    let camp = (req.query.campaign) ? req.query.campaign : '';
    logs.forEach((item) => {
        //let createdAt = dateFormat(item.createdAt, "dd-mmm-yyyy HH:MM:ss Z");
        let updatedAt = moment(item.updatedAt, 'YYYY/MM/DD HH:mm:ss ZZ').tz(dbtimezone).format('DD-MMM-YYYY HH:mm:ss');
        //item.updatedAt = moment.tz(item.updatedAt, dbtimezone).format("DD-MM-YYYY HH:mm:ss");

        let campaign;
        if ((item.url.search("fbclid") >= 0) || (item.url.search("utm_source=facebook") >= 0)) {
            campaign = { "campaign": "facebook", "title": "Facebook Ads" };
            fbcount = fbcount + item.count;
        } else if ((item.url.search("gclid") >= 0) || (item.url.search("utm_source=google") >= 0)) {
            campaign = { "campaign": "google", "title": "Google Ads" };
            goglcount = goglcount + item.count;
        } else if (item.url.search("utm_source=instagram") >= 0) {
            campaign = { "campaign": "instagram", "title": "Instagram Ads" };
            instacount = instacount + item.count;
        } else {
            campaign = { "campaign": "web", "title": "Organic Search" }
            orgcount = orgcount + item.count;
        }
        if ((camp == '') || (camp.search(campaign.campaign) >= 0)) {
            sNo++;
            data.push({ clientId: item.clientId, host: item.host, url: item.url, updatedAt: updatedAt, sNo: sNo, count: item.count, campaign: campaign });
        }
    });

    return res.render("logs.hbs", {
        data: data,
        sDate: sDate,
        eDate: eDate,
        fbcount,
        orgcount,
        instacount,
        goglcount,
        userTypePro : req.userTypePro,
        camp: camp,
        sideNavValidation: await sideNavValidation(req)
    });
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
}

router.get("/logs", isLoggedIn,isUserPro, async function(req, res) {
    logsFunc(req, res);
});

router.get("/story", isLoggedIn,isUserPro, showActivateButton,async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        let dataToRender = { sideNavValidation: await sideNavValidation(req) };
        if (req.user.clientId == "patton_chatbot") {
            res.render("basic.hbs", dataToRender);
            return;
        }
        let clientId = req.user.clientId;
        let isGenAiEnabled=false;
        const clientData = await db.Client.findOne({ clientId: clientId }, { genAiConfig: 1 , isGenAiEnabled: 1})
        let genAiConfig = clientData.genAiConfig;
        if(genAiConfig!=undefined && clientData.isGenAiEnabled==true){
            if(genAiConfig.genai_key!="" && genAiConfig.genai_key!=undefined){
                isGenAiEnabled=true;
            }
        }
        fs.readFile("./upload/" + clientId + ".json", async function read(err, data) {
            if (err) {
                if(isGenAiEnabled){
                    res.render("story.hbs", { userTypePro : req.userTypePro, genAiStory : true, showActivateButton : req.showActivateButton,"isGenAiEnabled":isGenAiEnabled, sideNavValidation: await sideNavValidation(req) });
                }else if (req.userTypePro ){
                    res.render("story.hbs", { userTypePro : req.userTypePro, genAiStory : false, showActivateButton : req.showActivateButton,"isGenAiEnabled":isGenAiEnabled, sideNavValidation: await sideNavValidation(req) });
                }else{
                    res.render("storyAdSection.hbs",{userTypePro : req.userTypePro, showActivateButton : req.showActivateButton});
                }
            } else {
                let tree = JSON.parse(data);
                tree.sideNavValidation = await sideNavValidation(req)
                if(isGenAiEnabled){
                    tree.about+=(tree.services!="")?"\n"+tree.services:"";
                    tree.about+=(tree.terms!="")?"\n"+tree.terms:"";
                    tree.about+=(tree.contact!="")?"\n"+tree.contact:"";
                    tree.about+=(tree.others!="")?"\n"+tree.others:"";
                    res.render("story.hbs", { userTypePro : req.userTypePro, genAiStory : true, showActivateButton : req.showActivateButton,"story":tree,"isGenAiEnabled":isGenAiEnabled});
                } else if(req.userTypePro){
                    res.render("story.hbs", { userTypePro : req.userTypePro, genAiStory : false, showActivateButton : req.showActivateButton,"story":tree,"isGenAiEnabled":isGenAiEnabled});
                } else {
                    res.render("storyAdSection.hbs",{userTypePro : req.userTypePro, showActivateButton : req.showActivateButton});
                }
            }
        });
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});

router.get("/tree", isLoggedIn, isUserPro, showActivateButton, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        let dataToRender = { userTypePro : req.userTypePro,showActivateButton : req.showActivateButton, sideNavValidation: await sideNavValidation(req) };
        if (req.user.clientId == "patton_chatbot") {
            res.render("basic.hbs", dataToRender);
            return;
        }
        dataToRender.clientId = req.user.clientId;
        return res.render("tree.hbs", dataToRender);
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});

// router.get('/branchingtree', function (req, res, next) {
//     res.render('tree1.hbs', { clientId: "btrack_chatbot" });
// });

router.get("/subscription", isLoggedIn,isUserPro, showActivateButton, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
    try{

        let dataToRender = { sideNavValidation: await sideNavValidation(req) };
        if (req.user.clientId == "patton_chatbot") {
            res.render("basic.hbs", dataToRender);
            return;
        }
        let clientData = await db.Client.findOne({ clientId: req.user.clientId });
        const date1 = new Date(clientData.createdAt);
        const date2 = new Date();
        const diffTime = Math.abs(date2 - date1);
        const diffDays = 30 - Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        var remainingDays,
            renew = false;
        if (diffDays <= 30 && diffDays > 0) {
            remainingDays = diffDays;
        } else if (diffDays <= 0) {
            renew = true;
        }

        // let profile = await db.Settings.findOne({ clientId: req.user._id });
        // if (profile.logoImage) {
        //     res.render('subscription.hbs', {
        //         logo: profile.logoImage
        //     });
        // }
        // else {
        let userchat = await db.Userchat.aggregate([{
                $match: {
                    $or: [
                        { clientId: req.user.clientId },
                        {
                            clientId: ObjectID(req.user._id),
                        },
                    ],
                },
            },
            {
                $group: {
                    _id: "$clientId",
                    total_sum: { $sum: { $size: "$chats" } },
                },
            },
        ]).allowDiskUse(true);

        let clientTemplateData = await db.Client.findOne({ clientId: req.user.clientId });
        let templateFind;
        if (clientTemplateData.agentTemplate == "") {
            templateFind = "Not Selected"
        } else {
            templateFind = await db.whatsAppTemplate.findOne({ _id: clientTemplateData.agentTemplate });
            templateFind = templateFind.name
        }

        res.render("subscription.hbs", {
            clientData : clientTemplateData.restartMessageStatus,
            clientId: req.user.clientId,
            templateName: templateFind,
            userTypePro : req.userTypePro,
            showActivateButton : req.showActivateButton,
            custom_design: clientData.custom_design ? true : false,
            remainingDays: remainingDays,
            renew: renew,

            clientId: req.user.clientId,
            chatcount: userchat[0] ? userchat[0].total_sum : 0,

            sideNavValidation: await sideNavValidation(req),
            isHumanAgentEnabled: req.user.isHumanAgentEnabled,
            isCampaigningEnabled: req.user.isCampaigningEnabled,
            isGenAiEnabled: true
        });
    }catch(error){
        console.log('An error occurs in the catch block:-', error)
    }
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});

router.post('/incoming', async function(req, res){  
  // Parse the request body from the POST
  console.log("incoming received ",req.body)
  res.status(200).send({"status":"success", "body":req.body})

});

router.get("/intent", isLoggedIn, isUserPro, showActivateButton, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        let dataToRender = { sideNavValidation: await sideNavValidation(req) };
        if (req.user.clientId == "patton_chatbot") {
            res.render("basic.hbs", dataToRender);
            return;
        }
        res.render("intent.hbs", {
            keyword: req.query.keyword,
            type: req.user.type,
            userTypePro : req.userTypePro,
            showActivateButton : req.showActivateButton,
            sideNavValidation: await sideNavValidation(req)
        });
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});

router.get("/login", function(req, res, next) {
    res.render("login.hbs");
});

router.get("/settings", isLoggedIn, function(req, res, next) {
    res.redirect("/subscription");
});

router.post("/checkClientDetails", isLoggedIn, async function(req, res, next) {
    let authToken = await jwt.sign({ host: req.body.referer }, "Yubo");
    //if(req.body.referer=='localhost'){
    return res.status(200).json({
        status: "success",
        message: "You are not subscribed!",
        token: authToken,
    });
    //}
    // else{
    //   return res.status(200).json({
    //     status:'failed',
    //     message:'You are subscribed!',
    //     data:data
    //   });
    // }
});

router.post(
    "/settings",
    isLoggedIn,
    upload.single("logoImage"),
    async function(req, res) {
    try{
        let settingsData = await db.Settings.findOne({ clientId: req.user._id });
        if (!settingsData) {
            if (req.body.registeredEmail) {
                if (req.body.registeredEmail == req.user.email) {
                    req.body.clientId = req.user._id;
                    req.body.backgroundColor = req.body.checkbox;
                    if (req.file) {
                        req.body.logoImage = req.file.filename;
                    }
                    let saveData = new db.Settings(req.body);
                    await saveData.save();
                    req.flash("success_msg", "Data saved successfully.");
                } else {
                    req.flash("error_msg", "Entered email id is not registered");
                }
            } else {
                req.body.clientId = req.user._id;
                req.body.backgroundColor = req.body.checkbox;
                if (req.file) {
                    req.body.logoImage = req.file.filename;
                }
                let saveData = new db.Settings(req.body);
                await saveData.save();
                req.flash("success_msg", "Data saved successfully.");
            }
        } else {
            var objForUpdate = {};
            if (req.body.checkbox) objForUpdate.backgroundColor = req.body.checkbox;
            if (req.body.onlineStatus)
                objForUpdate.onlineStatus = req.body.onlineStatus;
            if (req.file) objForUpdate.logoImage = req.file.filename;
            if (req.body.widgetColor) objForUpdate.widgetColor = req.body.widgetColor;
            if (req.body.emailCheckbox)
                objForUpdate.emailCheckbox = req.body.emailCheckbox;
            if (req.body.registeredEmail)
                objForUpdate.registeredEmail = req.body.registeredEmail;
            if (req.body.industryType)
                objForUpdate.industryType = req.body.industryType;
            if (req.body.bgTemplate) objForUpdate.bgTemplate = req.body.bgTemplate;
            if (req.body.chatbotDevice)
                objForUpdate.chatbotDevice = req.body.chatbotDevice;
            if (req.body.websiteURL) objForUpdate.websiteURL = req.body.websiteURL;
            if (req.body.chatbotVisibility)
                objForUpdate.chatbotVisibility = req.body.chatbotVisibility;
            if (req.body.registeredEmail) {
                if (req.body.registeredEmail == req.user.email) {
                    objForUpdate = { $set: objForUpdate };
                    await db.Settings.updateOne({ clientId: req.user._id }, objForUpdate);
                    req.flash("success_msg", "Data updated successfully");
                } else {
                    req.flash("error_msg", "Entered email id is not registered");
                }
            } else {
                objForUpdate = { $set: objForUpdate };
                await db.Settings.updateOne({ clientId: req.user._id }, objForUpdate);
                req.flash("success_msg", "Data updated successfully");
            }
        }
        res.redirect("/subscription");
    }catch(error){
        console.log('An error occurs in the catch block:-', error)
    }
    }
);

router.get("/clientAuthentication/:token?", isLoggedIn, function(req, res) {
    var token = req.params.token;
    // var headers=
    //    { host: '172.105.42.134:4900',
    //      'user-agent': 'WordPress/5.2.2; http://localhost/positively-perfect',
    //      accept: '*/*',
    //      'accept-encoding': 'deflate, gzip',
    //      referer: 'http://172.105.42.134:4900/checkClientDetails',
    //      'content-type': 'application/json; charset=utf-8',
    //      connection: 'close',
    //      'content-length': '23' }
});

router.post("/subscribeYubo", isLoggedIn, async function(req, res, next) {
    let client = getClient(req);
    var data = await axios({
        method: "post",
        url: "http://172.105.42.134:5500/yugasa",
        data: { data: req.body },
    });
    return res.json({ success: true, message: data.data });
});

router.post("/trainYubo", isLoggedIn, async function(req, res, next) {
    let client = getClient(req);
    var data = await axios({
        method: "post",
        url: "http://172.105.42.134:5500/yugasa",
        data: { data: req.body },
    });
    return res.json({ success: true, message: data.data });
});

async function add(req, res, query) {
    // const query = await db.Intent.find({ clientId: req.user.clientId }).sort('-createdAt');
    const IntensVersion = await db.IntentV.find({ clientId: req.user.clientId });

    const arrayF = [];
    for (data of query) {
        arrayF.push(data._doc);
    }
    if (IntensVersion.length >= 5) {
        await db.IntentV.deleteOne({ clientId: req.user.clientId });
    }

    let versionData = new db.IntentV({
        clientId: req.user.clientId,
        versionData: arrayF,
    });
    await versionData.save();
    return res.send(query);
}

/**###############Intent page functionality ########################*/
router.post("/addIntentData", isLoggedIn, async(req, res, next) => {
try{
    req.body.tag      = escapeHtml(req.body.tag);
    req.body.pattern  = req.body.pattern;
    req.body.response = req.body.response;
    req.body.entites  = req.body.entites; 
    let clientId = req.user.clientId;
    let client = await db.Client.findOne({ clientId: clientId });
    if (!client) {
        return res.json({ message: "Session expire! Please login again" });
    }
    const tagName_exist = await db.Intent.find({
        clientId: clientId,
        tag: req.body.tag
            // tag: { $regex: req.body.tag, $options: "$i" },
    });

    // console.log('tagname', tagName_exist)

    if (tagName_exist.length > 0) {
        return res.send("exists");
    } else {
        let data = new db.Intent({
            clientId: req.user.clientId,
            tag: req.body.tag,
            patterns: req.body.pattern,
            responses: req.body.response,
            entities: req.body.entites
        });
        await data.save();

        let isExists = intentscontroller.checkdirexist(clientId);
        if (isExists && req.body.entites) {
            let updatefile = intentscontroller.updatedirWritefile(clientId, req.body);
        } else if (req.body.entites) {
            let data = intentscontroller.createEntitydata(req.body);
            let createfile = intentscontroller.createdirWritefile(clientId, data)
        }


        //To create entities.json file
        if (req.body.createintent == "") {
            return res.send("intent created successfully");
        }
        /** for ignore message */
        if (req.body.text == "") {
            req.body.userId.forEach(async function(item, index) {
                var dbD = await db.Userchat.findOne({ userId: item.id }).allowDiskUse(true);
                var indx;
                for (var i = 0; i < dbD.chats.length; i++) {
                    if (
                        dbD.chats[i].text == item.text &&
                        dbD.chats[i].ignoreMsg != true
                    ) {
                        indx = i;
                        break;
                    }
                }
                if (indx !== "") {
                    var placeholder = {};
                    placeholder["chats." + indx + ".ignoreMsg"] = true;
                    await db.Userchat.updateOne({ userId: item.id }, {
                        $set: placeholder,
                    }).allowDiskUse(true);
                }
            });
        } else {
            var dbD = await db.Userchat.findOne({ userId: req.body.userId }).allowDiskUse(true);
            var indx;
            for (var i = 0; i < dbD.chats.length; i++) {
                if (
                    dbD.chats[i].text == req.body.text &&
                    dbD.chats[i].ignoreMsg != true
                ) {
                    indx = i;
                    break;
                }
            }

            if (indx !== "") {
                var placeholder = {};
                placeholder["chats." + indx + ".ignoreMsg"] = true;
                await db.Userchat.updateOne({ userId: req.body.userId }, {
                    $set: placeholder,
                }).allowDiskUse(true);
            }
        }
    }

    const query = await db.Intent.find({ clientId: req.user.clientId }).sort(
        "-createdAt"
    );
    await add(req, res, query);
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});

/**Edit intent data used on create intent page*/
router.post("/editIntentData", isLoggedIn, async(req, res) => {
try{
    req.body.tag      = escapeHtml(req.body.tag);
    req.body.pattern  = req.body.pattern;
    req.body.response = req.body.response;
    req.body.entites  = req.body.entites;  
    let clientId = req.user.clientId;
    let client = await db.Client.findOne({ clientId: clientId });
    if (!client) {
        return res.json({ message: "Session expire! Please login again" });
    }
    if (!req.body.entites) {
        req.body.entites = [];
    }
    const intent_exist = await db.Intent.find({ _id: req.body.id });
    //var tag = req.body.tag.charAt(0).toUpperCase() + req.body.tag.slice(1);

    req.body.entites = req.body.entites ? req.body.entites : [];
    if (intent_exist.length > 0) {
        const tagName_exist = await db.Intent.find({
            tag: { $regex: req.body.tag, $options: "$i" },
        });


        // const tagName_existt = await db.Intent.find({
        //     _id: { $nin: req.body.id },
        //     tag: req.body.tag,
        // });

        const tagName_existt = await db.Intent.find({
            _id: { $nin: req.body.id },
            tag: req.body.tag,
            clientId: req.user.clientId
        });

        // const tagName_existt = await db.Intent.find({tag:req.body.tag,  _id: { $nin: req.body.id }});
        // console.log('tagname', tagName_existt, tagName_existt.length)

        if (tagName_existt.length > 0) {
            return res.send("exists");
        } else {
            // console.log('update data success')
            await db.Intent.updateOne({ _id: req.body.id }, {
                $set: {
                    clientId: req.user.clientId,
                    tag: req.body.tag,
                    patterns: req.body.pattern,
                    responses: req.body.response,
                    entities: req.body.entites
                },
            });
            // return res.send('Updated successfully');
            const query = await db.Intent.find({ clientId: req.user.clientId }).sort(
                "-createdAt"
            );
            await add(req, res, query);

        }

        // if ( tagName_exist.length > 0 &&  tagName_exist[0].tag.toLowerCase() == req.body.tag.toLowerCase()) {
        //   if (JSON.stringify(req.body.pattern) != JSON.stringify(intent_exist[0].patterns) ||JSON.stringify(req.body.response) != JSON.stringify(intent_exist[0].responses)) 
        //   {

        //     console.log('dada', JSON.stringify(req.body.pattern),  JSON.stringify(intent_exist[0].patterns), JSON.stringify(req.body.response), 
        //     JSON.stringify(intent_exist[0].responses) )

        //     console.log('update data 01')
        //     await db.Intent.updateOne(
        //       { _id: req.body.id },
        //       {
        //         $set: {
        //           clientId: req.user.clientId,
        //           tag: req.body.tag,
        //           patterns: req.body.pattern,
        //           responses: req.body.response,
        //           entities: req.body.entites
        //         },
        //       }
        //     );
        //     // return res.send('Updated successfully');
        //     const query = await db.Intent.find({
        //       clientId: req.user.clientId,
        //     }).sort("-createdAt");
        //     await add(req, res, query);
        //   } else if (
        //     JSON.stringify(req.body.pattern) ==
        //     JSON.stringify(intent_exist[0].patterns) &&
        //     JSON.stringify(req.body.response) ==
        //     JSON.stringify(intent_exist[0].responses)
        //   ) {

        //      console.log('data ', JSON.stringify(req.body.pattern), JSON.stringify(intent_exist[0].patterns),
        //    JSON.stringify(req.body.response), JSON.stringify(intent_exist[0].responses) )

        //    //  console.log('dadadadada', intent_exist, intent_exist[0].patterns )
        //    return res.send("exists");
        //    // console.log('not success')

        //   }


        // }

        //  else {

        //   console.log('update data 02')
        //   await db.Intent.updateOne(
        //     { _id: req.body.id },
        //     {
        //       $set: {
        //         clientId: req.user.clientId,
        //         tag: req.body.tag,
        //         patterns: req.body.pattern,
        //         responses: req.body.response,
        //         entities: req.body.entites
        //       },
        //     }
        //   );
        //   // return res.send('Updated successfully');
        //   const query = await db.Intent.find({ clientId: req.user.clientId }).sort(
        //     "-createdAt"
        //   );
        //   await add(req, res, query);
        // }


        // code for entites.json file
        let isExists = intentscontroller.checkdirexist(clientId);
        if (isExists) {
            let updatedata = intentscontroller.updatedirWritefile(clientId, req.body)
        } else if (req.body.entites.length > 0) {
            let data = intentscontroller.createEntitydata(req.body);
            let createfile = intentscontroller.createdirWritefile(clientId, data)
        }
    } else {
        return res.send("Something went wrong");
    }
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});


/**This route call training page*/
router.get("/train", isLoggedIn, async(req, res) => {
    checkForToken(req.user._id).then(async function(response) {
        return res.render("train.hbs");
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});

router.post("/getTrainData", isLoggedIn, async(req, res) => {
    let client = await clientcontroller.clientQuery(req, res);
    if (!client) {
        return res.json({ message: "Session expire! Please login again" });
    }

    let clientId = client.clientId,
        // dir = process.env.pythonDir + clientId,
        feed = await db.Intent.find({ clientId: req.user.clientId, active_status: { $ne: "off" } }, { tag: 1, patterns: 1, responses: 1, entities: 1, _id: 0 }, { updatedAt: 0, createdAt: 0 });
    if (feed.length <= 0) {
        return res.send({ message: "empty" });
    }

    // if (!fs.existsSync(dir)) {
    //   fs.mkdirSync(dir, { recursive: true });
    // }
    let obj = {
            intents: feed,
        },
        data = JSON.stringify(obj, null, 2);

    await intentscontroller.chckDir(clientId);
    let resp = await intentscontroller.trainIntents(client.type, data, clientId);
    return res.send({ message: resp });
    // await fs.writeFileSync(
    //   process.env.pythonDir + clientId + "/intents.json",
    //   data,
    //   { encoding: "utf8", flag: "w" },
    //   (err) => {
    //     if (err) {
    //       return res.send({ message: "error" });
    //     }
    //   }
    // );
    // let requestData = {
    //   userId: "c1fcfbca-af31-5885-5779-dc9f0c91dd27",
    //   client_id: clientId,
    //   train: "True",
    //   unsubsribe: "False",
    //   text: "",
    //   node: "False",
    //   session: {
    //     name: "",
    //     phone: "",
    //     email: "",
    //   },
    //   category: req.user.type ? req.user.type : "basic",
    //   update_tree: "False",
    //   update_story: "False",
    // };
    // await axios({
    //   method: "POST",
    //   url: process.env.pythonUrl,
    //   data: { data: requestData },
    // })
    //   .then(function (response) {
    //     if (response.data.status == "success") {
    //       return res.send({ message: "success" });
    //     } else {
    //       return res.send({ message: "error" });
    //     }
    //   })
    //   .catch(function (error) {
    //     return res.send({ message: "error" });
    //   });
});

router.post("/getFaqData", isLoggedIn, async(req, res) => {
    let client = await clientcontroller.clientQuery(req, res);
    if (!client) {
        return res.json({ message: "Session expire! Please login again" });
    }

    let clientId = client.clientId,
        // dir = process.env.pythonDir + clientId,
        feed = await db.ZeroShot.find({ clientId: req.user.clientId, status: { $ne: "false" } }, { tagName: 1, question: 1, answer: 1, _id: 0 }, { updatedAt: 0, createdAt: 0 });
    if (feed.length <= 0) {
        return res.send({ message: "empty" });
    }

    // if (!fs.existsSync(dir)) {
    //   fs.mkdirSync(dir, { recursive: true });
    // }
    let obj = feed,
        data = JSON.stringify(obj, null, 2);

    await intentscontroller.chckDir(clientId);
    let resp = await intentscontroller.trainFaqs(client.type, data, clientId);
    return res.send({ message: resp });
});

router.post("/intent", isLoggedIn, async function(req, res, next) {
    var query,
        type = req.user.type;
    if (req.body.data) {
        query = await db.Intent.find({
            clientId: req.user.clientId,
            $or: [
                { tag: { $regex: req.body.data, $options: "$i" } },
                { patterns: { $regex: req.body.data, $options: "$i" } },
                { responses: { $regex: req.body.data, $options: "$i" } },
            ],
        }).sort("-createdAt");
    } else {
        query = await db.Intent.find({ clientId: req.user.clientId }).sort(
            "-createdAt"
        );
    }
    return res.send({ query: query, type: type });
});

router.post("/intentbyid", isLoggedIn, async(req, res) => {
    const query = await db.Intent.find({ _id: req.body.intentId });
    return res.send(query);
});

router.post("/deleteintent", isLoggedIn, async(req, res, next) => {

    var deleteCaht = await db.Intent.deleteOne({ _id: req.body.intentId });
    let deletedataaa = intentscontroller.deleteintentdata(req.user.clientId, req.body.intentName);
    const query = await db.Intent.find({ clientId: req.user.clientId }).sort(
        "-createdAt"
    );
    await add(req, res, query);
    // return res.send(query);
});

/**############### End Intent page functionality ########################*/

/**############### Start user communication page ########################*/

router.post("/userChat", isLoggedIn, async(req, res) => {
    let data = [],
        userdata = "";
    var userchat = await db.Userchat.findOne({ userId: req.body.userId }).allowDiskUse(true);
    if (userchat) {
        let i = 0;
        for (chat of userchat.chats) {
            if (i == 5000) { break; }
            let chatDate = new Date(chat.createdAt);
            if (chat.messageType == "incoming") {
                let url = (chat.url && chat.url != '' && chat.url != null) ? chat.url : false;
                var msg = {
                    botMsg: chat.text,
                    url: url,
                    // botDate: dateFormat(chat.createdAt, "dd/mm/yyyy"),
                    // botTime: dateFormat(chat.createdAt, "H:MM"),
                    botDate: dateTimeConverter(chatDate, req.user.timezone, "DD-MM-YYYY"),
                    botTime: dateTimeConverter(chatDate, req.user.timezone, "HH:mm"),
                };
                if (chat.attachment == true) {
                    msg['attachmentLink'] = chat.attachmentLink;
                }
                data.push(msg);
            } else {
                var msg = {
                    userMsg: chat.text,
                    // userDate: dateFormat(chat.createdAt, "dd/mm/yyyy"),
                    // userTime: dateFormat(chat.createdAt, "H:MM"),
                    userDate: dateTimeConverter(chatDate, req.user.timezone, "DD-MM-YYYY"),
                    userTime: dateTimeConverter(chatDate, req.user.timezone, "HH:mm"),
                };
                if (chat.attachment == true) {
                    msg['attachmentLink'] = chat.attachmentLink;
                }
                data.push(msg);
            }
            i++;
        }
        userdata = { data: data, index: 1 };
    }
    return res.send(userdata);
});

router.post("/deleteChat", isLoggedIn, async(req, res) => {
    let deleteChat = await db.Userchat.deleteOne({ userId: req.body.userId });
    if (deleteChat.deletedCount > 0) {
        let userDeletedChat = new db.UserDeletedChat({
          clientId: req.user.clientId, 
          NoOfDeletedChats: deleteChat.deletedCount,
          deleteChatDate: new Date(),
        });
  
        await userDeletedChat.save();
      }
    res.redirect("/chat");
});

router.post("/deleteM", isLoggedIn, async (req, res) => {
    try {
      let deletedChats = await db.Userchat.deleteMany({
        userId: { $in: req.body.userId },
      });
  
      if (deletedChats.deletedCount > 0) {
        let userDeletedChat = new db.UserDeletedChat({
          clientId: req.user.clientId, 
          NoOfDeletedChats: deletedChats.deletedCount,
          deleteChatDate: new Date(),
        });
  
        await userDeletedChat.save();
      }
  
      res.redirect("/chat");
    } catch (error) {
      // Handle the error
      console.error(error);
      res.redirect("/chat"); 
    }
  });
  
// db.users.remove({'_id':{'$in':inactive_users}}) 

router.put("/updateUserInfo", isLoggedIn, async(req, res) => {
    try {
        let updateUser = await db.Userchat.findOneAndUpdate({ userId: req.body.userId }, {
            $set: {
                "name": req.body.name,
                // "session.name": req.body.name,
                "session.phone": req.body.phone,
                "session.email": req.body.email,
                userStatus: req.body.status,
                leadStatus: req.body.status == "4" ? 1 : 0,
            },
        }, { returnOriginal: false });
        const objData = {
            session: updateUser.session,
            userStatus: updateUser.userStatus,
            leadStatus: updateUser.leadStatus
        }
        return res.send({
            msg: 'Update user data successfully',
            data: objData,
            statusCode: 200
        })
    } catch (error) {
        return res.send({
            msg: 'user data not updated',
            error: error,
            statusCode: 500
        })
    }

});

router.post("/findUserInfobyId", isLoggedIn, async(req, res) => {
    try {
        const userData = await db.Userchat.findOne({ userId: req.body.userId }, { name : 1, session: 1, userStatus: 1, leadStatus: 1, userId: 1 });
        return res.send({
            msg: 'find user data successfully',
            data: userData,
            statusCode: 200
        })

    } catch (error) {
        return res.send({
            msg: 'user data not find',
            error: error,
            statusCode: 500
        })
    }
})

/**############### End user communication page ########################*/

/**###############Start Upload Bert Intent########################*/
router.post("/uploadYourStory", isLoggedIn, async(req, res) => {
try{
    let msg,
        wordLength = 0,
        maxlimit = 60000,
        minLength = 50;
    //to check the total limit of the story
    var storyArray = req.body.pythonBertIntent.stories;
    storyArray.forEach((element) => {
        wordLength += element.length;
    });
    if (wordLength < minLength) {
        return res.json({ message: "less" });
    }
    if (wordLength > maxlimit) {
        msg = "limit";
    } else {
        let clientId = req.user.clientId;
        var dir = process.env.pythonDir + clientId;

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true });
        }
        req.body.nodeBertIntent.clientId = clientId;

        let pythonBertIntent = JSON.stringify(req.body.pythonBertIntent, null, 2);
        let nodeBertIntent = JSON.stringify(req.body.nodeBertIntent, null, 2);

        fs.writeFile("./upload/" + clientId + ".json", nodeBertIntent, (err) => {
            // if (err) //console.log(err);
        });

        fs.writeFile(
            process.env.pythonDir + clientId + "/stories.json",
            pythonBertIntent,
            (err) => {
                // if (err) //console.log("############################err", err);
            }
        );

        let requestData = {
            userId: "c1fcfbca-af31-5885-5779-dc9f0c91dd27",
            client_id: clientId,
            train: "False",
            unsubsribe: "False",
            text: "",
            node: "False",
            session: {
                email: "",
                name: "",
                phone: "",
            },
            category: req.user.type ? req.user.type : "basic",
            update_tree: "False",
            update_story: "True",
        };

        await axios({
                method: "POST",
                url: process.env.pythonUrl,
                data: { data: requestData },
            })
            .then(function(response) {
                msg = "success";
            })
            .catch(function(error) {
                msg = "error";
            });
    }

    return res.json({ message: msg });
    // res.send(nodeBertIntent);
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});
/**###############End Upload Bert Intent########################*/

/*#########start branching tree #########*/

router.post("/uploadYourTree", isLoggedIn, async(req, res) => {
    let saveTree = await db.Client.updateOne({ clientId: req.body.clientId }, { $set: { tree: JSON.parse(req.body.jsonTree) } });
    return res.redirect("/chat");
});

router.get("/makeJson", isLoggedIn, async(req, res) => {
    // let client=await db.Client.findOne({clientId:"btrack_chatbot"});
    // jsonData  = [];
    // jsonData.push(client.tree);
    // let newJson   = [];
    // let finalJson = [];
    // let options   = [];
    // let i         = 0;
    // /** Converting Decision Tree Default JSON formate Into Uygasa's JSON formate */
    // jsonData.forEach(function (json, index) {
    //   newJson['DTree'][index]['node_id']        = i == 0 ? 'root' : 'n'+(i+1);
    //   newJson['DTree'][index]['node_name']      = i == 0 ? 'root' : json.properties.title;
    //   newJson['DTree'][index]['text']           = json.properties.question;
    //   let outputs=json.properties.outputs;
    //   outputs.forEach(function (output, outputKey) {
    //     options[outputKey]['option'] = output.answer;
    //   });
    //   newJson['DTree'][index]['options']=options;
    //   options  = [];
    //   i++;
    // });
    // //** Adding Links Information in JSON  */
    // for(link of jsonData.links){
    //   newJson['DTree'][link.fromOperator]['options'][link.fromConnector]['link'] = str_replace('operator', 'n', link.toOperator);
    // }
    // //   /** Generating the final formate of JSON */
    //  i = 0;
    // for(operator of newJson['DTree']){
    //   finalJson['DTree'][i]['node_id']   = operator['node_id'];
    //   finalJson['DTree'][i]['node_name'] = operator['node_name'];
    //   finalJson['DTree'][i]['text']      = operator['text'];
    //   for(option of operator.options){
    //     options = option;
    //     finalJson['DTree'][i]['options']  = options;
    //     options = [];
    //     i++;
    //   }
    // }
    // return newJson['DTree'];
});

// For saving conversational flow in db
router.post("/savetreejson", isLoggedIn, async(req, res) => {
    let treeData = await db.Tree.findOne({ client: req.user._id });
    if (!treeData) {
        let saveData = new db.Tree({
            client: req.user._id,
            tree: req.body.tree,
            jsonOfTree: req.body.btree,
        });
        await saveData.save();
        msg = "success";
    } else {
        let savetreejson = await db.Tree.updateOne({ client: req.user._id }, { $set: { jsonOfTree: req.body.btree, tree: req.body.tree } });
        msg = "success";
    }

    return res.send({ message: msg });
});

// For conversational flow training
router.post("/traintree", isLoggedIn, async(req, res) => {
try{
    let client = await clientcontroller.clientQuery(req, res);
    if (!client) {
        return res.json({ message: "Session expire! Please login again" });
    }
    // if (client.type == "basic" || !client.type) {
    //   return res.send({ message: "basic" });
    // } else {
    let clientId = req.user.clientId,
        msg,
        dir = process.env.pythonDir + clientId;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }

    var jsonOfTree = await db.Tree.findOne({ client: req.user._id });
    let data = JSON.stringify(JSON.parse(jsonOfTree.jsonOfTree, null, 2));

    fs.writeFile(process.env.pythonDir + clientId + "/tree.json", data, (err) => {
        // if (err) //console.log("error", err);
    });

    let requestData = {
        userId: "c1fcfbca-af31-5885-5779-dc9f0c91dd27",
        client_id: clientId,
        train: "False",
        unsubsribe: "False",
        text: "",
        node: "False",
        session: {
            name: "",
            phone: "",
            email: "",
        },
        category: req.user.type ? req.user.type : "basic",
        update_tree: "True",
        update_story: "False",
    };

    await axios({
            method: "POST",
            url: process.env.pythonUrl,
            data: { data: requestData },
        })
        .then(function(response) {
            msg = "success";
        })
        .catch(function(error) {
            msg = "error";
        });
    return res.send({ message: msg });
    // }
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});

router.post("/loadtreejson", isLoggedIn, async(req, res) => {
    let tree = await db.Tree.findOne({ client: req.user._id });

    let crm_data,
        type = req.user.type;
    let webhook = await db.Webhook.findOne({ clientId: req.user.clientId });
    if (webhook) {
        let integration = await db.Integrations.findOne({
            clientId: req.user.clientId,
            name: webhook.webhookName,
        });
        if (integration) {
            crm_data = integration;
        }
    }
    return res.send({ tree: tree, type: type, crm_data: crm_data });
});

router.post("/addTrainingPhrase", isLoggedIn, async(req, res) => {
try{
    var msg = "";
    /** if condition for multiple intent add  */
    if (req.body.tag == "") {
        req.body.text.forEach(async function(item, index) {
            let intents = await db.Intent.findOne({
                clientId: req.user.clientId,
                tag: { $regex: item.tag, $options: "$i" },
            });
            var patterns = intents.patterns;
            if (intents) {
                var patternExist = false;
                for (var i = 0; i < patterns.length; i++) {
                    if (patterns[i] == item.text) {
                        patternExist = true;
                    }
                }
                if (patternExist == false) {
                    await db.Intent.updateOne({
                        clientId: req.user.clientId,
                        tag: { $regex: item.tag, $options: "$i" },
                    }, {
                        $push: { patterns: item.text },
                    });
                    /** for ignore flag in user chat table */
                    var dbD = await db.Userchat.findOne({ userId: item.id });

                    var indx;
                    for (var i = 0; i < dbD.chats.length; i++) {
                        if (
                            dbD.chats[i].text == item.text &&
                            dbD.chats[i].ignoreMsg != true
                        ) {
                            indx = i;
                            break;
                        }
                    }
                    if (indx !== "") {
                        var placeholder = {};
                        placeholder["chats." + indx + ".ignoreMsg"] = true;
                        await db.Userchat.updateOne({ userId: item.id }, {
                            $set: placeholder,
                        });
                    }

                    msg = "Training phrase added successfully";
                } else {
                    msg = "This training phrase is already exist";
                }
                return res.send(msg);
            }
        });

        /** for single intent */
    } else {
        let intents = await db.Intent.findOne({
            clientId: req.user.clientId,
            tag: { $regex: req.body.tag, $options: "$i" },
        });
        var patterns = intents.patterns;
        if (intents) {
            var patternExist = false;
            for (var i = 0; i < patterns.length; i++) {
                if (patterns[i] == req.body.text) {
                    patternExist = true;
                }
            }
            if (patternExist == false) {
                await db.Intent.updateOne({
                    clientId: req.user.clientId,
                    tag: { $regex: req.body.tag, $options: "$i" },
                }, {
                    $push: { patterns: req.body.text },
                });
                /** for ignore flag in user chat table */
                var dbD = await db.Userchat.findOne({ userId: req.body.userId });
                var indx;
                for (var i = 0; i < dbD.chats.length; i++) {
                    if (
                        dbD.chats[i].text == req.body.text &&
                        dbD.chats[i].ignoreMsg != true
                    ) {
                        indx = i;
                        break;
                    }
                }

                if (indx !== "") {
                    var placeholder = {};
                    placeholder["chats." + indx + ".ignoreMsg"] = true;
                    await db.Userchat.updateOne({ userId: req.body.userId }, {
                        $set: placeholder,
                    });
                }

                msg = "Training phrase added successfully";
            } else {
                msg = "This training phrase is already exist";
            }
        }
        return res.send(msg);
    }
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});

router.post("/ignoreMsg", isLoggedIn, async function(req, res) {
    if (req.body.text == "") {
        req.body.userId.forEach(async function(item, index) {
            var dbD = await db.Userchat.findOne({ userId: item.id });
            var indx;
            for (var i = 0; i < dbD.chats.length; i++) {
                if (dbD.chats[i].text == item.text && dbD.chats[i].ignoreMsg != true) {
                    indx = i;
                    break;
                }
            }
            if (indx !== "") {
                var placeholder = {};
                placeholder["chats." + indx + ".ignoreMsg"] = true;
                await db.Userchat.updateOne({ userId: item.id }, {
                    $set: placeholder,
                });
            }
        });
    } else {
        var dbD = await db.Userchat.findOne({ userId: req.body.userId });
        var indx;
        for (var i = 0; i < dbD.chats.length; i++) {
            if (
                dbD.chats[i].text == req.body.text &&
                dbD.chats[i].ignoreMsg != true
            ) {
                indx = i;
                break;
            }
        }

        if (indx !== "") {
            var placeholder = {};
            placeholder["chats." + indx + ".ignoreMsg"] = true;
            await db.Userchat.updateOne({ userId: req.body.userId }, {
                $set: placeholder,
            });
        }
    }

    return res.send("success");
});

router.post(
    "/uploadIntents",
    isLoggedIn,
    upload.single("intents"),
    async function(req, res) {
    try{
        var fileName = req.file.filename;
        let clientId = req.user.clientId;
        var query;
        let client = await db.Client.findOne({ clientId: clientId });

        if (!client) {
            return res.json({ message: "Session expire! Please login again" });
        }

        const tagName_exist = await db.Intent.find({});

        await fs.readFile(
            "./public/dist/img/" + fileName,
            async function read(err, data) {
                if (err) {
                    res.render("intent.hbs");
                } else {
                    var intent = JSON.parse(data);
                    intent = intent.intents;

                    if (intent) {

                        for (int of intent) {
                            if ((int.tag && int.patterns && int.entities) || (int.tag && int.tag.toLowerCase() == "fallback")) {
                                const tagName_exist = await db.Intent.find({
                                    clientId: clientId,
                                    tag: { $regex: int.tag, $options: "$i" },
                                });

                                if (tagName_exist.length > 0) {
                                    await db.Intent.updateOne({
                                        clientId: req.user.clientId,
                                        tag: { $regex: int.tag, $options: "$i" },
                                    }, {
                                        $addToSet: {
                                            patterns: { $each: int.patterns },
                                            responses: { $each: int.responses },
                                            entities: { $each: int.entities },
                                        },
                                    });
                                } else {
                                    let data = new db.Intent({
                                        clientId: req.user.clientId,
                                        tag: int.tag,
                                        patterns: int.patterns,
                                        responses: int.responses,
                                        entities: int.entities,
                                    });
                                    await data.save();
                                }
                                query = await db.Intent.find({
                                    clientId: req.user.clientId,
                                }).sort("-createdAt");
                            } else {
                                return res.send(
                                    "The given json format is not valid to crate the intents"
                                );
                            }
                        }
                        await fs.unlink(
                            "./public/dist/img/" + fileName,
                            async function(err, result) {
                                //  return res.send(query);
                                // console.log('result', result)
                                //  console.log('err', err)

                            }
                        );

                        comman_intents = intent.filter(o => tagName_exist.some(({ tag }) => o.tag === tag));
                        return res.send(comman_intents);

                    } else {
                        return res.send("The given file format is not valid");
                    }

                }
            }
        );
    }catch(error){
        console.log('An error occurs in the catch block:-', error)
    }
    }
);


router.post("/setActiveStatus", isLoggedIn, async function(req, res) {
    await db.Intent.updateOne({ _id: req.body.id, clientId: req.user.clientId }, {
        $set: {
            active_status: req.body.status,
        },
    });
    res.send({ message: "success" });
});

router.post("/addWebhook", isLoggedIn, async(req, res, next) => {
try{
    let client = await clientcontroller.clientQuery(req, res);
    if (!client) {
        return res.json({ message: "Session expire! Please login again" });
    }

    const webhookName_exist = await db.Webhook.find({
        clientId: client.clientId,
        webhookName: { $regex: req.body.webhookName, $options: "$i" },
    });
    if (webhookName_exist.length > 0) {
        return res.send("exists");
    } else {
        let data = new db.Webhook({
            clientId: req.user.clientId,
            webhookName: req.body.webhookName,
            webhookUrl: req.body.webhookUrl,
            webhookParameters: req.body.webhookParameters,
            webhookApiKey: req.body.webhookApiKey,
            webhookRequestMethod: req.body.webhookRqwstMthd,
            webhookApiSecretKey: req.body.webhookApiScrtKey,
        });
        await data.save();
    }

    const query = await db.Webhook.find({ clientId: req.user.clientId }).sort(
        "-createdAt"
    );
    return res.send(query);
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});

router.post("/webhook", isLoggedIn, async(req, res) => {
    let query = await db.Webhook.find({ clientId: req.user.clientId }).sort(
        "-createdAt"
    );
    return res.send(query);
});

router.post("/webhookbyid", isLoggedIn, async(req, res) => {
    const query = await db.Webhook.find({ _id: req.body.webhookId });
    return res.send(query);
});

router.post("/updateWebhook", isLoggedIn, async(req, res) => {
try{
    let clientId = req.user.clientId;
    let client = await db.Client.findOne({ clientId: clientId });
    if (!client) {
        return res.json({ message: "Session expire! Please login again" });
    }
    const webhook_exist = await db.Webhook.find({ _id: req.body.webhookId });
    if (webhook_exist.length > 0) {
        // const webhookName_exist = await db.Webhook.find({ webhookName: { $regex: req.body.webhookName, $options: "$i" } });

        // if (webhookName_exist.length > 0 && (webhookName_exist[0].webhookName).toLowerCase() == (req.body.webhookName).toLowerCase()) {

        // } else {
        await db.Webhook.updateOne({ _id: req.body.webhookId }, {
            $set: {
                clientId: req.user.clientId,
                webhookName: req.body.webhookName,
                webhookUrl: req.body.webhookUrl,
                webhookParameters: req.body.webhookParameters,
                webhookApiKey: req.body.webhookApiKey,
                webhookRequestMethod: req.body.webhookRqwstMthd,
                webhookApiSecretKey: req.body.webhookApiScrtKey,
            },
        });
        const query = await db.Webhook.find({ clientId: req.user.clientId }).sort(
            "-createdAt"
        );
        return res.send(query);
        // }
    } else {
        return res.send("Something went wrong");
    }
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});

router.post("/deleteWebhook", isLoggedIn, async(req, res, next) => {
    var deleteWebhook = await db.Webhook.deleteOne({ _id: req.body.webhookId });
    const query = await db.Webhook.find({ clientId: req.user.clientId }).sort(
        "-createdAt"
    );
    await add(req, res, query);
    // return res.send(query);
});

router.post('/excelsheet', async(req, res) => {
try{
    let workbook = new excel.Workbook();
    let worksheet = workbook.addWorksheet("Leads");

    worksheet.columns = [
        { header: "S.No.", key: "id", width: 5 },
        { header: "Name", key: "Name", width: 32 },
        { header: "Email", key: "Email", width: 32 },
        { header: "Phone No", key: "PhoneNo", width: 32 },
        { header: "Location", key: "Location", width: 32 },
        { header: "Chat Time", key: "ChatTime", width: 32 },
        { header: "Chats", key: "Chats", width: 100 }
    ];
    let obj1 = req.body;
    var users = await db.Userchat.find({
        $and: [
            { clientId: req.user._id },
            {
                date: { $gte: obj1[0].startDate, $lte: obj1[0].endDate },
            },
        ],
    }).allowDiskUse(true);
    var data = [];
    var index = 1,
        username = "",
        location = "";
    for (user of users) {
        if (user.session) {
            username = user.session.name;
        } else {
            username = '';
        }
        if (user.location && user.location.city) {
            location = user.location.city;
        } else {
            location = '';
        }
        var fileupload = JSON.parse(JSON.stringify(user._doc.fileUploaded));
        var now = new Date();
        var chatDate = new Date(user.date);
        var currentdate =
            ("0" + now.getDate()).slice(-2) +
            "-" +
            ("0" + (now.getMonth() + 1)).slice(-2) +
            "-" +
            now.getFullYear();
        var dbdate =
            ("0" + chatDate.getDate()).slice(-2) +
            "-" +
            ("0" + (chatDate.getMonth() + 1)).slice(-2) +
            "-" +
            chatDate.getFullYear() +
            " " +
            ("0" + chatDate.getHours()).slice(-2) +
            ":" +
            ("0" + chatDate.getMinutes()).slice(-2);
        const date = new Date(now);
        date.setDate(date.getDate() - 1);
        const prevdate =
            ("0" + date.getDate()).slice(-2) +
            "-" +
            ("0" + (date.getMonth() + 1)).slice(-2) +
            "-" +
            date.getFullYear();

        let clientId = req.user.clientId;
        let dbtimezone = req.user.timezone;

        const timechat = dateTimeConverter(chatDate, dbtimezone, "HH:mm");
        const Chatdate = dateTimeConverter(chatDate, dbtimezone, "DD-MM-YYYY");

        /** for time extraction */

        if (Chatdate == currentdate) {
            chatTime = "Today at " + timechat;
        } else if (Chatdate == prevdate) {
            chatTime = "Yesterday at " + timechat;
        } else {
            chatTime = Chatdate + " at " + timechat;
        }

        let userphone = user.phone == null ? "" : user.phone;
        let useremail = user.email == null ? "" : user.email;
        if (user.session) {
            if (Array.isArray(user.session.phone)) {
                userphone =
                    userphone == "" ?
                    user.session.phone.join(", ") :
                    user.session.phone.join().includes(userphone) ?
                    user.session.phone.join(", ") :
                    userphone + ", " + user.session.phone.join(", ");
            } else if (user.session.phone) {
                userphone = user.session.phone;
            }

            if (Array.isArray(user.session.email)) {
                useremail =
                    useremail == "" ?
                    user.session.email.join(", ") :
                    user.session.email.join().includes(useremail) ?
                    user.session.email.join(", ") :
                    useremail + ", " + user.session.email.join(", ");
            } else if (user.session.email) {
                useremail = user.session.email;
            }
        }

        var chatTexts = user.chats.map(function(el) {
            if (el.messageType == "outgoing") {
                return "Bot Says: " + el.text
            } else {
                return "User Says: " + el.text
            }
        });
        chatTexts = chatTexts.join("\n\n")

        var obj;
        if (req.query.filter == "Leads") {
            if (useremail || userphone) {
                filterValue = "Leads";
                obj = {
                    index: index,
                    userId: user.userId,
                    email: useremail,
                    name: user.name,
                    status: user.userStatus,
                    chatTime: chatTime,
                    phone: userphone,
                    location: location,
                    name: username,
                    src: getsrc(user.userId),
                    chats: chatTexts.toString()
                };
                if (fileupload.length > 0) {
                    let imgType = "1";
                    Object.assign(obj, { imgType: imgType });
                }
                index++;
                data.push(obj);
            }
        } else {
            filterValue = "All";
            obj = {
                index: index,
                userId: user.userId,
                email: useremail,
                name: user.name,
                status: user.userStatus,
                chattime: chatTime,
                phone: userphone,
                location: location,
                name: username,
                src: getsrc(user.userId),
                chats: chatTexts.toString()
            };
            if (fileupload.length > 0) {
                let imgType = "1";
                Object.assign(obj, { imgType: imgType });
            }
            index++;
            data.push(obj);
        }
    }

    let arr3 = data.map(v => ({...v, ...obj1.find(sp => sp.userId === v.userId && sp.chattime === v.chattime) }));
    arr3.forEach((e) => {
        let tutorials = [{ id: e.index, Name: e.name, Email: e.email, PhoneNo: e.phone, Location: e.location, ChatTime: e.chattime, Chats: e.chats }]
        worksheet.addRows(tutorials);
    })

    res.setHeader("fileName", "Leads.xlsx");
    res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    res.setHeader("Content-Disposition", "attachment; filename=" + "Leads.xlsx");

    return workbook.xlsx.write(res).then(function() {
        res.status(200).end();
    });
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});

/** */
/** for excel sheet generation */

/** */
// function to format number as k,m,b,t
function abbrNum(number, decPlaces) {
    decPlaces = Math.pow(10, decPlaces);
    var abbrev = [" k", " m", " b", " t"];

    for (var i = abbrev.length - 1; i >= 0; i--) {
        var size = Math.pow(10, (i + 1) * 3);
        if (size <= number) {
            var number = Math.round((number * decPlaces) / size) / decPlaces;
            if (number == 1000 && i < abbrev.length - 1) {
                number = 1;
                i++;
            }
            number += abbrev[i];
            break;
        }
    }
    return number;
}

/**for fallback excel sheet */
async function getFallbackExcel(headers, rows) {
    const workbook = new excel.stream.xlsx.WorkbookWriter({});
    const sheet = workbook.addWorksheet("My Worksheet");
    sheet.columns = headers;
    rows.forEach(async function(item, index) {
        sheet.addRow({ "S.no": item.id, Fallback: item.text });
    });

    sheet.commit();
    return new Promise((resolve, reject) => {
        workbook
            .commit()
            .then(() => {
                const stream = workbook.stream;
                const result = stream.read();
                resolve(result);
            })
            .catch((e) => {
                reject(e);
            });
    });
}

router.post("/fallbackexcel", async(req, res) => {
try{
    var obj = req.body;
    const stream = await getFallbackExcel(
        [{
                header: "S.no",
                key: "S.no",
                width: 10,
                style: { font: { name: "Arial Black" } },
            },
            {
                header: "Fallback",
                key: "Fallback",
                width: 32,
                style: { font: { name: "Arial Black" } },
            },
        ],
        obj
    );
    res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader("fileName", "fallback.xlsx");
    res.setHeader("Content-Disposition", `attachment; filename=test.xlsx`);
    res.setHeader("Content-Length", stream.length);
    res.send(stream);
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});
/** */

async function chatLeadTime(chatDate) {
    var now = new Date();
    var chatDate = new Date(chatDate);
    var time = (now.getTime() - chatDate.getTime()) / 60000;
    var chatTime = time.toFixed(0) + " min ago";

    if (time >= 60) {
        time = time / 60;
        chatTime = time.toFixed(0) + " hr ago";
        if (time >= 24) {
            time = time / 24;
            chatTime = time.toFixed(0) + " days ago";
        }
    }
    return chatTime;
}

async function getIntentExcel(headers, rows) {
    const workbook = new excel.stream.xlsx.WorkbookWriter({});
    const sheet = workbook.addWorksheet("Intents");
    sheet.columns = headers;
    rows.forEach(async function(item, index) {
        sheet.addRow({
            SNo: item.SNo,
            Title: item.Title,
            Pattern: item.Pattern,
            Response: item.Response,
            Entities: item.Entities
        });
    });

    sheet.commit();
    return new Promise((resolve, reject) => {
        workbook
            .commit()
            .then(() => {
                const stream = workbook.stream;
                const result = stream.read();
                resolve(result);
            })
            .catch((e) => {
                reject(e);
            });
    });
}

router.post("/intentexcel", async(req, res) => {
try{
    const querystatus = await db.Intent.find({ clientId: req.user.clientId }, { tag: 1, patterns: 1, responses: 1, entities: 1, _id: 0 });

    if (querystatus.length < 1) {
        res.setHeader("intents", false);
        return res.send();
    }

    let intentexceldata = [];
    querystatus.forEach((item, index) => {
        index = index + 1;
        var sn = index.toString();
        if (item.entities.length > 0) {
            var data = item.entities.toString();
        }
        let obj = {
            SNo: sn,
            Title: item.tag,
            Pattern: item.patterns[0],
            Response: item.responses[0],
            Entities: data ? data : "",
        };
        intentexceldata.push(obj);

        for (var i = 1; i < item.patterns.length; i++) {
            let obj = {
                SNo: "",
                Title: "",
                Pattern: item.patterns[i],
                Response: "",
                Entities: "",
            };
            intentexceldata.push(obj);
        }
    });

    const workbook = new excel.Workbook();
    const worksheet = workbook.addWorksheet("Intents");

    worksheet.columns = [
        { header: "SNo", key: "SNo", width: 10 },
        { header: "Title", key: "Title", width: 32 },
        { header: "Pattern", key: "Pattern", width: 32 },
        { header: "Response", key: "Response", width: 45 },
        { header: "Entities", key: "Entities", width: 32 },
    ];

    intentexceldata.forEach((e) => {
        worksheet.addRow(e);
    });

    const stream = await workbook.xlsx.writeBuffer();

    res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader("fileName", "Intents.xlsx");
    res.setHeader("Content-Disposition", `attachment; filename=test.xlsx`);
    res.setHeader("Content-Length", stream.length);
    res.send(stream);
}catch(error){
    console.log('An error occurs in the catch block:-', error)
}
});

router.post("/integration", isLoggedIn, async function(req, res, next) {
    let clientId = req.user.clientId;
    if (clientId) {
        const getQuery = await db.Integrations.findOne({
            clientId: clientId,
            name: req.body.name,
        });
        if (getQuery) {
            if (getQuery.apiKey) {
                return res.send("Configured");
            } else {
                return res.send("Required");
            }
        } else if (req.body.flag == "false") {
            return res.send("Required");
        } else {
            if (req.body.apiKey) {
                let newConfiguration = new db.Integrations({
                    clientId: clientId,
                    name: req.body.name,
                    apiKey: req.body.apiKey,
                });
                const query = await newConfiguration.save();
                if (query) {
                    return res.send("Saved");
                } else {
                    return res.send("not_saved");
                }
            } else {
                return res.send("Required");
            }
        }
    } else {
        return res.redirect("/login");
    }
});

router.get("/integration", isLoggedIn, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        if (req.user.clientId) {
            const getQuery = await db.Integrations.findOne({
                clientId: req.user.clientId,
            });
            let sideNavValidation = {
                isActionEnabled: req.user.isActionEnabled ? req.user.isActionEnabled : false,
                isHumanAgentEnabled: req.user.isHumanAgentEnabled ? req.user.isHumanAgentEnabled : false,
            };
            if (getQuery) {
                if (getQuery.name == "Hubspot") {
                    const hubspot = new Hubspot({
                        apiKey: getQuery.apiKey,
                    });
                    hubspot.contacts.properties
                        .get()
                        .then((results) => {
                            var dbPropArray = getQuery.properties;
                            if (dbPropArray && dbPropArray.length > 0) {
                                for (var i = 0; i < dbPropArray.length; i++) {
                                    for (var j = 0; j < results.length; j++) {
                                        if (
                                            dbPropArray[i].name == results[j].name &&
                                            dbPropArray[i].label == results[j].label
                                        ) {
                                            results[j].checkbox = true;
                                        }
                                    }
                                }
                            }
                            return res.render("hubspot.hbs", { prop: results, sideNavValidation: sideNavValidation });
                        })
                        .catch((err) => {
                            return res.render("hubspot.hbs", { prop: err, sideNavValidation: sideNavValidation });
                        });
                } else {
                    return res.render("hubspot.hbs", { msg: "No integrations", sideNavValidation: sideNavValidation });
                }
            } else {
                return res.render("hubspot.hbs", { msg: "No integrations", sideNavValidation: sideNavValidation });
            }
        } else {
            return res.redirect("/login");
        }
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});

router.post("/properties", isLoggedIn, async function(req, res) {
    if (req.user.clientId) {
        const getQuery = await db.Integrations.findOne({
            clientId: req.user.clientId,
        });
        if (getQuery) {
            await db.Integrations.updateOne({
                clientId: req.user.clientId,
            }, {
                $set: {
                    properties: req.body.properties,
                },
            });
            return res.send({ msg: "Success" });
        } else {
            let sideNavValidation = {
                isActionEnabled: req.user.isActionEnabled ? req.user.isActionEnabled : false,
                isHumanAgentEnabled: req.user.isHumanAgentEnabled ? req.user.isHumanAgentEnabled : false,
            };
            return res.render("hubspot.hbs", { msg: "No integrations", sideNavValidation: sideNavValidation });
        }
    } else {
        return res.redirect("/login");
    }
});

router.post("/readAProperty", isLoggedIn, async function(req, res) {
    if (req.user.clientId) {
        const getQuery = await db.Integrations.findOne({
            clientId: req.user.clientId,
        });
        const hubspot = new Hubspot({
            apiKey: getQuery.apiKey,
        });
        hubspot.contacts.properties
            .getByName(req.body.propertyName)
            .then((results) => {
                return res.send(results);
            })
            .catch((err) => {
                return res.send(err);
            });
    } else {
        return res.redirect("/login");
    }
});

router.get("/forgot", async function(req, res, next) {
    res.render("forgot.hbs");
});

router.get("/reset", async function(req, res, next) {
    if (req.query.token) {
        res.render("reset.hbs", {
            token: req.query.token,
        });
    } else {
        res.redirect("/login");
    }
});
router.get("/resetAgentPassword", async function(req, res, next) {
    if (req.query.token) {
        res.render("resetAgentPassword.hbs", {
            token: req.query.token,
        });
    } else {
        res.redirect("/login");
    }
});
router.get("/");

//route for forgot password
router.post("/forgot",forgotPassApiLimit, resetcontroller.forgotpassword);

//route for reset password
router.post("/reset/:token?", resetcontroller.resettoken);

//route to save preview data
router.post(
    "/botsavepreview",
    upload.single("profile_img"),
    isLoggedIn,
    yubosettingcontroller.botsavepreview
);

//load bot css from database
router.post("/templateload", isLoggedIn, yubosettingcontroller.templateload);

router.get("/help", isUserPro, async function(req, res) {
    if(req.userTypePro){
        res.render('help.hbs', { title: "Yugasa Bot Admin" })
    }else{
        res.render('helpForBasicUser.hbs', { title: "Yugasa Bot Admin" })
    }
})

// router.get("/help", isLoggedIn, async function(req, res, next) {
//     checkForToken(req.user._id).then(async function(response) {
//         let data = { sideNavValidation: await sideNavValidation(req) }
//         return res.render("help.hbs", data);
//     }).catch(err => {
//         req.session.destroy(function(err) {
//             req.logout();
//             res.render("login.hbs", { title: "Yugasa Bot Admin" });
//         });
//     })
// });

router.get("/helps", isLoggedIn, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        let data = { sideNavValidation: await sideNavValidation(req) }
        return res.render("helps.hbs", data);
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});

router.get("/help2", isLoggedIn, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        return res.render("help2.hbs");
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});
//Send mail for help
router.post("/helpMail", isLoggedIn, emailverifycontroller.helpMail);

//To check user type pro or basic
router.post("/checkUserType", isLoggedIn, clientcontroller.checkUserType);

//Route for edit profile under edit profile
router.get("/editProfile", isLoggedIn, isUserPro, showActivateButton, editProfilecontroller.editProfileGet);

// add money to wallet
router.post("/add-money", isLoggedIn, editProfilecontroller.creditMoneyInWallet);

// payment status check
router.get("/payment-status", isLoggedIn, editProfilecontroller.paymentStatus);

// wallet balance check
router.post('/wallet-balance-checking', isLoggedIn, editProfilecontroller.walletBalanceCheck);

// debit money from wallet
router.post('/debit-amount', isLoggedIn, editProfilecontroller.debitMoney);

router.get('/payment-successfull', isLoggedIn, editProfilecontroller.paymentSuccess);

router.post(
    "/editProfile",
    isLoggedIn,
    upload.single("profile_img"),
    editProfilecontroller.editProfilePost
);
//Route for change password under edit profile
router.post(
    "/changePassword",
    isLoggedIn,
    editProfilecontroller.changePasswordPost
);
//for community page
router.get("/community-login", async function(req, res, next) {
    return res.render("login.hbs", { type: 'community' });
});
router.post('/communitylogin', communitycontroller.communitylogin)
router.get('/communitydata', communitycontroller.communitydata)

function emailmasking(userEmail) {
    userEmail = userEmail.split('');
    let finalArr = [];
    let len = userEmail.indexOf('@');
    userEmail.forEach((item, pos) => {
        (pos >= 2 && pos <= len - 2) ? finalArr.push('*'): finalArr.push(userEmail[pos]);
    })
    return finalArr.join('')
}

//for otp verfication 
router.post('/getotp', getLoginOtpLimiter, async function(req, res, next) {
    try {
        let checkdata = await db.Otp.findOne({ phone: req.body.phone, email: req.body.email, clientId: req.body.clientId, otpFor: 'mobileVerification' })
        var otp = (Math.floor(Math.random() * 1000) + 1000);
        if (checkdata) {
            var save = await db.Otp.updateOne({ phone: req.body.phone, email: req.body.email, otpFor: 'mobileVerification' }, { $set: { otp: otp, clientId: req.body.clientId } })
        } else {
            var save = await db.Otp.create({ phone: req.body.phone, email: req.body.email, otp: otp, clientId: req.body.clientId, otpFor: 'mobileVerification' });
        }
        //SMS gateWay
        let url = 'http://admagister.net/api/mt/SendSMS?user=YUGASA2018&password=Yugasa2018&senderid=YUGASA&channel=Trans&DCS=0&flashsms=0&number=' + req.body.phone + '&text=Welcome to Yugasa Bot, your OTP is: ' + otp + '&route=40';
        axios.get(url).then(function(response) {
            //console.log(response) 
        });

        //   let data = {
        //       button: "true",
        //       buttonUrlParam: "",
        //       clientId: "yugasa_chatbot",
        //       footer: "",
        //       header: "",
        //       lang: "es",
        //       media_url: "",
        //       msg: 'Welcome to Yugasa Bot, your OTP is: ' + otp,
        //       msg_type: "TEXT",
        //       parameters: [`${otp}`],
        //       send_to:  req.body.phone,
        //       templateName: "sample_issue_resolution"
      
      
        //     };
        //     console.log(data)
      
        //   let response = await axios.post('https://campaign.helloyubo.com/sendWaCampaignToParticularUser', data);
        //   console.log("----------------------------------=")
        //   if (response.data.statusCode === 200 && response.data.status === true) {
        //       console.log(response.data)
        //       console.log("---------------34566733-------------------=")
      
        //       return res.json({ msg: 'success', limit: `${req["rateLimit"].remaining}` })
        //   } else {
        //     throw new Error(response.data.message);
        //   }
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: 'Failed to send OTP on WhatsApp' });
    }
    return res.json({ msg: 'success', limit: `${req["rateLimit"].remaining}` })
});

router.post('/verfiyotp', async function(req, res) {
        let check = await db.Otp.findOne({ phone: req.body.phone, email: req.body.email, clientId: req.body.clientId, otpFor: 'mobileVerification' })
        if (!check) {
            return res.json({ status: 'false', message: "User not found." });
        } else {
            if (req.body.otp != check.otp) {
                return res.json({ status: 'false', message: "OTP not matched." });
            } else {
                let statusupdate = await db.Otp.updateOne({ phone: req.body.phone, email: req.body.email, clientId: req.body.clientId, otpFor: 'mobileVerification' }, { $set: { status: 'true', otp_verified: true } })
                return res.json({ status: 'success', message: "Otp verfied Successfully", verified: 'true' });
            }
        }
    })
    //to enable  two factor authencation 
router.post('/getOtpFor2FactrAuth', async function(req, res, next) {
    try {
        let clientData = await db.Client.findOne({ clientId: req.user.clientId }, { phone: 1 });
        var otp = (Math.floor(Math.random() * 1000) + 1000);
        let checkdata = await db.Otp.findOne({
            phone: clientData.phone,
            clientId: req.user.clientId,
            otpFor: 'twoFactorAuth'
        })
        if (checkdata) {
            await db.Otp.updateOne({ _id: checkdata._id }, { $set: { status: false, otp_verified: false, otp: otp } });
        } else {
            var save = await db.Otp.create({ phone: clientData.phone, clientId: req.user.clientId, otp: otp, otpFor: 'twoFactorAuth' });
        }
        //SMS gateWay
        let url = 'http://admagister.net/api/mt/SendSMS?user=YUGASA2018&password=Yugasa2018&senderid=YUGASA&channel=Trans&DCS=0&flashsms=0&number=' + clientData.phone + '&text=Welcome to Yugasa Bot, your OTP is: ' + otp + '&route=40';
        axios.get(url).then(function(response) {
            // console.log(response) 
        });
        return res.json({ message: 'success', statusCode: 200 })
    } catch (err) {
        console.log('error in catch block', err)
    }
});
router.post('/verfiyOtpFor2Fact', async function(req, res) {
    try {
        let clientData = await db.Client.findOne({ clientId: req.user.clientId }, { phone: 1 });
        let checkdata = await db.Otp.findOne({
            phone: clientData.phone,
            clientId: req.user.clientId,
            otpFor: 'twoFactorAuth'
        });
        if (checkdata) {
            if (checkdata.status == true) {
                return res.json({ message: 'Two step verification is already enabled for your account !', statusCode: 201 })
            } else {
                if (req.body.otp == checkdata.otp) {
                    await db.Otp.updateOne({ _id: checkdata._id }, { $set: { status: 'true', otp_verified: true } })
                    await db.Client.updateOne({ clientId: req.user.clientId }, { $set: { _2FA: true } })
                    return res.json({ status: true, message: "Congratulations ! Two step verification is enabled successfully", statusCode: 200 });
                } else {
                    return res.json({ status: 'false', message: "OTP not matched.", statusCode: 201 });
                }
            }
        } else {
            return res.json({ status: 'false', message: "Generate otp first", statusCode: 201 });
        }
    } catch (err) {
        console.log('error in catch block', err)
    }
});
//get login time 2FA otp 
router.post('/getLoginOtp', getLoginOtpLimiter, async function(req, res, next) {
    try {
        var otp = (Math.floor(Math.random() * 1000) + 1000);
        let clientData = await db.Client.findOne({
            clientId: req.body.clientId
        })
        if (!clientData) {
            return res.json({
                message: 'Invaid Client Id/Username , please provide valid username',
                statusCode: 201
            })
        }
        let checkdata = await db.Otp.findOne({
            phone: clientData.phone,
            clientId: req.body.clientId,
            otpFor: 'loginOtp'
        })
        if (checkdata) {
            await db.Otp.updateOne({ _id: checkdata._id }, { $set: { otp: otp, status: false, otp_verified: false } });
        } else {
            await db.Otp.create({ phone: clientData.phone, email: clientData.email, clientId: req.body.clientId, otp: otp, otpFor: 'loginOtp' });
        }
        //SMS gateWay
        let url = 'http://admagister.net/api/mt/SendSMS?user=YUGASA2018&password=Yugasa2018&senderid=YUGASA&channel=Trans&DCS=0&flashsms=0&number=' + clientData.phone + '&text=Welcome to Yugasa Bot, your OTP is: ' + otp + '&route=40';
        axios.get(url).then(function(response) {
            //console.log(response) 
        });
        return res.json({
            message: 'otp send successfully to your registered number ',
            statusCode: 200,
            status: true,
            limit: `${req["rateLimit"].remaining}`,
        })
    } catch (err) {
        console.log('error in catch block', err);
    }
});
//verify login time  2FA otp
router.post('/verfiyLoginOtp', async function(req, res) {
    try {
        let clientData = await db.Client.findOne({
            clientId: req.body.clientId
        });
        let checkdata = await db.Otp.findOne({
            phone: clientData.phone,
            clientId: req.body.clientId,
            otpFor: 'loginOtp'
        });
        if (checkdata) {
            if (checkdata.status == true) {
                return res.json({ message: 'Please generate otp first', statusCode: 201 })
            } else {
                if (req.body.otp == checkdata.otp) {
                    await db.Otp.updateOne({ _id: checkdata._id }, { $set: { status: true, otp_verified: true } })
                    return res.json({ status: true, message: "otp verified successfully", statusCode: 200 });
                } else {
                    return res.json({ status: 'false', message: "OTP not matched.", statusCode: 201 });
                }
            }
        } else {
            return res.json({ status: 'false', message: "Generate otp first", statusCode: 201 });
        }
    } catch (err) {
        console.log('error in catch block', err)
    }
});
//get 2FA status of particular client by client id
router.post('/get2FaStatus', async function(req, res) {
    try {
        let clientId = req.body.clientId ? req.body.clientId : req.user.clientId;
        let checkdata = await db.Client.findOne({ clientId: clientId }, { _2FA: 1, phone: 1 });
        let result;
        if (!checkdata) {
            return res.json({
                status: false,
                message: "Username not found, Pleasae try again",
                statusCode: 201
            });
        }
        if (checkdata && checkdata.phone) {
            string = await JSON.stringify(checkdata.phone)
            result = await string.slice(6, 10);
        }
        let number = `***** *${result}`
        let data = {
            _2FA: checkdata._2FA,
            phone: number
        }

        if (checkdata) {
            return res.json({
                status: true,
                message: "client data",
                statusCode: 200,
                data: data
            });
        } else {
            return res.json({
                status: false,
                message: "Incorrect Username ",
                statusCode: 201
            });
        }
    } catch (err) {
        console.log('get2FaStatus error', err)
    }
});
router.post('/change2FAStatus', async function(req, res) {
    try {
        let checkdata = await db.Client.findOne({ clientId: req.user.clientId });
        if (!checkdata) {
            return res.json({
                status: false,
                message: "User not found ",
                statusCode: 201
            });
        }
        await db.Otp.updateOne({ clientId: req.user.clientId, phone: checkdata.phone }, { $set: { status: req.body._2FA, otp_verified: req.body._2FA } });
        await db.Client.updateOne({ clientId: req.user.clientId }, { $set: { _2FA: req.body._2FA } });
        if (req.body._2FA === true || req.body._2FA == 'true') {
            return res.json({
                status: true,
                message: "Two factor authentication is activated successfully",
                statusCode: 200,
            });
        } else {
            return res.json({
                status: false,
                message: "Two factor authentication is deactivated successfully",
                statusCode: 200,
            });
        }
    } catch (err) {
        console.log('error in catch block', err)
    }
});



router.post("/getuserName", isLoggedIn, emailTemp.getuserName);
// router.post("/getEmail", emailTemp.getEmail);

router.get("/campaigning", isLoggedIn,accessProFeatures, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
    try{
        if (!req.user.isCampaigningEnabled) {
            return res.render('campaignsubscribe.hbs', { clientId: req.user.clientId ,userTypePro: req.userTypePro })
        }
        if (req.user.gupShupConfig != undefined && (req.user.gupShupConfig.accountType == "Yugasa" || req.user.gupShupConfig.accountType == "AiSensy")) {
            res.render("campaigning_cloud.hbs", { clientId: req.user.clientId , userTypePro: req.userTypePro});
        } else {
            res.render("campaigning.hbs", { clientId: req.user.clientId, userTypePro: req.userTypePro });
        }
    }catch(error){
        console.log('Error occurs in catch block :', error)
    }
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});


router.get("/mailsettings", isLoggedIn, function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        res.render("mailsettings.hbs");
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});
router.post("/mailconfigdata", isLoggedIn, emailTemp.mailconfigdata)
router.post('/addmoremember', isLoggedIn, emailTemp.addmoremember)
router.post('/creategroup', isLoggedIn, emailTemp.creategroup);
/**for deleting on Managegroup page */
router.post('/deletegroup', isLoggedIn, emailTemp.deletegroup)
    /**forediting on Managegroup page */
router.post('/editgroup', isLoggedIn, emailTemp.editgroup);
/**for group dropdown on communication page */
router.post("/usergroupdata", isLoggedIn, emailTemp.usergroupdata)
    /**for groupdetail on Managegroup page */
router.post("/getgroupdata", isLoggedIn, emailTemp.getgroupdata)
    /** for updategroup */
router.post('/updategroup', isLoggedIn, emailTemp.updategroup);
/**for template data */
router.post('/campaigntempdata', isLoggedIn, emailTemp.campaigntempdata);
/**for emailconfig update */
router.post('/emailconfig', isLoggedIn, emailTemp.emailconfig);
/**for mail scheduling */
router.post('/schedulemail', isLoggedIn, uploadbulk.any(), emailTemp.schedulemail);
/**for test mail */
router.post('/sendtestmail', emailTemp.sendtestmail);
router.post('/saveDataOnS3', emailTemp.saveDataOnS3);
router.get("/mygroup", isLoggedIn, accessProFeatures,function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        if (!req.user.isCampaigningEnabled) {
            return res.render('campaignsubscribe.hbs', { clientId: req.user.clientId,userTypePro: req.userTypePro })
        }
        res.render("mygroup.hbs",{userTypePro : req.userTypePro});
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});
//to load intents 
router.post("/loadintentsname", isLoggedIn, intentscontroller.loadIntentsName);
//Bot API
router.post("/sendMessage", botApi.sendMessage);
router.post("/followUp", botApi.followUp);

router.get("/thank-you", async function(req, res) {
    let client = await decodedTokenFunc(req.query.valid)
    maskedemail = client ? ((client.email) ? emailmasking(client.email) : "your registered mail id") : "your registered mail id";
    res.render("thankyou.hbs", {
        verification_msg: "We have sent a verification link on " + maskedemail + ". Please check your spam folder in case you do not see that in your inbox.",
        //verification_msg: "We have received your registration request. Our team will get in touch with you to complete the on-boarding process."
    });
})

router.post("/need-assistance", async function(req, res) {
    let clientDetails = await decodedTokenFunc(req.body.token);
    await emailverifycontroller.need_assistanceMail(clientDetails)
    res.send("sent")
})

async function decodedTokenFunc(token) {
    let decodedToken = jwt.verify(token, "yubo2020"),
        client = await db.Client.findOne({ clientId: decodedToken ? decodedToken.clientId : "" });
    return client;
}

function checkdirexist() {
    let dir = "public/dist/campaign";
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
        return dir
    } else {
        return dir;
    }
}

function dateTimeConverter(chatDateTime, timezone, format) {
    if (timezone == "") {
        timezone = "Asia/Kolkata";
    }
    return moment
        .utc(chatDateTime, "YYYYMMDD HH:mm:ss")
        .clone()
        .tz(timezone)
        .format(format);
}

router.get("/pythonTest", async function(req, res) {
    let requestData1 = {
            "userId": "c1fcfbca-af31-5885-5779-dc9f0c91dd27",
            "client_id": "test_staging",
            "train": "False",
            "unsubsribe": "False",
            "text": "hi",
            "node": "False",
            "session": {
                "name": "",
                "phone": "",
                "email": ""
            },
            "category": "pro",
            "update_tree": "False",
            "update_story": "False",
            "basic": "True"
        },
        requestData2 = {
            "client_id": "test_staging",
            "query": "I am looking for red shoes"
        },
        yData = await axios({
            method: "post",
            url: "http://192.46.209.18:5000/yugasa",
            data: { data: requestData1 },
        }),
        fData = await axios({
            method: "post",
            url: "http://192.46.209.18:5000/findslots",
            data: requestData2,
        });

    // console.log("hgfhg", yData, fData)
    //   async.parallel([
    //     function(callback){
    //         callback(null, yData)
    //     }, 
    //     function(callback){
    //       callback(null, fData)
    //   }
    //   ], function(err, results) {
    //   if(err) console.log(err);
    //   console.log("fgfh",results[0].data)
    //   console.log("gjhg", results[1].data)
    //     res.send({
    //        resp1: results[0].data,
    //        resp2: results[1].data
    //      })
    //     // results now equals to: results.one: 'abc\n', results.two: 'xyz\n'
    // });

    // await axios.all([yData, fData]).then(axios.spread((...responses) => {
    //    let resp1 = responses[0].data,
    //    resp2 = responses[1].data
    //    console.log("gshg", resp1, resp2)
    //    res.send({
    //      resp1: resp1,
    //      resp2: resp2
    //    })
    // })).catch(errors => {
    //   console.log("erro", errors)
    //   res.send(errors)
    // })

    try {
        var data = fData;
        console.log(data.data)
        res.send(data.data)
    } catch (err) {
        console.log("error " + err);
        return res.json({
            success: false,
            message: errorMessage,
            type_option: true,
        });
    }

})

router.get("/fbform", async function(req, res) {
    res.render("fbform.hbs")
})

router.post("/fbform", async function(req, res) {
    res.send(req.body)
})

//API to get whatsapp inbound msgs
router.post("/inboundMsg", whatsappController.getWhatsappMsg)

//********* DB Integration routes ***********//
router.get("/slots", isLoggedIn,accessProFeatures, async function(req, res) {
    checkForToken(req.user._id).then(async function(response) {
        let dataToRender = { userTypePro : req.userTypePro, sideNavValidation: await sideNavValidation(req) };
        res.render("slot.hbs", dataToRender)
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
})
router.post("/uploadSlotJson", isLoggedIn, upload.single("slotJson"), dbController.uploadSlotJson);
router.post("/webhook-actions-list", isLoggedIn, dbController.webhookActionsList);
router.post("/getActnReturnValues", isLoggedIn, dbController.getActnReturnValues)
router.post("/deleteAction", isLoggedIn, dbController.deleteActionById);
router.post("/updateAction", isLoggedIn, dbController.updateAction);
router.post("/getActnsList", isLoggedIn, dbController.getActnsList);
router.post("/getSlotsList", isLoggedIn, dbController.getSlotsList);
router.post("/actionById", isLoggedIn, dbController.actionById);
router.post("/addAction", isLoggedIn, dbController.addAction);
router.post("/deleteSlot", isLoggedIn, dbController.deleteSlot);
router.post("/updateSlot", isLoggedIn, dbController.updateSlot);
router.post("/loadslot", isLoggedIn, dbController.loadslot);
router.post("/slotbyId", isLoggedIn, dbController.slotbyId);
router.post("/trainslot", isLoggedIn, dbController.trainslot);
router.post("/setslotActive", isLoggedIn, dbController.setstatusactive);
router.post("/downloadjson", isLoggedIn, dbController.downloadjsonfile)
router.post("/botrequest", dbController.botrequest)
router.post('/callActionApi', actionController.callActionApi)
router.post('/jsonPathConversion', actionController.jsonPathConversion);
router.post('/slotadd', isLoggedIn, dbController.slotadd)
router.post('/selectedData', dbController.selectedData)
router.get("/subscribers", isLoggedIn,accessProFeatures, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        if (!req.user.isCampaigningEnabled) {
            return res.render('campaignsubscribe.hbs', { clientId: req.user.clientId ,userTypePro: req.userTypePro})
        }
        res.render("subscribers.hbs", {userTypePro : req.userTypePro, clientId: req.user.clientId });
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});
router.get("/whatsappGroup", isLoggedIn, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        res.render("whatsappGroup.hbs", { clientId: req.user.clientId });
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});
router.get("/billing", isLoggedIn, isUserPro, showActivateButton, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        if(!req.userTypePro){
            res.render("billing.hbs", {userTypePro : req.userTypePro, showActivateButton : req.showActivateButton, clientId: req.user.clientId });
        }else{
            return res.render('pageNotFound.hbs');
        }
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});
//  bot language list
router.post('/botLanguage', botApi.botLanguage);

//  bot language list
router.post('/botGenAi', botApi.botGenAi);

//  score count
async function scoreCount(req, res) {

    var startDate = moment().utcOffset(0);
    startDate.format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');

    var endDate = moment().utcOffset(0);
    endDate.subtract(15, 'minutes');
    endDate.toISOString()
    endDate.format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');

    try {
        const score = await db.Userchat.aggregate([{
                $match: {
                    $or: [
                        { "clientId": "saakaar_chatbot" },
                        { "clientId": ObjectID("5f6b1a39419b4121d93eb4bc") },
                    ]
                }
            },
            {
                $match: {
                    $or: [{
                            "createdAt": {
                                "$lt": new Date(startDate),
                                "$gt": new Date(endDate)
                            }
                        },
                        {
                            "updatedAt": {
                                "$lt": new Date(startDate),
                                "$gt": new Date(endDate)
                            }
                        },
                    ]
                }

            },
            {
                $project: {
                    // chats: 1
                    session: 1
                }
            }
        ])

        var newArr = []
        for (var i = 0; i < score.length; i++) {
            var newData = score[i].session.score
            var sum = 0;

            for (var obj in newData) {
                var datas = newData[obj];
                var weight = parseInt(datas.weight)
                sum = (sum + weight);
            }
            sum = sum.toString();
            var newobj = {
                "firstName": score[i].session.name,
                "email": score[i].session.email,
                "mobilePhone": score[i].session.phone,
                "udF_17": sum,
                "DumpdataObjectId": score[i].session.createdDate,
            }
            newArr.push(newobj)
        }

        for (var i = 0; i < newArr.length; i++) {
            var obj = newArr[i]
            var DumpdataObjectid = moment(obj.DumpdataObjectId).format('DDMMYYYYHHmm');

            if (obj.mobilePhone) {
                await axios.post('https://farvisioncloud.com/sfasync/api/syncleads/website', {
                        "firstName": obj.firstName,
                        "lastName": 'ji',
                        "email": obj.email,
                        "mobilePhone": obj.mobilePhone,
                        "comments": "",
                        "originFrom": "Lead from Chatbot",
                        "product": "",
                        "campaign": "",
                        "udF_17": obj.udF_17,
                        "isUpdatefromUIDate": false,
                        "isImported": true,
                        "DumpdataObjectId": DumpdataObjectid,
                        "tenantId": "34"
                    })
                    .then(function(response) {
                        console.log(response);
                    })
                    .catch(function(error) {
                        console.log(error);
                    });

            }

        }

    } catch (error) {
        console.log('error', error);
        // res.status(500).json({
        //     msg:'error',
        //     statusCode:500
        // })
    }
}

const job = new CronJob('*/15 * * * *', async function(req, res) {
    scoreCount(req, res);

}, null, true, 'Asia/Kolkata');
job.start();

/**** routes for language update  ******/
router.get("/languageCode", isLoggedIn, function(req, res) {
    var langCode = req.user.locales;
    if (langCode) {
        res.json({
            msg: "success",
            data: langCode
        })
    } else {
        res.json({
            msg: "error",
            // data:langCode
        })
    }
});

router.get("/language", isLoggedIn,isUserPro, function(req, res) {
    res.render("language.hbs",{ userTypePro : req.userTypePro});
});

router.post("/readJson", isLoggedIn,isUserPro, function(req, res) {
    var clientId = req.user.clientId;
    var language = Object.keys(req.body);
    fs.readFile(process.env.pythonDir + clientId + "/locales/" + language + ".json", { encoding: 'utf-8', charset: 'utf-8' }, function(err, data) {
        if (err) {
            console.log('err', err);
            res.render("language.hbs",{ userTypePro : req.userTypePro});
        } else {
            const languageData = JSON.parse(data);
            res.json({
                msg: "success",
                data: languageData
            })
        }

    });
});

router.post("/updatelanguage", isUserPro, function(req, res) {
    var clientId = req.user.clientId;
    var newjson = req.body.data
    var language = req.body.path
    fs.writeFile(process.env.pythonDir + clientId + "/locales/" + language + ".json", JSON.stringify(newjson), function(err) {
        if (err) {
            console.log('err', err);
            res.render("language.hbs",{ userTypePro : req.userTypePro})
        } else {
            res.render("language.hbs",{ userTypePro : req.userTypePro})
        }
    });
});

/**** routes for whats app  ******/
router.post('/add-user', isLoggedIn, whatsappController.addUser);
// router.post('/user-list',isLoggedIn,whatsappController.userList);
router.get('/viewUserById', isLoggedIn, whatsappController.viewUserById);
router.post('/addMultipleUser', isLoggedIn, whatsappController.addMultipleUser);
router.post('/updateUser', isLoggedIn, whatsappController.updateUser);
router.post('/getWhatsappUserList', isLoggedIn, whatsappController.getWhatsappUserList);
router.post('/createWhatsappTemp', isLoggedIn, whatsappController.createWhatsappTemp);
router.post('/createNewWhatsappTemp', isLoggedIn, whatsappController.createNewWhatsappTemp);
router.post('/assignAgentToTemplate', isLoggedIn, whatsappController.assignAgentToTemplate);
router.post('/updateWhatsappTemp', isLoggedIn, whatsappController.updateWhatsappTemp);
router.post('/getWhatsappTempById', isLoggedIn, whatsappController.getWhatsappTempById);
router.post('/deleteWhatsappTemp', isLoggedIn, whatsappController.deleteWhatsappTemp);
router.post('/deleteCloudWhatsappTemp', isLoggedIn, whatsappController.deleteCloudWhatsappTemp);
router.post('/whatsappTemplateList', isLoggedIn, whatsappController.whatsappTemplateList);
router.post('/whatsappCloudTemplateList', isLoggedIn, whatsappController.whatsappCloudTemplateList);
router.post('/updateGupshupConfig', isLoggedIn, whatsappController.updateGupshupConfig);
router.post('/updateGenAiConfig', isLoggedIn, editProfilecontroller.updateGenAiConfig);
router.post('/onBoardWABA', isLoggedIn, whatsappController.onBoardWABA);
router.post('/getGupshupConfig', isLoggedIn, whatsappController.getGupshupConfig);
router.post('/getGenAiConfig', isLoggedIn, editProfilecontroller.getGenAiConfig);
router.post('/optInOptoutUser', isLoggedIn, whatsappController.optInOptoutUser);
router.post('/sendWhatsAppCampaign', isLoggedIn, whatsappController.sendWhatsAppCampaign);
router.post('/sendWhatsappCampToMultiUser', whatsappController.sendWhatsappCampToMultiUser);
router.post('/setMultiUserOptin', whatsappController.setMultiUserOptin);
router.post('/sendAgentControlRequest', whatsappController.sendAgentControlRequest);
router.get('/getTimerConfigData', whatsappController.getTimerConfigData);
router.post('/updateTimerConfig', whatsappController.updateTimerConfig);
router.post('/deleteMultipleUser', whatsappController.deleteMultipleUser);
router.post('/sendWaCampaignFromSpreadsheet', whatsappController.sendWaCampaignFromSpreadsheet);
router.post('/sendWaCampaignToParticularUser', whatsappController.sendWaCampaignToParticularUser);
router.post('/generateToken', whatsappController.generateToken);
router.post('/getTokenList', whatsappController.getTokenList);
router.post('/deleteTokenById', whatsappController.deleteTokenById);
router.post("/findTempLists", whatsappController.findTempLists);
router.post("/campaignUserListForAgent", whatsappController.campaignUserListForAgent);
router.post("/getSentUserTemplate", whatsappController.getSentUserTemplate);
router.post('/updateClientApiSecret', whatsappController.updateClientApiSecret);
router.get("/findHumanAgent", isLoggedIn, whatsappController.findHumanAgent);
router.get("/currentLogedInClientData", isLoggedIn, whatsappController.currentLogedInClientData);
/*************************************************************
 * Campaign Microservice APIs' here
 ************************************************************8*/
router.post("/getClientDataByClientId", whatsappController.getClientDataByClientId);
router.post("/getGroupDataByGroupName", whatsappController.getGroupDataByGroupName);
router.post("/findOptOutUserList", whatsappController.findOptOutUserList);
router.post("/findTemplateData", whatsappController.findTemplateData);
router.post("/updateWACampStatus", whatsappController.updateWACampStatus);
router.post("/getUserByPhoneNumber", whatsappController.getUserByPhoneNumber);
/**********************************************
 * send attachment from whatsapp to agent panel ,
 * save in s3 and also in user communication 
 ***********************************************/
// router.post('/sendMedia', upload.single("fileList"), async function(req, res) {
//         try {
//             let buffer, mimeType, clientId, userId, name, attachmentType;
//             /**************************************************************************
//              * when file data is URL link then convert the url data into buffer
//              *  data then upload it into s3 server then emit the path to agent panel
//              * ********************************************************************** */
//             if (req.body.url && req.body.url != "" && req.body.url != 'undefined') {
//                 mimeType = req.body.mimeType;
//                 clientId = req.body.clientId;
//                 userId = req.body.userId;
//                 name = req.body.name;
//                 let tempType = mimeType.split("/");
//                 attachmentType = tempType[1];
//                 let response;
//                 if (req.body.token != undefined) {
//                     response = await axios.get(req.body.url, {
//                         responseType: 'arraybuffer',
//                         'headers': { 'Authorization': req.body.token }
//                     });
//                 } else {
//                     response = await axios.get(req.body.url, {
//                         responseType: 'arraybuffer'
//                     });
//                 }

//                 buffer = Buffer.from(response.data, 'base64');
//             }

//             if (req.body.buffer != undefined) {
//                 buffer = Buffer.from(req.body.buffer, 'base64');
//                 mimeType = req.body.mimeType;
//                 clientId = req.body.clientId;
//                 userId = req.body.userId;
//                 name = req.body.name;
//                 let tempType = mimeType.split("/");
//                 attachmentType = tempType[1];
//             }

//             /**************************************************************************
//              * when file data is binary  then  upload it into s3 server then emit the path to agent panel
//              * ********************************************************************** */
//             if (req.file) {
//                 mimeType = req.file.mimetype;
//                 clientId = req.body.fileList[1];
//                 userId = req.body.fileList[0];
//                 name = req.body.fileList[3];
//                 let tempType = mimeType.split("/");
//                 attachmentType = tempType[1];

//                 buffer = fs.readFileSync(req.file.path);
//             }

//             let userData = await db.Userchat.findOne({ userId: userId }, { agentCurrentConrol: 1 });
//             agentControl = 'false';
//             if (userData) {
//                 if (userData.agentCurrentConrol.currentControl == 'true' || userData.agentCurrentConrol.currentControl == true) {
//                     agentControl = 'true';
//                 }
//             } else {
//                 return res.json({
//                     success: false,
//                     statusCode: 201,
//                     message: 'User not found with this user id ',
//                 });
//             }

//             let file_name = 'file';
//             var path;
//             let time = new Date().getTime();
//             filename = `${time}_${file_name}.${attachmentType}`;
//             await AWS.config.update({
//                 accessKeyId: config.s3Linode.accessKeyId,
//                 secretAccessKey: config.s3Linode.secretAccessKey,
//                 region: config.s3Linode.region,
//                 s3BucketEndpoint: config.s3Linode.s3BucketEndpoint,
//             });

//             var s3 = await new AWS.S3({ endpoint: config.s3Linode.url });
//             var params = {
//                 Bucket: `yugasabot/${clientId}/Media/User_files`,
//                 Key: `${filename}`,
//                 ACL: "public-read",
//                 ContentType: mimeType,
//                 Body: buffer,
//             };
//             await s3.putObject(params, function(err, result) {
//                 if (err) {
//                     return res.json({
//                         success: false,
//                         statusCode: 201,
//                         message: 'Error occure during file upload on S3 server ',
//                         error: err
//                     });
//                 } else {
//                     path = `${config.s3Linode.url}/${clientId}/Media/User_files/${filename}`;
//                     console.log('file path  ', path);
//                     io.sockets.emit("message", {
//                         clientId: clientId,
//                         userId: userId,
//                         message: "Attachment sent",
//                         messagetype: 'usermessage',
//                         attachment: true,
//                         name: name ? name : 'Unknown',
//                         attachmentType: attachmentType,
//                         attachmentLink: path,
//                         msgType: 'attachment'
//                     });

//                     db.Userchat.updateOne({ userId: userId }, {
//                         $push: { fileUploaded: { path: path } },
//                         $addToSet: {
//                             chats: {
//                                 text: "Attachment sent",
//                                 attachment: true,
//                                 attachmentType: attachmentType,
//                                 attachmentLink: path,
//                                 agentControl: agentControl,
//                                 messageType: "incoming",
//                                 ignoreMsg: false,
//                                 replies: [],
//                                 node: 'False',
//                                 type_option: true,
//                                 createdAt: new Date(),
//                             },
//                         },
//                     }, function(error, respo) {
//                         if (respo) {
//                             return res.json({
//                                 success: true,
//                                 statusCode: 200,
//                                 message: 'Attachment uploaded to s3 and emit data to agent successfully',
//                                 filePath: path
//                             });
//                         } else {
//                             return res.json({
//                                 success: false,
//                                 statusCode: 201,
//                                 message: 'INTERNAL DB ERROR ',
//                                 error: error
//                             });
//                         }
//                     });

//                 }
//             });

//         } catch (error) {
//             console.log('error in try catch block  ', error);
//             return res.json({
//                 success: false,
//                 statusCode: 201,
//                 message: 'error in catch block may the given URL is invalid',
//                 error: error
//             });
//         }
//     })

router.post('/sendMedia', upload.single("fileList"), async function(req, res) {
        try {
            let buffer, mimeType, clientId, userId, name, attachmentType;
            /**************************************************************************
             * when file data is URL link then convert the url data into buffer
             *  data then upload it into s3 server then emit the path to agent panel
             * ********************************************************************** */
            if (req.body.url && req.body.url != "" && req.body.url != 'undefined') {
                mimeType = req.body.mimeType;
                clientId = req.body.clientId;
                userId = req.body.userId;
                name = req.body.name;
                let tempType = mimeType.split("/");
                attachmentType = tempType[1];
                let response;
                if (req.body.token != undefined) {
                    response = await axios.get(req.body.url, {
                        responseType: 'arraybuffer',
                        'headers': { 'Authorization': req.body.token }
                    });
                } else {
                    response = await axios.get(req.body.url, {
                        responseType: 'arraybuffer'
                    });
                }

                buffer = Buffer.from(response.data, 'base64');
            }
            if (req.body.buffer != undefined) {
                mimeType = req.body.mimeType;
                clientId = req.body.clientId;
                userId = req.body.userId;
                name = req.body.name;
                let tempType = mimeType.split("/");
                attachmentType = tempType[1];
                buffer = Buffer.from(req.body.buffer, 'base64');
            }

            /**************************************************************************
             * when file data is binary  then  upload it into s3 server then emit the path to agent panel
             * ********************************************************************** */
            if (req.file) {
                mimeType = req.file.mimetype;
                clientId = req.body.fileList[1];
                userId = req.body.fileList[0];
                name = req.body.fileList[3];
                let tempType = mimeType.split("/");
                attachmentType = tempType[1];

                buffer = fs.readFileSync(req.file.path);
            }

            let userData = await db.Userchat.findOne({ userId: userId });
            let agentControl = 'false';
            console.log("USERDATA", userData)
            if (userData) {
                if (userData.agentCurrentConrol.currentControl == 'true' || userData.agentCurrentConrol.currentControl == true) {
                    agentControl = 'true';
                }
            } else {
                agentControl = 'false'
                console.log("IN ELSE LOOP")
                return res.json({
                    success: false,
                    statusCode: 201,
                    message: 'User not found with this user id ',
                });
            }
            console.log("DATA********", clientId, userId, userData, agentControl, buffer, attachmentType, mimeType, name)
            await storageController.uploadOnSelectedStorage_sendMedia(req, res, clientId, userId, userData, agentControl, buffer, attachmentType, mimeType, name)
        } catch (error) {
            console.log('error in try catch block  ', error);
            return res.json({
                success: false,
                statusCode: 201,
                message: 'error in catch block may the given URL is invalid',
                error: error
            });
        }
    })
    /**********************************************
     * render whatsapp analytics page .
     ***********************************************/
router.get("/whatsapppAnalytics", isLoggedIn,accessProFeatures, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        if (!req.user.isCampaigningEnabled) {
            return res.render('campaignsubscribe.hbs', { clientId: req.user.clientId,userTypePro: req.userTypePro })
        }
        res.render("campaignAnalytics.hbs", {userTypePro: req.userTypePro, clientId: req.user.clientId, clientObjId: req.user._id });
    }).catch((err) => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { clientId: req.user.clientId, clientObjId: req.user._id });
        });
    });
});
router.post('/updateStatusByMsgId', whatsappController.updateStatusByMsgId);
router.post('/getCampaignAnalytics', isLoggedIn, whatsappController.getCampaignAnalytics);
router.post('/getCampSpecificData', isLoggedIn, whatsappController.getCampSpecificData);
router.post('/deleteCampAnalytics', isLoggedIn, whatsappController.deleteCampAnalytics);
router.post('/whatsappdata', async function(req, res) {
    try {
        let clientGupShupconfig = await db.Client.findOne({ clientId: req.body.clientId }, { gupShupConfig: 1, clientId: 1 });
        if (!clientGupShupconfig) {
            return res.json({
                message: 'Client not found with this clien id',
                statusCode: 201,
                status: false
            })
        }
        if (!clientGupShupconfig.gupShupConfig || clientGupShupconfig.gupShupConfig.password) {
            return res.json({
                message: 'Please set you gupshup credential to send message',
                statusCode: 201,
                status: false
            })
        }

        console.log('clientGupShupconfig==', clientGupShupconfig);
        console.log('clientGupShupconfig==', clientGupShupconfig.gupShupConfig);
        console.log('clientGupShupconfig==', clientGupShupconfig.gupShupConfig.password);

        let data = {
            userid: clientGupShupconfig.gupShupConfig.userId,
            password: clientGupShupconfig.gupShupConfig.password,
            method: "SendMessage",
            auth_scheme: "plain",
            v: "1.1",
            send_to: req.body.send_to,
            msg: req.body.msg,
            format: "json",
            isHSM: "true",
            msg_type: "HSM",
            isTemplate: "true"
        }
        console.log('shiviiii', data)
        return;
        await axios({
            method: "GET",
            url: "https://media.smsgupshup.com/GatewayAPI/rest",
            data: data,
        })
    } catch (error) {
        console.log('error occured in catch block', error);
    }
})

// router.post('/createWhatsappGroup',isLoggedIn,whatsappController.createWhatsappGroup);
// router.post("/userwhatsappGrpdata",isLoggedIn,whatsappController.userwhatsappGrpdata)
// router.post("/getWhatsappGroupData",isLoggedIn,whatsappController.getWhatsappGroupData)
// router.post('/editWhatsappGroup',isLoggedIn,whatsappController.editWhatsappGroup);
// router.post('/updateWhatsappGroup',isLoggedIn,whatsappController.updateWhatsappGroup);
// router.post('/deleteWhatsappGroup',isLoggedIn,whatsappController.deleteWhatsappGroup);

router.post('/getSubscriberList', isLoggedIn, whatsappController.getSubscriberList);
router.post("/getAllSubscriberList", isLoggedIn, whatsappController.getAllSubscriberList);
router.post('/deleteUser', isLoggedIn, whatsappController.deleteUser);

router.post("/clientAPI", actionController.clientAPI) //dummy api used as client api
router.get("/manage-agents", isLoggedIn, async function(req, res, next) {
    checkForToken(req.user._id).then(async function(response) {
        if (!req.user.isHumanAgentEnabled) {
            return res.render('agentsubscribe.hbs', { clientId: req.user.clientId })
        }
        let userchat = await db.Userchat.aggregate(
            [{
                    $match: {
                        $or: [{ clientId: req.user.clientId }, {
                            clientId: ObjectID(req.user._id)
                        }]
                    }
                },
                {
                    $group: {
                        "_id": "$clientId",
                        "total_sum": { "$sum": { "$size": "$chats" } }
                    }
                },
            ]
        ).allowDiskUse(true);
        res.render("agents.hbs", { clientId: req.user.clientId, chatcount: userchat[0] ? (userchat[0].total_sum) : 0, sideNavValidation: await sideNavValidation(req) });
    }).catch(err => {
        req.session.destroy(function(err) {
            req.logout();
            res.render("login.hbs", { title: "Yugasa Bot Admin" });
        });
    })
});

//Function to get template name based on message id from analytics
router.post("/getTemplateName", async function(req, res) {
    try {
        const campaign = await db.campAnalytics.find({ messageId: req.body.messageId }, { tempName: 1 });
        // console.log(campaign);
        if (campaign.length <= 0) {
            return res.status(400).json({
                status: "fail",
                message: "no tempName found with this message id"
            })
        }
        res.status(200).json({
            status: "success",
            result: campaign.length,
            data: {
                campaign
            }
        })
    } catch (err) {
        // console.log(err.message)
        res.status(400).json({
            status: "fail",
            message: "error in catch"
        })
    }
})



//function for side nav validation
async function sideNavValidation(req) {
    let sideNavValidationObj = {
        isActionEnabled: req.user.isActionEnabled ? req.user.isActionEnabled : false,
        isHumanAgentEnabled: req.user.isHumanAgentEnabled ? req.user.isHumanAgentEnabled : false,
    };
    return sideNavValidationObj;
}

//Whatsapp Opt-in Opt-out route
router.post("/usersInfo", usersController.saveUpdtUsersInfoInDb)

function getsrc(userid) {
    if (!userid) {
        return false;
    }
    let src = userid.split("_")[0];
    switch (src) {
        case "wa":
            return { "src": src, "title": "WhatsApp" }
            break;
        case "fb":
            return { "src": src, "title": "Facebook Messenger" }
            break;
        case "gbm":
            return { "src": src, "title": "Google Business Messages" }
            break;
        case "tg":
            return { "src": src, "title": "Telegram Messenger" }
            break;
        case "ig":
            return { "src": src, "title": "Instagram" }
            break;
        case "android":
            return { "src": src, "title": "Android App" }
            break;
        case "ios":
            return { "src": src, "title": "iOS App" }
            break;
        default:
            return { "src": "web", "title": "Website" };
            break;
    }
}

async function checkForToken(user) {
    console.log("In check token function", user)
    return new Promise((resolve, reject) => {
        db.Client.find({ _id: user }, async function(error, clientData) {
            if (error) {
                reject();
            } else if (clientData) {
                let session_token = clientData[0]._doc.sessionToken
                if (session_token == "") {
                    reject();
                } else {
                    try {
                        const verified = jwt.verify(session_token, "YUGASA@BOT@123*");
                        console.log("verified", verified)
                        if (verified) {
                            resolve(true)
                        }
                    } catch (err) {
                        reject();
                    }
                }
            }
        });
    })
}

router.post('/saveCodes', codeNodeController.saveCodes);
router.post('/getCodes', codeNodeController.getCodes);
router.post('/generateCode', codeNodeController.generateCode);
router.post('/codeNodeFunctions', codeNodeController.codeNodeFunctions)
router.post('/deleteCodeNode', codeNodeController.deleteCodeNode)
router.post('/getActionDetails', codeNodeController.getActionDetails)
router.post("/activateCodeNodeLog", codeNodeController.activateCodeNodeLog);
router.post("/getCodeNodeLogs", codeNodeController.getCodeNodeLogs);
router.post("/saveCodeNodeErrorLog", codeNodeController.saveCodeNodeErrorLog);
router.post("/removeCodeNodeLogs", codeNodeController.removeCodeNodeLogs);

router.post('/updatePatterns', isLoggedIn, intentscontroller.updatePatterns);
router.post('/agentTemplate', isLoggedIn, agentTemplateController.agentTemplate)
router.post('/getAgentTempList', agentTemplateController.getAgentTempList)
router.post('/uploadMedia', isLoggedIn, upload.single('whatsappMedia'), whatsappController.uploadMedia)
router.post('/uploadWhatsappMedia', isLoggedIn, upload.single('whatsappMedia'), whatsappController.uploadWhatsappMedia)
router.post('/uploadConversationalMedia', isLoggedIn, upload.single('conversationalMedia'), treecontroller.uploadConversationalMedia)
router.post('/stopPromotion', optOutController.stopPromotion)
router.get('/stopPramotionNumber', isLoggedIn, optOutController.getStopPramotionNumber)
router.delete('/deleteStopPramotionNumber', isLoggedIn, optOutController.deleteStopPramotionNumber)

router.post('/createCampaign', isLoggedIn, scheduledCampaignController.createCampaign)
router.get('/scheduledCampaignList', isLoggedIn, scheduledCampaignController.scheduledCampaignList)
router.post('/getScheduledCampaign', isLoggedIn, scheduledCampaignController.getScheduledCampaign)
router.put('/updateScheduledCampaign', isLoggedIn, scheduledCampaignController.updateScheduledCampaign)
router.delete('/deleteScheduledCampaign', isLoggedIn, scheduledCampaignController.deleteScheduledCampaign)
router.post('/sendWhatsupCampaignInGroup', scheduledCampaignController.sendWhatsupCampaignInGroup)
router.post("/deleteAccountRequest", isLoggedIn, whatsappController.deleteAccountRequest);
router.get("/getDeleteRequestStatus", isLoggedIn, whatsappController.getDeleteRequestStatus);
router.post("/setAnalyticsFlag", isLoggedIn, async function(req, res) {
    let updateData = await db.Intent.updateOne({ _id: req.body.id, clientId: req.user.clientId }, {
        $set: {
            analyticsFlag: req.body.analyticsFlag,
        },
    });
    res.send({ message: "Status change successfully", statusCode: 200, status: true });
});

function getOccurrence(array, value) {
    console.log("LENGTH", array.filter((v) => (v === value)))
    return array.filter((v) => (v === value)).length;
}

//Routes for Azure Storage

router.post("/uploadImgToAzureBob", upload.single("fileList"), async function(req, res) {
    console.log("In uploadImgToAzureBob url")
    azureController.createBlobFromReadStream(req)
        ///console.log("File", req.file)
})
router.post("/checkIfContainerExists", upload.single("fileList"), async function(req, res) {
    console.log("In checkIfContainerExists url")
    azureController.checkIfContainerExists(req)
        ///console.log("File", req.file)
})

router.post("/getBlobsInContainer", upload.single("fileList"), async function(req, res) {
    console.log("In getBlobsInContainer url")
    azureController.getBlobsInContainer(req)
        ///console.log("File", req.file)
})
router.post("/createContainer", upload.single("fileList"), async function(req, res) {
    console.log("In createContainer url")
    azureController.createContainer(req)
        ///console.log("File", req.file)
})

router.post("/getcontainerList", upload.single("fileList"), async function(req, res) {
    console.log("In getContainer list url")
    azureController.getcontainerList(req)
        ///console.log("File", req.file)
})

//Azure routes end

router.post("/checkIfNewUser", async function(req, res) {
    console.log("In check user function", req.body)
    try {
        let client = await db.Client.findOne({ clientId: req.body.clientId });
        if (!client) {
            return res.json({
                success: false,
                message: errorMessage,
                type_option: true,
            });
        }
        console.log("CLIENT*****", client)
        const user = await db.Userchat.findOne({ userId: req.body.userId, clientId: client._id });
        console.log("USERS****", user)
        let userData
        if (!user) {
            console.log("USER NOT EXISTS")
            userData = { "newUser": true }
            res.status(200).json({
                    statusCode: 200,
                    status: "success",
                    result: userData,
                })
                //return userData
                //checkNewUserNotificationFlag(req.body.username, req.body.userId)
        } else {
            console.log("USER CHATS: ", user.chats, user.chats.length)
            if (user.chats.length <= 3) {
                console.log("USER NOT EXISTS 1")
                userData = { "newUser": true }
            } else {
                console.log("USER ALREADY EXISTS", userData)
                userData = { "newUser": false }
            }
            ///return userData
            res.status(200).json({
                statusCode: 200,
                status: "success",
                result: userData,
            })
        }
    } catch (err) {
        console.log("ERROR in catch block", err)
        return false
    }
})

async function checkNewUserNotificationFlag(clientId, userId) {
    console.log("checkNewUserNotificationFlag", clientId)
    let clientData = await db.Client.findOne({ "clientId": clientId })
    if (clientData) {
        if (clientData.whatsappNotification == true) {
            let data = { "newUser": true }
            return data
        } else if (clientData.emailNotification == true) {
            let data = { "newUser": true }
            return data
        } else {
            return false
        }
    } else {
        console.log("No client available having clientId = ", clientId)
    }
}

router.patch("/dashboard/filter-date", isLoggedIn, async(req, res) => {
    const { day } = req.body;

    await db.Client.updateOne({ clientId: req.user.clientId }, { $set: { filterDays: day } });
});

router.post("/agent-template-data", isLoggedIn, async function(req, res, next) {
    try {
        let client_id = req.user.clientId;
        const { agentWhatsAppTemplate1 } = req.body;
        const clientData = await db.Client.findOne({ clientId: client_id });
        if (agentWhatsAppTemplate1 == 1) {
            await db.Client.updateOne({ clientId: req.user.clientId }, { $set: { agentTemplate: "" } });

        }
        if (!clientData) {
            res.redirect('/adminPanel')
        }
        await db.Client.updateOne({ clientId: req.user.clientId }, { $set: { agentTemplate: ObjectID(req.body.agentWhatsAppTemplate1) } });
        res.redirect('/subscription')
    } catch (err) {
        res.redirect('/subscription')
    }
})

router.get("/template-list", isLoggedIn, async function(req, res, next) {
    let templateData = [];
    const template = await db.whatsAppTemplate.find({ status: 'APPROVED', clientId: req.user.clientId, msg_type: 'TEXT', button_url: false, text: { '$not': { '$regex': '\\{.*\\}' } } });
    template.forEach((ele) => {
        templateData.push({ id: ele._id, name: ele.name })
    });
    res.status(200).json({
        status: "success",
        message: "Template name found",
        result: templateData,
    });
});


router.post("/approveUser", async function(req, res) {
    try {
        let username = req.body.clientId;
        if (username == undefined) {
            res.status(200).json({
                status: "failed",
                message: "clientId is manadatory"
            });
        } else {
            var user = await db.Client.find({
                $or: [{ clientId: username }, { email: username }],
            });
            if (user.length > 0) {
                if (user[0].email_verified == false || !user[0].email_verified) {
                    await emailverifycontroller.resendVerificationMail(user[0]);
                    res.status(200).json({
                        status: "success",
                        message: "Verification email sent to the client on registered email address."
                    });
                } else {
                    res.status(200).json({
                        status: "failed",
                        message: "Client is already verified"
                    });
                }
            } else {
                res.status(200).json({
                    status: "failed",
                    message: "clientId not found."
                });
            }
        }
    } catch (err) {
        console.log("Error approveUser", err);
        res.status(200).json({
            status: "failed",
            message: "There was some error approving the user."
        });
    }
});

router.get("/approveUser", async function(req, res) {
    try {
        let username = req.query.clientId;
        if (username == undefined) {
            res.status(200).json({
                status: "failed",
                message: "clientId is manadatory"
            });
        } else {
            var user = await db.Client.find({
                $or: [{ clientId: username }, { email: username }],
            });
            if (user.length > 0) {
                if (user[0].email_verified == false || !user[0].email_verified) {
                    await emailverifycontroller.resendVerificationMail(user[0]);
                    res.status(200).json({
                        status: "success",
                        message: "Verification email sent to the client on registered email address."
                    });
                } else {
                    res.status(200).json({
                        status: "failed",
                        message: "Client is already verified"
                    });
                }
            } else {
                res.status(200).json({
                    status: "failed",
                    message: "clientId not found."
                });
            }
        }
    } catch (err) {
        console.log("Error approveUser", err);
        res.status(200).json({
            status: "failed",
            message: "There was some error approving the user."
        });
    }
});

router.post("/updateServerConfiguration", isLoggedIn, async function(req, res) {
    console.log("API call")
    try {
        db.Client.find({ "clientId": req.user.clientId }, async function(error, clientData) {
            if (error) {
                reject();
            } else if (clientData) {
                db.Client.updateOne({ "clientId": req.user.clientId }, {
                    $set: {
                        "storageUsed": req.body.storageUsed,
                        "storageConfigKeys": req.body.configData
                    }
                }, async function(updateError, updateResult) {
                    if (updateError) {
                        res.status(401).json({
                            statusCode: 401,
                            status: "fail",
                            result: "Error while updating storage configuration",
                        })
                    } else if (updateResult) {
                        res.status(200).json({
                            statusCode: 200,
                            status: "success",
                            result: "Storage configuration data updated successfully",
                        })
                    } else {
                        res.status(401).json({
                            statusCode: 401,
                            status: "fail",
                            result: "Error while updating server configuration",
                        })
                    }
                })
            }
        });
    } catch (err) {
        res.status(400).json({
            status: "fail",
            message: "error in catch"
        })
    }
})

router.post("/getServerConfiguration", async function(req, res) {
    try {
        console.log("In get server config route***********", req.user)
        db.Client.find({ "clientId": req.user.clientId }, async function(error, clientData) {
            if (error) {
                reject();
            } else if (clientData) {
                console.log("clientData", clientData)
                let responseObj = {
                    storageName: clientData[0].storageUsed,
                    storageData: clientData[0].storageConfigKeys
                }
                console.log("responseObj********", responseObj)
                res.status(200).json({
                    status: "success",
                    statusCode: 200,
                    message: "Server config data fetched successfully",
                    data: responseObj
                })
            }
        });
    } catch (err) {
        console.log("ERROR ", err)
        res.status(400).json({
            status: "fail",
            statusCode: 400,
            message: "error in catch block"
        })
    }
});
router.patch("/zeroShotStatus", isLoggedIn, intentscontroller.inActiveStatusBulkShot);
router.patch("/zeroShotAnalyticStatus", isLoggedIn, intentscontroller.inActiveAnalyticStatusBulkShot)
router.post("/editBulkIntentData", isLoggedIn, intentscontroller.editBulkIntentData);
router.post("/getZeroShotDataById", isLoggedIn, intentscontroller.getZeroShotDataById);
router.get("/getIntentBulkData", isLoggedIn, intentscontroller.getBulkIntentData)
router.delete("/delete/:Tag", isLoggedIn, intentscontroller.deleteBulkIntent);
router.post("/addQuestion", isLoggedIn, intentscontroller.insertFAQ)
router.get('/excel', isLoggedIn, intentscontroller.downloadExcel)
router.post('/bulkFaqs', upload.single('file'), intentscontroller.insertDataBulkIntent);
router.get("/readLanguage", isLoggedIn, languageConvert.readLanguage);
router.post('/uploadExcelLanguage', isLoggedIn, upload.single('excelFile'), languageConvert.convertExcelToJson);
router.post("/deleteSelected", isLoggedIn, intentscontroller.deleteSelected)

router.post("/activateCodeNodeMonitor", isLoggedIn, async function(req, res) {
    try {
        let clientData = await db.Client.find({ "clientId": req.user.clientId });
        if (clientData) {
            db.Client.updateOne({ "clientId": req.user.clientId }, {
                $set: {
                    "codeNodeMonitorFlag": req.body.codeNodeMonitorFlag,
                    "spreadsheetURL": req.body.spreadsheetURL,
                    "sheetName": req.body.sheetName
                }
            }, async function(updateError, updateResult) {
                if (updateError) {
                    res.status(401).json({
                        statusCode: 401,
                        status: "fail",
                        result: "Error while updating the settings",
                    })
                } else {
                    res.status(200).json({
                        statusCode: 200,
                        status: "success",
                        result: "Settings updated successfully",
                    })
                }
            })
        } else {
            res.status(404).json({
                statusCode: 404,
                status: "fail",
                result: "Client not found",
            })
        }
    } catch (err) {
        console.log("ERROR ", err)
        res.status(400).json({
            status: "fail",
            statusCode: 400,
            message: "error in catch block"
        })
    }
})


router.post("/showCodeNodeSettings", isLoggedIn, async function(req, res) {
    try {
        db.Client.find({ "clientId": req.user.clientId }, async function(error, clientData) {
            if (clientData.length > 0) {
                let data = {
                    codeNodeMonitorFlag: clientData[0].codeNodeMonitorFlag,
                    spreadsheetURL: clientData[0].spreadsheetURL,
                    sheetName: clientData[0].sheetName
                }
                res.status(200).json({
                    statusCode: 200,
                    status: "success",
                    result: "Settings data fetched successfully",
                    data: data
                })
            } else if (clientData.length > 0) {
                res.status(404).json({
                    statusCode: 404,
                    status: "fail",
                    result: "Client not found",
                    data: data
                })
            } else {
                res.status(401).json({
                    statusCode: 401,
                    status: "fail",
                    result: "Error while fetching settings",
                    data: data
                })
            }
        })
    } catch (err) {
        console.log("ERROR ", err)
        res.status(400).json({
            status: "fail",
            statusCode: 400,
            message: "error in catch block"
        })
    }
})
router.post("/readCodeNodeMonitorFlag", async function(req, res) {
    try {
        db.Client.find({ "clientId": req.body.clientId }, async function(error, clientData) {
            if (clientData.length > 0) {
                let data = {
                    codeNodeMonitorFlag: clientData[0].codeNodeMonitorFlag,
                    spreadsheetURL: clientData[0].spreadsheetURL,
                    sheetName: clientData[0].sheetName
                }
                res.status(200).json({
                    statusCode: 200,
                    status: "success",
                    result: "Settings data fetched successfully",
                    data: data
                })
            } else if (clientData.length > 0) {
                res.status(404).json({
                    statusCode: 404,
                    status: "fail",
                    result: "Client not found",
                })
            } else {
                res.status(401).json({
                    statusCode: 401,
                    status: "fail",
                    result: "Error while fetching settings",
                })
            }
        })
    } catch (err) {
        console.log("ERROR ", err)
        res.status(400).json({
            status: "fail",
            statusCode: 400,
            message: "error in catch block"
        })
    }
})
router.post("/api/template",  agentTemplateController.handleTemplateRequest )

// Restart Message in bot ON and OFF switch API.
router.post('/restart-message-status', isLoggedIn,async function(req,res,next){
    try{
    let clientId =  req.user.clientId;
    let status = req.body.status;

    if(status === "true"){
        status = true;
        await db.Client.updateOne({ clientId: clientId }, { $set: { restartMessageStatus: status} })
        await redisClient.hSet("config_"+clientId,"restartMessageStatus","true")
        res.status(200).json({status:"success",statusCode:200,message:"Status activated successfully."});
    }else if(status === "false"){
        status = false;
        await db.Client.updateOne({ clientId: clientId }, { $set: { restartMessageStatus: status} });
        await redisClient.hSet("config_"+clientId,"restartMessageStatus","false")
        res.status(200).json({status:"success",statusCode:200,message:"Status deactivated successfully."});
    }else{
        res.status(201).json({status:"error",statusCode:201,message:"Something went wrong"});
    }
}catch(err){
    console.log(err)
}
});
//*****************Coupon related router here ***********************//
router.post('/addCoupon', couponController.addCoupon);
router.post('/updateCoupon', couponController.updateCoupon);
router.post('/deleteCoupon', couponController.deleteCoupon);
router.post('/getCouponList', couponController.getCouponList);
router.post('/getAllcouponList', couponController.getAllcouponList);
router.post('/applyCoupon', couponController.applyCoupon);
//**************basic user subscription router here*****************//
router.post("/getClientSubscriptionDetails", isLoggedIn, paymentController.getClientSubscriptionDetails);

/*******************************************************************************************************
*check user type =  pro or basic
********************************************************************************************************/
async function isUserPro(req, res, next) {
    try {
        let client = await db.Client.findOne({ clientId: req.user.clientId });
        let userTypePro = client.type == 'pro' ? true : false ;
        req.userTypePro = userTypePro;
        next();
    }catch(err){
        console.log('Error in catch block isUserPro', err)
   }
}
/*******************************************************************************************************
*show Activate now button for BASIC user if user not subscribed and dont show this button for PRO user
********************************************************************************************************/
async function showActivateButton(req, res, next){
    try{
        let client = await db.Client.findOne({ clientId: req.user.clientId });
        if(client.type == 'basic'){
                let currentDate = new Date(); 
                let subscriptionEndDate;
                
                if(client.subscription == undefined || 
                    client.subscription == null ||
                    client.subscription.endDate == undefined ||
                    client.subscription.endDate ==null || 
                    client.subscription.endDate == ""){
                        subscriptionEndDate = 0 ;
                }else{
                    subscriptionEndDate = client.subscription.endDate ;
                    subscriptionEndDate = new Date(subscriptionEndDate);
                }
                
                if( subscriptionEndDate > currentDate ) { 
                    req.showActivateButton = false;
                    next();
                } else if( subscriptionEndDate < currentDate ) {  
                    req.showActivateButton = true;
                    next();
                }
         }else{
            req.showActivateButton = false;
             next();
         }
    }catch(error){
        console.log('Error in catch block ---:', error)
    }
}
async function chatWindowMiddleware(req, res, next){
    try{
        //*****check url is preview link url ********//
        let url = (req.query.url) ? req.query.url : ''; 
        // const url =  'https://preview.helloyubo.com/?url=https%3A%2F%2Fuat.bookmyforex.com%2F%23sellForex&username=bookmyforex#sellForex&username=bookmyforex'
        // const url =  'https://?url=https%3A%2F%2Fuat.bookmyforex.com%2F%23sellForex&username=bookmyforex#sellForex&username=bookmyforex'
        req.isPreviewUrl = url.includes("preview.helloyubo.com") ? true : false ;
        req.subscribed = false;
        let decodedToken = jwt.verify(req.query.clientId, "yubo2020");
        let client = await db.Client.findOne({ clientId: decodedToken.clientId });

        if(client.type == 'basic'){
            let currentDate = new Date() , subscriptionEndDate;
            if(client.subscription == undefined || 
                client.subscription == null ||
                client.subscription.endDate == undefined ||
                client.subscription.endDate ==null || 
                client.subscription.endDate == ""){
                    subscriptionEndDate = 0 ;
            }else{
                subscriptionEndDate = client.subscription.endDate ;
                subscriptionEndDate = new Date(subscriptionEndDate);
            }
           
           if (currentDate < subscriptionEndDate) {
                req.subscribed = true;
                next();
            } else if (currentDate > subscriptionEndDate) {
                req.subscribed = false;
                next();
            }
        }else{
              req.subscribed = true;
              next();
        }
    }catch(error){
        console.log('Error in catch block ---:', error)
    }
}
/*******************************************************************************************************
*pro features can access by only user type=pro  , for basic user you can not hit the router as well
*so this is a router restriction we have added here
********************************************************************************************************/
function accessProFeatures(req, res, next) {
    let userTypePro = req.user.type == 'pro' ? true : false ;
    req.userTypePro = userTypePro;
    if(userTypePro){
        next();
    }else{
        return res.render('pageNotFound.hbs');
    }
}
module.exports = router;
