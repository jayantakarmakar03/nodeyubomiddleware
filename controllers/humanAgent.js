const db = require('../models/all-models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mongo = require('mongodb');
const fs = require("fs");
var rimraf = require("rimraf");
const { log } = require('debug');
const ObjectID = mongo.ObjectID;
require('dotenv').config();
const salt = 10;
const secretKey = process.env.secretKey;
const emailverifycontroller = require("../controllers/emailverification.js");
const config = require("../config/config.json");
const yudesk_access_token = process.env.yudesk_access_token;
const axios = require('axios');
escapeHtml = require('escape-html');
const mongoose = require("mongoose")

const AdgentController = {

    addHumanAgent: async(req, clientId, res) => {
        req.body.name       =  escapeHtml(req.body.name);
        req.body.mobile     =  escapeHtml(req.body.mobile);
        req.body.department =  escapeHtml(req.body.department);
        req.body.periority  =  escapeHtml(req.body.periority);

        if (!clientId) {
            res({
                status: false,
                statusCode: 201,
                message: 'please provide client id'
            })
        }
        try {
            let agentDetails = await db.humanagents.find({
                agents: { $elemMatch: { email: req.body.email } }
            });

            if (agentDetails.length > 0) {
                res({
                    status: false,
                    statusCode: 203,
                    message: "Agent Already exist with this emailId"
                })
            } else {

                let clientDetails = await db.humanagents.findOne({
                    clientId: clientId
                });
                const hashedpassword = await
                bcrypt.hash(req.body.password, salt)
                req.body = {
                    ...req.body,
                    ... {
                        _id: new
                        ObjectID,
                        isBlocked: false,
                        createdAt: new Date(),
                        updatedAt: new
                        Date(),
                        clientId: clientId,
                        password: hashedpassword
                    }
                }

                if (!clientDetails) {
                    let saveData = new db.humanagents({
                        clientId: clientId,
                        agents: [req.body]
                    });

                    await saveData.save(function(err, result) {
                        if (err) {
                            res({
                                statusCode: 201,
                                status: false,
                                message: "INTERNAL DB ERROR",
                                res: err
                            })
                        } else {
                            res({
                                statusCode: 200,
                                status: true,
                                message: "Agent added successfully",
                            })
                        }
                    });

                } else {

                    let update = db.humanagents.updateOne({ clientId: clientId }, { '$push': { 'agents': req.body } }, { safe: true, multi: true }, function(err, result) {
                        if (err) {
                            res({
                                statusCode: 201,
                                status: false,
                                message: 'Not added ',
                                error: err
                            })
                        } else {
                            res({
                                statusCode: 200,
                                status: true,
                                message: "Agent added successfully",
                            })
                        }
                    });
                }
            }
        } catch (error) {
            console.log("error occured", error)
        }

    },
    //get all human agent list by clienid with each agent how many user handled
    viewHumanAgent: async(clientId, res) => {
        try {
            var tokenData = { clientId: clientId };
            var token = jwt.sign(tokenData, secretKey, {
                expiresIn: 86400 // expires in 24 hours
            });
            // { $replaceRoot: { newRoot: "$agents" } },
            let agentDetails = await db.humanagents.aggregate([
                { $match: { "clientId": clientId } },
                { $unwind: "$agents" },
                {
                    $lookup: {
                        from: "userchats",
                        localField: "agents._id",
                        foreignField: "agentsCommunicated",
                        as: "handledUser",
                    }
                },
                {
                    $project: {
                        _id: 0,
                        clientId: 1,
                        // "handledUserData":"$handledUser",
                        // agents : 1,
                        "handledUser": { $size: "$handledUser" },
                        name: "$agents.name",
                        email: "$agents.email",
                        mobile: "$agents.mobile",
                        isBlocked: "$agents.isBlocked",
                        _id: "$agents._id",
                    }
                },
                {
                    $lookup: {
                        from: "agentresponsetimes",
                        localField: "_id",
                        foreignField: "agentId",
                        as: "resposeData",
                    }
                },
                {
                    $project: {
                        clientId: 1,
                        handledUser: 1,
                        name: 1,
                        email: 1,
                        mobile: 1,
                        isBlocked: 1,
                        _id: 1,
                        responseTime: "$resposeData.responseTime",
                        listAllMillisec: "$resposeData.totalSumOfMilliSec",
                        listAllCount: "$resposeData.count",
                        totalMilliSec: { "$sum": "$resposeData.totalSumOfMilliSec" },
                        totalCount: { "$sum": "$resposeData.count" },
                        averageResponseTime: { $divide: ["$totalMilliSec", "$totalCount"] }
                    }
                }
            ]);
            res({
                statusCode: 200,
                status: true,
                message: "Agent list with related data",
                res: agentDetails,
                accessToken: token
            })
        } catch (error) {
            console.log("error occured", error)
        }
    },
    updateHumanAgent: async(data, clientId, res) => {

        try {
            data.name       =  escapeHtml(data.name);
            data.department =  escapeHtml(data.department);
            let clientData = await db.humanagents.findOne({
                clientId: clientId
            });
            if (clientData == null) {
                res({
                    statusCode: 201,
                    status: false,
                    message: "Client not  found with this client id"
                })
            } else {

                if (clientData.agents.length > 0) {

                    index = clientData.agents.findIndex(x => x._id ==
                        data.agentId);
                    if (index != -1) {
                        let update = await db.humanagents.updateOne({
                            'clientId': clientId,
                            'agents._id': ObjectID(data.agentId)
                        }, {
                            '$set': {
                                'agents.$.name': data.name,
                                'agents.$.email': data.email,
                                'agents.$.mobile': data.mobile,
                                'agents.$.department': data.department,
                                'agents.$.periority': data.periority,
                                'agents.$.updatedAt': new Date()
                            }
                        }, function(err, result) {
                            if (result) {
                                res({
                                    status: true,
                                    message: 'Agent updated successfully',
                                    statusCode: 200,
                                })
                            } else {
                                res({
                                    status: false,
                                    message: 'INTERNAL DB ERROR ',
                                    statusCode: 201,
                                })
                            }
                        });

                    } else {
                        res({
                            status: false,
                            message: "Agent Id not exists",
                            statusCode: 201,
                        })
                    }

                } else {
                    res({
                        status: false,
                        message: "No agent added till now",
                        statusCode: 201,
                    })
                }
            }
        } catch (error) {
            console.log("error occured", error)
        }

    },
    updateAgentPassword: async(data, token, res) => {

        try {
            let clientId = data.decoded.clientId;

            let clientData = await db.humanagents.findOne({
                clientId: clientId
            });
            if (clientData == null) {
                res({
                    statusCode: 201,
                    status: false,
                    message: "Client not  found with this client id"
                })
            } else {
                if (clientData.agents.length > 0) {
                    const hashedpassword = await bcrypt.hash(data.password, salt)
                    index = await clientData.agents.findIndex(x => x.email == data.decoded.agentEmail);
                    let agentData = await clientData.agents.filter(x => x.email == data.decoded.agentEmail);
                    if (agentData[0].resetToken == token) {
                        if (index != -1) {
                            let update = await db.humanagents.updateOne({
                                'clientId': clientId,
                                'agents.email': data.decoded.agentEmail
                            }, {
                                '$set': {
                                    'agents.$.password': hashedpassword,
                                    'agents.$.updatedAt': new Date()
                                }
                            }, function(err, result) {
                                if (result) {
                                    res({
                                        status: true,
                                        message: 'Password changed successfully',
                                        statusCode: 200,
                                    })
                                } else {
                                    res({
                                        status: false,
                                        message: 'INTERNAL DB ERROR ',
                                        statusCode: 201,
                                    })
                                }
                            });
                        } else {
                            res({
                                status: false,
                                message: "Agent Id not exists",
                                statusCode: 201,
                                clientData: clientData
                            })
                        }
                    } else {
                        res({
                            statusCode: 202,
                            status: false,
                            message: "You email verification mail has been expired, Please try again"
                        })
                    }

                } else {
                    res({
                        status: false,
                        message: "No agent added till now",
                        statusCode: 201,
                    })
                }
            }
        } catch (error) {
            console.log("error occured", error)
        }

    },
    deleteHumanAgent: async(req, clientId, res) => {
        try {
            //***Find the agent array by client id *****/
            let humanagents = await db.humanagents.findOne({ clientId: clientId });
            if (humanagents == null) {
                res({
                    statusCode: 201,
                    status: false,
                    message: 'Client not found with this client id'
                })
            }
            //***Before delete the user leave the all user control taken by agent *****/
            let agentTookControlUserList = await db.Userchat.find({
                "agentCurrentConrol.agentId": req.body.agentId,
                "agentCurrentConrol.currentControl": true
            }, { userId: 1, _id: 0 });

            if (agentTookControlUserList) {
                userListArray = await agentTookControlUserList.map(ele => ele.userId);
                if (userListArray && userListArray.length > 0) {
                    updateUserData = await db.Userchat.update({ userId: { $in: userListArray } }, {
                        $set: {
                            "agentCurrentConrol": { "agentId": "", "currentControl": false },
                            "chatControl": 0,
                        }
                    }, { multi: true });
                    // console.log('updateUserData====--=>',updateUserData);
                }
                //console.log('userListArray----======',userListArray);
                //***now delete the agent code here *****/
                if (humanagents && humanagents.agents.length > 0) {
                    let update = await
                    db.humanagents.updateOne({ clientId: clientId }, {
                            '$pull': {
                                'agents': {
                                    '_id': ObjectID(req.body.agentId)
                                }
                            }
                        }, { multi: true },
                        function(err, result) {
                            // console.log('result===',result);
                            if (result) {
                                res({
                                    statusCode: 200,
                                    status: true,
                                    message: 'Agent deleted successfully'
                                })
                            } else {
                                res({
                                    statusCode: 201,
                                    status: false,
                                    message: 'INTERNAL DB ERROR',
                                    err: err
                                })
                            }
                        });
                } else {
                    res({
                        statusCode: 201,
                        status: false,
                        message: 'Agent not found with this agent id'
                    })
                }
            } else {
                res({
                    statusCode: 200,
                    message: "Agent have not taken any control 2",
                    status: true,
                    res: agentTookControlUserList
                });
            }
        } catch (err) {
            console.log('error occured', err)
        }
    },
    viewParticularAgent: async(data, clientId, res) => {
        try {
            let agentDetails = await db.humanagents.aggregate([
                { $match: { "clientId": clientId } },
                { $unwind: "$agents" },
                { $match: { "agents._id": ObjectID(data.agentId) } },
                { $replaceRoot: { newRoot: "$agents" } }
            ]);
            // console.log('agentDetails', agentDetails)
            if (agentDetails == null) {
                res({
                    statusCode: 201,
                    status: false,
                    message: "Agent not found"
                })
            } else {
                res({
                    statusCode: 200,
                    status: true,
                    message: "Agent Details",
                    res: agentDetails
                })
            }
        } catch (error) {
            console.log("error occured", error)
        }
    },
    blockUnblockAgent: async(req, clientId, res) => {
        try {
            let clientData = await db.humanagents.findOne({
                clientId: clientId
            });
            if (clientData == null) {
                res({
                    statusCode: 201,
                    status: false,
                    message: "Client not  found with this client id"
                })
            } else {

                let status;
                if (req.isBlocked === true || req.isBlocked == 'true') {
                    status = true;
                } else {
                    status = false;
                }
                if (clientData.agents.length > 0) {

                    index = clientData.agents.findIndex(x => x._id ==
                        req.agentId);
                    if (index != -1) {
                        let update = await db.humanagents.updateOne({
                            'clientId': clientId,
                            'agents._id': ObjectID(req.agentId)
                        }, {
                            '$set': {
                                'agents.$.isBlocked': status
                            }
                        }, function(err, result) {
                            if (result) {

                                if (status === true) {
                                    status = 'blocked';
                                } else {
                                    status = 'unblocked';
                                }
                                res({
                                    status: true,
                                    message: `Agent  ${status} successfully`,
                                    statusCode: 200,
                                })
                            } else {
                                res({
                                    status: false,
                                    message: 'INTERNAL DB ERROR ',
                                    statusCode: 201,
                                })
                            }
                        });

                    } else {
                        res({
                            status: false,
                            message: "Agent Id not exists",
                            statusCode: 201,
                        })
                    }

                } else {
                    res({
                        status: false,
                        message: "No agent added till now",
                        statusCode: 201,
                    })
                }
            }
        } catch (error) {
            console.log("error occured", error)
        }

    },

    agentUserList: async (req, res) => {
        try {
            console.log('body:', req);
            if (req.skip == undefined) {
                req.skip = 0;
            } else {
                req.skip = parseInt(req.skip);
            }
            if (req.limit == undefined) {
                req.limit = 10;
            } else {
                req.limit = parseInt(req.limit);
            }
    
            let clientdata = await db.Client.findOne(
                { clientId: req.clientId },
                { _id: 1, clientId: 1, agentConfig: 1 }
            );
    
            let docCount = await db.Userchat.aggregate([
                {
                    $match: {
                        $or: [
                            { clientId: clientdata.clientId },
                            { clientId: clientdata._id }
                        ]
                    }
                },
                {
                    $count: "documentCount"
                }
            ]);

            let matchQuery = {};
    
            if (req.userType === 'whatsapp') {
                matchQuery.userId = { $regex: '^wa_' };
            }else if (req.userType === 'facebook') {
                matchQuery.userId = { $regex: '^fb_' };
            }else if (req.userType === 'google') {
                matchQuery.userId = { $regex: '^gbm_' };
            }else if (req.userType === 'telegram') {
                matchQuery.userId = { $regex: '^tg_' };
            }else if (req.userType === 'insta') {
                matchQuery.userId = { $regex: '^ig_' };
            }else if (req.userType === 'android') {
                matchQuery.userId = { $regex: '^android_' };
            }else if (req.userType === 'ios') {
                matchQuery.userId = { $regex: '^ios_' };
            }else if (req.userType === 'web') {
                matchQuery.userId = { $not: { $regex: '^wa_|^fb_|^gbm_|^tg_|^ig_|^android_|^ios_' } };
            }
           
            let data;
            if(req.agentId) {
                const agentId = req.agentId;
            const agentObjectId = mongoose.Types.ObjectId(agentId);
                 data = await db.Userchat.aggregate([
                    {
                        $match: {
                            $and: [matchQuery],
                            $or: [
                                { clientId: clientdata.clientId },
                                { clientId: clientdata._id }
                            ],
                            agentsCommunicated: { $in: [agentObjectId] }
    
                        }
                    },
                    {
                        $project: {
                            _id: 0,
                            userId: 1,
                            clientId: 1,
                            session: 1,
                            name: 1,
                            agentCurrentConrol: 1,
                            createdAt: { $slice: ["$chats.createdAt", -1] },
                            lastchat: { $slice: ["$chats.text", -1] }
                        }
                    },
                    { $sort: { createdAt: -1 } },
                    { $skip: req.skip },
                    { $limit: req.limit }
                ]);
            }else {
                
            data = await db.Userchat.aggregate([
                {
                    $match: {
                        $and: [matchQuery],
                        $or: [
                            { clientId: clientdata.clientId },
                            { clientId: clientdata._id }
                        ],

                    }
                },
                {
                    $project: {
                        _id: 0,
                        userId: 1,
                        clientId: 1,
                        session: 1,
                        name: 1,
                        agentCurrentConrol: 1,
                        createdAt: { $slice: ["$chats.createdAt", -1] },
                        lastchat: { $slice: ["$chats.text", -1] }
                    }
                },
                { $sort: { createdAt: -1 } },
                { $skip: req.skip },
                { $limit: req.limit }
            ]);
            }
    
    
            if (data.length == 0) {
                res({
                    statusCode: 200,
                    status: true,
                    message: "No user found",
                    res: [],
                    config: clientdata.agentConfig,
                });
            } else {
                res({
                    statusCode: 200,
                    status: true,
                    message: "User list found successfully",
                    res: data,
                    limit: req.limit,
                    skip: req.skip,
                    docCount: docCount[0].documentCount,
                    config: clientdata.agentConfig,
                });
            }
    
        } catch (error) {
            console.log("Error occurred", error);
            // res.status(500).json({ error: 'An error occurred' });
        }
    },
    agentUserChat: async(req, res) => {
        try {
            let getTempName = await db.campAnalytics.findOne(
                { chatId : req.userId },
                { parameters : 1 ,media_url:1, tempName : 1, chatId : 1 , clientId :1,createdAt:1});

            let waTemplateData,parameters,sentOn,media_url;
            if(getTempName){
                parameters=getTempName.parameters;
                sentOn=getTempName.createdAt;
                media_url=getTempName.media_url;
                waTemplateData = await db.whatsAppTemplate.findOne(
                    { name : getTempName.tempName , clientId : getTempName.clientId },
                    { text : 1 ,msg_type : 1 ,name :1 });
            }else{
                waTemplateData ={};
                parameters =[];
                sentOn ="";
                media_url="";
            }

            
            let data = await db.Userchat.aggregate([{
                    $match: {
                        $and: [{ "userId": req.userId }]
                    }
                }, {
                    $project: {
                        _id: 0,
                        userId: 1,
                        clientId: 1,
                        chats: 1,
                        agentCurrentConrol: 1,
                        location: 1,
                        session: 1,
                        createdAt: 1
                    }
                }])
                // console.log('ss',data)
            if (data.length == 0) {
                res({
                    statusCode: 201,
                    status: false,
                    message: "No user founded"

                })
            } else {
                res({
                    statusCode: 200,
                    status: true,
                    message: "Userlist founded successfully",
                    res: data,
                    templateData : waTemplateData,
                    parameters : parameters,
                    sentOn  : sentOn,
                    media_url : media_url
                })
            }

        } catch (error) {
            console.log("error occured", error)
        }


    },
    login: async(data, res) => {
        try {
            console.log('shviamm', data)
            let agentDetails = await db.humanagents.aggregate([
                { $match: { "clientId": data.clientId } },
                { $unwind: "$agents" },
                { $match: { "agents.email": data.email } },
                { $replaceRoot: { newRoot: "$agents" } }
            ]);
            if (!agentDetails || agentDetails.length == 0) {
                res({
                    statusCode: 201,
                    status: false,
                    message: `Wrong password or email or client id.
Please provide registered details.`
                })
            } else {
                let agentDetailsObj = agentDetails[0];
                if (agentDetailsObj.isBlocked === true ||
                    agentDetailsObj.isBlocked == 'true') {
                    res({
                        statusCode: 201,
                        status: false,
                        message: `Your account has been blocked, please contact the administration for further process.`
                    })
                } else {
                    let hashPassword = await bcrypt.hash(data.password, salt);
                    let comparepwd = await
                    bcrypt.compare(data.password.toString(), agentDetailsObj.password);
                    if (comparepwd === true) {
                        let token = createToken(agentDetailsObj);
                        agentDetailsObj.authtoken = token;
                        agentDetailsObj.statusCode = 200;
                        agentDetailsObj.status = true;
                        agentDetailsObj.message = "Logged your account";
                        res(agentDetailsObj);
                    } else {
                        res({
                            statusCode: 201,
                            message: "wrong Password",
                            status: false
                        });
                    }
                }
            }
        } catch (error) {
            console.log("error occured", error)
        }
    },
    agentTakeOrLeaveControl: async(data, res) => {
        try {
            if (data.agentCurrentConrol.agentId) {

                db.Userchat.update({ userId: data.userId }, {
                        $addToSet: {
                            agentsCommunicated: ObjectID(data.agentCurrentConrol.agentId)
                        },
                        $set: {
                            agentCurrentConrol: data.agentCurrentConrol,
                            chatControl: data.chatControl,
                            agentHandover: true,
                            userStatus: '1'
                        }
                    },
                    function(err, resUpdate) {
                        if (err) {
                            res({
                                statusCode: 201,
                                status: false,
                                message: 'Not added',
                                res: err

                            })
                        } else {
                            res({
                                statusCode: 200,
                                status: true,
                                message: "Data updated successflly",
                            })
                        }
                    });
            } else {
                db.Userchat.update({ userId: data.userId }, {
                        $set: {
                            agentCurrentConrol: data.agentCurrentConrol,
                            chatControl: data.chatControl,
                        }
                    },
                    function(err, resUpdate) {
                        if (err) {
                            res({
                                statusCode: 201,
                                status: false,
                                message: 'Not added',
                                res: err

                            })
                        } else {
                            res({
                                statusCode: 200,
                                status: true,
                                message: "Data updated successflly",
                            })
                        }
                    });
            }
        } catch (err) {
            console.log('error occured', err)
        }
    },
    myChatsUserList: async(req, res) => {
        try {
            let clientdata = await db.Client.findOne({ clientId: req.clientId }, { _id: 1, clientId: 1 });
            let data = await
            db.Userchat.aggregate([{
                    $match: {
                        $and: [
                            { $or: [{ clientId: clientdata.clientId }, { clientId: clientdata._id }] },
                            { 'agentCurrentConrol.agentId': req.agentId, 'agentCurrentConrol.currentControl': true },
                        ]
                    }
                },
                {
                    $project: { _id: 0, userId: 1, clientId: 1, session: 1, name: 1, agentCurrentConrol: 1, lastchat: { $slice: ["$chats.text", -1] } }
                },
                { $sort: { updatedAt: -1 } }
            ]);
            console.log("data", data.length)
            if (data.length == 0) {
                res({
                    statusCode: 200,
                    status: true,
                    message: "No user founded",
                    res: []

                })
            } else {
                // console.log('selels')
                res({
                    statusCode: 200,
                    status: true,
                    message: "Userlist founded successfully",
                    res: data
                })
            }

        } catch (error) {
            console.log("error occured", error)
        }


    },
    agentDataSave: async function(req, res) {
        try {
            let chats = {
                text: escapeHtml(req.body.text),
                ignoreMsg: false,
                messageType: "outgoing",
                agentControl: req.body.agentControl,
                replies: [],
                type_option: "true",
                possible_conflict: "False",
                user_query: "",
                intent: "",
                response_source: "",
                score: "NA",
                node: "False",
                createdAt: new Date()
            }
            let updateOpt = await db.Userchat.updateOne({ userId: req.body.userId }, {
                lastActiveTime: new Date(),
                userState: "OPEN",
                $addToSet: {
                    chats: chats,
                },
            });

            let userChats = await db.Userchat.aggregate([{
                    $match: {
                        $and: [
                            { "userId": req.body.userId },
                            //  {$or: [{ clientId: 'test_chatbot' }, { clientId: ObjectID("5f8b12ff2445497381fcfb27")}]},
                            { $or: [{ clientId: req.body.clientId }, { clientId: ObjectID(req.body.client_id) }] },
                            { "chats.agentControl": "true" },
                        ]
                    },
                },
                {
                    $project: {
                        chats: {
                            $filter: {
                                input: '$chats',
                                as: 'chats',
                                cond: {
                                    "$and": [
                                        { $eq: ['$$chats.agentControl', 'true'] }
                                    ]
                                }
                            },
                        },
                        _id: 0
                    },
                },
                { $unwind: "$chats" },
                { $replaceRoot: { newRoot: "$chats" } }
            ]);
            userChats.pop();
            let updateStatus = false;
            let diff = 0;
            if (userChats.length > 2) {
                if (userChats[userChats.length - 1].messageType == "outgoing" &&
                    userChats[userChats.length - 2].messageType == "outgoing") {} else if (userChats[userChats.length - 1].messageType == "outgoing") {} else if (userChats[userChats.length - 1].messageType == "incoming" &&
                    userChats[userChats.length - 2].messageType == "incoming") {
                    /*
                      when user sent sent multiple message to get the 1st message of user ,get the last  outgoing message index + 1 ,we will get the 1st message of user for current time
                      {last outgoing message ke bad wala message pick karo}
                    */
                    updateStatus = true;
                    let lastOutgoingMsgIndex = await userChats.map(el => el.messageType).lastIndexOf("outgoing"); //4
                    userChats[lastOutgoingMsgIndex + 1].createdAt;
                    diff = new Date() - userChats[lastOutgoingMsgIndex + 1].createdAt;;
                } else if (userChats[userChats.length - 1].messageType == "incoming") {
                    diff = new Date() - userChats[userChats.length - 1].createdAt;
                    updateStatus = true;
                }

                if (updateStatus == true) {
                    let findData = await db.agentResponseTime.findOne({
                        userId: req.body.userId,
                        agentId: new ObjectID(req.body.agentId)
                    })
                    if (findData) {
                        let saveData = await db.agentResponseTime.updateOne({ userId: req.body.userId, agentId: ObjectID(req.body.agentId) }, { $push: { responseTime: diff } })
                    } else {
                        let saveData = new db.agentResponseTime({
                            clientId: "test_chatbot",
                            userId: req.body.userId,
                            agentId: new ObjectID(req.body.agentId),
                            responseTime: [diff]
                        });
                        await saveData.save();
                    }
                }
            }

            let updateSum = await db.agentResponseTime.aggregate([{
                    $match: { "userId": req.body.userId }
                },
                {
                    "$project": {
                        "clientId": 1,
                        "totalRespTime": {
                            "$sum": "$responseTime"
                        },
                        "count": { $size: "$responseTime" }
                    }
                }
            ]);
            if (updateSum) {
                if (updateSum.length > 0) {
                    await db.agentResponseTime.updateOne({ userId: req.body.userId }, {
                        $set: {
                            totalSumOfMilliSec: updateSum[0].totalRespTime,
                            count: updateSum[0].count,
                        }
                    });
                }
            }
            res({
                statusCode: 200,
                message: "Chat Saved in database ",
                status: true,
                res: { "ChatSaved": updateOpt }
            });
        } catch (err) {
            console.log('some error occured', err)
        }
    },
    v2_agentSendMessageApi: async function(req, res) {
        try {
            io.sockets.emit("join", req.body.userId);
            io.sockets.emit("sendMessageInRoom", {
                userId: req.body.userId,
                message: req.body.message,
                clientId: req.body.clientId,
                agentName: req.body.agentName,
                avatar: "https://helloyubo.com/avatar.png"
            });
            res({
                statusCode: 200,
                message: "Chat Saved in database",
                status: true
            });
        } catch (err) {
            console.log('some error occured', err)
        }
    },
    v2_agentTakeControlApi: async function(req, res) {
        try {
            io.sockets.emit("join", req.body.userId);
            io.sockets.emit("sendMessageInRoom", {
                userId: req.body.userId,
                message: `${req.body.agentName} taking control now`,
                control: "yes",
                clientId: req.body.clientId,
                agentName: req.body.agentName,
                avatar: "https://helloyubo.com/avatar.png"
            });
            res({
                statusCode: 200,
                message: "Agent taking control now",
                status: true
            });
        } catch (err) {
            console.log('some error occured', err)
        }
    },
    v2_agentLeaveControlApi: async function(req, res) {
        try {
            io.sockets.emit("leave", req.body.userId);
            io.sockets.emit("sendMessageInRoom", {
                userId: req.body.userId,
                message: `${req.body.agentName} transferred the chat control to the bot`,
                control: "no",
                clientId: req.body.clientId,
                agentName: req.body.agentName,
                avatar: "https://helloyubo.com/avatar.png"
            });
            res({
                statusCode: 200,
                message: "Agent taking control now",
                status: true
            });
        } catch (err) {
            console.log('some error occured', err)
        }
    },

    /*************************************
     * To save agent attachment in db
     *************************************/
    saveAgentAttachment: async function(req, res) {
        try {
            new Promise((resolve, reject) => {
                var path;
                let time = new Date().getTime();
                filename = time + req.file.filename;
                clientId = req.body.fileList[0];
                AWS.config.update({
                    accessKeyId: config.s3Linode.accessKeyId,
                    secretAccessKey: config.s3Linode.secretAccessKey,
                    region: config.s3Linode.region,
                    s3BucketEndpoint: config.s3Linode.s3BucketEndpoint,
                });
                var s3 = new AWS.S3({ endpoint: config.s3Linode.url });
                const fileContent = fs.readFileSync(req.file.path);
                var params = {
                    Bucket: `yugasabot/${clientId}/Media/User_files`,
                    Key: `${filename}`,
                    ACL: "public-read",
                    ContentType: req.file.mimetype,
                    Body: fileContent,
                };
                s3.putObject(params, function(err, result) {
                    if (err) {
                        console.log("Error uploading data: ", err);
                        reject(err);
                    } else {
                        path = `${config.s3Linode.url}/${clientId}/Media/User_files/${filename}`;
                        // console.log('file path  ',path);
                        resolve(path);
                    }
                });

            }).then(function(path) {

                userId = req.body.fileList[1];
                attachmentType = req.body.fileList[2];

                let chats = {
                    text: "Attachment sent",
                    ignoreMsg: false,
                    messageType: "outgoing",
                    attachment: true,
                    attachmentType: attachmentType,
                    attachmentLink: path,
                    agentControl: "true",
                    replies: [],
                    type_option: "true",
                    possible_conflict: "False",
                    user_query: "",
                    intent: "",
                    response_source: "",
                    score: "NA",
                    node: "False",
                    createdAt: new Date()
                }
                db.Userchat.updateOne({ userId: userId }, {
                    $addToSet: {
                        chats: chats,
                    },
                }, function(err, result2) {
                    if (err) {
                        console.log('err', err)
                        res({
                            success: false,
                            statusCode: 201,
                            message: 'INTERNAL DB ERROR',
                            error: result2
                        });
                    }
                    // console.log('res',result2);
                });
                fs.unlink(req.file.path, function(err) {
                    if (err) console.log('Error during unlink file ')
                        // console.log('file deleted successfully')
                });

                res({
                    statusCode: 200,
                    message: "Chat saved successfully ",
                    status: true,
                    attachmentType: attachmentType,
                    attachmentLink: path
                });

            }).catch(function(err) {
                console.log(' Error in promise catch', err)
                res({
                    success: false,
                    statusCode: 201,
                    message: 'Error in promise catch block',
                    error: error
                });
            });

        } catch (error) {
            console.log('error in try catch block  ', error);
            res({
                success: false,
                statusCode: 201,
                message: 'error in try catch block',
                error: error
            });
        }
    },

    //to get card data of agent currently handling user count
    agentTakencontrolsCount: async(data, clientId, res) => {
        try {
            let humanAgent = await db.humanagents.findOne({ clientId: clientId }, { _id: 0, agents: 1 });
            let agent_id_array;
            // message: "Client Id does not exist ",
            if (humanAgent === null) {
                res({
                    statusCode: 201,
                    status: false,
                    message: "Client is not associated with any agents",
                    res: humanAgent
                })
            } else {
                if (humanAgent.agents && humanAgent.agents.length > 0) {
                    agent_id_array = await humanAgent.agents.map(ele => ele._id);
                    let agentTookControlUserList = await db.Userchat.find({
                        "agentCurrentConrol.agentId": { $in: agent_id_array },
                        "agentCurrentConrol.currentControl": true
                    }, { userId: 1, _id: 0, agentCurrentConrol: 1 });
                    res({
                        statusCode: 200,
                        message: "Total chats handling by agents ",
                        status: true,
                        res: { "chat_handling_by_agent": agentTookControlUserList.length }
                    });
                } else {
                    res({
                        statusCode: 200,
                        message: "Total chats handling by agents ",
                        status: true,
                        res: { "chat_handling_by_agent": Number('000') }
                    });
                }
            }
        } catch (err) {
            console.log('error occure', err);
        }

    },
    searchData: async(data, clientId, res) => {

    try{
        let client_id = await db.Client.findOne({ clientId: clientId }, { _id: 1, clientId: 1 });
        if(!client_id){
            res({
                statusCode: 201,
                message: `Client not found with this client id - ${clientId}`,
                status: false,
                res: []
            });
        }
        // let searchData = await db.Userchat.aggregate([
        //     { $match: { clientId: client_id._id, "chats.text": new RegExp(data.searchText, 'i') } },
        //     { $unwind: "$chats" },
        //     { $match: { clientId: client_id._id, "chats.text": new RegExp(data.searchText, 'i') } },
        //     { $group: { "_id": "$_id", "chats": { "$push": "$chats" }, userData: { $first: "$$ROOT" } } }
        // ]);
        let searchData = await db.Userchat.aggregate([
            {
              $match: {
                clientId: client_id._id,
                $or: [
                  { "chats.text": new RegExp(data.searchText, 'i') },
                  { name: new RegExp(data.searchText, 'i') } // Modified line for name search
                ]
              }
            },
            { $unwind: "$chats" },
            {
              $match: {
                clientId: client_id._id,
                $or: [
                  { "chats.text": new RegExp(data.searchText, 'i') },
                  { name: new RegExp(data.searchText, 'i') } // Modified line for name search
                ]
              }
            },
            {
              $group: {
                "_id": "$_id",
                "chats": { "$push": "$chats" },
                userData: { $first: "$$ROOT" }
              }
            }
          ]);

        res({
            statusCode: 200,
            message: "Search data",
            status: true,
            res: searchData
        });
    }catch(error){
        console.log('error occured in catch block', error)
    }
    },

    leaveAllControlsOnLogout: async(data, clientId, res) => {
        try {
            let agentTookControlUserList = await db.Userchat.find({
                "agentCurrentConrol.agentId": data.agentId,
                "agentCurrentConrol.currentControl": true
            }, { userId: 1, _id: 0 });

            if (agentTookControlUserList) {
                userListArray = await agentTookControlUserList.map(ele => ele.userId);

                updateUserData = await db.Userchat.update({ userId: { $in: userListArray } }, {
                    $set: {
                        "agentCurrentConrol": { "agentId": "", "currentControl": false },
                        "chatControl": 0,
                    }
                }, { multi: true })
                if (updateUserData.nModified > 0) {
                    console.log('updateUserData', updateUserData)
                    res({
                        statusCode: 200,
                        message: "All user control left by agent",
                        status: true,
                        res: updateUserData,
                        agentTookControlUserIds: userListArray
                    });
                } else {
                    res({
                        statusCode: 200,
                        message: "All user control left by agent",
                        status: true,
                        res: updateUserData,
                        agentTookControlUserIds: []
                    });
                }

            } else {
                res({
                    statusCode: 200,
                    message: "Agent have not taken any control",
                    status: true,
                    res: []
                });
            }

        } catch (err) {
            console.log('error occured', err)
        }

    },
    changeAgentpassword: async(data, res) => {
        try {
            let clientEmail = await db.Client.findOne({ clientId: data.clientId }, { _id: 0, email: 1 })
            if (clientEmail.email) {
                const token = jwt.sign({ clientId: data.clientId, clientEmail: clientEmail.email, agentEmail: data.email }, secretKey, {
                    expiresIn: "24h",
                });
                let update = await db.humanagents.updateOne({
                    'clientId': data.clientId,
                    'agents.email': data.email
                }, {
                    '$set': {
                        'agents.$.resetToken': token
                    }
                }, async function(err, result) {
                    if (result) {
                        await emailverifycontroller.resetAgentpasswordmail(
                            clientEmail.email,
                            token,
                            data.clientId,
                            data.email
                        );
                        res({
                            status: true,
                            message: `Token set successfully`,
                            statusCode: 200,
                            clientMail: clientEmail.email
                        })
                    } else {
                        res({
                            status: false,
                            message: 'INTERNAL DB ERROR ',
                            statusCode: 201,
                        })
                    }
                });
            }

        } catch (error) {
            console.log(error)
        }
    },
    agentListByClientId: async(data, clientId, res) => {
        try {
            userDetails = await db.Userchat.find({ userId: data.userId }, { session: 1, _id: 0 });
            agentIds = await db.humanagents.find({ clientId: clientId }, { agents: 1, clientId: 1, _id: 0 });
            let resultArray = [];
            if (agentIds.length > 0)
                resultArray = agentIds[0].agents.map(ele => { return ele._id })
            else
                resultArray = []

            let resData = {
                agentIds: resultArray,
                userId: data.userId,
                userDetails: userDetails,
                clientId: data.clientId
            }
            io.sockets.emit('send-notification', resData);

            res({
                statusCode: 200,
                message: "Agent Ids by client id  ",
                status: true,
                res: resData
            });

        } catch (err) {
            console.log('err', err);
        }

    },

    activeBotCount: async function(data, clientId, res) {
        try {
            let carBotData = await db.Userchat.aggregate([
                { $match: { $or: [{ clientId: clientId }, { clientId: ObjectID(clientId) }] } },
                {
                    $project: {
                        _id: 0,
                        botActive: {
                            $let: {
                                vars: {
                                    diff: {
                                        $divide: [
                                            { $subtract: [new Date(), { $last: "$chats.createdAt" }] },
                                            1000 * 60,
                                        ],
                                    },
                                },
                                in: {
                                    $cond: { if: { $lte: ["$$diff", 10] }, then: "Yes", else: "No" }
                                },
                            }
                        }
                    }
                },
                { $match: { botActive: "Yes" } },
                { $count: "activeBotsCount" }
            ]).cursor().exec().toArray()

            res({
                statusCode: 200,
                message: "Card bot data  ",
                status: true,
                res: carBotData
            });
        } catch (err) {
            console.log('err==', err);
        }

    },
    updateUserData: async function(data, res) {
        db.Userchat.update({ userId: data.userId }, {
            $set: {
                'session.name': data.name,
                'name' : data.name,
                'session.email': data.email,
                'session.phone': data.phone,
            }
        }, function(err, result) {
            if (err) {
                res({
                    statusCode: 201,
                    status: false,
                    message: "INTERNAL DB ERROR",
                    res: err
                })
            } else {
                res({
                    statusCode: 200,
                    message: "User data updated successfully",
                    status: true,
                });
            }

        });
    },
    //****generate token for frontend bot [used  in client.js file ] */
    generateToken: async(data, res) => {
        try {
            //console.log('data=======',data);
            let token = createToken(data);
            res({
                statusCode: 200,
                message: "Token generated",
                status: true,
                accessToken: token
            });
        } catch (err) {
            console.log('Error occured ', err);
        }
    },
    agent_List_By_ClientId_for_agentpanel: async(data, res) => {
        try {
            let humanAgent = await db.humanagents.findOne({
                clientId: data.clientId
            }, {
                _id: 0,
                'agents._id': 1,
                'agents.name': 1,
                'agents.email': 1,
                'agents.isBlocked': 1
            });
            if (humanAgent == null) {
                res({
                    statusCode: 201,
                    status: false,
                    message: "Client not  found with this client id"
                })
            } else {
                res({
                    statusCode: 200,
                    status: true,
                    message: "Agent list",
                    res: humanAgent,
                })
            }
        } catch (error) {
            console.log("error occured", error)
        }
    },
    getHandledUserByAgent: async(req, res) => {
        try {
            let userChatData = await db.Userchat.find({
                // $or : [{clientId: ObjectID("5f8b12ff2445497381fcfb27")},{clientId:'test_chatbot'}],
                $or: [{ clientId: ObjectID(req.user._id) }, { clientId: req.user.clientId }],
                agentHandover: true
            }).count();
            res({
                statusCode: 200,
                status: true,
                message: "Total number of user handled by agents",
                data: userChatData,
            })
        } catch (error) {
            res({
                statusCode: 201,
                status: false,
                message: "Error in catch block",
                error: error,
            })
            console.log("error occured", error)
        }
    },
    userAgentConversation: async(req, res) => {
        try {
            let agentAndUserChat = await db.Userchat.aggregate([{
                    $match: {
                        $and: [
                            // {$or: [{ clientId: 'test_chatbot' }, { clientId: ObjectID("5f8b12ff2445497381fcfb27")}]},
                            { $or: [{ clientId: req.user.clientId }, { clientId: ObjectID(req.user._id) }] },
                            { "chats.agentControl": "true" },
                        ]
                    },
                },
                {
                    $project: {
                        chats: {
                            $filter: {
                                input: '$chats',
                                as: 'chats',
                                cond: { $eq: ['$$chats.agentControl', 'true'] },
                            },
                        },
                        _id: 0,
                    },
                },
                { $unwind: "$chats" },
                { $replaceRoot: { newRoot: "$chats" } }
            ]);
            res({
                statusCode: 200,
                status: true,
                message: "agent and user's total conversations ",
                dataLength: agentAndUserChat.length,
                data: agentAndUserChat,
            })
        } catch (error) {
            res({
                statusCode: 201,
                status: false,
                message: "Error in catch block",
                error: error,
            })
            console.log("error occured", error)
        }
    },
    getClientData: async(req, res) => {
        let clientdata = await db.Client.findOne({
            clientId: req.body.clientId
        }, { _id: 1, clientId: 1 });

        res({
            statusCode: 200,
            status: true,
            message: "client data  ",
            data: clientdata
        })
    },
    //yudesk add ticket for a particular user and dont add ticket if ticket already exist for user
    addTicket: async(req, res) => {
        try {
            let userExistingTicket = await db.Userchat.findOne({ userId: req.body.userId }, { ticketId: 1 });
            if (userExistingTicket.ticketId == "" || userExistingTicket.ticketId == 'undefined' || userExistingTicket.ticketId == null) {
                // let agentDetails = await db.humanagents.aggregate([
                //     { $match: { "clientId": req.body.clientId } },
                //     { $unwind: "$agents" },
                //     { $match: { "agents._id": ObjectID(req.body.agentId) } },
                //     { $replaceRoot: { newRoot: "$agents" } },
                //     { $project: {"yudesk_access_token" : 1} }
                // ]);

                let data = {
                    "subject": req.body.subject,
                    "category": 2,
                    "body": req.body.body,
                    "created_by_agent": true
                };
                let headers = {
                    headers: {
                        'accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + yudesk_access_token,
                        // 'Authorization': 'Bearer '+ agentDetails[0].yudesk_access_token 

                    }
                };
                let addTicket = await axios.post('https://helpdesk.yugasa.org/api/v1/tickets', data, headers);

                let addTicketResult = await db.Userchat.updateOne({ userId: req.body.userId }, {
                    $set: { ticketId: addTicket.data.id }
                });

                res({
                    statusCode: 200,
                    status: true,
                    message: "Ticket added successfully",
                    data: addTicket.data
                })
            } else {
                res({
                    statusCode: 200,
                    status: true,
                    message: "Ticket already exists for this user, No need to regnerate a new ticket .",
                    data: {}
                })
            }
        } catch (error) {
            console.log('Error occured in catch block ', error);
            res({
                statusCode: 201,
                status: false,
                message: "ERROR OCCURED",
                data: error
            })
        }
    },
    getRepliesByTicketId: async function(req, res) {
        try {
            let userExistingTicket = await db.Userchat.findOne({ userId: req.body.userId }, { ticketId: 1 });
            if (userExistingTicket.ticketId && userExistingTicket.ticketId != "" &&
                userExistingTicket.ticketId != 'undefined' && userExistingTicket.ticketId != null) {

                // let agentDetails = await db.humanagents.aggregate([
                //     { $match: { "clientId": req.body.clientId } },
                //     { $unwind: "$agents" },
                //     { $match: { "agents._id": ObjectID(req.body.agentId) } },
                //     { $replaceRoot: { newRoot: "$agents" } },
                //     { $project: {"yudesk_access_token" : 1} }
                // ]);
                let headers = {
                    headers: {
                        'accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + yudesk_access_token
                            // 'Authorization': 'Bearer '+ agentDetails[0].yudesk_access_token 

                    }
                };

                let URL = `https://helpdesk.yugasa.org/api/v1/tickets/${userExistingTicket.ticketId}/replies?perPage=100&page=1`;
                let getTicketData = await axios.get(URL, headers);
                res({
                    statusCode: 200,
                    status: true,
                    message: "Ticket replies found",
                    data: getTicketData.data
                })
            } else {
                res({
                    statusCode: 200,
                    status: true,
                    message: "Ticket id not found ,Please take control to generate ticket if for this user",
                    data: {}
                })
            }
        } catch (error) {
            console.log('error 222222', error);
            res({
                statusCode: 201,
                status: false,
                message: "ERROR OCCURED",
                data: error
            })
        }
    },
    addRepliesOnTicket: async function(req, res) {
        try {
            let userExistingTicket = await db.Userchat.findOne({ userId: req.body.userId }, { ticketId: 1 });
            if (userExistingTicket.ticketId && userExistingTicket.ticketId != "" &&
                userExistingTicket.ticketId != 'undefined' && userExistingTicket.ticketId != null) {

                // let agentDetails = await db.humanagents.aggregate([
                //     { $match: { "clientId": req.body.clientId } },
                //     { $unwind: "$agents" },
                //     { $match: { "agents._id": ObjectID(req.body.agentId) } },
                //     { $replaceRoot: { newRoot: "$agents" } },
                //     { $project: {"yudesk_access_token" : 1} }
                // ]);
                let headers = {
                    headers: {
                        'accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + yudesk_access_token
                            // 'Authorization': 'Bearer '+ agentDetails[0].yudesk_access_token 

                    }
                };

                let data = {
                    "body": req.body.body,
                    "status": req.body.status
                };

                let URL = `https://helpdesk.yugasa.org/api/v1/tickets/${userExistingTicket.ticketId}/replies`
                let getTicketData = await axios.post(URL, data, headers);
                res({
                    statusCode: 200,
                    status: true,
                    message: "Ticket replies added successfully.",
                    data: getTicketData.data
                })

            } else {
                res({
                    statusCode: 200,
                    status: true,
                    message: "Ticket not generated for this user, Please take control over the chat to generate ticket for this user",
                    data: {}
                })
            }

        } catch (error) {
            console.log('error 222222', error);
            res({
                statusCode: 201,
                status: false,
                message: "ERROR OCCURED",
                data: error
            })
        }
    },
    deleteRepliesById: async function(req, res) {
        try {
            let headers = {
                headers: {
                    'accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + '17|whYkgBWDwLodah281cu2skryl9mn5kBkU7pMsLmu'
                }
            };
            let URL = `https://helpdesk.yugasa.org/api/v1/replies/${req.body.replyId}`
            let getTicketData = await axios.delete(URL, headers);

            res({
                statusCode: 200,
                status: true,
                message: "Replies deleted successfully.",
            })
        } catch (error) {
            console.log('error 222222', error);
            res({
                statusCode: 201,
                status: false,
                message: "ERROR OCCURED",
                data: error
            })
        }
    },
    getTicketById: async function(req, res) {
        try {
            let headers = {
                headers: {
                    'accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + '17|whYkgBWDwLodah281cu2skryl9mn5kBkU7pMsLmu'
                }
            };

            let URL = `https://helpdesk.yugasa.org/api/v1/tickets/+${req.body.ticketId}`;
            let getTicketData = await axios.get(URL, headers);
            res({
                statusCode: 200,
                status: true,
                message: "Ticket data found",
                data: getTicketData.data
            })
        } catch (error) {
            console.log('error 222222', error);
            res({
                statusCode: 201,
                status: false,
                message: "ERROR OCCURED",
                data: error
            })
        }
    },
};


// **********create token using arrow function ***********************//
function createToken(data) {
    var tokenData = {
        id: data._id,
        clientId: data.clientId
    };
    var token = jwt.sign(tokenData, secretKey, {
        expiresIn: 86400 // expires in 24 hours
            //  expiresIn: '30d'
    });
    return token;
};
// **********yudesk generate token using username and password of yudesk agent  ***********//
async function generateYuboDeskToken(data) {
    try {
        let loginData = {
            "email": data.email,
            "password": data.password,
            "token_name": data.email
        };
        let generateYudeskToken = await axios.post('https://helpdesk.yugasa.org/api/v1/auth/login', loginData);
        yuDeskAccessToken = generateYudeskToken.data.user.access_token;
        let update = await db.humanagents.updateOne({
            'clientId': data.clientId,
            'agents.email': data.email
        }, {
            '$set': {
                'agents.$.yudesk_access_token': yuDeskAccessToken,
            }
        });

    } catch (error) {
        console.log('Error occure during YuDesk Login api hit', error);
    }
}
module.exports = AdgentController;
