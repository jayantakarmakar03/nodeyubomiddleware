var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var hbs = require('hbs');
var cors = require('cors');
// const helmet = require('helmet');
var bodyParser = require('body-parser');
require('dotenv').config();
var indexRouter = require('./routes/index');
var botsRouter = require('./routes/bot');
var botsAppRouter = require('./routes/yuboapp');
var superadminRouter = require('./routes/superadmin');
var humanAgent = require('./routes/humanAgent')
const { globalAgent } = require('http');
var app = express();  

// view engine setup
// view engine setup
// app.use(helmet.frameguard({ action: "sameorigin" }));
hbs.registerPartials("./views/partials");
global.rootdir = __dirname
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));

app.use(logger('dev'));
app.use(bodyParser.json({limit: '100mb'}))
app.use(bodyParser.urlencoded({limit: '100mb', extended: true, parameterLimit: 1000000})) 
// app.use(express.json());
// app.use(bodyParser.json({limit: '100mb'}))
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/yuboBot', botsRouter);
app.use('/app', botsAppRouter);
app.use('/admin', superadminRouter);
app.use('/agent',humanAgent)
app.use(cors());
//CORS
app.use(function(req, res, next) {
  // res.setHeader("Access-Control-Allow-Origin", "*");
  // res.setHeader('Set-Cookie', 'key=value; secure; HttpsOnly;  SameSite=None');
  res.header("access-control-allow-methods", "GET, POST, PUT");
  res.header("Content-Type", "application/json");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

	next();
});

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });
function ignoreFavicon(req, res, next) {
  if (req.originalUrl === '/favicon.ico') {
    res.status(204).json({nope: true});
  } else {
    next();
  }
}
app.use(ignoreFavicon);

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log(err);
  //res.render('error');
});

module.exports = app;
