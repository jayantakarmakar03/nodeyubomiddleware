const db = require("../models/all-models"),
jwt = require("jsonwebtoken"),
emailverifycontroller = require("../controllers/emailverification.js");
bcrypt = require('bcrypt');

module.exports = {
    forgotpassword:forgotpassword,
    resettoken:resettoken
  };

    /** for forgot password */
async function forgotpassword (req, res, next) {
    const user = await db.Client.findOne( {"$or":[{ email: req.body.email },{clientId:req.body.email}]});
    if (user) {
      const token = jwt.sign({ userId: user._id }, process.env.SECRETKEY, {
        expiresIn: "10m",
      });
      await db.Client.updateOne(
        { email: user.email },
        { $set: { reset_token: token } }
      );
      await emailverifycontroller.resetpasswordmail(
        user.email,
        token,
        user.clientId
      );
      console.log('ssadfsa',user.email)
     let mail =user.email
   mail =mail.split('');
    let finalArr=[];
    let len =mail.indexOf('@');
   
   mail.forEach((item,pos)=>{
    (pos>=2 && pos<=len+5) ? finalArr.push('*') : finalArr.push(user.email[pos]);
    })
    resetFailedLoginStatus(user);
  
      req.flash(
        "success_msg",
        "A reset link has been sent to " +finalArr.join('')
      );
      res.redirect("/login");
    } else {
      req.flash("error_msg", "User not found");
      res.redirect("/register");
    }
  }
async function resetFailedLoginStatus(user){
    try{
           await db.Client.updateOne(
            {_id : user._id},
            {
                $set: {
                        failedLoginCount : 0
                }
            });
            return ;
    }catch(error){
        console.log('Error in catch block', error)
    }
}
  /** for reset password */
  async function resettoken (req, res, next) {
    var user = await db.Client.findOne({ reset_token: req.body.token });
    if (user) {
      var useremail = user.email;
      await jwt.verify(
        req.body.token,
        process.env.SECRETKEY,
        async function (err) {
          if (err) {
            req.flash("error_msg", "Reset Token Expired");
            res.redirect("/forgot");
          } else {
            if (req.body.newPassword != req.body.cnfrmNewPassword) {
              req.flash("error_msg", "Both the passwords do not match.");
              res.redirect(`/reset?token=${req.body.token}`);
            } else {
              await db.Client.updateOne(
                { _id: user._id },
                {
                  $set: {
                    password: bcrypt.hashSync(req.body.newPassword, 12),
                    reset_token: "",
                  },
                }
              );
              req.flash("success_msg", "Password reset successfully.");
              await emailverifycontroller.updatepasswordmail(
                useremail,
                user.clientId
              );
              res.redirect("/login");
            }
          }
        }
      );
    } else {
      req.flash("error_msg", "User not found");
      res.redirect("/register");
    }
  }